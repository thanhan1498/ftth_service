/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.business;

import com.viettel.bccs.cc.dao.CompGetDataDAO;
import com.viettel.bccs.cc.supplier.ComplaintSupplier;
import com.viettel.bccs.cm.bussiness.BaseBussiness;
import com.viettel.bccs.cc.model.CompDepartmentGroup;
import com.viettel.bccs.cc.model.ComplaintAcceptSource;
import com.viettel.bccs.cc.model.ComplaintAcceptType;
import com.viettel.bccs.cc.model.ComplaintGroup;
import com.viettel.bccs.cc.model.ComplaintLevel;
import com.viettel.bccs.cc.model.ComplaintType;
import com.viettel.bccs.cc.model.Employee;
import com.viettel.bccs.cc.model.GetSubscriptionInfoForm;
import com.viettel.bccs.cc.model.ServiceType;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.brcd.ws.model.input.ComplaintInput;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author duyetdk
 */
public class ComplaintBusiness extends BaseBussiness {
    protected ComplaintSupplier supplier = new ComplaintSupplier();

    public ComplaintBusiness() {
        logger = Logger.getLogger(ComplaintBusiness.class);
    }
    
    /**
     * @author: duyetdk
     * @since 9/4/2019
     * lay danh sach loai complaint duoc accept
     * @param ccSession
     * @param code
     * @return 
     */
    public List<ComplaintAcceptType> getListComplaintAcceptType(Session ccSession, String code){
        List<ComplaintAcceptType> result = supplier.getListComplaintAcceptType(ccSession,code);
        return result;
    }
    
     /**
     * @author: duyetdk
     * @since 9/4/2019
     * lay danh sach loai complaint duoc accept
     * @param ccSession
     * @return 
     */
    public List<ComplaintAcceptSource> getListComplaintAcceptSource(Session ccSession){
        List<ComplaintAcceptSource> result = supplier.getListComplaintAcceptSource(ccSession);
        return result;
    }
    
    /**
     * @author: duyetdk
     * @since 9/4/2019
     * lay danh sach loai complaint theo groupId
     * @param ccSession
     * @param groupId
     * @return 
     */
    public List<ComplaintType> getListComplaintType(Session ccSession, Long groupId){
        List<ComplaintType> result = null;
        if(groupId == null || groupId <= 0L){
            return result;
        }
        result = supplier.getListComplaintType(ccSession, groupId);
        return result;
    }
    
    /**
     * @author: duyetdk
     * @since 9/4/2019
     * lay danh sach loai nhom complaint theo parentId
     * @param ccSession
     * @param parentId
     * @return 
     */
    public List<ComplaintGroup> getListComplaintGroupType(Session ccSession, Long parentId){
        List<ComplaintGroup> result = null;
        if(parentId == null || parentId <= 0L){
            return result;
        }
        result = supplier.getListComplaintGroupType(ccSession, parentId);
        return result;
    }
    
    /**
     * @author: duyetdk
     * @since 9/4/2019
     * lay danh sach nhom complaint theo comp_level_id
     * @param ccSession
     * @param compLevelId
     * @return 
     */
    public List<ComplaintGroup> getListComplaintGroup(Session ccSession, Long compLevelId){
        List<ComplaintGroup> result = null;
        if(compLevelId == null || compLevelId <= 0L){
            return result;
        }
        result = supplier.getListComplaintGroup(ccSession, compLevelId);
        return result;
    }
    
    /**
     * 
     * @param ccSession
     * @param complaintCode
     * @return 
     */
    public List<ComplaintLevel> getListComplaintLevel(Session ccSession, String complaintCode){      
        List<ComplaintLevel> result = supplier.getListComplaintLevel(ccSession, complaintCode);
        return result;
    }
    
    /**
     * @throws java.lang.Exception
     * @author: duyetdk
     * @since 18/4/2019
     * get team giai quyet su co
     * @param cmPosSession
     * @param subId
     * @return 
     */
    public List<CompDepartmentGroup> getListCompTeam(Session cmPosSession, Long subId) throws Exception {
        List<CompDepartmentGroup> result = null;
        List<CompDepartmentGroup> lst = new ArrayList<CompDepartmentGroup>();

        if (subId == null || subId <= 0L) {
            return result;
        }

        GetSubscriptionInfoForm mangeStaff = new CompGetDataDAO().getStaffManageConnector(cmPosSession, subId);

        if (mangeStaff != null) {
            lst.add(new CompDepartmentGroup(mangeStaff.getShopId(), mangeStaff.getShopCode(), mangeStaff.getShopName(),
                    mangeStaff.getStaffCode(), mangeStaff.getStaffName(), null));
            return lst;
        } else {
            result = supplier.getListCompTeam(cmPosSession, subId);
            return result;
        }
    }
    
    /**
     * 
     * @param ccSession
     * @param complaintInput
     * @param subAdslll
     * @param staffCodeLogin
     * @param acceptTypeId
     * @param nowDate
     * @param timeToResolve
     * @param dateHour
     * @param compLevelId
     * @param contractNo
     * @param shopId
     * @param serviceTypeId
     * @param depId
     * @return 
     */
    public String insertComplain(Session ccSession, ComplaintInput complaintInput, SubAdslLeaseline subAdslll,
            String staffCodeLogin, Long acceptTypeId, Date nowDate, Date timeToResolve, Date dateHour, Long compLevelId,
            String contractNo, Long shopId, Long serviceTypeId, Long depId) {
        String mess = null;
        supplier.insertComplain(ccSession, complaintInput, subAdslll, nowDate, staffCodeLogin, acceptTypeId, 
                timeToResolve, dateHour, compLevelId, contractNo, shopId, serviceTypeId, depId);
        return mess;
    }
    
    /**
     * 
     * @param ccSession
     * @param complainId
     * @param nowDate
     * @param description
     * @param employeeId
     * @param assignType
     * @param active
     * @param managerId
     * @param depSendId
     * @param depReceiveId
     * @param agreeDate
     * @return 
     */
    public String insertAssignManagement(Session ccSession, Long complainId, Date nowDate,
            String description, Long employeeId, Long assignType, Long active, Long managerId, 
            Long depSendId, Long depReceiveId, Date agreeDate) {
        String mess = null;
        supplier.insertAssignManagement(ccSession, complainId, nowDate, description, employeeId, assignType, active, managerId, depSendId, depReceiveId, agreeDate);
        return mess;
    }
    
    /**
     * 
     * @param ccSession
     * @param complainId
     * @param staffCodeLogin
     * @param nowDate
     * @param limitDate
     * @param resultContent
     * @param description
     * @param memo
     * @param depId
     * @return 
     */
    public String insertCompProcess(Session ccSession, Long complainId, String staffCodeLogin, Date nowDate, 
            Date limitDate, String resultContent, String description, String memo, Long depId){
        String mess = null;
        supplier.insertCompProcess(ccSession, complainId, staffCodeLogin, nowDate, limitDate, resultContent, description, memo, depId);
        return mess;
    }
    
    /**
     * 
     * @param cmPosSession
     * @param taskMngtId
     * @param complaintInput
     * @param subAdslll
     * @param complainId
     * @param staffIdLogin
     * @param nowDate
     * @param limitDate
     * @param contractNo
     * @return 
     */
    public String insertTaskManagement(Session cmPosSession,Long taskMngtId, ComplaintInput complaintInput, SubAdslLeaseline subAdslll, 
            Long complainId, Long staffIdLogin, Date nowDate, Date limitDate, String contractNo){
        String mess = null;
        supplier.insertTaskManagement(cmPosSession, complaintInput,taskMngtId, subAdslll, complainId, staffIdLogin, nowDate, limitDate, contractNo);
        return mess;
    }
    
    /**
     * 
     * @param cmPosSession
     * @param taskShopMngtId
     * @param taskMngtId
     * @param receiveShopId
     * @param staffIdLogin
     * @param nowDate
     * @return 
     */
    public String insertTaskShopManagement(Session cmPosSession,Long taskShopMngtId, Long taskMngtId, Long receiveShopId,
            Long staffIdLogin, Date nowDate){
        String mess = null;
        supplier.insertTaskShopManagement(cmPosSession,taskShopMngtId, taskMngtId, receiveShopId, staffIdLogin, nowDate);
        return mess;
    }
    
    /**
     * 
     * @param ccSession
     * @param staffCodeLogin
     * @return 
     */
    public  List<Employee> getListEmployeeByUserName(Session ccSession, String staffCodeLogin){      
        List<Employee> result = supplier.getListEmployeeByUserName(ccSession, staffCodeLogin);
        return result;
    }
    
    /**
     * 
     * @param cmPosSession
     * @param subId
     * @return 
     */
    public boolean checkAccountVip(Session cmPosSession, Long subId){
        return supplier.checkAccountVip(cmPosSession, subId);
    }
    
    /**
     * 
     * @param complainId
     * @param isVip
     * @param ccSession
     * @return 
     */
    public String updateIsVipAccountComplain(Long complainId, Long isVip, Session ccSession){
        String mess = null;
        int result = supplier.updateIsVipAccountComplain(complainId, isVip, ccSession);
        if(result == 0){
            mess = "Error update table complain";
        }
        return mess;
    }
    
    /**
     * 
     * @param cmPosSession
     * @param subId
     * @param nowDate
     * @return 
     */
    public Date getLimitDate(Session cmPosSession, Long subId, Date nowDate){
        return supplier.getLimitDate(cmPosSession, subId, nowDate);
    }
    
    /**
     * 
     * @param ccSession
     * @param serviceCode
     * @return 
     */
    public List<ServiceType> getListServiceType(Session ccSession, String serviceCode){
        return supplier.getListServiceType(ccSession, serviceCode);
    }
}
