/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.controller;

import com.viettel.bccs.cc.business.ComplaintBusiness;
import com.viettel.bccs.cc.model.Complain;
import com.viettel.bccs.cm.controller.BaseController;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cc.model.ComplainAgent;
import com.viettel.bccs.cc.model.ComplaintAcceptSource;
import com.viettel.brcd.ws.model.output.CompDepartmentGroupOut;
import com.viettel.bccs.cc.model.ComplaintAcceptType;
import com.viettel.bccs.cc.model.ComplaintEx;
import com.viettel.brcd.ws.model.output.ComplaintAcceptTypeOut;
import com.viettel.bccs.cc.model.ComplaintGroup;
import com.viettel.brcd.ws.model.output.ComplaintGroupOut;
import com.viettel.bccs.cc.model.ComplaintLevel;
import com.viettel.bccs.cc.model.ComplaintType;
import com.viettel.bccs.cc.model.Employee;
import com.viettel.bccs.cc.supplier.ComplaintSupplier;
import com.viettel.bccs.cm.bussiness.BaseBussiness;
import com.viettel.bccs.cm.bussiness.ContractBussiness;
import com.viettel.bccs.cm.bussiness.SubAdslLeaselineBussines;
import com.viettel.bccs.cm.bussiness.TokenBussiness;
import com.viettel.bccs.cm.bussiness.common.ExchangeClient;
import com.viettel.bccs.cm.bussiness.common.Object.InfoCell;
import com.viettel.bccs.cm.dao.CustomerDAO;
import com.viettel.bccs.cm.dao.TaskManagementDAO;
import com.viettel.bccs.cm.database.DAO.ContractDAO;
import com.viettel.bccs.cm.model.Contract;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubMbPos;
import com.viettel.bccs.cm.model.TaskManagement;
import com.viettel.bccs.cm.model.Token;
import com.viettel.bccs.cm.model.pre.CustomerPre;
import com.viettel.bccs.cm.model.pre.SubMbPre;
import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.brcd.dao.pre.CustomerPreDAO;
import com.viettel.brcd.dao.pre.SubMbPreDAO;
import com.viettel.brcd.ws.model.input.ComplaintInput;
import com.viettel.brcd.ws.model.input.SubAdslLeaselineOut;
import com.viettel.brcd.ws.model.output.ComplaintAcceptSourceOut;
import com.viettel.brcd.ws.model.output.ComplaintTypeOut;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.im.database.DAO.ShopDAO;
import com.viettel.im.database.DAO.StaffDAO;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author metfone
 */
public class ComplaintController extends BaseController {

    private static Long GROUP_COMPLAINT_ID = 1000007l;
    public static final Long NETWORK_2G = 1000048l;
    public static final Long NETWORK_3G = 1000050l;
    public static final Long NETWORK_4G = 101542l;

    public ComplaintController() {
        logger = Logger.getLogger(ComplaintController.class);
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019 lay danh sach loai complaint duoc accept
     * @param hibernateHelper
     * @param locale
     * @param token
     * @param code
     * @return
     */
    public ComplaintAcceptTypeOut getListComplaintAcceptType(HibernateHelper hibernateHelper, String locale, String token, String code) {
        ComplaintAcceptTypeOut result = null;
        Session ccSession = null;
        try {
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            ComplaintBusiness complaintBusiness = new ComplaintBusiness();
            List<ComplaintAcceptType> lst = complaintBusiness.getListComplaintAcceptType(ccSession, code);
            if (lst == null || lst.isEmpty()) {
                result = new ComplaintAcceptTypeOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            result = new ComplaintAcceptTypeOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lst);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ComplaintAcceptTypeOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(ccSession);
            LogUtils.info(logger, "ComplaintController.getListComplaintAcceptType:result=" + LogUtils.toJson(result));
        }
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019 lay danh sach nguon complaint
     * @param hibernateHelper
     * @param locale
     * @param token
     * @return
     */
    public ComplaintAcceptSourceOut getListComplaintAcceptSource(HibernateHelper hibernateHelper, String locale, String token) {
        ComplaintAcceptSourceOut result = null;
        Session ccSession = null;
        try {
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            ComplaintBusiness complaintBusiness = new ComplaintBusiness();
            List<ComplaintAcceptSource> lst = complaintBusiness.getListComplaintAcceptSource(ccSession);
            if (lst == null || lst.isEmpty()) {
                result = new ComplaintAcceptSourceOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            result = new ComplaintAcceptSourceOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lst);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ComplaintAcceptSourceOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(ccSession);
            LogUtils.info(logger, "ComplaintController.getListComplaintAcceptSource:result=" + LogUtils.toJson(result));
        }
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019 lay danh sach loai complaint theo groupId
     * @param hibernateHelper
     * @param groupId
     * @param locale
     * @param token
     * @return
     */
    public ComplaintTypeOut getListComplaintType(HibernateHelper hibernateHelper, Long groupId, String locale, String token) {
        ComplaintTypeOut result = null;
        Session ccSession = null;
        try {
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            ComplaintBusiness complaintBusiness = new ComplaintBusiness();
            List<ComplaintType> lst = complaintBusiness.getListComplaintType(ccSession, groupId);
            if (lst == null || lst.isEmpty()) {
                result = new ComplaintTypeOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            result = new ComplaintTypeOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lst);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ComplaintTypeOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(ccSession);
            LogUtils.info(logger, "ComplaintController.getListComplaintType:result=" + LogUtils.toJson(result));
        }
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019 lay danh sach loai nhom complaint theo parentId
     * @param hibernateHelper
     * @param parentId
     * @param locale
     * @param token
     * @return
     */
    public ComplaintGroupOut getListComplaintGroupType(HibernateHelper hibernateHelper, Long parentId, String locale, String token) {
        ComplaintGroupOut result = null;
        Session ccSession = null;
        try {
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            ComplaintBusiness complaintBusiness = new ComplaintBusiness();
            List<ComplaintGroup> lst = complaintBusiness.getListComplaintGroupType(ccSession, parentId);
            if (lst == null || lst.isEmpty()) {
                result = new ComplaintGroupOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            result = new ComplaintGroupOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lst);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ComplaintGroupOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(ccSession);
            LogUtils.info(logger, "ComplaintController.getListComplaintGroupType:result=" + LogUtils.toJson(result));
        }
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019 lay danh sach nhom complaint theo comp_level_id
     * @param hibernateHelper
     * @param locale
     * @param token
     * @return
     */
    public ComplaintGroupOut getListComplaintGroup(HibernateHelper hibernateHelper, String locale, String token) {
        ComplaintGroupOut result = null;
        Session ccSession = null;
        try {
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            ComplaintBusiness complaintBusiness = new ComplaintBusiness();
            List<ComplaintLevel> lstLevel = complaintBusiness.getListComplaintLevel(ccSession, Constants.COMPLAINT_CODE);
            List<ComplaintGroup> lstGroup = complaintBusiness.getListComplaintGroup(ccSession,
                    (lstLevel != null && lstLevel.size() > 0) ? lstLevel.get(0).getComplaintLevelId() : null);
            if (lstGroup == null || lstGroup.isEmpty()) {
                result = new ComplaintGroupOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            result = new ComplaintGroupOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lstGroup);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ComplaintGroupOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(ccSession);
            LogUtils.info(logger, "ComplaintController.getListComplaintGroupType:result=" + LogUtils.toJson(result));
        }
    }

    /**
     * @author: duyetdk
     * @since 18/4/2019
     * @des get team giai quyet su co
     * @param hibernateHelper
     * @param subId
     * @param locale
     * @param token
     * @return
     */
    public CompDepartmentGroupOut getListCompTeam(HibernateHelper hibernateHelper, Long subId, String locale, String token) {
        CompDepartmentGroupOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            ComplaintBusiness complaintBusiness = new ComplaintBusiness();

            //get limit date
            Date nowDate = new BaseBussiness().getSysDateTime(cmPosSession);
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date limitDate = complaintBusiness.getLimitDate(cmPosSession, subId, nowDate);

            //get team
//            List<CompDepartmentGroup> lst = complaintBusiness.getListCompTeam(cmPosSession, subId);
//            if (lst == null || lst.isEmpty()) {
//                result = new CompDepartmentGroupOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
//                return result;
//            }
            result = new CompDepartmentGroupOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), formatter.format(limitDate), null);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CompDepartmentGroupOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ComplaintController.getListCompTeam:result=" + LogUtils.toJson(result));
        }
    }

    /**
     * @author: duyetdk
     * @since 16/5/2019
     * @des lay thong tin subAdslLL
     * @param hibernateHelper
     * @param isdn
     * @param locale
     * @param token
     * @return
     */
    public SubAdslLeaselineOut getListSubAdslLeaseline(HibernateHelper hibernateHelper, String isdn, String locale, String token) {
        SubAdslLeaselineOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            if (isdn == null || isdn.trim().isEmpty()) {
                result = new SubAdslLeaselineOut(Constants.ERROR_CODE_1, "ISDN is required");
                return result;
            }
            SubAdslLeaseline subAdslll = new SubAdslLeaselineBussines().findByIsdn(cmPosSession, isdn);

            if (subAdslll == null) {
                result = new SubAdslLeaselineOut(Constants.ERROR_CODE_0, "ISDN is invalid");
                return result;
            }
            result = new SubAdslLeaselineOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), subAdslll);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new SubAdslLeaselineOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ComplaintController.getListCompTeam:result=" + LogUtils.toJson(result));
        }
    }

    /**
     *
     * @param hibernateHelper
     * @param complaintInput
     * @param locale
     * @param token
     * @return
     */
    public WSRespone submitComplaint(HibernateHelper hibernateHelper, ComplaintInput complaintInput, String locale, String token) {
        WSRespone result = null;
        Session cmPosSession = null;
        Session imPosSession = null;
        Session ccSession = null;
        boolean hasErr = false;
        String mess;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);

            if (complaintInput == null) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.complaintInput", locale));
                return result;
            }

            //lay danh sach task_management
            List<TaskManagement> lstTaskManagement = new TaskManagementDAO().findByProperty(cmPosSession, "subId", complaintInput.getSubId());
            if (lstTaskManagement != null && !lstTaskManagement.isEmpty()) {
                for (TaskManagement tsm : lstTaskManagement) {
                    if (Constants.TASK_PROGRESS_PROCESS.equals(tsm.getProgress())) {
                        result = new WSRespone(Constants.ERROR_CODE_1, "This customer complaint is being processed and not created anymore");
                        return result;
                    }
                }
            }
            LogUtils.info(logger, "ComplaintController.submitComplaint:wsRequest=" + LogUtils.toJson(complaintInput));
            Token tokenObj = new TokenBussiness().findByToken(cmPosSession, token);
            Long staffId = null;
            if (tokenObj != null) {
                staffId = tokenObj.getStaffId();
            }
            /*get info of user*/
            List<com.viettel.im.database.BO.Staff> staff = new StaffDAO(imPosSession).findByProperty("staffId", staffId);
            com.viettel.im.database.BO.Shop shop = new ShopDAO(imPosSession).findById(staff.get(0).getShopId());

            if (complaintInput.getComplainerName() == null || complaintInput.getComplainerName().trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.complainerName", locale));
                return result;
            }
            if (complaintInput.getComplainerPhone() == null || complaintInput.getComplainerPhone().trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.complainerPhone", locale));
                return result;
            }
            if (complaintInput.getComplainerAddress() == null || complaintInput.getComplainerAddress().trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.complainerAddress", locale));
                return result;
            }
            if (complaintInput.getSubId() == null || complaintInput.getSubId() <= 0L) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.subId", locale));
                return result;
            }
            if (complaintInput.getAcceptSourceId() == null || complaintInput.getAcceptSourceId() <= 0L) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.acceptSource", locale));
                return result;
            }
            if (complaintInput.getCompTypeId() == null || complaintInput.getCompTypeId() <= 0L) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.complaintType", locale));
                return result;
            }
            if (complaintInput.getCompContent() == null || complaintInput.getCompContent().trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.complaintContent", locale));
                return result;
            }
            if (complaintInput.getCustLimitDate() == null || complaintInput.getCustLimitDate().trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.timeToResolve", locale));
                return result;
            }
//            if (complaintInput.getDepId() == null || complaintInput.getDepId() <= 0L) {
//                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.coordinationUnit", locale));
//                return result;
//            }
//            if (complaintInput.getProLimitDate() == null || complaintInput.getProLimitDate().trim().isEmpty()) {
//                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.dateProcess", locale));
//                return result;
//            }

            SubAdslLeaseline subAdslll = new SubAdslLeaselineBussines().findById(cmPosSession, complaintInput.getSubId());
            if (subAdslll == null) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("complain.not.found.service", locale));
                return result;
            }
            String serviceCode = "";
            if ("A".equals(subAdslll.getServiceType())) {
                serviceCode = "ADSL";
            }
            if ("F".equals(subAdslll.getServiceType())) {
                serviceCode = "FTTH";
            }
            if ("L".equals(subAdslll.getServiceType())) {
                serviceCode = "LeaseLine";
            }

            ComplaintBusiness complaintBusiness = new ComplaintBusiness();
//            Long serviceTypeId = complaintBusiness.getListServiceType(ccSession, serviceCode).get(0).getServiceTypeId();
            Long serviceTypeId = (complaintBusiness.getListServiceType(ccSession, serviceCode) != null
                    && !complaintBusiness.getListServiceType(ccSession, serviceCode).isEmpty())
                    ? complaintBusiness.getListServiceType(ccSession, serviceCode).get(0).getServiceTypeId() : null;
            Date nowDate = new BaseBussiness().getSysDateTime(ccSession);
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Long complainId = new BaseSupplier().getSequence(ccSession, Constants.SEQ_COMPLAIN);
            complaintInput.setComplainId(complainId);
            Long acceptTypeId = complaintBusiness.getListComplaintAcceptType(ccSession, "MBCCS").get(0).getAcceptTypeId();
            Date custLimitDate = formatter.parse(complaintInput.getCustLimitDate());
//            Date proLimitDate = formatter.parse(complaintInput.getProLimitDate());
//            Date custLimitDate = complaintBusiness.getLimitDate(cmPosSession, complaintInput.getSubId(), nowDate);
//            Date proLimitDate = custLimitDate;
            Long compLevelId = complaintBusiness.getListComplaintLevel(ccSession, Constants.COMPLAINT_CODE).get(0).getComplaintLevelId();
            Contract contract = new ContractBussiness().findByContractId(cmPosSession, subAdslll.getContractId());

            //get depId
            Long depIdAccept;
            List<Employee> listEmp = complaintBusiness.getListEmployeeByUserName(ccSession, staff.get(0).getStaffCode());
            if (listEmp != null && listEmp.size() > 0) {
                depIdAccept = listEmp.get(0).getDepId();
            } else {
                depIdAccept = Constants.CALL_CENTER_DEP_ID;
            }

            //insert COMPLAIN
            mess = complaintBusiness.insertComplain(ccSession, complaintInput, subAdslll, staff.get(0).getStaffCode(),
                    acceptTypeId, nowDate, custLimitDate, null, compLevelId, contract.getContractNo(), shop.getShopId(), serviceTypeId, depIdAccept);

            if (mess != null) {
                if (!mess.trim().isEmpty()) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, mess);
                }
            }

            //check account is vip
            boolean isVip = complaintBusiness.checkAccountVip(cmPosSession, complaintInput.getSubId());

            //update COMPLAIN
            mess = complaintBusiness.updateIsVipAccountComplain(complainId, isVip ? 1L : 0L, ccSession);

            if (mess != null) {
                if (!mess.trim().isEmpty()) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, mess);
                }
            }

            //insert COMP_PROCESS
            mess = complaintBusiness.insertCompProcess(ccSession, complainId, staff.get(0).getStaffCode(),
                    nowDate, null, "Receive new complaint", "", "", shop.getShopId());

            if (mess != null) {
                if (!mess.trim().isEmpty()) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, mess);
                }
            }

            //insert ASSIGN_MANAGEMENT
            Long managerId = (complaintBusiness.getListEmployeeByUserName(ccSession, staff.get(0).getStaffCode()) != null
                    && !complaintBusiness.getListEmployeeByUserName(ccSession, staff.get(0).getStaffCode()).isEmpty())
                    ? complaintBusiness.getListEmployeeByUserName(ccSession, staff.get(0).getStaffCode()).get(0).getEmpId() : null;
            /*Ban ghi tiep nhan*/
            mess = complaintBusiness.insertAssignManagement(ccSession, complainId, nowDate, "Receive - Assign",
                    null, Constants.ASSIGN_TYPE_ACCEPT, Constants.ASSIGN_MANAGEMENT_ACTIVE, managerId, shop.getShopId(), shop.getShopId(), null);

            if (mess != null) {
                if (!mess.trim().isEmpty()) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, mess);
                }
            }

            /*Ban ghi giao viec*/
//            mess = complaintBusiness.insertAssignManagement(ccSession, complainId, nowDate, "Coordinate Departments", 
//                     null, Constants.ASSIGN_TYPE_COOR, Constants.ASSIGN_MANAGEMENT_ACTIVE, managerId, shop.getShopId(), complaintInput.getDepId(), null);
//            
//            if (mess != null) {
//                if (!mess.trim().isEmpty()) {
//                    hasErr = true;
//                    return new WSRespone(Constants.ERROR_CODE_1, mess);
//                }
//            }
            //insert TASK_MANAGEMENT
//            Long taskMngtId = new BaseSupplier().getSequence(cmPosSession, Constants.TASK_MANAGEMENT_SEQ);
//            mess = complaintBusiness.insertTaskManagement(cmPosSession, taskMngtId, complaintInput, subAdslll, 
//                    complainId, staffId, nowDate, custLimitDate, contract.getContractNo());
//            
//            if (mess != null) {
//                if (!mess.trim().isEmpty()) {
//                    hasErr = true;
//                    return new WSRespone(Constants.ERROR_CODE_1, mess);
//                }
//            }
            //insert TASK_SHOP_MANAGEMENT
//            Long taskShopMngtId = new BaseSupplier().getSequence(cmPosSession, Constants.TASK_SHOP_MANAGEMENT_SEQ);
//            mess = complaintBusiness.insertTaskShopManagement(cmPosSession,taskShopMngtId, taskMngtId, complaintInput.getDepId(), staffId, nowDate);
//            
//            if (mess != null) {
//                if (!mess.trim().isEmpty()) {
//                    hasErr = true;
//                    return new WSRespone(Constants.ERROR_CODE_1, mess);
//                }
//            }
            commitTransactions(ccSession);
            commitTransactions(cmPosSession);

            //sau khi create Task thanh cong => assign TASK
//            if (complaintInput.getReceiveStaffCode() != null && !complaintInput.getReceiveStaffCode().trim().isEmpty()) {
//                List<com.viettel.im.database.BO.Staff> receiveStaff = new StaffDAO(imPosSession).findByProperty("staffCode", complaintInput.getReceiveStaffCode());
//
//                TaskToAssignIn taskToAssignIn = new TaskToAssignIn();
//                taskToAssignIn.setTaskShopMngtId(taskShopMngtId.toString());
//                taskToAssignIn.setUserNameLogin(staff.get(0).getStaffCode());
//                taskToAssignIn.setStaffIdLogin(staffId);
//                taskToAssignIn.setShopCodeLogin(shop.getShopCode());
//                taskToAssignIn.setStaffAssignId(receiveStaff.get(0).getStaffId());
//                taskToAssignIn.setStaDate(formatter.format(nowDate));
//                taskToAssignIn.setEndDate(formatter.format(proLimitDate));
//                taskToAssignIn.setTelToAssign(receiveStaff.get(0).getIsdn());
//                taskToAssignIn.setTaskMngtId(taskMngtId.toString());
//                taskToAssignIn.setShopAssignId(receiveStaff.get(0).getShopId());
//                new TaskToAssignController().assignTask(hibernateHelper, locale, taskToAssignIn);
//            }
            result = new WSRespone(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (ParseException ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                    rollBackTransactions(ccSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            closeSessions(imPosSession);
            closeSessions(ccSession);
            LogUtils.info(logger, "ComplaintController.submitComplaint:result=" + LogUtils.toJson(result));
        }
    }

    public ParamOut getComNetworkQuality(HibernateHelper hibernateHelper, String token, String locale) {
        Session ccSession = null;
        ParamOut result = new ParamOut();
        result.setErrorCode(Constants.ERROR_CODE_1);
        try {
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            ComplaintBusiness complaintBusiness = new ComplaintBusiness();
            List<ComplaintGroup> lst = complaintBusiness.getListComplaintGroupType(ccSession, GROUP_COMPLAINT_ID);
            result.setErrorCode(Constants.ERROR_CODE_0);
            result.setErrorDecription(LabelUtil.getKey("common.success", locale));
            result.setListComplaintGroup(lst);
        } catch (Exception ex) {
            ex.printStackTrace();
            result.setErrorDecription(ex.getMessage());
        } finally {
            closeSessions(ccSession);
        }
        return result;
    }

    public ParamOut getComType(HibernateHelper hibernateHelper, String token, String locale, String isdn) {
        Session ccSession = null;
        Session raSession = null;
        ParamOut result = new ParamOut();
        result.setErrorCode(Constants.ERROR_CODE_1);
        try {
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            raSession = hibernateHelper.getSession(Constants.SESSION_RA);
            ComplaintBusiness complaintBusiness = new ComplaintBusiness();
            Long groupId = 0l;
            StringBuilder cellInfo = new StringBuilder();
            if (isdn == null || isdn.isEmpty()) {
                result.setErrorDecription(LabelUtil.getKey("e0204.im.response", locale));
                return result;
            }
            InfoCell infoCell = ExchangeClient.getInforHlr(isdn);
            if (infoCell == null) {
                result.setErrorDecription("Can not find info hlr");
                return result;
            }
            if (infoCell.getMscId() != null) {
                InfoCell infoMsc = ExchangeClient.getMscInfor(isdn, infoCell.getMscId());
                if (infoMsc == null) {
                    result.setErrorDecription("Error Get MSC id");
                    return result;
                }
                groupId = infoMsc.getType();
                cellInfo.append("\nCell: ").append(infoMsc.getCellId()).append("/").append(infoMsc.getCellCode());

                String cellId = infoMsc.getCellId();
                if ((cellId != null) && (!cellId.isEmpty())) {
                    String[] cellIdLst = cellId.split("-");
                    if (cellIdLst.length == 3) {
                        String idCell = hex2decimal(cellIdLst[2]) + "";
                        String idLocal = hex2decimal(cellIdLst[1]) + "";
                        String ci = idLocal + idCell;
                        this.logger.info("ci " + ci);
                        InfoCell infoCellBts = getInfoCell(raSession, ci);
                        if (infoCellBts != null) {
                            cellInfo.append("\nProvince: ").append(infoCellBts.getProvinceCode());
                        }
                    } else if ((cellIdLst.length == 1) && (cellId.length() > 8)) {
                        String idCell = hex2decimal(cellId.substring(cellId.length() - 4, cellId.length())) + "";
                        String idLocal = hex2decimal(cellId.substring(cellId.length() - 8, cellId.length() - 4)) + "";
                        String ci = idLocal + idCell;
                        this.logger.info("ci " + ci);
                        InfoCell infoCellBts = getInfoCell(raSession, ci);
                        if (infoCellBts != null) {
                            cellInfo.append("\nProvince: ").append(infoCellBts.getProvinceCode());
                        }
                    }
                }
                cellInfo.append("\nLocation: ");
            } else {
                result.setErrorDecription("Can not find MSC id");
                return result;
            }
            if (groupId == null) {
                result.setErrorDecription("Can not find MSC id");
                return result;
            }
            List<ComplaintType> lst = complaintBusiness.getListComplaintType(ccSession, groupId);
            if (lst != null && !lst.isEmpty()) {
                for (ComplaintType temp : lst) {
                    String content = (temp.getCompTemplate() == null ? "" : temp.getCompTemplate()) + cellInfo.toString();
                    temp.setCompTemplate(content.replaceAll("\n", "newline"));
                    temp.setName(temp.getName().substring(3));
                }
            } else {
                result.setErrorDecription(LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            result.setErrorCode(Constants.ERROR_CODE_0);
            result.setErrorDecription(LabelUtil.getKey("common.success", locale));
            result.setListComType(lst);
        } catch (Exception ex) {
            ex.printStackTrace();
            result.setErrorDecription(ex.getMessage());
        } finally {
            closeSessions(ccSession, raSession);
        }
        return result;
    }

    public WSRespone submitComplaintMyMetfone(HibernateHelper hibernateHelper, ComplaintInput complaintInput, String locale, String token) {
        WSRespone result = null;
        Session cmPosSession = null;
        Session cmPreSession = null;
        Session imPosSession = null;
        Session ccSession = null;
        boolean hasErr = false;
        String mess;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);

            if (complaintInput == null) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.complaintInput", locale));
                return result;
            }
            if (complaintInput.getErrorPhone() == null || complaintInput.getErrorPhone().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.errorPhone", locale));
                return result;
            }
            if (complaintInput.getComplainerPhone() == null || complaintInput.getComplainerPhone().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.complainerPhone", locale));
                return result;
            }
            if (complaintInput.getCompTypeId() == null || complaintInput.getCompTypeId() == 0) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.compType", locale));
                return result;
            }
            Date nowDate = new Date();
            if ("42262".equals(complaintInput.getCompTypeId().toString())) {
                complaintInput.setCompTypeId(42369l);
            }
            if (checkCompTypeNoProcess(ccSession, complaintInput.getCompTypeId())) {
                ComplainAgent comp = new ComplainAgent();
                comp.setServiceTypeId(2l);
                comp.setIsdn(complaintInput.getErrorPhone());
                comp.setCompLevelId(1000001l);/*CONSULT*/
                comp.setComplainId(new BaseSupplier().getSequence(ccSession, "seq_complain_agent"));
                comp.setComplainerPhone(complaintInput.getComplainerPhone());
                comp.setComplainerIsOther("0");
                comp.setReCompNumber(1L);
                comp.setAcceptUser(Constants.APP_MY_METFONE);
                comp.setAcceptDate(nowDate);

                comp.setAcceptTypeId(Constants.SOURCE_APP_MY_METFONE);
                comp.setAcceptSourceId(16l/*province*/);
                comp.setPriorityId(1L);
                comp.setCompContent(complaintInput.getCompContent().replaceAll("newline", "\n"));
                comp.setIsValid("1");
                comp.setDepId(Constants.CALL_CENTER_DEP_ID);
                comp.setStatus("0");
                comp.setReturnStatus("0");
                comp.setNumContactCust(0L);
                comp.setSendSMS("1");
                comp.setCompLevelId(1000000l);/*COMPLAINT*/
                comp.setCompTypeId(complaintInput.getCompTypeId());
                comp.setLastAcceptDate(nowDate);
                comp.setProDate(nowDate);
                comp.setResLimitDate(nowDate);
                comp.setEndDate(nowDate);
                Calendar cal = Calendar.getInstance();
                cal.setTime(nowDate);
                cal.add(Calendar.HOUR, 72);
                comp.setCustLimitDate(cal.getTime());

                if (complaintInput.getCompContent().contains("Province")) {
                    String temp = complaintInput.getCompContent().substring(complaintInput.getCompContent().indexOf("Province")).split("\n")[0];
                    temp = temp.replaceAll("Province", "");
                    temp = temp.replaceAll(":", "");
                    temp = temp.replaceAll(" ", "");
                    String sql = "select accept_source_id from Comp_Accept_Source where code = ? and isactive = 1";
                    Query query = ccSession.createSQLQuery(sql);
                    query.setParameter(0, temp);
                    List list = query.list();
                    if (list != null && !list.isEmpty()) {
                        comp.setAcceptSourceId(Long.parseLong(list.get(0).toString()));
                    }
                }
                SubMbPre subPre = new SubMbPreDAO().findByIsdn(cmPreSession, complaintInput.getErrorPhone());
                if (subPre == null) {
                    String sql = "from SubMbPos where isdn = ? and status = 2";
                    Query query = cmPosSession.createQuery(sql);
                    query.setParameter(0, complaintInput.getErrorPhone());
                    List<SubMbPos> listSubPos = query.list();
                    if (listSubPos != null && !listSubPos.isEmpty()) {
                        SubMbPos subPos = listSubPos.get(0);
                        comp.setSubId(subPos.getSubId());
                        comp.setContractId(subPos.getContractId());
                        com.viettel.bccs.cm.database.BO.Contract contract = new ContractDAO().findById(cmPosSession, subPos.getContractId());
                        if (contract != null) {
                            comp.setContractNo(contract.getContractNo());
                        }
                        Customer cusPos = new CustomerDAO().findById(ccSession, contract.getCustId());
                        if (cusPos != null) {
                            comp.setComplainerName(cusPos.getName());
                            comp.setCustName(cusPos.getName());
                            comp.setComplainerPassport(cusPos.getIdNo());
                        }
                        comp.setPackages(subPos.getProductCode());
                    } else {
                        result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("invalid.complain.errorPhone", locale));
                        return result;
                    }
                } else {
                    CustomerPre cusPre = new CustomerPreDAO().findByCustId(cmPreSession, subPre.getCustId() == null ? 0l : subPre.getCustId());
                    if (cusPre != null) {
                        comp.setComplainerName(cusPre.getName());
                        comp.setComplainerPassport(cusPre.getIdNo());
                        comp.setCustName(cusPre.getName());
                    }
                    comp.setSubId(subPre.getSubId());
                    comp.setPackages(subPre.getProductCode());
                }
                ccSession.save(comp);
            } else {
                Complain comp = new Complain();
                comp.setServiceTypeId(2l);
                comp.setIsdn(complaintInput.getErrorPhone());
                comp.setComplainId(new BaseSupplier().getSequence(ccSession, "seq_complain"));
                comp.setComplainerPhone(complaintInput.getComplainerPhone());
                comp.setComplainerIsOther("0");
                comp.setReCompNumber(1L);
                comp.setAcceptUser(Constants.APP_MY_METFONE);
                comp.setAcceptDate(nowDate);

                comp.setAcceptTypeId(Constants.SOURCE_APP_MY_METFONE);
                comp.setAcceptSourceId(16l/*province*/);
                comp.setPriorityId(1L);
                comp.setCompContent(complaintInput.getCompContent().replaceAll("newline", "\n"));
                comp.setIsValid("1");
                comp.setDepId(Constants.CALL_CENTER_DEP_ID);
                comp.setStatus("0");
                comp.setReturnStatus("0");
                comp.setNumContactCust(0L);
                comp.setSendSMS("1");
                comp.setCompLevelId(1000000l);/*COMPLAINT*/
                comp.setCompTypeId(complaintInput.getCompTypeId());
                comp.setLastAcceptDate(nowDate);
                comp.setProDate(nowDate);
                comp.setNumCusDelay(0L);
                comp.setResLimitDate(nowDate);
                comp.setEndDate(nowDate);
                comp.setTokenId(complaintInput.getTokenId());
                Calendar cal = Calendar.getInstance();
                cal.setTime(nowDate);
                cal.add(Calendar.HOUR, 72);
                comp.setCustLimitDate(cal.getTime());

                if (complaintInput.getCompContent().contains("Province")) {
                    String temp = complaintInput.getCompContent().substring(complaintInput.getCompContent().indexOf("Province")).split("\n")[0];
                    temp = temp.replaceAll("Province", "");
                    temp = temp.replaceAll(":", "");
                    temp = temp.replaceAll(" ", "");
                    String sql = "select accept_source_id from Comp_Accept_Source where code = ? and isactive = 1";
                    Query query = ccSession.createSQLQuery(sql);
                    query.setParameter(0, temp);
                    List list = query.list();
                    if (list != null && !list.isEmpty()) {
                        comp.setAcceptSourceId(Long.parseLong(list.get(0).toString()));
                    }
                }
                SubMbPre subPre = new SubMbPreDAO().findByIsdn(cmPreSession, complaintInput.getErrorPhone());
                if (subPre == null) {
                    String sql = "from SubMbPos where isdn = ? and status = 2";
                    Query query = cmPosSession.createQuery(sql);
                    query.setParameter(0, complaintInput.getErrorPhone());
                    List<SubMbPos> listSubPos = query.list();
                    if (listSubPos != null && !listSubPos.isEmpty()) {
                        SubMbPos subPos = listSubPos.get(0);
                        comp.setSubId(subPos.getSubId());
                        comp.setContractId(subPos.getContractId());
                        com.viettel.bccs.cm.database.BO.Contract contract = new ContractDAO().findById(cmPosSession, subPos.getContractId());
                        if (contract != null) {
                            comp.setContractNo(contract.getContractNo());
                        }
                        Customer cusPos = new CustomerDAO().findById(ccSession, contract.getCustId());
                        if (cusPos != null) {
                            comp.setComplainerName(cusPos.getName());
                            comp.setCustName(cusPos.getName());
                            comp.setComplainerPassport(cusPos.getIdNo());
                        }
                        comp.setPackages(subPos.getProductCode());
                    } else {
                        result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("invalid.complain.errorPhone", locale));
                        return result;
                    }
                } else {
                    CustomerPre cusPre = new CustomerPreDAO().findByCustId(cmPreSession, subPre.getCustId() == null ? 0l : subPre.getCustId());
                    if (cusPre != null) {
                        comp.setComplainerName(cusPre.getName());
                        comp.setComplainerPassport(cusPre.getIdNo());
                        comp.setCustName(cusPre.getName());
                    }
                    comp.setSubId(subPre.getSubId());
                    comp.setPackages(subPre.getProductCode());
                }
                ccSession.save(comp);
            }

            ccSession.flush();
            commitTransactions(ccSession);

            result = new WSRespone(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(ccSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            closeSessions(imPosSession);
            closeSessions(ccSession);
            closeSessions(cmPreSession);
            LogUtils.info(logger, "ComplaintController.submitComplaint:result=" + LogUtils.toJson(result));
        }
    }

    public ParamOut getComplaintHistory(HibernateHelper hibernateHelper, String locale, String isdn, Long complaintId, int rate) {
        Session ccSession = null;
        if (isdn == null || isdn.isEmpty()) {
            return new ParamOut(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.complainerPhone", locale));
        }
        try {
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            ParamOut result;
            List<ComplaintEx> list = new ComplaintSupplier().getComplaintHistory(ccSession, isdn, complaintId, rate);
            result = new ParamOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            result.setListComplaintHistory(list);
            if (list != null && !list.isEmpty()) {
                for (ComplaintEx temp : list) {
                    String content = temp.getCompContent();
                    if (content.contains("Customer input")) {
                        String accessNetworkType = temp.getCompContent().substring(temp.getCompContent().indexOf("Customer input")).split("\n")[0];
                        temp.setCompContent(accessNetworkType);
                    } else {
                        temp.setCompContent("");
                    }
                }
            }
            return result;
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            return new ParamOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
        } finally {
            closeSessions(ccSession);
        }
    }

    public ParamOut rateComplain(HibernateHelper hibernateHelper, String locale, String isdn, String complaintId, String rate) {
        Session ccSession = null;
        if (isdn == null || isdn.isEmpty()) {
            return new ParamOut(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.complainerPhone", locale));
        }
        if (complaintId == null || complaintId.isEmpty()) {
            return new ParamOut(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.complaintId", locale));
        }
        if (rate == null || rate.isEmpty()) {
            return new ParamOut(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.rate", locale));
        }
        boolean hasErr = false;
        try {
            String[] arrId = complaintId.split(";");
            String[] arrRate = rate.split(";");
            if (arrId.length != arrRate.length) {
                return new ParamOut(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.rate", locale));
            }
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            String sql = "from Complain where complainId = ? and isdn = ?";
            Query query = ccSession.createQuery(sql);
            for (int i = 0; i < arrId.length; i++) {
                if (arrId[i] != null && !arrId[i].isEmpty()
                        && arrRate[i] != null && !arrRate[i].isEmpty()) {
                    Long id = Long.parseLong(arrId[i]);
                    Long rateId = Long.parseLong(arrRate[i]);
                    query.setParameter(0, id);
                    query.setParameter(1, isdn);
                    List<Complain> list = query.list();
                    if (list != null && !list.isEmpty()) {
                        list.get(0).setRate(rateId);
                        ccSession.save(list.get(0));
                        ccSession.flush();
                    }
                }
            }
            ParamOut result = new ParamOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Exception ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            return new ParamOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(ccSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            } else {
                commitTransactions(ccSession);
            }
            closeSessions(ccSession);
        }
    }

    public ParamOut closeComplain(HibernateHelper hibernateHelper, String locale, String isdn, Long complaintId) {
        Session ccSession = null;
        if (isdn == null || isdn.isEmpty()) {
            return new ParamOut(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.complainerPhone", locale));
        }
        if (complaintId == null || complaintId.intValue() == 0) {
            return new ParamOut(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.complaintId", locale));
        }
        boolean hasErr = false;
        try {
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            String sql = "from Complain where complainId = ? and isdn = ?";
            Query query = ccSession.createQuery(sql);
            query.setParameter(0, complaintId);
            query.setParameter(1, isdn);
            List<Complain> list = query.list();
            if (list != null && !list.isEmpty() && !Constants.COMPLAIN_CLOSE_STATUS.equals(list.get(0).getStatus())) {
                list.get(0).setStatus(Constants.COMPLAIN_CLOSE_STATUS);
                ccSession.save(list.get(0));
                ccSession.flush();
                return new ParamOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            } else {
                return new ParamOut(Constants.ERROR_CODE_0, LabelUtil.getKey("complain.resolved", locale));
            }

        } catch (Exception ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            return new ParamOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(ccSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            } else {
                commitTransactions(ccSession);
            }
            closeSessions(ccSession);
        }
    }

    public ParamOut reopenComplain(HibernateHelper hibernateHelper, String locale, String isdn, Long complaintId, String content) {
        Session ccSession = null;
        if (isdn == null || isdn.isEmpty()) {
            return new ParamOut(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.complainerPhone", locale));
        }
        if (complaintId == null || complaintId.intValue() == 0) {
            return new ParamOut(Constants.ERROR_CODE_1, LabelUtil.getKey("required.complain.complaintId", locale));
        }
        boolean hasErr = false;
        try {
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            String sql = "from Complain where complainId = ? and isdn = ?";
            Query query = ccSession.createQuery(sql);
            query.setParameter(0, complaintId);
            query.setParameter(1, isdn);
            List<Complain> list = query.list();
            if (list != null && !list.isEmpty() && !Constants.COMPLAIN_CLOSE_STATUS.equals(list.get(0).getStatus()) && !Constants.COMPLAIN_DELETED_STATUS.equals(list.get(0).getStatus())) {
                Date nowDate = new Date();
                Complain comp = new Complain();
                comp.setServiceTypeId(2l);
                comp.setIsdn(list.get(0).getIsdn());
                comp.setComplainId(new BaseSupplier().getSequence(ccSession, "seq_complain"));
                comp.setComplainerPhone(list.get(0).getComplainerPhone());
                comp.setComplainerIsOther("0");
                comp.setReCompNumber(1L);
                comp.setAcceptUser(Constants.APP_MY_METFONE);
                comp.setAcceptDate(nowDate);
                comp.setCompTypeId(list.get(0).getCompTypeId());
                comp.setAcceptTypeId(Constants.SOURCE_APP_MY_METFONE);
                comp.setAcceptSourceId(16l/*province*/);
                comp.setPriorityId(1L);
                comp.setCompContent(content == null || content.isEmpty() ? list.get(0).getCompContent() : content);
                comp.setIsValid("1");
                comp.setDepId(Constants.CALL_CENTER_DEP_ID);
                comp.setStatus("0");
                comp.setReturnStatus("0");
                comp.setNumContactCust(0L);
                comp.setSendSMS("1");
                comp.setCompLevelId(1000000l);/*COMPLAINT*/
                comp.setLastAcceptDate(nowDate);
                comp.setProDate(nowDate);
                comp.setNumCusDelay(0L);
                comp.setResLimitDate(nowDate);
                comp.setEndDate(nowDate);
                comp.setEndUser(Constants.APP_MY_METFONE);
                comp.setTokenId(list.get(0).getTokenId());
                Calendar cal = Calendar.getInstance();
                cal.setTime(nowDate);
                cal.add(Calendar.HOUR, 72);
                comp.setCustLimitDate(cal.getTime());

                comp.setSubId(list.get(0).getSubId());
                comp.setContractId(list.get(0).getContractId());
                comp.setComplainerName(list.get(0).getComplainerName());
                comp.setCustName(list.get(0).getCustName());
                comp.setComplainerPassport(list.get(0).getComplainerPhone());
                comp.setPackages(list.get(0).getPackages());

                list.get(0).setStatus(Constants.COMPLAIN_DELETED_STATUS);
                ccSession.save(list.get(0));
                ccSession.save(comp);
                ccSession.flush();
            } else {
                return new ParamOut(Constants.ERROR_CODE_0, LabelUtil.getKey("complain.resolved", locale));
            }
            ParamOut result = new ParamOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Exception ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            return new ParamOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(ccSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            } else {
                commitTransactions(ccSession);
            }
            closeSessions(ccSession);
        }
    }

    public static int hex2decimal(String s) {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16 * val + d;
        }
        return val;
    }

    InfoCell getInfoCell(Session sessionIm, String ci) {
        String sql = "select BTS_CODE,PROVINCE_CODE from data_center.v_all_sites_vtc where lac_ci =?";
        try {
            Query query = sessionIm.createSQLQuery(sql);
            query.setParameter(0, ci);
            List<Object[]> lst = query.list();
            Iterator i$ = lst.iterator();
            if (i$.hasNext()) {
                Object[] obj = (Object[]) i$.next();
                InfoCell result = new InfoCell();
                result.setBtsCode(obj[0].toString());
                result.setProvinceCode(obj[1].toString());

                return result;
            }
        } catch (Exception ex) {
            this.logger.error("Error getDealerCode", ex);
        }
        return null;
    }

    public boolean checkCompTypeNoProcess(Session sessionCc, Long compTypeId) {
        try {
            String strSQLs = "select e.comp_Type_Id compTypeId, e.name name ";
            strSQLs += " from Comp_Type e, Comp_Group g ";
            strSQLs += " where g.is_Process =0 and e.group_Id = g.group_Id ";
            strSQLs += " and e.comp_Type_Id = ? ";

            Query query = sessionCc.createSQLQuery(strSQLs);
            query.setParameter(0, compTypeId);
            List list = query.list();

            if (list != null && !list.isEmpty()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
}
