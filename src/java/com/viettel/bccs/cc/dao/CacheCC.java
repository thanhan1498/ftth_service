/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.dao;

import com.viettel.bccs.cm.database.BO.ApParam;
import com.viettel.bccs.cm.model.KpiDeadlineTask;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author duyetdk
 */
public class CacheCC {
    
    private static final Object LOCK = new Object();
    
    /**
     * 
     * @param paramType
     * @param cmPosSession
     * @return 
     */
    public static List<ApParam> getApParamByType(String paramType, Session cmPosSession) {
        String sql = " select param_type paramType, "
                + "           param_code paramCode,"
                + "           param_value paramValue"
                + " from cm_pos2.ap_param where param_type = ? and status = 1 ";
        Query query = cmPosSession.createSQLQuery(sql)
                .addScalar("paramType", Hibernate.STRING)
                .addScalar("paramCode", Hibernate.STRING)
                .addScalar("paramValue", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ApParam.class));
        query.setParameter(0, paramType);
        return query.list();
    }
    
    /**
     * 
     * @param cmPosSession
     * @return 
     */
    public static List<ApParam> getSubTypeVip(Session cmPosSession) {
        List<ApParam> apParamSubTypeVip = null;
        if (apParamSubTypeVip == null) {
            synchronized (LOCK) {
                apParamSubTypeVip = getApParamByType("DEFINE_VIP_SUB_TYPE", cmPosSession);
            }
        }
        return apParamSubTypeVip;
    }
    
    public static List<ApParam> getListPackageVip(Session cmPosSession) {
        List<ApParam> listPackageVip = null;
        if (listPackageVip == null) {
            synchronized (LOCK) {
                listPackageVip = getApParamByType("PACKAGE_VIP", cmPosSession);
            }
        }
        return listPackageVip;
    }
    
    /**
     * 
     * @param cmPosSession
     * @return 
     */
    public static HashMap<String, Long> getListKpiDeadlineTask(Session cmPosSession) {
        HashMap<String, Long> listKpiDeadlineTask = null;
        if (listKpiDeadlineTask == null) {
            synchronized (LOCK) {
                String sql = " SELECT JOB_TYPE jobType,"
                        + " service_type serviceType,"
                        + " NVL(infra_Type,'AON') infraType,"
                        + " is_vip isVip,"
                        + " OVERTIME overTime"
                        + " FROM CM_POS2.KPI_DEADLINE_TASK "
                        + " WHERE status = 1";
                Query query = cmPosSession.createSQLQuery(sql)
                        .addScalar("jobType", Hibernate.STRING)
                        .addScalar("serviceType", Hibernate.STRING)
                        .addScalar("infraType", Hibernate.STRING)
                        .addScalar("isVip", Hibernate.LONG)
                        .addScalar("overTime", Hibernate.LONG)
                        .setResultTransformer(Transformers.aliasToBean(KpiDeadlineTask.class));
                List<KpiDeadlineTask> list = query.list();
                if (list != null && !list.isEmpty()) {
                    listKpiDeadlineTask = new HashMap<String, Long>();
                    for (KpiDeadlineTask deadline : list) {
                        listKpiDeadlineTask.put(
                                deadline.getJobType()
                                + "_" + deadline.getServiceType()
                                + "_" + deadline.getInfraType()
                                + "_" + deadline.getIsVip().toString(),
                                deadline.getOverTime());
                    }
                }
            }
        }
        return listKpiDeadlineTask;
    }
}
