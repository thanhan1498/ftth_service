/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.dao;

import com.viettel.bccs.cc.model.GetSubscriptionInfoForm;
import com.viettel.bccs.cm.database.BO.ApParam;
import com.viettel.brcd.ws.common.WebServiceClient;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import com.viettel.brcd.ws.bccsgw.model.Param;
import com.viettel.brcd.ws.model.output.GetSubscriptionInfoResponse;

/**
 *
 * @author duyetdk
 */
public class CompGetDataDAO {
    
    /**
     * 
     * @param cmPosSession
     * @param subType
     * @param productCode
     * @return 
     */
    public boolean isAccountVip(Session cmPosSession, String subType, String productCode) {
        if (subType != null && productCode != null) {
            /*Get define sub type is vip*/
            List<ApParam> subTypeVip = CacheCC.getSubTypeVip(cmPosSession);
            /*Get package vip*/
            List<ApParam> packageVip = CacheCC.getListPackageVip(cmPosSession);
            /*Kiem tra sub_type co phai vip hay khong*/
            if (subTypeVip != null && !subTypeVip.isEmpty()) {
                for (ApParam ap : subTypeVip) {
                    if (ap.getParamValue().trim().equalsIgnoreCase(subType.trim())) {
                        return true;
                    }
                }
            }
            if (packageVip != null && !packageVip.isEmpty()) {
                for (ApParam param : packageVip) {
                    if (param.getParamCode().trim().equalsIgnoreCase(productCode.trim())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * 
     * @param sessionCc
     * @param subId
     * @return
     * @throws Exception 
     */
    public GetSubscriptionInfoForm getStaffManageConnector(Session sessionCc, Long subId) throws Exception {
        /*Get account from subId*/
        String sql = "SELECT account, NVL(infra_type, 'CCN') FROM CM_POS2.sub_adsl_ll s WHERE sub_id = ? ";
        GetSubscriptionInfoForm info = null;
        Query query = sessionCc.createSQLQuery(sql);
        query.setParameter(0, subId);
        List lis = query.list();
        if (lis != null && !lis.isEmpty()) {
            Object[] arr = (Object[])lis.get(0);
            String account = arr[0].toString();
            String infraType = arr[1].toString();
            /*get infrastructure from nims*/
            WebServiceClient wsc = new WebServiceClient();
            List<Param> lstParam = new ArrayList<Param>();
            Param param = new Param();
            param.setName("accountIsdn");
            param.setValue(account);
            lstParam.add(param);
            GetSubscriptionInfoResponse infoNims = null;
            infoNims = (GetSubscriptionInfoResponse) wsc.sendRequestWithParam(lstParam, "getSubscriptionInfo", GetSubscriptionInfoResponse.class);
            
            sql = "SELECT sh.shop_id as shopId, sh.name shopName, sh.shop_code shopCode, st.staff_code staffCode, st.name staffName"
                    + "  FROM cm_pos2.technical_connector c,  "
                    + "       cm_pos2.shop sh, "
                    + "       cm_pos2.staff st "
                    + " WHERE st.shop_id = sh.shop_id "
                    + "   AND st.staff_id = c.staff_id "
                    + "   AND st.status = 1 "
                    + "   AND sh.status = 1"
                    + "   AND c.status = 1 ";
            if (infoNims != null) {
                if (infoNims.getReturn() == null) {
                    sql += " AND c.connector_id = ? AND c.infra_type= ? ";
                    String sql1 = " SELECT cable_box_id FROM CM_POS2.sub_deployment WHERE sub_id = ? AND status =1 ";
                    query = sessionCc.createSQLQuery(sql1);
                    query.setParameter(0, subId);
                    lis = query.list();
                    if (lis != null && !lis.isEmpty()) {
                        Long cableBoxId = lis.get(0) != null ? Long.parseLong(lis.get(0).toString()) : null;
                        if (cableBoxId != null) {
                            query = sessionCc.createSQLQuery(sql)
                                    .addScalar("shopId", Hibernate.LONG)
                                    .addScalar("shopName", Hibernate.STRING)
                                    .addScalar("shopCode", Hibernate.STRING)
                                    .addScalar("staffCode", Hibernate.STRING)
                                    .addScalar("staffName", Hibernate.STRING)
                                    .setResultTransformer(Transformers.aliasToBean(GetSubscriptionInfoForm.class));
                            query.setParameter(0, cableBoxId);
                            query.setParameter(1, infraType);
                            List<GetSubscriptionInfoForm> result = query.list();
                            if (result != null && !result.isEmpty()) {
                                info = result.get(0);
                            }
                        }
                    }
                } else {
                    String connectorCode = null;
                    if ("GPON_OLT".equalsIgnoreCase(infoNims.getReturn().getNetworkClass())) {
                        connectorCode = infoNims.getReturn().getNodeTbCode();
                    } else if ("SWITCH".equalsIgnoreCase(infoNims.getReturn().getNetworkClass())) {
                        connectorCode = infoNims.getReturn().getOdfCode();
                    } else {
                        /*DSLAM*/
                        connectorCode = infoNims.getReturn().getBoxCode();
                    }
                    if (connectorCode != null) {
                        sql += " AND UPPER(c.connector_code) = ? ";
                        query = sessionCc.createSQLQuery(sql)
                                .addScalar("shopId", Hibernate.LONG)
                                .addScalar("shopName", Hibernate.STRING)
                                .addScalar("shopCode", Hibernate.STRING)
                                .addScalar("staffCode", Hibernate.STRING)
                                .addScalar("staffName", Hibernate.STRING)
                                .setResultTransformer(Transformers.aliasToBean(GetSubscriptionInfoForm.class));
                        query.setParameter(0, connectorCode.toUpperCase());
                        List<GetSubscriptionInfoForm> result = query.list();
                        if (result != null && !result.isEmpty()) {
                            info = result.get(0);
                        }
                    }
                }
            }
        }
        return info;
    }
}
