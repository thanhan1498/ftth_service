/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author duyetdk
 */

@Entity
@Table(name = "ASSIGN_MANAGEMENT")
public class AssignManagement implements Serializable {
    
    @Id
    @Column(name = "ASSIGN_ID", length = 20, precision = 20, scale = 0)
    private Long assignId;
    @Column(name = "COMPLAIN_ID", length = 20, precision = 20, scale = 0)
    private Long complainId;
    @Column(name = "ASSIGN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date assignDate;
    @Column(name = "EMPLOYEE_ID", length = 10, precision = 10, scale = 0)
    private Long employeeId;
    @Column(name = "DESCRIPTION", length = 4000)
    private String description;
    @Column(name = "ASSIGN_TYPE", length = 1, precision = 1, scale = 0)
    private Long assignType;
    @Column(name = "ACTIVE", length = 1, precision = 1, scale = 0)
    private Long active;
    @Column(name = "MANAGER_ID", length = 10, precision = 10, scale = 0)
    private Long managerId;
    @Column(name = "DEP_SEND_ID", length = 10, precision = 10, scale = 0)
    private Long depSendId;
    @Column(name = "DEP_RECEIVE_ID", length = 10, precision = 10, scale = 0)
    private Long depReceiveId;
    @Column(name = "AGREE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date agreeDate;
    @Column(name = "PROCESS_TIME", length = 8, precision = 10, scale = 2)
    private Long processTime;

    public Long getAssignId() {
        return assignId;
    }

    public void setAssignId(Long assignId) {
        this.assignId = assignId;
    }

    public Long getComplainId() {
        return complainId;
    }

    public void setComplainId(Long complainId) {
        this.complainId = complainId;
    }

    public Date getAssignDate() {
        return assignDate;
    }

    public void setAssignDate(Date assignDate) {
        this.assignDate = assignDate;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getAssignType() {
        return assignType;
    }

    public void setAssignType(Long assignType) {
        this.assignType = assignType;
    }

    public Long getActive() {
        return active;
    }

    public void setActive(Long active) {
        this.active = active;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public Long getDepSendId() {
        return depSendId;
    }

    public void setDepSendId(Long depSendId) {
        this.depSendId = depSendId;
    }

    public Long getDepReceiveId() {
        return depReceiveId;
    }

    public void setDepReceiveId(Long depReceiveId) {
        this.depReceiveId = depReceiveId;
    }

    public Date getAgreeDate() {
        return agreeDate;
    }

    public void setAgreeDate(Date agreeDate) {
        this.agreeDate = agreeDate;
    }

    public Long getProcessTime() {
        return processTime;
    }

    public void setProcessTime(Long processTime) {
        this.processTime = processTime;
    }
    
}
