/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.model;

/**
 *
 * @author duyetdk
 */
public class CompDepartmentGroup implements java.io.Serializable {

    private Long depId;
    private String code;
    private String name;
    private String staffCode;
    private String staffName;
    private Long stationId;

    public CompDepartmentGroup() {
    }

    public CompDepartmentGroup(Long depId, String code, String name, String staffCode, String staffName, Long stationId) {
        this.depId = depId;
        this.code = code;
        this.name = name;
        this.staffCode = staffCode;
        this.staffName = staffName;
        this.stationId = stationId;
    }

    public Long getDepId() {
        return depId;
    }

    public void setDepId(Long depId) {
        this.depId = depId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the staffCode
     */
    public String getStaffCode() {
        return staffCode;
    }

    /**
     * @param staffCode the staffCode to set
     */
    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    /**
     * @return the staffName
     */
    public String getStaffName() {
        return staffName;
    }

    /**
     * @param staffName the staffName to set
     */
    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    /**
     * 
     * @return stationId
     */
    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
}