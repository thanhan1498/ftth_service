/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author duyetdk
 */

@Entity
@Table(name = "COMP_PROCESS")
public class CompProcess implements Serializable {

    @Id
    @Column(name = "COMP_PRO_ID", length = 20, precision = 20, scale = 0)
    private Long compProId;
     @Column(name = "COMPLAIN_ID", length = 20, precision = 20, scale = 0)
    private Long complainId;
     @Column(name = "USERNAME", length = 50)
    private String userName;
    @Column(name = "FW_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fwDate;
    @Column(name = "PRO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date proDate;
    @Column(name = "LIMIT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date limitDate;
    @Column(name = "RESULT_CONTENT", length = 2000)
    private String resultContent;
    @Column(name = "DESCRIPTION", length = 200)
    private String description;
    @Column(name = "MEMO", length = 4000)
    private String memo;
    @Column(name = "DEP_STATUS", length = 1)
    private String depStatus;
    @Column(name = "DEP_ID", length = 10, precision = 10, scale = 0)
    private Long depId;
    @Column(name = "STATUS", length = 1)
    private String status;

    public Long getCompProId() {
        return compProId;
    }

    public void setCompProId(Long compProId) {
        this.compProId = compProId;
    }

    public Long getComplainId() {
        return complainId;
    }

    public void setComplainId(Long complainId) {
        this.complainId = complainId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getFwDate() {
        return fwDate;
    }

    public void setFwDate(Date fwDate) {
        this.fwDate = fwDate;
    }

    public Date getProDate() {
        return proDate;
    }

    public void setProDate(Date proDate) {
        this.proDate = proDate;
    }

    public Date getLimitDate() {
        return limitDate;
    }

    public void setLimitDate(Date limitDate) {
        this.limitDate = limitDate;
    }

    public String getResultContent() {
        return resultContent;
    }

    public void setResultContent(String resultContent) {
        this.resultContent = resultContent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getDepStatus() {
        return depStatus;
    }

    public void setDepStatus(String depStatus) {
        this.depStatus = depStatus;
    }

    public Long getDepId() {
        return depId;
    }

    public void setDepId(Long depId) {
        this.depId = depId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
}
