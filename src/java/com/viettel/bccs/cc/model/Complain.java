/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author duyetdk
 */
@Entity
@Table(name = "COMPLAIN")
public class Complain implements Serializable {

    @Id
    @Column(name = "COMPLAIN_ID", length = 20, precision = 20, scale = 0)
    private Long complainId;
    @Column(name = "SERVICE_TYPE_ID", length = 10, precision = 10, scale = 0)
    private Long serviceTypeId;
    @Column(name = "ISDN", length = 100)
    private String isdn;
    @Column(name = "SUB_ID", length = 11, precision = 11, scale = 0)
    private Long subId;
    @Column(name = "CONTRACT_ID", length = 10, precision = 10, scale = 0)
    private Long contractId;
    @Column(name = "COMPLAINER_NAME", length = 200)
    private String complainerName;
    @Column(name = "COMPLAINER_PHONE", length = 50)
    private String complainerPhone;
    @Column(name = "COMPLAINER_ADDRESS", length = 150)
    private String complainerAddress;
    @Column(name = "COMPLAINER_PASSPORT", length = 20)
    private String complainerPassport;
    @Column(name = "COMPLAINER_IS_OTHER", length = 1)
    private String complainerIsOther;
    @Column(name = "RE_COMP_NUMBER", length = 2, precision = 2, scale = 0)
    private Long reCompNumber;
    @Column(name = "ACCEPT_USER", length = 50)
    private String acceptUser;
    @Column(name = "ACCEPT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date acceptDate;
    @Column(name = "COMP_TYPE_ID", length = 10, precision = 10, scale = 0)
    private Long compTypeId;
    @Column(name = "ACCEPT_TYPE_ID", length = 10, precision = 10, scale = 0)
    private Long acceptTypeId;
    @Column(name = "ACCEPT_SOURCE_ID", length = 10, precision = 10, scale = 0)
    private Long acceptSourceId;
    @Column(name = "PRIORITY_ID", length = 10, precision = 10, scale = 0)
    private Long priorityId;
    @Column(name = "COMP_CONTENT", length = 2000)
    private String compContent;
    @Column(name = "PRE_RESULT", length = 2000)
    private String preResult;
    @Column(name = "ATTACH_DOCUMENT", length = 100)
    private String attachDocument;
    @Column(name = "IS_VALID", length = 1)
    private String isValid;
    @Column(name = "CUST_LIMIT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date custLimitDate;
    @Column(name = "RESULT_CONTENT", length = 2000)
    private String resultContent;
    @Column(name = "DEP_ID", length = 10, precision = 10, scale = 0)
    private Long depId;
    @Column(name = "EMP_ID", length = 10, precision = 10, scale = 0)
    private Long empId;
    @Column(name = "DESCRIPTION", length = 2000)
    private String description;
    @Column(name = "PRO_LIMIT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date proLimitDate;
    @Column(name = "ONTIME", length = 1, precision = 1, scale = 0)
    private Long ontime;
    @Column(name = "STATUS", length = 1)
    private String status;
    @Column(name = "IM_PROCESS", length = 1, precision = 1, scale = 0)
    private Long imProcess;
    @Column(name = "RESULT_ID", length = 10, precision = 10, scale = 0)
    private Long resultId;
    @Column(name = "REASON_ID", length = 10, precision = 10, scale = 0)
    private Long reasonId;
    @Column(name = "END_USER", length = 50)
    private String endUser;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "REDUCTION_MONEY", length = 10)
    private String reductionMoney;
    @Column(name = "CUST_NAME", length = 240)
    private String custName;
    @Column(name = "COMPLAINER_EMAIL", length = 30)
    private String complainerEmail;
    @Column(name = "RESULT_DOCUMENT_NO", length = 2000)
    private String resultDocumentNo;
    @Column(name = "COMP_LEVEL_ID", length = 10, precision = 10, scale = 0)
    private Long compLevelId;
    @Column(name = "RISE", length = 1)
    private String rise;
    @Column(name = "RES_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date resDate;           //Ngay phan hoi khach hang
    @Column(name = "RES_LIMIT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date resLimitDate;      //Ngay hen phan hoi khach hang
    @Column(name = "SAT_LEVEL_ID", length = 10, precision = 10, scale = 0)
    private Long satLevelId;        //Muc do hai long khach hang
    @Column(name = "RETURN_STATUS", length = 1)
    private String returnStatus;    //Tra lai khieu nai
    @Column(name = "DUPLICATE_ID", length = 10, precision = 10, scale = 0)
    private Long duplicateId;       //Ma trung
    @Column(name = "NOTE", length = 1)
    private String note;            // Ghi chu
    @Column(name = "DISTRICT", length = 15)
    private String district;        //Quan,huyen
    @Column(name = "PRECINCT", length = 15)
    private String precinct;        //Phuong,xa
    @Column(name = "COMP_CAUSE_ID", length = 10, precision = 10, scale = 0)
    private Long compCauseId;       //nguyen nhan gay loi
    @Column(name = "NUM_CONTACT_CUST", length = 2, precision = 2, scale = 0)
    private Long numContactCust;    //So lan lien lac voi khach hang
    @Column(name = "RETURN_REASON", length = 1)
    private String returnReason;    //Ly do tra lai
    @Column(name = "COMP_CHANNEL_ID", length = 10, precision = 10, scale = 0)
    private Long compChannelId;     //Kenh tiep nhan
    @Column(name = "NUM_CONTRACT", length = 10, precision = 10, scale = 0)
    private Long numContract;       //So hop dong
    @Column(name = "HAPPYCALL_CONTENT", length = 1000)
    private String happyCallContent;//noi dung happyCall
    @Column(name = "CUSTOMER_TEXT", length = 2000)
    private String customerText;    //Ghi chu DTV
    @Column(name = "SEND_SMS", length = 1)
    private String sendSMS;         //Gui SMS khi phoi hop phong ban
    @Column(name = "PROCESSING_USER", length = 50)
    private String processingUser; //User dang xu ly khieu nai
    @Column(name = "LOCATION_NAME", length = 500)
    private String locationName;    //Ten dia chi zone  
    /*Homephone*/
    @Column(name = "IMSI", length = 50)
    private String imsi;            //IMSI
    @Column(name = "SERIAL", length = 50)
    private String serial;          //SERIAL
    @Column(name = "PROCESSING_NOTE", length = 2000)
    private String processingNote; //Ghi chu qua trinh xu ly
    @Column(name = "LAST_ACCEPT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastAcceptDate;
    @Column(name = "LOCK_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lockDate;
    @Column(name = "COMP_PRODUCT_ID", length = 10, precision = 10, scale = 0)
    private Long compProductId;
    @Column(name = "DSLAM_DLU_ID", length = 10, precision = 10, scale = 0)
    private Long dslamId;
    @Column(name = "BOARD_ID", length = 10, precision = 10, scale = 0)
    private Long boardId;
    @Column(name = "CABLE_ID", length = 10, precision = 10, scale = 0)
    private Long cableId;
    @Column(name = "BLADE_A", length = 10, precision = 10, scale = 0)
    private Long bladeA;
    @Column(name = "BLADE_B", length = 10, precision = 10, scale = 0)
    private Long bladeB;
    @Column(name = "PORT", length = 10, precision = 10, scale = 0)
    private Long port;
    @Column(name = "CABLE_LENGTH", length = 10, precision = 10, scale = 0)
    private Long cableLength;
    @Column(name = "CONTRACT_NO", length = 200)
    private String contractNo;
    @Column(name = "NUMB_CABLE_USED", length = 5, precision = 5, scale = 0)
    private Long numbCableUsed;
    @Column(name = "ADDRESS_CODE", length = 50)
    private String addressCode;
    @Column(name = "PACKAGES", length = 100)
    private String packages;
    @Column(name = "BREAK_DOWN_TYPE", length = 2)
    private String breakDownType;
    @Column(name = "ACT_STATUS", length = 3)
    private String actStatus;
    @Column(name = "PROCESS_TYPE_ID", length = 10, precision = 10, scale = 0)
    private Long processTypeId;
    @Column(name = "LIMIT_CUST_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date limitCustDate;
    @Column(name = "DEP_SEND", length = 10, precision = 10, scale = 0)
    private Long depSend;
    @Column(name = "PRO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date proDate;
    @Column(name = "PROCESS_RESULT_CONTENT", length = 3000)
    private String processResultContent;
    @Column(name = "STATE_SYS_ERROR", length = 10, precision = 10, scale = 0)
    private Long stateSysError;
    @Column(name = "DEP_KPI_ID", length = 10, precision = 10, scale = 0)
    private Long depKpiId;
    @Column(name = "COMP_TYPE_KPI_ID", length = 10, precision = 10, scale = 0)
    private Long compTypeKpiId; //loai khieu nai kpi, don vi chiu trach nhiem
    @Column(name = "COMP_CAUSE_KPI_ID", length = 10, precision = 10, scale = 0)
    private Long compCauseKpiId; //nguyen nhan loi kpi
    @Column(name = "DEP_ACCEPT", length = 10, precision = 10, scale = 0)
    private Long depAccept;
    @Column(name = "ASSIGN_TYPE", length = 1, precision = 1, scale = 0)
    private Long assignType;
    @Column(name = "USER_ASSIGN", length = 100)
    private String userAssign;
    @Column(name = "USER_ASSIGNED", length = 100)
    private String userAssigned;
    @Column(name = "DELAY_FROM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date delayFrom;
    @Column(name = "DELAY_TO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date delayTo;
    @Column(name = "DELAY_REASON", length = 2000)
    private String delayReason;
    @Column(name = "NUM_CUS_DELAY", length = 3, precision = 3, scale = 0)
    private Long NumCusDelay;
    @Column(name = "REQ_TEAM_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reqTeamTime;
    @Column(name = "token_id", length = 250)
    private String tokenId;
    @Column(name = "rate", length = 1, precision = 1, scale = 0)
    private Long rate;

    public Complain() {
    }

    public Long getComplainId() {
        return complainId;
    }

    public void setComplainId(Long complainId) {
        this.complainId = complainId;
    }

    public Long getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(Long serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public String getComplainerName() {
        return complainerName;
    }

    public void setComplainerName(String complainerName) {
        this.complainerName = complainerName;
    }

    public String getComplainerPhone() {
        return complainerPhone;
    }

    public void setComplainerPhone(String complainerPhone) {
        this.complainerPhone = complainerPhone;
    }

    public String getComplainerAddress() {
        return complainerAddress;
    }

    public void setComplainerAddress(String complainerAddress) {
        this.complainerAddress = complainerAddress;
    }

    public String getComplainerPassport() {
        return complainerPassport;
    }

    public void setComplainerPassport(String complainerPassport) {
        this.complainerPassport = complainerPassport;
    }

    public String getComplainerIsOther() {
        return complainerIsOther;
    }

    public void setComplainerIsOther(String complainerIsOther) {
        this.complainerIsOther = complainerIsOther;
    }

    public Long getReCompNumber() {
        return reCompNumber;
    }

    public void setReCompNumber(Long reCompNumber) {
        this.reCompNumber = reCompNumber;
    }

    public String getAcceptUser() {
        return acceptUser;
    }

    public void setAcceptUser(String acceptUser) {
        this.acceptUser = acceptUser;
    }

    public Date getAcceptDate() {
        return acceptDate;
    }

    public void setAcceptDate(Date acceptDate) {
        this.acceptDate = acceptDate;
    }

    public Long getCompTypeId() {
        return compTypeId;
    }

    public void setCompTypeId(Long compTypeId) {
        this.compTypeId = compTypeId;
    }

    public Long getAcceptTypeId() {
        return acceptTypeId;
    }

    public void setAcceptTypeId(Long acceptTypeId) {
        this.acceptTypeId = acceptTypeId;
    }

    public Long getAcceptSourceId() {
        return acceptSourceId;
    }

    public void setAcceptSourceId(Long acceptSourceId) {
        this.acceptSourceId = acceptSourceId;
    }

    public Long getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(Long priorityId) {
        this.priorityId = priorityId;
    }

    public String getCompContent() {
        return compContent;
    }

    public void setCompContent(String compContent) {
        this.compContent = compContent;
    }

    public String getPreResult() {
        return preResult;
    }

    public void setPreResult(String preResult) {
        this.preResult = preResult;
    }

    public String getAttachDocument() {
        return attachDocument;
    }

    public void setAttachDocument(String attachDocument) {
        this.attachDocument = attachDocument;
    }

    public String getIsValid() {
        return isValid;
    }

    public void setIsValid(String isValid) {
        this.isValid = isValid;
    }

    public Date getCustLimitDate() {
        return custLimitDate;
    }

    public void setCustLimitDate(Date custLimitDate) {
        this.custLimitDate = custLimitDate;
    }

    public String getResultContent() {
        return resultContent;
    }

    public void setResultContent(String resultContent) {
        this.resultContent = resultContent;
    }

    public Long getDepId() {
        return depId;
    }

    public void setDepId(Long depId) {
        this.depId = depId;
    }

    public Long getEmpId() {
        return empId;
    }

    public void setEmpId(Long empId) {
        this.empId = empId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getProLimitDate() {
        return proLimitDate;
    }

    public void setProLimitDate(Date proLimitDate) {
        this.proLimitDate = proLimitDate;
    }

    public Long getOntime() {
        return ontime;
    }

    public void setOntime(Long ontime) {
        this.ontime = ontime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getImProcess() {
        return imProcess;
    }

    public void setImProcess(Long imProcess) {
        this.imProcess = imProcess;
    }

    public Long getResultId() {
        return resultId;
    }

    public void setResultId(Long resultId) {
        this.resultId = resultId;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public String getEndUser() {
        return endUser;
    }

    public void setEndUser(String endUser) {
        this.endUser = endUser;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getReductionMoney() {
        return reductionMoney;
    }

    public void setReductionMoney(String reductionMoney) {
        this.reductionMoney = reductionMoney;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getComplainerEmail() {
        return complainerEmail;
    }

    public void setComplainerEmail(String complainerEmail) {
        this.complainerEmail = complainerEmail;
    }

    public String getResultDocumentNo() {
        return resultDocumentNo;
    }

    public void setResultDocumentNo(String resultDocumentNo) {
        this.resultDocumentNo = resultDocumentNo;
    }

    public Long getCompLevelId() {
        return compLevelId;
    }

    public void setCompLevelId(Long compLevelId) {
        this.compLevelId = compLevelId;
    }

    public String getRise() {
        return rise;
    }

    public void setRise(String rise) {
        this.rise = rise;
    }

    public Date getResDate() {
        return resDate;
    }

    public void setResDate(Date resDate) {
        this.resDate = resDate;
    }

    public Date getResLimitDate() {
        return resLimitDate;
    }

    public void setResLimitDate(Date resLimitDate) {
        this.resLimitDate = resLimitDate;
    }

    public Long getSatLevelId() {
        return satLevelId;
    }

    public void setSatLevelId(Long satLevelId) {
        this.satLevelId = satLevelId;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public Long getDuplicateId() {
        return duplicateId;
    }

    public void setDuplicateId(Long duplicateId) {
        this.duplicateId = duplicateId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public Long getCompCauseId() {
        return compCauseId;
    }

    public void setCompCauseId(Long compCauseId) {
        this.compCauseId = compCauseId;
    }

    public Long getNumContactCust() {
        return numContactCust;
    }

    public void setNumContactCust(Long numContactCust) {
        this.numContactCust = numContactCust;
    }

    public String getReturnReason() {
        return returnReason;
    }

    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }

    public Long getCompChannelId() {
        return compChannelId;
    }

    public void setCompChannelId(Long compChannelId) {
        this.compChannelId = compChannelId;
    }

    public Long getNumContract() {
        return numContract;
    }

    public void setNumContract(Long numContract) {
        this.numContract = numContract;
    }

    public String getHappyCallContent() {
        return happyCallContent;
    }

    public void setHappyCallContent(String happyCallContent) {
        this.happyCallContent = happyCallContent;
    }

    public String getCustomerText() {
        return customerText;
    }

    public void setCustomerText(String customerText) {
        this.customerText = customerText;
    }

    public String getSendSMS() {
        return sendSMS;
    }

    public void setSendSMS(String sendSMS) {
        this.sendSMS = sendSMS;
    }

    public String getProcessingUser() {
        return processingUser;
    }

    public void setProcessingUser(String processingUser) {
        this.processingUser = processingUser;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getProcessingNote() {
        return processingNote;
    }

    public void setProcessingNote(String processingNote) {
        this.processingNote = processingNote;
    }

    public Date getLastAcceptDate() {
        return lastAcceptDate;
    }

    public void setLastAcceptDate(Date lastAcceptDate) {
        this.lastAcceptDate = lastAcceptDate;
    }

    public Date getLockDate() {
        return lockDate;
    }

    public void setLockDate(Date lockDate) {
        this.lockDate = lockDate;
    }

    public Long getCompProductId() {
        return compProductId;
    }

    public void setCompProductId(Long compProductId) {
        this.compProductId = compProductId;
    }

    public Long getDslamId() {
        return dslamId;
    }

    public void setDslamId(Long dslamId) {
        this.dslamId = dslamId;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Long getCableId() {
        return cableId;
    }

    public void setCableId(Long cableId) {
        this.cableId = cableId;
    }

    public Long getBladeA() {
        return bladeA;
    }

    public void setBladeA(Long bladeA) {
        this.bladeA = bladeA;
    }

    public Long getBladeB() {
        return bladeB;
    }

    public void setBladeB(Long bladeB) {
        this.bladeB = bladeB;
    }

    public Long getPort() {
        return port;
    }

    public void setPort(Long port) {
        this.port = port;
    }

    public Long getCableLength() {
        return cableLength;
    }

    public void setCableLength(Long cableLength) {
        this.cableLength = cableLength;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public Long getNumbCableUsed() {
        return numbCableUsed;
    }

    public void setNumbCableUsed(Long numbCableUsed) {
        this.numbCableUsed = numbCableUsed;
    }

    public String getAddressCode() {
        return addressCode;
    }

    public void setAddressCode(String addressCode) {
        this.addressCode = addressCode;
    }

    public String getPackages() {
        return packages;
    }

    public void setPackages(String packages) {
        this.packages = packages;
    }

    public String getBreakDownType() {
        return breakDownType;
    }

    public void setBreakDownType(String breakDownType) {
        this.breakDownType = breakDownType;
    }

    public String getActStatus() {
        return actStatus;
    }

    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    public Long getProcessTypeId() {
        return processTypeId;
    }

    public void setProcessTypeId(Long processTypeId) {
        this.processTypeId = processTypeId;
    }

    public Date getLimitCustDate() {
        return limitCustDate;
    }

    public void setLimitCustDate(Date limitCustDate) {
        this.limitCustDate = limitCustDate;
    }

    public Long getDepSend() {
        return depSend;
    }

    public void setDepSend(Long depSend) {
        this.depSend = depSend;
    }

    public Date getProDate() {
        return proDate;
    }

    public void setProDate(Date proDate) {
        this.proDate = proDate;
    }

    public String getProcessResultContent() {
        return processResultContent;
    }

    public void setProcessResultContent(String processResultContent) {
        this.processResultContent = processResultContent;
    }

    public Long getStateSysError() {
        return stateSysError;
    }

    public void setStateSysError(Long stateSysError) {
        this.stateSysError = stateSysError;
    }

    public Long getDepKpiId() {
        return depKpiId;
    }

    public void setDepKpiId(Long depKpiId) {
        this.depKpiId = depKpiId;
    }

    public Long getCompTypeKpiId() {
        return compTypeKpiId;
    }

    public void setCompTypeKpiId(Long compTypeKpiId) {
        this.compTypeKpiId = compTypeKpiId;
    }

    public Long getCompCauseKpiId() {
        return compCauseKpiId;
    }

    public void setCompCauseKpiId(Long compCauseKpiId) {
        this.compCauseKpiId = compCauseKpiId;
    }

    public Long getDepAccept() {
        return depAccept;
    }

    public void setDepAccept(Long depAccept) {
        this.depAccept = depAccept;
    }

    public Long getAssignType() {
        return assignType;
    }

    public void setAssignType(Long assignType) {
        this.assignType = assignType;
    }

    public String getUserAssign() {
        return userAssign;
    }

    public void setUserAssign(String userAssign) {
        this.userAssign = userAssign;
    }

    public String getUserAssigned() {
        return userAssigned;
    }

    public void setUserAssigned(String userAssigned) {
        this.userAssigned = userAssigned;
    }

    public Date getDelayFrom() {
        return delayFrom;
    }

    public void setDelayFrom(Date delayFrom) {
        this.delayFrom = delayFrom;
    }

    public Date getDelayTo() {
        return delayTo;
    }

    public void setDelayTo(Date delayTo) {
        this.delayTo = delayTo;
    }

    public String getDelayReason() {
        return delayReason;
    }

    public void setDelayReason(String delayReason) {
        this.delayReason = delayReason;
    }

    public Long getNumCusDelay() {
        return NumCusDelay;
    }

    public void setNumCusDelay(Long NumCusDelay) {
        this.NumCusDelay = NumCusDelay;
    }

    public Date getReqTeamTime() {
        return reqTeamTime;
    }

    public void setReqTeamTime(Date reqTeamTime) {
        this.reqTeamTime = reqTeamTime;
    }

    /**
     * @return the tokenId
     */
    public String getTokenId() {
        return tokenId;
    }

    /**
     * @param tokenId the tokenId to set
     */
    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    /**
     * @return the rate
     */
    public Long getRate() {
        return rate;
    }

    /**
     * @param rate the rate to set
     */
    public void setRate(Long rate) {
        this.rate = rate;
    }
}
