/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "COMPLAIN_AGENT")
public class ComplainAgent implements Serializable {

    @Id
    @Column(name = "COMPLAIN_ID", length = 20, precision = 20, scale = 0)
    private Long complainId;
    @Column(name = "DEP_ID", length = 10, precision = 10, scale = 0)
    private Long depId;
    @Column(name = "SERVICE_TYPE_ID", length = 10, precision = 10, scale = 0)
    private Long serviceTypeId;
    @Column(name = "ACCEPT_SOURCE_ID", length = 10, precision = 10, scale = 0)
    private Long acceptSourceId;
    @Column(name = "COMP_TYPE_ID", length = 10, precision = 10, scale = 0)
    private Long compTypeId;
    @Column(name = "PRIORITY_ID", length = 10, precision = 10, scale = 0)
    private Long priorityId;
    @Column(name = "ACCEPT_TYPE_ID", length = 10, precision = 10, scale = 0)
    private Long acceptTypeId;
    @Column(name = "EMP_ID", length = 10, precision = 10, scale = 0)
    private Long empId;
    @Column(name = "RESULT_ID", length = 10, precision = 10, scale = 0)
    private Long resultId;
    @Column(name = "REASON_ID", length = 10, precision = 10, scale = 0)
    private Long reasonId;
    @Column(name = "ISDN", length = 100)
    private String isdn;
    @Column(name = "SUB_ID", length = 11, precision = 11, scale = 0)
    private Long subId;
    @Column(name = "CONTRACT_ID", length = 10, precision = 10, scale = 0)
    private Long contractId;
    @Column(name = "COMPLAINER_NAME", length = 200)
    private String complainerName;
    @Column(name = "COMPLAINER_PHONE", length = 50)
    private String complainerPhone;
    @Column(name = "COMPLAINER_ADDRESS", length = 150)
    private String complainerAddress;
    @Column(name = "COMPLAINER_PASSPORT", length = 20)
    private String complainerPassport;
    @Column(name = "COMPLAINER_IS_OTHER", length = 1)
    private String complainerIsOther;
    @Column(name = "RE_COMP_NUMBER", length = 2, precision = 2, scale = 0)
    private Long reCompNumber;
    @Column(name = "ACCEPT_USER", length = 50)
    private String acceptUser;
    @Column(name = "ACCEPT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date acceptDate;
    @Column(name = "COMP_CONTENT", length = 2000)
    private String compContent;
    @Column(name = "PRE_RESULT", length = 2000)
    private String preResult;
    @Column(name = "ATTACH_DOCUMENT", length = 100)
    private String attachDocument;
    @Column(name = "IS_VALID", length = 1)
    private String isValid;
    @Column(name = "CUST_LIMIT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date custLimitDate;
    @Column(name = "RESULT_CONTENT", length = 2000)
    private String resultContent;
    @Column(name = "DESCRIPTION", length = 2000)
    private String description;
    @Column(name = "PRO_LIMIT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date proLimitDate;
    @Column(name = "ONTIME", length = 1, precision = 1, scale = 0)
    private Long ontime;
    @Column(name = "STATUS", length = 1)
    private String status;
    @Column(name = "IM_PROCESS", length = 1, precision = 1, scale = 0)
    private Long imProcess;
    @Column(name = "END_USER", length = 50)
    private String endUser;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "REDUCTION_MONEY", length = 10)
    private String reductionMoney;
    @Column(name = "CUST_NAME", length = 240)
    private String custName;
    @Column(name = "COMPLAINER_EMAIL", length = 30)
    private String complainerEmail;
    @Column(name = "RESULT_DOCUMENT_NO", length = 2000)
    private String resultDocumentNo;
    @Column(name = "COMP_LEVEL_ID", length = 10, precision = 10, scale = 0)
    private Long compLevelId;
    @Column(name = "RISE", length = 1)
    private String rise;
    @Column(name = "RES_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date resDate;
    @Column(name = "RES_LIMIT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date resLimitDate;
    @Column(name = "SAT_LEVEL_ID", length = 10, precision = 10, scale = 0)
    private Long satLevelId;
    @Column(name = "RETURN_STATUS", length = 1)
    private String returnStatus;
    @Column(name = "DUPLICATE_ID", length = 10, precision = 10, scale = 0)
    private Long duplicateId;
    @Column(name = "NOTE", length = 1000)
    private String note;
    @Column(name = "DISTRICT", length = 15)
    private String district;
    @Column(name = "PRECINCT", length = 15)
    private String precinct;
    @Column(name = "COMP_CAUSE_ID", length = 10, precision = 10, scale = 0)
    private Long compCauseId;
    @Column(name = "NUM_CONTACT_CUST", length = 2, precision = 2, scale = 0)
    private Long numContactCust;
    @Column(name = "RETURN_REASON", length = 1)
    private String returnReason;
    @Column(name = "COMP_CHANNEL_ID", length = 10, precision = 10, scale = 0)
    private Long compChannelId;
    @Column(name = "HAPPYCALL_CONTENT", length = 1000)
    private String happyCallContent;
    @Column(name = "NUM_CONTRACT", length = 10, precision = 10, scale = 0)
    private Long numContract;
    @Column(name = "CUSTOMER_TEXT", length = 2000)
    private String customerText;
    @Column(name = "SEND_SMS", length = 1)
    private String sendSMS;
    @Column(name = "IMSI", length = 50)
    private String imsi;
    @Column(name = "SERIAL", length = 50)
    private String serial;
    @Column(name = "LOCATION_NAME", length = 500)
    private String locationName;
    @Column(name = "PROCESSING_USER", length = 50)
    private String processingUser;
    @Column(name = "PROCESSING_NOTE", length = 2000)
    private String processingNote;
    @Column(name = "LAST_ACCEPT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastAcceptDate;
    @Column(name = "LOCK_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lockDate;
    @Column(name = "COMP_PRODUCT_ID", length = 10, precision = 10, scale = 0)
    private Long compProductId;
    @Column(name = "DSLAM_DLU_ID", length = 10, precision = 10, scale = 0)
    private Long dslamId;
    @Column(name = "BOARD_ID", length = 10, precision = 10, scale = 0)
    private Long boardId;
    @Column(name = "CABLE_ID", length = 10, precision = 10, scale = 0)
    private Long cableId;
    @Column(name = "BLADE_A", length = 10, precision = 10, scale = 0)
    private Long bladeA;
    @Column(name = "BLADE_B", length = 10, precision = 10, scale = 0)
    private Long bladeB;
    @Column(name = "PORT", length = 10, precision = 10, scale = 0)
    private Long port;
    @Column(name = "CABLE_LENGTH", length = 10, precision = 10, scale = 0)
    private Long cableLength;
    @Column(name = "CONTRACT_NO", length = 200)
    private String contractNo;
    @Column(name = "NUMB_CABLE_USED", length = 5, precision = 5, scale = 0)
    private Long numbCableUsed;
    @Column(name = "ADDRESS_CODE", length = 50)
    private String addressCode;
    @Column(name = "PACKAGES", length = 100)
    private String packages;
    @Column(name = "BREAK_DOWN_TYPE", length = 2)
    private String breakDownType;
    @Column(name = "ACT_STATUS", length = 3)
    private String actStatus;
    @Column(name = "PROCESS_TYPE_ID", length = 10, precision = 10, scale = 0)
    private Long processTypeId;
    @Column(name = "LIMIT_CUST_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date limitCustDate;
    @Column(name = "DEP_SEND", length = 10, precision = 10, scale = 0)
    private Long depSend;
    @Column(name = "PRO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date proDate;

    /**
     * @return the complainId
     */
    public Long getComplainId() {
        return complainId;
    }

    /**
     * @param complainId the complainId to set
     */
    public void setComplainId(Long complainId) {
        this.complainId = complainId;
    }

    /**
     * @return the depId
     */
    public Long getDepId() {
        return depId;
    }

    /**
     * @param depId the depId to set
     */
    public void setDepId(Long depId) {
        this.depId = depId;
    }

    /**
     * @return the serviceTypeId
     */
    public Long getServiceTypeId() {
        return serviceTypeId;
    }

    /**
     * @param serviceTypeId the serviceTypeId to set
     */
    public void setServiceTypeId(Long serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    /**
     * @return the acceptSourceId
     */
    public Long getAcceptSourceId() {
        return acceptSourceId;
    }

    /**
     * @param acceptSourceId the acceptSourceId to set
     */
    public void setAcceptSourceId(Long acceptSourceId) {
        this.acceptSourceId = acceptSourceId;
    }

    /**
     * @return the compTypeId
     */
    public Long getCompTypeId() {
        return compTypeId;
    }

    /**
     * @param compTypeId the compTypeId to set
     */
    public void setCompTypeId(Long compTypeId) {
        this.compTypeId = compTypeId;
    }

    /**
     * @return the priorityId
     */
    public Long getPriorityId() {
        return priorityId;
    }

    /**
     * @param priorityId the priorityId to set
     */
    public void setPriorityId(Long priorityId) {
        this.priorityId = priorityId;
    }

    /**
     * @return the acceptTypeId
     */
    public Long getAcceptTypeId() {
        return acceptTypeId;
    }

    /**
     * @param acceptTypeId the acceptTypeId to set
     */
    public void setAcceptTypeId(Long acceptTypeId) {
        this.acceptTypeId = acceptTypeId;
    }

    /**
     * @return the empId
     */
    public Long getEmpId() {
        return empId;
    }

    /**
     * @param empId the empId to set
     */
    public void setEmpId(Long empId) {
        this.empId = empId;
    }

    /**
     * @return the resultId
     */
    public Long getResultId() {
        return resultId;
    }

    /**
     * @param resultId the resultId to set
     */
    public void setResultId(Long resultId) {
        this.resultId = resultId;
    }

    /**
     * @return the reasonId
     */
    public Long getReasonId() {
        return reasonId;
    }

    /**
     * @param reasonId the reasonId to set
     */
    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    /**
     * @return the isdn
     */
    public String getIsdn() {
        return isdn;
    }

    /**
     * @param isdn the isdn to set
     */
    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    /**
     * @return the subId
     */
    public Long getSubId() {
        return subId;
    }

    /**
     * @param subId the subId to set
     */
    public void setSubId(Long subId) {
        this.subId = subId;
    }

    /**
     * @return the contractId
     */
    public Long getContractId() {
        return contractId;
    }

    /**
     * @param contractId the contractId to set
     */
    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    /**
     * @return the complainerName
     */
    public String getComplainerName() {
        return complainerName;
    }

    /**
     * @param complainerName the complainerName to set
     */
    public void setComplainerName(String complainerName) {
        this.complainerName = complainerName;
    }

    /**
     * @return the complainerPhone
     */
    public String getComplainerPhone() {
        return complainerPhone;
    }

    /**
     * @param complainerPhone the complainerPhone to set
     */
    public void setComplainerPhone(String complainerPhone) {
        this.complainerPhone = complainerPhone;
    }

    /**
     * @return the complainerAddress
     */
    public String getComplainerAddress() {
        return complainerAddress;
    }

    /**
     * @param complainerAddress the complainerAddress to set
     */
    public void setComplainerAddress(String complainerAddress) {
        this.complainerAddress = complainerAddress;
    }

    /**
     * @return the complainerPassport
     */
    public String getComplainerPassport() {
        return complainerPassport;
    }

    /**
     * @param complainerPassport the complainerPassport to set
     */
    public void setComplainerPassport(String complainerPassport) {
        this.complainerPassport = complainerPassport;
    }

    /**
     * @return the complainerIsOther
     */
    public String getComplainerIsOther() {
        return complainerIsOther;
    }

    /**
     * @param complainerIsOther the complainerIsOther to set
     */
    public void setComplainerIsOther(String complainerIsOther) {
        this.complainerIsOther = complainerIsOther;
    }

    /**
     * @return the reCompNumber
     */
    public Long getReCompNumber() {
        return reCompNumber;
    }

    /**
     * @param reCompNumber the reCompNumber to set
     */
    public void setReCompNumber(Long reCompNumber) {
        this.reCompNumber = reCompNumber;
    }

    /**
     * @return the acceptUser
     */
    public String getAcceptUser() {
        return acceptUser;
    }

    /**
     * @param acceptUser the acceptUser to set
     */
    public void setAcceptUser(String acceptUser) {
        this.acceptUser = acceptUser;
    }

    /**
     * @return the acceptDate
     */
    public Date getAcceptDate() {
        return acceptDate;
    }

    /**
     * @param acceptDate the acceptDate to set
     */
    public void setAcceptDate(Date acceptDate) {
        this.acceptDate = acceptDate;
    }

    /**
     * @return the compContent
     */
    public String getCompContent() {
        return compContent;
    }

    /**
     * @param compContent the compContent to set
     */
    public void setCompContent(String compContent) {
        this.compContent = compContent;
    }

    /**
     * @return the preResult
     */
    public String getPreResult() {
        return preResult;
    }

    /**
     * @param preResult the preResult to set
     */
    public void setPreResult(String preResult) {
        this.preResult = preResult;
    }

    /**
     * @return the attachDocument
     */
    public String getAttachDocument() {
        return attachDocument;
    }

    /**
     * @param attachDocument the attachDocument to set
     */
    public void setAttachDocument(String attachDocument) {
        this.attachDocument = attachDocument;
    }

    /**
     * @return the isValid
     */
    public String getIsValid() {
        return isValid;
    }

    /**
     * @param isValid the isValid to set
     */
    public void setIsValid(String isValid) {
        this.isValid = isValid;
    }

    /**
     * @return the custLimitDate
     */
    public Date getCustLimitDate() {
        return custLimitDate;
    }

    /**
     * @param custLimitDate the custLimitDate to set
     */
    public void setCustLimitDate(Date custLimitDate) {
        this.custLimitDate = custLimitDate;
    }

    /**
     * @return the resultContent
     */
    public String getResultContent() {
        return resultContent;
    }

    /**
     * @param resultContent the resultContent to set
     */
    public void setResultContent(String resultContent) {
        this.resultContent = resultContent;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the proLimitDate
     */
    public Date getProLimitDate() {
        return proLimitDate;
    }

    /**
     * @param proLimitDate the proLimitDate to set
     */
    public void setProLimitDate(Date proLimitDate) {
        this.proLimitDate = proLimitDate;
    }

    /**
     * @return the ontime
     */
    public Long getOntime() {
        return ontime;
    }

    /**
     * @param ontime the ontime to set
     */
    public void setOntime(Long ontime) {
        this.ontime = ontime;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the imProcess
     */
    public Long getImProcess() {
        return imProcess;
    }

    /**
     * @param imProcess the imProcess to set
     */
    public void setImProcess(Long imProcess) {
        this.imProcess = imProcess;
    }

    /**
     * @return the endUser
     */
    public String getEndUser() {
        return endUser;
    }

    /**
     * @param endUser the endUser to set
     */
    public void setEndUser(String endUser) {
        this.endUser = endUser;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the reductionMoney
     */
    public String getReductionMoney() {
        return reductionMoney;
    }

    /**
     * @param reductionMoney the reductionMoney to set
     */
    public void setReductionMoney(String reductionMoney) {
        this.reductionMoney = reductionMoney;
    }

    /**
     * @return the custName
     */
    public String getCustName() {
        return custName;
    }

    /**
     * @param custName the custName to set
     */
    public void setCustName(String custName) {
        this.custName = custName;
    }

    /**
     * @return the complainerEmail
     */
    public String getComplainerEmail() {
        return complainerEmail;
    }

    /**
     * @param complainerEmail the complainerEmail to set
     */
    public void setComplainerEmail(String complainerEmail) {
        this.complainerEmail = complainerEmail;
    }

    /**
     * @return the resultDocumentNo
     */
    public String getResultDocumentNo() {
        return resultDocumentNo;
    }

    /**
     * @param resultDocumentNo the resultDocumentNo to set
     */
    public void setResultDocumentNo(String resultDocumentNo) {
        this.resultDocumentNo = resultDocumentNo;
    }

    /**
     * @return the compLevelId
     */
    public Long getCompLevelId() {
        return compLevelId;
    }

    /**
     * @param compLevelId the compLevelId to set
     */
    public void setCompLevelId(Long compLevelId) {
        this.compLevelId = compLevelId;
    }

    /**
     * @return the rise
     */
    public String getRise() {
        return rise;
    }

    /**
     * @param rise the rise to set
     */
    public void setRise(String rise) {
        this.rise = rise;
    }

    /**
     * @return the resDate
     */
    public Date getResDate() {
        return resDate;
    }

    /**
     * @param resDate the resDate to set
     */
    public void setResDate(Date resDate) {
        this.resDate = resDate;
    }

    /**
     * @return the resLimitDate
     */
    public Date getResLimitDate() {
        return resLimitDate;
    }

    /**
     * @param resLimitDate the resLimitDate to set
     */
    public void setResLimitDate(Date resLimitDate) {
        this.resLimitDate = resLimitDate;
    }

    /**
     * @return the satLevelId
     */
    public Long getSatLevelId() {
        return satLevelId;
    }

    /**
     * @param satLevelId the satLevelId to set
     */
    public void setSatLevelId(Long satLevelId) {
        this.satLevelId = satLevelId;
    }

    /**
     * @return the returnStatus
     */
    public String getReturnStatus() {
        return returnStatus;
    }

    /**
     * @param returnStatus the returnStatus to set
     */
    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    /**
     * @return the duplicateId
     */
    public Long getDuplicateId() {
        return duplicateId;
    }

    /**
     * @param duplicateId the duplicateId to set
     */
    public void setDuplicateId(Long duplicateId) {
        this.duplicateId = duplicateId;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * @param district the district to set
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     * @return the precinct
     */
    public String getPrecinct() {
        return precinct;
    }

    /**
     * @param precinct the precinct to set
     */
    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    /**
     * @return the compCauseId
     */
    public Long getCompCauseId() {
        return compCauseId;
    }

    /**
     * @param compCauseId the compCauseId to set
     */
    public void setCompCauseId(Long compCauseId) {
        this.compCauseId = compCauseId;
    }

    /**
     * @return the numContactCust
     */
    public Long getNumContactCust() {
        return numContactCust;
    }

    /**
     * @param numContactCust the numContactCust to set
     */
    public void setNumContactCust(Long numContactCust) {
        this.numContactCust = numContactCust;
    }

    /**
     * @return the returnReason
     */
    public String getReturnReason() {
        return returnReason;
    }

    /**
     * @param returnReason the returnReason to set
     */
    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }

    /**
     * @return the compChannelId
     */
    public Long getCompChannelId() {
        return compChannelId;
    }

    /**
     * @param compChannelId the compChannelId to set
     */
    public void setCompChannelId(Long compChannelId) {
        this.compChannelId = compChannelId;
    }

    /**
     * @return the happyCallContent
     */
    public String getHappyCallContent() {
        return happyCallContent;
    }

    /**
     * @param happyCallContent the happyCallContent to set
     */
    public void setHappyCallContent(String happyCallContent) {
        this.happyCallContent = happyCallContent;
    }

    /**
     * @return the numContract
     */
    public Long getNumContract() {
        return numContract;
    }

    /**
     * @param numContract the numContract to set
     */
    public void setNumContract(Long numContract) {
        this.numContract = numContract;
    }

    /**
     * @return the customerText
     */
    public String getCustomerText() {
        return customerText;
    }

    /**
     * @param customerText the customerText to set
     */
    public void setCustomerText(String customerText) {
        this.customerText = customerText;
    }

    /**
     * @return the sendSMS
     */
    public String getSendSMS() {
        return sendSMS;
    }

    /**
     * @param sendSMS the sendSMS to set
     */
    public void setSendSMS(String sendSMS) {
        this.sendSMS = sendSMS;
    }

    /**
     * @return the imsi
     */
    public String getImsi() {
        return imsi;
    }

    /**
     * @param imsi the imsi to set
     */
    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    /**
     * @return the serial
     */
    public String getSerial() {
        return serial;
    }

    /**
     * @param serial the serial to set
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     * @return the locationName
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * @param locationName the locationName to set
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    /**
     * @return the processingUser
     */
    public String getProcessingUser() {
        return processingUser;
    }

    /**
     * @param processingUser the processingUser to set
     */
    public void setProcessingUser(String processingUser) {
        this.processingUser = processingUser;
    }

    /**
     * @return the processingNote
     */
    public String getProcessingNote() {
        return processingNote;
    }

    /**
     * @param processingNote the processingNote to set
     */
    public void setProcessingNote(String processingNote) {
        this.processingNote = processingNote;
    }

    /**
     * @return the lastAcceptDate
     */
    public Date getLastAcceptDate() {
        return lastAcceptDate;
    }

    /**
     * @param lastAcceptDate the lastAcceptDate to set
     */
    public void setLastAcceptDate(Date lastAcceptDate) {
        this.lastAcceptDate = lastAcceptDate;
    }

    /**
     * @return the lockDate
     */
    public Date getLockDate() {
        return lockDate;
    }

    /**
     * @param lockDate the lockDate to set
     */
    public void setLockDate(Date lockDate) {
        this.lockDate = lockDate;
    }

    /**
     * @return the compProductId
     */
    public Long getCompProductId() {
        return compProductId;
    }

    /**
     * @param compProductId the compProductId to set
     */
    public void setCompProductId(Long compProductId) {
        this.compProductId = compProductId;
    }

    /**
     * @return the dslamId
     */
    public Long getDslamId() {
        return dslamId;
    }

    /**
     * @param dslamId the dslamId to set
     */
    public void setDslamId(Long dslamId) {
        this.dslamId = dslamId;
    }

    /**
     * @return the boardId
     */
    public Long getBoardId() {
        return boardId;
    }

    /**
     * @param boardId the boardId to set
     */
    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    /**
     * @return the cableId
     */
    public Long getCableId() {
        return cableId;
    }

    /**
     * @param cableId the cableId to set
     */
    public void setCableId(Long cableId) {
        this.cableId = cableId;
    }

    /**
     * @return the bladeA
     */
    public Long getBladeA() {
        return bladeA;
    }

    /**
     * @param bladeA the bladeA to set
     */
    public void setBladeA(Long bladeA) {
        this.bladeA = bladeA;
    }

    /**
     * @return the bladeB
     */
    public Long getBladeB() {
        return bladeB;
    }

    /**
     * @param bladeB the bladeB to set
     */
    public void setBladeB(Long bladeB) {
        this.bladeB = bladeB;
    }

    /**
     * @return the port
     */
    public Long getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(Long port) {
        this.port = port;
    }

    /**
     * @return the cableLength
     */
    public Long getCableLength() {
        return cableLength;
    }

    /**
     * @param cableLength the cableLength to set
     */
    public void setCableLength(Long cableLength) {
        this.cableLength = cableLength;
    }

    /**
     * @return the contractNo
     */
    public String getContractNo() {
        return contractNo;
    }

    /**
     * @param contractNo the contractNo to set
     */
    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    /**
     * @return the numbCableUsed
     */
    public Long getNumbCableUsed() {
        return numbCableUsed;
    }

    /**
     * @param numbCableUsed the numbCableUsed to set
     */
    public void setNumbCableUsed(Long numbCableUsed) {
        this.numbCableUsed = numbCableUsed;
    }

    /**
     * @return the addressCode
     */
    public String getAddressCode() {
        return addressCode;
    }

    /**
     * @param addressCode the addressCode to set
     */
    public void setAddressCode(String addressCode) {
        this.addressCode = addressCode;
    }

    /**
     * @return the packages
     */
    public String getPackages() {
        return packages;
    }

    /**
     * @param packages the packages to set
     */
    public void setPackages(String packages) {
        this.packages = packages;
    }

    /**
     * @return the breakDownType
     */
    public String getBreakDownType() {
        return breakDownType;
    }

    /**
     * @param breakDownType the breakDownType to set
     */
    public void setBreakDownType(String breakDownType) {
        this.breakDownType = breakDownType;
    }

    /**
     * @return the actStatus
     */
    public String getActStatus() {
        return actStatus;
    }

    /**
     * @param actStatus the actStatus to set
     */
    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    /**
     * @return the processTypeId
     */
    public Long getProcessTypeId() {
        return processTypeId;
    }

    /**
     * @param processTypeId the processTypeId to set
     */
    public void setProcessTypeId(Long processTypeId) {
        this.processTypeId = processTypeId;
    }

    /**
     * @return the limitCustDate
     */
    public Date getLimitCustDate() {
        return limitCustDate;
    }

    /**
     * @param limitCustDate the limitCustDate to set
     */
    public void setLimitCustDate(Date limitCustDate) {
        this.limitCustDate = limitCustDate;
    }

    /**
     * @return the depSend
     */
    public Long getDepSend() {
        return depSend;
    }

    /**
     * @param depSend the depSend to set
     */
    public void setDepSend(Long depSend) {
        this.depSend = depSend;
    }

    /**
     * @return the proDate
     */
    public Date getProDate() {
        return proDate;
    }

    /**
     * @param proDate the proDate to set
     */
    public void setProDate(Date proDate) {
        this.proDate = proDate;
    }

}
