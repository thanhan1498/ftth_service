/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.model;

/**
 *
 * @author duyetdk
 */
public class ComplaintAcceptType {
    private Long acceptTypeId;
    private String name;
    private String codeAccept;
    private String description;

    public Long getAcceptTypeId() {
        return acceptTypeId;
    }

    public void setAcceptTypeId(Long acceptTypeId) {
        this.acceptTypeId = acceptTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCodeAccept() {
        return codeAccept;
    }

    public void setCodeAccept(String codeAccept) {
        this.codeAccept = codeAccept;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
