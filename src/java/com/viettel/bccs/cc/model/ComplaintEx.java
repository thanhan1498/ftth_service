/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.model;

/**
 *
 * @author cuongdm
 */
public class ComplaintEx {

    private Long complainId;
    private String acceptDate;
    private String compContent;
    private String proLimitDate;
    private String status;
    private String staffInfo;
    private String preResult;
    private String errorPhone;
    private String complainerPhone;

    /**
     * @return the complainId
     */
    public Long getComplainId() {
        return complainId;
    }

    /**
     * @param complainId the complainId to set
     */
    public void setComplainId(Long complainId) {
        this.complainId = complainId;
    }

    /**
     * @return the acceptDate
     */
    public String getAcceptDate() {
        return acceptDate;
    }

    /**
     * @param acceptDate the acceptDate to set
     */
    public void setAcceptDate(String acceptDate) {
        this.acceptDate = acceptDate;
    }

    /**
     * @return the compContent
     */
    public String getCompContent() {
        return compContent;
    }

    /**
     * @param compContent the compContent to set
     */
    public void setCompContent(String compContent) {
        this.compContent = compContent;
    }

    /**
     * @return the proLimitDate
     */
    public String getProLimitDate() {
        return proLimitDate;
    }

    /**
     * @param proLimitDate the proLimitDate to set
     */
    public void setProLimitDate(String proLimitDate) {
        this.proLimitDate = proLimitDate;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the preResult
     */
    public String getPreResult() {
        return preResult;
    }

    /**
     * @param preResult the preResult to set
     */
    public void setPreResult(String preResult) {
        this.preResult = preResult;
    }

    /**
     * @return the staffInfo
     */
    public String getStaffInfo() {
        return staffInfo;
    }

    /**
     * @param staffInfo the staffInfo to set
     */
    public void setStaffInfo(String staffInfo) {
        this.staffInfo = staffInfo;
    }

    /**
     * @return the errorPhone
     */
    public String getErrorPhone() {
        return errorPhone;
    }

    /**
     * @param errorPhone the errorPhone to set
     */
    public void setErrorPhone(String errorPhone) {
        this.errorPhone = errorPhone;
    }

    /**
     * @return the complainerPhone
     */
    public String getComplainerPhone() {
        return complainerPhone;
    }

    /**
     * @param complainerPhone the complainerPhone to set
     */
    public void setComplainerPhone(String complainerPhone) {
        this.complainerPhone = complainerPhone;
    }
}
