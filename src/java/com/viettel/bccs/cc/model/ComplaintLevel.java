/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.model;

/**
 *
 * @author duyetdk
 */
public class ComplaintLevel {
    private Long complaintLevelId;
    private String name;
    private String code;
    private String description;

    public Long getComplaintLevelId() {
        return complaintLevelId;
    }

    public void setComplaintLevelId(Long complaintLevelId) {
        this.complaintLevelId = complaintLevelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
