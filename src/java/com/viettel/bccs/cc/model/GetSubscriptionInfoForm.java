/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.model;

/**
 *
 * @author duyetdk
 */
public class GetSubscriptionInfoForm {

    private String account;
    private String deviceCode;
    private String deviceName;
    private Long deviceId;
    private String infraType;
    private String networkClass;
    private String networkType;
    private Long stationId;
    private String stationCode;
    private String spliterCode;
    private String odfCode;
    private Long odfId;
    private Long shopId;
    private String shopName;
    private String shopCode;
    private String staffCode;
    private String staffName;
    private String nodeTbCode;
    private Long nodeTbId;
    private String boxCode;

    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * @return the deviceCode
     */
    public String getDeviceCode() {
        return deviceCode;
    }

    /**
     * @param deviceCode the deviceCode to set
     */
    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    /**
     * @return the deviceName
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * @param deviceName the deviceName to set
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    /**
     * @return the deviceId
     */
    public Long getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId the deviceId to set
     */
    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return the infraType
     */
    public String getInfraType() {
        return infraType;
    }

    /**
     * @param infraType the infraType to set
     */
    public void setInfraType(String infraType) {
        this.infraType = infraType;
    }

    /**
     * @return the networkClass
     */
    public String getNetworkClass() {
        return networkClass;
    }

    /**
     * @param networkClass the networkClass to set
     */
    public void setNetworkClass(String networkClass) {
        this.networkClass = networkClass;
    }

    /**
     * @return the networkType
     */
    public String getNetworkType() {
        return networkType;
    }

    /**
     * @param networkType the networkType to set
     */
    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    /**
     * @return the stationId
     */
    public Long getStationId() {
        return stationId;
    }

    /**
     * @param stationId the stationId to set
     */
    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    /**
     * @return the stationCode
     */
    public String getStationCode() {
        return stationCode;
    }

    /**
     * @param stationCode the stationCode to set
     */
    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    /**
     * @return the spliterCode
     */
    public String getSpliterCode() {
        return spliterCode;
    }

    /**
     * @param spliterCode the spliterCode to set
     */
    public void setSpliterCode(String spliterCode) {
        this.spliterCode = spliterCode;
    }

    /**
     * @return the odfCode
     */
    public String getOdfCode() {
        return odfCode;
    }

    /**
     * @param odfCode the odfCode to set
     */
    public void setOdfCode(String odfCode) {
        this.odfCode = odfCode;
    }

    /**
     * @return the odfId
     */
    public Long getOdfId() {
        return odfId;
    }

    /**
     * @param odfId the odfId to set
     */
    public void setOdfId(Long odfId) {
        this.odfId = odfId;
    }

    /**
     * @return the shopId
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * @param shopId the shopId to set
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * @return the shopCode
     */
    public String getShopCode() {
        return shopCode;
    }

    /**
     * @param shopCode the shopCode to set
     */
    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    /**
     * @return the staffCode
     */
    public String getStaffCode() {
        return staffCode;
    }

    /**
     * @param staffCode the staffCode to set
     */
    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    /**
     * @return the staffName
     */
    public String getStaffName() {
        return staffName;
    }

    /**
     * @param staffName the staffName to set
     */
    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    /**
     * @return the nodeTbCode
     */
    public String getNodeTbCode() {
        return nodeTbCode;
    }

    /**
     * @param nodeTbCode the nodeTbCode to set
     */
    public void setNodeTbCode(String nodeTbCode) {
        this.nodeTbCode = nodeTbCode;
    }

    /**
     * @return the nodeTbId
     */
    public Long getNodeTbId() {
        return nodeTbId;
    }

    /**
     * @param nodeTbId the nodeTbId to set
     */
    public void setNodeTbId(Long nodeTbId) {
        this.nodeTbId = nodeTbId;
    }

    /**
     * @return the boxCode
     */
    public String getBoxCode() {
        return boxCode;
    }

    /**
     * @param boxCode the boxCode to set
     */
    public void setBoxCode(String boxCode) {
        this.boxCode = boxCode;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
    
}
