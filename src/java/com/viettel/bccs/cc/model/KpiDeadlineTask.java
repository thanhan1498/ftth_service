/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.model;

/**
 *
 * @author duyetdk
 */
public class KpiDeadlineTask {

    private String jobType;
    private String serviceType;
    private String infraType;
    private Long isVip;
    private Long warning;
    private Long overTime;
    private Long status;

    /**
     * @return the jobType
     */
    public String getJobType() {
        return jobType;
    }

    /**
     * @param jobType the jobType to set
     */
    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    /**
     * @return the infraType
     */
    public String getInfraType() {
        return infraType;
    }

    /**
     * @param infraType the infraType to set
     */
    public void setInfraType(String infraType) {
        this.infraType = infraType;
    }

    /**
     * @return the isVip
     */
    public Long getIsVip() {
        return isVip;
    }

    /**
     * @param isVip the isVip to set
     */
    public void setIsVip(Long isVip) {
        this.isVip = isVip;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the warning
     */
    public Long getWarning() {
        return warning;
    }

    /**
     * @param warning the warning to set
     */
    public void setWarning(Long warning) {
        this.warning = warning;
    }

    /**
     * @return the overTime
     */
    public Long getOverTime() {
        return overTime;
    }

    /**
     * @param overTime the overTime to set
     */
    public void setOverTime(Long overTime) {
        this.overTime = overTime;
    }
}
