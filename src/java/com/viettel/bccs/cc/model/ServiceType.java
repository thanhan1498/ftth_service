/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.model;

/**
 *
 * @author duyetdk
 */
public class ServiceType implements java.io.Serializable {

    private Long serviceTypeId;
    private String name;
    private String code;
    private String description;
    private String telecomServiceId;

    public ServiceType() {
    }

    public Long getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(Long serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTelecomServiceId() {
        return telecomServiceId;
    }

    public void setTelecomServiceId(String telecomServiceId) {
        this.telecomServiceId = telecomServiceId;
    }

}