/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cc.supplier;

import com.viettel.bccs.cc.dao.CacheCC;
import com.viettel.bccs.cc.dao.CompGetDataDAO;
import com.viettel.bccs.cc.model.AssignManagement;
import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.bccs.cc.model.CompDepartmentGroup;
import com.viettel.bccs.cc.model.CompProcess;
import com.viettel.bccs.cc.model.Complain;
import com.viettel.bccs.cc.model.ComplaintAcceptSource;
import com.viettel.bccs.cc.model.ComplaintAcceptType;
import com.viettel.bccs.cc.model.ComplaintEx;
import com.viettel.bccs.cc.model.ComplaintGroup;
import com.viettel.bccs.cc.model.ComplaintLevel;
import com.viettel.bccs.cc.model.ComplaintType;
import com.viettel.bccs.cc.model.Employee;
import com.viettel.bccs.cc.model.ServiceType;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.TaskManagement;
import com.viettel.bccs.cm.model.TaskShopManagement;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.ws.model.input.ComplaintInput;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author duyetdk
 */
public class ComplaintSupplier extends BaseSupplier {

    /**
     * @author: duyetdk
     * @since 9/4/2019 lay danh sach loai complaint duoc accept
     * @param ccSession
     * @param codeAccept
     * @return
     */
    public List<ComplaintAcceptType> getListComplaintAcceptType(Session ccSession, String codeAccept) {
        StringBuilder sql = new StringBuilder();
        HashMap<String, Object> params = new HashMap<String, Object>();
        sql.append(" SELECT cat.accept_type_id acceptTypeId, cat.name, cat.code codeAccept, cat.description ");
        sql.append(" FROM comp_accept_type cat WHERE cat.isactive = 1 ");
        if (codeAccept != null && !codeAccept.trim().isEmpty()) {
            sql.append(" AND LOWER(cat.code) = :codeAccept ");
            params.put("codeAccept", codeAccept.trim().toLowerCase());
        }
        sql.append(" ORDER BY cat.name ASC ");

        Query query = ccSession.createSQLQuery(sql.toString())
                .addScalar("acceptTypeId", Hibernate.LONG)
                .addScalar("name", Hibernate.STRING)
                .addScalar("codeAccept", Hibernate.STRING)
                .addScalar("description", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ComplaintAcceptType.class));

        buildParameter(query, params);
        List<ComplaintAcceptType> result = query.list();
        return result;
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019 lay danh sach nguon complaint
     * @param ccSession
     * @return
     */
    public List<ComplaintAcceptSource> getListComplaintAcceptSource(Session ccSession) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT cas.accept_source_id acceptSourceId, cas.name, cas.code, cas.description ");
        sql.append(" FROM comp_accept_source cas WHERE cas.isactive = 1 ");
        sql.append(" ORDER BY cas.name ASC ");

        Query query = ccSession.createSQLQuery(sql.toString())
                .addScalar("acceptSourceId", Hibernate.LONG)
                .addScalar("name", Hibernate.STRING)
                .addScalar("code", Hibernate.STRING)
                .addScalar("description", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ComplaintAcceptSource.class));

        List<ComplaintAcceptSource> result = query.list();
        return result;
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019 lay danh sach loai complaint theo groupId
     * @param ccSession
     * @param groupId
     * @return
     */
    public List<ComplaintType> getListComplaintType(Session ccSession, Long groupId) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT cpt.comp_type_id compTypeId, cpt.name, cpt.code, cpt.group_id groupId, ");
        sql.append(" cpt.description, cpt.comp_template compTemplate, cpt.service_type serviceType ");
        sql.append(" FROM comp_type cpt WHERE cpt.isactive = 1 AND cpt.group_id = :groupId ");
        sql.append(" ORDER BY NLSSORT(cpt.name, 'NLS_SORT=vietnamese') ASC ");

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("groupId", groupId);

        Query query = ccSession.createSQLQuery(sql.toString())
                .addScalar("compTypeId", Hibernate.LONG)
                .addScalar("name", Hibernate.STRING)
                .addScalar("code", Hibernate.STRING)
                .addScalar("groupId", Hibernate.LONG)
                .addScalar("description", Hibernate.STRING)
                .addScalar("compTemplate", Hibernate.STRING)
                .addScalar("serviceType", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ComplaintType.class));

        buildParameter(query, params);
        List<ComplaintType> result = query.list();
        return result;
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019 lay danh sach loai nhom complaint theo parentId
     * @param ccSession
     * @param parentId
     * @return
     */
    public List<ComplaintGroup> getListComplaintGroupType(Session ccSession, Long parentId) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT LEVEL, LPAD (' '||'+ '||' ', 3 * (LEVEL - 1)) || cpg.name as name,cpg.group_id as groupId ");
        sql.append(" FROM comp_group cpg WHERE cpg.isactive=1 START WITH cpg.parent_id = :parentId ");
        sql.append(" CONNECT BY PRIOR cpg.group_id = cpg.parent_id ");
        sql.append(" ORDER SIBLINGS BY cpg.name ASC ");

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("parentId", parentId);

        Query query = ccSession.createSQLQuery(sql.toString())
                .addScalar("name", Hibernate.STRING)
                .addScalar("groupId", Hibernate.LONG)
                .setResultTransformer(Transformers.aliasToBean(ComplaintGroup.class));

        buildParameter(query, params);
        List<ComplaintGroup> result = query.list();
        return result;
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019 lay danh sach nhom complaint theo comp_level_id
     * @param ccSession
     * @param compLevelId
     * @return
     */
    public List<ComplaintGroup> getListComplaintGroup(Session ccSession, Long compLevelId) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT cpg.group_id groupId, cpg.name, cpg.code, cpg.description, cpg.service_type serviceType ");
        sql.append(" FROM comp_group cpg WHERE cpg.parent_id IS NULL AND substr(cpg.service_type,3,1)='1' ");
        sql.append(" AND cpg.comp_level_id = :compLevelId AND cpg.isactive = 1 ");
        sql.append(" ORDER BY cpg.name ASC ");

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("compLevelId", compLevelId);

        Query query = ccSession.createSQLQuery(sql.toString())
                .addScalar("groupId", Hibernate.LONG)
                .addScalar("name", Hibernate.STRING)
                .addScalar("code", Hibernate.STRING)
                .addScalar("description", Hibernate.STRING)
                .addScalar("serviceType", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ComplaintGroup.class));

        buildParameter(query, params);
        List<ComplaintGroup> result = query.list();
        return result;
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019 lay danh sach muc do complaint
     * @param ccSession
     * @param complaintCode
     * @return
     */
    public List<ComplaintLevel> getListComplaintLevel(Session ccSession, String complaintCode) {
        StringBuilder sql = new StringBuilder();
        HashMap<String, Object> params = new HashMap<String, Object>();
        sql.append(" SELECT cpl.comp_level_id complaintLevelId, cpl.name, cpl.code, cpl.description ");
        sql.append(" FROM comp_level cpl WHERE cpl.isvalid = 1 ");

        if (complaintCode != null && !complaintCode.trim().isEmpty()) {
            sql.append(" AND LOWER(cpl.code) = :complaintCode ");
            params.put("complaintCode", complaintCode.trim().toLowerCase());
        }
        sql.append(" ORDER BY cpl.name ASC ");

        Query query = ccSession.createSQLQuery(sql.toString())
                .addScalar("complaintLevelId", Hibernate.LONG)
                .addScalar("name", Hibernate.STRING)
                .addScalar("code", Hibernate.STRING)
                .addScalar("description", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ComplaintLevel.class));

        buildParameter(query, params);
        List<ComplaintLevel> result = query.list();
        return result;
    }

    /**
     * @author: duyetdk
     * @since 18/4/2019 get team giai quyet su co
     * @param cmPosSession
     * @param subId
     * @return
     */
    public List<CompDepartmentGroup> getListCompTeam(Session cmPosSession, Long subId) {
        HashMap<String, Object> params = new HashMap<String, Object>();
        String sql = " SELECT s.station_id stationId, s.team_id depId, sh.name, sh.shop_code code FROM sub_adsl_ll s, shop sh "
                + " WHERE s.team_id = sh.shop_id AND s.sub_id = :subId ORDER BY sh.name ";

        params.put("subId", subId);
        Query query = cmPosSession.createSQLQuery(sql)
                .addScalar("stationId", Hibernate.LONG)
                .addScalar("depId", Hibernate.LONG)
                .addScalar("name", Hibernate.STRING)
                .addScalar("code", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(CompDepartmentGroup.class));

        buildParameter(query, params);
        List<CompDepartmentGroup> result = query.list();

        if (result != null && result.size() > 0 && result.get(0).getStationId() != null) {
            params = new HashMap<String, Object>();
            sql = " SELECT ts.team_id depId, ts.team_code code, sh.name "
                    + " FROM technical_station ts, shop sh WHERE ts.team_id = sh.shop_id AND ts.status = 1 AND ts.station_id = :stationId ";

            params.put("stationId", result.get(0).getStationId());
            query = cmPosSession.createSQLQuery(sql)
                    .addScalar("depId", Hibernate.LONG)
                    .addScalar("code", Hibernate.STRING)
                    .addScalar("name", Hibernate.STRING)
                    .setResultTransformer(Transformers.aliasToBean(CompDepartmentGroup.class));

            buildParameter(query, params);
            result = query.list();
        }
        return result;
    }

    /**
     * lay thong tin employee
     *
     * @param ccSession
     * @param staffCodeLogin
     * @return
     */
    public List<Employee> getListEmployeeByUserName(Session ccSession, String staffCodeLogin) {
        StringBuilder sql = new StringBuilder();
        HashMap<String, Object> params = new HashMap<String, Object>();
        sql.append(" SELECT em.emp_id empId, em.dep_Id depId ");
        sql.append(" FROM employee em WHERE em.isactive = 1 ");

        if (staffCodeLogin != null && !staffCodeLogin.trim().isEmpty()) {
            sql.append(" AND LOWER(em.user_name) = :userName ");
            params.put("userName", staffCodeLogin.trim().toLowerCase());
        }

        Query query = ccSession.createSQLQuery(sql.toString())
                .addScalar("empId", Hibernate.LONG)
                .addScalar("depId", Hibernate.LONG)
                .setResultTransformer(Transformers.aliasToBean(Employee.class));

        buildParameter(query, params);
        List<Employee> result = query.list();
        return result;
    }

    /**
     *
     * @param ccSession
     * @param complaintInput
     * @param subAdslll
     * @param nowDate
     * @param staffCodeLogin
     * @param acceptTypeId
     * @param timeToResolve
     * @param dateHour
     * @param compLevelId
     * @param contractNo
     * @param shopId
     * @param serviceTypeId
     * @param depId
     */
    public void insertComplain(Session ccSession, ComplaintInput complaintInput, SubAdslLeaseline subAdslll,
            Date nowDate, String staffCodeLogin, Long acceptTypeId, Date timeToResolve, Date dateHour, Long compLevelId,
            String contractNo, Long shopId, Long serviceTypeId, Long depId) {

        Complain comp = new Complain();
        comp.setComplainId(complaintInput.getComplainId());
        comp.setServiceTypeId(serviceTypeId);
        comp.setIsdn(subAdslll.getAccount());
        comp.setSubId(complaintInput.getSubId());
        comp.setContractId(subAdslll.getContractId());
        comp.setComplainerName(complaintInput.getComplainerName());
        comp.setComplainerPhone(complaintInput.getComplainerPhone());
        comp.setComplainerAddress(complaintInput.getComplainerAddress());
        comp.setComplainerPassport(complaintInput.getComplainerPassport());
        comp.setComplainerIsOther("0");
        comp.setReCompNumber(1L);
        comp.setAcceptUser(staffCodeLogin);
        comp.setAcceptDate(nowDate);
        comp.setCompTypeId(complaintInput.getCompTypeId());
        comp.setAcceptTypeId(acceptTypeId);
        comp.setAcceptSourceId(complaintInput.getAcceptSourceId());
        comp.setPriorityId(2L);
        comp.setCompContent(complaintInput.getCompContent());
        comp.setIsValid("1");
        comp.setCustLimitDate(timeToResolve);
        comp.setDepId(depId);
//        comp.setProLimitDate(dateHour);
        comp.setStatus("0");
        comp.setEndUser(staffCodeLogin);
        comp.setCustName(complaintInput.getComplainerName());
        comp.setCompLevelId(compLevelId);
        comp.setReturnStatus("0");
        comp.setNumContactCust(0L);
        comp.setSendSMS("1");
        comp.setLastAcceptDate(nowDate);
        comp.setDslamId(subAdslll.getStationId());
        comp.setBoardId(subAdslll.getBoardId());
        comp.setCableId(subAdslll.getCableBoxId());
//        comp.setPort(Long.valueOf(subAdslll.getPortNo()));
        comp.setContractNo(contractNo);
        comp.setAddressCode(subAdslll.getAddressCode());
        comp.setDepSend(shopId);
        comp.setProDate(nowDate);
        comp.setNumCusDelay(0L);
//        comp.setReqTeamTime(nowDate);
        comp.setPackages(subAdslll.getProductCode());
        comp.setResLimitDate(nowDate);
        comp.setDistrict(complaintInput.getDistrict());
        comp.setPrecinct(complaintInput.getPrecinct());
//        comp.setProcessingUser(staffCodeLogin);
        comp.setProcessResultContent(staffCodeLogin + " : " + nowDate + " : Receive new complaint");
        comp.setEndDate(nowDate);

        ccSession.save(comp);
        ccSession.flush();
    }

    /**
     *
     * @param ccSession
     * @param complainId
     * @param assignDate
     * @param description
     * @param employeeId
     * @param assignType
     * @param active
     * @param managerId
     * @param depSendId
     * @param depReceiveId
     * @param agreeDate
     */
    public void insertAssignManagement(Session ccSession, Long complainId, Date assignDate,
            String description, Long employeeId, Long assignType, Long active, Long managerId,
            Long depSendId, Long depReceiveId, Date agreeDate) {

        AssignManagement asm = new AssignManagement();
        Long assignId = getSequence(ccSession, Constants.ASSIGN_MANAGEMENT_SEQ);
        asm.setAssignId(assignId);
        asm.setComplainId(complainId);
        asm.setAssignDate(assignDate);
        asm.setEmployeeId(employeeId);
        asm.setDescription(description);
        asm.setAssignType(assignType);
        asm.setActive(active);
        asm.setManagerId(managerId);
        asm.setDepSendId(depSendId);
        asm.setDepReceiveId(depReceiveId);
        asm.setAgreeDate(agreeDate);
        asm.setProcessTime(0L);

        ccSession.save(asm);
        ccSession.flush();
    }

    /**
     *
     * @param ccSession
     * @param complainId
     * @param staffCodeLogin
     * @param nowDate
     * @param limitDate
     * @param resultContent
     * @param description
     * @param memo
     * @param depId
     */
    public void insertCompProcess(Session ccSession, Long complainId, String staffCodeLogin, Date nowDate,
            Date limitDate, String resultContent, String description, String memo, Long depId) {

        CompProcess compProcess = new CompProcess();
        Long compProId = getSequence(ccSession, Constants.COMP_PROCESS_SEQ);
        compProcess.setCompProId(compProId);
        compProcess.setComplainId(complainId);
        compProcess.setUserName(staffCodeLogin);
        compProcess.setFwDate(nowDate);
        compProcess.setProDate(nowDate);
        compProcess.setLimitDate(limitDate);
        compProcess.setResultContent(resultContent);
        compProcess.setDescription(description);
        compProcess.setMemo(memo);
        compProcess.setDepStatus("0");
        compProcess.setDepId(depId);
        compProcess.setStatus("1");

        ccSession.save(compProcess);
        ccSession.flush();
    }

    /**
     *
     * @param cmPosSession
     * @param complaintInput
     * @param taskMngtId
     * @param subAdslll
     * @param complainId
     * @param staffIdLogin
     * @param nowDate
     * @param limitDate
     * @param contractNo
     */
    public void insertTaskManagement(Session cmPosSession, ComplaintInput complaintInput, Long taskMngtId,
            SubAdslLeaseline subAdslll, Long complainId, Long staffIdLogin, Date nowDate, Date limitDate, String contractNo) {

        TaskManagement tm = new TaskManagement();
        String taskName = "[BCCS_CC]Complain problem: " + complainId + ", service " + subAdslll.getServiceType();
        tm.setTaskMngtId(taskMngtId);
        tm.setCustReqId(complainId);
        tm.setUserId(staffIdLogin);
        tm.setTaskName(taskName);
        tm.setProgress(Constants.TASK_PROGRESS_WORKING);
        tm.setStatus(Constants.STATUS_USE);
        tm.setSourceType(Constants.TASK_SOURCE_TYPE_CC);
        tm.setJobCode(Constants.JOB_TYPE_COMPLAINT);
        tm.setContractNo(contractNo);
        tm.setProductCode(subAdslll.getProductCode());
        tm.setCreateDate(nowDate);
        tm.setName(complaintInput.getComplainerName());
        tm.setTelFax(complaintInput.getComplainerPhone());
        tm.setAreaCode(subAdslll.getDeployAreaCode());
        tm.setDeployAddress(complaintInput.getComplainerAddress());
        tm.setSubId(complaintInput.getSubId());
        tm.setLimitDate(limitDate);
        tm.setDslamId(subAdslll.getStationId());
        tm.setAccount(subAdslll.getAccount());
        tm.setServiceType(subAdslll.getServiceType());

        cmPosSession.save(tm);
        cmPosSession.flush();
    }

    /**
     *
     * @param cmPosSession
     * @param taskShopMngtId
     * @param taskMngtId
     * @param receiveShopId
     * @param staffIdLogin
     * @param nowDate
     */
    public void insertTaskShopManagement(Session cmPosSession, Long taskShopMngtId, Long taskMngtId, Long receiveShopId,
            Long staffIdLogin, Date nowDate) {

        TaskShopManagement tsm = new TaskShopManagement();
        tsm.setTaskShopMngtId(taskShopMngtId);
        tsm.setTaskMngtId(taskMngtId);
        tsm.setStageId(Constants.STAGE_REPAIR_PROBLEM);
        tsm.setShopId(receiveShopId);
        tsm.setUserId(staffIdLogin);
        tsm.setStatus(Constants.STATUS_USE);
        tsm.setProgress(Constants.TASK_SHOP_PROGRESS_START);
        tsm.setDescription("[BCCS_CC]Complain task");
        tsm.setCreateDate(nowDate);

        cmPosSession.save(tsm);
        cmPosSession.flush();
    }

    /**
     * Update table COMPLAIN
     *
     * @param complainId
     * @param isVip
     * @param ccSession
     * @return
     */
    public int updateIsVipAccountComplain(Long complainId, Long isVip, Session ccSession) {
        StringBuilder sql = new StringBuilder();
        sql.append(" UPDATE CC_OWNER.complain SET comp_product_id = ? ");
        sql.append(" WHERE complain_id = ? ");

        Query query = ccSession.createSQLQuery(sql.toString());
        query.setLong(0, isVip);
        query.setLong(1, complainId);

        int result = query.executeUpdate();

        return result;
    }

    /**
     * check account is VIP
     *
     * @param cmPosSession
     * @param subId
     * @return
     */
    public boolean checkAccountVip(Session cmPosSession, Long subId) {
        boolean isVip = false;
        String sql = " SELECT NVL(infra_type, 'AON'), SERVICE_TYPE, PRODUCT_CODE, SUB_TYPE "
                + " FROM CM_POS2.sub_adsl_ll s WHERE sub_id = ? ";
        Query query = cmPosSession.createSQLQuery(sql).setParameter(0, subId);
        List result = query.list();
        if (result != null && !result.isEmpty()) {
            Object[] arr = (Object[]) result.get(0);
            String subType = arr[3] == null ? "" : arr[3].toString();
            String productCode = arr[2] == null ? "" : arr[2].toString();
            isVip = new CompGetDataDAO().isAccountVip(cmPosSession, subType, productCode);
        }
        return isVip;
    }

    /**
     *
     * @param cmPosSession
     * @param subId
     * @param nowDate
     * @return
     */
    public Date getLimitDate(Session cmPosSession, Long subId, Date nowDate) {
        Calendar cStart = Calendar.getInstance();
        cStart.setTime(nowDate);
        /*Neu thoi gian tu 17h30 - 7h59 default la 8h hom sau*/
        if (cStart.get(Calendar.HOUR_OF_DAY) > 17 || (cStart.get(Calendar.HOUR_OF_DAY) == 17 && cStart.get(Calendar.MINUTE) >= 30)) {
            cStart.add(Calendar.DATE, 1);
            cStart.set(Calendar.HOUR_OF_DAY, 8);
            cStart.set(Calendar.MINUTE, 0);
            cStart.set(Calendar.SECOND, 0);
            cStart.set(Calendar.MILLISECOND, 0);
        } else if (cStart.get(Calendar.HOUR_OF_DAY) < 8) {
            cStart.set(Calendar.HOUR_OF_DAY, 8);
            cStart.set(Calendar.MINUTE, 0);
            cStart.set(Calendar.SECOND, 0);
            cStart.set(Calendar.MILLISECOND, 0);
        }
        nowDate = cStart.getTime();
        /*Get limit date from KPI deadline*/
        Long hourDeadline = null;
        HashMap<String, Long> mapKpiDeadline = CacheCC.getListKpiDeadlineTask(cmPosSession);
        if (mapKpiDeadline != null) {
            String sql = " SELECT NVL(infra_type, 'AON'), SERVICE_TYPE, PRODUCT_CODE, SUB_TYPE "
                    + " FROM CM_POS2.sub_adsl_ll s WHERE sub_id = ? ";
            Query query = cmPosSession.createSQLQuery(sql).setParameter(0, subId);
            List result = query.list();
            if (result != null && !result.isEmpty()) {
                Object[] arr = (Object[]) result.get(0);
                String infraType = arr[0] == null ? "" : arr[0].toString();
                String subType = arr[3] == null ? "" : arr[3].toString();
                String productCode = arr[2] == null ? "" : arr[2].toString();
                String serviceType = arr[1] == null ? "" : arr[1].toString();
                boolean isVip = new CompGetDataDAO().isAccountVip(cmPosSession, subType, productCode);
                hourDeadline = mapKpiDeadline.get("COMPLAIN" + "_" + serviceType + "_" + infraType + "_" + (isVip ? "1" : "0"));
            }
        }
        if (hourDeadline != null) {
            Calendar cEnd = Calendar.getInstance();
            cEnd.setTime(nowDate);
            cEnd.add(Calendar.HOUR, hourDeadline.intValue());
            return cEnd.getTime();
        }
        return nowDate;
    }

    /**
     *
     * @param ccSession
     * @param serviceCode
     * @return
     */
    public List<ServiceType> getListServiceType(Session ccSession, String serviceCode) {
        StringBuilder sql = new StringBuilder();
        HashMap<String, Object> params = new HashMap<String, Object>();
        sql.append(" SELECT st.service_type_id serviceTypeId, st.name, st.description ");
        sql.append(" FROM service_type st WHERE st.isactive = 1 ");

        if (serviceCode != null && !serviceCode.trim().isEmpty()) {
            sql.append(" AND LOWER(st.code) = :serviceCode ");
            params.put("serviceCode", serviceCode.trim().toLowerCase());
        }

        Query query = ccSession.createSQLQuery(sql.toString())
                .addScalar("serviceTypeId", Hibernate.LONG)
                .addScalar("name", Hibernate.STRING)
                .addScalar("description", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ServiceType.class));

        buildParameter(query, params);
        List<ServiceType> result = query.list();
        return result;
    }

    public List<ComplaintEx> getComplaintHistory(Session ccSession, String isdn, Long complaintId, int rate) {
        String sql = "SELECT   c.complain_id complainId,\n"
                + "         TO_CHAR (c.accept_date, 'dd/mm/yyyy hh24:mi:ss') acceptDate,\n"
                + "         c.comp_content compContent,\n"
                + "         TO_CHAR (c.cust_limit_date, 'dd/mm/yyyy hh24:mi:ss') proLimitDate,\n"
                + "         c.status,\n"
                + "         c.end_user staffInfo,\n"
                + "         (SELECT   case when instr(name, '_') > 0 then  SUBSTR(name, 4, LENGTH(name)- 3) else name end\n"
                + "            FROM   comp_type t\n"
                + "           WHERE   t.comp_type_id = c.comp_type_id) preResult,\n"
                + "         c.isdn errorPhone,\n"
                + "         c.complainer_phone complainerPhone\n "
                + "  FROM   complain c\n"
                + " WHERE       c.isdn = ?\n"
                + "         AND c.accept_date >= SYSDATE - 30\n"
                + "         AND c.status <> 4 ";
        if (complaintId != null && complaintId > 0) {
            sql += " AND c.complain_id = ? ";
        }
        if (rate > 0) {
            //sql += " and c.status = 3 and c.rate is null ";
            return new ArrayList<ComplaintEx>();
        }
        sql += " order by c.accept_date desc ";
        Query query = ccSession.createSQLQuery(sql.toString())
                .addScalar("complainId", Hibernate.LONG)
                .addScalar("acceptDate", Hibernate.STRING)
                .addScalar("compContent", Hibernate.STRING)
                .addScalar("proLimitDate", Hibernate.STRING)
                .addScalar("status", Hibernate.STRING)
                .addScalar("staffInfo", Hibernate.STRING)
                .addScalar("errorPhone", Hibernate.STRING)
                .addScalar("preResult", Hibernate.STRING)
                .addScalar("complainerPhone", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ComplaintEx.class));
        query.setParameter(0, isdn);
        if (complaintId != null && complaintId > 0) {
            query.setParameter(1, complaintId);
        }
        return query.list();
    }
}
