/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.AccountInfra;
import com.viettel.bccs.cm.model.BoxCode;
import com.viettel.bccs.cm.model.FrasUpdateLog;
import com.viettel.bccs.cm.model.LenghtCable;
import com.viettel.bccs.cm.model.LocationODF;
import com.viettel.bccs.cm.model.LocationSN;
import com.viettel.bccs.cm.model.TechInfo;
import com.viettel.bccs.cm.model.portOnBox;
import com.viettel.bccs.cm.supplier.AccountInfrasSupplier;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author nhandv
 */
public class AccountInfrasBussiness extends BaseBussiness {

    protected AccountInfrasSupplier supplier = new AccountInfrasSupplier();

    public AccountInfrasBussiness() {
        logger = Logger.getLogger(AccountInfrasBussiness.class);
    }

    public List<AccountInfra> getLstAccountInfra(Session cmPosSession,
            String staffID, String account, String infrasType,
            String infrasStatus, String btsCode,
            String staffCode, Integer start, Integer max,
            String connectorCode, String deviceCode, String shopPath) {
        List<AccountInfra> lst = supplier.getLstAccountInfra(cmPosSession,
                staffID, account, infrasType, infrasStatus, btsCode,
                staffCode, start, max, connectorCode, deviceCode, shopPath);
        return lst;
    }
    
    public Integer getCountAccount(Session cmPosSession,
            String staffID, String account, String infrasType,
            String infrasStatus, String btsCode, String staffCode,
            String connectorCode, String deviceCode, String shopPath) {
        Integer rs = supplier.getCountAccount(cmPosSession,
                staffID, account, infrasType, infrasStatus, btsCode, staffCode,
                connectorCode, deviceCode, shopPath);
        return rs;
    }
    
    public List<BoxCode> getLstCabinet(Session cmPosSession,
            String stationCode) {
        List<BoxCode> lst = supplier.getLstCabinet(cmPosSession, stationCode);
        return lst;
    }

    public List<portOnBox> getLstCabinetPort(Session cmPosSession, String stationCode) {
        List<portOnBox> lst = supplier.getLstCabinetPort(cmPosSession, stationCode);
        return lst;
    }

    public List<LenghtCable> getLenghtCable(Session cmPosSession, String account) {
        List<LenghtCable> lst = supplier.getLenghCap(cmPosSession, account);
        return lst;
    }

    public List<Object> getInfraStructureOfAccount(Session nims, boolean isGpon, String stationCode) {
        List<Object> lst = supplier.getInfraStructureOfAccount(nims, isGpon, stationCode);
        return lst;
    }

    public boolean checkAccountByStaff(Session cmPosSession, String account, String staffID) {
        return supplier.checkAccountbyStaff(cmPosSession, account, staffID);
    }

    public LocationODF getLocationODF(Session cmPosSession, String odfCode, boolean isAON,
            String connectorCode, String accountIsdn) {
        return supplier.getXYODF_AON(cmPosSession, odfCode, isAON, connectorCode, accountIsdn);
    }

    public LocationODF getLocationAccount(Session cmPosSession, String accountIsdn) {
        return supplier.getLocationAccount(cmPosSession,  accountIsdn);
    }
    
    public LocationSN getLocationSNCode(Session cmNimsSession, String deviceCode) {
        return supplier.getLocationSNCode(cmNimsSession,  deviceCode);
    }
    
    public void updateSub_ADSL_ll(Session cmPosSession, Double x, Double y, String account) {
        supplier.updateSub_ADSL_ll(cmPosSession, x, y, account);
    }

    public void insertFrasUpdateLog(Session cmPosSession, FrasUpdateLog frasUpdateLog) {
        supplier.insertFrasUpdateLog(cmPosSession, frasUpdateLog);
    }
    
    public List<TechInfo> getAllConnectorOfStation(Session nims, Long stationId, String stationCode){
         return supplier.getAllConnectorOfStation(nims, stationId, stationCode);
    }
}
