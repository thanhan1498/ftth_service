package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.ActionAudit;
import com.viettel.bccs.cm.supplier.ActionAuditSupplier;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ActionAuditBussiness extends BaseBussiness {

    protected ActionAuditSupplier supplier = new ActionAuditSupplier();

    public ActionAuditBussiness() {
        logger = Logger.getLogger(ActionAuditBussiness.class);
    }

    public ActionAudit insert(Session cmPosSession, Date issueDatetime, String actionCode, Long reasonId, String shopCode, String userName, String pkType, Long pkId, String ip, String description) {
        return supplier.insert(cmPosSession, issueDatetime, actionCode, reasonId, shopCode, userName, pkType, pkId, ip, description);
    }
}
