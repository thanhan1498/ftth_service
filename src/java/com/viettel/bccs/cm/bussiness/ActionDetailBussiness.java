package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.ActionDetail;
import com.viettel.bccs.cm.supplier.ActionDetailSupplier;
import com.viettel.bccs.cm.util.ConvertUtils;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ActionDetailBussiness extends BaseBussiness {

    protected ActionDetailSupplier supplier = new ActionDetailSupplier();

    public ActionDetailBussiness() {
        logger = Logger.getLogger(ActionDetailBussiness.class);
    }

    public ActionDetail insert(Session cmPosSession, Long actionAuditId, String tableName, Long rowId, String colName, Object oldValue, Object newValue, Date issueDateTime) {
        return supplier.insert(cmPosSession, actionAuditId, tableName, rowId, colName, ConvertUtils.toStringValue(oldValue), ConvertUtils.toStringValue(newValue), issueDateTime);
    }
}
