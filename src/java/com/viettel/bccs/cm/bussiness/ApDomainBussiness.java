package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.ApDomain;
import com.viettel.bccs.cm.supplier.ApDomainSupplier;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ApDomainBussiness extends BaseBussiness {

    protected ApDomainSupplier supplier = new ApDomainSupplier();

    public ApDomainBussiness() {
        logger = Logger.getLogger(ApDomainBussiness.class);
    }

    public List<ApDomain> findByType(Session cmPosSession, String type) {
        List<ApDomain> result = null;
        if (type == null) {
            return result;
        }
        type = type.trim();
        if (type.isEmpty()) {
            return result;
        }
        result = supplier.findByType(cmPosSession, type, Constants.STATUS_USE);
        return result;
    }

    public List<ApDomain> findByTypeCode(Session cmPosSession, String type, String code) {
        List<ApDomain> result = null;
        if (type == null || code == null) {
            return result;
        }
        type = type.trim();
        if (type.isEmpty()) {
            return result;
        }
        code = code.trim();
        if (code.isEmpty()) {
            return result;
        }
        result = supplier.findByTypeCode(cmPosSession, type, code, Constants.STATUS_USE);
        return result;
    }

    public List<ApDomain> findByTypeGroup(Session cmPosSession, String type, String groupCode) {
        List<ApDomain> result = null;
        if (type == null || groupCode == null) {
            return result;
        }
        type = type.trim();
        if (type.isEmpty()) {
            return result;
        }
        groupCode = groupCode.trim();
        if (groupCode.isEmpty()) {
            return result;
        }
        result = supplier.findByTypeGroup(cmPosSession, type, groupCode, Constants.STATUS_USE);
        return result;
    }

    public List<ApDomain> getLineTypes(Session cmPosSession, String serviceType) {
        List<ApDomain> result;
        if (Constants.SERVICE_ALIAS_LEASEDLINE.equals(serviceType)) {
            result = findByType(cmPosSession, Constants.AP_DOMAIN_TYPE_LINE_TYPE_LL);
            return result;
        }
        result = findByTypeGroup(cmPosSession, Constants.AP_DOMAIN_TYPE_LINE_TYPE, serviceType);
        return result;
    }

    public boolean isMandatoryTrasaction(Session cmPosSession, String actionCode) {
        boolean result = false;
        if (actionCode == null) {
            return result;
        }
        actionCode = actionCode.trim();
        if (actionCode.isEmpty()) {
            return result;
        }
        List<ApDomain> domains = getApDomainByTypeAndCode(cmPosSession, Constants.AP_DOMAIN_TYPE_ACTION_DOC, actionCode);
        if (domains == null || domains.isEmpty()) {
            return result;
        }
        String value = new ApParamBussiness().getUserManualConfig(cmPosSession, Constants.MANDATORY_TRANSACTION_ACTION);
        if (value != null) {
            for (ApDomain domain : domains) {
                if (domain != null) {
                    if (value.equals(domain.getGroupCode())) {
                        result = true;
                        return result;
                    }
                }
            }
        }
        return result;
    }

    public List<ApDomain> getApDomainByTypeAndCode(Session cmPosSession, String type, String code) {
        if (code == null) {
            return findByType(cmPosSession, type);
        }
        code = code.trim();
        if (code.isEmpty()) {
            return findByType(cmPosSession, type);
        }
        return findByTypeCode(cmPosSession, type, code);
    }
}
