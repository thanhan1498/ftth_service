package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.supplier.ApParamSupplier;
import com.viettel.bccs.cm.util.Constants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ApParamBussiness extends BaseBussiness {

    protected ApParamSupplier supplier = new ApParamSupplier();

    public ApParamBussiness() {
        logger = Logger.getLogger(ApParamBussiness.class);
    }

    public List<ApParam> findByType(Session cmPosSession, String paramType) {
        List<ApParam> result = null;
        if (paramType == null) {
            return result;
        }
        paramType = paramType.trim();
        if (paramType.isEmpty()) {
            return result;
        }
        result = supplier.findByType(cmPosSession, paramType, Constants.STATUS_USE);
        return result;
    }

    public List<ApParam> findByTypeCode(Session cmPosSession, String paramType, String paramCode) {
        List<ApParam> result = null;
        if (paramType == null || paramCode == null) {
            return result;
        }
        paramType = paramType.trim();
        if (paramType.isEmpty()) {
            return result;
        }
        paramCode = paramCode.trim();
        if (paramCode.isEmpty()) {
            return result;
        }
        result = supplier.findByTypeCode(cmPosSession, paramType, paramCode, Constants.STATUS_USE);
        return result;
    }

    public String getValueByTypeCode(Session cmPosSession, String paramType, String paramCode) {
        List<ApParam> params = findByTypeCode(cmPosSession, paramType, paramCode);
        if (params != null && !params.isEmpty()) {
            for (ApParam param : params) {
                if (param != null) {
                    String value = param.getParamValue();
                    if (value != null) {
                        value = value.trim();
                        if (!value.isEmpty()) {
                            return value;
                        }
                    }
                }
            }
        }
        return null;
    }

    public String getUserManualConfig(Session cmPosSession, String paramType) {
        return getValueByTypeCode(cmPosSession, paramType, Constants.USER_MANUAL_CONFIG);
    }

    /**
     * @author : duyetdk
     * @des: find param value: reason & suggest
     * @since 15-03-2018
     */
    public List<ApParam> findReasonAndSuggest(Session cmPosSession, String paramCode) {
        if (paramCode == null || paramCode.trim().isEmpty()) {
            return null;
        }
        List<ApParam> result = supplier.find(cmPosSession, paramCode, true);
        return result;
    }

    /**
     * @author : duyetdk
     * @des: find param value: reason
     * @since 15-03-2018
     */
    public ApParam findReason(Session cmPosSession, String paramCode) {
        ApParam result;
        if (paramCode == null || paramCode.trim().isEmpty()) {
            return null;
        }
        List<ApParam> objs = supplier.find(cmPosSession, paramCode, false);
        result = (ApParam) getBO(objs);
        return result;
    }

    /**
     * @author : duyetdk
     * @des: find param value: reason
     * @since 15-03-2018
     */
    public List<ApParam> findReasonConfig(Session cmPosSession, String paramCode) {
        List<ApParam> result = supplier.find(cmPosSession, paramCode, false);
        return result;
    }

    //phuonghc 06092020
    public Map<String, String> getUserManualConfig(Session cmPosSession) {
        return supplier.getUserManualConfig(cmPosSession);
    }

}
