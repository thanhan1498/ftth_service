package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.Area;
import com.viettel.bccs.cm.supplier.AreaSupplier;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

public class AreaBussiness extends BaseBussiness {

    protected AreaSupplier supplier = new AreaSupplier();

    public AreaBussiness() {
        logger = Logger.getLogger(AreaBussiness.class);
    }

    public Area findById(Session cmPosSession, String areaCode) {
        Area result = null;
        if (areaCode == null) {
            return result;
        }
        areaCode = areaCode.trim();
        if (areaCode.isEmpty()) {
            return result;
        }
        List<Area> objs = supplier.findById(cmPosSession, areaCode, Constants.STATUS_USE);
        result = (Area) getBO(objs);
        return result;
    }

    public List<Area> findProvinces(Session cmPosSession) {
        List<Area> result = supplier.findProvinces(cmPosSession, Constants.STATUS_USE);
        return result;
    }

    public List<Area> findDistricts(Session cmPosSession, String province) {
        List<Area> result = null;
        if (province == null) {
            return result;
        }
        province = province.trim();
        if (province.isEmpty()) {
            return result;
        }
        result = supplier.findDistricts(cmPosSession, province, Constants.STATUS_USE);
        return result;
    }

    public List<Area> findPrecincts(Session cmPosSession, String province, String district) {
        List<Area> result = null;
        if (province == null || district == null) {
            return result;
        }
        province = province.trim();
        if (province.isEmpty()) {
            return result;
        }
        district = district.trim();
        if (district.isEmpty()) {
            return result;
        }
        result = supplier.findPrecincts(cmPosSession, province, district, Constants.STATUS_USE);
        return result;
    }
}
