package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.Bank;
import com.viettel.bccs.cm.supplier.BankSupplier;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class BankBussiness extends BaseBussiness {

    protected BankSupplier supplier = new BankSupplier();

    public BankBussiness() {
        logger = Logger.getLogger(BankBussiness.class);
    }

    public Long count(Session cmPosSession, String bankCode, String name) {
        Long result = supplier.count(cmPosSession, bankCode, name, Constants.STATUS_USE);
        return result;
    }

    public List<Bank> find(Session cmPosSession, String bankCode, String name, Integer start, Integer max) {
        List<Bank> result = supplier.find(cmPosSession, bankCode, name, Constants.STATUS_USE, start, max);
        return result;
    }
}
