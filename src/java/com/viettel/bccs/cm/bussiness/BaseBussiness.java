package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.brcd.common.util.PerformanceLogger;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class BaseBussiness {

    protected Logger logger = Logger.getLogger(PerformanceLogger.class);
    protected BaseSupplier baseSupplier = new BaseSupplier();

    public Long getSequence(Session session, String seq) {
        return baseSupplier.getSequence(session, seq);
    }

    public Date getSysDateTime(Session session) throws ParseException {
        return baseSupplier.getSysDateDb(session);
    }

    public String getStrSysDateTime(Session session) {
        return baseSupplier.getStrSysDateTime(session);
    }

    public void rollBackTransactions(Session... sessions) {
        baseSupplier.rollBackTransactions(sessions);
    }

    public void commitTransactions(Session... sessions) {
        baseSupplier.commitTransactions(sessions);
    }

    public void flushSessions(Session... sessions) {
        baseSupplier.flushSessions(sessions);
    }

    public void beginTransactions(Session... sessions) {
        baseSupplier.beginTransactions(sessions);
    }

    public void clearSessions(Session... sessions) {
        baseSupplier.clearSessions(sessions);
    }

    public void closeSessions(Session... sessions) {
        baseSupplier.closeSessions(sessions);
    }

    public Object getBO(List objs) {
        Object result = null;
        if (objs != null && !objs.isEmpty()) {
            for (Object obj : objs) {
                if (obj != null) {
                    result = obj;
                    return result;
                }
            }
        }
        return result;
    }
}
