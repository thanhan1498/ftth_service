package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.BusType;
import com.viettel.bccs.cm.supplier.BusTypeSupplier;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class BusTypeBussiness extends BaseBussiness {

    protected BusTypeSupplier supplier = new BusTypeSupplier();

    public BusTypeBussiness() {
        logger = Logger.getLogger(BusTypeBussiness.class);
    }

    public BusType findById(Session cmPosSession, String busType) {
        BusType result = null;
        if (busType == null) {
            return result;
        }
        busType = busType.trim();
        if (busType.isEmpty()) {
            return result;
        }
        List<BusType> busTypes = supplier.findById(cmPosSession, busType, Constants.STATUS_USE);
        result = (BusType) getBO(busTypes);
        return result;
    }
    
    public BusType findByIdPre(Session cmPreSession, String busType) {
        BusType result = null;
        if (busType == null) {
            return result;
        }
        busType = busType.trim();
        if (busType.isEmpty()) {
            return result;
        }
        List<BusType> busTypes = supplier.findByIdPre(cmPreSession, busType, Constants.STATUS_USE);
        result = (BusType) getBO(busTypes);
        return result;
    }
    
    public List<BusType> getListBusTypePre(Session cmPreSession) {
        return supplier.findByStatusPre(cmPreSession, Constants.STATUS_USE);
    }

    public List<BusType> findActives(Session cmPosSession) {
        List<BusType> result = supplier.findByStatus(cmPosSession, Constants.STATUS_USE);
        return result;
    }
}
