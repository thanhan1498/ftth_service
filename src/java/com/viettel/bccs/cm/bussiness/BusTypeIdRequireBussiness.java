package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.BusTypeIdRequire;
import com.viettel.bccs.cm.supplier.BusTypeIdRequireSupplier;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class BusTypeIdRequireBussiness extends BaseBussiness {

    protected BusTypeIdRequireSupplier supplier = new BusTypeIdRequireSupplier();

    public BusTypeIdRequireBussiness() {
        logger = Logger.getLogger(BusTypeIdRequireBussiness.class);
    }

    public boolean isExists(List<BusTypeIdRequire> idRequires, String idType) {
        boolean isExists = false;
        if (idRequires != null && !idRequires.isEmpty()) {
            for (BusTypeIdRequire idRequire : idRequires) {
                if (idRequire != null) {
                    Long type = idRequire.getIdType();
                    if (type != null && type.toString().trim().equals(idType)) {
                        isExists = true;
                        break;
                    }
                }
            }
        }
        return isExists;
    }

    public List<BusTypeIdRequire> findByBusType(Session cmPosSession, String busType) {
        List<BusTypeIdRequire> result = null;
        if (busType == null) {
            return result;
        }
        busType = busType.trim();
        if (busType.isEmpty()) {
            return result;
        }
        result = supplier.findByBusType(cmPosSession, busType, Constants.STATUS_USE);
        return result;
    }
}
