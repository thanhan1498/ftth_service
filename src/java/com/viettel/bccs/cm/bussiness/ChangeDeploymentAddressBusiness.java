/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.supplier.ChangingDeploymentAddressSupplier;
import com.viettel.brcd.ws.model.input.ChangeDeploymentAddressIn;
import com.viettel.brcd.ws.model.output.ParamOut;
import java.util.List;
import org.hibernate.Session;

/**
 * @since 02/09/2020 ChangeDeploymentAddressBusiness
 * @author phuonghc - hop
 */
public class ChangeDeploymentAddressBusiness extends BaseBussiness {

    public List<SubAdslLeaseline> getListAccountForSubscriber(Session sessionCmPos, String custId) {
        ChangingDeploymentAddressSupplier supplier = new ChangingDeploymentAddressSupplier();
        return supplier.getListAccountForSubscriber(sessionCmPos, custId);
    }

    public ParamOut doingChangeDeploymentAddress(Session sessionCmPos, Session sessionCmPre, Session sessionCc, Session sessionNims, Session sessionIm,
            ChangeDeploymentAddressIn input, String locale) throws Exception {
        ChangingDeploymentAddressSupplier supplier = new ChangingDeploymentAddressSupplier();
        ParamOut resultChanging = supplier.changeDeploymentAddress(sessionCmPos, sessionCmPre, sessionCc, sessionNims, sessionIm, input, locale);
        return resultChanging;
    }

    public List<Reason> getListReasonByType(Session sessionCmPos, String custId, String account) {
        ChangingDeploymentAddressSupplier supplier = new ChangingDeploymentAddressSupplier();
        return supplier.getListReasonByType(sessionCmPos, custId, account);
    }

}
