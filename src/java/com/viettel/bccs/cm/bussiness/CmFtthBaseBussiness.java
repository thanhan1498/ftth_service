/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness;

import com.opensymphony.xwork2.util.TextUtils;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.TechnicalConnector;
import com.viettel.bccs.cm.model.TechnicalStation;
import com.viettel.bccs.cm.model.ftth.CustomerBTSConnectorBean;
import com.viettel.bccs.cm.model.ftth.InterruptExtend;
import com.viettel.bccs.cm.model.ftth.InterruptReasonDetail;
import com.viettel.bccs.cm.model.ftth.InterruptReasonType;
import com.viettel.bccs.cm.model.ftth.InterruptSms;
import com.viettel.bccs.cm.model.ftth.Interruption;
import com.viettel.bccs.cm.model.ftth.SubscriberFtth;
import com.viettel.bccs.cm.model.ftth.SubscriberInterruptSms;
import com.viettel.bccs.cm.supplier.ApParamSupplier;
import com.viettel.bccs.cm.supplier.CmFtthBaseSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.DateTimeUtils;
import com.viettel.brcd.dao.pre.IsdnSendSmsDAO;
import com.viettel.brcd.ws.model.input.CreateInterruptFtthInput;
import com.viettel.brcd.ws.model.input.PushSmsInterruptFtthInput;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author partner1
 */
public class CmFtthBaseBussiness extends BaseBussiness {

    protected CmFtthBaseSupplier supplier = new CmFtthBaseSupplier();

    public CmFtthBaseBussiness() {
        logger = Logger.getLogger(CmFtthBaseBussiness.class);
    }

    public Shop getCurBranchUser(Session cmPosSession, Long staffId) {
        Shop result = supplier.getCurBranchUser(cmPosSession, staffId);
        return result;
    }

    public List<InterruptReasonType> getReasonType(Session cmPosSession) {
        List<InterruptReasonType> result = supplier.getReasonType(cmPosSession, null, Constants.STATUS_USE);
        return result;
    }

    public List<InterruptReasonDetail> getReasonDetail(Session cmPosSession, Long reasonTypeId) {
        List<InterruptReasonDetail> result = supplier.getReasonDetail(cmPosSession, reasonTypeId, Constants.STATUS_USE, null);
        return result;
    }

    public List<Shop> getBranchList(Session cmPosSession) {
        List<Shop> result = supplier.getBranchList(cmPosSession);
        return result;
    }

    public List<Staff> getStaffOfBranch(Session cmPosSession, Long branchShopId) {
        List<Staff> result = supplier.getStaffOfBranch(cmPosSession, branchShopId);
        return result;
    }

    public List<TechnicalStation> getBtsByBranch(Session cmPosSession, Long branchShopId) {
        List<TechnicalStation> result = supplier.getBtsByBranch(cmPosSession, branchShopId);
        return result;
    }

    public String createInterruptFtth(Session cmPosSession, Session cmPreSession, CreateInterruptFtthInput createInterruptFtthInput) throws Exception {
        String errorMessage = validateCreateInterrupt(createInterruptFtthInput);
        if (errorMessage != null && !"".equals(errorMessage)) {
            return errorMessage;
        }
        Long reasonTypeId = createInterruptFtthInput.getReasonTypeId();
        List<InterruptReasonType> lstReasonTypes = supplier.getReasonType(cmPosSession, reasonTypeId, Constants.STATUS_USE);
        if (lstReasonTypes == null || lstReasonTypes.isEmpty()) {
            return "Reason Type does not exists!";
        }
        Long reasonDetailId = createInterruptFtthInput.getReasonDetailId();
        List<InterruptReasonDetail> lstReasonDetails = supplier.getReasonDetail(cmPosSession, reasonTypeId, Constants.STATUS_USE, reasonDetailId);
        if (lstReasonDetails == null || lstReasonDetails.isEmpty()) {
            return "Reason Detail does not exists!";
        }
        Date nowDate = getSysDateTime(cmPosSession);
        //insert interruption
        Date affectFromDate = DateTimeUtils.toDateddMMyyyy(createInterruptFtthInput.getAffectFromDate());
        Date affectToDate = DateTimeUtils.toDateddMMyyyy(createInterruptFtthInput.getAffectToDate());
        List<String> listCustomerType = createInterruptFtthInput.getLstCustomerType();
        Long influenceScope = createInterruptFtthInput.getInfluenceScope();
        Long influenceLevel = createInterruptFtthInput.getInfluenceLevel();
        List<String> listServiceInterrupted = createInterruptFtthInput.getServiceInterrupted();
        String moreDescription = createInterruptFtthInput.getMoreDescription();
        String documentName = createInterruptFtthInput.getDocumentName();
        byte[] documentData = createInterruptFtthInput.getDocumentData();
        List<TechnicalStation> listBts = createInterruptFtthInput.getListBts();
        List<TechnicalConnector> listConnectors = createInterruptFtthInput.getListConnector();
        String staffCode = createInterruptFtthInput.getStaffCode();
        String shopCode = createInterruptFtthInput.getShopCode();
        Long shopId = createInterruptFtthInput.getShopId();

        Long interruptionId = getSequence(cmPosSession, "INTERRUPTION_SEQ");
        supplier.insertInterruption(cmPosSession, interruptionId, reasonTypeId, reasonDetailId,
                affectFromDate, affectToDate, listCustomerType,
                influenceScope, influenceLevel, listServiceInterrupted,
                moreDescription, documentName, documentData,
                staffCode, nowDate, shopId);
        //Them thong tin BTS/CONNECTOR va subscriber

        if (influenceScope.equals(0L)) {
            //su co toan mang
            for (String serviceType : listCustomerType) {
                List<SubscriberFtth> lstSubscriberFtths = getListSubscriberFtth(cmPosSession, serviceType, influenceScope, null, null);
                for (SubscriberFtth subscriberFtth : lstSubscriberFtths) {
                    Long subInterruptSmsId = getSequence(cmPosSession, "SUB_INTERRUPTION_SMS_seq");
                    String isdnAccount = subscriberFtth.getIsdnAccount();
                    String customerName = subscriberFtth.getCustomerName();
                    String productCode = subscriberFtth.getProductCode();
                    String contractNumber = subscriberFtth.getContractNumber();
                    String deploymentAddress = subscriberFtth.getDeploymentAddress();
                    Long subId = subscriberFtth.getSubId();
                    String telMobile = subscriberFtth.getTelMobile();
                    String infraType = subscriberFtth.getInfraType();
                    supplier.insertSubscriberInterruptSms(cmPosSession, subInterruptSmsId, interruptionId, staffCode, nowDate, isdnAccount, customerName,
                            productCode, serviceType, contractNumber, deploymentAddress, null, null, null, null,
                            subId, telMobile, infraType);
                }
            }
        } else if (influenceScope.equals(1L)) {
            //su co BTS
            for (TechnicalStation technicalStation : listBts) {
                Long interruptExtendId = getSequence(cmPosSession, "INTERRUPTION_EXTEND_seq");
                supplier.insertInterruptionExtend(cmPosSession, interruptExtendId, interruptionId, 1L, technicalStation.getStationId(), null, technicalStation.getStationCode(), technicalStation.getStationCode());
                for (String serviceType : listCustomerType) {
                    List<SubscriberFtth> lstSubscriberFtths = getListSubscriberFtth(cmPosSession, serviceType, influenceScope, technicalStation.getStationId(), null);
                    for (SubscriberFtth subscriberFtth : lstSubscriberFtths) {
                        Long subInterruptSmsId = getSequence(cmPosSession, "SUB_INTERRUPTION_SMS_seq");
                        String isdnAccount = subscriberFtth.getIsdnAccount();
                        String customerName = subscriberFtth.getCustomerName();
                        String productCode = subscriberFtth.getProductCode();
                        String contractNumber = subscriberFtth.getContractNumber();
                        String deploymentAddress = subscriberFtth.getDeploymentAddress();
                        Long subId = subscriberFtth.getSubId();
                        String telMobile = subscriberFtth.getTelMobile();
                        String infraType = subscriberFtth.getInfraType();
                        supplier.insertSubscriberInterruptSms(cmPosSession, subInterruptSmsId, interruptionId, staffCode, nowDate, isdnAccount, customerName,
                                productCode, serviceType, contractNumber, deploymentAddress, technicalStation.getStationCode(), technicalStation.getStationId(), null, null,
                                subId, telMobile, infraType);
                    }
                }
            }
        } else {
            //su co connector
            for (TechnicalConnector technicalConnector : listConnectors) {
                Long interruptExtendId = getSequence(cmPosSession, "INTERRUPTION_EXTEND_seq");
                supplier.insertInterruptionExtend(cmPosSession, interruptExtendId, interruptionId, 2L, technicalConnector.getStationId(), technicalConnector.getConnectorId(), technicalConnector.getConnectorCode(), technicalConnector.getConnectorCode());
                for (String serviceType : listCustomerType) {
                    List<SubscriberFtth> lstSubscriberFtths = getListSubscriberFtth(cmPosSession, serviceType, influenceScope, technicalConnector.getStationId(), technicalConnector.getConnectorId());
                    for (SubscriberFtth subscriberFtth : lstSubscriberFtths) {
                        Long subInterruptSmsId = getSequence(cmPosSession, "SUB_INTERRUPTION_SMS_seq");
                        String isdnAccount = subscriberFtth.getIsdnAccount();
                        String customerName = subscriberFtth.getCustomerName();
                        String productCode = subscriberFtth.getProductCode();
                        String contractNumber = subscriberFtth.getContractNumber();
                        String deploymentAddress = subscriberFtth.getDeploymentAddress();
                        Long subId = subscriberFtth.getSubId();
                        String telMobile = subscriberFtth.getTelMobile();
                        String infraType = subscriberFtth.getInfraType();
                        supplier.insertSubscriberInterruptSms(cmPosSession, subInterruptSmsId, interruptionId, staffCode, nowDate, isdnAccount, customerName,
                                productCode, serviceType, contractNumber, deploymentAddress, null, technicalConnector.getStationId(), technicalConnector.getConnectorCode(), technicalConnector.getConnectorId(),
                                subId, telMobile, infraType);
                    }
                }
            }
        }

        //nhan tin ky thuat AP
        List<ApParam> listApParams = new ApParamSupplier().findParam(cmPosSession, "INTERRUPTION_PARAM", "APPROVE_ISDN", null);
        if (listApParams != null && !listApParams.isEmpty()) {
            String isdn = listApParams.get(0).getParamValue();
            if (isdn != null && !"".equals(isdn)) {
                String content = shopCode + " has just updated the service interruption, please check and approve for them";
                new IsdnSendSmsDAO().insertIsdnSendSms(cmPreSession, isdn, content, nowDate, staffCode, shopCode, "CM_FTTH");
            }
        }

        return "";
    }

    public String validateCreateInterrupt(CreateInterruptFtthInput createInterruptFtthInput) {
        Long reasonTypeId = createInterruptFtthInput.getReasonTypeId();
        if (reasonTypeId == null) {
            return "Reason type must be not null!";
        }

        Long reasonDetailId = createInterruptFtthInput.getReasonDetailId();
        if (reasonDetailId == null) {
            return "Reason detail must be not null!";
        }

        String affectFromDate = createInterruptFtthInput.getAffectFromDate();
        if (affectFromDate == null || "".equals(affectFromDate)) {
            return "From date must be not null!";
        }

        String affectToDate = createInterruptFtthInput.getAffectToDate();
        if (affectToDate == null || "".equals(affectToDate)) {
            return "To date must be not null!";
        }

        List<String> lstCustomerType = createInterruptFtthInput.getLstCustomerType();
        if (lstCustomerType == null || lstCustomerType.isEmpty()) {
            return "Customer type must be not null!";
        }
        for (String customerType : lstCustomerType) {
            String customerTypeName = decodeCustomerType(customerType);
            if (customerTypeName == null || "".equals(customerTypeName)) {
                return "Customer Type incorrec, please check and choose again";
            }
        }
        Long influenceScope = createInterruptFtthInput.getInfluenceScope();
        if (influenceScope == null) {
            return "Influence Scope must be not null!";
        }

        List<TechnicalStation> listBts = createInterruptFtthInput.getListBts();
        if (influenceScope.equals(1L) && (listBts == null || listBts.isEmpty())) {
            return "BTS must be not null!";
        }

        List<TechnicalConnector> listConnector = createInterruptFtthInput.getListConnector();
        if (influenceScope.equals(2L) && (listConnector == null || listConnector.isEmpty())) {
            return "Connector must be not null!";
        }

        Long influenceLevel = createInterruptFtthInput.getInfluenceLevel();
        if (influenceLevel == null) {
            return "Influence Level must be not null!";
        }

        List<String> serviceInterrupted = createInterruptFtthInput.getServiceInterrupted();
        if (serviceInterrupted == null || serviceInterrupted.isEmpty()) {
            return "Service Interrupted must be not null!";
        }

        String documentName = createInterruptFtthInput.getDocumentName();
        byte[] documentData = createInterruptFtthInput.getDocumentData();
        if (documentName != null && !"".equals(documentName) && documentData == null) {
            return "Document import must enought Name and Data!";
        }

        String staffCode = createInterruptFtthInput.getStaffCode();
        if (staffCode == null || "".equals(staffCode)) {
            return "Staff code must be not null!";
        }

        String shopCode = createInterruptFtthInput.getShopCode();
        if (shopCode == null || "".equals(shopCode)) {
            return "Shop Code must be not null!";
        }

        Long shopId = createInterruptFtthInput.getShopId();
        if (shopId == null) {
            return "Shop Id must be not null!";
        }

        return "";
    }

    public List<SubscriberFtth> getListSubscriberFtth(Session cmPosSession, String serviceType, Long influenceScope, Long stationId, Long connectorId) {
        if ("P".equals(serviceType)) {
            return supplier.getListSubPstn(cmPosSession, influenceScope, stationId, connectorId);
        } else {
            return supplier.getListSubAdslLeaseLine(cmPosSession, serviceType, influenceScope, stationId, connectorId);
        }
    }

    public List<Interruption> getInterruptFtth(Session cmPosSession, String staffCode, Long reasonTypeId, Long reasonDetailId, String influenceScope, String fromDate, String toDate, Long shopId) throws Exception {
        List<Interruption> result = supplier.getInterruptFtth(cmPosSession, null, staffCode, reasonTypeId, reasonDetailId, influenceScope, fromDate, toDate, shopId);
        for (Interruption interruption : result) {
            List<InterruptReasonType> lstReasonTypes = supplier.getReasonType(cmPosSession, interruption.getReasonType(), null);
            if (lstReasonTypes != null && !lstReasonTypes.isEmpty()) {
                interruption.setReasonTypeName(lstReasonTypes.get(0).getName());
            }
            List<InterruptReasonDetail> lstReasonDetails = supplier.getReasonDetail(cmPosSession, null, null, interruption.getReasonDetail());
            if (lstReasonDetails != null && !lstReasonDetails.isEmpty()) {
                interruption.setReasonDetailName(lstReasonDetails.get(0).getName());
            }
            if (interruption.getStatus() != 0L && interruption.getStatus() != 3L) {
                interruption.setSmsStatusName(this.getSmsStatus(cmPosSession, interruption.getId(), interruption.getAffectCustomerType()));
            }
            Shop shop = new ShopBussiness().findById(cmPosSession, interruption.getShopId());
            interruption.setShopBranchName(shop.getName());
        }
        return result;
    }

    public List<Interruption> getDetailInterruptFtth(Session cmPosSession, Long interruptId) throws Exception {
        List<Interruption> result = supplier.getInterruptFtth(cmPosSession, interruptId, null, null, null, null, null, null, null);
        for (Interruption interruption : result) {
            Long influenceScope = interruption.getInfluenceScope();
            if (influenceScope.equals(0L)) {
                //su co toan mang
            } else if (influenceScope.equals(1L)) {
                //su co bts
                List<InterruptExtend> lstStations = supplier.getInterruptExtend(cmPosSession, interruptId, 1L);
                interruption.setLstStation(lstStations);
            } else {
                //su co connector
                List<InterruptExtend> lstStations = supplier.getInterruptExtend(cmPosSession, interruptId, 2L);
                interruption.setLstConnector(lstStations);
            }
        }
        return result;
    }

    public List<Interruption> getHistoryInterruptFtth(Session cmPosSession, Long interruptId) throws Exception {
        List<Interruption> result = supplier.getInterruptFtth(cmPosSession, interruptId, null, null, null, null, null, null, null);
        for (Interruption interruption : result) {
            List<InterruptSms> lstInterruptSms = supplier.getInterruptSms(cmPosSession, interruptId);
            interruption.setLstInterruptSms(lstInterruptSms);
        }
        return result;
    }

    public String approveInterruptFtth(Session cmPosSession, Long interruptId, String staffCode) throws Exception {
        Date nowDate = getSysDateTime(cmPosSession);
        List<Interruption> lstInterruptions = supplier.getInterruptFtth(cmPosSession, interruptId, null, null, null, null, null, null, null);
        for (Interruption interruption : lstInterruptions) {
            interruption.setStatus(1L);
            interruption.setUpdateDate(nowDate);
            interruption.setUpdateUser(staffCode);
            cmPosSession.update(interruption);
        }
        return "";
    }

    public String cancelInterruptFtth(Session cmPosSession, Long interruptId, String staffCode) throws Exception {
        Date nowDate = getSysDateTime(cmPosSession);
        List<Interruption> lstInterruptions = supplier.getInterruptFtth(cmPosSession, interruptId, null, null, null, null, null, null, null);
        for (Interruption interruption : lstInterruptions) {
            interruption.setStatus(3L);
            interruption.setUpdateDate(nowDate);
            interruption.setUpdateUser(staffCode);
            cmPosSession.update(interruption);
        }
        return "";
    }

    public String pushSmsInterruptFtth(Session cmPosSession, Session cmPreSession, PushSmsInterruptFtthInput pushSmsInterruptFtthInput) throws Exception {
        Date nowDate = getSysDateTime(cmPosSession);
        Long interruptId = pushSmsInterruptFtthInput.getInterruptId();
        List<String> lstCustomerType = pushSmsInterruptFtthInput.getLstCustomerType();
        String customerTypeStr = TextUtils.join(",", lstCustomerType);
        String content = pushSmsInterruptFtthInput.getContent();
        String staffCode = pushSmsInterruptFtthInput.getStaffCode();
        if (interruptId == null) {
            return "Interrupt Id null, please check and choose again!";
        }
        if (lstCustomerType == null || lstCustomerType.isEmpty()) {
            return "You must select type of customer, please check and choose again!";
        }
        if (content == null || "".equals(content)) {
            return "Content to send sms must be not null, please check and choose again!";
        }
        List<Interruption> lstInterruptions = supplier.getInterruptFtth(cmPosSession, interruptId, null, null, null, null, null, null, null);
        if (lstInterruptions == null || lstInterruptions.isEmpty()) {
            return "Interrupt not found, please check and choose again!";
        }
        Interruption interruption = lstInterruptions.get(0);
        if (!interruption.getAffectCustomerType().contains(customerTypeStr)) {
            return "This type of customer is not in the error update table, please check and choose again!";
        }
        Long numSendSms = 2L;
        List<ApParam> listApParams = new ApParamSupplier().findParam(cmPosSession, "INTERRUPTION_PARAM", "NUM_SEND_SMS", null);
        if (listApParams != null && !listApParams.isEmpty()) {
            String paramValue = listApParams.get(0).getParamValue();
            if (paramValue != null && !"".equals(paramValue)) {
                numSendSms = Long.parseLong(paramValue);
            }
        }
        for (String customerType : lstCustomerType) {
            String customerTypeName = decodeCustomerType(customerType);
            String infraType = "";
            if ("F".equals(customerType)) {
                infraType = "AON";
            } else if ("G".equals(customerType)) {
                infraType = "GPON";
            }
            if (customerTypeName == null || "".equals(customerTypeName)) {
                return "Customer Type incorrec, please check and choose again";
            }
            Long currentSendSms = supplier.getNumSendSmsByService(cmPosSession, interruptId, customerType);
            if (numSendSms <= currentSendSms) {
                return "This request is over the SMS quota with customer type:" + customerTypeName;
            }
            Long id = getSequence(cmPosSession, "INTERRUPTION_SMS_seq");
            supplier.insertInterruptSms(cmPosSession, id, interruptId, staffCode, nowDate, content, customerType, infraType);

            List<SubscriberInterruptSms> listSub = supplier.getSubscriberInterruptSms(cmPosSession, interruptId, customerType);
            for (SubscriberInterruptSms subscriberInterruptSms : listSub) {
                Long sendSmsId = new IsdnSendSmsDAO().insertIsdnSendSms(cmPreSession, subscriberInterruptSms.getTelMobile(), content, nowDate, staffCode, null, "CM_FTTH");
                String sendSmsIdStr = subscriberInterruptSms.getSendSmsId();
                if (sendSmsId != null) {
                    if (sendSmsIdStr == null || "".equals(sendSmsIdStr)) {
                        sendSmsIdStr = sendSmsId.toString();
                    } else {
                        sendSmsIdStr = "," + sendSmsId.toString();
                    }
                }
                subscriberInterruptSms.setSendSmsId(sendSmsIdStr);
                cmPosSession.update(subscriberInterruptSms);
            }
        }
        return "";
    }

    public String decodeCustomerType(String customerType) {
        if ("F".equals(customerType)) {
            return "FTTH AON";
        }
        if ("G".equals(customerType)) {
            return "FTTH GPON";
        }
        if ("A".equals(customerType)) {
            return "ADSL";
        }
        if ("L".equals(customerType)) {
            return "Leaseline";
        }
        if ("P".equals(customerType)) {
            return "PSTN";
        }
        return customerType;
    }

    public String getSmsStatus(Session cmPosSession, Long interruptionId, String affectCustomerType) {
        Long numSendSms = 2L;
        List<ApParam> listApParams = new ApParamSupplier().findParam(cmPosSession, "INTERRUPTION_PARAM", "NUM_SEND_SMS", null);
        if (listApParams != null && !listApParams.isEmpty()) {
            String paramValue = listApParams.get(0).getParamValue();
            if (paramValue != null && !"".equals(paramValue)) {
                numSendSms = Long.parseLong(paramValue);
            }
        }
        List<InterruptSms> lstInterruptSms = supplier.getServiceSmsNotYet(cmPosSession, interruptionId, numSendSms, affectCustomerType);
        if (lstInterruptSms != null && !lstInterruptSms.isEmpty()) {
            return "Not Yet";
        }
        return "Completed";
    }
    
    public List<CustomerBTSConnectorBean> countCustomerBTSConnector(Session cmPosSession, Long interruptId) throws Exception {
        List<CustomerBTSConnectorBean> result = supplier.countCustomerBTSConnector(cmPosSession, interruptId);
        return result;
    }
    
    public List<CustomerBTSConnectorBean> countCustomerServiceBtsConnector(Session cmPosSession, Long interruptId, Long btsId, Long connectorId) throws Exception {
        List<CustomerBTSConnectorBean> result = supplier.countCustomerServiceBtsConnector(cmPosSession, interruptId, btsId, connectorId);
        return result;
    }
    
    public String rejectInterruptFtth(Session cmPosSession, Long interruptId, String staffCode) throws Exception {
        Date nowDate = getSysDateTime(cmPosSession);
        List<Interruption> lstInterruptions = supplier.getInterruptFtth(cmPosSession, interruptId, null, null, null, null, null, null, null);
        for (Interruption interruption : lstInterruptions) {
            interruption.setStatus(2L);
            interruption.setUpdateDate(nowDate);
            interruption.setUpdateUser(staffCode);
            cmPosSession.update(interruption);
        }
        return "";
    }
}