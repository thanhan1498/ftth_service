package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.CmUserToken;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.supplier.CmUserTokenSupplier;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.common.util.PerformanceLogger;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import viettel.passport.client.ObjectToken;
import viettel.passport.client.UserToken;

public class CmUserTokenBusiness extends BaseBussiness {

    protected CmUserTokenSupplier supplier = new CmUserTokenSupplier();

    public CmUserTokenBusiness() {
        logger = Logger.getLogger(PerformanceLogger.class);
    }
    /*private final List<String> listComponentCTVShop3 = Arrays.asList(
            "INFRASTRUCTURE_MANAGER_SURVEY",
            "SP_BRCD.PAYMENT",
            "INFRASTRUCTURE_MANAGER_SURVEY",
            "SP_BRCD.REGISTER_SUB",
            "INFRASTRUCTURE_MANAGER_VIEW_DETAIL",
            "CREATE_REQUEST_INSERT",
            "CUSTOMER_MANAGER_VIEW_DETAIL");
    private final List<String> listComponentCTVShop5 = Arrays.asList(
            "SP_BRCD.TASK_MANAGEMENT",
            "UPDATE_TASK_MANAGEMENT",
            "REVOKE_TASK_MANAGEMENT",
            "SEARCH_TASK_MANAGEMENT");*/

    public CmUserToken getUserToken(String userName, String password, String locale, String shopType,boolean isN3P3) throws MalformedURLException, IOException, SAXException, ParserConfigurationException {
        CmUserToken result;
        LogUtils.info(logger, "PassportBusiness.getUserToken:userName=" + userName + ";password=" + password + ";locale=" + locale);
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="userName">
        if (userName == null) {
            result = new CmUserToken(Constants.ERROR_CODE_1, LabelUtil.getKey("error.userName", locale));
            return result;
        }
        userName = userName.trim();
        if (userName.isEmpty()) {
            result = new CmUserToken(Constants.ERROR_CODE_1, LabelUtil.getKey("error.userName", locale));
            return result;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="password">
        if (password == null) {
            result = new CmUserToken(Constants.ERROR_CODE_1, LabelUtil.getKey("error.passWord", locale));
            return result;
        }
        password = password.trim();
        if (password.isEmpty()) {
            result = new CmUserToken(Constants.ERROR_CODE_1, LabelUtil.getKey("error.passWord", locale));
            return result;
        }
        //</editor-fold>
        //</editor-fold>
        Date sTime = new Date();
        LogUtils.info(logger, "CmUserTokenBusiness.getUserToken:start time=" + sTime);
        UserToken vsaUserToken = supplier.getUserToken(userName, password, locale);
        sTime = new Date();
        LogUtils.info(logger, "CmUserTokenBusiness.getUserToken:end time=" + sTime);
        if (vsaUserToken == null) {
            result = new CmUserToken(Constants.ERROR_CODE_1, LabelUtil.getKey("login.vsa.account.access.denie", locale));
            return result;
        }

        result = new CmUserToken();
        List<String> menus = getMenus(vsaUserToken);
        if (isN3P3) //  menus.add("SP_BRCD.SABAYN3_P3");
        {
            menus.add("SP_BRCD.SABAYN3_P3");
        }
        result.setMenus(menus);
        List<String> components = getComponents(vsaUserToken);
        if (isN3P3) //  components.add("SP_BRCD.SABAYN3_P3");
        {
            components.add("SP_BRCD.SABAYN3_P3");
        }
        result.setComponents(components);
        if (Constants.SHOP_TYPE_SHOWROOM.toString().equals(shopType)) {
            /*cho phep CTV ban hang hien thi menu dau noi, cho phep gach no*/
            result.getComponents().addAll(Arrays.asList(ResourceUtils.getResource("ROLE_CTV_SHOP3").split(",")));
        } else if (Constants.SHOP_TYPE_AGENT_DELEGATE.toString().equals(shopType)) {
            /*cho phep CTV technical thien thi task_mangement*/
            result.getComponents().addAll(Arrays.asList(ResourceUtils.getResource("ROLE_CTV_SHOP5").split(",")));
        }
        result.setResultCode(Constants.ERROR_CODE_0);
        return result;
    }

    public List<String> getMenus(UserToken vsaUserToken) {
        List<String> menus = new ArrayList<String>();
        /*
         *@since 180222
         *@author Cuongdm
         *@des add deafault menu or component, crash app on ios when user have one role
         */
        menus.add("GUESS");
        List tokens = vsaUserToken.getObjectTokens();
        if (tokens != null && !tokens.isEmpty()) {
            for (Object obj : tokens) {
                if (obj != null) {
                    ObjectToken token = (ObjectToken) obj;
                    getMenus(menus, token);
                }
            }
        }
        return menus;
    }

    public void getMenus(List menus, ObjectToken token) {
        String url = token.getObjectUrl();
        if (url != null) {
            url = url.trim();
        }
        if (url != null && !url.isEmpty() && !"#".equals(url)) {
            menus.add(url);
        } else {
            List childs = token.getChildObjects();
            if (childs != null && !childs.isEmpty()) {
                for (Object obj : childs) {
                    if (obj != null) {
                        ObjectToken child = (ObjectToken) obj;
                        getMenus(menus, child);
                    }
                }
            }
        }
    }

    public List<String> getComponents(UserToken vsaUserToken) {
        List<String> components = new ArrayList<String>();
        /*
         *@since 180222
         *@author Cuongdm
         *@des add deafault menu or component, crash app on ios when user have one role
         */
        components.add("GUESS");
        List tokens = vsaUserToken.getComponentList();
        if (tokens != null && !tokens.isEmpty()) {
            for (Object obj : tokens) {
                if (obj != null) {
                    ObjectToken token = (ObjectToken) obj;
                    String code = token.getObjectCode();
                    if (code != null) {
                        code = code.trim();
                        if (!code.isEmpty()) {
                            components.add(code);
                        }
                    }
                }
            }
        }
        return components;
    }
}
