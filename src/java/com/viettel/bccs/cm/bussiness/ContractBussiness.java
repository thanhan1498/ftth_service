package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.dao.ContractOfferDAO;
import com.viettel.bccs.cm.dao.CurrBillCycleDAO;
import com.viettel.bccs.cm.dao.CustomerDAO;
import com.viettel.bccs.cm.dao.DeployRequirementDAO;
import com.viettel.bccs.cm.dao.OfferReqSubItemDAO;
import com.viettel.bccs.cm.dao.OfferRequestDAO;
import com.viettel.bccs.cm.dao.OfferRequestIsdnDiscountedDAO;
import com.viettel.bccs.cm.dao.OfferRequestSubDAO;
import com.viettel.bccs.cm.dao.OfferSubDAO;
import com.viettel.bccs.cm.dao.ReasonDAO;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.dao.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.dao.SubReqAdslLlDAO;
import com.viettel.bccs.cm.dao.TaskManagementDAO;
import com.viettel.bccs.cm.dao.TaskShopManagementDAO;
import com.viettel.bccs.cm.dao.TaskStaffManagementDAO;
import com.viettel.bccs.cm.dao.TaskStageItemDAO;
import com.viettel.brcd.common.util.DateTimeUtils;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.brcd.common.util.ValidateUtils;
import com.viettel.bccs.cm.model.ActionAudit;
import com.viettel.bccs.cm.model.Area;
import com.viettel.bccs.cm.model.Contract;
import com.viettel.bccs.cm.model.ContractBank;
import com.viettel.bccs.cm.model.ContractOffer;
import com.viettel.bccs.cm.model.CurrBillCycle;
import com.viettel.bccs.cm.model.CustRequest;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.OfferIsdnDiscounted;
import com.viettel.bccs.cm.model.OfferReqSubItem;
import com.viettel.bccs.cm.model.OfferRequest;
import com.viettel.bccs.cm.model.OfferRequestIsdnDiscounted;
import com.viettel.bccs.cm.model.OfferRequestSub;
import com.viettel.bccs.cm.model.OfferSub;
import com.viettel.bccs.cm.model.OfferSubItem;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubReqAdslLl;
import com.viettel.bccs.cm.model.TaskManagement;
import com.viettel.bccs.cm.model.TaskShopManagement;
import com.viettel.bccs.cm.model.TaskStaffManagement;
import com.viettel.bccs.cm.model.TaskStageItem;
import com.viettel.bccs.cm.model.Token;
import com.viettel.bccs.cm.supplier.ContractSupplier;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.supplier.nims.lockinfras.LockAndUnlockInfrasResponse;
import com.viettel.brcd.ws.supplier.nims.lockinfras.NimsWsLockUnlockInfrasBusiness;
import com.viettel.brcd.ws.supplier.nims.lockinfras.ResultForm;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ContractBussiness extends BaseBussiness {

    protected ContractSupplier supplier = new ContractSupplier();

    public ContractBussiness() {
        logger = Logger.getLogger(ContractBussiness.class);
    }

    public Contract findById(Session cmPosSession, Long contractId) {
        Contract result = null;
        if (contractId == null || contractId <= 0L) {
            return result;
        }
        List<Contract> objs = supplier.findById(cmPosSession, contractId, Constants.CONTRACT_STATUS_INACTIVE);
        result = (Contract) getBO(objs);
        return result;
    }
    
    public Contract findByIdActive(Session cmPosSession, Long contractId) {
        Contract result = null;
        if (contractId == null || contractId <= 0L) {
            return result;
        }
        List<Contract> objs = supplier.findById(cmPosSession, contractId, Constants.CONTRACT_STATUS_ACTIVE);
        result = (Contract) getBO(objs);
        return result;
    }
    
    /**
     * @author VangHV1
     * @description Ham ky moi hop dong ( khong ky phu luc hop dong ) dich vu A,
     * F, L
     * @param cmPosSession
     * @param wsRequest
     * @param locale
     * @return
     * @throws Exception
     */
    public String signContract(Session cmPosSession, Long shopId, Long staffId, String noticeCharge, String email, String telMobile, String telFax,
            String printMethod, String nickName, String nickDomain, String payMethod, String bankCode, String account, String accountName, String bankContractNo,
            String bankContractDate, Long custId, Long reasonId, String province, String district, String precinct, Long reqId, String receiveInvoice, String locale) throws ParseException, IllegalAccessException, InvocationTargetException {
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="shopId">
        if (shopId == null || shopId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.shopId");
            return mess;
        }
        Shop shop = new ShopBussiness().findById(cmPosSession, shopId);
        if (shop == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "shop");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="staffId">
        if (staffId == null || staffId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.staffId");
            return mess;
        }
        Staff staff = new StaffBussiness().findById(cmPosSession, staffId);
        if (staff == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "staff");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="noticeCharge">
        if (noticeCharge == null) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.noticeCharge");
            return mess;
        }
        noticeCharge = noticeCharge.trim();
        if (noticeCharge.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.noticeCharge");
            return mess;
        }
        Integer length = ReflectUtils.getColumnLength(Contract.class, "noticeCharge");
        if (length != null && length > 0 && noticeCharge.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("noticeCharge", locale), length);
            return mess;
        }
        //<editor-fold defaultstate="collapsed" desc="email">
        if (Constants.NOTICE_CHARGE_BY_EMAIL.equals(noticeCharge) || Constants.NOTICE_CHARGE_BY_SMS_EMAIL.equals(noticeCharge)) {
            if (email == null) {
                mess = LabelUtil.getKey("validate.required", locale, "sign.contract.email");
                return mess;
            }
            email = email.trim();
            if (email.isEmpty()) {
                mess = LabelUtil.getKey("validate.required", locale, "sign.contract.email");
                return mess;
            }
        }
        if (email != null) {
            email = email.trim();
            if (!email.isEmpty()) {
                length = ReflectUtils.getColumnLength(Contract.class, "email");
                if (length != null && length > 0 && email.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("email", locale), length);
                    return mess;
                }
                if (!ValidateUtils.isEmail(email)) {
                    mess = LabelUtil.getKey("validate.invalid", locale, "email");
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="telMobile">
        if (Constants.NOTICE_CHARGE_BY_SMS.equals(noticeCharge) || Constants.NOTICE_CHARGE_BY_SMS_EMAIL.equals(noticeCharge)) {
            if (telMobile == null) {
                mess = LabelUtil.getKey("validate.required", locale, "sign.contract.telMobile");
                return mess;
            }
            telMobile = telMobile.trim();
            if (telMobile.isEmpty()) {
                mess = LabelUtil.getKey("validate.required", locale, "sign.contract.telMobile");
                return mess;
            }
        }
        /*if (telMobile != null) {
            telMobile = telMobile.trim();
            if (!telMobile.isEmpty()) {
                length = ReflectUtils.getColumnLength(Contract.class, "telMobile");
                if (length != null && length > 0 && telMobile.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("telMobile", locale), length);
                    return mess;
                }
                if (!ValidateUtils.isIsdn(telMobile)) {
                    mess = LabelUtil.getKey("validate.invalid", locale, "telMobile");
                    return mess;
                }
            }
        }*/
        //</editor-fold>
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="telFax">
        /*if (telFax != null) {
            telFax = telFax.trim();
            if (!telFax.isEmpty()) {
                length = ReflectUtils.getColumnLength(Contract.class, "telFax");
                if (length != null && length > 0 && telFax.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("telFax", locale), length);
                    return mess;
                }
                if (!ValidateUtils.isIsdn(telFax)) {
                    mess = LabelUtil.getKey("validate.invalid", locale, "telFax");
                    return mess;
                }
            }
        }*/
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="printMethod">
        if (printMethod == null) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.printMethod");
            return mess;
        }
        printMethod = printMethod.trim();
        if (printMethod.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.printMethod");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Contract.class, "printMethodCode");
        if (length != null && length > 0 && printMethod.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("printMethod", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="nickName">
        if (nickName != null) {
            nickName = nickName.trim();
            if (!nickName.isEmpty()) {
                length = ReflectUtils.getColumnLength(Contract.class, "nickName");
                if (length != null && length > 0 && nickName.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("nickName", locale), length);
                    return mess;
                }
                if (nickDomain == null) {
                    mess = LabelUtil.getKey("validate.required.when", locale, "nickDomain", "sign.contract.nickName");
                    return mess;
                }
                nickDomain = nickDomain.trim();
                if (nickDomain.isEmpty()) {
                    mess = LabelUtil.getKey("validate.required.when", locale, "nickDomain", "sign.contract.nickName");
                    return mess;
                }
                length = ReflectUtils.getColumnLength(Contract.class, "nickDomain");
                if (length != null && length > 0 && nickDomain.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("nickDomain", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="payMethod">
        if (payMethod == null) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.payMethod");
            return mess;
        }
        payMethod = payMethod.trim();
        if (payMethod.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.payMethod");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Contract.class, "payMethodCode");
        if (length != null && length > 0 && payMethod.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("payMethod", locale), length);
            return mess;
        }
        Date nowDate = getSysDateTime(cmPosSession);
        Date bankDate = null;
        if (Constants.PAY_METHOD_CODE_ORDER_TO_RECEIPT.equals(payMethod) || Constants.PAY_METHOD_CODE_ORDER_TO_PAY.equals(payMethod)) {
            //<editor-fold defaultstate="collapsed" desc="bankCode">
            if (bankCode == null) {
                mess = LabelUtil.getKey("validate.required", locale, "sign.contract.bankCode");
                return mess;
            }
            bankCode = bankCode.trim();
            if (bankCode.isEmpty()) {
                mess = LabelUtil.getKey("validate.required", locale, "sign.contract.bankCode");
                return mess;
            }
            length = ReflectUtils.getColumnLength(ContractBank.class, "bankCode");
            if (length != null && length > 0 && bankCode.length() > length) {
                mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("bankCode", locale), length);
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="account">
            if (account == null) {
                mess = LabelUtil.getKey("validate.required", locale, "sign.contract.account");
                return mess;
            }
            account = account.trim();
            if (account.isEmpty()) {
                mess = LabelUtil.getKey("validate.required", locale, "sign.contract.account");
                return mess;
            }
            length = ReflectUtils.getColumnLength(ContractBank.class, "account");
            if (length != null && length > 0 && account.length() > length) {
                mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("account", locale), length);
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="accountName">
            if (accountName != null) {
                accountName = accountName.trim();
                if (!accountName.isEmpty()) {
                    length = ReflectUtils.getColumnLength(ContractBank.class, "accountName");
                    if (length != null && length > 0 && accountName.length() > length) {
                        mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("accountName", locale), length);
                        return mess;
                    }
                }
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="bankContractNo">
            if (bankContractNo != null) {
                bankContractNo = bankContractNo.trim();
                if (!bankContractNo.isEmpty()) {
                    length = ReflectUtils.getColumnLength(ContractBank.class, "bankContractNo");
                    if (length != null && length > 0 && bankContractNo.length() > length) {
                        mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("bankContractNo", locale), length);
                        return mess;
                    }
                }
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="bankContractDate">
            if (bankContractDate != null) {
                bankContractDate = bankContractDate.trim();
                if (!bankContractDate.isEmpty()) {
                    if (!ValidateUtils.isDateddMMyyyy(bankContractDate)) {
                        mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("bankContractDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                        return mess;
                    }
                    try {
                        bankDate = DateTimeUtils.toDateddMMyyyy(bankContractDate);
                    } catch (Exception ex) {
                        LogUtils.error(logger, ex.getMessage());
                        mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("bankContractDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                        return mess;
                    }
                    if (bankDate == null || bankDate.before(nowDate)) {
                        mess = LabelUtil.getKey("validate.less.than.now.date", locale, "bankContractDate");
                        return mess;
                    }
                }
            }
            //</editor-fold>
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="customer">
        if (custId == null || custId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.customer.custId");
            return mess;
        }
        Customer customer = new CustomerBussiness().findById(cmPosSession, custId);
        if (customer == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "customer");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="reason">
        if (reasonId == null || reasonId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.reason");
            return mess;
        }
        Reason reason = new ReasonBussiness().findById(cmPosSession, reasonId);
        if (reason == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "reason");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="province">
        if (province == null) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.province");
            return mess;
        }
        province = province.trim();
        if (province.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.province");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Contract.class, "province");
        if (length != null && length > 0 && province.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("province", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="district">
        if (district == null) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.district");
            return mess;
        }
        district = district.trim();
        if (district.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.district");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Contract.class, "district");
        if (length != null && length > 0 && district.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("district", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="precinct">
        if (precinct == null) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.precinct");
            return mess;
        }
        precinct = precinct.trim();
        if (precinct.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.precinct");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Contract.class, "precinct");
        if (length != null && length > 0 && precinct.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("precinct", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="address">
        String areaCode = province + district + precinct;
        Area area = new AreaBussiness().findById(cmPosSession, areaCode);
        if (area == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "area");
            return mess;
        }
        String address = area.getFullName();
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="subReq">
        if (reqId == null || reqId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "sign.contract.subReq");
            return mess;
        }
        SubReqAdslLl subReq = new SubReqAdslLlBussiness().findById(cmPosSession, reqId);
        if (subReq == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "subReq");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Neu la yeu cau dau moi thue bao">
        if (Constants.IS_NEW_SUB_TRUE.equals(subReq.getIsNewSub())) {
            Long subId = subReq.getSubId();
            LogUtils.info(logger, "ContractBussiness.validateSignContract:subId=" + subId);
            SubAdslLeaseline sub = new SubAdslLeaselineBussines().findById(cmPosSession, subId);
            if (sub != null) {
                mess = LabelUtil.getKey("validate.exists", locale, "subscriber");
                return mess;
            }
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="custRequest">
        Long custReqId = subReq.getCustReqId();
        LogUtils.info(logger, "ContractBussiness.validateSignContract:custReqId=" + custReqId);
        CustRequest custRequest = new CustRequestBussiness().findById(cmPosSession, custReqId);
        if (custRequest == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "custRequest");
            return mess;
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="offerRequest">
        Long offerReqId = subReq.getReqOfferId();
        OfferRequest offerRequest = new OfferRequestDAO().findById(cmPosSession, offerReqId);
        if (offerRequest == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "offerRequest");
            return mess;
        }
        String offerReqType = offerRequest.getType();
        if (offerReqType != null) {
            offerReqType = offerReqType.trim();
            if (Constants.GROUP_TYPE_CORPORATE.equals(offerReqType) || Constants.GROUP_TYPE_FAMILY.equals(offerReqType)) {
                mess = LabelUtil.formatKey("sign.contract.group.only.sign.offer", locale, offerReqType);
                return mess;
            }
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="receiveInvoice">
        if (Constants.PAY_METHOD_CODE_CARD_TO_PAY.equals(payMethod)) {// Neu thanh toan qua the cao ==> mac dinh khong nhan hoa don
            receiveInvoice = Constants.NOT_RECEIVE_INVOICE;
        } else if (receiveInvoice != null) {
            receiveInvoice = receiveInvoice.trim();
        }
        //</editor-fold>
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="save contract">
        Contract contract = new Contract();

        contract.setBillCycleFrom(Constants.CONTRACT_BILL_CYCLE_DEFAULT);
        CurrBillCycle cycle = new CurrBillCycleDAO().findById(cmPosSession, Constants.CONTRACT_BILL_CYCLE_DEFAULT);
        if (cycle != null) {
            contract.setBillCycleFromCharging(cycle.getChargingCode());
        }

        contract.setSignDate(nowDate);
//        contract.setEffectDate(nowDate);// Khi ky hop dong de truong effect_date = null
        contract.setDateCreate(nowDate);
        contract.setUserCreate(staff.getStaffCode());

        contract.setNoticeCharge(noticeCharge);
        contract.setEmail(email);
        contract.setTelMobile(telMobile);
        contract.setTelFax(telFax);
        contract.setPrintMethodCode(printMethod);
        contract.setNickName(nickName);
        contract.setNickDomain(nickDomain);
        contract.setReceiveInvoice(receiveInvoice);
        contract.setReceiveInvoice(Constants.NOT_RECEIVE_INVOICE);// Mac dinh luu la khong nhan hoa don cuoc
        contract.setPayMethodCode(payMethod);
        contract.setProvince(province);
        contract.setDistrict(district);
        contract.setPrecinct(precinct);
        contract.setAddress(address);
        contract.setPayAreaCode(areaCode);

        contract.setCustId(customer.getCustId());
        contract.setPayer(customer.getName());
        contract.setContractType(Constants.CONTRACT_TYPE_NORMAL);
        contract.setContractTypeCode(Constants.CONTRACT_TYPE_NORMAL);
        Long numberSubscriber = 1L;// Ky moi hop dong nen so luong thue bao = 1
        contract.setNumOfSubscribers(numberSubscriber);
        contract.setStatus(Constants.CONTRACT_STATUS_INACTIVE);

        String serviceType = subReq.getServiceType();
        contract.setServiceTypes(serviceType);// Ky moi hop dong nen dich vu la dich vu cua thue bao
        contract.setMainSubId(subReq.getSubId());
        contract.setMainIsdn(subReq.getAccount());

        Long contractId = getSequence(cmPosSession, Constants.CONTRACT_ID_SEQ);
        contract.setContractId(contractId);
        contract.setContractNo(generateContractNo(cmPosSession, contractId, subReq.getShopCode(), nowDate));

        LogUtils.info(logger, "ContractBussiness.signContract:start save Contract;Contract=" + LogUtils.toJson(contract));
        cmPosSession.save(contract);
        cmPosSession.flush();
        LogUtils.info(logger, "ContractBussiness.signContract:end save Contract");

        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Neu chon hinh thuc thanh toan uy nhiem">
        if (Constants.PAY_METHOD_CODE_ORDER_TO_RECEIPT.equals(payMethod) || Constants.PAY_METHOD_CODE_ORDER_TO_PAY.equals(payMethod)) {
            ContractBank contractBank = new ContractBank();
            Long contractBankId = getSequence(cmPosSession, Constants.CONTRACT_BANK_SEQ);
            contractBank.setId(contractBankId);
            contractBank.setBankCode(bankCode);
            contractBank.setAccount(account);
            contractBank.setAccountName(accountName);
            contractBank.setBankContractNo(bankContractNo);
            contractBank.setBankContractDate(bankDate);
            contractBank.setStatus(Constants.CONTRACT_BANK_USE_STATUS);
            contractBank.setContractId(contractId);

            LogUtils.info(logger, "ContractBussiness.signContract:end save ContractBank;ContractBank=" + LogUtils.toJson(contractBank));
            cmPosSession.save(contractBank);
            cmPosSession.flush();
            LogUtils.info(logger, "ContractBussiness.signContract:end save ContractBank");
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="luu log action">
        ActionAuditBussiness auditDAO = new ActionAuditBussiness();
        LogUtils.info(logger, "ContractBussiness.signContract:start save ActionAudit");
        auditDAO.insert(cmPosSession, nowDate, Constants.ACTION_CONTRACT_NEW, reasonId, shop.getShopCode(), staff.getStaffCode(), Constants.ACTION_AUDIT_PK_TYPE_CONTRACT, contractId, Constants.WS_IP, "Sign new contract");
        LogUtils.info(logger, "ContractBussiness.signContract:end save ActionAudit");
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Neu la yeu cau dau moi thue bao">
        if (Constants.IS_NEW_SUB_TRUE.equals(subReq.getIsNewSub())) {
            LogUtils.info(logger, "ContractBussiness.signContract:isNewSub");
            //<editor-fold defaultstate="collapsed" desc="save SubAdslLeaseline">
            SubAdslLeaseline adslSub = new SubAdslLeaseline(subReq);
            adslSub.setContractId(contractId);
            adslSub.setUserCreated(staff.getStaffCode());
            adslSub.setStaDatetime(nowDate);
            adslSub.setIsInfoCompleted(Constants.IS_NOT_INFO_COMPLETED);// Luu la chua hoan thien thong tin

            LogUtils.info(logger, "ContractBussiness.signContract:start save SubAdslLeaseline;SubAdslLeaseline=" + LogUtils.toJson(adslSub));
            cmPosSession.save(adslSub);
            cmPosSession.flush();
            LogUtils.info(logger, "ContractBussiness.signContract:end save SubAdslLeaseline");
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="cap nhat yeu cau hotline">
            LogUtils.info(logger, "ContractBussiness.signContract:start update DeployRequirement;subId=" + adslSub.getSubId());
            new DeployRequirementDAO().updateStatusBySubId(cmPosSession, adslSub.getSubId(), Constants.DEPLOY_REQ_STATUS_REGIS_CONTRACT);
            LogUtils.info(logger, "ContractBussiness.signContract:end update DeployRequirement");
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="update subRequest">
            subReq.setStatus(Constants.SUB_REQ_STATUS_SIGN_CONTRACT);
            subReq.setCurrentStepId(Constants.STEP_ID_REG_CONTRACT);

            LogUtils.info(logger, "ContractBussiness.signContract:start update SubReqAdslLl;SubReqAdslLl=" + LogUtils.toJson(subReq));
            cmPosSession.update(subReq);
            cmPosSession.flush();
            LogUtils.info(logger, "ContractBussiness.signContract:start update SubReqAdslLl");
            //</editor-fold>
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="update custRequest">
        Long numsSubInCustReq = 0L;
        Long numsOfSubInContractOffer = 1L;// Ky moi hop dong nen so luong thue bao trong contract offer = 1
        numsSubInCustReq += numsOfSubInContractOffer;
        Long numOfSubscribersRemain = custRequest.getNumOfSubscribers() - numsSubInCustReq;
        custRequest.setNumOfSubscribers(numOfSubscribersRemain);
        if (numOfSubscribersRemain == 0) {
            custRequest.setStatus(Constants.STATUS_CUST_REQUEST_RESPONSE);
        }
        LogUtils.info(logger, "ContractBussiness.signContract:start save CustRequest;CustRequest=" + LogUtils.toJson(custRequest));
        cmPosSession.update(custRequest);
        cmPosSession.flush();
        LogUtils.info(logger, "ContractBussiness.signContract:end save CustRequest");
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="save contractOffer">
        ContractOffer contractOffer = new ContractOffer();
        Long contractOfferId = getSequence(cmPosSession, Constants.CONTRACT_OFFER_ID_SEQ);
        contractOffer.setContractOfferId(contractOfferId);
        contractOffer.setContractId(contractId);
        contractOffer.setQualities(numsOfSubInContractOffer);
        contractOffer.setOfferId(offerRequest.getOfferId());
        contractOffer.setReqOfferId(offerRequest.getReqOfferId());
        contractOffer.setStatus(Constants.STATUS_USE);
        contractOffer.setIsBundle(offerRequest.getIsBundle());
        contractOffer.setGroupIsActivate(Constants.CONTRACT_OFFER_INACTIVE);
        if (offerReqType != null && Constants.CONTRACT_OFFER_IS_BUNDLE.equals(offerRequest.getIsBundle())) {
            if (Constants.GROUP_TYPE_CORPORATE.equals(offerReqType) || Constants.GROUP_TYPE_FAMILY.equals(offerReqType) || Constants.GROUP_TYPE_CORPORATE.equals(offerReqType)) {
                Long groupId = getSequence(cmPosSession, Constants.VPN_GROUP_SEQ);
                contractOffer.setGroupId(groupId.toString());
            }
            contractOffer.setType(offerReqType);
            contractOffer.setCode(contractOffer.getGroupId());
            contractOffer.setName(contractOffer.getGroupId());
        }

        LogUtils.info(logger, "ContractBussiness.signContract:start save ContractOffer;ContractOffer=" + LogUtils.toJson(contractOffer));
        cmPosSession.save(contractOffer);
        cmPosSession.flush();
        LogUtils.info(logger, "ContractBussiness.signContract:end save ContractOffer");
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="update offerRequest">
        offerRequest.setStatus(Constants.OFFER_REQUEST_SIGN_CONTRACT);
        LogUtils.info(logger, "ContractBussiness.signContract:start update OfferRequest;OfferRequest=" + LogUtils.toJson(offerRequest));
        cmPosSession.update(offerRequest);
        cmPosSession.flush();
        LogUtils.info(logger, "ContractBussiness.signContract:end update OfferRequest");
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="save offerSub">
        if (Constants.CONTRACT_OFFER_IS_BUNDLE.equals(offerRequest.getIsBundle())) {
            LogUtils.info(logger, "ContractBussiness.signContract:isContractOfferBundle");
            LogUtils.info(logger, "ContractBussiness.signContract:call OfferRequestSubDAO.findBySubReqId;offerRequestSubId=" + LogUtils.toJson(subReq.getId()));
            List<OfferRequestSub> offerRequestSubs = new OfferRequestSubDAO().findBySubReqId(cmPosSession, subReq.getId());
            if (offerRequestSubs != null && !offerRequestSubs.isEmpty()) {
                for (OfferRequestSub offerReqSub : offerRequestSubs) {
                    if (offerReqSub != null) {
                        OfferSub offerSub = new OfferSub();
                        Long offerSubId = getSequence(cmPosSession, Constants.OFFER_SUB_ID_SEQ);
                        BeanUtils.copyProperties(offerSub, offerReqSub);
                        offerSub.setContractOfferId(contractOfferId);
                        offerSub.setOfferSubId(offerSubId);
                        offerSub.setSubId(subReq.getSubId());
                        offerSub.setIsNewSub(subReq.getIsNewSub());
                        offerSub.setStatus(Constants.STATUS_USE);

                        LogUtils.info(logger, "ContractBussiness.signContract:start save OfferSub;OfferSub=" + LogUtils.toJson(offerSub));
                        cmPosSession.save(offerSub);
                        cmPosSession.flush();
                        LogUtils.info(logger, "ContractBussiness.signContract:end save OfferSub");
                    }
                }
            }
        } else {
            OfferSub offerSub = new OfferSub();
            Long offerSubId = getSequence(cmPosSession, Constants.OFFER_SUB_ID_SEQ);
            offerSub.setContractOfferId(contractOfferId);
            offerSub.setOfferSubId(offerSubId);
            offerSub.setSubId(subReq.getSubId());
            offerSub.setIsNewSub(subReq.getIsNewSub());

            offerSub.setStatus(Constants.STATUS_USE);
            offerSub.setTelService(serviceType);

            LogUtils.info(logger, "ContractBussiness.signContract:start save OfferSub;OfferSub=" + LogUtils.toJson(offerSub));
            cmPosSession.save(offerSub);
            cmPosSession.flush();
            LogUtils.info(logger, "ContractBussiness.signContract:end save OfferSub");
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="save danh sach cac thue bao duoc giam gia cuoc goi tu so thue bao nay">
        List<OfferRequestIsdnDiscounted> discounteds = new OfferRequestIsdnDiscountedDAO().findBySubReqId(cmPosSession, subReq.getId());
        if (discounteds != null && !discounteds.isEmpty()) {
            for (OfferRequestIsdnDiscounted item : discounteds) {
                if (item != null) {
                    OfferIsdnDiscounted discounted = new OfferIsdnDiscounted();
                    BeanUtils.copyProperties(discounted, item);
                    discounted.setContractOfferId(contractOfferId);
                    discounted.setSubId(subReq.getSubId());
                    discounted.setDiscountIsdn(item.getDiscountIsdn());

                    LogUtils.info(logger, "ContractBussiness.signContract:start save OfferIsdnDiscounted;OfferIsdnDiscounted=" + LogUtils.toJson(discounted));
                    cmPosSession.save(discounted);
                    cmPosSession.flush();
                    LogUtils.info(logger, "ContractBussiness.signContract:end save OfferIsdnDiscounted");
                }
            }
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Cap nhat danh sach dich vu chu nhom thanh toan">
        List<OfferReqSubItem> items = new OfferReqSubItemDAO().findBySubIdAndSubReqId(cmPosSession, subReq.getSubId(), subReq.getId());
        if (items != null && !items.isEmpty()) {
            for (OfferReqSubItem item : items) {
                if (item != null) {
                    OfferSubItem off = new OfferSubItem();
                    BeanUtils.copyProperties(off, item);
                    off.setContractOfferId(contractOfferId);
                    off.setSubId(subReq.getSubId());
                    off.setServiceId(item.getServiceId());
                    off.setStatus(Constants.STATUS_USE);

                    LogUtils.info(logger, "ContractBussiness.signContract:start save OfferSubItem;OfferSubItem=" + LogUtils.toJson(off));
                    cmPosSession.save(off);
                    cmPosSession.flush();
                    LogUtils.info(logger, "ContractBussiness.signContract:end save OfferSubItem");
                }
            }
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="close CustRequest">
        LogUtils.info(logger, "ContractBussiness.signContract:start close CustRequest;CustReqId=" + subReq.getCustReqId());
        new CustRequestBussiness().closeRequest(cmPosSession, subReq.getCustReqId());
        LogUtils.info(logger, "ContractBussiness.signContract:end close CustRequest");
        //</editor-fold>
        mess = null;
        return mess;
    }

    /**
     * @author Vietnn6
     * @description Ham huy hop dong dich vu A, F, L
     * @param cmPosSession
     * @param wsRequest
     * @param locale
     * @return
     * @throws Exception
     */
    public String cancelContract(Session cmPosSession, Session imSession, Session pmSession, Long contractId, String tokenVal, Long reasonId, Long reqId, String locale) throws Exception {
        LogUtils.info(logger, "ContractBussiness.cancelContract:locale=" + locale + ";contractId=" + contractId + ";token=" + tokenVal + ";reasonId=" + reasonId);
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">
        Token token = new TokenBussiness().findByToken(cmPosSession, tokenVal);
        //<editor-fold defaultstate="collapsed" desc="staffId">
        if (token.getStaffId() == null || token.getStaffId() <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "staffId");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="staff">
        Staff staff = new StaffDAO().findById(cmPosSession, token.getStaffId());
        if (staff == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "staff");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="shopId">
        if (staff.getShopId() == null || staff.getShopId() <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "shopId");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="shop">
        Shop shop = new ShopDAO().findById(cmPosSession, staff.getShopId());
        if (shop == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "shop");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="reasonID">
        if (reasonId == null || reasonId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "reason");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="reason">
        Reason reason = new ReasonDAO().findById(cmPosSession, reasonId);
        if (reason == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "reason");
            return mess;
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Contract">
        Contract contract = findById(cmPosSession, contractId);
        if (contract == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "contract");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="customer">
        Long custId = contract.getCustId();
        Customer customer = new CustomerDAO().findById(cmPosSession, custId);
        if (customer == null) {
            return LabelUtil.getKey("validate.not.exists", locale, "customer");
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="kiem tra hop dong trong trang thai da ky va tiep nhan tu smartPhone">
        SubReqAdslLl subReq = new SubReqAdslLlDAO().findByContractId(cmPosSession, reqId, contractId);
        if (subReq == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "subReq");
            return mess;
        }
        //</editor-fold>
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Huy hop dong">
        Date nowDate = getSysDateTime(cmPosSession);
        //<editor-fold defaultstate="collapsed" desc="luu log action">
        ActionAuditBussiness auditDAO = new ActionAuditBussiness();
        StringBuilder des = new StringBuilder()
                .append("Cancel subscriber contract, Request contractId=").append(contractId);
        LogUtils.info(logger, "ContractBussiness.cancelContract:start save ActionAudit");
        ActionAudit audit = auditDAO.insert(cmPosSession, nowDate, Constants.ACTION_CONTRACT_CANCEL, reason.getReasonId(), shop.getShopCode(), staff.getStaffCode(), Constants.ACTION_AUDIT_PK_TYPE_SUBSCRIBER, contractId, Constants.WS_IP, des.toString());
        LogUtils.info(logger, "ContractBussiness.cancelContract:end save ActionAudit");
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="cancelContract">
        updateStatus(cmPosSession, contract, Constants.CONTRACT_STATUS_CANCEL, staff.getStaffCode(), nowDate, nowDate, audit.getActionAuditId(), nowDate);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Huy cac goi san pham dich vu ContractOffer">
        List<ContractOffer> contractOffers = new ContractOfferDAO().findByContractId(cmPosSession, contractId);
        if (contractOffers != null && !contractOffers.isEmpty()) {
            for (ContractOffer contractOf : contractOffers) {
                //<editor-fold defaultstate="collapsed" desc="Huy offer request">
                OfferRequest offerReq = new OfferRequestDAO().findById(cmPosSession, contractOf.getReqOfferId());

                if (offerReq != null) {

                    if (!Constants.OFFER_REQUEST_CANCEL.equals(offerReq.getStatus())) {
                        offerReq.setStatus(Constants.OFFER_REQUEST_CANCEL);
                        cmPosSession.update(offerReq);
                        cmPosSession.flush();

                    }//<editor-fold defaultstate="collapsed" desc="huy yeu cau khach hang neu co">

                    boolean checkCancelReq = new OfferRequestDAO().isAllCancel(cmPosSession, offerReq.getCustReqId(), contractOf.getReqOfferId());
                    if (checkCancelReq) {
                        CustRequest custReq = new CustRequestBussiness().findById(cmPosSession, offerReq.getCustReqId());
                        custReq.setStatus(Constants.STATUS_CUST_REQUEST_CANCEL);
                        cmPosSession.update(custReq);
                        cmPosSession.flush();
                    }
                    //</editor-fold>
                }

                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="Huy phu luc thue bao offerSub">
                List<OfferSub> offerSub = new OfferSubDAO().finBySubContractOffferId(cmPosSession, contractOf.getContractOfferId());
                if (offerSub != null && !offerSub.isEmpty()) {
                    for (OfferSub offer : offerSub) {

                        SubAdslLeaseline subAdslLL = new SubAdslLeaselineDAO().findById(cmPosSession, offer.getSubId());

                        if (subAdslLL != null) {
                            //<editor-fold defaultstate="collapsed" desc="huy yeu cau">

                            if (Constants.SUB_STATUS_NORMAL_INACTIVE.equals(subAdslLL.getStatus())) {
                                SubReqAdslLl subReqAdsl = new SubReqAdslLlDAO().findBySubId(cmPosSession, offer.getSubId());
                                if (subReqAdsl != null) {
                                    Object oldValue = subReqAdsl.getStatus();
                                    subReqAdsl.setStatus(Constants.SUB_REQ_STATUS_CANCEL);
                                    Object newValue = subReqAdsl.getStatus();
                                    cmPosSession.update(subReqAdsl);
                                    cmPosSession.flush();
                                    new ActionDetailBussiness().insert(cmPosSession, audit.getActionAuditId(), ReflectUtils.getTableName(SubReqAdslLl.class), subReqAdsl.getId(), ReflectUtils.getColumnName(SubReqAdslLl.class, "status"), oldValue, newValue, nowDate);
                                }
                            }
                            //</editor-fold>
                            //<editor-fold defaultstate="collapsed" desc="huy cong viec thue bao">

                            if (Constants.SUB_STATUS_WAIT_ACTIVE.equals(subAdslLL.getStatus())) {
//                                List<SaleTrans> saleTrans = new SaleTransDAO().findById(cmPosSession, offer.getSubId());
//                                if (saleTrans != null && saleTrans.size() > 0L) {
//                                    for (SaleTrans saleTr : saleTrans) {
//                                        WebServiceAPDAO webIMDAO = new WebServiceAPDAO(imSession);
//                                        WebServiceAPDAO.APResult longResult = webIMDAO.destroyAPSaleTrans(saleTr.getSaleTransId(), subAdslLL.getTeamId());
//                                        mess = MultiLaguageDAO.convertErrCodeToString(longResult, new Locale(locale));
//                                        if (mess != null && !"".equals(mess)) {
//                                            return mess;
//                                        }
//                                    }
//                                }
//                                mess = destroyTaskManageAndTaskShop(cmPosSession, audit.getActionAuditId(), offer.getSubId(), reasonId, token, nowDate, locale);
//                                if (mess != null && !"".equals(mess)) {
//                                    return mess;
//                                }
//
//                                //<editor-fold defaultstate="collapsed" desc="cap nhap bang sub_ip adsl ll">
//                                List<SubIpAdslLl> subIp = new SubIpAdslLlDAO().findById(cmPosSession, offer.getSubId());
//                                if (subIp != null && subIp.size() > 0L) {
//                                    for (SubIpAdslLl subIpAdslLl : subIp) {
//                                        Object oldValue = subIpAdslLl.getStatus();
//                                        subIpAdslLl.setStatus(Constants.STATUS_NOT_USE);
//                                        Object newValue = subIpAdslLl.getStatus();
//                                        subIpAdslLl.setUntil(nowDate);
//                                        cmPosSession.update(subIpAdslLl);
//                                        cmPosSession.flush();
//
//                                        new ActionDetailBussiness().insert(cmPosSession, audit.getActionAuditId(), ReflectUtils.getTableName(SubIpAdslLl.class), subIpAdslLl.getSubId(), ReflectUtils.getColumnName(SubIpAdslLl.class, "status"), oldValue, newValue, nowDate);
//                                    }
//                                }
//
//
//                                //</editor-fold>
//                                //<editor-fold defaultstate="collapsed" desc="cap nhap bang sub account adsl ll">
//                                List<SubAccountsAdslLl> subAccount = new SubAccountsAdslLlDAO().findBySubId(cmPosSession, offer.getSubId());
//                                if (subAccount != null && subAccount.size() > 0L) {
//                                    for (SubAccountsAdslLl subAccountsAdslLl : subAccount) {
//                                        Object oldValue = subAccountsAdslLl.getStatus();
//                                        subAccountsAdslLl.setStatus(Constants.STATUS_NOT_USE);
//                                        Object newValue = subAccountsAdslLl.getStatus();
//                                        subAccountsAdslLl.setUntil(nowDate);
//                                        cmPosSession.update(subAccountsAdslLl);
//                                        cmPosSession.flush();
//                                        new ActionDetailBussiness().insert(cmPosSession, audit.getActionAuditId(), ReflectUtils.getTableName(SubAccountsAdslLl.class), subAccountsAdslLl.getSubId(), ReflectUtils.getColumnName(SubAccountsAdslLl.class, "status"), oldValue, newValue, nowDate);
//                                    }
//                                }
//
//
//                                //</editor-fold>
//                                //<editor-fold defaultstate="collapsed" desc="cap nhap bang sub email adsl ll">
//                                List<SubEmailAdslLl> subEmails = new SubEmailAdslLlDAO().findBySubId(cmPosSession, offer.getSubId());
//                                if (subEmails != null && subEmails.size() > 0L) {
//                                    for (SubEmailAdslLl subEmailAdslLl : subEmails) {
//                                        Object oldValue = subEmailAdslLl.getUntil();
//                                        subEmailAdslLl.setUntil(nowDate);
//                                        Object newValue = subEmailAdslLl.getUntil();
//                                        cmPosSession.update(subEmailAdslLl);
//                                        cmPosSession.flush();
//                                        new ActionDetailBussiness().insert(cmPosSession, audit.getActionAuditId(), ReflectUtils.getTableName(SubEmailAdslLl.class), subEmailAdslLl.getSubId(), ReflectUtils.getColumnName(SubEmailAdslLl.class, "util"), oldValue, newValue, nowDate);
//                                    }
//                                }
//
//
//                                //</editor-fold>
//                                //<editor-fold defaultstate="collapsed" desc="cap nhap bang sub action adsl ll">
//                                List<SubActivateAdslLl> subActivates = new SubActivateAdslLlDAO().findBySubId(cmPosSession, offer.getSubId());
//                                if (subActivates != null && subActivates.size() > 0L) {
//                                    for (SubActivateAdslLl subActivateAdslLl : subActivates) {
//                                        Object oldValue = subActivateAdslLl.getUntil();
//                                        subActivateAdslLl.setUntil(nowDate);
//                                        Object newValue = subActivateAdslLl.getUntil();
//                                        cmPosSession.update(subActivateAdslLl);
//                                        cmPosSession.flush();
//                                        new ActionDetailBussiness().insert(cmPosSession, audit.getActionAuditId(), ReflectUtils.getTableName(SubActivateAdslLl.class), subActivateAdslLl.getSubId(), ReflectUtils.getColumnName(SubActivateAdslLl.class, "util"), oldValue, newValue, nowDate);
//                                    }
//                                }
//
//
//                                //</editor-fold>
//                                //<editor-fold defaultstate="collapsed" desc="cap nhap bang sub rel product adsl ll">
//                                List<SubRelProductAdslLl> subRelPro = new SubRelProductAdslLlDAO().findBySubId(cmPosSession, offer.getSubId());
//                                if (subRelPro != null && subRelPro.size() > 0L) {
//                                    for (SubRelProductAdslLl subRelProductAdslLl : subRelPro) {
//                                        subRelProductAdslLl.setEndDatetime(nowDate);
//                                        Object oldValue = subRelProductAdslLl.getStatus();
//                                        subRelProductAdslLl.setStatus(Constants.STATUS_NOT_USE);
//                                        Object newValue = subRelProductAdslLl.getStatus();
//                                        cmPosSession.update(subRelProductAdslLl);
//                                        cmPosSession.flush();
//                                        new ActionDetailBussiness().insert(cmPosSession, audit.getActionAuditId(), ReflectUtils.getTableName(SubRelProductAdslLl.class), subRelProductAdslLl.getId(), ReflectUtils.getColumnName(SubRelProductAdslLl.class, "status"), oldValue, newValue, nowDate);
//                                    }
//                                }
//
//
//                                //</editor-fold>
//                                //<editor-fold defaultstate="collapsed" desc="cap nhap bang sub promotion adsl ll">
//                                List<SubPromotionAdslLl> subPromotions = new SubPromotionAdslLlDAO().findBySubId(cmPosSession, offer.getSubId());
//                                if (subPromotions != null && subPromotions.size() > 0L) {
//                                    for (SubPromotionAdslLl subPromotionAdslLl : subPromotions) {
//                                        Object oldValue = subPromotionAdslLl.getUntil();
//                                        subPromotionAdslLl.setUntil(nowDate);
//                                        Object newValue = subPromotionAdslLl.getUntil();
//                                        cmPosSession.update(subPromotionAdslLl);
//                                        cmPosSession.flush();
//                                        new ActionDetailBussiness().insert(cmPosSession, audit.getActionAuditId(), ReflectUtils.getTableName(SubPromotionAdslLl.class), subPromotionAdslLl.getSubId(), ReflectUtils.getColumnName(SubPromotionAdslLl.class, "util"), oldValue, newValue, nowDate);
//                                    }
//                                }
//
//
//                                //</editor-fold>
//                                //<editor-fold defaultstate="collapsed" desc="cap nhap bang sub deployment">
//                                List<SubDeployment> deployments = new SubDeploymentDAO().findBySubId(cmPosSession, offer.getSubId());
//                                if (deployments != null && deployments.size() > 0L) {
//                                    for (SubDeployment subDeployment : deployments) {
//                                        Object oldValue = subDeployment.getStatus();
//                                        subDeployment.setStatus(Constants.STATUS_NOT_USE);
//                                        Object newValue = subDeployment.getStatus();
//                                        cmPosSession.update(subDeployment);
//                                        cmPosSession.flush();
//                                        new ActionDetailBussiness().insert(cmPosSession, audit.getActionAuditId(), ReflectUtils.getTableName(SubDeployment.class), subDeployment.getReqId(), ReflectUtils.getColumnName(SubDeployment.class, "status"), oldValue, newValue, nowDate);
//                                    }
//                                }
//                                //</editor-fold>
//                                //<editor-fold defaultstate="collapsed" desc="call Provisioning">
//                                ViettelService activeResponse = new Provisioning().activeSubscriber(cmPosSession, imSession, pmSession, customer.getBusType(), contract, subAdslLL, Constants.MODIFICATION_TYPE_REGISTER_NEW, "", shop.getShopCode(), staff.getStaffCode(), false, nowDate, locale);
//                                if (activeResponse == null) {
//                                    return LabelUtil.getKey("activeSubscriber.fail", locale);
//                                }
//                                Object responseCode = activeResponse.get(Constants.RESPONSE_CODE);
//                                if (Constants.RESPONSE_INVALID.equals(responseCode)) {
//                                    return (String) activeResponse.get(Constants.RESPONSE);
//                                }
//
//                                if (Constants.RESPONSE_EXCEPTION.equals(responseCode)) {
//                                    return (String) activeResponse.get(Constants.EXCEPTION);
//                                }
//                                //</editor-fold>
                            }

                            //</editor-fold>

                            boolean checkUser = isActivateOfOther(subAdslLL, token, staff.getStaffCode(), shop.getShopCode());
                            if (!checkUser) {
                                Object oldValue = subAdslLL.getStatus();
                                subAdslLL.setStatus(Constants.CONTRACT_STATUS_CANCEL);
                                Object newValue = subAdslLL.getStatus();
                                subAdslLL.setEndDatetime(nowDate);
                                subAdslLL.setFinishReasonId(reasonId);
                                cmPosSession.update(subAdslLL);
                                cmPosSession.flush();
                                new ActionDetailBussiness().insert(cmPosSession, audit.getActionAuditId(), ReflectUtils.getTableName(SubAdslLeaseline.class), subAdslLL.getSubId(), ReflectUtils.getColumnName(SubAdslLeaseline.class, "status"), oldValue, newValue, nowDate);
                            } else {
                                mess = LabelUtil.getKey("e0454.destroy.contract.fail", locale);
                                return mess;
                            }
                        }


                        //<editor-fold defaultstate="collapsed" desc="cap nhat yeu cau hotline">
                        new DeployRequirementDAO().updateStatusBySubId(cmPosSession, subAdslLL.getSubId(), Constants.DEPLOY_REQ_STATUS_UN_SATISFY, audit.getActionAuditId(), nowDate);
                        LogUtils.info(logger, "ContractBussiness.cancelContract:end update DeployRequirement");
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="goi NIMS giai phong tai nguyen">
                        LogUtils.info(logger, "ContractBussiness.cancelContract:start unlock infras on NIMS system;account=" + subReq.getAccount() + ";isdn=" + subReq.getIsdn()
                                + ";cableBoxId="
                                + subReq.getCableBoxId() + ";serviceType=" + subReq.getServiceType());
                        LockAndUnlockInfrasResponse infrasResponse = NimsWsLockUnlockInfrasBusiness.lockUnlockInfras(NimsWsLockUnlockInfrasBusiness.INFRAS_ACTION_UNLOCK,
                                subReq.getAccount(),
                                null,
                                subReq.getCableBoxId(),
                                null,
                                subReq.getServiceType(),
                                null,
                                null, null, null);
                        LogUtils.info(logger, "ContractBussiness.cancelContract:end unlock infras on NIMS system;infrasResponse=" + LogUtils.toJson(infrasResponse));
                        ResultForm resultForm = infrasResponse == null ? null : infrasResponse.getReturn();
                        if (resultForm == null) {
                            mess = LabelUtil.formatKey("lock.unlock.fail.1", locale);
                            return mess;
                        }
                        if (!NimsWsLockUnlockInfrasBusiness.INFRAS_LOCL_UNLOCK_OK.equals(resultForm.getResult())) {
                            mess = LabelUtil.formatKey("lock.unlock.fail", locale, resultForm.getMessage());
                            return mess;
                        }
                        //</editor-fold>

                        offer.setStatus(Constants.STATUS_NOT_USE);
                        offer.setEndDatetime(nowDate);
                        cmPosSession.update(offer);
                        cmPosSession.flush();
                    }
                }
                //</editor-fold>
                contractOf.setStatus(Constants.STATUS_NOT_USE);
                cmPosSession.update(contractOf);
                cmPosSession.flush();
            }

        }

        //</editor-fold>


        //</editor-fold>
        mess = null;
        return mess;
    }

    public String generateContractNo(Session cmPosSession, Long contractId, String shopCode, Date nowDate) {
        String contractNo;
        if (contractId != null && contractId > 0L) {
            contractNo = contractId.toString();
        } else {
            String sequenceName = Constants.CONTRACT_NO_SEQ + DateTimeUtils.formatDate(nowDate, "MMdd");
            Long contractNoId = getSequence(cmPosSession, sequenceName);
            contractNo = contractNoId.toString();
        }

        int len = Constants.CONTRACT_CONTRACT_NO_LENGTH;
        String contractLeng = new ApParamBussiness().getUserManualConfig(cmPosSession, Constants.LENG_CONTRACT_NO_INCREASE_LIST_NUMBER);
        if (contractLeng != null) {
            contractLeng = contractLeng.trim();
            if (!contractLeng.isEmpty()) {
                len = Integer.parseInt(contractLeng);
            }
        }
        int leftSpace = len - contractNo.length();
        if (leftSpace > 0) {
            for (int i = 0; i < leftSpace; i++) {
                contractNo = "0" + contractNo;
            }
        }
        return shopCode + "/" + DateTimeUtils.formatddMMyyyy(nowDate) + "/" + contractNo;
    }

    private void updateStatus(Session cmPosSession, Contract contract, Long status, String staffCode, Date lastUpdateTime, Date endDateTime, Long actionAuditId, Date nowDate) {
        Object oldValue = contract.getStatus();
        contract.setStatus(status);
        contract.setLastUpdateUser(staffCode);
        contract.setLastUpdateTime(lastUpdateTime);
        contract.setEndDatetime(endDateTime);
        Object newValue = contract.getStatus();

        new ActionDetailBussiness().insert(cmPosSession, actionAuditId, ReflectUtils.getTableName(Contract.class), contract.getCustId(), ReflectUtils.getColumnName(Contract.class, "status"), oldValue, newValue, nowDate);

        cmPosSession.update(contract);
        cmPosSession.flush();
    }

    private boolean isActivateOfOther(SubAdslLeaseline subAdslLL, Token token, String staffCode, String shopCode) {
        boolean isError = false;
        
        // Cuongnt4 fix bug NV dia ban dau thay A,P
        if (staffCode.trim().equals(subAdslLL.getUserCreated())) {
            return false;
        }
        /* cuongnt - check cho A, P, O, W check theo userCreated*/
        if (staffCode != null && shopCode != null) {
            staffCode = staffCode.trim();
            shopCode = shopCode.trim();
            if (!staffCode.isEmpty() && !shopCode.isEmpty()) {
                if (!staffCode.equals(subAdslLL.getUserCreated())) {
                    return true;
                }
            }
        }
        //Truong hop dau noi thay cho cua hang
        if (staffCode != null && !"".equals(staffCode)) {
            if (subAdslLL.getStaffId() != null && subAdslLL.getStaffId().longValue() != token.getStaffId().longValue()) {
                isError = true;
            }
        }//Truong hop dau noi thay cho dai ly
        else if (shopCode != null && !"".equals(shopCode)) {
            if (subAdslLL.getShopCode() != null && !subAdslLL.getShopCode().equals(shopCode)) {
                isError = false;
            }
        }
        return isError;
    }

    private String destroyTaskManageAndTaskShop(Session cmPosSession, Long actionAuditId, Long subId, Long reasonId, Token token, Date nowDate, String locale) {
        String result = "";
        try {
            if (subId != null || subId > 0L) {
                TaskManagementDAO taskManagementDAO = new TaskManagementDAO();
                List<TaskManagement> lstTask = taskManagementDAO.findByProperty(cmPosSession, Constants.SUB_ID, subId);
                if (lstTask != null && lstTask.size() > 0) {
                    for (int i = 0; i < lstTask.size(); i++) {
                        TaskManagement bo = lstTask.get(i);
                        //Xoa cac ban ghi trong taskShop
                        TaskShopManagementDAO taskShopManagementDAO = new TaskShopManagementDAO();
                        //Huy cong viec giao cho nv
                        TaskStaffManagementDAO taskStaffManagementDAO = new TaskStaffManagementDAO();
                        //Huy Task_Stage_Item
                        TaskStageItemDAO taskStageItemDAO = new TaskStageItemDAO();
                        List<TaskShopManagement> lstTaskShop = taskShopManagementDAO.findByProperty(cmPosSession, Constants.TASK_MNGT_ID, bo.getTaskMngtId());
                        if (lstTaskShop != null && lstTaskShop.size() > 0) {
                            for (int j = 0; j < lstTaskShop.size(); j++) {
                                //Thuc hien set status trong bang taskShop
                                TaskShopManagement boTaskShop = lstTaskShop.get(j);
                                //Neu da giao viec cho nhan vien thi phai huy cong viec
                                //set status cua task_staff_management
                                List<TaskStaffManagement> lstTaskStaff =
                                        taskStaffManagementDAO.findByProperty(cmPosSession, Constants.TASK_SHOP_MNGT_ID, boTaskShop.getTaskShopMngtId());
                                if (lstTaskStaff != null && lstTaskStaff.size() > 0) {
                                    for (int k = 0; k < lstTaskStaff.size(); k++) {
                                        TaskStaffManagement boTaskStaff = lstTaskStaff.get(k);


                                        //Neu nhan vien cap nhat con viec trong bang Task_Stage_Item
                                        List<TaskStageItem> lstTaskStageItem =
                                                taskStageItemDAO.findByProperty(cmPosSession, Constants.TASK_STAFF_MNGT_ID, boTaskStaff.getTaskStaffMngtId());
                                        if (lstTaskStageItem != null && lstTaskStageItem.size() > 0) {
                                            for (int l = 0; l < lstTaskStageItem.size(); l++) {
                                                TaskStageItem boStageItem = lstTaskStageItem.get(l);
                                                boStageItem.setStatus(Constants.STATUS_NOT_USE);
                                                cmPosSession.update(boStageItem);
                                                cmPosSession.flush();
                                            }
                                        }

                                        boTaskStaff.setStatus(Constants.STATUS_NOT_USE);
                                        cmPosSession.update(boTaskStaff);
                                        cmPosSession.flush();
                                    }

//                                    Object oldValue = boTaskShop.getStatus();
//                                    Object newValue = Constants.STATUS_NOT_USE;
//
//                                    ActionDetailBussiness detailDAO = new ActionDetailBussiness();
//                                    detailDAO.insert(cmPosSession, actionAuditId, ReflectUtils.getTableName(TaskShopManagement.class), boTaskShop.getTaskShopMngtId(), ReflectUtils.getColumnName(Contract.class, "status"), oldValue, newValue, nowDate);
                                }
                                boTaskShop.setStatus(Constants.STATUS_NOT_USE);
                                cmPosSession.update(boTaskShop);
                                cmPosSession.flush();
                            }
                        }
                        bo.setStatus(Constants.STATUS_NOT_USE);
                        cmPosSession.update(bo);
                        cmPosSession.flush();
                    }

                } else {
                    result = LabelUtil.getKey("a0641.sub.not.found", locale);
                }
            } else {
                result = LabelUtil.getKey("a0642.sub.not.exists", locale);
            }
        } catch (Exception e) {
            result = LabelUtil.getKey("a0640.destroy.task.fail", locale);
        }
        return result;
    }
    
     /**
     * @author duyetdk
     * @param cmPosSession
     * @param contractId
     * @return 
     */
    public Contract findByContractId(Session cmPosSession, Long contractId) {
        Contract result = null;
        if (contractId == null || contractId <= 0L) {
            return result;
        }
        List<Contract> objs = supplier.findById(cmPosSession, contractId, null);
        result = (Contract) getBO(objs);
        return result;
    }
}
