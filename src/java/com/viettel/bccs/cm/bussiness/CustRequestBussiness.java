package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.dao.SubReqAdslLlDAO;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.model.ActionAudit;
import com.viettel.bccs.cm.model.CustRequest;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.supplier.CustRequestSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.bccs.cm.util.LogUtils;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class CustRequestBussiness extends BaseBussiness {

    protected CustRequestSupplier supplier = new CustRequestSupplier();

    public CustRequestBussiness() {
        logger = Logger.getLogger(CustRequestBussiness.class);
    }

    public CustRequest findById(Session cmPosSession, Long custRequestId) {
        CustRequest result = null;
        if (custRequestId == null || custRequestId <= 0L) {
            return result;
        }
        List<CustRequest> objs = supplier.findById(cmPosSession, custRequestId);
        result = (CustRequest) getBO(objs);
        return result;
    }

    public void closeRequest(Session cmPosSession, Long custRequestId) {
        CustRequest custRequest = findById(cmPosSession, custRequestId);
        if (custRequest != null) {
            boolean isClose = new ViewAllCustRequestBussiness().isCloseRequest(cmPosSession, custRequestId);
            if (isClose) {
                custRequest.setStatus(Constants.STATUS_CUST_REQUEST_CLOSE);
                supplier.update(cmPosSession, custRequest);
            }
        }
    }

    public void cancelRequest(Session cmPosSession, Long custRequestId, Long subReqId, Reason reason, Staff staff, Shop shop, Date issueDateTime) {
        CustRequest custRequest = findById(cmPosSession, custRequestId);
        if (custRequest != null) {
            boolean isCancel = new SubReqAdslLlDAO().isAllCancel(cmPosSession, custRequestId, subReqId);
            if (isCancel) {
                //<editor-fold defaultstate="collapsed" desc="luu log action">
                StringBuilder des = new StringBuilder().append("Cancel customer request, Request id=").append(custRequest.getCustRequestId());
                LogUtils.info(logger, "CustRequestBussiness.cancelRequest:start save ActionAudit");
                ActionAudit audit = new ActionAuditBussiness().insert(cmPosSession, issueDateTime, Constants.ACTION_CANCEL_REQUEST, reason.getReasonId(), shop.getShopCode(), staff.getStaffCode(), Constants.ACTION_AUDIT_PK_TYPE_CUSTOMER, custRequest.getCustRequestId(), Constants.WS_IP, des.toString());
                LogUtils.info(logger, "CustRequestBussiness.cancelRequest:end save ActionAudit");
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="update status">
                Object oldValue = custRequest.getStatus();
                custRequest.setStatus(Constants.STATUS_CUST_REQUEST_CANCEL);
                Object newValue = custRequest.getStatus();

                ActionDetailBussiness detailBussiness = new ActionDetailBussiness();
                detailBussiness.insert(cmPosSession, audit.getActionAuditId(), ReflectUtils.getTableName(CustRequest.class), custRequest.getCustRequestId(), ReflectUtils.getColumnName(CustRequest.class, "status"), oldValue, newValue, issueDateTime);
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="update reason">
                oldValue = custRequest.getReason();
                custRequest.setReason(ConvertUtils.toStringValue(reason.getReasonId()));
                newValue = custRequest.getReason();
                detailBussiness.insert(cmPosSession, audit.getActionAuditId(), ReflectUtils.getTableName(CustRequest.class), custRequest.getCustRequestId(), ReflectUtils.getColumnName(CustRequest.class, "reason"), oldValue, newValue, issueDateTime);
                //</editor-fold>

                cmPosSession.update(custRequest);
                cmPosSession.flush();
            }
        }
    }
}
