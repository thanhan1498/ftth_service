package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.api.Task.DAO.CustomerDAO;
import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.cm.dao.CustImageDetailDAO;
import com.viettel.bccs.cm.dao.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.model.Area;
import com.viettel.bccs.cm.model.BusType;
import com.viettel.bccs.cm.model.Contract;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.CustomerExtend;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.pre.CustomerPre;
import com.viettel.bccs.cm.supplier.ContractSupplier;
import com.viettel.bccs.cm.supplier.CustomerSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.bccs.cm.util.DateTimeUtils;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.util.ValidateUtils;
import com.viettel.brcd.common.util.FileSaveProcess;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.ImageInput;
import com.viettel.brcd.ws.model.output.CcActionInformation;
import com.viettel.brcd.ws.model.output.VBtsAccount;
import com.viettel.brcd.ws.vsa.ResourceBundleUtil;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class CustomerBussiness extends BaseBussiness {

    protected CustomerSupplier supplier = new CustomerSupplier();

    public CustomerBussiness() {
        logger = Logger.getLogger(CustomerBussiness.class);
    }

    public Customer findById(Session cmPosSession, Long custId) {
        Customer result = null;
        if (custId == null || custId <= 0L) {
            return result;
        }
        List<Customer> objs = supplier.findById(cmPosSession, custId, Constants.STATUS_USE);
        result = (Customer) getBO(objs);
        return result;
    }

    public CustomerPre findByIdPre(Session cmPreSession, Long custId) {
        CustomerPre result = null;
        if (custId == null || custId <= 0L) {
            return result;
        }
        List<CustomerPre> objs = supplier.findByIdPre(cmPreSession, custId, Constants.STATUS_USE);
        result = (CustomerPre) getBO(objs);
        return result;
    }

    public Long count(Session cmPosSession, String idNo, String isdn, String account) {
        Long result = supplier.count(cmPosSession, idNo, isdn, account);
        return result;
    }

    public List<CustomerExtend> find(Session cmPosSession, String idNo, String isdn, String account, Integer start, Integer max, String customerName, String contact) {
        List<CustomerExtend> result = supplier.find(cmPosSession, idNo, isdn, account, start, max, customerName, contact);
        return result;
    }

    public Customer findByIdNo(Session cmPosSession, String idNo) {
        Customer result = null;
        if (idNo == null) {
            return result;
        }
        idNo = idNo.trim();
        if (idNo.isEmpty()) {
            return result;
        }
        List<Customer> objs = supplier.find(cmPosSession, idNo, Constants.STATUS_USE, Constants.CORRECT_CUSTOMER);
        result = (Customer) getBO(objs);
        return result;
    }

    public boolean isUsedIdNo(Session cmPosSession, String idNo) {
        Long count = supplier.count(cmPosSession, idNo, Constants.STATUS_USE, Constants.CORRECT_CUSTOMER);
        return count != null && count > 0L;
    }

    public boolean isUsedIdNoPre(Session cmPreSession, String idNo) {
        Long count = supplier.countPre(cmPreSession, idNo, Constants.STATUS_USE, Constants.CORRECT_CUSTOMER);
        return count != null && count > 0L;
    }

    public String insert(Session cmPosSession, Long idType, String idNo, String busType, String name, String birthDate, String idIssueDate,
            String idIssuePlace, Long staffId, String province, String district, String precinct, String streetBlock, String streetBlockName, String sex,
            String xLocation, String yLocation, String telMobile, String locale, List<ImageInput> lstImage, String staffCode) throws ParseException {
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">

        //<editor-fold defaultstate="collapsed" desc="idNo">
        if (idNo == null) {
            mess = LabelUtil.getKey("validate.required", locale, "idNo");
            return mess;
        }
        idNo = idNo.trim();
        if (idNo.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "idNo");
            return mess;
        }
        Integer length = ReflectUtils.getColumnLength(Customer.class, "idNo");
        if (length != null && length > 0 && idNo.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("idNo", locale), length);
            return mess;
        }
        if (isUsedIdNo(cmPosSession, idNo)) {
            mess = LabelUtil.getKey("a.error.has.idNo", locale);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="busType">
        if (busType == null) {
            mess = LabelUtil.getKey("validate.required", locale, "busType");
            return mess;
        }
        busType = busType.trim();
        if (busType.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "busType");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "busType");
        if (length != null && length > 0 && busType.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("busType", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="name">
        if (name == null) {
            mess = LabelUtil.getKey("validate.required", locale, "custName");
            return mess;
        }
        name = name.trim();
        if (busType.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "custName");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "name");
        if (length != null && length > 0 && name.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("custName", locale), length);
            return mess;
        }
        //</editor-fold>
        Date nowDate = getSysDateTime(cmPosSession);
        //<editor-fold defaultstate="collapsed" desc="birthDate">
        if (birthDate == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        birthDate = birthDate.trim();
        if (birthDate.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (!ValidateUtils.isDateddMMyyyy(birthDate)) {
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        Date bDate;
        try {
            bDate = DateTimeUtils.toDateddMMyyyy(birthDate);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        if (bDate == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (bDate.after(nowDate)) {
            mess = LabelUtil.getKey("greater.than", locale, "birthDate", "nowDate");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="idIssueDate">
        Date issDate = null;
        if (idIssueDate != null) {
            idIssueDate = idIssueDate.trim();
            if (!idIssueDate.isEmpty()) {
                if (!ValidateUtils.isDateddMMyyyy(idIssueDate)) {
                    mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("idIssueDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                    return mess;
                }
                try {
                    issDate = DateTimeUtils.toDateddMMyyyy(idIssueDate);
                } catch (Exception ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                    mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("idIssueDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                    return mess;
                }
                if (issDate == null) {
                    mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("idIssueDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="idIssuePlace">
        if (idIssuePlace != null) {
            idIssuePlace = idIssuePlace.trim();
            if (!idIssuePlace.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "idIssuePlace");
                if (length != null && length > 0 && busType.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("idIssuePlace", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="province">
        if (province == null) {
            mess = LabelUtil.getKey("validate.required", locale, "province");
            return mess;
        }
        province = province.trim();
        if (province.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "province");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "province");
        if (length != null && length > 0 && province.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("province", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="district">
        if (district == null) {
            mess = LabelUtil.getKey("validate.required", locale, "district");
            return mess;
        }
        district = district.trim();
        if (district.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "district");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "district");
        if (length != null && length > 0 && district.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("district", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="precinct">
        if (precinct == null) {
            mess = LabelUtil.getKey("validate.required", locale, "precinct");
            return mess;
        }
        precinct = precinct.trim();
        if (precinct.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "precinct");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "precinct");
        if (length != null && length > 0 && precinct.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("precinct", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="streetBlock">
        if (streetBlock != null) {
            streetBlock = streetBlock.trim();
            if (!streetBlock.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "streetBlock");
                if (length != null && length > 0 && streetBlock.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("streetBlock", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="streetBlockName">
        if (streetBlockName != null) {
            streetBlockName = streetBlockName.trim();
            if (!streetBlockName.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "streetBlockName");
                if (length != null && length > 0 && streetBlockName.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("streetBlock", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="sex">
        if (sex != null) {
            sex = sex.trim();
            if (!sex.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "sex");
                if (length != null && length > 0 && sex.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("sex", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="xLocation">
        if (xLocation != null) {
            xLocation = xLocation.trim();
            if (!xLocation.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "xLocation");
                if (length != null && length > 0 && xLocation.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("xLocation", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="yLocation">
        if (yLocation != null) {
            yLocation = yLocation.trim();
            if (!yLocation.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "yLocation");
                if (length != null && length > 0 && yLocation.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("yLocation", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="telMobile">
        if (telMobile == null) {
            mess = LabelUtil.getKey("validate.required.telMobile", locale);
            return mess;
        }
        telMobile = telMobile.trim();
        if (telMobile.isEmpty()) {
            mess = LabelUtil.getKey("validate.required.telMobile", locale);
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "telFax");
        if (length != null && length > 0 && telMobile.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("telMobile", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="busType">
        BusType bus = new BusTypeBussiness().findById(cmPosSession, busType);
        if (bus == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "busType");
            return mess;
        }
        //</editor-fold>
        String custType = bus.getCustType();
        if (!Constants.BUS_TYPE_BUSINESS.equals(custType)) {// Neu la KHDN
            //<editor-fold defaultstate="collapsed" desc="idType">
            if (idType == null) {
                mess = LabelUtil.getKey("validate.required", locale, "idType");
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="idIssueDate">
            if (issDate == null) {
                mess = LabelUtil.getKey("validate.required", locale, "idIssueDate");
                return mess;
            }
            if (issDate.after(nowDate)) {
                mess = LabelUtil.getKey("greater.than", locale, "idIssueDate", "nowDate");
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="idIssuePlace">
            if (idIssuePlace == null || idIssuePlace.isEmpty()) {
                mess = LabelUtil.getKey("validate.required", locale, "idIssuePlace");
                return mess;
            }
            //</editor-fold>
        }
        //<editor-fold defaultstate="collapsed" desc="staffId">
        Staff staff = new StaffBussiness().findById(cmPosSession, staffId);
        if (staff == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "staff");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="area">
        String areaCode = province + district + precinct;
        Area area = new AreaBussiness().findById(cmPosSession, areaCode);
        if (area == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "area");
            return mess;
        }
        //</editor-fold>
        //</editor-fold>
        Customer cus = supplier.insert(cmPosSession, busType, name, idType, custType, idNo, issDate, idIssuePlace, province, district, precinct, areaCode, area.getFullName(), streetBlock, streetBlockName, staff.getStaffCode(), nowDate, bDate, sex, xLocation, yLocation, telMobile);
        LogUtils.info(logger, "CustomerBussiness.insert:cus=" + LogUtils.toJson(cus));
        mess = ConvertUtils.toStringValue(cus.getCustId());
        //<editor-fold defaultstate="collapsed" desc="upload image if have">
        if (lstImage != null && lstImage.size() > 0) {
            try {
                Long idImage = Common.getSequence("CUST_IMAGE_SEQ", cmPosSession);
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
                int count = 0;
                for (ImageInput imageInput : lstImage) {
                    String imageDate = dateFormat.format(new Date());
                    String imageName = staffCode + "_" + imageDate + count + "_" + idImage + ".jpg";
                    boolean checkUpload = new FileSaveProcess().saveFile(imageName, imageInput.getImage(), String.valueOf(cus.getCustId()), logger);
                    if (!checkUpload) {
                        logger.info("\n--------------\nUpload image Faile\n--------------\n");
                        continue;
                    }

                    int checkInsertDB = new CustomerDAO().insertCusImageDetail(cmPosSession, idImage, imageName, imageInput.getImageName(), String.valueOf(idType), staffCode);
                    if (checkInsertDB == 0) {
                        logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                    }
                    count++;
                }
                int checkInsertDB = new CustomerDAO().insertCusImage(cmPosSession, cus.getCustId(), "", "", "CUST_IMG", staffCode, idImage);
                if (checkInsertDB == 0) {
                    logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                }
            } catch (Exception e) {
                logger.info("\n--------------\nUpload image Faile\n--------------\n" + e);
            }
        }
        //</editor-fold>
        return mess;
    }

    public String update(Session cmPosSession, Long custId, Long idType, String idNo, String busType, String name, String birthDate, String idIssueDate,
            String idIssuePlace, Long staffId, String province, String district, String precinct, String streetBlock, String streetBlockName, String sex,
            String xLocation, String yLocation, String telMobile, String locale, List<ImageInput> lstImage, String staffCode) throws ParseException {
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="custId">
        if (custId == null || custId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "custId");
            return mess;
        }
        //</editor-fold>
        Customer cusCheck = findById(cmPosSession, custId);
        if (cusCheck == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "customer");
            return mess;
        }

        //<editor-fold defaultstate="collapsed" desc="idNo">
        if (idNo == null) {
            mess = LabelUtil.getKey("validate.required", locale, "idNo");
            return mess;
        }
        idNo = idNo.trim();
        if (idNo.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "idNo");
            return mess;
        }
        Integer length = ReflectUtils.getColumnLength(Customer.class, "idNo");
        if (length != null && length > 0 && idNo.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("idNo", locale), length);
            return mess;
        }
        if (isUsedIdNo(cmPosSession, idNo) && !cusCheck.getIdNo().equals(idNo)) {
            mess = LabelUtil.getKey("a.error.has.idNo", locale);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="busType">
        if (busType == null) {
            mess = LabelUtil.getKey("validate.required", locale, "busType");
            return mess;
        }
        busType = busType.trim();
        if (busType.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "busType");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "busType");
        if (length != null && length > 0 && busType.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("busType", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="name">
        if (name == null) {
            mess = LabelUtil.getKey("validate.required", locale, "custName");
            return mess;
        }
        name = name.trim();
        if (busType.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "custName");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "name");
        if (length != null && length > 0 && name.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("custName", locale), length);
            return mess;
        }
        //</editor-fold>
        Date nowDate = getSysDateTime(cmPosSession);
        //<editor-fold defaultstate="collapsed" desc="birthDate">
        if (birthDate == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        birthDate = birthDate.trim();
        if (birthDate.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (!ValidateUtils.isDateddMMyyyy(birthDate)) {
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        Date bDate;
        try {
            bDate = DateTimeUtils.toDateddMMyyyy(birthDate);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        if (bDate == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (bDate.after(nowDate)) {
            mess = LabelUtil.getKey("greater.than", locale, "birthDate", "nowDate");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="idIssueDate">
        Date issDate = null;
        if (idIssueDate != null) {
            idIssueDate = idIssueDate.trim();
            if (!idIssueDate.isEmpty()) {
                if (!ValidateUtils.isDateddMMyyyy(idIssueDate)) {
                    mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("idIssueDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                    return mess;
                }
                try {
                    issDate = DateTimeUtils.toDateddMMyyyy(idIssueDate);
                } catch (Exception ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                    mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("idIssueDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                    return mess;
                }
                if (issDate == null) {
                    mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("idIssueDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="idIssuePlace">
        if (idIssuePlace != null) {
            idIssuePlace = idIssuePlace.trim();
            if (!idIssuePlace.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "idIssuePlace");
                if (length != null && length > 0 && busType.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("idIssuePlace", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="province">
        if (province == null) {
            mess = LabelUtil.getKey("validate.required", locale, "province");
            return mess;
        }
        province = province.trim();
        if (province.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "province");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "province");
        if (length != null && length > 0 && province.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("province", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="district">
        if (district == null) {
            mess = LabelUtil.getKey("validate.required", locale, "district");
            return mess;
        }
        district = district.trim();
        if (district.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "district");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "district");
        if (length != null && length > 0 && district.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("district", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="precinct">
        if (precinct == null) {
            mess = LabelUtil.getKey("validate.required", locale, "precinct");
            return mess;
        }
        precinct = precinct.trim();
        if (precinct.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "precinct");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "precinct");
        if (length != null && length > 0 && precinct.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("precinct", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="streetBlock">
        if (streetBlock != null) {
            streetBlock = streetBlock.trim();
            if (!streetBlock.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "streetBlock");
                if (length != null && length > 0 && streetBlock.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("streetBlock", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="streetBlockName">
        if (streetBlockName != null) {
            streetBlockName = streetBlockName.trim();
            if (!streetBlockName.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "streetBlockName");
                if (length != null && length > 0 && streetBlockName.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("streetBlock", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="sex">
        if (sex != null) {
            sex = sex.trim();
            if (!sex.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "sex");
                if (length != null && length > 0 && sex.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("sex", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="telMobile">
        if (telMobile == null) {
            mess = LabelUtil.getKey("validate.required.telMobile", locale);
            return mess;
        }
        telMobile = telMobile.trim();
        if (telMobile.isEmpty()) {
            mess = LabelUtil.getKey("validate.required.telMobile", locale);
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "telFax");
        if (length != null && length > 0 && telMobile.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("telMobile", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="xLocation">
        if (xLocation != null) {
            xLocation = xLocation.trim();
            if (!xLocation.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "xLocation");
                if (length != null && length > 0 && xLocation.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("xLocation", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="yLocation">
        if (yLocation != null) {
            yLocation = yLocation.trim();
            if (!yLocation.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "yLocation");
                if (length != null && length > 0 && yLocation.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("yLocation", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="busType">
        BusType bus = new BusTypeBussiness().findById(cmPosSession, busType);
        if (bus == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "busType");
            return mess;
        }
        //</editor-fold>
        String custType = bus.getCustType();
        if (!Constants.BUS_TYPE_BUSINESS.equals(custType)) {// Neu la KHDN
            //<editor-fold defaultstate="collapsed" desc="idType">
            if (idType == null) {
                mess = LabelUtil.getKey("validate.required", locale, "idType");
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="idIssueDate">
            if (issDate == null) {
                mess = LabelUtil.getKey("validate.required", locale, "idIssueDate");
                return mess;
            }
            if (issDate.after(nowDate)) {
                mess = LabelUtil.getKey("greater.than", locale, "idIssueDate", "nowDate");
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="idIssuePlace">
            if (idIssuePlace == null || idIssuePlace.isEmpty()) {
                mess = LabelUtil.getKey("validate.required", locale, "idIssuePlace");
                return mess;
            }
            //</editor-fold>
        }
        //<editor-fold defaultstate="collapsed" desc="staffId">
        Staff staff = new StaffBussiness().findById(cmPosSession, staffId);
        if (staff == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "staff");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="area">
        String areaCode = province + district + precinct;
        Area area = new AreaBussiness().findById(cmPosSession, areaCode);
        if (area == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "area");
            return mess;
        }
        //</editor-fold>
        //</editor-fold>
        Customer cus = supplier.update(cmPosSession, cusCheck, custId, busType, name, idType, custType, idNo, issDate, idIssuePlace, province, district, precinct, areaCode, area.getFullName(), streetBlock, streetBlockName, staff.getStaffCode(), nowDate, bDate, sex, xLocation, yLocation, telMobile);
        LogUtils.info(logger, "CustomerBussiness.insert:cus=" + LogUtils.toJson(cus));
        mess = ConvertUtils.toStringValue(cus.getCustId());
        //<editor-fold defaultstate="collapsed" desc="upload image if have">
        if (lstImage != null && lstImage.size() > 0) {
            try {
                Long idImage = Common.getSequence("CUST_IMAGE_SEQ", cmPosSession);
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
                CustomerDAO cusDAO = new CustomerDAO();
                int count = 0;
                for (ImageInput imageInput : lstImage) {
                    String imageDate = dateFormat.format(new Date());
                    String imageName = staffCode + "_" + imageDate + count + "_" + idImage + ".jpg";
                    boolean checkUpload = new FileSaveProcess().saveFile(imageName, imageInput.getImage(), String.valueOf(cus.getCustId()), logger);
                    if (!checkUpload) {
                        logger.info("\n--------------\nUpload image Faile\n--------------\n");
                        continue;
                    }
                    int checkInsertDB = cusDAO.insertCusImageDetail(cmPosSession, idImage, imageName, imageInput.getImageName(), String.valueOf(idType), staffCode);
                    if (checkInsertDB == 0) {
                        logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                    }
                    count++;
                }
                cusDAO.updateCusImage(cmPosSession, custId);
                int checkInsertDB = cusDAO.insertCusImage(cmPosSession, cus.getCustId(), "", "", "CUST_IMG", staffCode, idImage);
                if (checkInsertDB == 0) {
                    logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                }
            } catch (Exception e) {
                logger.info("\n--------------\nUpload image Faile\n--------------\n" + e);
            }
        }
        //</editor-fold>
        return mess;
    }

    public String setCustomHouseCoord(Session cmPosSession, Long custId, String xLocation, String yLocation, String locale, String userUpdate,
            List<ImageInput> lstImageList, Long subId, String account) throws Exception {
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="custId">
        if (custId == null || custId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "custId");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="xLocation">
        if (xLocation == null) {
            mess = LabelUtil.getKey("validate.required", locale, "xLocation");
            return mess;
        }
        xLocation = xLocation.trim();
        if (xLocation.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "xLocation");
            return mess;
        }
        Integer length = ReflectUtils.getColumnLength(Customer.class, "xLocation");
        if (length != null && length > 0 && xLocation.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("xLocation", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="yLocation">
        if (yLocation == null) {
            mess = LabelUtil.getKey("validate.required", locale, "yLocation");
            return mess;
        }
        yLocation = yLocation.trim();
        if (yLocation.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "yLocation");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "yLocation");
        if (length != null && length > 0 && yLocation.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("yLocation", locale), length);
            return mess;
        }
        //</editor-fold>
        Customer cus = findById(cmPosSession, custId);
        if (cus == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "customer");
            return mess;
        }

        //duyetdk: bo sung upload anh khi cap nhat location nha KH
        Long custImageId = null;
        if (lstImageList != null && !lstImageList.isEmpty()) {
            custImageId = new CustImageDetailDAO().insertCustImageLocation(cmPosSession, lstImageList, account, Constants.IMAGE_TYPE_LOCATION_CUSTOMER,
                    subId, userUpdate, logger, ResourceBundleUtil.getResource("temp.folder.image.location"), xLocation, yLocation);
        }

        if (custImageId == null || custImageId == 0L) {
            return "upload image location fail";
        }
        supplier.update(cmPosSession, cus, xLocation, yLocation, userUpdate);
        //</editor-fold>
        mess = null;
        return mess;
    }

    public List<Customer> find(Session cmPosSession, String idNo, String isdn, String service) throws Exception {
        List<Customer> result = supplier.find(cmPosSession, idNo, isdn, service);
        return result;
    }

    public String insertPre(Session cmPreSession, Session cmPosSession, Long idType, String idNo, String busType, String name, String birthDate, String idIssueDate,
            String idIssuePlace, Long staffId, String province, String district, String precinct, String streetBlock, String streetBlockName, String sex,
            String xLocation, String yLocation, String telMobile, String locale, List<ImageInput> lstImage, String staffCode) throws ParseException {
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="idNo">
        if (idNo == null) {
            mess = LabelUtil.getKey("validate.required", locale, "idNo");
            return mess;
        }
        idNo = idNo.trim();
        if (idNo.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "idNo");
            return mess;
        }
        Integer length = ReflectUtils.getColumnLength(Customer.class, "idNo");
        if (length != null && length > 0 && idNo.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("idNo", locale), length);
            return mess;
        }
        if (isUsedIdNoPre(cmPreSession, idNo)) {
            mess = LabelUtil.getKey("a.error.has.idNo", locale);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="busType">
        if (busType == null) {
            mess = LabelUtil.getKey("validate.required", locale, "busType");
            return mess;
        }
        busType = busType.trim();
        if (busType.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "busType");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "busType");
        if (length != null && length > 0 && busType.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("busType", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="name">
        if (name == null) {
            mess = LabelUtil.getKey("validate.required", locale, "custName");
            return mess;
        }
        name = name.trim();
        if (busType.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "custName");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "name");
        if (length != null && length > 0 && name.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("custName", locale), length);
            return mess;
        }
        //</editor-fold>
        Date nowDate = getSysDateTime(cmPreSession);
        //<editor-fold defaultstate="collapsed" desc="birthDate">
        if (birthDate == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        birthDate = birthDate.trim();
        if (birthDate.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (!ValidateUtils.isDateddMMyyyy(birthDate)) {
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        Date bDate;
        try {
            bDate = DateTimeUtils.toDateddMMyyyy(birthDate);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        if (bDate == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (bDate.after(nowDate)) {
            mess = LabelUtil.getKey("greater.than", locale, "birthDate", "nowDate");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="idIssueDate">
        Date issDate = null;
        if (idIssueDate != null) {
            idIssueDate = idIssueDate.trim();
            if (!idIssueDate.isEmpty()) {
                if (!ValidateUtils.isDateddMMyyyy(idIssueDate)) {
                    mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("idIssueDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                    return mess;
                }
                try {
                    issDate = DateTimeUtils.toDateddMMyyyy(idIssueDate);
                } catch (Exception ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                    mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("idIssueDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                    return mess;
                }
                if (issDate == null) {
                    mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("idIssueDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="idIssuePlace">
        if (idIssuePlace != null) {
            idIssuePlace = idIssuePlace.trim();
            if (!idIssuePlace.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "idIssuePlace");
                if (length != null && length > 0 && busType.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("idIssuePlace", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="province">
        if (province == null) {
            mess = LabelUtil.getKey("validate.required", locale, "province");
            return mess;
        }
        province = province.trim();
        if (province.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "province");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "province");
        if (length != null && length > 0 && province.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("province", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="district">
        if (district == null) {
            mess = LabelUtil.getKey("validate.required", locale, "district");
            return mess;
        }
        district = district.trim();
        if (district.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "district");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "district");
        if (length != null && length > 0 && district.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("district", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="precinct">
        if (precinct == null) {
            mess = LabelUtil.getKey("validate.required", locale, "precinct");
            return mess;
        }
        precinct = precinct.trim();
        if (precinct.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "precinct");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "precinct");
        if (length != null && length > 0 && precinct.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("precinct", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="streetBlock">
        if (streetBlock != null) {
            streetBlock = streetBlock.trim();
            if (!streetBlock.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "streetBlock");
                if (length != null && length > 0 && streetBlock.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("streetBlock", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="streetBlockName">
        if (streetBlockName != null) {
            streetBlockName = streetBlockName.trim();
            if (!streetBlockName.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "streetBlockName");
                if (length != null && length > 0 && streetBlockName.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("streetBlock", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="sex">
        if (sex != null) {
            sex = sex.trim();
            if (!sex.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "sex");
                if (length != null && length > 0 && sex.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("sex", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="xLocation">
        if (xLocation != null) {
            xLocation = xLocation.trim();
            if (!xLocation.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "xLocation");
                if (length != null && length > 0 && xLocation.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("xLocation", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="yLocation">
        if (yLocation != null) {
            yLocation = yLocation.trim();
            if (!yLocation.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "yLocation");
                if (length != null && length > 0 && yLocation.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("yLocation", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="busType">
        BusType bus = new BusTypeBussiness().findByIdPre(cmPreSession, busType);
        if (bus == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "busType");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="telMobile">
        if (telMobile == null) {
            mess = LabelUtil.getKey("validate.required.telMobile", locale);
            return mess;
        }
        telMobile = telMobile.trim();
        if (telMobile.isEmpty()) {
            mess = LabelUtil.getKey("validate.required.telMobile", locale);
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "telFax");
        if (length != null && length > 0 && province.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("telMobile", locale), length);
            return mess;
        }
        //</editor-fold>
        String custType = bus.getCustType();
        if (!Constants.BUS_TYPE_BUSINESS.equals(custType)) {// Neu la KHDN
            //<editor-fold defaultstate="collapsed" desc="idType">
            if (idType == null) {
                mess = LabelUtil.getKey("validate.required", locale, "idType");
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="idIssueDate">
            if (issDate == null) {
                mess = LabelUtil.getKey("validate.required", locale, "idIssueDate");
                return mess;
            }
            if (issDate.after(nowDate)) {
                mess = LabelUtil.getKey("greater.than", locale, "idIssueDate", "nowDate");
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="idIssuePlace">
            if (idIssuePlace == null || idIssuePlace.isEmpty()) {
                mess = LabelUtil.getKey("validate.required", locale, "idIssuePlace");
                return mess;
            }
            //</editor-fold>
        }
        //<editor-fold defaultstate="collapsed" desc="staffId">
        Staff staff = new StaffBussiness().findById(cmPosSession, staffId);
        if (staff == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "staff");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="area">
        String areaCode = province + district + precinct;
        Area area = new AreaBussiness().findById(cmPosSession, areaCode);
        if (area == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "area");
            return mess;
        }
        //</editor-fold>
        //</editor-fold>
        CustomerPre cus = supplier.insertPre(cmPreSession, busType, name, idType, custType, idNo, issDate, idIssuePlace, province, district, precinct, areaCode, area.getFullName(), streetBlock, streetBlockName, staff.getStaffCode(), nowDate, bDate, sex, telMobile);
        LogUtils.info(logger, "CustomerBussiness.insert:cus=" + LogUtils.toJson(cus));
        mess = ConvertUtils.toStringValue(cus.getCustId());
        //<editor-fold defaultstate="collapsed" desc="upload image if have">
        if (lstImage != null && lstImage.size() > 0) {
            try {
                Long idImage = Common.getSequence("CUST_IMAGE_SEQ", cmPosSession);
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
                int count = 0;
                for (ImageInput imageInput : lstImage) {
                    String imageDate = dateFormat.format(new Date());
                    String imageName = staffCode + "_" + imageDate + count + "_" + idImage + ".jpg";
                    boolean checkUpload = new FileSaveProcess().saveFile(imageName, imageInput.getImage(), String.valueOf(cus.getCustId()), logger);
                    if (!checkUpload) {
                        logger.info("\n--------------\nUpload image Faile\n--------------\n");
                        continue;
                    }

                    int checkInsertDB = new CustomerDAO().insertCusImageDetail(cmPosSession, idImage, imageName, imageInput.getImageName(), String.valueOf(idType), staffCode);
                    if (checkInsertDB == 0) {
                        logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                    }
                    count++;
                }
                int checkInsertDB = new CustomerDAO().insertCusImage(cmPosSession, cus.getCustId(), "", "", "CUST_IMG", staffCode, idImage);
                if (checkInsertDB == 0) {
                    logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                }
            } catch (Exception e) {
                logger.info("\n--------------\nUpload image Faile\n--------------\n" + e);
            }
        }
        //</editor-fold>
        return mess;
    }

    public String updatePre(Session cmPreSession, Long custId, Session cmPosSession, Long idType, String idNo, String busType, String name, String birthDate, String idIssueDate,
            String idIssuePlace, Long staffId, String province, String district, String precinct, String streetBlock, String streetBlockName, String sex,
            String xLocation, String yLocation, String telMobile, String locale, List<ImageInput> lstImage, String staffCode) throws ParseException {
        String mess;
        //<editor-fold defaultstate="collapsed" desc="custId">
        if (custId == null || custId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "custId");
            return mess;
        }
        //</editor-fold>
        CustomerPre cusCheck = findByIdPre(cmPreSession, custId);
        if (cusCheck == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "customer");
            return mess;
        }
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="idNo">
        if (idNo == null) {
            mess = LabelUtil.getKey("validate.required", locale, "idNo");
            return mess;
        }
        idNo = idNo.trim();
        if (idNo.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "idNo");
            return mess;
        }
        Integer length = ReflectUtils.getColumnLength(Customer.class, "idNo");
        if (length != null && length > 0 && idNo.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("idNo", locale), length);
            return mess;
        }
        if (isUsedIdNoPre(cmPreSession, idNo) && !cusCheck.getIdNo().equals(idNo)) {
            mess = LabelUtil.getKey("a.error.has.idNo", locale);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="busType">
        if (busType == null) {
            mess = LabelUtil.getKey("validate.required", locale, "busType");
            return mess;
        }
        busType = busType.trim();
        if (busType.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "busType");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "busType");
        if (length != null && length > 0 && busType.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("busType", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="name">
        if (name == null) {
            mess = LabelUtil.getKey("validate.required", locale, "custName");
            return mess;
        }
        name = name.trim();
        if (busType.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "custName");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "name");
        if (length != null && length > 0 && name.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("custName", locale), length);
            return mess;
        }
        //</editor-fold>
        Date nowDate = getSysDateTime(cmPreSession);
        //<editor-fold defaultstate="collapsed" desc="birthDate">
        if (birthDate == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        birthDate = birthDate.trim();
        if (birthDate.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (!ValidateUtils.isDateddMMyyyy(birthDate)) {
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        Date bDate;
        try {
            bDate = DateTimeUtils.toDateddMMyyyy(birthDate);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        if (bDate == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (bDate.after(nowDate)) {
            mess = LabelUtil.getKey("greater.than", locale, "birthDate", "nowDate");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="idIssueDate">
        Date issDate = null;
        if (idIssueDate != null) {
            idIssueDate = idIssueDate.trim();
            if (!idIssueDate.isEmpty()) {
                if (!ValidateUtils.isDateddMMyyyy(idIssueDate)) {
                    mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("idIssueDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                    return mess;
                }
                try {
                    issDate = DateTimeUtils.toDateddMMyyyy(idIssueDate);
                } catch (Exception ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                    mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("idIssueDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                    return mess;
                }
                if (issDate == null) {
                    mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("idIssueDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="idIssuePlace">
        if (idIssuePlace != null) {
            idIssuePlace = idIssuePlace.trim();
            if (!idIssuePlace.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "idIssuePlace");
                if (length != null && length > 0 && busType.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("idIssuePlace", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="province">
        if (province == null) {
            mess = LabelUtil.getKey("validate.required", locale, "province");
            return mess;
        }
        province = province.trim();
        if (province.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "province");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "province");
        if (length != null && length > 0 && province.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("province", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="district">
        if (district == null) {
            mess = LabelUtil.getKey("validate.required", locale, "district");
            return mess;
        }
        district = district.trim();
        if (district.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "district");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "district");
        if (length != null && length > 0 && district.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("district", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="precinct">
        if (precinct == null) {
            mess = LabelUtil.getKey("validate.required", locale, "precinct");
            return mess;
        }
        precinct = precinct.trim();
        if (precinct.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "precinct");
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "precinct");
        if (length != null && length > 0 && precinct.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("precinct", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="streetBlock">
        if (streetBlock != null) {
            streetBlock = streetBlock.trim();
            if (!streetBlock.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "streetBlock");
                if (length != null && length > 0 && streetBlock.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("streetBlock", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="streetBlockName">
        if (streetBlockName != null) {
            streetBlockName = streetBlockName.trim();
            if (!streetBlockName.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "streetBlockName");
                if (length != null && length > 0 && streetBlockName.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("streetBlock", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="sex">
        if (sex != null) {
            sex = sex.trim();
            if (!sex.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "sex");
                if (length != null && length > 0 && sex.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("sex", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="xLocation">
        if (xLocation != null) {
            xLocation = xLocation.trim();
            if (!xLocation.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "xLocation");
                if (length != null && length > 0 && xLocation.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("xLocation", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="yLocation">
        if (yLocation != null) {
            yLocation = yLocation.trim();
            if (!yLocation.isEmpty()) {
                length = ReflectUtils.getColumnLength(Customer.class, "yLocation");
                if (length != null && length > 0 && yLocation.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("yLocation", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="busType">
        BusType bus = new BusTypeBussiness().findByIdPre(cmPreSession, busType);
        if (bus == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "busType");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="telMobile">
        if (telMobile == null) {
            mess = LabelUtil.getKey("validate.required.telMobile", locale);
            return mess;
        }
        telMobile = telMobile.trim();
        if (telMobile.isEmpty()) {
            mess = LabelUtil.getKey("validate.required.telMobile", locale);
            return mess;
        }
        length = ReflectUtils.getColumnLength(Customer.class, "telFax");
        if (length != null && length > 0 && province.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("telMobile", locale), length);
            return mess;
        }
        //</editor-fold>
        String custType = bus.getCustType();
        if (!Constants.BUS_TYPE_BUSINESS.equals(custType)) {// Neu la KHDN
            //<editor-fold defaultstate="collapsed" desc="idType">
            if (idType == null) {
                mess = LabelUtil.getKey("validate.required", locale, "idType");
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="idIssueDate">
            if (issDate == null) {
                mess = LabelUtil.getKey("validate.required", locale, "idIssueDate");
                return mess;
            }
            if (issDate.after(nowDate)) {
                mess = LabelUtil.getKey("greater.than", locale, "idIssueDate", "nowDate");
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="idIssuePlace">
            if (idIssuePlace == null || idIssuePlace.isEmpty()) {
                mess = LabelUtil.getKey("validate.required", locale, "idIssuePlace");
                return mess;
            }
            //</editor-fold>
        }
        //<editor-fold defaultstate="collapsed" desc="staffId">
        Staff staff = new StaffBussiness().findById(cmPosSession, staffId);
        if (staff == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "staff");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="area">
        String areaCode = province + district + precinct;
        Area area = new AreaBussiness().findById(cmPosSession, areaCode);
        if (area == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "area");
            return mess;
        }
        //</editor-fold>
        //</editor-fold>
        CustomerPre cus = supplier.updatePre(cmPreSession, cusCheck, custId, busType, name, idType, custType, idNo, issDate, idIssuePlace, province, district, precinct, areaCode, area.getFullName(), streetBlock, streetBlockName, staff.getStaffCode(), nowDate, bDate, sex, telMobile);
        LogUtils.info(logger, "CustomerBussiness.insert:cus=" + LogUtils.toJson(cus));
        mess = ConvertUtils.toStringValue(cus.getCustId());
        //<editor-fold defaultstate="collapsed" desc="upload image if have">
        if (lstImage != null && lstImage.size() > 0) {
            try {
                Long idImage = Common.getSequence("CUST_IMAGE_SEQ", cmPosSession);
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
                CustomerDAO cusDAO = new CustomerDAO();
                int count = 0;
                for (ImageInput imageInput : lstImage) {
                    String imageDate = dateFormat.format(new Date());
                    String imageName = staffCode + "_" + imageDate + count + "_" + idImage + ".jpg";
                    boolean checkUpload = new FileSaveProcess().saveFile(imageName, imageInput.getImage(), String.valueOf(cus.getCustId()), logger);
                    if (!checkUpload) {
                        logger.info("\n--------------\nUpload image Faile\n--------------\n");
                        continue;
                    }
                    int checkInsertDB = cusDAO.insertCusImageDetail(cmPosSession, idImage, imageName, imageInput.getImageName(), String.valueOf(idType), staffCode);
                    if (checkInsertDB == 0) {
                        logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                    }
                    count++;
                }
                cusDAO.updateCusImage(cmPosSession, custId);
                int checkInsertDB = cusDAO.insertCusImage(cmPosSession, cus.getCustId(), "", "", "CUST_IMG", staffCode, idImage);
                if (checkInsertDB == 0) {
                    logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                }
            } catch (Exception e) {
                logger.info("\n--------------\nUpload image Faile\n--------------\n" + e);
            }
        }
        //</editor-fold>
        return mess;
    }

    //duyetdk
    public List<SubAdslLeaseline> findByCustId(Session cmPosSession, Long custId) {
        List<SubAdslLeaseline> result = null;
        if (custId == null || custId <= 0L) {
            return result;
        }
        List<SubAdslLeaseline> objs = supplier.findByCustId(cmPosSession, custId);
        //result = (SubAdslLeaseline) getBO(objs);
        return objs;
    }

    //duyetdk
    public List<Contract> findContractByCustId(Session cmPosSession, Long custId) {
        List<Contract> result = null;
        if (custId == null || custId <= 0L) {
            return result;
        }
        List<Contract> objs = supplier.findContractByCustId(cmPosSession, custId);
        //result = (SubAdslLeaseline) getBO(objs);
        return objs;
    }

    //duyetdk
    public static double distanceBetween2Points(double la1, double lo1,
            double la2, double lo2) {
        double dLat = (la2 - la1) * (Math.PI / 180);
        double dLon = (lo2 - lo1) * (Math.PI / 180);
        double la1ToRad = la1 * (Math.PI / 180);
        double la2ToRad = la2 * (Math.PI / 180);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(la1ToRad)
                * Math.cos(la2ToRad) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = (Constants.EARTH_RADIUS) * c;
        return d;
    }

    //duyetdk
    public VBtsAccount findCoorByCustId(Session cmPosSession, Long custId) {
        VBtsAccount result = null;
        if (custId == null || custId <= 0L) {
            return result;
        }
        List<VBtsAccount> objs = supplier.findCoorByCustId(cmPosSession, custId);
        result = (VBtsAccount) getBO(objs);
        return result;
    }

    public List<CcActionInformation> getComplainHistory(Session sessionCc, String account) {
        return supplier.getComplainHistory(sessionCc, account);
    }

    public List<CcActionInformation> getBillingCollectionHistory(Session payment, Session cmPos, String account) {
        List<CcActionInformation> list = new ArrayList<CcActionInformation>();
        SubAdslLeaseline listSub = new SubAdslLeaselineDAO().findByAccount(cmPos, account);
        if (listSub != null) {
            list = supplier.getBillingCollection(payment, listSub.getContractId());
        }
        return list;
    }

    public List<String> getAccountOfCustomer(Session sessionCmPos, Long customerId) {
        List<Contract> listContract = new ContractSupplier().findByCustId(sessionCmPos, customerId, null);
        List<String> listIsdn = new ArrayList<String>();
        if (listContract != null && !listContract.isEmpty()) {
            SubAdslLeaselineDAO subDao = new SubAdslLeaselineDAO();
            for (Contract c : listContract) {
                List<SubAdslLeaseline> listSub = subDao.findByContractIdAll(sessionCmPos, c.getContractId());
                if (listSub != null && !listSub.isEmpty()) {
                    for (SubAdslLeaseline sub : listSub) {
                        listIsdn.add(sub.getAccount());
                    }
                }
            }
        }
        return listIsdn;
    }

    //phuonghc
    public List<SubAdslLeaseline> findObjectByCustIdAndAccount(Session cmPosSession, Long custId, String account) {
        List<SubAdslLeaseline> result = null;
        if (custId == null || custId <= 0L) {
            return result;
        }
        List<SubAdslLeaseline> objs = supplier.findObjectByCustId(cmPosSession, custId, account);
        return objs;
    }
}
