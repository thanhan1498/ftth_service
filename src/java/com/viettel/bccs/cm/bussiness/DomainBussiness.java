package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.Domain;
import com.viettel.bccs.cm.supplier.DomainSupplier;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

public class DomainBussiness extends BaseBussiness {

    protected DomainSupplier supplier = new DomainSupplier();

    public DomainBussiness() {
        logger = Logger.getLogger(DomainBussiness.class);
    }

    public List<Domain> findActives(Session cmPosSession) {
        List<Domain> result = supplier.findByStatus(cmPosSession, Constants.STATUS_USE);
        return result;
    }
}
