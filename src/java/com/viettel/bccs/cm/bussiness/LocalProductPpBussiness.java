package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.LocalProductPp;
import com.viettel.bccs.cm.supplier.LocalProductPpSupplier;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

public class LocalProductPpBussiness extends BaseBussiness {

    protected LocalProductPpSupplier supplier = new LocalProductPpSupplier();

    public LocalProductPpBussiness() {
        logger = Logger.getLogger(LocalProductPpBussiness.class);
    }

    public LocalProductPp findById(Session cmPosSession, Long id) {
        LocalProductPp result = null;
        if (id == null || id <= 0L) {
            return result;
        }
        List<LocalProductPp> objs = supplier.findById(cmPosSession, id, Constants.STATUS_USE.toString());
        result = (LocalProductPp) getBO(objs);
        return result;
    }

    public List<LocalProductPp> findByOfferId(Session cmPosSession, Long offerId) {
        List<LocalProductPp> result = null;
        if (offerId == null || offerId <= 0L) {
            return result;
        }
        result = supplier.findByOfferId(cmPosSession, offerId, Constants.STATUS_USE.toString());
        return result;
    }
}
