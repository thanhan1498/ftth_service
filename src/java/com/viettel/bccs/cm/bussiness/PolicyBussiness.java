/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.bussiness.common.FTPBusiness;
import com.viettel.bccs.cm.model.PolicyFile;
import com.viettel.bccs.cm.model.pre.ApParamPre;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.supplier.cm.pre.PolicySupplier;
import com.viettel.brcd.dao.pre.ApParamDAO;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author vietnn6
 */
public class PolicyBussiness {

    protected static Logger logger = Logger.getLogger(PolicyBussiness.class);

    public List<String> getFilePolicy(String user, String pass, String sever, String link) throws Exception {
        return FTPBusiness.retrieveFileFromFTPServer(sever, user, pass, link);
    }

    public List<PolicyFile> getFilePolicy(Session cmPre, String fromDate, String toDate, Long objectType, Long serviceType, Long formType) throws Exception {
        List<PolicyFile> lstPolicy = new ArrayList<PolicyFile>();
        List<String> lstFileName = new PolicySupplier().findByDate(cmPre.connection(), fromDate, toDate, objectType, serviceType, formType);
        if (lstFileName == null || lstFileName.isEmpty()) {
            return null;
        } else {
            // get duong dan
            List<ApParamPre> lstParam = new ApParamDAO().findByTypeCode(cmPre, Constants.OUTPUT_FILE_POLICY, Constants.GET_FILE_DIRECTORY);
            if (lstParam == null || lstParam.isEmpty()) {
                return null;
            } else {
                String path = lstParam.get(0).getParamValue();
                for (String fileName : lstFileName) {
                    PolicyFile policyFile = new PolicyFile();
                    policyFile.setName(fileName);
                    policyFile.setLink(path + "/" + fileName);
                    lstPolicy.add(policyFile);
                }
                return lstPolicy;
            }
        }
    }
}
