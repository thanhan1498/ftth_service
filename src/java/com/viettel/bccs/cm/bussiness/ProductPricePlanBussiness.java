package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.ProductPricePlan;
import com.viettel.bccs.cm.supplier.ProductPricePlanSupplier;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

public class ProductPricePlanBussiness extends BaseBussiness {

    protected ProductPricePlanSupplier supplier = new ProductPricePlanSupplier();

    public ProductPricePlanBussiness() {
        logger = Logger.getLogger(ProductPricePlanBussiness.class);
    }

    public ProductPricePlan findById(Session cmPosSession, Long id) {
        ProductPricePlan result = null;
        if (id == null || id <= 0L) {
            return result;
        }
        List<ProductPricePlan> objs = supplier.findById(cmPosSession, id, Constants.STATUS_USE.toString());
        result = (ProductPricePlan) getBO(objs);
        return result;
    }

    public List<ProductPricePlan> findByOfferId(Session cmPosSession, Long offerId) {
        List<ProductPricePlan> result = null;
        if (offerId == null || offerId <= 0L) {
            return result;
        }
        result = supplier.findByOfferId(cmPosSession, offerId, Constants.STATUS_USE.toString());
        return result;
    }
}
