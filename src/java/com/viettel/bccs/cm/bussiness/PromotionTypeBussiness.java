package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.PromotionType;
import com.viettel.bccs.cm.supplier.PromotionTypeSupplier;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class PromotionTypeBussiness extends BaseBussiness {

    protected PromotionTypeSupplier supplier = new PromotionTypeSupplier();

    public PromotionTypeBussiness() {
        logger = Logger.getLogger(PromotionTypeBussiness.class);
    }

    public List<PromotionType> findByService(Session cmPosSession, String telService) {
        List<PromotionType> result = null;
        if (telService == null) {
            return result;
        }
        telService = telService.trim();
        if (telService.isEmpty()) {
            return result;
        }
        result = supplier.findByService(cmPosSession, telService);
        return result;
    }
}
