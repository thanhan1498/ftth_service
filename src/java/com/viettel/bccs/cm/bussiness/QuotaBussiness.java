package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.Quota;
import com.viettel.bccs.cm.supplier.QuotaSupplier;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

public class QuotaBussiness extends BaseBussiness {

    protected QuotaSupplier supplier = new QuotaSupplier();

    public QuotaBussiness() {
        logger = Logger.getLogger(QuotaBussiness.class);
    }

    public Quota findById(Session cmPosSession, Long quotaId) {
        Quota result = null;
        if (quotaId == null || quotaId <= 0L) {
            return result;
        }
        List<Quota> objs = supplier.findById(cmPosSession, quotaId, Constants.STATUS_USE);
        result = (Quota) getBO(objs);
        return result;
    }

    public List<Quota> findByService(Session cmPosSession, String serviceType) {
        List<Quota> result = null;
        if (serviceType == null) {
            return result;
        }
        serviceType = serviceType.trim();
        if (serviceType.isEmpty()) {
            return result;
        }
        result = supplier.findByService(cmPosSession, serviceType, Constants.STATUS_USE);
        return result;
    }
}
