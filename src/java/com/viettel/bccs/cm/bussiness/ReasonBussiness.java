package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.MapActiveInfo;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.pre.ReasonPre;
import com.viettel.bccs.cm.supplier.ReasonSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.dao.pre.ApParamDAO;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

public class ReasonBussiness extends BaseBussiness {

    protected ReasonSupplier supplier = new ReasonSupplier();

    public ReasonBussiness() {
        logger = Logger.getLogger(ReasonBussiness.class);
    }

    public Reason findById(Session cmPosSession, Long reasonId) {
        Reason result = null;
        if (reasonId == null || reasonId <= 0L) {
            return result;
        }
        List<Reason> objs = supplier.findById(cmPosSession, reasonId, Constants.STATUS_USE);
        result = (Reason) getBO(objs);
        return result;
    }

    public Reason findByCode(Session cmPosSession, String code) {
        Reason result = null;
        if (code == null) {
            return result;
        }
        code = code.trim();
        if (code.isEmpty()) {
            return result;
        }
        List<Reason> objs = supplier.findByCode(cmPosSession, code, Constants.STATUS_USE);
        result = (Reason) getBO(objs);
        return result;
    }

    public Reason findByTypeCode(Session cmPosSession, String type, String code) {
        Reason result = null;
        if (type == null || code == null) {
            return result;
        }
        type = type.trim();
        if (type.isEmpty()) {
            return result;
        }
        code = code.trim();
        if (code.isEmpty()) {
            return result;
        }
        List<Reason> objs = supplier.findByTypeCode(cmPosSession, type, code, Constants.STATUS_USE);
        result = (Reason) getBO(objs);
        return result;
    }

    public String getModelType(Long numPort, String infraType) {
        String modelType = Constants.MODEL_TYPE_2;
        if (numPort != null && !numPort.equals(0l)) {
            if (numPort >= 4L) {
                modelType = Constants.MODEL_TYPE_2;
            } else {
                modelType = Constants.MODEL_TYPE_1;
            }
        }
        return modelType;
    }

    public List<Reason> getRegTypes(Session cmPosSession, Long numPort, String productCode, String serviceType, Long channelTypeId, String province, String infraType) {
        if (Constants.SERVICE_ALIAS_ADSL.equals(serviceType)) {
            return getRegTypes(cmPosSession, productCode, serviceType);
        }
        if (Constants.SERVICE_ALIAS_FTTH.equals(serviceType)) {
            return getRegTypes(cmPosSession, channelTypeId, province, productCode, Constants.SERVICE_FTTH_ID_WEBSERVICE, numPort, infraType);
        }
        return getRegTypes(cmPosSession, channelTypeId, province, productCode, Constants.SERVICE_LEASELINE_ID_WEBSERVICE, numPort, infraType);
    }

    public List<Reason> getRegTypes(Session cmPosSession, String productCode, String telService) {
        List<Reason> result = null;
        if (telService == null) {
            return result;
        }
        telService = telService.trim();
        if (telService.isEmpty()) {
            return result;
        }
        result = supplier.getRegTypes(cmPosSession, productCode, telService);
        return result;
    }

    public List<Reason> getRegTypes(Session cmPosSession, Long channelTypeId, String province, String productCode, Long telServiceId, Long numPort, String infraType) {
        List<Reason> result = null;
        String modelType = getModelType(numPort, infraType);
        System.out.println("============ modelType: " + modelType);
        if (telServiceId == null || telServiceId <= 0L || modelType == null) {
            return result;
        }
        modelType = modelType.trim();
        if (modelType.isEmpty()) {
            return result;
        }
        result = supplier.getRegTypes(cmPosSession, channelTypeId, province, productCode, telServiceId, modelType);
        return result;
    }

    public List<Reason> find(Session cmPosSession, String actionCode, String telService, String productCode) {
        List<Reason> result = null;
        if (actionCode == null) {
            return result;
        }
        actionCode = actionCode.trim();
        if (actionCode.isEmpty()) {
            return result;
        }
        if (!new ApDomainBussiness().isMandatoryTrasaction(cmPosSession, actionCode)) {
            result = supplier.find(cmPosSession, actionCode, telService);
            return result;
        }
        result = supplier.find(cmPosSession, actionCode, telService, productCode);
        return result;
    }

    public List<ReasonPre> getRegTypesPre(Session cmPreSession, String user, String productCode, String serviceType, Long channelTypeId, String province) {
        String checkMap = new ApParamDAO().getValueByTypeCode(cmPreSession, Constants.SERVICE_ALIAS_HOMEPHONE, "CHECK_MAP_TEL_SERVICE");
        List<ReasonPre> lstRegType = new ArrayList<ReasonPre>();
        if (checkMap != null && !checkMap.isEmpty()) {
            lstRegType = supplier.getAllRegTypeListByUser(cmPreSession, user.toUpperCase(), province, null, productCode, channelTypeId);
        } else {
            if (Constants.SHOP_TYPE_AGENT_XNK.equals(channelTypeId)) {
                lstRegType = supplier.getListRegTypeForAgent(cmPreSession, Constants.REASON_CODE_FOR_AGENT_XNK);
            } else if (Constants.SHOP_TYPE_AGENT_DELEGATE.equals(channelTypeId)) {
                lstRegType = supplier.getListRegTypeForAgent(cmPreSession, Constants.REASON_CODE_FOR_AGENT_DELEGATE);
            } else {
                lstRegType = supplier.getAllRegTypeListByUser(cmPreSession, productCode, user.toUpperCase());
            }
        }
        return lstRegType;
    }

    public List<Reason> getReasonPayAdvance(Session cmPreSession, Session payment, Session product, Long contractId, String isdn) throws Exception {
        return supplier.getReasonPayAdvance(cmPreSession, payment, product, contractId, isdn);
    }

    /**
     * @author : duyetdk
     * @des: find Deposit from MapActiveInfo by regReasonId
     * @since 15-03-2018
     */
    public MapActiveInfo findByReasonId(Session cmPosSession, Long reasonId) {
        MapActiveInfo result = null;
        if (reasonId == null || reasonId <= 0L) {
            return result;
        }
        List<MapActiveInfo> objs = supplier.find(cmPosSession, null, null, null, null, reasonId);
        result = (MapActiveInfo) getBO(objs);
        return result;
    }
}
