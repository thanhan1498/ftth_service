/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.api.Task.BO.Reason;
import com.viettel.bccs.api.Task.DAO.CustomerDAO;
import com.viettel.bccs.cm.model.FormChangeInformation;
import com.viettel.bccs.cm.model.SubMb;
import com.viettel.bccs.cm.supplier.RequestChangeInformationSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.brcd.ws.model.output.ChangeInformationOut;
import com.viettel.bccs.cm.common.util.Constant;
import com.viettel.bccs.cm.model.CustImagePre;
import com.viettel.bccs.cm.model.ElementImageChangeInformation;
import com.viettel.bccs.cm.model.OTPObject;
import com.viettel.bccs.cm.model.pre.CustomerPre;
import com.viettel.bccs.cm.model.pre.ReasonPre;
import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.bccs.cm.util.DateTimeUtils;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.common.util.FileSaveProcessCR;
import com.viettel.brcd.dao.pre.IsdnSendSmsDAO;
import com.viettel.brcd.ws.vsa.ResourceBundleUtil;
import java.sql.CallableStatement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class RequestChangeInformationBusiness extends BaseBussiness {

    protected RequestChangeInformationSupplier supplier = new RequestChangeInformationSupplier();

    public RequestChangeInformationBusiness() {
        logger = Logger.getLogger(RequestChangeInformationBusiness.class);
    }

    public SubMb getSubMbByISDN(Session cmPreSession, Session cmProductSession,
            Session cmIMSession, String isdn) {
        SubMb result = null;
        if (isdn == null || isdn.trim().isEmpty()) {
            return result;
        }
        if (isdn.startsWith("0") || isdn.startsWith("855")) {
            if (isdn.startsWith("0")) {
                isdn = isdn.substring(1);
            }
            if (isdn.startsWith("855")) {
                isdn = isdn.substring(3);
            }
        }
        if (isdn == null || isdn.trim().isEmpty()) {
            return result;
        }
        List<SubMb> objs = supplier.getSubMbFromISDN(cmPreSession, isdn);
        result = (SubMb) getBO(objs);
        if (result == null) {
            return result;
        }
        if (result != null && result.getOfferId() > 0L) {
            String offerName = supplier.getOfferNameFromOfferId(cmProductSession, result.getOfferId());
            result.setOfferName(offerName);
        }
        String groupNames = supplier.getGroupNameByISDN(cmIMSession, isdn);
        result.setGroupName(groupNames);
        if (result.getStatus() == 1L) {
            result.setStatusName("Not connect");
        } else if (result.getStatus() == 3L) {
            result.setStatusName("Terminated");
        } else if (result.getStatus() == 2L) {
            if ("00".equals(result.getActStatus())) {
                result.setStatusName("Normal");
            } else if ("01".equals(result.getActStatus())) {
                result.setStatusName("One way block (Metfone)");
            } else if ("02".equals(result.getActStatus())) {
                result.setStatusName("Two way block (Metfone)");
            } else if ("03".equals(result.getActStatus())) {
                result.setStatusName("Inactive");
            } else if ("04".equals(result.getActStatus())) {
                result.setStatusName("One way block (by customer request)");
            }
        }
        return result;
    }

    public List<ReasonPre> getListReasonByCodes(Session cmPreSession, List<String> codes) {
        List<ReasonPre> result = null;
        if (codes == null || codes.size() <= 0) {
            return result;
        }
        result = supplier.getReasonFromCodes(cmPreSession, codes);
        return result;
    }

    public OTPObject getOTPChangeInformationPre(String isdn) {
        return supplier.getOTPFromService_(isdn);

    }

    public ChangeInformationOut changeInformationCustomer(Session imPreSession, Session cmPreSession, String locale,
            String otp, Long subId, String serviceType, Long custId, String name,
            Long idType, String province, String address, String DOB, String Sex,
            String isdn, String contact, FormChangeInformation form,
            FormChangeInformation lstImageIDCard, FormChangeInformation lstOldImageIDCard,
            String reasonCode, Long reasonId, String idNo, String staffCode) throws ParseException, Exception {
        System.out.println("Debug address = " + address);
        ChangeInformationOut result = null;
        //<editor-fold defaultstate="collapsed" desc="Kiem tra so OTP">
        //Tam thoi chua thong mang nen chua goi check OTP
        boolean checkOTP = supplier.checkOTP_(isdn, otp);
        //<editor-fold defaultstate="collapsed" desc="Test OTP = 123456 => true">
        /*
        if (otp.equals("123456")) {
            checkOTP = true;
        }
         */
        //</editor-fold>
        if (!checkOTP) {
            result = new ChangeInformationOut(Constants.ERROR_CODE_4, "OTP is incorrect");
            return result;
        }
        //</editor-fold>
        CustomerPre cust = null;
        //<editor-fold defaultstate="collapsed" desc="Kiem tra PRODUCT_CODE truyen len co null hay ko">
        String packageWhiteList = ResourceUtils.getResource("PACKAGE_WHITE_LIST", "provisioning");
        List<SubMb> subMbChangeInfoLst = supplier.getSubMbChangeInfo(cmPreSession, subId);
        SubMb subMbChangeInfo = null;
        if (subMbChangeInfoLst != null && subMbChangeInfoLst.size() > 0) {
            subMbChangeInfo = subMbChangeInfoLst.get(0);
        }
        if (subMbChangeInfo == null) {
            result = new ChangeInformationOut(Constants.ERROR_CODE_1, "SubId is not exist");
            return result;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Truong hop SubMb nam trong whitelist">
        if (subMbChangeInfo.getProductCode() != null && packageWhiteList != null) {
            if (packageWhiteList.contains(subMbChangeInfo.getProductCode().toUpperCase())) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Can not change information this number");
                return result;
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Truong hop SubMb ko nam trong whitelist">
        //<editor-fold defaultstate="collapsed" desc="Lay ra danh sach cust_image theo serial=subMb.serial isdn=submb.isdn status=1">
        List<CustImagePre> listCustImage = supplier.getCustImageFromSerialAndIsdn(cmPreSession,
                subMbChangeInfo.getSerial(), subMbChangeInfo.getIsdn());
        //</editor-fold>
        String mess = null;
        Long staffType = 2L;
        String account = ResourceUtils.getResource("default.folder.upload", "provisioning");
        if (staffCode != null && !staffCode.trim().equals("")) {
            account = staffCode;
        }
        CustomerDAO cusDAO = new CustomerDAO();
        CustomerPre newCust = new CustomerPre();
        Date nowDate = com.viettel.bccs.cm.common.util.Common.getDateTimeFromDB(cmPreSession);
        boolean custIdIsNull = false;
        //<editor-fold defaultstate="collapsed" desc="Kiem tra sub_mb.custId is null">
        if (subMbChangeInfo.getCustId() == null) {
            custIdIsNull = true;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Truong hop khong co du lieu custImage">
        if (listCustImage == null || listCustImage.isEmpty() || custIdIsNull) {
            //<editor-fold defaultstate="collapsed" desc="Tao moi customer">
            mess = supplier.insertCustomer(cmPreSession, locale, name, DOB, Sex, idNo.toUpperCase(), idType, province, address,
                    staffCode, nowDate, staffCode, nowDate, contact);
            Long custIdReturn = ConvertUtils.toLong(mess);
            custId = custIdReturn;
            if (mess != null) {
                if (custIdReturn == null || custIdReturn == 0L) {
                    mess = mess.trim();
                    if (!mess.isEmpty()) {
                        result = new ChangeInformationOut(Constants.ERROR_CODE_1, "A error has occurred: "
                                + "Create customer error");
                        return result;
                    }
                }
            }
            //</editor-fold> 
            //<editor-fold defaultstate="collapsed" desc="Update SubMb theo subId">
            mess = supplier.updateSubMbChangeInfoBySubId(cmPreSession, locale, name, DOB, contact,
                    address, Sex, custId, subId, province);
            Long subIdReturn = ConvertUtils.toLong(mess);
            if (mess != null) {
                if (subIdReturn == null || subIdReturn == 0L) {
                    mess = mess.trim();
                    if (!mess.isEmpty()) {
                        result = new ChangeInformationOut(Constants.ERROR_CODE_1, "A error has occurred: "
                                + "Update SubMb error");
                        return result;
                    }
                }
            }
            //</editor-fold> 
            //<editor-fold defaultstate="collapsed" desc="Insert cust_image/cust_image_detail/upload file">
            List<ElementImageChangeInformation> elements = lstImageIDCard.getElement();
            List<ElementImageChangeInformation> elementsForm = form.getElement();
            try {
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
                //<editor-fold defaultstate="collapsed" desc="Tao moi cust_image">
                Long custImagePreCreated = supplier.createCustImageHaveIsdn(cmPreSession, custId, account, idNo.toUpperCase(),
                        staffCode, name, subMbChangeInfo.getSerial(),
                        subMbChangeInfo.getIsdn(), staffType);
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="Kiểm tra cust_image vừa tạo">

                //</editor-fold>
                if (custImagePreCreated != null && custImagePreCreated.compareTo(0l) > 0) {
                    //<editor-fold defaultstate="collapsed" desc="Lưu lại file upload lên, thêm mới bản ghi CUST_IMAGE_DETAIL">
                    for (ElementImageChangeInformation imageInput : elements) {
                        String imageDate = dateFormat.format(new Date());
                        Long custImageDetailId = getSequence(cmPreSession, Constants.CUST_IMAGE_DETAIL_SEQ);
                        String imageName = staffCode + "_" + imageDate + "_" + custImageDetailId + ".jpg";
                        boolean checkUpload = new FileSaveProcessCR().saveFile(imageName, imageInput.getData(), account);
                        if (!checkUpload) {
                            logger.info("\n--------------\nUpload image Faile\n--------------\n");
                            continue;
                        }
                        int checkInsertDB = cusDAO.insertCusImageDetailV1(cmPreSession, custImagePreCreated, imageName,
                                imageInput.getName(), "ID_CARD", staffCode, custImageDetailId);
                        if (checkInsertDB == 0) {
                            logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                        }
                    }
                    for (ElementImageChangeInformation imageInput : elementsForm) {
                        String imageDate = dateFormat.format(new Date());
                        Long custImageDetailId = getSequence(cmPreSession, Constants.CUST_IMAGE_DETAIL_SEQ);
                        String imageName = staffCode + "_" + imageDate + "_" + custImageDetailId + ".jpg";

                        boolean checkUpload = new FileSaveProcessCR().saveFile(imageName, imageInput.getData(), account);
                        if (!checkUpload) {
                            logger.info("\n--------------\nUpload image Faile\n--------------\n");
                            continue;
                        }
                        int checkInsertDB = cusDAO.insertCusImageDetailV1(cmPreSession, custImagePreCreated, imageName,
                                imageInput.getName(), "CHANGE_INFOR_FORM", staffCode, custImageDetailId);
                        if (checkInsertDB == 0) {
                            logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                        }
                    }
                    //</editor-fold>
                } else {
                    //<editor-fold defaultstate="collapsed" desc="Tao ban ghi cust_image loi">
                    result = new ChangeInformationOut(Constants.ERROR_CODE_1, "A error has occurred: "
                            + "insert cust_image failed");
                    return result;
                    //</editor-fold>
                }

            } catch (Exception e) {
                logger.info("\n--------------\nUpload image Faile\n--------------\n" + e);
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Ghi log tao moi customer">
            //<editor-fold defaultstate="collapsed" desc="Ghi log audit">
            Long actionAuditId = new BaseSupplier().getSequence(cmPreSession, Constants.SEQ_ACTION_AUDIT_PRE);
            nowDate = com.viettel.bccs.cm.common.util.Common.getDateTimeFromDB(cmPreSession);
            supplier.logActionV2(actionAuditId, subId, Constant.ACTION_AUDIT_PK_TYPE_SUBSCRIBER, reasonId, "15", "Create customer information, has CustId : " + custId,
                    staffCode, "", nowDate, cmPreSession, "mBCCS");
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Ghi log audit_detail">
            newCust = new CustomerPre();
            newCust.setCustId(custId);
            newCust.setName(name);
            Date bDate = DateTimeUtils.toDateddMMyyyy(DOB);
            newCust.setBirthDate(bDate);
            newCust.setSex(Sex);
            newCust.setIdNo(idNo);
            newCust.setIdType(idType);
            newCust.setProvince(province);
            newCust.setAddress(address);
            newCust.setTelFax(contact);
            Date issDateTime = cusDAO.getSysdate(cmPreSession);
            if (cust == null) {
                cust = new CustomerPre();
            }
            supplier.logCustomerNew(cust, newCust, actionAuditId, issDateTime, cmPreSession);
            //</editor-fold> 
            //</editor-fold>
        } //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Truong hop co du lieu custImage">
        else {
            //<editor-fold defaultstate="collapsed" desc="Lay ra list sub_mb cust_id=req.custid status=2 (active) => ListA">
            List<SubMb> listSubMbByCustId = null;
            if (custId != null) {
                System.out.println("Lay ra list sub_mb where custid=req.custid = " + custId);
                listSubMbByCustId = supplier.getListSubMbChangeInfoActiveByCustId(cmPreSession, custId);
            } else {
                System.out.println("Lay ra list sub_mb where custid=subMb.custid = " + subMbChangeInfo.getCustId());
                listSubMbByCustId = supplier.getListSubMbChangeInfoActiveByCustId(cmPreSession, subMbChangeInfo.getCustId());
            }
            //</editor-fold>
            List<CustomerPre> listCust = null;
            //<editor-fold defaultstate="collapsed" desc="Lay ra customer where custid=subMb.custid => Object1">
            System.out.println("Lay ra customer where custid=subMb.custid = " + subMbChangeInfo.getCustId());
            listCust = supplier.checkCustExistByCustId(cmPreSession, subMbChangeInfo.getCustId());
            if (listCust != null && listCust.size() > 0) {
                cust = (CustomerPre) getBO(listCust);
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="object1 != null && listA.size() == 1 ==> true">
            if (listSubMbByCustId != null && listSubMbByCustId.size() == 1 && cust != null) {
                //<editor-fold defaultstate="collapsed" desc="Update customer (object1)">
                if (custId == null) {
                    custId = subMbChangeInfo.getCustId();
                }
                mess = supplier.updateCustomerChangeInfo(cmPreSession, locale, name, idType, idNo, DOB, address, custId, contact,
                        Sex, province);
                Long custIdReturn = ConvertUtils.toLong(mess);
                if (mess != null && custIdReturn == null) {
                    mess = mess.trim();
                    if (!mess.isEmpty()) {
                        result = new ChangeInformationOut(Constants.ERROR_CODE_1, "A error has occurred: "
                                + "Update customer error");
                        return result;
                    }
                }
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="Ghi log update customer">
                //<editor-fold defaultstate="collapsed" desc="Ghi log audit">
                Long actionAuditId = new BaseSupplier().getSequence(cmPreSession, Constants.SEQ_ACTION_AUDIT_PRE);
                nowDate = com.viettel.bccs.cm.common.util.Common.getDateTimeFromDB(cmPreSession);
                supplier.logActionV2(actionAuditId, subId, Constant.ACTION_AUDIT_PK_TYPE_SUBSCRIBER, reasonId, "152", "This Id update from staff " + staffCode,
                        staffCode, "", nowDate, cmPreSession, "mBCCS");
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="Ghi log audit_detail update">
                newCust = new CustomerPre();
                newCust.setCustId(custId);
                newCust.setName(name);
                Date bDate = DateTimeUtils.toDateddMMyyyy(DOB);
                newCust.setBirthDate(bDate);
                newCust.setSex(Sex);
                newCust.setIdNo(idNo);
                newCust.setIdType(idType);
                newCust.setProvince(province);
                newCust.setAddress(address);
                newCust.setTelFax(contact);

                Date issDateTime = cusDAO.getSysdate(cmPreSession);
                if (cust == null) {
                    cust = new CustomerPre();
                }
                supplier.logCustomer(cust, newCust, actionAuditId, issDateTime, cmPreSession);
                //</editor-fold> 
                //</editor-fold>
            } //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="object1 != null && listA.size() == 1 ==> false">
            else {
                //<editor-fold defaultstate="collapsed" desc="Tao moi customer">
                mess = supplier.insertCustomer(cmPreSession, locale, name, DOB, Sex, idNo.toUpperCase(), idType, province, address,
                        staffCode, nowDate, staffCode, nowDate, contact);
                Long custIdReturn = ConvertUtils.toLong(mess);
                custId = custIdReturn;
                if (mess != null) {
                    if (custIdReturn == null || custIdReturn == 0L) {
                        mess = mess.trim();
                        if (!mess.isEmpty()) {
                            result = new ChangeInformationOut(Constants.ERROR_CODE_1, "A error has occurred: "
                                    + "Create customer error");
                            return result;
                        }
                    }
                }
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="Ghi log tao moi customer">
                //<editor-fold defaultstate="collapsed" desc="Ghi log audit">
                Long actionAuditId = new BaseSupplier().getSequence(cmPreSession, Constants.SEQ_ACTION_AUDIT_PRE);
                nowDate = com.viettel.bccs.cm.common.util.Common.getDateTimeFromDB(cmPreSession);
                supplier.logActionV2(actionAuditId, subId, Constant.ACTION_AUDIT_PK_TYPE_SUBSCRIBER, reasonId, "15", "Create customer information, has CustId : " + custId,
                        staffCode, "", nowDate, cmPreSession, "mBCCS");
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="Ghi log audit_detail">
                newCust = new CustomerPre();
                newCust.setCustId(custId);
                newCust.setName(name);
                Date bDate = DateTimeUtils.toDateddMMyyyy(DOB);
                newCust.setBirthDate(bDate);
                newCust.setSex(Sex);
                newCust.setIdNo(idNo);
                newCust.setIdType(idType);
                newCust.setProvince(province);
                newCust.setAddress(address);
                newCust.setTelFax(contact);
                Date issDateTime = cusDAO.getSysdate(cmPreSession);
                if (cust == null) {
                    cust = new CustomerPre();
                }
                supplier.logCustomerNew(cust, newCust, actionAuditId, issDateTime, cmPreSession);
                //</editor-fold> 
                //</editor-fold>
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Update SubMb where sub_id = req.subId">
            mess = supplier.updateSubMbBySubId(cmPreSession, locale, name, DOB,
                    contact, address, Sex, custId, subId, province);
            Long subIdReturn = ConvertUtils.toLong(mess);
            if (mess != null) {
                if (subIdReturn == null || subIdReturn == 0L) {
                    mess = mess.trim();
                    if (!mess.isEmpty()) {
                        result = new ChangeInformationOut(Constants.ERROR_CODE_1, "A error has occurred: "
                                + "Update SubMb error");
                        return result;
                    }
                }
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Lay ra danh sach cust_image isdn = req.isdn status = 1">
            List<CustImagePre> listCustImageFromIsdn = supplier.getCustImageActiveFromIsdn(cmPreSession, isdn);
            //</editor-fold>
            if (listCustImageFromIsdn != null && listCustImageFromIsdn.size() > 0) {
                //<editor-fold defaultstate="collapsed" desc="Update cust_image">
                for (int i = 0; i < listCustImageFromIsdn.size(); i++) {
                    Long custImageId = listCustImageFromIsdn.get(i).getId();
                    supplier.updateInfoCustomerImage(cmPreSession, staffCode, custImageId);
                }
                //</editor-fold>         
            }
            //<editor-fold defaultstate="collapsed" desc="Insert cust_image/cust_image_detail/upload file">
            List<ElementImageChangeInformation> elements = lstImageIDCard.getElement();
            List<ElementImageChangeInformation> elementsForm = form.getElement();
            try {
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
                //<editor-fold defaultstate="collapsed" desc="Tao moi cust_image">
                Long createCustImageId = supplier.createCustImageHaveIsdn(cmPreSession, custId, account, idNo.toUpperCase(),
                        staffCode, name, subMbChangeInfo.getSerial(),
                        subMbChangeInfo.getIsdn(), staffType);
                //</editor-fold>
                if (createCustImageId != null && createCustImageId.compareTo(0l) > 0) {
                    //<editor-fold defaultstate="collapsed" desc="Lưu lại file upload lên, thêm mới bản ghi CUST_IMAGE_DETAIL">
                    for (ElementImageChangeInformation imageInput : elements) {
                        String imageDate = dateFormat.format(new Date());
                        Long custImageDetailId = getSequence(cmPreSession, Constants.CUST_IMAGE_DETAIL_SEQ);
                        String imageName = staffCode + "_" + imageDate + "_" + custImageDetailId + ".jpg";
                        boolean checkUpload = new FileSaveProcessCR().saveFile(imageName, imageInput.getData(), account);
                        if (!checkUpload) {
                            logger.info("\n--------------\nUpload image Faile\n--------------\n");
                            continue;
                        }
                        int checkInsertDB = cusDAO.insertCusImageDetailV1(cmPreSession, createCustImageId, imageName,
                                imageInput.getName(), "ID_CARD", staffCode, custImageDetailId);
                        if (checkInsertDB == 0) {
                            logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                        }
                    }
                    for (ElementImageChangeInformation imageInput : elementsForm) {
                        String imageDate = dateFormat.format(new Date());
                        Long custImageDetailId = getSequence(cmPreSession, Constants.CUST_IMAGE_DETAIL_SEQ);
                        String imageName = staffCode + "_" + imageDate + "_" + custImageDetailId + ".jpg";

                        boolean checkUpload = new FileSaveProcessCR().saveFile(imageName, imageInput.getData(), account);
                        if (!checkUpload) {
                            logger.info("\n--------------\nUpload image Faile\n--------------\n");
                            continue;
                        }
                        int checkInsertDB = cusDAO.insertCusImageDetailV1(cmPreSession, createCustImageId, imageName,
                                imageInput.getName(), "CHANGE_INFOR_FORM", staffCode, custImageDetailId);
                        if (checkInsertDB == 0) {
                            logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                        }
                    }
                    //</editor-fold>
                } else {
                    //<editor-fold defaultstate="collapsed" desc="Tao ban ghi cust_image loi">
                    result = new ChangeInformationOut(Constants.ERROR_CODE_1, "A error has occurred: "
                            + "insert cust_image failed");
                    return result;
                    //</editor-fold>
                }

            } catch (Exception e) {
                logger.info("\n--------------\nUpload image Faile\n--------------\n" + e);
            }
            //</editor-fold>

        }
        //</editor-fold>
        //</editor-fold>
        //send sms
        sendSMS(cmPreSession, isdn, Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.PARAM_TYPE_CHANGE_INFO_SUCCESS, Arrays.asList(isdn));
        //send test message toi so dien thoai cua Suong
        //sendSMS(cmPreSession, "972550101", Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.PARAM_TYPE_CHANGE_INFO_SUCCESS, Arrays.asList(isdn));
        result = new ChangeInformationOut(Constants.ERROR_CODE_0, "Successfully");
        return result;
    }

    public void sendSMS(Session cmPre, String isdn, String paramType, String paramCode, List<String> listParam) {
        CallableStatement cs = null;
        try {
            if (isdn.startsWith("0")) {
                isdn = isdn.replaceFirst("0", "");
            }
            if (isdn.startsWith("855")) {
                isdn = isdn.replaceFirst("855", "");
            }
            IsdnSendSmsDAO isdnDao = new IsdnSendSmsDAO();
            List<String> listContentSms = isdnDao.genarateSMSContent(cmPre, null, paramType, paramCode, listParam);
            cs = cmPre.connection().prepareCall("{call CM_PRE2.send_sms_alert(?, ?, ?)}");
            for (String content : listContentSms) {

                cs.setString(1, isdn);
                cs.setString(2, "Metfone");
                cs.setString(3, content);
                cs.execute();

            }
        } catch (Exception ex) {
            logger.error("sendSMS: " + ex.getMessage());
        } finally {
            try {
                if (cs != null) {
                    cs.close();
                }
            } catch (Exception ex) {
            }
        }
    }
}
