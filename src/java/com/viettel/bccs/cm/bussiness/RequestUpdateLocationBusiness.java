/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.dao.CustImageDetailDAO;
import com.viettel.bccs.cm.model.ActionAudit;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.PotentialCustomer;
import com.viettel.bccs.cm.model.PotentialCustomerHistoryDto;
import com.viettel.bccs.cm.model.RequestUpdateLocation;
import com.viettel.bccs.cm.supplier.CustomerSupplier;
import com.viettel.bccs.cm.supplier.RequestUpdateLocationSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.ImageInput;
import com.viettel.brcd.ws.model.input.NewImageInput;
import com.viettel.brcd.ws.vsa.ResourceBundleUtil;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author duyetdk
 */
public class RequestUpdateLocationBusiness extends BaseBussiness {

    protected RequestUpdateLocationSupplier supplier = new RequestUpdateLocationSupplier();

    public RequestUpdateLocationBusiness() {
        logger = Logger.getLogger(RequestUpdateLocationBusiness.class);
    }

    public RequestUpdateLocation findById(Session cmPosSession, Long rulId, String province) {
        RequestUpdateLocation result = null;
        if (rulId == null || rulId <= 0L) {
            return result;
        }
        if (province == null || province.trim().isEmpty()) {
            return result;
        }
        List<RequestUpdateLocation> objs = supplier.findById(cmPosSession, rulId, Constants.WAITING, province);
        result = (RequestUpdateLocation) getBO(objs);
        return result;
    }

    public RequestUpdateLocation findWaitingOver30days(Session cmPosSession, Long cusId) {
        RequestUpdateLocation result = null;
        if (cusId == null || cusId <= 0L) {
            return result;
        }
        List<RequestUpdateLocation> objs = supplier.findWaitingOver30days(cmPosSession, cusId);
        result = (RequestUpdateLocation) getBO(objs);
        return result;
    }

    public String sendRequestUpdateLocation(Session cmPosSession, Long custId, String cusName, String address, Long staffId,
            String xLocation, String yLocation, String cxLocation, String cyLocation, String locale, String createUser,
            com.viettel.im.database.BO.Shop shop, String staffOwnType, String reasonRequest, List<ImageInput> lstImageList, String account, Long subId) throws Exception {
        String mess = null;
        Date nowDate = getSysDateTime(cmPosSession);
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="custId">
        if (custId == null || custId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "custId");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="xLocation">
        if (xLocation == null) {
            mess = LabelUtil.getKey("validate.required", locale, "xLocation");
            return mess;
        }
        xLocation = xLocation.trim();
        if (xLocation.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "xLocation");
            return mess;
        }
        Integer length = ReflectUtils.getColumnLength(RequestUpdateLocation.class, "xLocation");
        if (length != null && length > 0 && xLocation.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("xLocation", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="yLocation">
        if (yLocation == null) {
            mess = LabelUtil.getKey("validate.required", locale, "yLocation");
            return mess;
        }
        yLocation = yLocation.trim();
        if (yLocation.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "yLocation");
            return mess;
        }
        length = ReflectUtils.getColumnLength(RequestUpdateLocation.class, "yLocation");
        if (length != null && length > 0 && yLocation.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("yLocation", locale), length);
            return mess;
        }
        //</editor-fold>
        //</editor-fold>
        Long custImageId = null;
        if (lstImageList != null && !lstImageList.isEmpty()) {
            custImageId = new CustImageDetailDAO().insertCustImage(cmPosSession, lstImageList, account, Constants.IMAGE_TYPE_LOCATION_CUSTOMER,
                    subId, createUser, logger, ResourceBundleUtil.getResource("temp.folder.image.location"), xLocation, yLocation);
        }


        supplier.insert(cmPosSession, custId, cusName, address, staffId,
                nowDate, xLocation, yLocation, cxLocation, cyLocation, createUser, shop, staffOwnType,
                reasonRequest, account, custImageId);
        return mess;
    }

    public Long count(Session cmPosSession, Date date, Long status, Long duration, String province, String createUser, String custName) {
        Long result = supplier.count(cmPosSession, date, status, duration, province, createUser, custName);
        return result;
    }

    public List<RequestUpdateLocation> find(Session cmPosSession, Date date, Long status, Long duration, Integer start, Integer max,
            String province, String createUser, String custName) {
        List<RequestUpdateLocation> result = supplier.find(cmPosSession, date, status, duration, start, max, province, createUser, custName);
        return result;
    }

    //reject request update customer location
    public String rejectOrApproveRUL(Session cmPosSession, Long type, Long rulId, String staffCode, String shopCode, String province, String description, String locale) throws Exception {
        String mess = null;
        LogUtils.info(logger, "RequestUpdateLocationBusiness.rejectRUL:rulId=" + rulId + ";staffCode="
                + staffCode + ";shopCode=" + shopCode + ";description=" + description + ";locale=" + locale);
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="rulId">
        if (rulId == null || rulId <= 0L) {
            mess = LabelUtil.getKey("validate.required.rulId", locale);
            return mess;
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="RequestUpdateLocation">
        RequestUpdateLocation rul = findById(cmPosSession, rulId, province);
        if (rul == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "rul");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="update database CM">
        Date nowDate = getSysDateTime(cmPosSession);
        //<editor-fold defaultstate="collapsed" desc="luu log action">
        ActionAuditBussiness auditDAO = new ActionAuditBussiness();
        StringBuilder des = new StringBuilder()
                .append("Reject request update customer location, Request_update_customer_location_id=").append(rul.getRulId())
                .append(", cusId=").append(rul.getCusId());
        LogUtils.info(logger, "RequestUpdateLocationBusiness.cancelRequest:start save ActionAudit");
        ActionAudit audit = auditDAO.insert(cmPosSession, nowDate, Constants.ACTION_REJECT_REQUEST_UPDATE_CUSTOMER_LOCATION, null, shopCode, staffCode, Constants.ACTION_AUDIT_PK_TYPE_CUSTOMER, rul.getRulId(), Constants.WS_IP, des.toString());
        LogUtils.info(logger, "RequestUpdateLocationBusiness.cancelRequest:end save ActionAudit");
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="reject request update customer location">
        if (Constants.REJECT.equals(type) && Constants.WAITING.equals(rul.getStatus())) {
            updateStatus(cmPosSession, rul, Constants.REJECT, staffCode, description, nowDate, audit.getActionAuditId(), nowDate);
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="approve request update customer location">
        Customer cus = new CustomerBussiness().findById(cmPosSession, rul.getCusId());
        if (Constants.APPROVED.equals(type) && Constants.WAITING.equals(rul.getStatus())) {
            updateStatus(cmPosSession, rul, Constants.APPROVED, staffCode, Constants.APPROVAL, nowDate, audit.getActionAuditId(), nowDate);
            new CustomerSupplier().update(cmPosSession, cus, rul.getxLocation(), rul.getyLocation(), staffCode);
        }
        //</editor-fold>


        return mess;
    }

    private void updateStatus(Session cmPosSession, RequestUpdateLocation rul, Long status, String staffCode, String description, Date lastUpdateDate, Long actionAuditId, Date nowDate) {
        Object oldValue = rul.getStatus();
        rul.setStatus(status);
        rul.setDescription(description);
        rul.setLastUpdateUser(staffCode.trim().toUpperCase());
        rul.setLastUpdateDate(lastUpdateDate);
        //Object newValue = rul.getStatus();

        new ActionDetailBussiness().insert(cmPosSession, actionAuditId, ReflectUtils.getTableName(RequestUpdateLocation.class), rul.getRulId(), ReflectUtils.getColumnName(RequestUpdateLocation.class, "status"), oldValue, status, nowDate);

        cmPosSession.update(rul);
        cmPosSession.flush();
    }

    public boolean isRequestWaiting30days(Session cmPosSession, Long custId) throws Exception {
        Long count = supplier.countWaiting30days(cmPosSession, custId);
        return count != null && count > 0L;
    }

    public boolean isRequestWaitingOver30days(Session cmPosSession, Long custId) {
        Long count = supplier.countWaitingOver30days(cmPosSession, custId);
        return count != null && count > 0L;
    }

    public Long countApproved(Session cmPosSession, Long cusId) {
        //<editor-fold defaultstate="collapsed" desc="cusId">
        if (cusId == null || cusId <= 0L) {
            return 0L;
        }
        //</editor-fold>
        Long result = supplier.countApproved(cmPosSession, cusId);
        return result;
    }

    public List<RequestUpdateLocation> findApproved(Session cmPosSession, Long cusId, Integer start, Integer max) {
        //<editor-fold defaultstate="collapsed" desc="cusId">
        if (cusId == null || cusId <= 0L) {
            return null;
        }
        //</editor-fold>
        List<RequestUpdateLocation> result = supplier.findApproved(cmPosSession, cusId, start, max);
        return result;
    }

    public String approveAll(Session cmPosSession, Long staffId, Long shopId, String province, String locale,
            Date date, Long duration, String createUser, String custName, String staffCode, String shopCode) throws Exception {
        String mess = null;
        LogUtils.info(logger, "RequestUpdateLocationBusiness.approveAll:locale=" + locale + ";staffId="
                + staffId + ";shopId=" + shopId + ";province=" + province);
        //<editor-fold defaultstate="collapsed" desc="province">
        if (province == null || province.trim().isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "province");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="RequestUpdateLocation">
        List<RequestUpdateLocation> lst_rul = findWaiting(cmPosSession, province, date, duration, createUser, custName);
        if (lst_rul == null || lst_rul.isEmpty()) {
            mess = LabelUtil.getKey("not.found.data.approve", locale);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="update database CM">
        Date nowDate = getSysDateTime(cmPosSession);
        //<editor-fold defaultstate="collapsed" desc="luu log action">
        ActionAuditBussiness auditDAO = new ActionAuditBussiness();
        LogUtils.info(logger, "RequestUpdateLocationBusiness.approveAll:start save ActionAudit");
        ActionAudit audit = auditDAO.insert(cmPosSession, nowDate, Constants.APPROVE_ALL, null, shopCode, staffCode, Constants.ACTION_AUDIT_PK_TYPE_CUSTOMER, null, Constants.WS_IP, Constants.APPROVE_ALL);
        LogUtils.info(logger, "RequestUpdateLocationBusiness.approveAll:end save ActionAudit");
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="approve all">
        for (RequestUpdateLocation rul : lst_rul) {
            Customer cus = new CustomerBussiness().findById(cmPosSession, rul.getCusId());
            if (cus != null) {
                updateStatus(cmPosSession, rul, Constants.APPROVED, staffCode, Constants.APPROVAL, nowDate, audit.getActionAuditId(), nowDate);
                new CustomerSupplier().update(cmPosSession, cus, rul.getxLocation(), rul.getyLocation(), staffCode);
            } else {
                mess = "error";
                return mess;
            }
        }
        //</editor-fold>
        //</editor-fold>
        return mess;
    }

    public List<RequestUpdateLocation> findWaiting(Session cmPosSession, String province, Date date, Long duration, String createUser, String custName) {
        List<RequestUpdateLocation> result = supplier.findWaiting(cmPosSession, province, date, duration, createUser, custName);
        return result;
    }

    public String rejectWaitingOver30days(Session cmPosSession, Long cusId, String staffCode, String shopCode, String locale) throws Exception {
        String mess = null;
        LogUtils.info(logger, "RequestUpdateLocationBusiness.rejectWaitingOver30days:cusId=" + cusId + ";staffCode="
                + staffCode + ";shopCode=" + shopCode + ";locale=" + locale);
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="cusId">
        if (cusId == null || cusId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "cusId");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="RequestUpdateLocation">
        RequestUpdateLocation rul = findWaitingOver30days(cmPosSession, cusId);
        if (rul == null) {
            mess = LabelUtil.getKey("not.found.data", locale);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="update database CM">
        Date nowDate = getSysDateTime(cmPosSession);
        //<editor-fold defaultstate="collapsed" desc="luu log action">
        ActionAuditBussiness auditDAO = new ActionAuditBussiness();
        StringBuilder des = new StringBuilder()
                .append("Reject request update customer location, Request_update_customer_location_id=").append(rul.getRulId())
                .append(", cusId=").append(rul.getCusId());
        LogUtils.info(logger, "RequestUpdateLocationBusiness.rejectWaitingOver30days:start save ActionAudit");
        ActionAudit audit = auditDAO.insert(cmPosSession, nowDate, Constants.ACTION_REJECT_REQUEST_UPDATE_CUSTOMER_LOCATION, null, shopCode, staffCode, Constants.ACTION_AUDIT_PK_TYPE_CUSTOMER, rul.getRulId(), Constants.WS_IP, des.toString());
        LogUtils.info(logger, "RequestUpdateLocationBusiness.rejectWaitingOver30days:end save ActionAudit");
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="reject request update customer location">
        updateStatus(cmPosSession, rul, Constants.REJECT, staffCode, Constants.REJECT_OVER_30DAYS, nowDate, audit.getActionAuditId(), nowDate);
        //</editor-fold>
        //</editor-fold>

        return mess;
    }

    public Long countApprovedAndReject(Session cmPosSession, Long cusId) {
        //<editor-fold defaultstate="collapsed" desc="cusId">
        if (cusId == null || cusId <= 0L) {
            return 0L;
        }
        //</editor-fold>
        Long result = supplier.countApprovedAndReject(cmPosSession, cusId);
        return result;
    }

    public List<RequestUpdateLocation> findApprovedAndReject(Session cmPosSession, Long cusId, Integer start, Integer max) {
        //<editor-fold defaultstate="collapsed" desc="cusId">
        if (cusId == null || cusId <= 0L) {
            return null;
        }
        //</editor-fold>
        List<RequestUpdateLocation> result = supplier.findApprovedAndReject(cmPosSession, cusId, start, max);
        return result;
    }

    public List<NewImageInput> getImageRequestLocationCustomer(Session cmPosSession, Long requestId, String typeImage) {
        return supplier.getImageRequestLocationCustomer(cmPosSession, requestId, typeImage);
    }

    /**
     * @param account
     * @author: duyetdk
     * @since 06/6/2019
     * @des hien thi anh nha KH
     * @param cmPosSession
     * @param custId
     * @return
     * @throws Exception
     */
    public List<String> getImageLocationCustomer(Session cmPosSession, Long custId, String account) throws Exception {
        return supplier.getImageLocationCustomer(cmPosSession, custId, account);
    }

    /**
     * @author duyetdk
     * @des lay account khach hang
     * @param cmPosSession
     * @param custId
     * @return
     * @throws Exception
     */
    public List<String> getAccountByCustId(Session cmPosSession, Long custId) throws Exception {
        return supplier.getAccountByCustId(cmPosSession, custId);
    }

    /**
     * @author duyetdk
     * @since 05/07/2019
     * @des lay thue bao mobile
     * @param cmPosSession
     * @param custId
     * @return
     */
    public List<String> getMobileByCustId(Session cmPosSession, Long custId) throws Exception {
        return supplier.getMobileByCustId(cmPosSession, custId);
    }

    /**
     * @author duyetdk
     * @since 05/07/2019
     * @des lay thue bao homephone
     * @param cmPosSession
     * @param custId
     * @return
     */
    public List<String> getHomephoneByCustId(Session cmPosSession, Long custId) throws Exception {
        return supplier.getHomephoneByCustId(cmPosSession, custId);
    }

    public Long countPotentialCust(Session cmPosSession, String name, String phoneNumber,
            String address, Long status, String province, String kindOfCustomer) {
        return supplier.countPotentialCust(cmPosSession, name, phoneNumber, address, status, province, kindOfCustomer);
    }

    public List<PotentialCustomer> getListPotentialCust(Session cmPosSession, Integer start, Integer max,
            String name, String phoneNumber, String address, Long status, String province, String kindOfCustomer) {
        return supplier.getListPotentialCust(cmPosSession, start, max, name, phoneNumber, address, status, province, kindOfCustomer);
    }

    public void addNewPotentialCust(Session cmPosSession, String name, String phoneNumber,
            String address, String province, String email, String otherOperator, Double fee,
            String latitude, String longitude, String userLogin,
            String kindOfCustomer, String expectedService, String expectedScale, String serviceUsed) throws Exception {
        supplier.addNewPotentialCust(cmPosSession, name, phoneNumber, address, province, email,
                otherOperator, fee, latitude, longitude, userLogin,
                kindOfCustomer, expectedService, expectedScale, serviceUsed);
    }

    public void updatePotentialCust(Session cmPosSession, Long custId, String name, String phoneNumber,
            String address, String province, String email, String otherOperator, Double fee,
            String latitude, String longitude, Long status, String userLogin,
            String kindOfCustomer, String expectedService, String expectedScale, String serviceUsed) throws Exception {
        supplier.updatePotentialCust(cmPosSession, custId, name, phoneNumber, address, province, email,
                otherOperator, fee, latitude, longitude, status, userLogin,
                kindOfCustomer, expectedService, expectedScale, serviceUsed);
    }

    public void deletePotentialCust(Session cmPosSession, Long custId, String userLogin) throws Exception {
        supplier.deletePotentialCust(cmPosSession, custId, userLogin);
    }

    public PotentialCustomer findPotentialCustByPhoneNumber(Session cmPosSession, String phoneNumber) {
        return supplier.findPotentialCustByPhoneNumber(cmPosSession, phoneNumber);
    }

    //phuonghc 15062020
    public List<PotentialCustomerHistoryDto> getListHistoryByCustId(Session session, String custId) {
        return supplier.getListPotentialCustHistory(session, custId);
    }
}
