/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.supplier.SaBaySupplier;
import org.apache.log4j.Logger;
import org.hibernate.Session;
/**
 *
 * @author quangdm
 */
public class SabayBussiness extends BaseBussiness {
    protected SaBaySupplier supplier = new SaBaySupplier();

    public SabayBussiness() {
        logger = Logger.getLogger(BankBussiness.class);
    }

    public String insert(Session cmPosSession, String OTP, String msisdn) {
        String result = supplier.insert(cmPosSession, OTP, msisdn);
        return result;
    }
     public Boolean checkOTP(Session cmPosSession, String OTP, String msisdn) {
        return supplier.checkOTP(cmPosSession, OTP, msisdn);
    }
}
