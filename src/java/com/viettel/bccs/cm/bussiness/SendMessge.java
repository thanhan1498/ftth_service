/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness;

import com.viettel.brcd.dao.pre.IsdnSendSmsDAO;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class SendMessge {

    public static void send(Session cmPreSession, String isdn, List<String> lstDynamic, Date nowDate, String staffCode, String shopCode, String typeMessage, String codeMessage) {
        if (isdn != null) {
            if (isdn.startsWith("0")) {
                isdn = isdn.replaceFirst("0", "");
            }
            if (isdn.startsWith("855")) {
                isdn = isdn.replaceFirst("855", "");
            }
        }
        IsdnSendSmsDAO isdnDao = new IsdnSendSmsDAO();
        List<String> listContentSms = isdnDao.genarateSMSContent(cmPreSession, null, typeMessage, codeMessage, lstDynamic);
        if (listContentSms != null & !listContentSms.isEmpty()) {
            for (String temp : listContentSms) {
                isdnDao.insert(cmPreSession, isdn, temp, nowDate, staffCode, shopCode, codeMessage);
            }
        }
    }
}
