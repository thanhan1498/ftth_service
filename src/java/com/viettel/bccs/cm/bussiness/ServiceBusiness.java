/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.supplier.ServiceSupplier;
import com.viettel.brcd.common.util.PerformanceLogger;
import com.viettel.brcd.ws.model.output.WSRespone;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class ServiceBusiness extends BaseBussiness {

    public ServiceBusiness() {
        logger = Logger.getLogger(PerformanceLogger.class);
    }

    public WSRespone configConnectImt(Session cmPosSession, Session nimSession, Session cmPre, Long staffId, Long taskManagementId, String locale, String splitter) {
        return new ServiceSupplier().configConnectImt(cmPosSession, nimSession, cmPre, staffId, taskManagementId, locale, splitter);
    }

    public WSRespone sendNimsSubActiveInfo(Session cmPosSession, Session nimSession, Long staffId, Long taskManagementId) {
        return new ServiceSupplier().sendNimsSubActiveInfo(cmPosSession, nimSession, staffId, taskManagementId);
    }

    public WSRespone changeStationInfraStructure(Session cmPosSession, Session nimSession, Long staffId, String newStation, String connector, String portNo, String account) throws Exception {
        return new ServiceSupplier().changeStationInfraStructure(cmPosSession, nimSession, staffId, newStation, connector, portNo, account);
    }
}
