package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.supplier.ShopSupplier;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

public class ShopBussiness extends BaseBussiness {

    protected ShopSupplier supplier = new ShopSupplier();

    public ShopBussiness() {
        logger = Logger.getLogger(ShopBussiness.class);
    }

    public Shop findById(Session cmPosSession, Long shopId) {
        Shop result = null;
        if (shopId == null || shopId <= 0L) {
            return result;
        }
        List<Shop> objs = supplier.findById(cmPosSession, shopId, Constants.STATUS_USE);
        result = (Shop) getBO(objs);
        return result;
    }

    public Shop findByShopCode(Session cmPosSession, String shopCode) {
        Shop result = null;
        if (shopCode == null || shopCode.isEmpty()) {
            return result;
        }
        List<Shop> objs = supplier.findByShopCode(cmPosSession, shopCode, Constants.STATUS_USE);
        result = (Shop) getBO(objs);
        return result;
    }
}
