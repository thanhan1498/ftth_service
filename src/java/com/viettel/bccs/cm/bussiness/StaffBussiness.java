package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.supplier.StaffSupplier;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

public class StaffBussiness extends BaseBussiness {

    protected StaffSupplier supplier = new StaffSupplier();

    public StaffBussiness() {
        logger = Logger.getLogger(StaffBussiness.class);
    }

    public Staff findById(Session cmPosSession, Long staffId) {
        Staff result = null;
        if (staffId == null || staffId <= 0L) {
            return result;
        }
        List<Staff> objs = supplier.findById(cmPosSession, staffId, Constants.STATUS_USE);
        result = (Staff) getBO(objs);
        return result;
    }
    
    public Staff findByIdIm(Session imSession, Long staffId) {
        Staff result = null;
        if (staffId == null || staffId <= 0L) {
            return result;
        }
        List<Staff> objs = supplier.findByIdIm(imSession, staffId, Constants.STATUS_USE);
        result = (Staff) getBO(objs);
        return result;
    }

    public Staff findByCode(Session cmPosSession, String staffCode) {
        Staff result = null;
        if (staffCode == null) {
            return result;
        }
        staffCode = staffCode.trim();
        if (staffCode.isEmpty()) {
            return result;
        }
        List<Staff> objs = supplier.findByCode(cmPosSession, staffCode, Constants.STATUS_USE);
        result = (Staff) getBO(objs);
        return result;
    }
    
}
