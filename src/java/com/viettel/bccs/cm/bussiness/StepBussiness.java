package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.Step;
import com.viettel.bccs.cm.model.StepInfo;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubReqAdslLl;
import com.viettel.bccs.cm.supplier.StepSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.util.LabelUtil;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

public class StepBussiness extends BaseBussiness {

    protected StepSupplier supplier = new StepSupplier();

    public StepBussiness() {
        logger = Logger.getLogger(StepBussiness.class);
    }

    public Step findById(Session cmPosSession, Long stepId) {
        Step result = null;
        if (stepId == null || stepId <= 0L) {
            return result;
        }
        List<Step> objs = supplier.findById(cmPosSession, stepId, Constants.STATUS_USE);
        result = (Step) getBO(objs);
        return result;
    }

    public StepInfo getNextStepInfo(Session cmPosSession, Long subReqId, String locale) {
        StepInfo result;
        if (subReqId == null || subReqId <= 0L) {
            result = new StepInfo(LabelUtil.getKey("validate.required", locale, "subReqId"));
            return result;
        }
        SubReqAdslLl subReq = new SubReqAdslLlBussiness().findById(cmPosSession, subReqId);
        if (subReq == null) {
            return new StepInfo(LabelUtil.getKey("validate.not.exists", locale, "subReq"));
        }
        if (!Constants.IS_SMART.equals(subReq.getIsSmart())) {
            return new StepInfo(LabelUtil.getKey("input.invalid.isNotIsSmart", locale));
        }
        result = getNextStepInfo(cmPosSession, subReq);
        return result;
    }

    public StepInfo getNextStepInfo(Session cmPosSession, SubReqAdslLl subReq) {
        StepInfo result = null;
        if (subReq == null) {
            return result;
        }
        Boolean cancelSubReq = false;
        Boolean signContract = false;
        Boolean cancelContract = false;
        Boolean activeSubscriber = false;
        Long contractId = null;
        if (Constants.SUB_REQ_STATUS_NEW.equals(subReq.getStatus())) {
            cancelSubReq = true;
            signContract = true;
            result = new StepInfo(cancelSubReq, signContract, cancelContract, activeSubscriber, contractId);
            return result;
        }
        if (Constants.SUB_REQ_STATUS_SIGN_CONTRACT.equals(subReq.getStatus())) {
            cancelContract = true;
            activeSubscriber = true;
            SubAdslLeaseline sub = new SubAdslLeaselineBussines().findById(cmPosSession, subReq.getSubId());
            if (sub != null) {
                contractId = sub.getContractId();
            }
            result = new StepInfo(cancelSubReq, signContract, cancelContract, activeSubscriber, contractId);
            return result;
        }
        result = new StepInfo(cancelSubReq, signContract, cancelContract, activeSubscriber, contractId);
        return result;
    }
}
