package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.supplier.SubAdslLeaselineSupplier;
import com.viettel.brcd.ws.model.output.CoordinateSubDrawCable;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

public class SubAdslLeaselineBussines extends BaseBussiness {

    protected SubAdslLeaselineSupplier supplier = new SubAdslLeaselineSupplier();

    public SubAdslLeaselineBussines() {
        logger = Logger.getLogger(SubAdslLeaselineBussines.class);
    }

    public SubAdslLeaseline findById(Session cmPosSession, Long subId) {
        SubAdslLeaseline result = null;
        if (subId == null || subId <= 0L) {
            return result;
        }
        List<SubAdslLeaseline> objs = supplier.findById(cmPosSession, subId);
        result = (SubAdslLeaseline) getBO(objs);
        return result;
    }
    
    
    /**
     * @author : duyetdk
     * @des: findCoordinateBySubId
     * @since 15-03-2018
     */
     public List<CoordinateSubDrawCable> findCoordinateBySubId(Session cmPosSession, String account){
          //<editor-fold defaultstate="collapsed" desc="subId">
        if (account == null || account.trim().isEmpty()) {
            return null;
        }
        //</editor-fold>
        List<CoordinateSubDrawCable> result = supplier.findCoordinateBySubId(cmPosSession, account);
        return result;
     }
     
     /**
      * @author : duyetdk
      * @des: findCoordinateBySubId
      * @since 16-05-2019
      * @param cmPosSession
      * @param isdn
      * @return 
      */
     public SubAdslLeaseline findByIsdn(Session cmPosSession, String isdn) {
        SubAdslLeaseline result = null;
        if (isdn == null || isdn.trim().isEmpty()) {
            return result;
        }
        List<SubAdslLeaseline> objs = supplier.findByIsdn(cmPosSession, isdn);
        result = (SubAdslLeaseline) getBO(objs);
        return result;
    }
}
