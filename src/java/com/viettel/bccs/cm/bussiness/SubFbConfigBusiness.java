/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness;

import com.google.gson.Gson;
import com.viettel.bccs.api.Task.DAO.CustomerDAO;
import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.cm.dao.NotificationMessageDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.dao.TaskManagementDAO;
import com.viettel.bccs.cm.dao.TaskShopManagementDAO;
import com.viettel.bccs.cm.dao.TaskStaffManagementDAO;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.StaffDetail;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubFbConfig;
import com.viettel.bccs.cm.model.SubFbConfigDetail;
import com.viettel.bccs.cm.model.SubFbConfigLog;
import com.viettel.bccs.cm.model.TaskShopManagement;
import com.viettel.bccs.cm.model.TaskStaffManagement;
import com.viettel.bccs.cm.supplier.SubFbConfigSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.common.util.FileSaveProcess;
import com.viettel.brcd.dao.pre.IsdnSendSmsDAO;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.NewImageInput;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author duyetdk
 */
public class SubFbConfigBusiness extends BaseBussiness {

    SubFbConfigSupplier supplier = new SubFbConfigSupplier();

    public SubFbConfigBusiness() {
        logger = Logger.getLogger(SubFbConfigBusiness.class);
    }

    public SubFbConfig findByTaskMngtId(Session cmPosSession, Long taskManagementId, String typeConfig) {
        SubFbConfig result = null;
        if (taskManagementId == null || taskManagementId <= 0L) {
            return result;
        }
        List<SubFbConfig> objs = supplier.find(cmPosSession, null, taskManagementId, null, null, null, null, typeConfig);
        result = (SubFbConfig) getBO(objs);
        return result;
    }

    public SubFbConfig findBySubId(Session cmPosSession, Long subId, Long status, String typeConfig) {
        SubFbConfig result = null;
        if (subId == null || subId <= 0L) {
            return result;
        }
        List<SubFbConfig> objs = supplier.find(cmPosSession, null, null, status, null, null, subId, typeConfig == null ? Constants.TYPE_CONF_IMT : typeConfig);
        result = (SubFbConfig) getBO(objs);
        return result;
    }

    public SubFbConfig findById(Session cmPosSession, Long id, String typeConfig) {
        SubFbConfig result = null;
        if (id == null || id <= 0L) {
            return result;
        }
        List<SubFbConfig> objs = supplier.find(cmPosSession, id, null, null, null, null, null, typeConfig);
        result = (SubFbConfig) getBO(objs);
        return result;
    }

    public List<SubFbConfig> findByStatus(Session cmPosSession, Long status, Long statusConfig, String typeConfig) {
        List<SubFbConfig> result = supplier.find(cmPosSession, null, null, status, null, statusConfig, null, typeConfig);
        return result;
    }

    public void updateStatus(Session cmPosSession, SubFbConfig sub, Long status, Long statusConfig, String staffCode, String reasonCode, String description, Date lastUpdateDate) {
        //Object oldValue = sub.getStatus();
        sub.setStatus(status);
        sub.setStatusConf(statusConfig);
        sub.setDescription(description);
        sub.setReasonCode(reasonCode);
        sub.setLastUpdateUser(staffCode.trim().toUpperCase());
        sub.setLastUpdateDate(lastUpdateDate);
        //Object newValue = sub.getStatus();

        //new ActionDetailBussiness().insert(cmPosSession, actionAuditId, ReflectUtils.getTableName(SubFbConfig.class), sub.getId(), ReflectUtils.getColumnName(SubFbConfig.class, "status"), oldValue, newValue, nowDate);
        cmPosSession.update(sub);
        cmPosSession.flush();
    }

    public Long count(Session cmPosSession, Long status, Long statusConfig, Long duration, String createUser, String account, String province) {
        Long result = supplier.count(cmPosSession, status, statusConfig, duration, createUser, account, province);
        return result;
    }

    public List<SubFbConfigDetail> findAll(Session cmPosSession, Long status, Long statusConfig, Long duration, Integer start, Integer max,
            String createUser, String account, String province) {
        List<SubFbConfigDetail> result = supplier.findAll(cmPosSession, status, statusConfig, duration, start, max, createUser, account, province);
        return result;
    }

    public String rejectOrApproveSubFbConfig(Session cmPosSession, Session cmPre, Long type, Long id, String staffCode, String reasonCode, String description, String locale, StringBuilder request, SubFbConfig subFbConfig, StringBuilder action) throws Exception {
        String mess = null;
        LogUtils.info(logger, "SubFbConfigBusiness.rejectOrApproveSubFbConfig:id=" + id + ";staffCode="
                + staffCode + ";reasonCode=" + reasonCode + ";description=" + description + ";locale=" + locale);
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="subFbCofigId">
        if (id == null || id <= 0L) {
            mess = LabelUtil.getKey("validate.required.subFbCofigId", locale);
            return mess;
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="SubFbConfig">
        SubFbConfig sub = findById(cmPosSession, id, null);
        subFbConfig = sub;
        if (sub == null) {
            mess = LabelUtil.getKey("validate.not.exists.sub", locale);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="update database CM">
        Date nowDate = getSysDateTime(cmPosSession);

        if (Constants.REJECT.equals(type) && (Constants.WAITING.equals(sub.getStatus()) || Constants.APPROVED.equals(sub.getStatus()))) {
            //<editor-fold defaultstate="collapsed" desc="insert subFbConfigLog">
            action.append("REJECT_REQUEST_CONF");
            request.append("{id: ").append(id).append(", type: ").append(type).append(", reasonCode: ").append(reasonCode).append(", description: ").append(description).append("}");
            //</editor-fold>
            updateStatus(cmPosSession, sub, Constants.REJECT, null, staffCode, reasonCode, description, nowDate);

            /*send message to annouce for staff*/
            List<TaskShopManagement> taskShop = new TaskShopManagementDAO().findByProperty(cmPosSession, "taskMngtId", sub.getTaskManagementId());
            List<TaskStaffManagement> taskStaff = new TaskStaffManagementDAO().findByProperty(cmPosSession, "taskShopMngtId", taskShop.get(0).getTaskShopMngtId());
            Staff staffIsdn = new StaffDAO().findById(cmPosSession, taskStaff.get(0).getStaffId());
            if (staffIsdn != null && staffIsdn.getTel() != null && staffIsdn.getIsdn() != null) {
                Staff staff = new StaffDAO().findByCode(cmPosSession, staffCode);
                List<String> list = new ArrayList<String>();
                list.add(sub.getAccount());
                list.add(staffCode);
                list.add(staff.getTel() == null ? "NA" : staff.getTel());
                list.add(description);
                com.viettel.bccs.cm.bussiness.SendMessge.send(cmPre, staffIsdn.getTel(), list, nowDate, staffCode, null, Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.CONFIG_IMT_REJECT);
            }

            /*push notification*/
            List<String> listStr = Arrays.asList(sub.getAccount());
            List<String> listContentSms = new IsdnSendSmsDAO().genarateSMSContent(cmPre, null, Constants.AP_PARAM_PARAM_TYPE_NOTIFICATION, "UPDATE_TASK", listStr);
            try {
                if (listContentSms != null && !listContentSms.isEmpty()) {
                    String tempNotification = "";
                    for (String temp : listContentSms) {
                        tempNotification += Constants.NEW_LINE + temp;
                    }
                    if (!tempNotification.isEmpty()) {
                        StringBuilder stringBui = new StringBuilder(tempNotification);
                        stringBui.replace(tempNotification.lastIndexOf(Constants.NEW_LINE), tempNotification.lastIndexOf(Constants.NEW_LINE) + Constants.NEW_LINE.length(), "");
                        tempNotification = stringBui.toString();
                        new NotificationMessageDAO().generateNotificationData(
                                cmPosSession,
                                staffIsdn.getStaffId(),
                                staffIsdn.getStaffCode(),
                                staffCode,
                                LabelUtil.getKey("Notification.UpdateTask.title", locale),
                                tempNotification,/*message*/
                                sub.getAccount(),/*account*/
                                sub.getTaskManagementId(),
                                taskStaff.get(0).getTaskStaffMngtId(),/*taskStaffMngtId*/
                                Constants.NOTIFICATION_ACTION_TYPE_UPDATE_TASK/*actionType*/);
                    }
                }
            } catch (Exception ex) {
                logger.info("notification: " + ex.getMessage());
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Approve request sub fixbroadband config">
        if (Constants.APPROVED.equals(type) && Constants.WAITING.equals(sub.getStatus())) {
            request.append("{id: ").append(id).append(", type: ").append(type).append("}");
            action.append("APPROVE_REQUEST_CONF");
            if (Constants.TYPE_CONF_DIGITAL.equals(sub.getTypeConfig())) {
                updateStatus(cmPosSession, sub, Constants.APPROVED, Constants.APPROVED, staffCode, null, Constants.APPROVAL, nowDate);
            } else {
                updateStatus(cmPosSession, sub, Constants.APPROVED, Constants.WAITING, staffCode, null, Constants.APPROVAL, nowDate);

                /*update thong tin toa do ha tang va vi tri lap dat tu sub_fb_conf vao sub_adsl_ll*/
                SubAdslLeaseline subAdslLl = new SubAdslLeaselineBussines().findById(cmPosSession, sub.getSubId());

                subAdslLl.setCustLat(sub.getCustLat() == null ? null : Double.parseDouble(sub.getCustLat()));
                subAdslLl.setCustLong(sub.getCustLong() == null ? null : Double.parseDouble(sub.getCustLong()));

                subAdslLl.setInfraLat(sub.getInfraLat() == null ? null : Double.parseDouble(sub.getInfraLat()));
                subAdslLl.setInfraLong(sub.getInfraLong() == null ? null : Double.parseDouble(sub.getInfraLong()));

                cmPosSession.update(subAdslLl);
            }

        }
        //</editor-fold>

        return mess;
    }

    public SubFbConfig findByTaskMngtIdAndAccount(Session cmPosSession, Long taskManagementId, String account, String typeConfig) {
        SubFbConfig result = null;
        if (taskManagementId == null || taskManagementId <= 0L) {
            return result;
        }
        List<SubFbConfig> objs = supplier.find(cmPosSession, null, taskManagementId, null, account, null, null, typeConfig);
        result = (SubFbConfig) getBO(objs);
        return result;
    }

    public String sendRequestSubFbConfig(Session cmPosSession, Long taskManagementId, Long subId, String account,
            List<NewImageInput> lstImageInfra, List<NewImageInput> lstImageGood, String createUser,
            String locale, String tab, String province, StringBuilder request, SubFbConfig subFbConfig) throws Exception {
        String mess = null;
        Date nowDate = getSysDateTime(cmPosSession);
        int max = Integer.parseInt(ResourceUtils.getResource("MAX_SIZE"));
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="taskManagementId">
        if (!"3".equals(tab) && !"4".equals(tab)) {
            mess = LabelUtil.getKey("validate.required.images", locale);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="taskManagementId">
        if (taskManagementId == null || taskManagementId <= 0L) {
            mess = LabelUtil.getKey("validate.required.taskManagementId", locale);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="subId">
        if (subId == null || subId <= 0L) {
            mess = LabelUtil.getKey("validate.required.subId", locale);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="account">
        if (account == null) {
            mess = LabelUtil.getKey("validate.required.account", locale);
            return mess;
        }
        account = account.trim();
        if (account.isEmpty()) {
            mess = LabelUtil.getKey("validate.required.account", locale);
            return mess;
        }
        Integer length = ReflectUtils.getColumnLength(SubFbConfig.class, "account");
        if (length != null && length > 0 && account.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("account", locale), length);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="lstImageInfra">
        com.viettel.bccs.cm.model.TaskManagement task = new TaskManagementDAO().findById(cmPosSession, taskManagementId);
        if ("3".equals(tab)) {
            if (lstImageInfra == null) {
                mess = LabelUtil.getKey("validate.required.lstImageInfra", locale);
                return mess;
            }
            //<editor-fold defaultstate="collapsed" desc="infraLat">
            String infraLat = lstImageInfra.get(lstImageInfra.size() - 1).getLat();
            String infraLong = lstImageInfra.get(lstImageInfra.size() - 1).getLon();
            if (infraLat == null) {
                mess = LabelUtil.getKey("validate.required.infraLat", locale);
                return mess;
            }
            infraLat = infraLat.trim();
            if (infraLat.isEmpty()) {
                mess = LabelUtil.getKey("validate.required.infraLat", locale);
                return mess;
            }
            length = ReflectUtils.getColumnLength(SubFbConfig.class, "infraLat");
            if (length != null && length > 0 && infraLat.length() > length) {
                mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("infraLat", locale), length);
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="infraLong">
            if (infraLong == null) {
                mess = LabelUtil.getKey("validate.required.infraLong", locale);
                return mess;
            }
            infraLong = infraLong.trim();
            if (infraLong.isEmpty()) {
                mess = LabelUtil.getKey("validate.required.infraLong", locale);
                return mess;
            }
            length = ReflectUtils.getColumnLength(SubFbConfig.class, "infraLong");
            if (length != null && length > 0 && infraLong.length() > length) {
                mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("infraLong", locale), length);
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="upload image Infra">
            Long idImageInfra = null;
            if (lstImageInfra.size() > 0) {

                idImageInfra = Common.getSequence("CUST_IMAGE_SEQ", cmPosSession);
                DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                int count = 0;
                for (NewImageInput imageInput : lstImageInfra) {
                    String imageDate = dateFormat.format(new Date());
                    String imageName = imageDate + "_" + account + "_" + count + "_" + idImageInfra + ".jpg";
                    boolean checkUpload = new FileSaveProcess().saveFileNotFtp(imageName, imageInput.getImage(), Constants.INFRA_IMG + "_" + account, logger, Constants.FTP_DIR_TASK);
                    if (!checkUpload) {
                        logger.info("\n--------------\nUpload image Faile\n--------------\n");
                        continue;
                    }

                    int checkInsertDB = supplier.insertImageDetail(cmPosSession, idImageInfra, imageName, imageInput.getImageType(), null, createUser, infraLat, infraLong);
                    if (checkInsertDB == 0) {
                        logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                    }
                    count++;
                }
                int checkInsertDB = new CustomerDAO().insertCusImage(cmPosSession, taskManagementId, account, "", Constants.INFRA_IMG, createUser, idImageInfra);
                if (checkInsertDB == 0) {
                    logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                }

            }
            //</editor-fold>
            subFbConfig = supplier.insert(cmPosSession, taskManagementId, subId, account, idImageInfra,
                    infraLat, infraLong, null, null, null, nowDate, createUser, province, Constants.TYPE_CONF_IMT, Constants.PENDING, null);
            if (task != null && task.getPathFileAttach() != null) {
                supplier.insert(cmPosSession, taskManagementId, subId, task.getPathFileAttach(), idImageInfra,
                        infraLat, infraLong, null, null, null, nowDate, createUser, province, Constants.TYPE_CONF_TV, Constants.WAITING, null);
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="lstImageGood">
        if ("4".equals(tab)) {
            if (lstImageGood == null || lstImageGood.size() < 0) {
                mess = LabelUtil.getKey("validate.required.lstImageGood", locale);
                return mess;
            }
            /*int goodSize = Double.compare(lstImageGood.size(), max);
             if (goodSize != 0) {
             mess = LabelUtil.getKey("validate.max.size.image", locale);
             return mess;
             }*/

            //<editor-fold defaultstate="collapsed" desc="custLat">
            String custLat = lstImageGood.get(lstImageGood.size() - 1).getLat();
            String custLong = lstImageGood.get(lstImageGood.size() - 1).getLon();
            if (custLat == null) {
                mess = LabelUtil.getKey("validate.required.custLat", locale);
                return mess;
            }
            custLat = custLat.trim();
            if (custLat.isEmpty()) {
                mess = LabelUtil.getKey("validate.required.custLat", locale);
                return mess;
            }
            length = ReflectUtils.getColumnLength(SubFbConfig.class, "custLat");
            if (length != null && length > 0 && custLat.length() > length) {
                mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("custLat", locale), length);
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="custLong">
            if (custLong == null) {
                mess = LabelUtil.getKey("validate.required.custLong", locale);
                return mess;
            }
            custLong = custLong.trim();
            if (custLong.isEmpty()) {
                mess = LabelUtil.getKey("validate.required.custLong", locale);
                return mess;
            }
            length = ReflectUtils.getColumnLength(SubFbConfig.class, "custLong");
            if (length != null && length > 0 && custLong.length() > length) {
                mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("custLong", locale), length);
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="upload image Good">
            Long idImageGood = null;
            if (lstImageGood.size() > 0) {

                idImageGood = Common.getSequence("CUST_IMAGE_SEQ", cmPosSession);
                DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                int count = 0;
                for (NewImageInput imageInput : lstImageGood) {
                    String imageDate = dateFormat.format(new Date());
                    String imageName = imageDate + "_" + account + "_" + count + "_" + idImageGood + ".jpg";
                    boolean checkUpload = new FileSaveProcess().saveFileNotFtp(imageName, imageInput.getImage(), Constants.GOOD_IMG + "_" + account, logger, Constants.FTP_DIR_TASK);
                    if (!checkUpload) {
                        logger.info("\n--------------\nUpload image Faile\n--------------\n");
                        continue;
                    }

                    int checkInsertDB = supplier.insertImageDetail(cmPosSession, idImageGood, imageName, imageInput.getImageType(), null, createUser, custLat, custLong);
                    if (checkInsertDB == 0) {
                        logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                    }
                    count++;
                }
                int checkInsertDB = new CustomerDAO().insertCusImage(cmPosSession, taskManagementId, account, "", Constants.GOOD_IMG, createUser, idImageGood);
                if (checkInsertDB == 0) {
                    logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                }

            }
            //</editor-fold>
            subFbConfig = supplier.insert(cmPosSession, taskManagementId, subId, account, null,
                    null, null, idImageGood, custLat, custLong, nowDate, createUser, province, Constants.TYPE_CONF_IMT, Constants.PENDING, null);
            if (task != null && task.getPathFileAttach() != null) {
                supplier.insert(cmPosSession, taskManagementId, subId, task.getPathFileAttach(), null,
                        null, null, idImageGood, custLat, custLong, nowDate, createUser, province, Constants.TYPE_CONF_TV, Constants.WAITING, null);
            }
        }
        //</editor-fold>
        //</editor-fold>F
        request.append(new Gson().toJson(subFbConfig));
        return mess;
    }

    public String updateRequestSubFbConfig(Session cmPosSession, Long taskManagementId, Long subId, String account,
            List<NewImageInput> lstImageInfra, List<NewImageInput> lstImageGood, String createUser, String locale,
            StringBuilder request, SubFbConfig subFbConfig, String typeConfig) throws Exception {
        String mess = null;
        Date nowDate = getSysDateTime(cmPosSession);
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="taskManagementId">
        if (taskManagementId == null || taskManagementId <= 0L) {
            mess = LabelUtil.getKey("validate.required.taskManagementId", locale);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="SubFbConfig">
        subFbConfig = findByTaskMngtId(cmPosSession, taskManagementId, typeConfig);

        if (subFbConfig == null) {
            mess = LabelUtil.getKey("validate.not.exists.subFbConfig", locale);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="subId">
        if (subId == null || subId <= 0L) {
            mess = LabelUtil.getKey("validate.required.subId", locale);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="account">
        if (account == null) {
            mess = LabelUtil.getKey("validate.required.account", locale);
            return mess;
        }
        account = account.trim();
        if (account.isEmpty()) {
            mess = LabelUtil.getKey("validate.required.account", locale);
            return mess;
        }
        Integer length = ReflectUtils.getColumnLength(SubFbConfig.class, "account");
        if (length != null && length > 0 && account.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("account", locale), length);
            return mess;
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="infraLatLong">
        String infraLat = null;
        String infraLong = null;
        if (lstImageInfra != null) {
            infraLat = lstImageInfra.get(lstImageInfra.size() - 1).getLat();
            infraLong = lstImageInfra.get(lstImageInfra.size() - 1).getLon();
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="custLatLong">
        String custLat = null;
        String custLong = null;
        if (lstImageGood != null) {
            custLat = lstImageGood.get(lstImageGood.size() - 1).getLat();
            custLong = lstImageGood.get(lstImageGood.size() - 1).getLon();
        }
        //</editor-fold>
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="upload image Infra">
        Long idImageInfra = null;
        CustomerDAO cusDAO = new CustomerDAO();
        if (lstImageInfra != null && lstImageInfra.size() > 0) {

            idImageInfra = Common.getSequence("CUST_IMAGE_SEQ", cmPosSession);
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            int count = 0;
            for (NewImageInput imageInput : lstImageInfra) {
                String imageDate = dateFormat.format(new Date());
                String imageName = imageDate + "_" + account + "_" + count + "_" + idImageInfra + ".jpg";
                boolean checkUpload = new FileSaveProcess().saveFileNotFtp(imageName, imageInput.getImage(), Constants.INFRA_IMG + "_" + account, logger, Constants.FTP_DIR_TASK);
                if (!checkUpload) {
                    logger.info("\n--------------\nUpload image Faile\n--------------\n");
                    continue;
                }

                int checkInsertDB = supplier.insertImageDetail(cmPosSession, idImageInfra, imageName, imageInput.getImageType(), null, createUser, infraLat, infraLong);
                if (checkInsertDB == 0) {
                    logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                }
                count++;
            }
            if (subFbConfig.getIfraImgId() != null && subFbConfig.getIfraImgId() > 0L) {
                supplier.updateImage(cmPosSession, subFbConfig.getCustImgId());
            }
            int checkInsertDB = cusDAO.insertCusImage(cmPosSession, taskManagementId, account, "", Constants.INFRA_IMG, createUser, idImageInfra);
            if (checkInsertDB == 0) {
                logger.info("\n--------------\nInsert DB fail image\n--------------\n");
            }

        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="upload image Good">
        Long idImageGood = null;
        if (lstImageGood != null && lstImageGood.size() > 0) {

            idImageGood = Common.getSequence("CUST_IMAGE_SEQ", cmPosSession);
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            int count = 0;
            for (NewImageInput imageInput : lstImageGood) {
                String imageDate = dateFormat.format(new Date());
                String imageName = imageDate + "_" + account + "_" + count + "_" + idImageGood + ".jpg";
                boolean checkUpload = new FileSaveProcess().saveFileNotFtp(imageName, imageInput.getImage(), Constants.GOOD_IMG + "_" + account, logger, Constants.FTP_DIR_TASK);
                if (!checkUpload) {
                    logger.info("\n--------------\nUpload image Faile\n--------------\n");
                    continue;
                }

                int checkInsertDB = supplier.insertImageDetail(cmPosSession, idImageGood, imageName, imageInput.getImageType(), null, createUser, custLat, custLong);
                if (checkInsertDB == 0) {
                    logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                }
                count++;
            }
            if (subFbConfig.getCustImgId() != null && subFbConfig.getCustImgId() > 0L) {
                supplier.updateImage(cmPosSession, subFbConfig.getCustImgId());
            }
            int checkInsertDB = cusDAO.insertCusImage(cmPosSession, taskManagementId, account, "", Constants.GOOD_IMG, createUser, idImageGood);
            if (checkInsertDB == 0) {
                logger.info("\n--------------\nInsert DB fail image\n--------------\n");
            }

        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="insert subFbConfigLog">
        request.append("{Infra_image_id: ").append(idImageInfra)
                .append(", Infra_lat: ").append(infraLat)
                .append(", Infra_long: ").append(infraLong)
                .append(", Cust_image_id: ").append(idImageGood)
                .append(", Cust_lat: ").append(custLat)
                .append("; Cust_long: ").append(custLong).append("}");
        //</editor-fold>
        supplier.update(cmPosSession, subFbConfig, subId, account, idImageInfra,
                infraLat, infraLong, idImageGood, custLat, custLong, nowDate, createUser);
        return mess;
    }

    public String requestConfig(Session cmPosSession, Long subId, String userUpdate, String locale, StringBuilder request, SubFbConfig sub) {
        String mess = null;
        try {
            LogUtils.info(logger, "SubFbConfigBusiness.update:subId = " + subId + ";staffCode="
                    + userUpdate + ";locale=" + locale);
            //<editor-fold defaultstate="collapsed" desc="validate">
            //<editor-fold defaultstate="collapsed" desc="subFbCofigId">
            if (subId == null || subId <= 0L) {
                mess = LabelUtil.getKey("validate.required.subId", locale);
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="SubFbConfig">
            sub = findBySubId(cmPosSession, subId, null, Constants.TYPE_CONF_IMT);

            if (sub == null) {
                mess = LabelUtil.getKey("validate.not.exists.sub", locale);
                return mess;
            }
            if (sub.getIfraImgId() == null || sub.getIfraImgId() < 0L) {
                mess = LabelUtil.getKey("validate.required.lstImageInfra", locale);
                return mess;
            }
            if (sub.getCustImgId() == null || sub.getCustImgId() < 0L) {
                mess = LabelUtil.getKey("validate.required.lstImageGood", locale);
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="update database CM">
            Date nowDate = getSysDateTime(cmPosSession);
            //<editor-fold defaultstate="collapsed" desc="luu log action">
            LogUtils.info(logger, "SubFbConfigBusiness.requestConfig:start save ActionAudit");
            LogUtils.info(logger, "SubFbConfigBusiness.requestConfig:end save ActionAudit");
            //</editor-fold>
            //</editor-fold>
            //</editor-fold>
            if (Constants.PENDING.equals(sub.getStatus()) || Constants.REJECT.equals(sub.getStatus())) {
                //<editor-fold defaultstate="collapsed" desc="insert subFbConfigLog">
                request.append("{id:").append(sub.getId()).append(", status:").append(Constants.WAITING).append("}");
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="update status to waiting">
                updateStatus(cmPosSession, sub, Constants.WAITING, null, userUpdate, null, null, nowDate);
                //</editor-fold>
            } else {
                mess = LabelUtil.getKey("task.not.permit.request.config", locale);
                return mess;
            }
        } catch (Exception ex) {
            logger.info("SubFbConfigBusiness.requestConfig: " + ex.getMessage());
            mess = ex.getMessage();
        }
        return mess;
    }

    public List<SubFbConfigLog> findAllHistoryById(Session cmPosSession, Long subFbConfigId) {
        List<SubFbConfigLog> result = supplier.findAllHistoryById(cmPosSession, subFbConfigId);
        return result;
    }

    public StaffDetail findStaffByStaffCode(Session imSession, String staffCode) {
        StaffDetail result = null;
        if (staffCode == null || staffCode.trim().isEmpty()) {
            return result;
        }
        List<StaffDetail> objs = supplier.findStaff(imSession, staffCode, null);
        result = (StaffDetail) getBO(objs);
        return result;
    }
}
