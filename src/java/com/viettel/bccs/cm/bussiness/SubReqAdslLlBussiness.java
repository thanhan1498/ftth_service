package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.SubReqAdslLl;
import com.viettel.bccs.cm.supplier.SubReqAdslLlSupplier;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

public class SubReqAdslLlBussiness extends BaseBussiness {

    protected SubReqAdslLlSupplier supplier = new SubReqAdslLlSupplier();

    public SubReqAdslLlBussiness() {
        logger = Logger.getLogger(SubReqAdslLlBussiness.class);
    }

    public SubReqAdslLl findById(Session cmPosSession, Long id) {
        SubReqAdslLl result = null;
        if (id == null || id <= 0L) {
            return result;
        }
        List<SubReqAdslLl> objs = supplier.findById(cmPosSession, id);
        result = (SubReqAdslLl) getBO(objs);
        return result;
    }
}
