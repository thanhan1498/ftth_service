package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.SubType;
import com.viettel.bccs.cm.supplier.SubTypeSupplier;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubTypeBussiness extends BaseBussiness {

    protected SubTypeSupplier supplier = new SubTypeSupplier();

    public SubTypeBussiness() {
        logger = Logger.getLogger(SubTypeBussiness.class);
    }

    public SubType findById(Session cmPosSession, String subType) {
        SubType result = null;
        if (subType == null) {
            return result;
        }
        subType = subType.trim();
        if (subType.isEmpty()) {
            return result;
        }
        List<SubType> objs = supplier.findById(cmPosSession, subType, Constants.STATUS_USE);
        result = (SubType) getBO(objs);
        return result;
    }

    public List<SubType> findByMap(Session cmPosSession, String serviceType, String productCode) {
        List<SubType> result = null;
        if (serviceType == null || productCode == null) {
            return result;
        }
        serviceType = serviceType.trim();
        if (serviceType.isEmpty()) {
            return result;
        }
        productCode = productCode.trim();
        if (productCode.isEmpty()) {
            return result;
        }
        result = supplier.findByMap(cmPosSession, serviceType, productCode);
        return result;
    }
}
