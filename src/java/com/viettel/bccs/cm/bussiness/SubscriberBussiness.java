package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.api.Task.DAO.ManagerTaskDAO;
import com.viettel.bccs.api.cm.pre.CmPreCommon;
import com.viettel.bccs.api.cm.pre.PreChangeSimBusiness;
import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.api.sale.BaseCommon;
import com.viettel.bccs.bean.common.MessageError;
import com.viettel.bccs.bo.cm.pos.Subscriber;
import com.viettel.bccs.bo.payment.PaymentContract;
import com.viettel.bccs.bo.sale.AccountAgent;
import com.viettel.bccs.bo.sale.StockSim;
import com.viettel.bccs.cm.common.util.CommonConnecSub;
import com.viettel.bccs.cm.common.util.pre.Constant;
import com.viettel.bccs.cm.controller.RegisterFTTHOnlineController;
import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.bccs.cm.dao.ContractOfferDAO;
import com.viettel.bccs.cm.dao.CustomerDAO;
import com.viettel.bccs.cm.dao.DeployRequirementDAO;
import com.viettel.bccs.cm.dao.MappingDAO;
import com.viettel.bccs.cm.dao.OfferRequestDAO;
import com.viettel.bccs.cm.dao.OfferSubDAO;
import com.viettel.bccs.cm.dao.ReasonDAO;
import com.viettel.bccs.cm.dao.ReasonOfflineDAO;
import com.viettel.bccs.cm.dao.RegOnlineDAO;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.dao.SubAccountsAdslLlDAO;
import com.viettel.bccs.cm.dao.SubActivateAdslLlDAO;
import com.viettel.bccs.cm.dao.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.dao.SubDeploymentDAO;
import com.viettel.bccs.cm.dao.SubDepositAdslLlDAO;
import com.viettel.bccs.cm.dao.SubIpAdslLlDAO;
import com.viettel.bccs.cm.dao.SubProductAdslLlDAO;
import com.viettel.bccs.cm.dao.SubPromotionAdslLlDAO;
import com.viettel.bccs.cm.dao.SubReqAdslLlDAO;
import com.viettel.bccs.cm.dao.TaskManagementDAO;
import com.viettel.bccs.cm.model.ApDomain;
import com.viettel.brcd.common.util.Provisioning;
import com.viettel.brcd.dao.im.IMDAO;
import com.viettel.bccs.cm.model.ActionAudit;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.Contract;
import com.viettel.bccs.cm.model.CustRequest;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.OfferRequest;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.RegOnline;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubReqAdslLl;
import com.viettel.bccs.cm.model.pre.ReasonPre;
import com.viettel.bccs.cm.supplier.ApParamSupplier;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.bccs.cm.util.DateTimeUtils;
import com.viettel.bccs.supplier.cm.pos.PosChangeSimSupplier;
import com.viettel.bccs.supplier.cm.pre.PreChangeSimSupplier;
import com.viettel.bccs.supplier.payment.PaymentChangeSimSupplier;
import com.viettel.brcd.dao.pre.IsdnSendSmsDAO;
import com.viettel.brcd.ws.model.input.ChangeSimIn;
import com.viettel.brcd.ws.model.output.ReasonChangeSimOut;
import com.viettel.common.ViettelService;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubscriberBussiness extends BaseDAO {

    public SubscriberBussiness() {
        _log = Logger.getLogger(SubscriberBussiness.class);
    }

    /**
     * @author VangHV1
     * @param paymentSession
     * @param ccSession
     * @param nimsSession
     * @param custId
     * @param dateDeploy
     * @param reasonDelay
     * @param account
     * @throws java.lang.Exception
     * @description Ham dau noi thue bao dich vu A, F, L
     * @param cmPosSession
     * @param cmPreSession
     * @param imSession
     * @param pmSession
     * @param reqId
     * @param shopId
     * @param staffId
     * @param locale
     * @return
     */
    public String activeSubscriber(Session cmPosSession, Session cmPreSession, Session imSession, Session pmSession, Session paymentSession, Session ccSession, Session nimsSession, Long reqId,
            Long shopId, Long staffId, Long custId, String account, String locale, String dateDeploy, String reasonDelay) throws Exception {
        info(_log, "SubscriberDAO.activeSubscriber:reqId=" + reqId + ";shopId=" + shopId + ";staffId=" + staffId + ";locale=" + locale);
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="locale">
        mess = LabelUtil.validateLocale(locale);
        if (mess != null) {
            mess = mess.trim();
            if (!mess.isEmpty()) {
                return mess;
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="shopId">
        if (shopId == null || shopId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "shopId");
            return mess;
        }
        Shop shop = new ShopDAO().findById(cmPosSession, shopId);
        if (shop == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "shop");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="staffId">
        if (staffId == null || staffId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "staffId");
            return mess;
        }
        Staff staff = new StaffDAO().findById(cmPosSession, staffId);
        if (staff == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "staff");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="subReq">
        if (reqId == null || reqId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "subReq");
            return mess;
        }
        SubReqAdslLl subReq = new SubReqAdslLlDAO().findById(cmPosSession, reqId);
        if (subReq == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "subReq");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="reason">
        Long reasonId = subReq.getRegReasonId();
        if (reasonId == null || reasonId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "reason");
            return mess;
        }
        Reason reason = new ReasonDAO().findById(cmPosSession, reasonId);
        if (reason == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "reason");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="custRequest">
        Long custReqId = subReq.getCustReqId();
        info(_log, "SubscriberDAO.validateActiveSubscriber:custReqId=" + custReqId);
        CustRequest custRequest = new CustRequestBussiness().findById(cmPosSession, custReqId);
        if (custRequest == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "custRequest");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="offerRequest">
        Long offerReqId = subReq.getReqOfferId();
        info(_log, "SubscriberDAO.validateActiveSubscriber:offerReqId=" + offerReqId);
        if (offerReqId == null || offerReqId < 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "offerRequest");
            return mess;
        }
        List<OfferRequest> lstOfferRequest = null;
        if (Constants.SUB_REQ_IS_ACTIVE_ALL_OFFER_REQUEST.equals(offerReqId)) {// Neu la dau noi tat ca yeu cau cua KH
            lstOfferRequest = new OfferRequestDAO().findByCustReqId(cmPosSession, custReqId);
        } else {
            OfferRequest offerRequest = new OfferRequestDAO().findById(cmPosSession, offerReqId);
            if (offerRequest != null) {
                lstOfferRequest = new ArrayList<OfferRequest>();
                lstOfferRequest.add(offerRequest);
            }
        }
        if (lstOfferRequest == null || lstOfferRequest.isEmpty()) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "offerRequest");
            return mess;
        }
        //</editor-fold>
        //</editor-fold>
        Date nowDate = getSysDateTime(cmPosSession);
        //<editor-fold defaultstate="collapsed" desc="active subs">
        SubAdslLeaselineDAO subDAO = new SubAdslLeaselineDAO();
        DeployRequirementDAO deployDAO = new DeployRequirementDAO();
        for (OfferRequest offerRequest : lstOfferRequest) {
            if (offerRequest != null) {
                List<SubAdslLeaseline> subs = subDAO.findByReqOfferId(cmPosSession, offerRequest.getReqOfferId());
                if (subs != null && !subs.isEmpty()) {
                    for (SubAdslLeaseline sub : subs) {
                        if (sub != null) {
                            mess = validateLimitSub(cmPosSession, reason, locale);
                            if (mess != null) {
                                mess = mess.trim();
                                if (!mess.isEmpty()) {
                                    return mess;
                                }
                            }
                            //<editor-fold defaultstate="collapsed" desc="cap nhat yeu cau hotline">
                            info(_log, "SubscriberDAO.activeSubscriber:start update DeployRequirement;subId=" + sub.getSubId());
                            deployDAO.updateStatusBySubId(cmPosSession, sub.getSubId(), Constants.DEPLOY_REQ_STATUS_ACTIVE_SUB);
                            info(_log, "SubscriberDAO.activeSubscriber:end update DeployRequirement");
                            //</editor-fold>
                            mess = activeSubAdsl(cmPosSession, cmPreSession, imSession, pmSession, paymentSession, ccSession, nimsSession,
                                    shop, staff, reason, sub, nowDate, subReq, offerRequest, locale, dateDeploy, reasonDelay);
                            if (mess != null) {
                                mess = mess.trim();
                                if (!mess.isEmpty()) {
                                    return mess;
                                }
                            } else {
                                /*gui tin nhan den customer*/
                                Double deposit = 0d;
                                Double amountTax = 0d;
                                List lstMethodPay = getListMethodPay(cmPosSession, com.viettel.bccs.cm.common.util.Constant.TYPE_PAY_METHOD);

                                Contract contract = new ContractBussiness().findById(cmPosSession, sub.getContractId());
                                String namePayMethod = "";
                                if (contract != null) {
                                    String isdnCustomer = contract.getTelMobile() == null ? "" : contract.getTelMobile();
                                    String[] listIsdnCust = null;
                                    if (isdnCustomer.contains("-")) {
                                        listIsdnCust = isdnCustomer.split("-");
                                    } else if (isdnCustomer.contains("/")) {
                                        listIsdnCust = isdnCustomer.split("/");
                                    } else if (isdnCustomer.contains(",")) {
                                        listIsdnCust = isdnCustomer.split(",");
                                    } else if (isdnCustomer.contains(";")) {
                                        listIsdnCust = isdnCustomer.split(";");
                                    } else if (isdnCustomer.contains(" ")) {
                                        listIsdnCust = isdnCustomer.split(" ");
                                    } else {
                                        listIsdnCust = new String[]{contract.getTelMobile()};
                                    }
                                    if (listIsdnCust.length == 1) {
                                        sub.setTelFax(listIsdnCust[0]);
                                    } else if (listIsdnCust.length == 2) {
                                        sub.setTelFax(listIsdnCust[0]);
                                        sub.setTelMobile(listIsdnCust[1]);
                                    }
                                    cmPosSession.save(sub);
                                    if (sub.getDeposit() != null) {
                                        deposit = sub.getDeposit();
                                    }
                                    Customer customer = new CustomerBussiness().findById(cmPosSession, contract.getCustId());
                                    if (customer != null) {
                                        amountTax = getAmountTax(imSession, contract.getContractNo());
                                        namePayMethod = getNameMethodPay(lstMethodPay, contract.getPayMethodCode().trim());
                                        deposit = deposit + amountTax;
                                        if (listIsdnCust != null && listIsdnCust.length > 0) {
                                            for (String isdn : listIsdnCust) {
                                                isdn = isdn.trim();
                                                if (!"".equals(isdn)) {
                                                    setMessageToSend(isdn, subReq.getAccount(), deposit, namePayMethod, customer.getName(), cmPreSession, cmPosSession, nowDate, staff.getStaffCode(), shop.getShopCode());
                                                }
                                            }
                                        }
                                        /*Gui thong bao khuyen mai pay advance*/
                                        //check province cua thue bao co thuoc nhom 1
                                        ApParamSupplier apBusiness = new ApParamSupplier();
                                        List<ApParam> para = apBusiness.findByEachProperty(cmPosSession, "BRANCH", "GROUP1", null, sub.getProvince());
                                        List<String> listContentSms = null;
                                        IsdnSendSmsDAO isdnDao = new IsdnSendSmsDAO();
                                        List<String> listParam = new ArrayList<String>();
                                        if (para == null || para.isEmpty()) {
                                            listContentSms = isdnDao.genarateSMSContent(cmPreSession, null, Constants.AP_PARAM_PARAM_TYPE_SMS, "GROUP2_PAY_ADVANCE_PROMOTION", listParam);
                                        } else {
                                            para = apBusiness.findByEachProperty(cmPosSession, "PACKAGE_VIP_1", sub.getProductCode(), null, null);
                                            if (para == null || para.isEmpty()) {
                                                listContentSms = isdnDao.genarateSMSContent(cmPreSession, null, Constants.AP_PARAM_PARAM_TYPE_SMS, "GROUP1_NORMAL_PAY_ADVANCE_PROMOTION", listParam);
                                            } else {
                                                listContentSms = isdnDao.genarateSMSContent(cmPreSession, null, Constants.AP_PARAM_PARAM_TYPE_SMS, "GROUP1_VIP_PAY_ADVANCE_PROMOTION", listParam);
                                            }
                                        }
                                        if (listContentSms != null && !listContentSms.isEmpty()) {
                                            for (String isdn : listIsdnCust) {
                                                isdn = isdn.trim();
                                                if (!"".equals(isdn)) {
                                                    for (String content : listContentSms) {
                                                        isdnDao.insert(cmPreSession, isdn, content, nowDate, staff.getStaffCode(), shop.getShopCode(), "SIGN_CONTRACT_SUCCESSFUL");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                    //<editor-fold defaultstate="collapsed" desc="update offerRequest">
                    offerRequest.setStatus(Constants.OFFER_REQUEST_ACTIVE);
                    info(_log, "SubscriberDAO.activeSubscriber:start update OfferRequest;OfferRequest=" + offerRequest);
                    cmPosSession.update(offerRequest);
//                    cmPosSession.flush();
                    info(_log, "SubscriberDAO.activeSubscriber:end update OfferRequest");
                    //</editor-fold>
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="close CustRequest">
        info(_log, "SubscriberDAO.activeSubscriber:start close CustRequest;CustReqId=" + subReq.getCustReqId());
        new CustRequestBussiness().closeRequest(cmPosSession, subReq.getCustReqId());
        info(_log, "SubscriberDAO.activeSubscriber:end close CustRequest");
        //</editor-fold>
        mess = null;
        return mess;
    }

    public String validateLimitSub(Session cmPosSession, Reason reason, String locale) {
        if (reason != null) {
            Long limit = reason.getLimitNumberIsdn();
            if (limit != null && limit > 0L) {
                Long totalUsed = new ReasonOfflineDAO().getTotalUsedRegType(cmPosSession, reason.getCode());
                if (totalUsed >= 0L) {
                    if ((totalUsed + 1) > limit) {
                        return LabelUtil.formatKey("regType.over.limit.isdn", locale, reason.getCode(), limit, totalUsed + 1);
                    }
                }
            }
        }
        return null;
    }

    public String activeSubAdsl(Session cmPosSession, Session cmPreSession, Session imSession, Session pmSession, Session paymentSession, Session ccSession, Session nimsSession, Shop shop, Staff staff, Reason reason, SubAdslLeaseline sub, Date nowDate,
            SubReqAdslLl subReq, OfferRequest offerRequest, String locale, String dateDeploy, String reasonDelay) throws Exception {
        String mess;
        Long contractId = sub.getContractId();
        info(_log, "SubscriberDAO.activeSubAdsl:contractId=" + contractId);
        Contract contract = new ContractBussiness().findById(cmPosSession, contractId);
        if (contract == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "contract");
            return mess;
        }
        Long custId = contract.getCustId();
        Customer customer = new CustomerDAO().findById(cmPosSession, custId);
        if (customer == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "customer");
            return mess;
        }
        Long webServiceId = getWebServiceId(sub.getServiceType());
        String saleServiceCode = new MappingDAO().getSaleServiceCode(cmPosSession, webServiceId, reason.getReasonId(), sub.getProductCode(), Constants.ACTION_SUBSCRIBER_ACTIVE_NEW, null);
        if (saleServiceCode == null) {
            mess = LabelUtil.getKey("reason.not.mapping.saleService", locale);
            return mess;
        }
        saleServiceCode = saleServiceCode.trim();
        if (saleServiceCode.isEmpty()) {
            mess = LabelUtil.getKey("reason.not.mapping.saleService", locale);
            return mess;
        }
        //<editor-fold defaultstate="collapsed" desc="update database CM">
        //<editor-fold defaultstate="collapsed" desc="luu log action">
        ActionAuditBussiness auditDAO = new ActionAuditBussiness();
        StringBuilder des = new StringBuilder()
                .append("Active subscriber, Account=").append(sub.getAccount())
                .append(", subId=").append(sub.getSubId());
        info(_log, "SubscriberDAO.activeSubAdsl:start save ActionAudit");
        ActionAudit audit = auditDAO.insert(cmPosSession, nowDate, Constants.ACTION_SUBSCRIBER_ACTIVE_NEW, reason.getReasonId(), shop.getShopCode(), staff.getStaffCode(), Constants.ACTION_AUDIT_PK_TYPE_SUBSCRIBER, sub.getSubId(), Constants.WS_IP, des.toString());
        info(_log, "SubscriberDAO.activeSubAdsl:end save ActionAudit");
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="update SubReq">
        new SubReqAdslLlDAO().update(cmPosSession, imSession, getServiceId(sub.getServiceType()), sub.getServiceType(), subReq, audit.getActionAuditId(), locale, nowDate, Constants.SUB_REQ_STATUS_ACTIVE);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="update Subscriber">
        new SubAdslLeaselineDAO().update(cmPosSession, sub, nowDate, audit.getActionAuditId(), nowDate);
        if (sub.getReqOnlineId() != null) {
            RegOnlineDAO regOnlineDAO = new RegOnlineDAO();
            RegOnline request = regOnlineDAO.getRegOnlineById(cmPosSession, sub.getReqOnlineId());
            if (request != null) {
                regOnlineDAO.saveActionLog(cmPosSession, sub.getReqOnlineId(), "STATUS", request.getStatus().toString(), RegisterFTTHOnlineController.STATUS_REGISTERED.toString(), staff.getStaffCode(), "Registered account");
                request.setStatus(RegisterFTTHOnlineController.STATUS_REGISTERED);
                request.setUpdateUser(staff.getStaffCode());
                request.setUpdateDate(new Date());
                request.setReason(reason.getName());
                request.setAccount(sub.getAccount());
                cmPosSession.save(request);
                cmPosSession.flush();
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="update OfferSub">
        new OfferSubDAO().update(cmPosSession, sub.getSubId(), offerRequest.getReqOfferId(), nowDate, audit.getActionAuditId(), nowDate);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="save SubProduct">
        new SubProductAdslLlDAO().insert(cmPosSession, sub.getSubId(), nowDate, sub.getProductCode(), offerRequest.getOfferId(), audit.getActionAuditId(), nowDate);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="save SubAccount">
        new SubAccountsAdslLlDAO().insert(cmPosSession, sub, nowDate, audit.getActionAuditId(), nowDate);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="save SubActivate">
        new SubActivateAdslLlDAO().insert(cmPosSession, sub, nowDate, Constants.ACTION_SUBSCRIBER_ACTIVE_NEW, reason.getReasonId(), audit.getActionAuditId(), nowDate);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="save SubIp">
        new SubIpAdslLlDAO().insert(cmPosSession, sub, nowDate, audit.getActionAuditId(), nowDate);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="save SubDeployment">
        new SubDeploymentDAO().insert(cmPosSession, sub, subReq, Constants.ACTION_SUBSCRIBER_ACTIVE_NEW, nowDate, audit.getActionAuditId(), nowDate);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="save SubPromotion">
        new SubPromotionAdslLlDAO().insert(cmPosSession, sub, nowDate, audit.getActionAuditId(), nowDate);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="save deposit">
        new SubDepositAdslLlDAO().insert(cmPosSession, imSession, sub, staff, nowDate, audit.getActionAuditId(), customer, nowDate);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="update ContractOffer">
        new ContractOfferDAO().update(cmPosSession, sub);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="giao viec tu dong">
        //duyetdk
        Date dateDeploy_v = new Date();
        String reason_v = "";
        if (dateDeploy != null && !dateDeploy.trim().isEmpty()) {
            dateDeploy_v = DateTimeUtils.toDateddMMyyyyHHmmss(dateDeploy);
            reason_v = reasonDelay;
        } else {
            dateDeploy_v = nowDate;
        }
        new TaskManagementDAO().insert(cmPosSession, cmPreSession, ccSession, nimsSession, Constants.TASK_REQ_TYPE_NEW_CONNECTION, subReq, sub, contract, shop, staff, dateDeploy_v, audit.getActionAuditId(), nowDate, locale, reason_v);
        //</editor-fold>
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="update database IM">
        //<editor-fold defaultstate="collapsed" desc="update Invoice">
        IMDAO imDAO = new IMDAO();
        mess = imDAO.updateInvoiceToUsing(paymentSession, imSession, shop, staff, reason, customer, sub, nowDate, locale);
        if (mess != null) {
            mess = mess.trim();
            if (!mess.isEmpty()) {
                return mess;
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="execute SaleTrans">
        mess = imDAO.executeSaleTrans(cmPosSession, imSession, saleServiceCode, Constants.TRANS_OF_SUBSCRIBER, contract, customer, sub, shop, staff, reason, Constants.ACTION_SUBSCRIBER_ACTIVE_NEW, nowDate, locale);
        if (mess != null) {
            mess = mess.trim();
            if (!mess.isEmpty()) {
                return mess;
            }
        }
        //</editor-fold>
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="call Provisioning">
        ViettelService activeResponse = new Provisioning().activeSubscriber(cmPosSession, imSession, pmSession, customer.getBusType(), contract, sub, Constants.MODIFICATION_TYPE_REGISTER_NEW, "", shop.getShopCode(), staff.getStaffCode(), false, nowDate, locale);
        if (activeResponse == null) {
            mess = LabelUtil.getKey("activeSubscriber.fail", locale);
            return mess;
        }
        String responseCode = ConvertUtils.toStringValue(activeResponse.get(Constants.RESPONSE_CODE));
        if (Constants.RESPONSE_FAIL.equals(responseCode)) {
            mess = ConvertUtils.toStringValue(activeResponse.get(Constants.RESPONSE));
            return mess;
        }
        if (Constants.RESPONSE_INVALID.equals(responseCode)) {
            mess = ConvertUtils.toStringValue(activeResponse.get(Constants.RESPONSE));
            return mess;
        }
        if (Constants.RESPONSE_EXCEPTION.equals(responseCode)) {
            mess = ConvertUtils.toStringValue(activeResponse.get(Constants.EXCEPTION));
            return mess;
        }
        //</editor-fold>
        mess = null;
        return mess;
    }

    public String changeSim(Session cmPosSession, Session cmPreSession, Session imSession, Session ccSession, Session paymentSession, ChangeSimIn wsRequest, String locale) throws Exception {
        info(_log, "SubscriberDAO.changeSim:wsRequest=" + wsRequest + ";locale=" + locale);
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">
        mess = LabelUtil.validateLocale(locale);
        if (mess != null) {
            mess = mess.trim();
            if (!mess.isEmpty()) {
                return mess;
            }
        }
        String userLogin = wsRequest.getUserLogin();
        if (userLogin == null || userLogin.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "userLogin");
            return mess;
        }
        AccountAgent accountAgent = new CommonConnecSub().getAccountAgentInfo(imSession.connection(), userLogin);
        if (accountAgent == null) {
            mess = "Your sim don't tookit sim";
            return mess;
        }
        String service = wsRequest.getService();
        if (service == null || service.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "service");
            return mess;
        }
        String isdn = wsRequest.getIsdn();
        if (isdn == null || isdn.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "isdn");
            return mess;
        }
        isdn = subPrefixIsdn(isdn);
        boolean check = new CmPreCommon().checkValidIsdn(isdn);
        if (!check) {
            return "The subscriber is wrong format. Please call 0889012345 to get support. Thank you";
        }
        String idCard = wsRequest.getIdCard();
        if (idCard == null || idCard.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "idCard");
            return mess;
        }
        boolean checkIdCard = new CmPreCommon().checkValidIdNo(idCard);
        if (!checkIdCard) {
            return "Id number is wrong format";
        }
        String serial = wsRequest.getSerial();
        if (serial == null || serial.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "serial");
            return mess;
        }
        String reasonId = wsRequest.getReasonId();
        if (reasonId == null || reasonId.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "reasonId");
            return mess;
        }

        // Check serial thuoc kho nhan vien
        StockSim stockSim = new BaseCommon().getSerialInfor(imSession.connection(), serial, accountAgent.getOwnerId(), accountAgent.getOwnerType().toString());
        if (stockSim == null) {
            mess = "Sorry ! The Serial number is NOT exist in Col Stock. Pls check again";
            return mess;
        }
        // Kiem tra serial co thuco kho nhan vien quan ly khong
        stockSim = new CommonConnecSub().getSerialInfor(imSession.connection(), serial, accountAgent.getOwnerId(), accountAgent.getOwnerType().toString());
        if (stockSim == null) {
            mess = "Not found serial sim in stock";
            return mess;
        }
        String recentIsdn1 = wsRequest.getRecentIsdn1();
        if (recentIsdn1 == null || recentIsdn1.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "isdn1");
            return mess;
        }

        // Check number call recent
        MessageError msError = new PreChangeSimSupplier().checkNumberRecent(cmPreSession.connection(), isdn);
        if (com.viettel.common.util.Constant.RESULT_FAIL.equals(msError.getResult())) {
            return "Wrong contact number " + recentIsdn1 + ".Please recheck and try it again or call 0889012345 to get support. Thank you.";
        }
        // check contact number 1 
        recentIsdn1 = subPrefixIsdn(recentIsdn1);
        boolean check1 = new CmPreCommon().checkValidIsdn(recentIsdn1);
        if (!check1) {
            return "recent contact Isdn 1 is wrong format";
        }
        msError = new PreChangeSimSupplier().checkContactNumber(cmPreSession.connection(), isdn, recentIsdn1);
        if (com.viettel.common.util.Constant.RESULT_FAIL.equals(msError.getResult())) {
            return "Wrong contact number " + recentIsdn1 + ".Please recheck and try it again or call 0889012345 to get support. Thank you.";
        }

        String recentIsdn2 = wsRequest.getRecentIsdn2();
        if (recentIsdn2 == null || recentIsdn2.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "isdn2");
            return mess;
        }
        recentIsdn2 = subPrefixIsdn(recentIsdn2);
        boolean check2 = new CmPreCommon().checkValidIsdn(recentIsdn2);
        if (!check2) {
            return "recent contact Isdn 2 is wrong format";
        }
        msError = new PreChangeSimSupplier().checkContactNumber(cmPreSession.connection(), isdn, recentIsdn2);
        if (com.viettel.common.util.Constant.RESULT_FAIL.equals(msError.getResult())) {
            return "Wrong contact number " + recentIsdn2 + ".Please recheck and try it again or call 0889012345 to get support. Thank you.";
        }
        String recentIsdn3 = wsRequest.getRecentIsdn3();
        if (recentIsdn3 == null || recentIsdn3.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "isdn3");
            return mess;
        }
        recentIsdn3 = subPrefixIsdn(recentIsdn3);
        boolean check3 = new CmPreCommon().checkValidIsdn(recentIsdn3);
        if (!check3) {
            return "recent contact Isdn 3 is wrong format";
        }
        msError = new PreChangeSimSupplier().checkContactNumber(cmPreSession.connection(), isdn, recentIsdn3);
        if (com.viettel.common.util.Constant.RESULT_FAIL.equals(msError.getResult())) {
            return "Wrong contact number " + recentIsdn3 + ".Please recheck and try it again or call 0889012345 to get support. Thank you.";
        }

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="active">
        if (Constants.PRE_PAID.equals(service)) {
            // get info sim prepaid
            Subscriber sub = new PreChangeSimSupplier().getSubMbPre(cmPreSession.connection(), isdn);
            if (sub == null) {
                return "This subcriber is not active";
            }
            // Check reason
            com.viettel.bccs.cm.database.BO.pre.Reason reasonPre = new PreChangeSimSupplier().findById(cmPreSession.connection(), Long.parseLong(reasonId));
            if (reasonPre == null) {
                return "Can not find reason";
            }
            String act_status = sub.getActStatus();
            if (!act_status.equals("00") && !act_status.equals("000")) {
                return "The subscriber is not Active. Please call 0889012345 to get support. Thank you.";
            }

            // Gui tin nhan cho khach hang de thong bao
            if (!Common.isNullOrEmpty(isdn)) {
                String ip = "Mobile";
                String content = "The staff XXXX has requested change sim your phone, if you complaint. Please call 088012345 for support, Thank you.";
                content = content.replaceAll("XXXX", "0" + accountAgent.getMsisdn());
                String sendMesResult = new ManagerTaskDAO().sendMessNotification(userLogin, userLogin, isdn, content, ip, ccSession, cmPosSession);
                if (!Common.isNullOrEmpty(sendMesResult)) {
                    return "Send message for customer is error";
                    /*Gửi tn thất bại (Giao việc vẫn thành công nhưng ko gửi được tn cho NV)*/
                }
            }

            // Check them thong tin neu ID no ko ton tai
            msError = new PreChangeSimSupplier().checkExistIdNoCustPre(cmPreSession.connection(), sub.getCustId(), idCard);
            if ("0".equals(msError.getResult())) {
                String product = wsRequest.getProductCode();
                if (product == null || product.isEmpty()) {
                    return "PRODUCT_ERR";
                }
                // check mã gói cước nhạp vào
                if (!product.toUpperCase().equals(sub.getProductCode().toUpperCase())) {
                    return "Subscriber package is incorrect. Please recheck and try it again or call 0889012345 to get support. Thank you.";
                }
                String topUp = wsRequest.getTopUpAmount();
                if (topUp == null || topUp.isEmpty()) {
                    return "TOP_UP_ERR";
                }
                // Check tien topup gan nhat
                Double amount = new PreChangeSimSupplier().checkRefillHistory(cmPreSession.connection(), sub.getSubId());
                if (amount == null || Double.parseDouble(topUp) != amount) {
                    return "Amount top up is incorrect. Please recheck and try it again or call 0889012345 to get support. Thank you";
                }
            }
            // doi sim tra truoc
            msError = new PreChangeSimBusiness().changeSimPre(imSession.connection(), cmPreSession.connection(), accountAgent, stockSim, sub, Long.parseLong(reasonId));
            if ("0".equals(msError.getResult())) {
                return msError.getMessage();
            }
        } else if (Constants.POST_PAID.equals(service)) {
            // get info sim postPaid
            Subscriber sub = new PosChangeSimSupplier().getSubMbPos(cmPosSession.connection(), isdn);
            if (sub == null) {
                return "This subcriber is not active";
            }
            // Check reason
            com.viettel.bccs.cm.database.BO.Reason reasonPos = new PosChangeSimSupplier().findById(cmPreSession.connection(), Long.parseLong(reasonId));
            if (reasonPos == null) {
                return "Can not find reason";
            }
            String act_status = sub.getActStatus();
            if (!act_status.equals("00") && !act_status.equals("000")) {
                return "The subscriber is not Active. Please call 0889012345 to get support. Thank you.";
            }

            // Gui tin nhan cho khach hang de thong bao
            if (!Common.isNullOrEmpty(isdn)) {
                String ip = "Mobile";
                String content = "The staff XXXX has requested change sim your phone, if you complaint. Please call 088012345 for support, Thank you.";
                content = content.replaceAll("XXXX", "0" + accountAgent.getMsisdn());
                String sendMesResult = new ManagerTaskDAO().sendMessNotification(userLogin, userLogin, isdn, content, ip, ccSession, cmPosSession);
                if (!Common.isNullOrEmpty(sendMesResult)) {
                    return "Send message for customer is error";
                    /*Gửi tn thất bại (Giao việc vẫn thành công nhưng ko gửi được tn cho NV)*/
                }
            }

            // Check them thong tin neu ID no ko ton tai
            msError = new PosChangeSimSupplier().checkExistIdNoCustPos(cmPosSession.connection(), sub.getCustId(), idCard);
            if ("0".equals(msError.getResult())) {
                String product = wsRequest.getProductCode();
                if (product == null || product.isEmpty()) {
                    return "PRODUCT_ERR";
                }
                // check mã gói cước nhạp vào
                if (!product.toUpperCase().equals(sub.getProductCode().toUpperCase())) {
                    return "Subscriber package is incorrect. Please recheck and try it again or call 0889012345 to get support. Thank you.";
                }
                String topUp = wsRequest.getTopUpAmount();
                if (topUp == null || topUp.isEmpty()) {
                    return "TOP_UP_ERR";
                }
                // Check tien topup gan nhat
                PaymentContract payContract = new PaymentChangeSimSupplier().getPaymentContract(paymentSession.connection(), sub.getContractId());
                if (payContract == null || Double.parseDouble(topUp) != payContract.getPaymentAmount()) {
                    return "Amount top up is incorrect. Please recheck and try it again or call 0889012345 to get support. Thank you";
                }
            }
            // doi sim tra sau
            msError = new PreChangeSimBusiness().changeSimPos(imSession.connection(), cmPosSession.connection(), accountAgent, stockSim, sub, Long.parseLong(reasonId));
            if ("0".equals(msError.getResult())) {
                return msError.getMessage();
            }
        } else {
            return "Service don't exits";
        }
        //</editor-fold>
        mess = null;
        return mess;
    }

    public ReasonChangeSimOut getListReasonChangeSim(Session cmPosSession, Session cmPreSession, String isdn, String type, String locale) throws Exception {
        info(_log, "SubscriberDAO.changeSim:wsRequest=" + isdn + ";locale=" + locale);
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">
        mess = LabelUtil.validateLocale(locale);
        ReasonChangeSimOut result = new ReasonChangeSimOut();
        if (mess != null) {
            mess = mess.trim();
            if (!mess.isEmpty()) {
                result.setErrorCode(Constants.ERROR_CODE_2);
                result.setErrorDecription(mess);
                return result;
            }
        }
        if (isdn == null || isdn.isEmpty()) {
            result.setErrorCode(Constants.ERROR_CODE_2);
            mess = LabelUtil.getKey("validate.required", locale, "isdn");
            result.setErrorDecription(mess);
            return result;
        }
        boolean check = new CmPreCommon().checkValidIsdn(isdn);
        if (!check) {
            result.setErrorCode(Constants.ERROR_CODE_2);
            result.setErrorDecription("The subscriber is wrong format. Please call 0889012345 to get support. Thank you");
            return result;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="active">
        if (Constants.PRE_PAID.equals(type)) {
            // get info sim prepaid
            Subscriber sub = new PreChangeSimSupplier().getSubMbPre(cmPreSession.connection(), isdn);
            if (sub == null) {
                result.setErrorCode(Constants.ERROR_CODE_2);
                result.setErrorDecription("This subcriber is not active");
                return result;
            }
            String act_status = sub.getActStatus();
            if (!act_status.equals("00") && !act_status.equals("03")
                    && !act_status.equals("000")) {
                result.setErrorCode(Constants.ERROR_CODE_2);
                result.setErrorDecription("The subscriber is not Active. Please call 0889012345 to get support. Thank you.");
                return result;
            }
            List<com.viettel.bccs.cm.database.BO.pre.Reason> lstReasonChangePre = new PreChangeSimSupplier().getListReasonByActionCodeForAudit(cmPreSession.connection(), Constant.ACTION_SUBSCRIBER_CHANGE_SIM, sub != null ? sub.getProductCode() : null);
            if (lstReasonChangePre != null && !lstReasonChangePre.isEmpty()) {
                List<ReasonPre> lstReasonOut = new ArrayList<ReasonPre>();
                for (com.viettel.bccs.cm.database.BO.pre.Reason reasonPre : lstReasonChangePre) {
                    ReasonPre reasonOut = new ReasonPre();
                    reasonOut.setCode(reasonPre.getCode());
                    reasonOut.setName(reasonPre.getName());
                    reasonOut.setReasonId(reasonPre.getReasonId());
                    reasonOut.setDescription(reasonPre.getDescription());
                    lstReasonOut.add(reasonOut);
                }
                result.setErrorCode(Constants.ERROR_CODE_0);
                result.setReasonChangePre(lstReasonOut);
                result.setErrorDecription("Success");
            } else {
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription("Reason is empty");
            }
        } else if (Constants.POST_PAID.equals(type)) {
            // get info sim postPaid
            Subscriber sub = new PosChangeSimSupplier().getSubMbPos(cmPosSession.connection(), isdn);
            if (sub == null) {
                result.setErrorCode(Constants.ERROR_CODE_2);
                result.setErrorDecription("This subcriber is not active.");
                return result;
            }
            String act_status = sub.getActStatus();
            if (!act_status.equals("00") && !act_status.equals("03")
                    && !act_status.equals("000")) {
                result.setErrorCode(Constants.ERROR_CODE_2);
                result.setErrorDecription("The subscriber is not Active. Please call 0889012345 to get support. Thank you.");
                return result;
            }
            List<com.viettel.bccs.cm.database.BO.Reason> lstReasonChangePos = new PosChangeSimSupplier().getListReasonByActionCodeAndTelServiceForAudit(cmPosSession.connection(), com.viettel.bccs.cm.common.util.Constant.ACTION_SUBSCRIBER_CHANGE_INFO, "M", sub.getProductCode());
            if (lstReasonChangePos != null && !lstReasonChangePos.isEmpty()) {
                List<Reason> lstReasonOut = new ArrayList<Reason>();
                for (com.viettel.bccs.cm.database.BO.Reason reasonPos : lstReasonChangePos) {
                    Reason reasonOut = new Reason();
                    reasonOut.setCode(reasonPos.getCode());
                    reasonOut.setName(reasonPos.getName());
                    reasonOut.setReasonId(reasonPos.getReasonId());
                    reasonOut.setDescription(reasonPos.getDescription());
                    lstReasonOut.add(reasonOut);
                }
                result.setErrorCode(Constants.ERROR_CODE_0);
                result.setReasonChangePos(lstReasonOut);
                result.setErrorDecription("Success");
            } else {
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription("Reason is empty");
            }
        }
        return result;
    }

    private String subPrefixIsdn(String isdn) {
        isdn = isdn.startsWith("855") ? isdn.substring(3) : isdn;
        isdn = isdn.startsWith("0") ? isdn.substring(1) : isdn;
        return isdn;
    }

    public static List getListMethodPay(Session cmPost, String type) {
        String sql = "From ApDomain Where status = 1 and id.type = ?";
        Query query = cmPost.createQuery(sql);
        query.setParameter(0, type);
        List<ApDomain> lst = query.list();
        return lst;
    }

    public static Double getPayAdvAmount(Session cmposSession, Long contractId) {
        String sql = "SELECT nvl(pay_adv_amount,0) from Invoice_List_Staff_log where contract_Id = ?";
        Query query = cmposSession.createSQLQuery(sql);
        query.setParameter(0, contractId);
        List lst = query.list();
        if (!lst.isEmpty()) {
            return Double.valueOf(lst.get(0).toString());
        }
        return 0d;
    }

    public static Double getAmountTax(Session imSession, String contractNo) {
        StringBuilder str = new StringBuilder();
        str.append("SELECT nvl(amount_tax,0) FROM   sale_trans ");
        str.append("WHERE   sale_trans_date >= TRUNC (SYSDATE, 'mm') ");
        str.append("AND sale_trans_type = 4 ");
        str.append("AND contract_no = ? ");

        Query query = imSession.createSQLQuery(str.toString());
        query.setParameter(0, contractNo);
        List lst = query.list();
        if (!lst.isEmpty()) {
            return Double.valueOf(lst.get(0).toString());
        }
        return 0d;
    }

    public static String getNameMethodPay(List<ApDomain> lst, String code) {
        for (Iterator iterator = lst.iterator(); iterator.hasNext();) {
            ApDomain apDomain = (ApDomain) iterator.next();
            if (apDomain.getCode().equalsIgnoreCase(code)) {
                return apDomain.getName();
            }
        }
        return "";
    }

    public static void setMessageToSend(String isdn, String account, Double deposit, String namePayMethod, String customerName, Session cmPreSession, Session cmPostSession, Date nowDate, String staffCode, String shopCode) {
        List<String> lstDynamic = new ArrayList();
        lstDynamic.add(customerName);
        lstDynamic.add(account);
        lstDynamic.add(String.valueOf(deposit));
        lstDynamic.add(String.valueOf(namePayMethod));
        IsdnSendSmsDAO isdnDao = new IsdnSendSmsDAO();
        List<String> listContentSms = isdnDao.genarateSMSContent(cmPreSession, null, Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.SIGN_CONTRACT_SUCCESSFUL_SMS_TYPE, lstDynamic);
        if (listContentSms != null && !listContentSms.isEmpty()) {
            for (String content : listContentSms) {
                isdnDao.insert(cmPreSession, isdn, content, nowDate, staffCode, shopCode, "SIGN_CONTRACT_SUCCESSFUL");
            }
        }

    }
}
