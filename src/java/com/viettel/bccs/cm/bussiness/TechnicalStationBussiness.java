package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.TechnicalStation;
import com.viettel.bccs.cm.supplier.TechnicalStationSupplier;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

public class TechnicalStationBussiness extends BaseBussiness {

    protected TechnicalStationSupplier supplier = new TechnicalStationSupplier();

    public TechnicalStationBussiness() {
        logger = Logger.getLogger(TechnicalStationBussiness.class);
    }

    public TechnicalStation findByStation(Session cmPosSession, Long stationId) {
        TechnicalStation result = null;
        if (stationId == null || stationId <= 0L) {
            return result;
        }
        List<TechnicalStation> objs = supplier.findByStation(cmPosSession, stationId, Constants.STATUS_USE);
        result = (TechnicalStation) getBO(objs);
        return result;
    }

    public List<TechnicalStation> findStationOfTeam(Session cmPosSession, Long teamId, String province) {
        return supplier.findStationByTeam(cmPosSession, teamId, province);

    }
}
