package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.api.Task.DAO.TokenDAO;
import com.viettel.bccs.cm.bussiness.common.CacheBO;
import com.viettel.bccs.cm.bussiness.im.ImBussiness;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.CmUserToken;
import com.viettel.bccs.cm.model.Token;
import com.viettel.bccs.cm.supplier.ApParamSupplier;
import com.viettel.bccs.cm.supplier.TokenSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.common.util.DateTimeUtils;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.im.database.BO.AccountAgent;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.apache.log4j.Logger;
import org.hibernate.Session;

public class TokenBussiness extends BaseBussiness {

    protected TokenSupplier supplier = new TokenSupplier();

    public TokenBussiness() {
        logger = Logger.getLogger(TokenBussiness.class);
    }

    public Token findByToken(Session cmPosSession, String token) {
        Token result = null;
        if (token == null) {
            return result;
        }
        token = token.trim();
        if (token.isEmpty()) {
            return result;
        }
        List<Token> objs = supplier.findByToken(cmPosSession, token, Constants.STATUS_LOGED);
        result = (Token) getBO(objs);
        return result;
    }

    public Token findByUser(Session cmPosSession, String userName) {
        Token result = null;
        if (userName == null) {
            return result;
        }
        userName = userName.trim();
        if (userName.isEmpty()) {
            return result;
        }
        List<Token> objs = supplier.findByUser(cmPosSession, userName, Constants.STATUS_LOGED);
        result = (Token) getBO(objs);
        return result;
    }

    public Token insert(Session cmPosSession, String userName, String serial, String tokenVal, Date issueDate, Long staffId, String permission, String staffName, String deviceId) {
        return supplier.insert(cmPosSession, userName, serial, tokenVal, issueDate, staffId, permission, staffName, Constants.STATUS_LOGED, deviceId);
    }

    public Token update(Session cmPosSession, Token token) {
        if (token == null) {
            return token;
        }
        return supplier.update(cmPosSession, token);
    }

    public String getPermission(Session cmPosSession, CmUserToken userToken) {
        StringBuilder result = new StringBuilder();
        List<ApParam> params = new ApParamBussiness().findByType(cmPosSession, Constants.MAP_CM_PERMISSION);
        if (params != null && !params.isEmpty()) {
            String joiner = "";
            List<String> menus = userToken.getMenus();
            if (menus != null && !menus.isEmpty()) {
                for (String menu : menus) {
                    String index = getPermissionIndex(menu, params);
                    if (index != null) {
                        result.append(joiner).append(index);
                        joiner = ",";
                    }
                }
            }
            List<String> components = userToken.getComponents();
            if (components != null && !components.isEmpty()) {
                joiner = "";
                for (String component : components) {
                    String index = getPermissionIndex(component, params);
                    if (index != null) {
                        result.append(joiner).append(index);
                        joiner = ",";
                    }
                }
            }
        }
        return result.toString();
    }

    public String getPermissionIndex(String permission, List<ApParam> params) {
        String result = null;
        if (permission == null || params == null || params.isEmpty()) {
            return result;
        }
        permission = permission.trim();
        if (permission.isEmpty()) {
            return result;
        }
        for (ApParam param : params) {
            if (param != null) {
                String code = param.getParamCode();
                if (code != null) {
                    code = code.trim();
                    if (permission.equals(code)) {
                        result = param.getParamValue();
                        if (result != null) {
                            result = result.trim();
                            if (result.isEmpty()) {
                                result = null;
                            }
                        }
                        return result;
                    }
                }
            }
        }
        return result;
    }

    public boolean isTimeOut(Token token, Date currentTime) {
        boolean result = true;
        if (token == null) {
            return result;
        }
        Long timeOut = getTimeOut();
        Long minuteBetween = DateTimeUtils.getMinuteBetween(token.getLastRequest(), currentTime);
        if (minuteBetween != null && minuteBetween > timeOut) {
            return result;
        }
        Long lifeTime = getLifeTime();
        Long timeBetween = DateTimeUtils.getMinuteBetween(token.getCreateTime(), currentTime);
        if (timeBetween != null && timeBetween > lifeTime) {
            return result;
        }
        result = false;
        return result;
    }

    public Long getTimeOut() {
        String param = ResourceUtils.getResource("tokenTimeOut", "vsa_config");
        Long result = ConvertUtils.toLong(param);
        return result == null ? Constants.DEFAULT_TIME_OUT : result;
    }

    public Long getLifeTime() {
        String param = ResourceUtils.getResource("tokenLifeTime", "vsa_config");
        Long result = ConvertUtils.toLong(param);
        return result == null ? Constants.DEFAULT_LIFE_TIME : result;
    }

    public Token logToken(Session cmPosSession, Session imSession, String userName, String serial, Long staffId, String staffName, CmUserToken userToken, String locale, String deviceId) throws ParseException {
        LogUtils.info(logger, "TokenBussiness.logToken:userName=" + userName + ";serial=" + serial + ";staffId=" + staffId + ";staffName=" + staffName + ";locale=" + locale);
        Token token;
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="userName">
        if (userName == null) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("error.userName", locale));
            return token;
        }
        userName = userName.trim();
        if (userName.isEmpty()) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("error.userName", locale));
            return token;
        }
        userName = userName.toUpperCase();
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="serial">
        if (serial == null) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("error.serialSim", locale));
            return token;
        }
        serial = serial.trim();
        if (serial.isEmpty()) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("error.serialSim", locale));
            return token;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="staffId">
        if (staffId == null || staffId < 0L) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("staffId.is.required", locale));
            return token;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="staffName">
        if (staffName == null) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("error.staffName", locale));
            return token;
        }
        staffName = staffName.trim();
        if (staffName.isEmpty()) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("error.staffName", locale));
            return token;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="chuyen doi userToken ==> permission">
        String permission = getPermission(cmPosSession, userToken);
        //</editor-fold>
        //</editor-fold>
        AccountAgent accountAgent = new ImBussiness().findAccountAgent(imSession, userName, serial);
        if (accountAgent == null) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("error.serial", locale));
            return token;
        }
        token = findByUser(cmPosSession, userName);
        Date nowDate = getSysDateTime(cmPosSession);
        String tokenVal = DateTimeUtils.formatyyyyMMddHHmmss(nowDate) + UUID.randomUUID().toString().replaceAll("-", "");
        if (token != null) {
            // Da co token ==> update
            token.setStatus(Constants.END_LOGED);
            token = update(cmPosSession, token);
        }
        token = insert(cmPosSession, userName, serial, tokenVal, nowDate, staffId, permission, staffName, deviceId);
        return token;
    }

    public Token logToken2(Session cmPosSession, Session imSession, String userName, Long staffId, String staffName, CmUserToken userToken, String locale, String deviceId) throws ParseException {
        LogUtils.info(logger, "TokenBussiness.logToken:userName=" + userName + ";staffId=" + staffId + ";staffName=" + staffName + ";locale=" + locale);
        Token token;
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="userName">
        if (userName == null) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("error.userName", locale));
            return token;
        }
        userName = userName.trim();
        if (userName.isEmpty()) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("error.userName", locale));
            return token;
        }
        userName = userName.toUpperCase();
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="staffId">
        if (staffId == null || staffId < 0L) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("staffId.is.required", locale));
            return token;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="staffName">
        if (staffName == null) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("error.staffName", locale));
            return token;
        }
        staffName = staffName.trim();
        if (staffName.isEmpty()) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("error.staffName", locale));
            return token;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="chuyen doi userToken ==> permission">
        String permission = getPermission(cmPosSession, userToken);
        //</editor-fold>
        //</editor-fold>

        token = findByUser(cmPosSession, userName);
        Date nowDate = getSysDateTime(cmPosSession);
        String tokenVal = DateTimeUtils.formatyyyyMMddHHmmss(nowDate) + UUID.randomUUID().toString().replaceAll("-", "");
        if (token != null) {
            // Da co token ==> update
//            token.setStatus(Constants.END_LOGED);
//            token = update(cmPosSession, token);
            new TokenDAO().updateTokenByUser(cmPosSession, userName);
        }
        token = insert(cmPosSession, userName, null, tokenVal, nowDate, staffId, permission, staffName, deviceId);
        return token;
    }

    public String validateToken(Session cmPosSession, String tokenVal, String locale) throws ParseException {
        LogUtils.info(logger, "TokenBussiness.validateToken:tokenVal=" + tokenVal + ";locale=" + locale);
        String mess = null;
        /*Check cac token mac dinh*/
        com.viettel.bccs.api.Task.BO.ApParam param = CacheBO.getPassToken(cmPosSession, tokenVal);
        if (param == null) {
            Token token = findByToken(cmPosSession, tokenVal);
            if (token == null) {
                mess = LabelUtil.getKey("common.token.invalid", locale);
                return mess;
            }
            Date currentTime = getSysDateTime(cmPosSession);
            boolean isTimeOut = isTimeOut(token, currentTime);
            if (isTimeOut) {
                token.setStatus(Constants.END_LOGED);
                mess = LabelUtil.getKey("common.token.invalid", locale);
            } else {
                token.setLastRequest(currentTime);
                mess = null;
            }
            update(cmPosSession, token);
        }
        return mess;
    }
	
	public String validateTokenNoExpired(Session cmPosSession, String tokenVal, String locale) throws ParseException {
        LogUtils.info(logger, "TokenBussiness.validateToken:tokenVal=" + tokenVal + ";locale=" + locale);
        String mess="";
        Token token = findByToken(cmPosSession, tokenVal);
        if (token == null) {
            mess = LabelUtil.getKey("common.token.invalid", locale);
            return mess;
        }    
        update(cmPosSession, token);
        return mess;
    }

    public Token logToken2(Session cmPosSession, Session imSession, Session seessionMerchant, String userName, Long staffId, String staffName, String locale, String deviceId) throws ParseException {
        LogUtils.info(logger, "TokenBussiness.logToken:userName=" + userName + ";staffId=" + staffId + ";staffName=" + staffName + ";locale=" + locale);
        Token token;
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="userName">
        if (userName == null) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("error.userName", locale));
            return token;
        }
        userName = userName.trim();
        if (userName.isEmpty()) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("error.userName", locale));
            return token;
        }
        userName = userName.toUpperCase();
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="staffId">
        if (staffId == null || staffId < 0L) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("staffId.is.required", locale));
            return token;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="staffName">
        if (staffName == null) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("error.staffName", locale));
            return token;
        }
        staffName = staffName.trim();
        if (staffName.isEmpty()) {
            token = new Token(Constants.ERROR_CODE_1, LabelUtil.getKey("error.staffName", locale));
            return token;
        }
        //</editor-fold>
        //</editor-fold>

        /*Kiem tra username cho phep dang nhap tu nhieu cua hang*/
        List<ApParam> paramWhiteList = new ApParamSupplier().findByTypeMerchant(seessionMerchant, Constants.PARAM_TYPE_PASS_USER, null, staffName);

        token = findByUser(cmPosSession, userName);
        Date nowDate = getSysDateTime(cmPosSession);
        String tokenVal = CommonWebservice.encryptData(cmPosSession, userName + "/" + DateTimeUtils.formatyyyyMMddHHmmss(nowDate));
        if (paramWhiteList == null || paramWhiteList.isEmpty()) {
            if (token != null) {
                new TokenDAO().updateTokenByUser(cmPosSession, userName);
            }
            token = insert(cmPosSession, userName, null, tokenVal, nowDate, staffId, null, staffName, deviceId);
        } else {
            /*Cho phep dang nhap va su dung tu nhieu nguon*/
            if (token == null) {
                token = insert(cmPosSession, userName, null, tokenVal, nowDate, staffId, null, staffName, deviceId);
            } else {
                /*Kiem tra token expire*/
                Date currentTime = getSysDateTime(cmPosSession);
                boolean isTimeOut = isTimeOut(token, currentTime);
                if (isTimeOut) {
                    new TokenDAO().updateTokenByUser(cmPosSession, userName);
                    token = insert(cmPosSession, userName, null, tokenVal, nowDate, staffId, null, staffName, deviceId);
                }
            }
        }
        return token;
    }
}
