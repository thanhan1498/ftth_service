package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.Token;
import com.viettel.bccs.cm.model.TransLog;
import com.viettel.bccs.cm.supplier.TransLogSupplier;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class TransLogBussiness extends BaseBussiness {

    protected TransLogSupplier supplier = new TransLogSupplier();

    public TransLogBussiness() {
        logger = Logger.getLogger(TransLogBussiness.class);
    }

    public TransLog insert(Session cmPosSession, String tokenVal, String className, String method, String params, Long excute, String response) throws Exception {
        Token token = new TokenBussiness().findByToken(cmPosSession, tokenVal);
        Date issueDate = new Date();/*getSysDateTime(cmPosSession);*/
        TransLog transLog;
        if (token == null) {
            transLog = supplier.insert(cmPosSession, null, tokenVal, className, method, params, excute, issueDate, response);
            return transLog;
        }
        transLog = supplier.insert(cmPosSession, token.getTokenId(), token.getToken(), className, method, params, excute, issueDate, response);
        return transLog;

    }
}
