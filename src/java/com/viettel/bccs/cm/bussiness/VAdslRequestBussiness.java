package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.VAdslRequest;
import com.viettel.bccs.cm.supplier.VAdslRequestSupplier;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class VAdslRequestBussiness extends BaseBussiness {

    protected VAdslRequestSupplier supplier = new VAdslRequestSupplier();

    public VAdslRequestBussiness() {
        logger = Logger.getLogger(VAdslRequestBussiness.class);
    }

    public Long count(Session cmPosSession, Long isSmart, String addUser, String custName, String idNo, String serviceAlias, String account, Long subReqStatus, Date fromDate, Date toDate, List<Long> lstSubReqStatus) {
        Long result = supplier.count(cmPosSession, isSmart, addUser, custName, idNo, serviceAlias, account, subReqStatus, fromDate, toDate, lstSubReqStatus);
        return result;
    }

    public List<VAdslRequest> find(Session cmPosSession, Long isSmart, String addUser, String custName, String idNo, String serviceAlias, String account, Long subReqStatus, Date fromDate, Date toDate, List<Long> lstSubReqStatus, Integer start, Integer max) {
        List<VAdslRequest> result = supplier.find(cmPosSession, isSmart, addUser, custName, idNo, serviceAlias, account, subReqStatus, fromDate, toDate, lstSubReqStatus, start, max);
        return result;
    }
}
