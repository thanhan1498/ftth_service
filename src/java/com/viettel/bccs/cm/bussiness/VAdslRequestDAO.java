package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.bccs.cm.dao.DeployRequirementDAO;
import com.viettel.bccs.cm.dao.OfferRequestDAO;
import com.viettel.bccs.cm.dao.ReasonDAO;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.dao.SubReqAdslLlDAO;
import com.viettel.brcd.common.util.DateTimeUtils;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.brcd.common.util.ValidateUtils;
import com.viettel.bccs.cm.model.ActionAudit;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.OfferRequest;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.StepInfo;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubReqAdslLl;
import com.viettel.bccs.cm.model.VAdslRequest;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.ws.supplier.nims.lockinfras.LockAndUnlockInfrasResponse;
import com.viettel.brcd.ws.supplier.nims.lockinfras.NimsWsLockUnlockInfrasBusiness;
import com.viettel.brcd.ws.supplier.nims.lockinfras.ResultForm;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class VAdslRequestDAO extends BaseDAO {

    private List<VAdslRequest> reqs;
    private Long count;

    public List<VAdslRequest> getReqs() {
        return reqs;
    }

    public Long getCount() {
        return count;
    }

    public void buildQuery(StringBuilder sql, HashMap params, Long isSmart, String addUser, String custName, String idNo, String serviceAlias, String account, Long subReqStatus, Date fromDate, Date toDate) {
        if (sql == null || params == null) {
            return;
        }
        //<editor-fold defaultstate="collapsed" desc="isSmart">
        sql.append(" and isSmart = :isSmart ");
        params.put("isSmart", isSmart);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="addUser">
        sql.append(" and addUser = :addUser ");
        params.put("addUser", addUser);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="custName">
        if (custName != null) {
            custName = custName.trim();
            if (!custName.isEmpty()) {
                sql.append(" and ").append(createLike("custName", "custName"));
                params.put("custName", formatLike(custName));
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="idNo">
        if (idNo != null) {
            idNo = idNo.trim();
            if (!idNo.isEmpty()) {
                sql.append("  and ( idNo = :idNo or busPermitNo = :idNo ) ");
                params.put("idNo", idNo);
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="serviceAlias">
        if (serviceAlias != null) {
            serviceAlias = serviceAlias.trim();
            if (!serviceAlias.isEmpty()) {
                sql.append(" and serviceAlias = :serviceAlias ");
                params.put("serviceAlias", serviceAlias);
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="account">
        if (account != null) {
            account = account.trim();
            if (!account.isEmpty()) {
                sql.append(" and account = :account ");
                params.put("account", account);
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="subReqStatus">
        if (subReqStatus != null) {
            sql.append(" and subReqStatus = :subReqStatus ");
            params.put("subReqStatus", subReqStatus);
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fromDate">
        sql.append(" and reqDate >= :fromDate ");
        params.put("fromDate", fromDate);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="toDate">
        sql.append(" and reqDate < :toDate ");
        params.put("toDate", DateTimeUtils.addDay(toDate, 1));
        //</editor-fold>
    }

    public List<VAdslRequest> find(Session cmPosSession, String addUser, String custName, String idNo, String serviceAlias, String account, Long subReqStatus, Date fromDate, Date toDate, Long start, Long max) {
        StringBuilder sql = new StringBuilder(" from VAdslRequest where 1 = 1 ");
        HashMap params = new HashMap();
        buildQuery(sql, params, Constants.IS_SMART, addUser, custName, idNo, serviceAlias, account, subReqStatus, fromDate, toDate);

        sql.append(" and subReqStatus in (:lstSubReqStatus) ");
        List<Long> lstSubReqStatus = new ArrayList<Long>();
        lstSubReqStatus.add(Constants.SUB_REQ_STATUS_NEW);
        lstSubReqStatus.add(Constants.SUB_REQ_STATUS_SIGN_CONTRACT);
        lstSubReqStatus.add(Constants.SUB_REQ_STATUS_ACTIVE);
        lstSubReqStatus.add(Constants.SUB_REQ_STATUS_CANCEL);

        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        query.setParameterList("lstSubReqStatus", lstSubReqStatus);
        if (start != null) {
            query.setFirstResult(start.intValue());
        }
        if (max != null) {
            query.setMaxResults(max.intValue());
        }

        return query.list();
    }

    public Long count(Session session, String addUser, String custName, String idNo, String serviceAlias, String account, Long subReqStatus, Date fromDate, Date toDate) {
        StringBuilder sql = new StringBuilder(" select count(*) from VAdslRequest where 1 = 1 ");
        HashMap params = new HashMap();
        buildQuery(sql, params, Constants.IS_SMART, addUser, custName, idNo, serviceAlias, account, subReqStatus, fromDate, toDate);

        sql.append(" and subReqStatus in ( :lstSubReqStatus ) ");
        List<Long> lstSubReqStatus = new ArrayList<Long>();
        lstSubReqStatus.add(Constants.SUB_REQ_STATUS_NEW);
        lstSubReqStatus.add(Constants.SUB_REQ_STATUS_SIGN_CONTRACT);
        lstSubReqStatus.add(Constants.SUB_REQ_STATUS_ACTIVE);
        lstSubReqStatus.add(Constants.SUB_REQ_STATUS_CANCEL);

        Query query = session.createQuery(sql.toString());
        buildParameter(query, params);
        query.setParameterList("lstSubReqStatus", lstSubReqStatus);

        Object obj = query.uniqueResult();
        return obj == null ? null : Long.parseLong(obj.toString());
    }

    public String searchSubRequest(Session cmPosSession, String staffCode, String custName, String idNo, String serviceAlias, String account, Long subReqStatus, String fromDate, String toDate, Long pageSize, Long pageNo, String locale) {
        String mess = null;
        try {
            info(_log, "VAdslRequestDAO.searchSubRequest:staffCode=" + staffCode + ";custName=" + custName + ";idNo=" + idNo + ";serviceAlias=" + serviceAlias
                    + ";account=" + account + ";subReqStatus=" + subReqStatus + ";fromDate=" + fromDate
                    + ";toDate=" + toDate + ";pageSize=" + pageSize + ";pageNo=" + pageNo + ";locale=" + locale);
            //<editor-fold defaultstate="collapsed" desc="validate">
            //<editor-fold defaultstate="collapsed" desc="locale">
            mess = LabelUtil.validateLocale(locale);
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    return mess;
                }
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="staffCode">
            if (staffCode == null) {
                mess = LabelUtil.getKey("validate.required", locale, "staffCode");
                return mess;
            }
            staffCode = staffCode.trim();
            if (staffCode.isEmpty()) {
                mess = LabelUtil.getKey("validate.required", locale, "staffCode");
                return mess;
            }
            Integer length = ReflectUtils.getColumnLength(Staff.class, "staffCode");
            if (length != null && length > 0 && staffCode.length() > length) {
                mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("staffCode", locale), length);
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="custName">
            if (custName != null) {
                custName = custName.trim();
                if (!custName.isEmpty()) {
                    length = ReflectUtils.getColumnLength(Customer.class, "name");
                    if (length != null && length > 0 && staffCode.length() > length) {
                        mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("custName", locale), length);
                        return mess;
                    }
                }
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="idNo">
            if (idNo != null) {
                idNo = idNo.trim();
                if (!idNo.isEmpty()) {
                    length = ReflectUtils.getColumnLength(Customer.class, "idNo");
                    if (length != null && length > 0 && idNo.length() > length) {
                        mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("idNo", locale), length);
                        return mess;
                    }
                }
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="account">
            if (account != null) {
                account = account.trim();
                if (!account.isEmpty()) {
                    length = ReflectUtils.getColumnLength(SubAdslLeaseline.class, "account");
                    if (length != null && length > 0 && account.length() > length) {
                        mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("account", locale), length);
                        return mess;
                    }
                }
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="fromDate">
            if (fromDate == null) {
                mess = LabelUtil.getKey("input.invalid.fromDate", locale);
                return mess;
            }
            fromDate = fromDate.trim();
            if (fromDate.isEmpty()) {
                mess = LabelUtil.getKey("input.invalid.fromDate", locale);
                return mess;
            }
            if (!ValidateUtils.isDateddMMyyyy(fromDate)) {
                mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("fromDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                return mess;
            }
            Date dFrom;
            try {
                dFrom = DateTimeUtils.toDateddMMyyyy(fromDate);
            } catch (Exception ex) {
                error(_log, ex.getMessage(), ex);
                mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("fromDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                return mess;
            }
            if (dFrom == null) {
                mess = LabelUtil.getKey("validate.required", locale, "fromDate");
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="toDate">
            if (toDate == null) {
                mess = LabelUtil.getKey("input.invalid.toDate", locale);
                return mess;
            }
            toDate = toDate.trim();
            if (toDate.isEmpty()) {
                mess = LabelUtil.getKey("input.invalid.toDate", locale);
                return mess;
            }
            if (!ValidateUtils.isDateddMMyyyy(toDate)) {
                mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("toDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                return mess;
            }
            Date dTo;
            try {
                dTo = DateTimeUtils.toDateddMMyyyy(toDate);
            } catch (Exception ex) {
                error(_log, ex.getMessage(), ex);
                mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("toDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
                return mess;
            }
            if (dTo == null) {
                mess = LabelUtil.getKey("validate.required", locale, "toDate");
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="chi tim kiem trong 30 ngay">
            Integer days = DateTimeUtils.getDayBetween(dFrom, dTo);
            if (days != null && days > 30) {
                mess = LabelUtil.formatKey("search.between", locale, 30);
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="pageSize">
            if (pageSize == null || pageSize < 0L) {
                mess = LabelUtil.getKey("input.invalid.pageSize", locale);
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="pageNo">
            if (pageNo == null || pageNo < 0L) {
                mess = LabelUtil.getKey("input.invalid.pageNo", locale);
                return mess;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Staff">
            Staff staff = new StaffDAO().findByCode(cmPosSession, staffCode);
            if (staff == null) {
                mess = LabelUtil.getKey("staff.does.not.exist", locale);
                return mess;
            }
            //</editor-fold>
            //</editor-fold>
            count = count(cmPosSession, staffCode, custName, idNo, serviceAlias, account, subReqStatus, dFrom, dTo);
            if (count != null && count > 0L) {
                Long start = pageNo * pageSize;
                Long max = pageSize < count ? pageSize : count;
                reqs = find(cmPosSession, staffCode, custName, idNo, serviceAlias, account, subReqStatus, dFrom, dTo, start, max);
            }
            mess = null;
            return mess;
        } catch (Exception ex) {
            error(_log, "VAdslRequestDAO.searchSubRequest:Exception=" + ex.getMessage(), ex);
            mess = LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage());
            return mess;
        } finally {
            info(_log, "VAdslRequestDAO.searchSubRequest:mess=" + toJson(mess));
        }
    }

    public StepInfo getNextStepInfo(Session cmPosSession, Long subReqId, String locale) {
        StepInfo stepInfo = null;
        try {
            new BaseDAO().info(_log, "VAdslRequestDAO.getNextStepInfo:subReqId=" + subReqId + ";locale=" + locale);
            //<editor-fold defaultstate="collapsed" desc="validate">
            //<editor-fold defaultstate="collapsed" desc="locale">
            String mess = LabelUtil.validateLocale(locale);
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    stepInfo = new StepInfo(mess);
                    return stepInfo;
                }
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="subReqId">
            if (subReqId == null || subReqId <= 0L) {
                stepInfo = new StepInfo(LabelUtil.getKey("validate.required", locale, "subReqId"));
                return stepInfo;
            }
            SubReqAdslLlDAO subReqAdslLlDAO = new SubReqAdslLlDAO();
            SubReqAdslLl subReq = subReqAdslLlDAO.findById(cmPosSession, subReqId);
            if (subReq == null) {
                return new StepInfo(LabelUtil.getKey("validate.not.exists", locale, "subReq"));
            }
            //</editor-fold>
            //</editor-fold>
            if (!Constants.IS_SMART.equals(subReq.getIsSmart())) {
                return new StepInfo(LabelUtil.getKey("input.invalid.isNotIsSmart", locale));
            }
            stepInfo = subReqAdslLlDAO.getNextStepInfo(cmPosSession, subReq);
            return stepInfo;
        } catch (Exception ex) {
            error(_log, "VAdslRequestDAO.getNextStepInfo:Exception=" + ex.getMessage(), ex);
            stepInfo = new StepInfo(LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return stepInfo;
        } finally {
            info(_log, "VAdslRequestDAO.getNextStepInfo:result=" + toJson(stepInfo));
        }
    }

    public String cancelSubRequest(Session cmPosSession, Long subReqId, Long staffId, Long shopId, Long reasonId, String locale) throws Exception {
        String mess;
        info(_log, "VAdslRequestDAO.cancelRequest:subReqId=" + subReqId + ";staffId=" + staffId + ";shopId=" + shopId + ";reasonId=" + reasonId + ";locale=" + locale);
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="subReqId">
        if (subReqId == null || subReqId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "subReq");
            return mess;
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="staffId">
        if (staffId == null || staffId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "staffId");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="shopId">
        if (shopId == null || shopId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "shopId");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="reasonId">
        if (reasonId == null || reasonId <= 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "reason");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="staff">
        Staff staff = new StaffDAO().findById(cmPosSession, staffId);
        if (staff == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "staff");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="shop">
        Shop shop = new ShopDAO().findById(cmPosSession, shopId);
        if (shop == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "shop");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="reason">
        Reason reason = new ReasonDAO().findById(cmPosSession, reasonId);
        if (reason == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "reason");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="subReq">
        SubReqAdslLl subReq = new SubReqAdslLlDAO().findById(cmPosSession, subReqId);
        if (subReq == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "subReq");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="isSmart">
        Long isSmart = subReq.getIsSmart();
        info(_log, "VAdslRequestDAO.cancelRequest:isSmart=" + isSmart);
        if (isSmart == null || !Constants.IS_SMART.equals(isSmart)) {
            mess = LabelUtil.getKey("input.invalid.isNotIsSmart", locale, "isSmart");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="offerReqId">
        Long offerReqId = subReq.getReqOfferId();
        info(_log, "VAdslRequestDAO.cancelRequest:offerReqId=" + offerReqId);
        if (offerReqId == null || offerReqId < 0L) {
            mess = LabelUtil.getKey("validate.required", locale, "offerRequest");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="offerRequest">
        OfferRequestDAO offerRequestDAO = new OfferRequestDAO();
        OfferRequest offerRequest = offerRequestDAO.findById(cmPosSession, offerReqId);
        if (offerRequest == null) {
            mess = LabelUtil.getKey("validate.not.exists", locale, "offerRequest");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="subReqStatus">
        Long subReqStatus = subReq.getStatus();
        info(_log, "VAdslRequestDAO.cancelRequest:subReqStatus=" + subReqStatus);
        //<editor-fold defaultstate="collapsed" desc="Neu la y/c tiep nhan moi">
        if (Constants.SUB_REQ_STATUS_NEW.equals(subReqStatus)) {
            if (!Constants.OFFER_REQUEST_NEW.equals(offerRequest.getStatus())) {
                mess = LabelUtil.getKey("input.invalid.offerStatus", locale, "offerRequest");
                return mess;
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="update database CM">
        Date nowDate = getSysDateTime(cmPosSession);
        //<editor-fold defaultstate="collapsed" desc="luu log action">
        ActionAuditBussiness auditDAO = new ActionAuditBussiness();
        StringBuilder des = new StringBuilder()
                .append("Cancel subscriber request, Request id=").append(subReq.getId())
                .append(", subId=").append(subReq.getSubId());
        info(_log, "VAdslRequestDAO.cancelRequest:start save ActionAudit");
        ActionAudit audit = auditDAO.insert(cmPosSession, nowDate, Constants.ACTION_CANCEL_REQUEST, reason.getReasonId(), shop.getShopCode(), staff.getStaffCode(), Constants.ACTION_AUDIT_PK_TYPE_SUBSCRIBER, subReq.getId(), Constants.WS_IP, des.toString());
        info(_log, "VAdslRequestDAO.cancelRequest:end save ActionAudit");
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="cancelSubRequest">
        new SubReqAdslLlDAO().updateStatus(cmPosSession, subReq, Constants.SUB_REQ_STATUS_CANCEL, audit.getActionAuditId(), nowDate);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="cap nhat yeu cau hotline">
        info(_log, "VAdslRequestDAO.cancelRequest:start update DeployRequirement;subId=" + subReq.getSubId());
        new DeployRequirementDAO().updateStatusBySubId(cmPosSession, subReq.getSubId(), Constants.DEPLOY_REQ_STATUS_UN_SATISFY, audit.getActionAuditId(), nowDate);
        info(_log, "VAdslRequestDAO.cancelRequest:end update DeployRequirement");
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="goi NIMS giai phong tai nguyen">
        info(_log, "VAdslRequestDAO.cancelRequest:start unlock infras on NIMS system;account=" + subReq.getAccount() + ";isdn=" + subReq.getIsdn()
                + ";cableBoxId=" + subReq.getCableBoxId() + ";serviceType=" + subReq.getServiceType());
        LockAndUnlockInfrasResponse infrasResponse = NimsWsLockUnlockInfrasBusiness.lockUnlockInfras(NimsWsLockUnlockInfrasBusiness.INFRAS_ACTION_UNLOCK,
                subReq.getAccount(),
                null,
                subReq.getCableBoxId(),
                null,
                subReq.getServiceType(),
                null,
                null, null, null);
        info(_log, "VAdslRequestDAO.cancelRequest:end unlock infras on NIMS system;infrasResponse=" + toJson(infrasResponse));
        ResultForm resultForm = infrasResponse == null ? null : infrasResponse.getReturn();
        if (resultForm == null) {
            mess = LabelUtil.formatKey("lock.unlock.fail.1", locale);
            return mess;
        }
        if (!NimsWsLockUnlockInfrasBusiness.INFRAS_LOCL_UNLOCK_OK.equals(resultForm.getResult())) {
            mess = LabelUtil.formatKey("lock.unlock.fail", locale, resultForm.getMessage());
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="update offerRequest">
        offerRequestDAO.updateStatus(cmPosSession, offerRequest, Constants.OFFER_REQUEST_CANCEL, audit.getActionAuditId(), nowDate);
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="cancel customer request">
        new CustRequestBussiness().cancelRequest(cmPosSession, subReq.getCustReqId(), subReq.getId(), reason, staff, shop, nowDate);
        //</editor-fold>
        //</editor-fold>

        mess = null;
        return mess;
    }
}
