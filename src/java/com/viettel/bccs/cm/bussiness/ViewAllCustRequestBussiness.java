package com.viettel.bccs.cm.bussiness;

import com.viettel.bccs.cm.model.ViewAllCustRequest;
import com.viettel.bccs.cm.supplier.ViewAllCustRequestSupplier;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ViewAllCustRequestBussiness extends BaseBussiness {

    protected ViewAllCustRequestSupplier supplier = new ViewAllCustRequestSupplier();

    public ViewAllCustRequestBussiness() {
        logger = Logger.getLogger(ViewAllCustRequestBussiness.class);
    }

    public ViewAllCustRequest findById(Session cmPosSession, Long subReqId) {
        ViewAllCustRequest result = null;
        if (subReqId == null || subReqId <= 0L) {
            return result;
        }
        List<ViewAllCustRequest> objs = supplier.findById(cmPosSession, subReqId);
        result = (ViewAllCustRequest) getBO(objs);
        return result;
    }

    public List<ViewAllCustRequest> findByCustReqId(Session cmPosSession, Long custReqId) {
        List<ViewAllCustRequest> result = null;
        if (custReqId == null || custReqId <= 0L) {
            return result;
        }
        result = supplier.findByCustReqId(cmPosSession, custReqId);
        return result;
    }

    public boolean isCloseRequest(Session session, Long custReqId) {
        List<ViewAllCustRequest> requests = findByCustReqId(session, custReqId);
        if (requests == null || requests.isEmpty()) {
            return false;
        }
        for (ViewAllCustRequest request : requests) {
            if (request != null) {
                if (Constants.IS_NEW_SUB_TRUE.equals(request.getIsNewSub())) {// Neu thue bao moi
                    if (Constants.SUB_REQ_STATUS_NEW.equals(request.getStatus()) || Constants.SUB_REQ_STATUS_SIGN_CONTRACT.equals(request.getStatus())) {
                        return false;
                    }
                }
                if (Constants.IS_NEW_SUB_FALSE.equals(request.getIsNewSub())) {// Thue bao cu
                    if (Constants.STATUS_CUST_REQUEST_NEW.equals(request.getStatus())) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
