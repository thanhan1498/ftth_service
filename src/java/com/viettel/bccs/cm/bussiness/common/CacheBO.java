/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness.common;

import com.viettel.bccs.api.Task.BO.ApParam;
import com.viettel.bccs.api.Task.DAO.ApParamDAO;
import com.viettel.bccs.cm.bussiness.ApParamBussiness;
import com.viettel.bccs.cm.bussiness.AreaBussiness;
import com.viettel.bccs.cm.controller.ApDomainController;
import com.viettel.bccs.cm.controller.BusTypeController;
import com.viettel.bccs.cm.dao.TaskDAO;
import com.viettel.bccs.cm.model.Area;
import com.viettel.bccs.cm.model.KpiBonusTask;
import com.viettel.bccs.cm.model.KpiDeadlineTask;
import com.viettel.brcd.ws.model.output.CustomType;
import com.viettel.brcd.ws.model.output.District;
import com.viettel.brcd.ws.model.output.IdType;
import com.viettel.brcd.ws.model.output.Precinct;
import com.viettel.brcd.ws.model.output.Province;
import com.viettel.brcd.ws.supplier.soc.ErrorKnowledgeCataForm;
import com.viettel.brcd.ws.supplier.soc.SocService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class CacheBO {

    private static List<Province> listProvince = null;
    private static List<IdType> listIdType = null;
    private static Map<String, List> mapAreaDistrict = new HashMap<String, List>();
    private static Map<String, List> mapAreaPrecint = new HashMap<String, List>();
    private static List<CustomType> listBusType = null;
    private static HashMap<String, KpiDeadlineTask> listKpiDeadlineTask = null;
    private static List<ApParam> apParamSubTypeVip = null;
    private static List<ApParam> listPackageVip = null;
    private static HashMap<String, List<KpiBonusTask>> listKpiBonusTask = null;
    private static final Object lock = new Object();
    private static Map<String, ApParam> passToken = null;
    private static List<ErrorKnowledgeCataForm> listSocReasonFail = null;
    private static List<String> listProductVasFtth = null;

    public static List<Province> getListProvince(Session cmPosSession) {
        if (listProvince == null) {
            synchronized (lock) {
                List<Area> areas = new AreaBussiness().findProvinces(cmPosSession);
                if (areas != null && !areas.isEmpty()) {
                    listProvince = new ArrayList<Province>();
                    for (Area area : areas) {
                        if (area != null) {
                            listProvince.add(new Province(area));
                        }
                    }
                }
            }
        }
        return listProvince;
    }

    public static List<District> getListDistrict(Session cmPosSession, String provinceCode) {

        if (mapAreaDistrict == null) {
            mapAreaDistrict = new HashMap<String, List>();
        }
        if (mapAreaDistrict.get(provinceCode) == null) {
            synchronized (lock) {

                List<District> result = null;
                List<Area> areas = new AreaBussiness().findDistricts(cmPosSession, provinceCode);
                if (areas == null || areas.isEmpty()) {
                    return result;
                }
                result = new ArrayList<District>();
                for (Area area : areas) {
                    if (area != null) {
                        result.add(new District(area));
                    }
                }
                mapAreaDistrict.remove(provinceCode);
                mapAreaDistrict.put(provinceCode, result);
            }
        }
        return mapAreaDistrict.get(provinceCode);
    }

    public static List<Precinct> getListPrevince(Session cmPosSession, String provinceCode, String dictrictCode) {

        if (mapAreaPrecint == null) {
            mapAreaPrecint = new HashMap<String, List>();
        }
        if (mapAreaPrecint.get(provinceCode + "_" + dictrictCode) == null) {
            synchronized (lock) {

                List<Precinct> result = null;
                List<Area> areas = new AreaBussiness().findPrecincts(cmPosSession, provinceCode, dictrictCode);
                if (areas == null || areas.isEmpty()) {
                    return result;
                }
                result = new ArrayList<Precinct>();
                for (Area area : areas) {
                    if (area != null) {
                        result.add(new Precinct(area));
                    }
                }
                mapAreaPrecint.remove(provinceCode + "_" + dictrictCode);
                mapAreaPrecint.put(provinceCode + "_" + dictrictCode, result);
            }
        }
        return mapAreaPrecint.get(provinceCode + "_" + dictrictCode);
    }

    public static List<IdType> getIdType(Session cmPosSession, String busType) {
        if (listIdType == null) {
            synchronized (lock) {
                listIdType = new ApDomainController().getIdTypes(cmPosSession, busType);
            }
        }
        return listIdType;
    }

    public static List<CustomType> getListBusType(Session cmPosSession) {
        if (listBusType == null) {
            synchronized (lock) {
                listBusType = new BusTypeController().getBusTypes(cmPosSession);
            }
        }
        return listBusType;
    }

    public static HashMap<String, KpiDeadlineTask> getListKpiDeadlineTask(Session cmPosSession) {
        if (listKpiDeadlineTask == null) {
            synchronized (lock) {
                List<KpiDeadlineTask> list = new TaskDAO().getListKpiDeadlineTask(cmPosSession);
                if (list != null && !list.isEmpty()) {
                    listKpiDeadlineTask = new HashMap<String, KpiDeadlineTask>();
                    for (KpiDeadlineTask deadline : list) {
                        listKpiDeadlineTask.put(
                                deadline.getJobType()
                                + "_" + deadline.getServiceType()
                                + "_" + deadline.getInfraType()
                                + "_" + deadline.getIsVip().toString(),
                                deadline);
                    }
                }
            }
        }
        return listKpiDeadlineTask;
    }

    public static List<ApParam> getSubTypeVip(Session cmPosSession) {
        if (apParamSubTypeVip == null) {
            synchronized (lock) {
                apParamSubTypeVip = ApParamDAO.getApParamByTypeCode(cmPosSession, "DEFINE_VIP_SUB_TYPE", "SUB_TYPE_CODE");
            }
        }
        return apParamSubTypeVip;
    }

    public static List<ApParam> getListPackageVip(Session cmPosSession) {
        if (listPackageVip == null) {
            synchronized (lock) {
                listPackageVip = ApParamDAO.findParamsByType(cmPosSession, "PACKAGE_VIP");
            }
        }
        return listPackageVip;
    }

    public static HashMap<String, List<KpiBonusTask>> getListKpiBonusTask(Session cmPosSession) {
        if (listKpiBonusTask == null) {
            synchronized (lock) {
                listKpiBonusTask = new HashMap<String, List<KpiBonusTask>>();
                List<KpiBonusTask> list = new TaskDAO().getListKpiBonusTask(cmPosSession);
                for (KpiBonusTask kpi : list) {
                    List<KpiBonusTask> currentKey = listKpiBonusTask.get(kpi.getKeyDic());
                    if (currentKey == null) {
                        List<KpiBonusTask> temp = new ArrayList<KpiBonusTask>();
                        temp.add(kpi);
                        listKpiBonusTask.put(kpi.getKeyDic(), temp);
                    } else {
                        currentKey.add(kpi);
                    }
                }
            }
        }
        return listKpiBonusTask;
    }

    public static ApParam getPassToken(Session cmPosSession, String token) {
        if (passToken == null) {
            passToken = new HashMap<String, ApParam>();
        }
        if (passToken.get(token) == null) {
            synchronized (lock) {
                List<ApParam> param = new ApParamDAO().findByProperty("param_value", token, cmPosSession);
                if (param == null || param.isEmpty()) {
                    return null;
                }
                passToken.put(token, param.get(0));
            }
        }
        return passToken.get(token);
    }

    public static List<ErrorKnowledgeCataForm> getListSocReasonFail() {
        if (listSocReasonFail == null) {
            listSocReasonFail = new ArrayList<ErrorKnowledgeCataForm>();
        }
        if (listSocReasonFail.isEmpty()) {
            synchronized (lock) {
                try {
                    listSocReasonFail = SocService.getSOCReasonFail().getReturn();
                } catch (Exception ex) {
                }
            }
        }
        return listSocReasonFail;
    }

    public static List<String> getProductVasFTTH(Session cmPosSession) {
        if (listProductVasFtth == null) {
            synchronized (lock) {
                try {
                    listProductVasFtth = new ArrayList<String>();
                    List<com.viettel.bccs.cm.model.ApParam> param = new ApParamBussiness().findByType(cmPosSession, "FTTH_OTHER_PRODUCT");
                    for(com.viettel.bccs.cm.model.ApParam temp : param){
                        listProductVasFtth.add(temp.getParamValue());
                    }
                } catch (Exception ex) {
                }
            }
        }
        return listProductVasFtth;
    }
}
