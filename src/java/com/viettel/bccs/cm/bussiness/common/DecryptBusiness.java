package com.viettel.bccs.cm.bussiness.common;

import com.viettel.bccs.cm.bussiness.BaseBussiness;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.vsa.ResourceBundleUtil;
import com.viettel.security.PassTranformer;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class DecryptBusiness extends BaseBussiness {

    public DecryptBusiness() {
        logger = Logger.getLogger(DecryptBusiness.class);
    }

    public String getUserName(String userName) {
        try {
            return PassTranformer.decrypt(userName);
        } catch (Exception ex) {
            LogUtils.error(logger, "DecryptBusiness.getUserName:Exception=" + ex.getMessage(), ex);
            return null;
        }
    }

    public String getPassWord(String passWord, String user) {
        try {
            String pass = PassTranformer.decrypt(passWord);
            pass = pass.replace(user, "");
            return pass;
        } catch (Exception ex) {
            LogUtils.error(logger, "DecryptBusiness.getPassWord:Exception=" + ex.getMessage(), ex);
            return null;
        }
    }

    public String getPassWordDecryptRSA(String passWord) {
        try {
            // Đọc file chứa private key
            FileInputStream fis = new FileInputStream(ResourceBundleUtil.getResource("private.key"));
            byte[] b = new byte[fis.available()];
            fis.read(b);
            fis.close();

            // Tạo private key
            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(b);
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PrivateKey priKey = factory.generatePrivate(spec);

            // Giải mã dữ liệu
            Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            c.init(Cipher.DECRYPT_MODE, priKey);
            String result = new String(c.doFinal(Base64.decodeBase64(passWord)));
            System.out.println("result--------------" + result);
            return result;
        } catch (Exception ex) {
            LogUtils.error(logger, "DecryptBusiness.getPassWord:Exception=" + ex.getMessage(), ex);
            System.out.println("DecryptBusiness.getPassWord:Exception=" + ex.getMessage());
            return null;
        }
    }

    public static String encryptionPass(String input) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String output;
        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        sha.reset();
        sha.update(input.getBytes("UTF-8"));
        output = Base64.encodeBase64String(sha.digest());
        return output;
    }
    private static final String ALGORITHM_AES = "AES";

    public static String encrypt(String key, String valueToEnc) {
        try {
            Key aesKey = new SecretKeySpec(key.getBytes(), ALGORITHM_AES);
            Cipher c = Cipher.getInstance(ALGORITHM_AES);
            c.init(Cipher.ENCRYPT_MODE, aesKey);

            System.out.println("valueToEnc.getBytes().length " + valueToEnc.getBytes().length);
            byte[] encValue = c.doFinal(valueToEnc.getBytes());
            System.out.println("encValue length" + encValue.length);
            byte[] encryptedByteValue = new Base64().encode(encValue);
            String encryptedValue = new String(encryptedByteValue);
            System.out.println("encryptedValue " + encryptedValue);

            return encryptedValue;
        } catch (Exception ex) {
        }
        return null;
    }

    public static String decrypt(String key, String encryptedValue) {
        try {
            Key aesKey = new SecretKeySpec(key.getBytes(), ALGORITHM_AES);
            Cipher c = Cipher.getInstance(ALGORITHM_AES);
            c.init(Cipher.DECRYPT_MODE, aesKey);

            byte[] enctVal = new Base64().decode(encryptedValue.getBytes());
            System.out.println("enctVal length " + enctVal.length);

            byte[] decordedValue = c.doFinal(enctVal);

            return new String(decordedValue);
        } catch (Exception ex) {
        }
        return null;
    }
}
