/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness.common;

import com.viettel.bccs.cc.controller.ComplaintController;
import com.viettel.bccs.cm.bussiness.BaseBussiness;
import com.viettel.common.ExchMsg;
import com.vtc.provisioning.client.Exchange;
import java.util.HashMap;
import com.viettel.bccs.cm.bussiness.common.Object.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cuongdm
 */
public class ExchangeClient extends BaseBussiness {

    static final String REQUEST = "REQUEST";
    static final String RESPONSE = "RESPONSE";
    static final String ERROR = "Error";
    static Logger logger = Logger.getLogger(ExchangeClient.class.getName());

    public static InfoCell getInforHlr(String msisdn) {
        String mscIdKey = "MSC_ID";
        String imeiKey = "IMEI";
        ExchMsg request;
        ExchMsg response;
        try {
            Exchange exchange = new Exchange();
            msisdn = msisdn.startsWith("0") ? msisdn.substring(1) : msisdn;
            msisdn = msisdn.startsWith("855") ? msisdn : "855" + msisdn;
            HashMap<String, ExchMsg> lstResult = exchange.getHlrInfo(msisdn);
            request = lstResult.get(REQUEST);
            logger.log(Level.INFO, request.toString());
            response = lstResult.get(RESPONSE);
            logger.log(Level.INFO, response.toString());
            String mscId = (String) response.get(mscIdKey);
            String imei = (String) response.get(imeiKey);
            InfoCell infoCell = new InfoCell();
            infoCell.setImei(imei);
            infoCell.setMscId(mscId);
            if (response.toString().contains("USIM")) {
                infoCell.setIsUSIM(true);
            } else {
                infoCell.setIsUSIM(false);
            }
            return infoCell;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.toString());
        }

        return null;
    }

    public static InfoCell getMscInfor(String msisdn, String mscId) {
        String detailKey = "DETAIL";
        String cellIdNameKey = "Cell Identity Name";
        String status4GKey = "SGs Association Status";
        String accessNetworkTypeKey = "Access network type";
        String keyImei = "IMEI";
        InfoCell infoCell = new InfoCell();
        ExchMsg request;
        ExchMsg response;
        try {
            Exchange exchange = new Exchange();
            msisdn = msisdn.startsWith("0") ? msisdn.substring(1) : msisdn;
            msisdn = msisdn.startsWith("855") ? msisdn : "855" + msisdn;
            HashMap<String, ExchMsg> lstResult = exchange.getMscInfo(msisdn, mscId);
            request = lstResult.get(REQUEST);
            logger.log(Level.INFO, request.toString());
            response = lstResult.get(RESPONSE);
            logger.log(Level.INFO, response.toString());
            String detail = (String) response.get(detailKey);
            if (detail != null && detail.contains(cellIdNameKey)) {
                String cellIdKeyAndName = detail.substring(detail.indexOf(cellIdNameKey)).split("\n")[0];
                String cellIdName = cellIdKeyAndName.split("=")[1].trim();

                String accessNetworkType = detail.substring(detail.indexOf(accessNetworkTypeKey)).split("\n")[0];
                if (accessNetworkType.contains("UTRAN")) {
                    String flag4g = detail.substring(detail.indexOf(status4GKey)).split("\n")[0];
                    if (flag4g.contains("Not")) {
                        infoCell.setType(ComplaintController.NETWORK_3G);
                    } else {
                        infoCell.setType(ComplaintController.NETWORK_4G);
                    }
                } else {
                    infoCell.setType(ComplaintController.NETWORK_2G);
                }
                infoCell.setCellCode(cellIdName);

                String keyImeiValue = detail.substring(detail.indexOf(keyImei)).split("\n")[0];
                String imeiValue = keyImeiValue.split("=")[1].trim();
                infoCell.setImei(imeiValue);
            }
            infoCell.setCellId((String) response.get("CELL_ID"));

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.toString());
        }
        return infoCell;
    }
}
