/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness.common;

import com.viettel.bccs.cm.model.ResultBO;
import com.viettel.brcd.common.util.MessageUtils;
import com.viettel.brcd.ws.vsa.ResourceBundleUtil;
import com.viettel.pf.database.BO.RecordTypeScan;
import com.viettel.pf.database.DAO.RecordTypeDao;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;
import org.apache.struts.upload.FormFile;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.codec.binary.Base64;
import org.hibernate.Session;

/**
 *
 * @author tuantm11
 */
public class FTPBusiness {

    private static final Logger log = Logger.getLogger(FTPBusiness.class);
    private static boolean uploadFtp = true;
    private static Map<String, List<List<RecordTypeScan>>> mapListProfileNeedToScanPrepaid = null;
    public static final String MAX_SIZE_ERROR_NOTICE = "WN_REGSF_0006";
    public static final String BAD_FILE_ERROR_NOTICE = "WN_REGSF_0005";
    public static final String MAX_INPUT_SIZE_ERROR_NOTICE = "Chỉ được upload file dữ liệu < ";
    private static String location = null;

    public FTPBusiness(String location) {
        if (location == null) {
            this.location = "en_US";
        } else {
            this.location = location;
        }
    }

    /**
     * put file to FTP server
     *
     * @param host
     * @param username
     * @param password
     * @param fileName
     * @param destinationDir
     * @throws Exception
     */
    public static boolean putFileToFTPServer(String filePath, String fileName,
            String serverName, String userName, String password, String serverDir) throws Exception {
        boolean isSend = true;
        try {
            //Mo ket noi toi FTP server
            log.info("\n start 1");
            int reply;
            boolean success = false;
            FileInputStream fis = null;
            FTPClient ftp = new FTPClient();
            ftp.enterLocalPassiveMode();
            ftp.connect(serverName);
            ftp.login(userName, password);
            reply = ftp.getReplyCode();
            log.info("\n start 2");
            if (FTPReply.isPositiveCompletion(reply)) {
                log.info("\n start 3");
                ftp.makeDirectory(serverDir);
                success = ftp.changeWorkingDirectory(serverDir);
                if (success) {
                    log.info("\n start 4");
                    ftp.enterLocalPassiveMode();
                    fis = new FileInputStream(filePath);
                    ftp.setFileType(2);
                    boolean isStore = ftp.storeFile(fileName, fis);
                    if (!isStore) {
                        ftp.enterLocalActiveMode();
                        fis = new FileInputStream(filePath);
                        ftp.setFileType(2);
                        ftp.storeFile(fileName, fis);
                    }
                } else {
                    isSend = false;
                    log.info("\n start 5");
                }
                log.info("\n start 6");
                //Logout server
                boolean logout = ftp.logout();
                if (logout) {
                }
                if (fis != null) {
                    fis.close();
                }
            } else {
                isSend = false;
            }
            ftp.disconnect();

        } catch (Exception e) {
            e.printStackTrace();
            isSend = false;
        }
        return isSend;
    }

    /**
     * retrieve file from FTP Server
     *
     * @param host
     * @param username
     * @param password
     * @param fileName
     * @param destinationDir
     * @return
     * @throws Exception
     */
    public static String retrieveFileFromFTPServer(String host, String username, String password, String fileName, String destinationDir, String tempDir) throws Exception {
        FTPClient client = new FTPClient();
        FileInputStream fis = null;
        try {
            // connect, login, set timeout
            client.connect(host);
            client.login(username, password);

            // set transfer type
            client.setFileTransferMode(FTP.BINARY_FILE_TYPE);
            client.setFileType(FTP.BINARY_FILE_TYPE);
            client.enterLocalPassiveMode();

            // set timeout
            client.setSoTimeout(60000); // default 1 minute

            // check connection
            int reply = client.getReplyCode();
            if (FTPReply.isPositiveCompletion(reply)) {
                // switch to working folder
                client.changeWorkingDirectory(destinationDir);
                // create temp file on temp directory
                File tempFile = new File(tempDir + File.separator + fileName);
                String strData = "";

                // retrieve file from server and logout
                OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(tempFile));
                if (client.retrieveFile(destinationDir + File.separator + fileName, outputStream)) {
                    outputStream.close();
                }
                // close connection
                client.logout();
                if (tempFile.isFile()) {
                    InputStream inputStream = new FileInputStream(tempFile);
                    BufferedInputStream bInf = new BufferedInputStream(inputStream);
                    byte[] buffer = new byte[(int) tempFile.length()];
                    bInf.read(buffer);
                    byte[] encode = Base64.encodeBase64(buffer);
                    strData = new String(encode, Charset.forName("UTF-8"));
//                    lstData.add(strData);
                    inputStream.close();
                }

                //delete file
                tempFile.delete();

                // return file
                return strData;
//                return tempFile;
            } else {
                client.disconnect();
                System.err.println("FTP server refused connection !");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (fis != null) {
                fis.close();
            }
            client.disconnect();
        }
        return null;
    }

    //<editor-fold defaultstate="collapsed" desc="QuocDM1 151126 code ham uploadImage len Profile">
    /**
     *
     * @param isdn
     * @param actionCode
     * @param reasonId
     * @param lstFile
     * @param lstFileCode
     * @param nowDate
     * @param actionAuditId
     * @param cur_id
     * @param serverCfg
     * @return
     * @throws Exception
     */
    //</editor-fold>
    public static String uploadToFtpServer(String isdn, String actionCode, Long reasonId, List<FormFile> lstFile, List<String> lstFileCode,
            Date nowDate, Long actionAuditId, long cur_id) throws Exception {
        if (!uploadFtp || lstFile == null || lstFile.isEmpty()) {
            return "";
        }
        String message = "";
        //<editor-fold defaultstate="collapsed" desc="check them cho chat che">
        int fileCount = lstFile == null ? 0 : lstFile.size();
        int fileCodeCount = lstFileCode == null ? 0 : lstFileCode.size();
        if (fileCount < fileCodeCount) {
            return MessageUtils.getMessage("err.uploadFrame.005", location);
        }
        //</editor-fold>
        //Khoi tao ten file de upload len server
        String[] allowedExtensionList = {"pdf", "png", "jpg", "bmp", "gif", "jpe", "jpeg", "doc"};
        final int MAX_SIZE = 2 * 1024 * 1024;

        //Validate file
        for (int i = 0; i < lstFile.size(); i++) {
            String errorMessage = validateFileUploadWhiteList(lstFile.get(i), allowedExtensionList, MAX_SIZE);
            if (!isNullOrEmpty(errorMessage)) {
                log.info(cur_id + ": Validate file " + lstFile.get(i).getFileName() + " failed.errorMessage=" + errorMessage);
                return errorMessage;
            }
        }
        log.info(cur_id + "Validate all files succesfull!");

        //Sau khi validate thanh cong, bat dau upload file
        DateFormat formatFileName = new SimpleDateFormat("yyyyMMdd");
        String strDate = formatFileName.format(nowDate);
        FTPClient ftp = null;
        long curr_time = 0;
        try {

            //Begin Get Ftp Connection
            log.info(cur_id + " start: get FTP connection....");
            curr_time = System.currentTimeMillis(); //Lay thoi gian hien tai
            ResultBO resultBO = getFtpClient(cur_id);
            log.info(cur_id + " end: get FTP connection. Duration = "
                    + (System.currentTimeMillis() - curr_time) / 1000);
            String getFtpMessage = resultBO.getMessage();
            if (!isNullOrEmpty(getFtpMessage)) {
                // CommonSub.rollBackSessions();
                log.info(cur_id + "Error happened. Detail:" + getFtpMessage);
                return getFtpMessage;
            } else {
                log.info(cur_id + " Connect to FTP server successfull! Begin upload file....");
            }
            ftp = (FTPClient) resultBO.getResultObject();
            for (int i = 0; i < lstFile.size(); i++) {
                //Ten file:    YYYYMMDD_ ISDN_mã chứng từ_actionCode_reasonId_ActionAuditId_OK.duoifile
                //Chu thich: fileCode = file code_actionCode_reasonId (xu ly tren JSP)
                String tmpFilename = strDate + "_" + isdn + "_" + lstFileCode.get(i) + "_" + actionCode + "_" + reasonId
                        + "_" + actionAuditId + "_OK" + processFile(lstFile.get(i).getFileName());
                log.info(cur_id + " Begin upload File: " + tmpFilename + " lên FTP... ");
                curr_time = System.currentTimeMillis(); //Lay thoi gian hien tai
                message = sendFileToFtpServer(lstFile.get(i), tmpFilename, ftp, cur_id);

                if (!"".equals(message)) {
                    log.info(cur_id + " Upload File: " + tmpFilename + " failed. Duration: "
                            + (System.currentTimeMillis() - curr_time));
                    return message;
                }
                log.info(cur_id + " Upload File: " + tmpFilename + " successfull. Duration: "
                        + (System.currentTimeMillis() - curr_time));
            }
        } catch (Exception ex) {
            log.error("Error!", ex);
            return MessageUtils.getMessage("err.uploadFrame.010", location);
        } finally {
            try {
                closeFtpConnection(ftp, cur_id);
            } catch (Exception ex) {
                log.error("Error!", ex);
            }
        }
        return message;
    }

    //<editor-fold defaultstate="collapsed" desc="QuocDM1 151126 code ham uploadImage len Profile">
    /**
     * R8445
     *
     * @param actionCode
     * @param reasonId
     * @param cusType
     * @param serviceType
     * @param productCode
     * @param lstFileCode
     * @return
     * @throws Exception
     */
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="QuocDM1 151127 code lai ham upload image tu Profile">
    /**
     * QuocDM1 151127 getListProfileNeedToScanPrepaid
     *
     * @param actionCode
     * @param reasonId
     * @param cusType
     * @param productCode
     * @param serviceType
     * @return
     * @throws Exception
     */
    //</editor-fold>
    public static List<List<RecordTypeScan>> getListProfileNeedToScanPrepaid(Session session, String actionCode, Long reasonId, String cusType,
            String productCode, String serviceType) throws Exception {
        Session profileSession = null;
        List<List<RecordTypeScan>> lstResult = new ArrayList();



        try {
            profileSession = session;
            RecordTypeDao scan = new RecordTypeDao(profileSession);

            List<List<RecordTypeScan>> lstTmp = getListProfileNeedToScanPrepaid(actionCode, reasonId, cusType, productCode, serviceType, scan);
            if (lstTmp != null) {
                for (List<RecordTypeScan> lstProfileType : lstTmp) {
                    List<RecordTypeScan> lstProfileTypeTmp = new ArrayList();
                    for (RecordTypeScan recordTypeScan : lstProfileType) {
                        if (recordTypeScan.getReqScan() == 1L) {//neu Ho so can phai scan
                            lstProfileTypeTmp.add(recordTypeScan);
                        }
                    }
                    if (!lstProfileTypeTmp.isEmpty()) {
                        lstResult.add(lstProfileTypeTmp);
                    }
                }
            }
        } catch (Exception ex) {
            log.error("Error: ", ex);
            throw ex;
        }
        return lstResult;
    }

    //<editor-fold defaultstate="collapsed" desc="QuocDM1 151127 code lai ham upload image tu Profile">
    /**
     * QuocDM1 151127 code lai ham upload image tu Ho So
     *
     * @param fileAttachment
     * @param extension
     * @param maxSize
     * @return
     */
    //</editor-fold>
    public static List<List<RecordTypeScan>> getListProfileNeedToScanPrepaid(String actionCode, Long reasonId, String cusType,
            String productCode, String serviceType, RecordTypeDao scan) throws Exception {
        if (mapListProfileNeedToScanPrepaid == null) {
            mapListProfileNeedToScanPrepaid = new HashMap<String, List<List<RecordTypeScan>>>();
        }
        String lang = System.getProperty("user.current.locale");
        if (lang == null || lang.isEmpty()) {
            lang = "en_US";
        } else {
            if (lang.equalsIgnoreCase("en")) {
                lang = "en_US";
            } else if (lang.equalsIgnoreCase("vi")) {
                lang = "vi_VN";
            } else if (lang.equalsIgnoreCase("fr")) {
                lang = "fr_FR";
            } else if (lang.equalsIgnoreCase("pt")) {
                lang = "pt_PT";
            } else {
                lang = "en_US";
            }
        }
        String strMap = actionCode + "," + reasonId + "," + cusType + "," + productCode + "," + serviceType + "," + lang;
        strMap = strMap.toLowerCase();
        List<List<RecordTypeScan>> lstResult = mapListProfileNeedToScanPrepaid.get(strMap);
        if (lstResult == null) {
            List<List<RecordTypeScan>> lstTmp = scan.getListRecordPrepaid(actionCode, reasonId, cusType, productCode, serviceType, lang);
            mapListProfileNeedToScanPrepaid.put(strMap, lstTmp);
        }
        return mapListProfileNeedToScanPrepaid.get(strMap);
    }

    private static String validateFileUploadWhiteList(FormFile fileAttachment, String[] extension, int maxSize) {
        if (fileAttachment == null) {
            return "";
        }
        String fileName = fileAttachment.getFileName();

        // check file name
        if (isNullOrEmpty(fileName)) {
            return "err.uploadFrame.007";
        }

        // check duoi file
        String[] fileNameArr = fileName.split("\\.");
        if (fileNameArr == null || fileNameArr.length != 2) {
            return BAD_FILE_ERROR_NOTICE;
        }
        if (maxSize != 0 && fileAttachment.getFileSize() > maxSize) {
            return MAX_INPUT_SIZE_ERROR_NOTICE + maxSize / (1024 * 1024) + " MB.";
        }

        //kiem tra ten file mo rong co fai la extension ko
        String extensionFileName = fileNameArr[1];
        boolean isOk = false;
        String strFile = "";
        for (int i = 0; i < extension.length; i++) {
            strFile += extension[i] + ",";
            if (!isOk) {
                if (extension[i].equalsIgnoreCase(extensionFileName)) {
                    isOk = true;
                    break;
                }
            }
        }
        if (!isNullOrEmpty(strFile) && strFile.substring(strFile.length() - 1).equals(",")) {
            strFile = strFile.substring(0, strFile.length() - 1);
        }
        if (!isOk) {
            String message = MessageUtils.getMessage("err.uploadFrame.008", location);
            //getMultiLanguage("err.uploadFrame.008", null, "MessageDAO");
            message = message.replace("{0}", fileName);
            message = message.replace("{1}", extensionFileName.toString());
            message = message.replace("{2}", strFile);
            return message;
        }

        return validateFileName(fileName);
    }

    //<editor-fold defaultstate="collapsed" desc="QuocDM1 151127 getListProfileNeedToScanPrepaid">
    /**
     * QuocDM1 151127 getListProfileNeedToScanPrepaid
     *
     * @param actionCode
     * @return
     * @throws Exception
     */
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="QuocDM1 151127 ham validateFileName">
    /**
     * QuocDM1 151127 ham validateFileName
     *
     * @param fileName
     * @return
     */
    //</editor-fold>
    public static String validateFileName(String fileName) {
        String expression = "[_a-zA-Z0-9\\-\\.]+";
        //Make the comparison case-insensitive.
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(fileName);
        if (!matcher.matches()) {
//            return "Tên file chỉ được bao gồm các kí tự chữ cái (a-z, A-Z), chữ số (0-9), gạch ngang (-), gạch dưới (_) và dấu chấm (.) ";
            return "WN_REGSF_0002";
        }
        return null;
    }

    //<editor-fold defaultstate="collapsed" desc="QuocDM1 151127 ham check isNullOrEmpty">
    /**
     * QuocDM1 151127
     *
     * @param str
     * @return
     */
    //</editor-fold>
    public static boolean isNullOrEmpty(String str) {
        if (str == null || str.trim().isEmpty()) {
            return true;
        }
        return false;
    }

    public static boolean isInteger(String str) {
        try {
            int value = Integer.valueOf(str.trim());
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public static String processFile(String nameFile) {
        String tempName = "";
        if (nameFile != null && !nameFile.equals("")) {
            String[] fileNameArr = nameFile.split("\\.");
            tempName = "." + fileNameArr[1];
        }
        return tempName;
    }

    //<editor-fold defaultstate="collapsed" desc="QuocDM1 151127 getFtpClient">
    /**
     * QuocDM1 151127 getFtpClient
     *
     * @param configName
     * @param current_call_id
     * @return
     * @throws Exception
     */
    //</editor-fold>
    public static ResultBO getFtpClient(long current_call_id) throws Exception {
        ResultBO resultBO = new ResultBO();
        String message = "";
        try {
            //Mo ket noi toi FTP server
            String serverName = ResourceBundleUtil.getResource("ftp_host");
            String userName = com.viettel.security.PassTranformer.decrypt(ResourceBundleUtil.getResource("ftp_username"));
            String password = com.viettel.security.PassTranformer.decrypt(ResourceBundleUtil.getResource("ftp_password"));
            String serverDir = ResourceBundleUtil.getResource("ftp_working_dir");
            int reply;
            boolean success = false;
            FTPClient ftp = new FTPClient();

            log.info(current_call_id + ": " + System.currentTimeMillis() + "Begin Connecting to server: " + serverName);
            ftp.connect(serverName);
            ftp.login(userName, password);
            ftp.enterLocalPassiveMode();
            log.info(current_call_id + ": " + System.currentTimeMillis() + "End connecting to server: " + serverName);
            System.out.println("Connecting to server: " + serverName);
            reply = ftp.getReplyCode();
            log.info(current_call_id + ": " + System.currentTimeMillis() + "Login status: " + reply);
            if (FTPReply.isPositiveCompletion(reply)) {
                log.info(current_call_id + ": " + System.currentTimeMillis() + "Connected to server: " + serverName);
                System.out.println("Connected to server: " + serverName);
                success = ftp.changeWorkingDirectory(serverDir);
                if (success) {
                    resultBO.setResultObject(ftp);
                } else {
                    message = MessageUtils.getMessage("err.uploadFrame.011", location);
                    boolean logout = ftp.logout();
                    if (logout) {
                        log.info(current_call_id + ": " + System.currentTimeMillis() + "Logout from FTP server...");
                    }
                }
            } else {
                message = MessageUtils.getMessage("err.uploadFrame.012", location);
            }
            if (!success) {
                ftp.disconnect();
                log.info(current_call_id + ": " + System.currentTimeMillis() + "Disconnected...");
            }
        } catch (Exception e) {
            log.error("Error: ", e);
            message = MessageUtils.getMessage("err.uploadFrame.009", location);
        }
        resultBO.setMessage(message);
        return resultBO;
    }

    //<editor-fold defaultstate="collapsed" desc="QuocDM1 151127 sendFileToFtpServer">
    /**
     * QuocDM1 151127 sendFileToFtpServer
     *
     * @param file
     * @param fileName
     * @param ftp
     * @param current_call_id
     * @return
     * @throws Exception
     */
    //</editor-fold>
    public static String sendFileToFtpServer(FormFile file, String fileName, FTPClient ftp, long current_call_id) throws Exception {
        String message = "";
        try {
            int reply;
            InputStream fis = null;

            boolean changeType = ftp.setFileType(FTP.BINARY_FILE_TYPE);
            log.info(current_call_id + ": " + System.currentTimeMillis() + "Change file type: " + changeType);
            System.out.println("Change file type: " + changeType);
            fis = file.getInputStream();
            ftp.storeFile(fileName, fis);
            reply = ftp.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                message = "err.uploadFrame.010";
                log.info(current_call_id + ": " + System.currentTimeMillis() + "Upload failed");
            }
            log.info(current_call_id + ": " + System.currentTimeMillis() + "Upload status: " + reply);
            System.out.println("Upload status: " + reply);
            fis.close();

        } catch (Exception e) {
            log.debug("WEB. " + e.toString());
            e.printStackTrace();
            message = "err.uploadFrame.010";
        }
        return message;
    }

    //<editor-fold defaultstate="collapsed" desc="QuocDM1 151127 code lai ham closeFtpConnection">
    /**
     * R8445 closeFtpConnection
     *
     * @param ftp
     * @param current_call_id
     * @throws Exception
     */
    //</editor-fold>
    public static void closeFtpConnection(FTPClient ftp, long current_call_id) throws Exception {
        try {
            boolean logout = ftp.logout();
            if (logout) {
                log.info(current_call_id + ": " + System.currentTimeMillis() + "Logout from FTP server...");
            }
            ftp.disconnect();
            log.info(current_call_id + ": " + System.currentTimeMillis() + "Disconnected...");
        } catch (Exception e) {
            log.debug("WEB. " + e.toString());
            e.printStackTrace();
        }
    }
    // vietnn6
 
    public static List<String> retrieveFileFromFTPServer(String host, String username, String password, String filePath) throws Exception {
        FTPClient client = new FTPClient();
        try {
            // connect, login, set timeout
            client.connect(host);
            client.login(username, password);

            // set transfer type
            client.setFileTransferMode(FTP.BINARY_FILE_TYPE);
            client.setFileType(FTP.BINARY_FILE_TYPE);
            client.enterLocalPassiveMode();

            // set timeout
            client.setSoTimeout(60000); // default 1 minute

            // check connection
            int reply = client.getReplyCode();
            if (FTPReply.isPositiveCompletion(reply)) {
                // switch to working folder
                // create temp file on temp directory

                client.changeWorkingDirectory(filePath);
                // create temp file on temp directory
                File file = new File(filePath);
                List<String> lstData = new ArrayList<String>();
                InputStream inputStream = client.retrieveFileStream(filePath);
                BufferedInputStream bInf = new BufferedInputStream(inputStream);
//                byte[] buffer = new byte[(int) file.length()];
//                bInf.read(buffer);
//                byte[] encode = Base64.encodeBase64(buffer);
//                String strData = new String(encode, Charset.forName("UTF-8"));
//                return strData;

                int sizeSplit = (int) Math.ceil((double) file.length()
                        / (double) 10);
//                int sizeBuffer = 10240;
//                int count = 1, leng;
                byte[] bufferSplit;
//                if (sizeSplit > sizeBuffer) {
//                    bufferSplit = new byte[sizeBuffer];
//                    count = (int) Math.ceil((double) sizeSplit
//                            / (double) sizeBuffer);
//                } else {
                bufferSplit = new byte[sizeSplit];
//                }
//                bInf.read(buffer);
                for (int i = 0; i < 10; i++) {
                    bInf.read(bufferSplit, 0, sizeSplit);
                    byte[] encode = Base64.encodeBase64(bufferSplit);
                    String strData = new String(encode, Charset.forName("UTF-8"));
                    lstData.add(strData);
                }
//                byte[] encode = Base64.encodeBase64(bufferSplit);
//                String strData = new String(encode, Charset.forName("UTF-8"));
                return lstData;
            } else {
                client.disconnect();
                System.err.println("FTP server refused connection !");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            client.disconnect();
        }
        return null;

    }
    
    /**
     * @author duyetdk
     * @param host
     * @param username
     * @param password
     * @param fileName
     * @param destinationDir
     * @param mkDir
     * @return
     * @throws Exception 
     */
    public static boolean putFileToNewFTPServer(String host, String username, String password, String fileName, String destinationDir, String mkDir) throws Exception {
        FTPClient client = new FTPClient();
        FileInputStream fis = null;
        try {
            // connect, login, set timeout
            client.connect(host);
            client.login(username, password);
            // set transfer type
            client.setFileTransferMode(FTP.BINARY_FILE_TYPE);
            client.setFileType(FTP.BINARY_FILE_TYPE);
            client.enterLocalPassiveMode();

            // set timeout
            client.setSoTimeout(60000); // default 1 minute
            client.setConnectTimeout(60000);

            // check connection
            int reply = client.getReplyCode();
            if (FTPReply.isPositiveCompletion(reply)) {
                // switch to working folder                
                client.changeWorkingDirectory(destinationDir);
                if (mkDir != null && !mkDir.isEmpty()) {
                    client.makeDirectory(mkDir);
                    client.changeWorkingDirectory(mkDir);
                }
                reply = client.getReplyCode();
                if (FTPReply.isPositiveCompletion(reply)) {
                    // Create an InputStream of the file to be uploaded
                    File file = new File(fileName);
                    fis = new FileInputStream(file);

                    // Store file on server and logout
                    client.storeFile(file.getName(), fis);

                    // delete temp file
                    file.delete();
                    // close connection
                    client.logout();
                    fis.close();
                    client.disconnect();
                    return true;
                } else {
                    client.disconnect();
                }
            } else {
                client.disconnect();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
