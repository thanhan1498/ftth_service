/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness.common;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

/**
 *
 * @author Anh Nguyen
 */
public class FTPBusinessCR {
    private static Logger logger = Logger.getLogger("loggerAction");

    public static List<String> retrieveFileFromFTPServer(String host, String username, String password, String fileName, String destinationDir, String tempDir) throws Exception {
        FTPClient client = new FTPClient();
        FileInputStream fis = null;
        try {
            // connect, login, set timeout
            client.connect(host);
            client.login(username, password);

            // set transfer type
            client.setFileTransferMode(FTP.BINARY_FILE_TYPE);
            client.setFileType(FTP.BINARY_FILE_TYPE);
            client.enterLocalPassiveMode();

            // set timeout
            client.setSoTimeout(60000); // default 1 minute

            // check connection
            int reply = client.getReplyCode();
            if (FTPReply.isPositiveCompletion(reply)) {
                // switch to working folder
                client.changeWorkingDirectory(destinationDir);
                // create temp file on temp directory
                File tempFile = new File(tempDir + File.separator + fileName);
                List<String> lstData = new ArrayList<String>();

                // retrieve file from server and logout
                OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(tempFile));
                if (client.retrieveFile(destinationDir + File.separator + fileName, outputStream)) {
                    outputStream.close();
                }
                // close connection
                client.logout();
                if (tempFile.isFile()) {
                    InputStream inputStream = new FileInputStream(tempFile);
                    BufferedInputStream bInf = new BufferedInputStream(inputStream);
                    byte[] buffer = new byte[(int) tempFile.length()];
                    bInf.read(buffer);
                    byte[] encode = Base64.encodeBase64(buffer);
                    String strData = new String(encode, Charset.forName("UTF-8"));
                    lstData.add(strData);
                    inputStream.close();
                }
                //delete file
                tempFile.delete();

                // return file
                return lstData;
            } else {
                client.disconnect();
                System.err.println("FTP server refused connection !");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (fis != null) {
                fis.close();
            }
            client.disconnect();
        }
        return null;
    }

    
    /**
     * put file to FTP server
     *
     * @param host
     * @param username
     * @param password
     * @param fileName
     * @param destinationDir
     * @throws Exception
     */

    public static boolean putFileToFTPServer(String host, String username, String password, String fileName, String destinationDir, String mkDir) throws Exception {
        FTPClient client = new FTPClient();
        FileInputStream fis = null;
        try {
            // connect, login, set timeout
            client.connect(host);
            client.login(username, password);
            // set transfer type
            client.setFileTransferMode(FTP.BINARY_FILE_TYPE);
            client.setFileType(FTP.BINARY_FILE_TYPE);
            client.enterLocalPassiveMode();

            // set timeout
            client.setSoTimeout(60000); // default 1 minute

            // check connection
            int reply = client.getReplyCode();
            if (FTPReply.isPositiveCompletion(reply)) {
                // switch to working folder                
                client.changeWorkingDirectory(destinationDir);
                if (mkDir != null && !mkDir.isEmpty()) {
                    client.makeDirectory(mkDir);
                    client.changeWorkingDirectory(mkDir);
                }
                reply = client.getReplyCode();
                if (FTPReply.isPositiveCompletion(reply)) {
                    // Create an InputStream of the file to be uploaded
                    File file = new File(fileName);
                    fis = new FileInputStream(file);

                    // Store file on server and logout
                    client.storeFile(file.getName(), fis);

                    // delete temp file
                    file.delete();
                    // close connection
                    client.logout();
                    fis.close();
                    client.disconnect();
                    return true;
                } else {
                    client.disconnect();
                }
            } else {
                client.disconnect();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
