/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness.common.Object;

/**
 *
 * @author cuongdm
 */
public class InfoCell {

    String cellCode;
    String cellId;
    String btsCode;
    String provinceCode;
    String status4G;
    String imei;
    String mscId;
    Long type;
    boolean isUSIM;

    public boolean isIsUSIM() {
        return isUSIM;
    }

    public void setIsUSIM(boolean isUSIM) {
        this.isUSIM = isUSIM;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getMscId() {
        return mscId;
    }

    public void setMscId(String mscId) {
        this.mscId = mscId;
    }

    public String getStatus4G() {
        return status4G;
    }

    public void setStatus4G(String status4G) {
        this.status4G = status4G;
    }

    public String getBtsCode() {
        return btsCode;
    }

    public void setBtsCode(String btsCode) {
        this.btsCode = btsCode;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCellCode() {
        return cellCode;
    }

    public void setCellCode(String cellCode) {
        this.cellCode = cellCode;
    }

    public String getCellId() {
        return cellId;
    }

    public void setCellId(String cellId) {
        this.cellId = cellId;
    }
}
