/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.bussiness.im;

import com.viettel.bccs.cm.bussiness.BaseBussiness;
import com.viettel.bccs.cm.supplier.im.DebitSupplier;
import com.viettel.brcd.ws.model.output.DebitStaffDetail;
import com.viettel.brcd.ws.model.output.DebitTransHistoryDetail;
import com.viettel.im.database.BO.BankReceipt;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author duyetdk
 */
public class DebitIMBusiness extends BaseBussiness {

    protected DebitSupplier supplier = new DebitSupplier();

    public DebitIMBusiness() {
        logger = Logger.getLogger(DebitIMBusiness.class);
    }

    //<editor-fold defaultstate="collapsed" desc="getListDebitStaff">
    /**
     * @author: duyetdk
     * @since 9/3/2019
     * @des: lay danh sach cong no cua nhan vien
     * @param imPosSession
     * @param staffCode
     * @param shopPath
     * @param status
     * @param staffIdLogin
     * @return 
     */
    public List<DebitStaffDetail> getListDebitStaff(Session imPosSession, String staffCode, String shopPath,Long status, Long staffIdLogin, String currency) {
        List<DebitStaffDetail> result = supplier.getListDebitStaff(imPosSession, staffCode, shopPath, status, staffIdLogin, currency);
        return result;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="countTransHistory">
    /**
     * @author: duyetdk
     * @since 11/3/2019
     * @des: count tong so ban ghi lich su giao dich
     * @param imPosSession
     * @param debitStaffId
     * @param payMethod
     * @param payCode
     * @param status
     * @return 
     */
    public Long countTransHistory(Session imPosSession, Long debitStaffId, String payMethod,
            String payCode, Long status) {
        Long result = supplier.countTransHistory(imPosSession, debitStaffId, payMethod, payCode, status);
        return result;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="transHistory">
    /**
     * @author: duyetdk
     * @since 11/3/2019
     * @des: lay lich su giao dich tung nhan vien
     * @param imPosSession
     * @param debitStaffId
     * @param payMethod
     * @param payCode
     * @param status
     * @param start
     * @param max
     * @return
     */
    public List<DebitTransHistoryDetail> transHistory(Session imPosSession, Long debitStaffId, String payMethod,
            String payCode, Long status, Integer start, Integer max){
        List<DebitTransHistoryDetail> result = supplier.transHistory(imPosSession, debitStaffId, payMethod, payCode, status, start, max);
        return result;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="getListBankReceipt">
    /**
     * @author: duyetdk
     * @since 15/5/2019
     * @des: lay danh sach giao dich dang cho phe duyet
     * @param imPosSession
     * @param status
     * @param staffId
     * @return 
     */
    public List<BankReceipt> getListBankReceipt(Session imPosSession, Long staffId, String status) {
        List<BankReceipt> result = supplier.getListBankReceipt(imPosSession, staffId,status);
        return result;
    }
    //</editor-fold>
    
}
