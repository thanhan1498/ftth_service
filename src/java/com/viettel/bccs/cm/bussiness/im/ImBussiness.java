package com.viettel.bccs.cm.bussiness.im;

import com.viettel.bccs.cm.bussiness.BaseBussiness;
import com.viettel.bccs.cm.supplier.im.ImSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.im.database.BO.AccountAgent;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ImBussiness extends BaseBussiness {

    protected ImSupplier supplier = new ImSupplier();

    public ImBussiness() {
        logger = Logger.getLogger(ImBussiness.class);
    }

    public AccountAgent findAccountAgent(Session imSession, String ownerCode, String serial) {
        AccountAgent result = null;
        if (ownerCode == null || serial == null) {
            return result;
        }
        ownerCode = ownerCode.trim();
        if (ownerCode.isEmpty()) {
            return result;
        }
        serial = serial.trim();
        if (serial.isEmpty()) {
            return result;
        }
        List<AccountAgent> objs = supplier.findAccountAgent(imSession, ownerCode, serial, Constants.STATUS_USE);
        result = (AccountAgent) getBO(objs);
        return result;
    }
}
