package com.viettel.bccs.cm.bussiness.payment;

import com.viettel.bccs.cm.bussiness.BaseBussiness;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.dao.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.dao.payment.SubscriberDAO;
import com.viettel.bccs.cm.model.Contract;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.payment.SubscriberExtend;
import com.viettel.bccs.cm.supplier.ContractSupplier;
import com.viettel.bccs.cm.supplier.payment.PaymentSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.ws.model.output.PayAdvanceBillOut;
import com.viettel.brcd.ws.model.output.PaymentReport;
import com.viettel.brcd.ws.model.output.PaymentReportRevenue;
import com.viettel.brcd.ws.model.output.PaymentReportSubscriber;
import com.viettel.brcd.ws.model.output.UnpaidPaymentDetail;
import com.viettel.brcd.ws.model.output.UnpaidPaymentDetailExpand;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class PaymentBussiness extends BaseBussiness {

    protected PaymentSupplier supplier = new PaymentSupplier();

    public PaymentBussiness() {
        logger = Logger.getLogger(PaymentBussiness.class);
    }

    public String getCustomerDebit(Session paymentSession, Long custId) throws Exception {
        String result = "";
        if (custId == null || custId <= 0L) {
            return result;
        }
        result = supplier.getCustomerDebit(paymentSession, custId);
        return result;
    }

    public boolean validatePermissionReport(Shop shopReporter, String codeReport, Shop shopReport, boolean isChiefCenter) {
        if (shopReporter == null || shopReport == null) {
            return false;
        }
        String pathShopReporter = shopReporter.getShopPath();
        String pathShopReport = shopReport.getShopPath();
        String userLevel = shopReporter.getShopType();
        if (Constants.USER_LEVEL_VTC.equals(userLevel)) {
            return true;
        }
        if (Constants.USER_LEVEL_BRANCH.equals(userLevel)) {
            //nhap gia tri branch nhung ko phai cua minh quan ly
            if (shopReporter.getShopType().equals(shopReport.getShopType())) {
                if (!pathShopReporter.contains(pathShopReport)) {
                    return false;
                }
            }
            //khong nhap gia tri branch
            if (StringUtils.isEmpty(codeReport)) {
                return false;
            }
        }
        if (Constants.USER_LEVEL_SHOWROOM.equals(userLevel)) {
            if (StringUtils.isEmpty(codeReport)) {
                return false;
            }
            //report cap cao hon level cua minh
            if (Integer.valueOf(shopReporter.getShopType()) > Integer.valueOf(shopReport.getShopType())) {
                return false;
            }
            //report ngang level = 3 nhung khong phai truong showroom
            if (Integer.valueOf(shopReporter.getShopType()) == Integer.valueOf(shopReport.getShopType())) {
                if (!pathShopReport.contains(pathShopReporter)) {
                    return false;
                }
                if (pathShopReport.equals(pathShopReporter) && !isChiefCenter) {
                    return false;
                }
            }
        }
        return true;
    }

    public PayAdvanceBillOut payAdvance(Session payment, Session im, Session cmPos, Long reasonId, Long staffId, Long contractId, Long invoiceListId, String isdn, String tid, Double payAmountDis, Double promotionDis) throws Exception {
        return supplier.payAdvance(payment, im, cmPos, reasonId, staffId, contractId, invoiceListId, isdn, tid, payAmountDis, promotionDis);
    }

    //phuonghc 07/07/2020
    public List<PaymentReport> getReportPaymentByUserLevel(Session sessionPayment, String branchCode, String showroomCode, String staffCode) {
        List<PaymentReport> result = new ArrayList<PaymentReport>();
        List<PaymentReportSubscriber> totalSubscriberList = supplier.getTotalSubcribersByUserLevel(sessionPayment, branchCode, showroomCode, staffCode);
        List<PaymentReportSubscriber> totalSubscriberPaidList = supplier.getTotalPaidSubscriberByUserLevel(sessionPayment, branchCode, showroomCode, staffCode);
        List<PaymentReportRevenue> totalRevenueList = supplier.getTotalRevenueByUserLevel(sessionPayment, branchCode, showroomCode, staffCode);
        List<PaymentReportRevenue> totalRevenuePaidList = supplier.getTotalPaidRevenueByUserLevel(sessionPayment, branchCode, showroomCode, staffCode);
        result = bonusInformationForPaymentReport(totalSubscriberList, totalSubscriberPaidList, totalRevenueList, totalRevenuePaidList);
        return result;
    }

    public List<UnpaidPaymentDetailExpand> getListUnpaidDetailByUserLevel(Session sessionCmPos, Session sessionPayment, String branchCode, String showroomCode, String staffCode) {
        List<UnpaidPaymentDetail> contractIdUnpaid = supplier.getListUnpaidDetailByUserLevel(sessionPayment, branchCode, showroomCode, staffCode);
        List<UnpaidPaymentDetailExpand> resultExpand = new ArrayList<UnpaidPaymentDetailExpand>();
        for (UnpaidPaymentDetail element : contractIdUnpaid) {
            List<SubscriberExtend> subscriberExtend = new SubscriberDAO().findByContractIdAll(sessionPayment, element.getContractId());
            if (subscriberExtend == null || subscriberExtend.isEmpty()) {
                logger.warn("### Cannot find subAdslLeaseLine from contractId in table sub_adsl_ll-CM_POS2 with contractId=" + element.getContractId());
                continue;
            }
            SubscriberExtend atem = subscriberExtend.get(0);
            UnpaidPaymentDetailExpand eachUnpaid = new UnpaidPaymentDetailExpand();
            eachUnpaid.setContractId(element.getContractId());
            eachUnpaid.setName(atem.getUserUsing());
            eachUnpaid.setAddress(atem.getDeployAddress());
            eachUnpaid.setAccount(atem.getAccount());
            if (atem.getCustLat() != null || atem.getCustLong() != null) {
                eachUnpaid.setLat(atem.getCustLat().toString());
                eachUnpaid.setLng(atem.getCustLong().toString());
            }
            List<Contract> contractList = new ContractSupplier().findById(sessionCmPos, Long.valueOf(element.getContractId()), null);
            if (contractList != null && !contractList.isEmpty()) {
                try {
                    eachUnpaid.setCustId(contractList.get(0).getCustId().toString());
                } catch (NullPointerException ne) {
                    logger.info("### Cannot find custId from contractId in table Contract-CM_POS2" + ne.getMessage());
                    eachUnpaid.setCustId("");
                }
            }
            String isdn = new ContractSupplier().findIsdnByContractId(sessionCmPos, Long.valueOf(element.getContractId()));
            eachUnpaid.setIsdn(isdn);
            resultExpand.add(eachUnpaid);
        }
        return resultExpand;
    }

    public List<String> getCategorySearchFromShopReport(Session session, Shop shopReport, Staff staffReport) {
        List<String> result = new ArrayList<String>();
        String shopPath = shopReport.getShopPath();
        String[] shopPathSpilit = shopPath.split("_", -1);
        boolean flagShipFirst = true;
        for (String unit : shopPathSpilit) {
            if (StringUtils.isEmpty(unit)) {
                continue;
            }
            if (flagShipFirst) {
                flagShipFirst = false;
                continue;
            }
            String shopCodeName = new ShopDAO().getParShopCodeFromParentShopId(session, unit);
            result.add(shopCodeName);
        }

        if (staffReport != null) {
            result.add(staffReport.getStaffCode() == null ? "" : staffReport.getStaffCode());
        }
        while (result.size() < 3) {
            result.add("");
        }
        return result;
    }

    private List<PaymentReport> bonusInformationForPaymentReport(List<PaymentReportSubscriber> prs, List<PaymentReportSubscriber> prsp, List<PaymentReportRevenue> prr, List<PaymentReportRevenue> prrp) {
        List<PaymentReport> result = new ArrayList<PaymentReport>();
        Map<String, Integer> totalSubscriberMap = convertListToMapSubscriber(prs);
        Map<String, Integer> totalSubscriberPaidMap = convertListToMapSubscriber(prsp);
        Map<String, Double> totalRevenueMap = convertListToMapRevenue(prr);
        Map<String, Double> totalRevenuePaidMap = convertListToMapRevenue(prrp);

        Set<String> unitName = totalSubscriberMap.keySet();
        for (String element : unitName) {
            PaymentReport paymentReport = new PaymentReport();
            paymentReport.setUnitName(element);
            paymentReport.setTotalSubscriber(totalSubscriberMap.get(element));
            paymentReport.setTotalSubscriberPaid(totalSubscriberPaidMap.get(element));
            paymentReport.setTotalRevenue(totalRevenueMap.get(element));
            paymentReport.setTotalRevenuePaid(totalRevenuePaidMap.get(element));
            result.add(paymentReport);
        }
        return result;
    }

    private Map<String, Integer> convertListToMapSubscriber(List<PaymentReportSubscriber> source) {
        Map<String, Integer> result = new HashMap<String, Integer>();
        for (PaymentReportSubscriber element : source) {
            result.put(element.getUnitName(), element.getTotalPerUnit());
        }
        return result;
    }

    private Map<String, Double> convertListToMapRevenue(List<PaymentReportRevenue> source) {
        Map<String, Double> result = new HashMap<String, Double>();
        for (PaymentReportRevenue element : source) {
            result.put(element.getUnitName(), element.getTotalPerUnit());
        }
        return result;
    }
}
