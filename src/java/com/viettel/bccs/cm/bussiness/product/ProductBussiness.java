package com.viettel.bccs.cm.bussiness.product;

import com.viettel.bccs.cm.bussiness.BaseBussiness;
import com.viettel.bccs.cm.supplier.product.ProductSupplier;
import com.viettel.brcd.ws.model.output.Group;
import com.viettel.brcd.ws.model.output.Product;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ProductBussiness extends BaseBussiness {

    protected ProductSupplier supplier = new ProductSupplier();

    public ProductBussiness() {
        logger = Logger.getLogger(ProductBussiness.class);
    }

    public List<Group> getProductGroups(Session productSession, Long telecomServiceId) {
        List<Group> result = null;
        if (telecomServiceId == null || telecomServiceId <= 0L) {
            return result;
        }
        result = supplier.getProductGroups(productSession, telecomServiceId);
        return result;
    }

    public List<Product> getProducts(Session productSession, Long telecomServiceId, String groupProduct) {
        List<Product> result = null;
        result = supplier.getProducts(productSession, telecomServiceId, groupProduct);
        return result;
    }
    
    /**
     * @author duyetdk
     * @since 12/5/2019
     * @des lay danh sach loai thue bao tra truoc
     * @param productSession
     * @param telecomServiceId
     * @param groupProduct
     * @return 
     */
    public List<Product> getListProductPre(Session productSession, Long telecomServiceId, String groupProduct) {
        List<Product> result = null;
        if (telecomServiceId == null || telecomServiceId <= 0L) {
            return result;
        }
        result = supplier.getListProductPre(productSession, telecomServiceId, groupProduct);
        return result;
    }
}
