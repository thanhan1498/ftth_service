/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.bccs.api.Task.BO.SubStockModelRel;
import com.viettel.bccs.api.Task.DAO.SubStockModelRelDAO;
import com.viettel.bccs.api.Util.Constant;
import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.api.supplier.nims.getInfrasGpon.GetPortBySplitterResultForm;
import com.viettel.bccs.api.supplier.nims.getInfrasGpon.GetSplitterBySubNodeResultForm;
import com.viettel.bccs.api.supplier.nims.getInfrasGpon.GetSubNodeByBranchNodeResultForm;
import com.viettel.bccs.cm.bussiness.AccountInfrasBussiness;
import com.viettel.bccs.cm.bussiness.ActionAuditBussiness;
import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.bussiness.TokenBussiness;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.dao.SubActivateAdslLlDAO;
import com.viettel.bccs.cm.dao.SubDeploymentDAO;
import com.viettel.bccs.cm.model.AccountInfra;
import com.viettel.bccs.cm.model.ActionAudit;
import com.viettel.bccs.cm.model.BoxCode;
import com.viettel.bccs.cm.model.FrasUpdateLog;
import com.viettel.bccs.cm.model.LenghtCable;
import com.viettel.bccs.cm.model.LocationODF;
import com.viettel.bccs.cm.model.LocationSN;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubDeployment;
import com.viettel.bccs.cm.model.TechInfo;
import com.viettel.bccs.cm.model.Token;
import com.viettel.bccs.cm.model.portOnBox;
import com.viettel.bccs.cm.supplier.AccountInfrasSupplier;
import com.viettel.bccs.cm.supplier.ServiceSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.output.AccountByStaffOut;
import com.viettel.brcd.ws.model.output.AccountInfrasOut;
import com.viettel.brcd.ws.model.output.TechInforOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.brcd.ws.model.output.getSubscriptionInfoADSLOutPut;
import com.viettel.brcd.ws.model.output.getSubscriptionInfoAONOutPut;
import com.viettel.brcd.ws.model.output.getSubscriptionInfoGponOutPut;
import static com.viettel.brcd.ws.supplier.accountinfras.NimWsGetInfoBusines.getSubscriptionInfoADSL;
import static com.viettel.brcd.ws.supplier.accountinfras.NimWsGetInfoBusines.getSubscriptionInfoAON;
import static com.viettel.brcd.ws.supplier.accountinfras.NimWsGetInfoBusines.getSubscriptionInfoGpon;
import com.viettel.brcd.ws.supplier.nims.sendSubscriptionInfo.SendSubscriptionInfoResponse;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import static com.viettel.im.database.DAO.WebServiceIMUtil.getSequence;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author nhandv
 */
public class AccountInfrasController extends BaseController {
    
    public AccountInfrasController() {
        logger = Logger.getLogger(AccountInfrasController.class);
    }
    
    public AccountByStaffOut searchAccountByStaff(HibernateHelper hibernateHelper,
            String locale, String staffID, String account, String infrasType,
            String infrasStatus,
            String bts, Long pageNo,
            String connectorCode, String deviceCode, String token) {
        AccountByStaffOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            if (staffID == null || staffID.isEmpty() || staffID.equals("?")) {
                result = new AccountByStaffOut(Constants.ERROR_CODE_1, LabelUtil.getKey("staffId.is.required", locale));
                return result;
            }
            
            Token tokenObj = new TokenBussiness().findByToken(cmPosSession, token);
            Long staffIdLogin = null;
            if (tokenObj != null) {
                staffIdLogin = tokenObj.getStaffId();
            }
            /*get info of user*/
            //List<com.viettel.im.database.BO.Staff> staff = new StaffDAO(imPosSession).findByProperty("staffId", staffId);
            if (!staffID.equals(String.valueOf(staffIdLogin))) {
                result = new AccountByStaffOut(Constants.ERROR_CODE_1, LabelUtil.getKey("staff.does.not.exist", locale));
                return result;
            }
            
            StaffDAO staffDAO = new StaffDAO();
            Staff staff = staffDAO.findById(cmPosSession, Long.parseLong(staffID));
            if (staff == null) {
                result = new AccountByStaffOut(Constants.ERROR_CODE_1, LabelUtil.getKey("staff.does.not.exist", locale));
                return result;
            }
            Shop shop = new ShopDAO().findById(cmPosSession, staff.getShopId());
            AccountInfrasBussiness accountInfrasBussiness = new AccountInfrasBussiness();
            Integer count = accountInfrasBussiness.getCountAccount(cmPosSession,
                    staffID, account, infrasType, infrasStatus, bts, staff.getStaffCode(),
                    connectorCode, deviceCode, shop.getShopPath());
            if (count == 0) {
                result = new AccountByStaffOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            Integer start = null;
            Integer max = null;
            if (pageNo != null) {
                start = getStart(Constants.PAGE_SIZE, pageNo);
                max = getMax(Constants.PAGE_SIZE, pageNo, Long.valueOf(count));
            }
            Long totalPage = 0L;
            if (count % Constants.PAGE_SIZE == 0) {
                totalPage = count / Constants.PAGE_SIZE;
            } else if (count % Constants.PAGE_SIZE > 0) {
                totalPage = count / Constants.PAGE_SIZE + 1;
            }
            List<AccountInfra> lst = accountInfrasBussiness.getLstAccountInfra(cmPosSession,
                    staffID, account, infrasType, infrasStatus, bts, staff.getStaffCode(), start, max,
                    connectorCode, deviceCode, shop.getShopPath());
            
            result = new AccountByStaffOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lst);
            result.setTotalPage(totalPage);
            result.setTotalSize(count);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new AccountByStaffOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "searchAccountByStaff:result=" + LogUtils.toJson(result));
        }
    }
    
    public AccountInfrasOut getInfraStructureOfAccount(HibernateHelper hibernateHelper,
            String locale, String stationCode) {
        AccountInfrasOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            if (stationCode == null || stationCode.isEmpty() || stationCode.equals("?")) {
                result = new AccountInfrasOut(Constants.ERROR_CODE_1, LabelUtil.getKey("infras.station.is.required", locale));
                return result;
            }
            result = new AccountInfrasOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            AccountInfrasBussiness accountInfrasBussiness = new AccountInfrasBussiness();
            List<Object> lstobj = accountInfrasBussiness.getInfraStructureOfAccount(cmPosSession, true, stationCode);
            if (lstobj != null) {
                List<GetSubNodeByBranchNodeResultForm> lstSubNode = new ArrayList<GetSubNodeByBranchNodeResultForm>();
                List<GetSplitterBySubNodeResultForm> lstSplitter = new ArrayList<GetSplitterBySubNodeResultForm>();
                List<GetPortBySplitterResultForm> lstPort = new ArrayList<GetPortBySplitterResultForm>();
                for (Object obj : lstobj) {
                    Object[] arr = (Object[]) obj;
                    GetSubNodeByBranchNodeResultForm subNode = new GetSubNodeByBranchNodeResultForm();
                    boolean isHave = false;
                    if (arr[3] != null && arr[2] != null) {
                        subNode.setSubNodeCode(arr[3].toString());
                        subNode.setSubNodeId(Long.parseLong(arr[2].toString()));
                        for (GetSubNodeByBranchNodeResultForm object : lstSubNode) {
                            if (object.getSubNodeCode().equals(subNode.getSubNodeCode())) {
                                isHave = true;
                                break;
                            }
                        }
                        if (!isHave) {
                            lstSubNode.add(subNode);
                        }
                    }
                    
                    GetSplitterBySubNodeResultForm splitter = new GetSplitterBySubNodeResultForm();
                    if (arr[4] != null && arr[5] != null) {
                        splitter.setSplitterCode(arr[4].toString());
                        splitter.setSplitterId(Long.parseLong(arr[5].toString()));
                        splitter.setResult(arr[2].toString());
                        isHave = false;
                        for (GetSplitterBySubNodeResultForm object : lstSplitter) {
                            if (object.getSplitterCode().equals(splitter.getSplitterCode())) {
                                isHave = true;
                                break;
                            }
                        }
                        if (!isHave) {
                            lstSplitter.add(splitter);
                        }
                    }
                    
                    GetPortBySplitterResultForm port = new GetPortBySplitterResultForm();
                    if (arr[6] != null) {
                        port.setPortCode(arr[6].toString());
                        port.setPortId(Long.parseLong(arr[8].toString()));
                        port.setResult(arr[5].toString());
                        isHave = false;
                        for (GetPortBySplitterResultForm object : lstPort) {
                            if (object.getPortCode().equals(port.getPortCode())) {
                                isHave = true;
                                break;
                            }
                        }
                        if (!isHave) {
                            lstPort.add(port);
                        }
                    }
                    
                }
                result.setLstSubBranchGpon(lstSubNode);
                result.setLstSpitterGpon(lstSplitter);
                if (lstPort.size() > 0) {
                    Collections.sort(lstPort, new Comparator<GetPortBySplitterResultForm>() {
                        @Override
                        public int compare(GetPortBySplitterResultForm u1, GetPortBySplitterResultForm u2) {
                            return Long.valueOf(u1.getPortCode()).compareTo(Long.valueOf(u2.getPortCode()));
                        }
                    });
                }
                result.setLstPortGpon(lstPort);
            }
            
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new AccountInfrasOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "getInfraStructureOfAccount:result=" + LogUtils.toJson(result));
        }
    }
    
    public AccountInfrasOut getAccountInfrasGpon(HibernateHelper hibernateHelper,
            String account, String locale) {
        AccountInfrasOut accountInfrasOut = new AccountInfrasOut();
        try {
            
            getSubscriptionInfoGponOutPut gponOutPut = getSubscriptionInfoGpon(account);
            
            if (gponOutPut != null) {
                accountInfrasOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                accountInfrasOut.setErrorDecription(LabelUtil.getKey("common.success", locale));
                accountInfrasOut.setStationCode(gponOutPut.getStationCode());
                accountInfrasOut.setDeviceCode(gponOutPut.getDeviceCode());
                accountInfrasOut.setDevicePort(gponOutPut.getActualPort());
                accountInfrasOut.setPortLogic(gponOutPut.getPortLogic());
                accountInfrasOut.setSnCode(gponOutPut.getNodeTbCode());
                accountInfrasOut.setSplitterCode(gponOutPut.getSpliterCode());
                accountInfrasOut.setInfraType(gponOutPut.getInfraType());
                accountInfrasOut.setSubType(gponOutPut.getSubType());
                accountInfrasOut.setSetupAddress(gponOutPut.getSetupAddress());
                accountInfrasOut.setSubAddress(gponOutPut.getSubAddress());
                accountInfrasOut.setPortId(gponOutPut.getPortId());
                accountInfrasOut.setDeviceId(gponOutPut.getDeviceId());
                accountInfrasOut.setStationId(gponOutPut.getStationId());
                String portCode = getPortSpliiter(hibernateHelper, locale, account);
                String splitterPortID = getPortSpliiterID(hibernateHelper, locale, account);
                if (portCode != null) {
                    accountInfrasOut.setSplitterPort(portCode);
                }
                accountInfrasOut.setSplitterPortID(splitterPortID);
                LocationODF localAccount = getLocationAccount(hibernateHelper, locale, account);
                if (localAccount != null) {
                    accountInfrasOut.setLatX(localAccount.getX() == null ? "0" : localAccount.getX().toString());
                    accountInfrasOut.setLatY(localAccount.getY() == null ? "0" : localAccount.getY().toString());
                }
                LocationSN localSNCode = getLocationSNCode(hibernateHelper, locale,
                        accountInfrasOut.getSnCode());
                if (localSNCode != null) {
                    accountInfrasOut.setLatXSN(localSNCode.getX() == null ? "0" : localSNCode.getX().toString());
                    accountInfrasOut.setLatYSN(localSNCode.getY() == null ? "0" : localSNCode.getY().toString());
                }
            } else {
                accountInfrasOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_FAIL);
            }
            
        } catch (Exception ex) {
            logger.error("getAccountInfrasGpon: " + ex.getMessage());
            accountInfrasOut = new AccountInfrasOut(Constants.ERROR_CODE_1,
                    LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
        }
        return accountInfrasOut;
    }
    
    public AccountInfrasOut getAccountInfrasAON(String account, String locale, HibernateHelper hibernateHelper) {
        AccountInfrasOut accountInfrasOut = new AccountInfrasOut();
        try {
            Session cmPosSession = null;
            getSubscriptionInfoAONOutPut aONOutPut = getSubscriptionInfoAON(account);
            if (aONOutPut != null) {
                
                cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
                SubActivateAdslLlDAO subActivateAdslLlDAO = new SubActivateAdslLlDAO();
                SubAdslLeaseline subAds = subActivateAdslLlDAO.findBySubIdByAccount(cmPosSession, account);
                Long sub_id = subAds.getSubId();
                List<SubDeployment> lstSubDep = new SubDeploymentDAO().findBySubId(cmPosSession, sub_id);
                accountInfrasOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                accountInfrasOut.setErrorDecription(LabelUtil.getKey("common.success", locale));
                accountInfrasOut.setStationCode(aONOutPut.getStationCode());
                accountInfrasOut.setDeviceCode(aONOutPut.getDeviceCode());
                accountInfrasOut.setDevicePort(aONOutPut.getPortCode());
                if (lstSubDep.size() > 0) {
                    accountInfrasOut.setOdfIndoorCode(aONOutPut.getOdfIndoorCode());
                    accountInfrasOut.setOdfIndoorPort(aONOutPut.getCouplerNo());
                    accountInfrasOut.setOdfOutdoorCode(aONOutPut.getOdfCode());
                    accountInfrasOut.setOdfOutdoorPort(aONOutPut.getCouplerNo());
                    accountInfrasOut.setIsSplitter(lstSubDep.get(0).getIsSplitter() == null ? "0" : lstSubDep.get(0).getIsSplitter());
                }
                accountInfrasOut.setSecondCouplerNo(aONOutPut.getSecondCouplerNo());
                accountInfrasOut.setInfraType(aONOutPut.getInfraType());
                accountInfrasOut.setSubType(aONOutPut.getSubType());
                accountInfrasOut.setSetupAddress(aONOutPut.getSetupAddress());
                accountInfrasOut.setSubAddress(aONOutPut.getSubAddress());
                accountInfrasOut.setPortId(aONOutPut.getPortId());
                accountInfrasOut.setDeviceId(aONOutPut.getDeviceId());
                accountInfrasOut.setStationId(aONOutPut.getStationId());
                accountInfrasOut.setOdfIndoorId(aONOutPut.getOdfIndoorId());
                accountInfrasOut.setOdfOutdoorId(aONOutPut.getOdfId());
                LocationODF localAccount = getLocationAccount(hibernateHelper, locale, account);
                if (localAccount != null) {
                    accountInfrasOut.setLatX(localAccount.getX() == null ? "0" : localAccount.getX().toString());
                    accountInfrasOut.setLatY(localAccount.getY() == null ? "0" : localAccount.getY().toString());
                }
                LocationODF locationODF = getLocationODF(hibernateHelper, locale, aONOutPut.getOdfCode(),
                        "", "");
                if (locationODF != null) {
                    accountInfrasOut.setLatXSN(locationODF.getX().toString());
                    accountInfrasOut.setLatYSN(locationODF.getY().toString());
                }
            } else {
                accountInfrasOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_FAIL);
            }
            
        } catch (Exception ex) {
            logger.error("getAccountInfrasGpon: " + ex.getMessage());
            accountInfrasOut = new AccountInfrasOut(Constants.ERROR_CODE_1,
                    LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
        }
        return accountInfrasOut;
    }
    
    public AccountInfrasOut getAccountInfrasADSL(HibernateHelper hibernateHelper, String account,
            String locale) {
        AccountInfrasOut accountInfrasOut = new AccountInfrasOut();
        try {
            
            getSubscriptionInfoADSLOutPut aONOutPut = getSubscriptionInfoADSL(account);
            if (aONOutPut != null) {
                accountInfrasOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                accountInfrasOut.setErrorDecription(LabelUtil.getKey("common.success", locale));
                accountInfrasOut.setStationCode(aONOutPut.getStationCode());
                accountInfrasOut.setDeviceCode(aONOutPut.getDeviceCode());
                accountInfrasOut.setDevicePort(aONOutPut.getActualPort());
                accountInfrasOut.setBoxCableCode(aONOutPut.getBoxCode());
                accountInfrasOut.setInfraType(aONOutPut.getInfraType());
                accountInfrasOut.setSubType(aONOutPut.getSubType());
                accountInfrasOut.setSetupAddress(aONOutPut.getSetupAddress());
                accountInfrasOut.setSubAddress(aONOutPut.getSubAddress());
                accountInfrasOut.setPortId(aONOutPut.getPortId());
                accountInfrasOut.setDeviceId(aONOutPut.getDeviceId());
                accountInfrasOut.setStationId(aONOutPut.getStationId());
                accountInfrasOut.setBoxCablePort(aONOutPut.getPortCode());
                accountInfrasOut.setPortNoMdfA(aONOutPut.getPortNoMdfA());
                accountInfrasOut.setPortNoMdfB(aONOutPut.getPortNoMdfB());
                LocationODF localAccount = getLocationAccount(hibernateHelper, locale, account);
                if (localAccount != null) {
                    accountInfrasOut.setLatX(localAccount.getX() == null ? "0" : localAccount.getX().toString());
                    accountInfrasOut.setLatY(localAccount.getY() == null ? "0" : localAccount.getY().toString());
                }
                LocationODF locationODF = getLocationODF(hibernateHelper, locale, "", aONOutPut.getBoxCode(), account);
                if (locationODF != null) {
                    accountInfrasOut.setLatXSN(locationODF.getX().toString());
                    accountInfrasOut.setLatYSN(locationODF.getY().toString());
                }
            } else {
                accountInfrasOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_FAIL);
            }
            
        } catch (Exception ex) {
            logger.error("getAccountInfrasGpon: " + ex.getMessage());
            accountInfrasOut = new AccountInfrasOut(Constants.ERROR_CODE_1,
                    LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
        }
        return accountInfrasOut;
    }
    
    public AccountInfrasOut getLstCabinet(HibernateHelper hibernateHelper, String stationCode, String locale) {
        AccountInfrasOut accountInfrasOut = new AccountInfrasOut();
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            if (stationCode == null || stationCode.isEmpty() || stationCode.equals("?")) {
                accountInfrasOut = new AccountInfrasOut(Constants.ERROR_CODE_1, LabelUtil.getKey("infras.station.is.required", locale));
                return accountInfrasOut;
            }
            accountInfrasOut = new AccountInfrasOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            AccountInfrasBussiness accountInfrasBussiness = new AccountInfrasBussiness();
            List<BoxCode> lstBoxCode = accountInfrasBussiness.getLstCabinet(cmPosSession, stationCode);
            
            if (!lstBoxCode.isEmpty()) {
                accountInfrasOut.setLstBoxCode(lstBoxCode);
            }
            List<portOnBox> lstPortOnBox = accountInfrasBussiness.getLstCabinetPort(cmPosSession, stationCode);
            if (!lstPortOnBox.isEmpty()) {
                accountInfrasOut.setLstBoxPort(lstPortOnBox);
            }
        } catch (Exception ex) {
            logger.error("getLstCabinet: " + ex.getMessage());
            accountInfrasOut = new AccountInfrasOut(Constants.ERROR_CODE_1,
                    LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "getLstCabinet:result=" + LogUtils.toJson(accountInfrasOut));
        }
        return accountInfrasOut;
    }
    
    public LocationODF getLocationODF(HibernateHelper hibernateHelper,
            String locale, String odfCode, String connectorCode, String accountIsdn) {
        Session cmPosSession = null;
        LocationODF locationODF = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            AccountInfrasBussiness accountInfrasBussiness = new AccountInfrasBussiness();
            locationODF = accountInfrasBussiness.getLocationODF(cmPosSession, odfCode,
                    true, connectorCode, accountIsdn);
        } catch (Exception ex) {
            logger.error("getLocationODF: " + ex.getMessage() + " odfCode:" + odfCode + "connectorCode:" + connectorCode
                    + "accountIsdn" + accountIsdn);
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "getLocationODF:result=" + LogUtils.toJson(locationODF));
        }
        return locationODF;
    }
    
    public LocationODF getLocationAccount(HibernateHelper hibernateHelper,
            String locale, String accountIsdn) {
        Session cmPosSession = null;
        LocationODF locationODF = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            AccountInfrasBussiness accountInfrasBussiness = new AccountInfrasBussiness();
            locationODF = accountInfrasBussiness.getLocationAccount(cmPosSession, accountIsdn);
        } catch (Exception ex) {
            logger.error("getLocationAccount: " + ex.getMessage());
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "getLocationAccount:result=" + LogUtils.toJson(locationODF));
        }
        return locationODF;
    }
    
    public LocationSN getLocationSNCode(HibernateHelper hibernateHelper,
            String locale, String deviceCode) {
        Session cmNimsSession = null;
        LocationSN locationODF = null;
        try {
            cmNimsSession = hibernateHelper.getSession(Constants.SESSION_NIMS);
            AccountInfrasBussiness accountInfrasBussiness = new AccountInfrasBussiness();
            locationODF = accountInfrasBussiness.getLocationSNCode(cmNimsSession, deviceCode);
        } catch (Exception ex) {
            logger.error("getLocationSNCode: " + ex.getMessage());
        } finally {
            closeSessions(cmNimsSession);
            LogUtils.info(logger, "getLocationSNCode:result=" + LogUtils.toJson(locationODF));
        }
        return locationODF;
    }
    
    public String getPortSpliiter(HibernateHelper hibernateHelper,
            String locale, String accountIsdn) {
        Session cmNimsSession = null;
        String portSpliiter = null;
        try {
            cmNimsSession = hibernateHelper.getSession(Constants.SESSION_NIMS);
            AccountInfrasSupplier supplier = new AccountInfrasSupplier();
            portSpliiter = supplier.getPortSpliiter(cmNimsSession, accountIsdn);
        } catch (Exception ex) {
            logger.error("getPortSpliiter: " + ex.getMessage());
        } finally {
            closeSessions(cmNimsSession);
            LogUtils.info(logger, "getPortSpliiter:result=" + portSpliiter);
        }
        return portSpliiter;
    }

    public String getPortSpliiterID(HibernateHelper hibernateHelper,
            String locale, String accountIsdn) {
        Session cmNimsSession = null;
        String portSpliiter = null;
        try {
            cmNimsSession = hibernateHelper.getSession(Constants.SESSION_NIMS);
            AccountInfrasSupplier supplier = new AccountInfrasSupplier();
            portSpliiter = supplier.getPortSpliiterID(cmNimsSession, accountIsdn);
        } catch (Exception ex) {
            logger.error("getPortSpliiterID: " + ex.getMessage());
        } finally {
            closeSessions(cmNimsSession);
            LogUtils.info(logger, "getPortSpliiterID:result=" + portSpliiter);
        }
        return portSpliiter;
    }
    
    public AccountInfrasOut getLenghtCable(HibernateHelper hibernateHelper,
            String account, String locale, AccountInfrasOut accountInfrasOut) {
        Session cmNimsSession = null;
        try {
            cmNimsSession = hibernateHelper.getSession(Constants.SESSION_NIMS);
            AccountInfrasBussiness accountInfrasBussiness = new AccountInfrasBussiness();
            List<LenghtCable> lstBoxCode = accountInfrasBussiness.getLenghtCable(cmNimsSession, account);
            if (!lstBoxCode.isEmpty()) {
                accountInfrasOut.setLengthCable(lstBoxCode.get(0).getLenghtCable() == null ? "0" : lstBoxCode.get(0).getLenghtCable());
            } else {
                accountInfrasOut.setLengthCable("0");
            }
        } catch (Exception ex) {
            logger.error("getLenghtCable: " + ex.getMessage());
            accountInfrasOut = new AccountInfrasOut(Constants.ERROR_CODE_1,
                    LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
        } finally {
            closeSessions(cmNimsSession);
            LogUtils.info(logger, "getLenghtCable:result=" + LogUtils.toJson(accountInfrasOut));
        }
        return accountInfrasOut;
    }
    
    public WSRespone UpdateAccountByStaffInfras(HibernateHelper hibernateHelper, String locale,
            String account,
            String staffId,
            String odfIndoorCode, Long odfIndoorPort, Long odfIndoorPort2,
            String odfOutdoorCode, Long odfOutdoorPort, Long odfOutdoorPort2,
            Double lat, Double longy, Long lengthCable, Long portLogic, String snCode,
            String splitterCode, Long splitterPort, String boxCableCode,
            String boxCablePort, String stationCode, String infraType,
            Long deviceId, Long portId) {//@duyetdk: them tham so @deviceId, @portId
        WSRespone wSRespone = new WSRespone();
        Session cmPosSession = null;
        Session cmNimsSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmNimsSession = hibernateHelper.getSession(Constants.SESSION_NIMS);
            AccountInfrasBussiness accountInfrasBussiness = new AccountInfrasBussiness();
            boolean checkAccountByStaff = accountInfrasBussiness.checkAccountByStaff(cmPosSession,
                    account, staffId);
            AccountInfrasOut accountInfrasOut = null;
            StaffDAO staffDAO = new StaffDAO();
            Staff staff = staffDAO.findById(cmPosSession, Long.parseLong(staffId));
            Long stationID = Common.getStationId(cmNimsSession, stationCode);
            if (staff == null) {
                wSRespone = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("staff.does.not.exist", locale));
                return wSRespone;
            }
            if (stationID == null) {
                wSRespone = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("infras.station.is.required", locale));
                return wSRespone;
            }
            HashMap paramTr = new HashMap();
            paramTr.put("locale", locale);
            paramTr.put("account", account);
            paramTr.put("infraType", infraType);
            paramTr.put("staffId", staffId);
            paramTr.put("odfIndoorCode", odfIndoorCode);
            paramTr.put("odfIndoorPort", odfIndoorPort);
            paramTr.put("odfIndoorPort2", odfIndoorPort2);
            paramTr.put("odfOutdoorCode", odfOutdoorCode);
            paramTr.put("odfOutdoorPort", odfOutdoorPort);
            paramTr.put("odfOutdoorPort2", odfOutdoorPort2);
            paramTr.put("lat", lat);
            paramTr.put("longy", longy);
            paramTr.put("lengthCable", lengthCable);
            paramTr.put("portLogic", portLogic);
            paramTr.put("snCode", snCode);
            paramTr.put("splitterCode", splitterCode);
            paramTr.put("splitterPort", splitterPort);
            paramTr.put("boxCableCode", boxCableCode);
            paramTr.put("boxCablePort", boxCablePort);
            paramTr.put("stationCode", stationCode);
            logger.error("UpdateAccountByStaffInfras: " + LogUtils.toJson(paramTr));
            
            if (!checkAccountByStaff) {
                wSRespone = new WSRespone(Constants.ERROR_CODE_1,
                        LabelUtil.formatKey("infras.account.staff.is.required", locale));
                return wSRespone;
            }
            
            SubActivateAdslLlDAO subActivateAdslLlDAO = new SubActivateAdslLlDAO();
            SubAdslLeaseline subAds = subActivateAdslLlDAO.findBySubIdByAccount(cmPosSession, account);
            if (subAds == null) {
                wSRespone = new WSRespone(Constants.ERROR_CODE_1,
                        LabelUtil.formatKey("infras.subadsl.is.required", locale));
                return wSRespone;
            }
            if(subAds.getDeployAddress() == null || subAds.getDeployAddress().isEmpty()){
                subAds.setDeployAddress(subAds.getAddress());
                cmPosSession.save(subAds);
                cmPosSession.flush();
            }
            if (lat != null && longy != null) {
                LocationODF localAccount = getLocationAccount(hibernateHelper, locale, account);
                accountInfrasBussiness.updateSub_ADSL_ll(cmPosSession, lat, longy, account);
                wSRespone.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                wSRespone.setErrorDecription(LabelUtil.getKey("common.success", locale));

                // update log
                FrasUpdateLog frasUpdateLog = new FrasUpdateLog();
                frasUpdateLog.setAccount(account);
                frasUpdateLog.setInsertDate(new Date());
                frasUpdateLog.setSubID(String.valueOf(subAds.getSubId()));
                frasUpdateLog.setStaffID(Long.valueOf(staffId));
                frasUpdateLog.setStaffName("");
                frasUpdateLog.setDescription("success");
                frasUpdateLog.setStationCode(stationCode);
                frasUpdateLog.setStatus(0L);
                frasUpdateLog.setFrasType(infraType);
                frasUpdateLog.setDeviceCode("");
                frasUpdateLog.setErrorCode("");
                frasUpdateLog.setStationID(stationID);
                // update lat
                frasUpdateLog.setValueType("x");
                frasUpdateLog.setOldValue(localAccount.getX() == null ? "" : localAccount.getX().toString());
                frasUpdateLog.setNewValue(String.valueOf(lat));
                new AccountInfrasBussiness().insertFrasUpdateLog(cmPosSession, frasUpdateLog);
                logger.error("UpdateAccountByStaffInfras: " + LogUtils.toJson(frasUpdateLog));
                // update long
                frasUpdateLog.setValueType("y");
                frasUpdateLog.setOldValue(localAccount.getY() == null ? "" : localAccount.getY().toString());
                frasUpdateLog.setNewValue(String.valueOf(longy));
                logger.error("UpdateAccountByStaffInfras_X_Y: " + LogUtils.toJson(frasUpdateLog));
                new AccountInfrasBussiness().insertFrasUpdateLog(cmPosSession, frasUpdateLog);
                return wSRespone;
            }
            
            SendSubscriptionInfoResponse responseSubInfo = null;
            StringBuilder requestNims = new StringBuilder();
            ServiceSupplier serviceSupplier = new ServiceSupplier();
            Long sub_id = subAds.getSubId();
            List<SubDeployment> lstSubDep = new SubDeploymentDAO().findBySubId(cmPosSession, sub_id);
            if (lstSubDep == null || lstSubDep.isEmpty()) {
                wSRespone.setErrorDecription("This account have no infrastructure");
                return wSRespone;
            }
            if (infraType.equals("GPON")) {
                if (snCode == null || snCode.isEmpty() || snCode.equals("?")) {
                    wSRespone = new WSRespone(Constants.ERROR_CODE_1,
                            LabelUtil.formatKey("infras.sncode.is.required", locale));
                    return wSRespone;
                }
                if (splitterCode == null || splitterCode.isEmpty() || splitterCode.equals("?")) {
                    wSRespone = new WSRespone(Constants.ERROR_CODE_1,
                            LabelUtil.formatKey("infras.sncode.is.required", locale));
                    return wSRespone;
                }
                if (portLogic == null) {
                    wSRespone = new WSRespone(Constants.ERROR_CODE_1,
                            LabelUtil.formatKey("infras.portlogic.is.required", locale));
                    return wSRespone;
                }
                if (splitterPort == null) {
                    wSRespone = new WSRespone(Constants.ERROR_CODE_1,
                            LabelUtil.formatKey("infras.splitterport.is.required", locale));
                    return wSRespone;
                }
                accountInfrasOut = new AccountInfrasController().getAccountInfrasGpon(hibernateHelper, account, locale);
                List<SubStockModelRel> lstSubStockModelRel = new SubStockModelRelDAO().findBySubId(subAds.getSubId(), cmPosSession);
                if (lstSubStockModelRel.size() > 0) {
                    logger.error("serial:result=OK" + lstSubStockModelRel.get(0).getSerialGpon() + "subID=" + subAds.getSubId());
                    responseSubInfo = serviceSupplier.sendNimsUpdateInfrasByStaffGPON(account,
                            lengthCable, portLogic, snCode, splitterCode, splitterPort,
                            infraType,
                            accountInfrasOut.getSubType(), subAds.getDeployAddress(),
                            subAds.getDeployAddress(), accountInfrasOut.getPortId(),
                            accountInfrasOut.getDeviceId(),
                            accountInfrasOut.getStationId(),
                            requestNims, lstSubStockModelRel.get(0).getSerialGpon());
                    
                } else {
                    wSRespone = new WSRespone(Constants.ERROR_CODE_1,
                            LabelUtil.formatKey("infras.notfound.substockmodel", locale));
                    return wSRespone;
                }
            } else if (infraType.equals("FCN") || infraType.equals("AON")) {
                if (odfIndoorCode == null || odfIndoorCode.isEmpty() || odfIndoorCode.equals("?")) {
                    wSRespone = new WSRespone(Constants.ERROR_CODE_1,
                            LabelUtil.formatKey("infras.odfindoorcode.is.required", locale));
                    return wSRespone;
                }
                if (odfIndoorPort == null) {
                    wSRespone = new WSRespone(Constants.ERROR_CODE_1,
                            LabelUtil.formatKey("infras.odfinportcode.is.required", locale));
                    return wSRespone;
                }
                AccountInfrasSupplier accountInfrasSupplier = new AccountInfrasSupplier();
                Long odfID = 0L;
                if (odfOutdoorCode != null && !odfOutdoorCode.isEmpty() && !odfOutdoorCode.equals("?")) {
                    odfID = accountInfrasSupplier.getOdfID(cmPosSession, odfOutdoorCode);
                } else {
                    odfID = accountInfrasSupplier.getOdfID(cmPosSession, odfIndoorCode);
                }
                logger.error("odfID:result=OK" + odfID);
                accountInfrasOut = new AccountInfrasController().getAccountInfrasAON(account, locale, hibernateHelper);
                //@duyetdk: set lai gtri @deviceId, @portId thay doi
                if (deviceId != null) {
                    accountInfrasOut.setDeviceId(deviceId);
                }
                if (portId != null) {
                    accountInfrasOut.setPortId(String.valueOf(portId));
                }
                responseSubInfo = serviceSupplier.sendNimsUpdateInfrasByStaff(account, odfID,
                        odfIndoorPort, odfIndoorPort2,
                        odfID, odfOutdoorPort, odfOutdoorPort2,
                        lengthCable, portLogic, snCode, splitterCode, splitterPort,
                        boxCableCode, boxCablePort, "FCN",
                        accountInfrasOut.getSubType(), accountInfrasOut.getSetupAddress(),
                        accountInfrasOut.getSubAddress(), accountInfrasOut.getPortId(),
                        accountInfrasOut.getDeviceId(), accountInfrasOut.getStationId(), null, null,
                        requestNims);
            } else if (infraType.equals("CCN")) {
                if (boxCableCode == null || boxCableCode.isEmpty() || boxCableCode.equals("?")) {
                    wSRespone = new WSRespone(Constants.ERROR_CODE_1,
                            LabelUtil.formatKey("infras.boxcode.is.required", locale));
                    return wSRespone;
                }
                if (boxCablePort == null || boxCablePort.isEmpty() || boxCablePort.equals("?")) {
                    wSRespone = new WSRespone(Constants.ERROR_CODE_1,
                            LabelUtil.formatKey("infras.portonbox.is.required", locale));
                    return wSRespone;
                }
                accountInfrasOut = new AccountInfrasController().getAccountInfrasADSL(hibernateHelper, account, locale);
                responseSubInfo = serviceSupplier.sendNimsUpdateInfrasByStaff(account, null,
                        odfIndoorPort, null,
                        null, odfOutdoorPort, null,
                        lengthCable, portLogic, snCode, splitterCode, splitterPort,
                        boxCableCode, boxCablePort, infraType,
                        accountInfrasOut.getSubType(), accountInfrasOut.getSetupAddress(),
                        accountInfrasOut.getSubAddress(), accountInfrasOut.getPortId(),
                        accountInfrasOut.getDeviceId(), accountInfrasOut.getStationId(), accountInfrasOut.getPortNoMdfA(),
                        accountInfrasOut.getPortNoMdfB(),
                        requestNims);
            }
            logger.error("sendNimsUpdateInfrasByStaff:result=OK");
            // send nim
            if ((responseSubInfo == null || (responseSubInfo.getReturn() != null && responseSubInfo.getReturn().getResult().equals(Constant.INFRAS_RESULT_NOK)))) {
                wSRespone = new WSRespone(Constants.ERROR_CODE_1,
                        LabelUtil.formatKey("a.error.has.occurred", locale, responseSubInfo.getReturn().getMessage()));
                return wSRespone;
            }
            wSRespone.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
            wSRespone.setErrorDecription(LabelUtil.getKey("common.success", locale));
            SubDeployment newSub = new SubDeployment();
            lstSubDep.get(0).copySubDeployment(newSub);
            lstSubDep.get(0).setStatus(Constant.STATUS_NOT_USE);
            newSub.setSubdeployId(getSequence(cmPosSession, Constants.SUB_DEPLOYMENT_SEQ));
            newSub.setCreateDate(new Date());
            newSub.setPortLogic(portLogic);
            if (infraType.equals("GPON")) {
                Long snID = Common.getInraDeviceIdFromCode(cmNimsSession, snCode);
                newSub.setCableBoxId(snID);
                Long splID = Common.getInraDeviceIdFromCode(cmNimsSession, splitterCode);
                newSub.setSplitterId(splID);
                //@duyetdk: setter aub_adsl_ll
                subAds.setCableBoxId(splID);
            } else if (infraType.equals("CCN")) {
                Long cableBoxId = Common.getInraDeviceIdFromCode(cmNimsSession, boxCableCode);
                newSub.setCableBoxId(cableBoxId);
                //@duyetdk: setter aub_adsl_ll
                subAds.setCableBoxId(cableBoxId);
            } else if (infraType.equals("FCN") || infraType.equals("AON")) {
                if (odfOutdoorCode != null && !odfOutdoorCode.isEmpty() && !odfOutdoorCode.equals("?")) {
                    AccountInfrasSupplier accountInfrasSupplier = new AccountInfrasSupplier();
                    Long odfID = accountInfrasSupplier.getOdfID(cmPosSession, odfOutdoorCode);
                    Long odfIndoorID = accountInfrasSupplier.getOdfID(cmPosSession, odfIndoorCode);
                    newSub.setIsSplitter("1");
                    if (odfID != null) {
                        newSub.setCableBoxId(odfID);
                        //@duyetdk: setter aub_adsl_ll
                        subAds.setCableBoxId(odfID);
                    }
                    if (odfIndoorID != null) {
                        //@duyetdk: setter aub_adsl_ll
                        newSub.setBoardId(odfIndoorID);
                        subAds.setBoardId(odfIndoorID);
                    }
                    
                } else {
                    AccountInfrasSupplier accountInfrasSupplier = new AccountInfrasSupplier();
                    Long odfID = accountInfrasSupplier.getOdfID(cmPosSession, odfIndoorCode);
                    if (odfID != null) {
                        newSub.setBoardId(odfID);
                        //@duyetdk: setter aub_adsl_ll
                        subAds.setBoardId(odfID);
                    }
                    newSub.setIsSplitter("");
                }
            }
            
            newSub.setStationCode(stationCode);
            //@duyetdk: update sub_deployment
            newSub.setPortNo(portId);
            newSub.setStationId(stationID);
            newSub.setDslamId(deviceId);
            cmPosSession.save(newSub);
            cmPosSession.save(lstSubDep.get(0));
            //@duyetdk: updata sub_adsl_ll -- start
            //@duyetdk: setter aub_adsl_ll
            subAds.setDslamId(deviceId);
            subAds.setStationId(stationID);
            subAds.setPortNo(String.valueOf(portId));
            subAds.setChangeDatetime(new Date());
            cmPosSession.update(subAds);
            cmPosSession.flush();
            //@duyetdk: end
            commitTransactions(cmPosSession);
            Date nowDate = new Date();
            ShopDAO shopDAO = new ShopDAO();
            Shop shop = shopDAO.findById(cmPosSession, staff.getShopId());
            ActionAudit action = new ActionAuditBussiness().insert(cmPosSession, nowDate,
                    com.viettel.bccs.cm.common.util.Constant.ACTION_ADD_DEPOSIT, null, shop.getShopCode(),
                    staff.getStaffCode(), com.viettel.bccs.cm.common.util.Constant.ACTION_AUDIT_PK_TYPE_SUBSCRIBER,
                    sub_id, "mBCCS", "updateInfras");
            new ActionDetailBussiness().insert(cmPosSession, action.getActionAuditId(), "SUB_ADSL_LL", sub_id, "updateInfras", null, 0, nowDate);
            
            FrasUpdateLog frasUpdateLog = new FrasUpdateLog();
            frasUpdateLog.setAccount(account);
            frasUpdateLog.setInsertDate(new Date());
            frasUpdateLog.setSubID(String.valueOf(sub_id));
            frasUpdateLog.setStaffID(Long.valueOf(staffId));
            frasUpdateLog.setDescription(responseSubInfo.getReturn().getResult());
            frasUpdateLog.setStationCode(stationCode);
            frasUpdateLog.setStatus(0L);
            frasUpdateLog.setDeviceCode(accountInfrasOut.getDeviceCode());
            frasUpdateLog.setFrasType(infraType);
            frasUpdateLog.setStaffName("");
            frasUpdateLog.setErrorCode("");
            frasUpdateLog.setStationID(stationID);
            
            if (infraType.equals("GPON")) {
                
                if (snCode != null && !snCode.equals("?") && !snCode.isEmpty()) {
                    frasUpdateLog.setValueType("snCode");
                    frasUpdateLog.setOldValue(accountInfrasOut.getSnCode());
                    frasUpdateLog.setNewValue(snCode);
                    new AccountInfrasBussiness().insertFrasUpdateLog(cmPosSession, frasUpdateLog);
                    
                }
                
                if (splitterCode != null && !splitterCode.equals("?") && !splitterCode.isEmpty()) {
                    frasUpdateLog.setValueType("splitterCode");
                    frasUpdateLog.setOldValue(accountInfrasOut.getSplitterCode());
                    frasUpdateLog.setNewValue(splitterCode);
                    new AccountInfrasBussiness().insertFrasUpdateLog(cmPosSession, frasUpdateLog);
                }
                if (portLogic != null && !portLogic.toString().equals("?")) {
                    frasUpdateLog.setValueType("portLogic");
                    frasUpdateLog.setOldValue(accountInfrasOut.getPortLogic());
                    frasUpdateLog.setNewValue(String.valueOf(portLogic));
                    new AccountInfrasBussiness().insertFrasUpdateLog(cmPosSession, frasUpdateLog);
                }
                if (splitterPort != null) {
                    
                    frasUpdateLog.setValueType("splitterPort");
                    frasUpdateLog.setOldValue(accountInfrasOut.getSplitterPort());
                    frasUpdateLog.setNewValue(String.valueOf(splitterPort));
                    new AccountInfrasBussiness().insertFrasUpdateLog(cmPosSession, frasUpdateLog);
                }
                if (lengthCable != null && !lengthCable.toString().equals("?")) {
                    frasUpdateLog.setValueType("lengthCable");
                    frasUpdateLog.setOldValue(accountInfrasOut.getLengthCable());
                    frasUpdateLog.setNewValue(String.valueOf(lengthCable));
                    new AccountInfrasBussiness().insertFrasUpdateLog(cmPosSession, frasUpdateLog);
                }
            } else if (infraType.equals("FCN") || infraType.equals("AON")) {
                if (odfIndoorCode != null && !odfIndoorCode.equals("?") && !odfIndoorCode.isEmpty()) {
                    
                    frasUpdateLog.setValueType("odfIndoorCode");
                    frasUpdateLog.setOldValue(accountInfrasOut.getOdfIndoorCode());
                    frasUpdateLog.setNewValue(odfIndoorCode);
                    new AccountInfrasBussiness().insertFrasUpdateLog(cmPosSession, frasUpdateLog);
                }
                if (odfIndoorPort != null) {
                    frasUpdateLog.setValueType("odfIndoorPort");
                    frasUpdateLog.setOldValue(accountInfrasOut.getOdfIndoorPort());
                    frasUpdateLog.setNewValue(odfIndoorPort != null ? String.valueOf(odfIndoorPort) : null);
                    new AccountInfrasBussiness().insertFrasUpdateLog(cmPosSession, frasUpdateLog);
                }
                if (odfOutdoorCode != null && !odfOutdoorCode.equals("?") && !odfOutdoorCode.isEmpty()) {
                    frasUpdateLog.setValueType("odfOutdoorCode");
                    frasUpdateLog.setOldValue(accountInfrasOut.getOdfOutdoorCode());
                    frasUpdateLog.setNewValue(odfOutdoorCode);
                    new AccountInfrasBussiness().insertFrasUpdateLog(cmPosSession, frasUpdateLog);
                }
                if (odfOutdoorPort != null) {
                    frasUpdateLog.setValueType("odfOutdoorPort");
                    frasUpdateLog.setOldValue(accountInfrasOut.getOdfOutdoorPort());
                    frasUpdateLog.setNewValue(odfOutdoorPort != null ? String.valueOf(odfOutdoorPort) : null);
                    new AccountInfrasBussiness().insertFrasUpdateLog(cmPosSession, frasUpdateLog);
                }
                if (lengthCable != null && !lengthCable.toString().equals("?")) {
                    frasUpdateLog.setValueType("lengthCable");
                    frasUpdateLog.setOldValue(accountInfrasOut.getLengthCable());
                    frasUpdateLog.setNewValue(String.valueOf(lengthCable));
                    new AccountInfrasBussiness().insertFrasUpdateLog(cmPosSession, frasUpdateLog);
                }
                
            } else if (infraType.equals("CCN")) {
                if (boxCableCode != null && !boxCableCode.equals("?") && !boxCableCode.isEmpty()) {
                    frasUpdateLog.setValueType("boxCableCode");
                    frasUpdateLog.setOldValue(accountInfrasOut.getBoxCableCode());
                    frasUpdateLog.setNewValue(boxCableCode);
                    new AccountInfrasBussiness().insertFrasUpdateLog(cmPosSession, frasUpdateLog);
                }
                if (boxCablePort != null && !boxCablePort.equals("?") && !boxCablePort.isEmpty()) {
                    frasUpdateLog.setValueType("boxCablePort");
                    frasUpdateLog.setOldValue(accountInfrasOut.getBoxCablePort());
                    frasUpdateLog.setNewValue(boxCablePort);
                    new AccountInfrasBussiness().insertFrasUpdateLog(cmPosSession, frasUpdateLog);
                }
                if (lengthCable != null && !lengthCable.toString().equals("?")) {
                    frasUpdateLog.setValueType("lengthCable");
                    frasUpdateLog.setOldValue(accountInfrasOut.getLengthCable());
                    frasUpdateLog.setNewValue(String.valueOf(lengthCable));
                    new AccountInfrasBussiness().insertFrasUpdateLog(cmPosSession, frasUpdateLog);
                }
            }
        } catch (Exception ex) {
            logger.error("UpdateAccountByStaffInfras: " + ex.getMessage());
            wSRespone = new WSRespone(Constants.ERROR_CODE_1,
                    LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
        } finally {
            closeSessions(cmPosSession);
            closeSessions(cmNimsSession);
            LogUtils.info(logger, "UpdateAccountByStaffInfras:result=" + LogUtils.toJson(wSRespone));
        }
        return wSRespone;
    }
    
    /**
     * @author duyetdk
     * @des lay ds connector gan theo tram
     * @param hibernateHelper
     * @param token
     * @param locale
     * @param stationId
     * @param stationCode
     * @return 
     */
    public TechInforOut getAllConnectorOfStation(HibernateHelper hibernateHelper, String token, String locale, 
            Long stationId, String stationCode){
        TechInforOut result = null;
        Session nimsSession = null;
        try {
            nimsSession = hibernateHelper.getSession(Constants.SESSION_NIMS);
            if (stationCode == null || stationCode.trim().isEmpty() || stationCode.equals("?")) {
                result = new TechInforOut(Constants.ERROR_CODE_1, "Station code is required");
                return result;
            }
            AccountInfrasBussiness accountInfrasBussiness = new AccountInfrasBussiness();
            List<TechInfo> lst = accountInfrasBussiness.getAllConnectorOfStation(nimsSession, null, stationCode);
            if (lst == null || lst.isEmpty()) {
                result = new TechInforOut(Constants.ERROR_CODE_1, "Not found data");
                return result;
            }
            result = new TechInforOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lst);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new TechInforOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(nimsSession);
            LogUtils.info(logger, "getAllConnectorOfStation:result=" + LogUtils.toJson(result));
        }
     }
}
