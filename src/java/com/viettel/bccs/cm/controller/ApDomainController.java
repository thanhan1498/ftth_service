package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.ApDomainBussiness;
import com.viettel.bccs.cm.bussiness.BusTypeBussiness;
import com.viettel.bccs.cm.bussiness.BusTypeIdRequireBussiness;
import com.viettel.bccs.cm.bussiness.common.CacheBO;
import com.viettel.bccs.cm.model.ApDomain;
import com.viettel.bccs.cm.model.BusType;
import com.viettel.bccs.cm.model.BusTypeIdRequire;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.model.output.ApDomainOut;
import com.viettel.brcd.ws.model.output.Deposit;
import com.viettel.brcd.ws.model.output.DepositOut;
import com.viettel.brcd.ws.model.output.IdType;
import com.viettel.brcd.ws.model.output.IdTypeOut;
import com.viettel.brcd.ws.model.output.LineTypeOut;
import com.viettel.brcd.ws.model.output.Payment;
import com.viettel.brcd.ws.model.output.PaymentOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ApDomainController extends BaseController {

    public ApDomainController() {
        logger = Logger.getLogger(ApDomainController.class);
    }

    public List<Payment> getPaymethods(Session cmPosSession) {
        List<Payment> result = null;
        List<ApDomain> domains = new ApDomainBussiness().findByType(cmPosSession, Constants.AP_DOMAIN_TYPE_PAY_METHOD);
        if (domains == null || domains.isEmpty()) {
            return result;
        }
        result = new ArrayList<Payment>();
        for (ApDomain domain : domains) {
            if (domain != null) {
                result.add(new Payment(domain));
            }
        }
        return result;
    }

    public PaymentOut getPaymethods(HibernateHelper hibernateHelper, String locale) {
        PaymentOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Payment> paymethods = getPaymethods(cmPosSession);
            result = new PaymentOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), paymethods);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new PaymentOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ApDomainController.getPaymethods:result=" + LogUtils.toJson(result));
        }
    }

    public List<Deposit> getDeposits(Session cmPosSession) {
        List<Deposit> result = null;
        List<ApDomain> domains = new ApDomainBussiness().findByType(cmPosSession, Constants.AP_DOMAIN_TYPE_DEPOSIT_TYPE_VALUE);
        if (domains == null || domains.isEmpty()) {
            return result;
        }
        result = new ArrayList<Deposit>();
        for (ApDomain domain : domains) {
            if (domain != null) {
                result.add(new Deposit(domain));
            }
        }
        return result;
    }

    public DepositOut getDeposits(HibernateHelper hibernateHelper, String locale) {
        DepositOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Deposit> deposits = getDeposits(cmPosSession);
            result = new DepositOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), deposits);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new DepositOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ApDomainController.getDeposits:result=" + LogUtils.toJson(result));
        }
    }

    public List<IdType> getIdTypes(Session cmPosSession, String busType) {
        List<IdType> result = null;
        BusType bus = new BusTypeBussiness().findById(cmPosSession, busType);
        boolean idRequired = bus != null && Constants.IS_REQUIRED.equals(bus.getIdRequired());
        List<BusTypeIdRequire> idRequires = null;
        if (idRequired) {
            idRequires = new BusTypeIdRequireBussiness().findByBusType(cmPosSession, busType);
            if (idRequires == null || idRequires.isEmpty()) {
                return result;
            }
        }
        List<ApDomain> domains = new ApDomainBussiness().findByType(cmPosSession, Constants.AP_DOMAIN_TYPE_PERSONAL_DOC);
        if (domains == null || domains.isEmpty()) {
            return result;
        }
        result = new ArrayList<IdType>();
        for (ApDomain domain : domains) {
            if (domain != null) {
                String code = domain.getCode();
                if (code != null) {
                    if (idRequired) {
                        if (new BusTypeIdRequireBussiness().isExists(idRequires, code)) {
                            result.add(new IdType(domain));
                        }
                    } else {
                        result.add(new IdType(domain));
                    }
                }
            }
        }
        return result;
    }

    public IdTypeOut getIdTypes(HibernateHelper hibernateHelper, String busType, String locale) {
        IdTypeOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            //List<IdType> idTypes = getIdTypes(cmPosSession, busType);
            List<IdType> idTypes = CacheBO.getIdType(cmPosSession, busType);
            result = new IdTypeOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), idTypes);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new IdTypeOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ApDomainController.getIdTypes:result=" + LogUtils.toJson(result));
        }
    }

    public LineTypeOut getLineTypes(HibernateHelper hibernateHelper, String serviceType, String locale) {
        LineTypeOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<ApDomain> lineTypes = new ApDomainBussiness().getLineTypes(cmPosSession, serviceType);
            result = new LineTypeOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lineTypes);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new LineTypeOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ApDomainController.getLineTypes:result=" + LogUtils.toJson(result));
        }
    }

    public ApDomainOut getNoticeCharges(HibernateHelper hibernateHelper, String locale) {
        ApDomainOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<ApDomain> noticeCharges = new ApDomainBussiness().findByType(cmPosSession, Constants.AP_DOMAIN_TYPE_NOTICE_CHARGE_TYPE);
            result = new ApDomainOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), noticeCharges);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ApDomainOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ApDomainController.getNoticeCharges:result=" + LogUtils.toJson(result));
        }
    }

    public ApDomainOut getPrintMethods(HibernateHelper hibernateHelper, String locale) {
        ApDomainOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<ApDomain> noticeCharges = new ApDomainBussiness().findByType(cmPosSession, Constants.AP_DOMAIN_TYPE_PRINT_METHOD);
            result = new ApDomainOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), noticeCharges);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ApDomainOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ApDomainController.getPrintMethods:result=" + LogUtils.toJson(result));
        }
    }
}
