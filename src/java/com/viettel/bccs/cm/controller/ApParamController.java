package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.ApParamBussiness;
import com.viettel.bccs.cm.bussiness.TokenBussiness;
import com.viettel.bccs.cm.bussiness.common.DecryptBusiness;
import com.viettel.bccs.cm.dao.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.dao.TaskDAO;
import com.viettel.bccs.cm.dao.TaskManagementDAO;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.ReasonConfigDetail;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.Token;
import com.viettel.bccs.cm.model.UpdateRecoverReason;
import com.viettel.bccs.cm.supplier.ApParamSupplier;
import com.viettel.bccs.cm.supplier.im.ImSupplier;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ValidateUtils;
import com.viettel.brcd.ws.model.output.ApParamOut;
import com.viettel.brcd.ws.model.output.ImageInfrastructureOut;
import com.viettel.brcd.ws.model.output.LoginOut;
import com.viettel.brcd.ws.model.output.MaxRadiusOut;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.ReasonConfigOut;
import com.viettel.brcd.ws.model.output.RequestStatusOut;
import com.viettel.brcd.ws.model.output.Service;
import com.viettel.brcd.ws.model.output.ServiceOut;
import com.viettel.brcd.ws.vsa.ResourceBundleUtil;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.im.database.BO.AccountAgent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vietnn6
 */
public class ApParamController extends BaseController {

    public ApParamController() {
        logger = Logger.getLogger(ApParamController.class);
    }

    public List<Service> getServices(Session cmPosSession) {
        List<Service> services = null;
        List<ApParam> params = new ApParamBussiness().findByType(cmPosSession, Constants.MAPPING_BRCD_SERVICE);
        if (params == null || params.isEmpty()) {
            return services;
        }
        services = new ArrayList<Service>();
        for (ApParam param : params) {
            if (isFixedService(param)) {
                Long serviceId = ConvertUtils.toLong(param.getParamValue());
                services.add(new Service(serviceId, param.getParamName(), param.getParamCode()));
            }
        }
        return services;
    }

    public Long getMaxRadius(Session cmPosSession, String serviceCode) {
        Long result = null;
        if (serviceCode == null) {
            return result;
        }
        serviceCode = serviceCode.trim().toUpperCase();
        String paramType = null;
        if (Constants.SERVICE_ALIAS_ADSL.equals(serviceCode)) {
            paramType = Constants.MAX_LENGTH_RADIUS_ADSL;
        } else if (Constants.SERVICE_ALIAS_FTTH.equals(serviceCode)) {
            paramType = Constants.MAX_LENGTH_RADIUS_FTTH;
        } else if (Constants.SERVICE_ALIAS_LEASEDLINE.equals(serviceCode)) {
            paramType = Constants.MAX_LENGTH_RADIUS_LL;
        }
        if (paramType == null) {
            return result;
        }
        result = ConvertUtils.toLong(new ApParamBussiness().getUserManualConfig(cmPosSession, paramType));
        return result;
    }

    public String getMaxLengthSurvey(Session cmPosSession, String serviceCode) {
        String result = null;
        if (serviceCode == null) {
            return result;
        }
        serviceCode = serviceCode.trim().toUpperCase();
        String paramType = null;
        if (Constants.SERVICE_ALIAS_ADSL.equals(serviceCode)) {
            paramType = Constants.MAX_LENGTH_CABLE_ADSL;
        } else if (Constants.SERVICE_ALIAS_FTTH.equals(serviceCode)) {
            paramType = Constants.MAX_LENGTH_CABLE_FTTH;
        } else if (Constants.SERVICE_ALIAS_LEASEDLINE.equals(serviceCode)) {
            paramType = Constants.MAX_LENGTH_CABLE_LL;
        }
        if (paramType == null) {
            return result;
        }
        result = new ApParamBussiness().getUserManualConfig(cmPosSession, paramType);
        return result;
    }

    public boolean isFixedService(ApParam param) {
        boolean isFixed = false;
        if (param == null) {
            return isFixed;
        }
        Long serviceId = ConvertUtils.toLong(param.getParamValue());
        if (serviceId == null || Constants.MOBILE_TEL_SERVICE.equals(serviceId)
                || Constants.HOMEPHONE_TEL_SERVICE.equals(serviceId)) {
            return isFixed;
        }
        isFixed = true;
        return isFixed;
    }

    public ServiceOut getServices(HibernateHelper hibernateHelper, String locale) {
        ServiceOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Service> services = getServices(cmPosSession);
            result = new ServiceOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), services);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ServiceOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ApParamController.getServices:result=" + LogUtils.toJson(result));
        }
    }

    public MaxRadiusOut getMaxRadius(HibernateHelper hibernateHelper, String serviceCode, String locale) {
        MaxRadiusOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            Long maxRadius = getMaxRadius(cmPosSession, serviceCode);
            result = new MaxRadiusOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), maxRadius);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new MaxRadiusOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ApParamController.getServices:result=" + LogUtils.toJson(result));
        }
    }

    public ApParamOut getMaxLengthSurvey(HibernateHelper hibernateHelper, String seviceCode, String locale) {
        ApParamOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            String maxLengthSurvey = getMaxLengthSurvey(cmPosSession, seviceCode);
            result = new ApParamOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), maxLengthSurvey);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ApParamOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ApParamController.getMaxLengthSurvey:result=" + LogUtils.toJson(result));
        }
    }

    public List<ApParam> getSearchRequestStatus(Session cmPosSession, String locale) {
        List<ApParam> lstReqStatus = new ApParamBussiness().findByType(cmPosSession, Constants.SEARCH_SUB_REQ_STATUS);
        if (lstReqStatus != null && !lstReqStatus.isEmpty()) {
            for (ApParam status : lstReqStatus) {
                if (status != null) {
                    String name = status.getParamName();
                    if (name != null) {
                        name = name.trim().toLowerCase();
                        if (!name.isEmpty()) {
                            status.setParamName(LabelUtil.getKey(name, locale));
                        }
                    }
                }
            }
        }
        return lstReqStatus;
    }

    public RequestStatusOut getSearchRequestStatus(HibernateHelper hibernateHelper, String locale) {
        RequestStatusOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<ApParam> lstReqStatus = getSearchRequestStatus(cmPosSession, locale);
            result = new RequestStatusOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lstReqStatus);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new RequestStatusOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ApParamController.getSearchRequestStatus:result=" + LogUtils.toJson(result));
        }
    }

    public ReasonConfigOut getListReasonConfig(HibernateHelper hibernateHelper, String token, String locale) {
        ReasonConfigOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<ApParam> lst = new ApParamBussiness().findReasonConfig(cmPosSession, null);
            if (lst == null || lst.isEmpty()) {
                result = new ReasonConfigOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            List<ReasonConfigDetail> lst_detail = new ArrayList<ReasonConfigDetail>();
            for (ApParam re : lst) {
                if (re != null) {
                    lst_detail.add(new ReasonConfigDetail(re));
                }
            }
            result = new ReasonConfigOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lst_detail);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ReasonConfigOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ApParamController.getListReasonConfig:result=" + LogUtils.toJson(result));
        }
    }

    public ImageInfrastructureOut getListImageType(HibernateHelper hibernateHelper, String token, String locale, String action, Long taskMngtId) {
        ImageInfrastructureOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            String paramType = "";
            if (action == null || "".equals(action) || "?".equals(action)) {
                //default la lay anh ha tang
                paramType = Constants.TYPE_IMAGE_INFRASTRUCTURE;
            } else {
                /*TYPE_IMAGE_CONTRACT*/
                paramType = action;
            }
            List<ApParam> lst = new ApParamBussiness().findByType(cmPosSession, paramType);
            if (taskMngtId != null) {
                com.viettel.bccs.cm.model.TaskManagement task = new TaskManagementDAO().findById(cmPosSession, taskMngtId);
                if (task != null && task.getPathFileAttach() != null) {
                    List<ApParam> lstTemp = new ApParamBussiness().findByType(cmPosSession, paramType + "_BUNDLE_TV");
                    lst.addAll(lstTemp);
                }
                if (Constants.SERVICE_ALIAS_CAMERA.equals(task.getServiceType())) {
                    lst.clear();
                    List<ApParam> lstTemp = new ApParamBussiness().findByType(cmPosSession, paramType + "_" + Constants.SERVICE_ALIAS_CAMERA);
                    lst.addAll(lstTemp);
                }
            }
            if (lst == null || lst.isEmpty()) {
                result = new ImageInfrastructureOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }

            result = new ImageInfrastructureOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lst);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ImageInfrastructureOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ApParamController.getListImageType:result=" + LogUtils.toJson(result));
        }
    }

    public ParamOut getListKpi(HibernateHelper hibernateHelper, String token, String locale) {
        Session cmPosSession = null;
        ParamOut param = new ParamOut();
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            param.setListKpiInfo(new ApParamBussiness().findByType(cmPosSession, "KPI"));
            param.setListKpiDeadline(new TaskDAO().getListKpiDeadlineTask(cmPosSession));
            param.setListKpiBonus(new TaskDAO().getListKpiBonusTask(cmPosSession));
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);

        } finally {
            closeSessions(cmPosSession);
        }
        return param;
    }

    public ParamOut getRecoverActionType(HibernateHelper hibernateHelper, String token, String locale) {
        Session cmPosSession = null;
        ParamOut param = new ParamOut();
        param.setErrorCode(Constants.ERROR_CODE_1);
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            param.setErrorCode(Constants.ERROR_CODE_0);
            param.setErrorDecription(LabelUtil.getKey("common.success", locale));
            ///phuonghc modify date: 09062020
            String supportingLanguage = "UPDATE_RECOVER_ACTION_TYPE";
            if (StringUtils.isNotEmpty(locale)) {
                supportingLanguage = supportingLanguage + "_" + locale;
            }
            List<ApParam> apParam = new ApParamSupplier().findParam(cmPosSession, supportingLanguage, null, null);
            //remove suffix for paramCode
            for (ApParam element : apParam) {
                String paramCode = element.getParamCode();
                if (paramCode != null && paramCode.contains("_" + locale)) {
                    paramCode = paramCode.replace("_" + locale, "");

                }
                element.setParamCode(paramCode);
            }
            param.setListActionType(apParam);
        } catch (Throwable ex) {
            param.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);

        } finally {
            closeSessions(cmPosSession);
        }
        return param;
    }

    public ParamOut getRecoverReason(HibernateHelper hibernateHelper, String token, String locale, String actionType) {
        Session cmPosSession = null;
        ParamOut param = new ParamOut();
        param.setErrorCode(Constants.ERROR_CODE_1);
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            param.setErrorCode(Constants.ERROR_CODE_0);
            param.setErrorDecription(LabelUtil.getKey("common.success", locale));
            if (StringUtils.isNotEmpty(locale)) {
                actionType = actionType + "_" + locale;
            }
            List<ApParam> apParam = new ApParamSupplier().findParam(cmPosSession, actionType, null, null);
            param.setListRecoverReason(apParam);
        } catch (Throwable ex) {
            param.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);

        } finally {
            closeSessions(cmPosSession);
        }
        return param;
    }

    public ParamOut updateRecoverReason(HibernateHelper hibernateHelper, String token, String locale, Long contractId, String actionType, String reasonCode, String x, String y) {
        Session cmPosSession = null;
        Session imSession = null;
        Session paymentSession = null;
        ParamOut param = new ParamOut();
        param.setErrorCode(Constants.ERROR_CODE_1);
        boolean isError = false;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            paymentSession = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            if (contractId == null) {
                param.setErrorDecription(LabelUtil.getKey("recover.contractId.isnull", locale));
                isError = true;
                return param;
            }
            if (actionType == null || actionType.isEmpty()) {
                param.setErrorDecription(LabelUtil.getKey("recover.actionType.isnull", locale));
                isError = true;
                return param;
            }
            if (reasonCode == null || reasonCode.isEmpty()) {
                param.setErrorDecription(LabelUtil.getKey("recover.reasonCode.isnull", locale));
                isError = true;
                return param;
            }
            if (x == null || x.isEmpty()) {
                param.setErrorDecription(LabelUtil.getKey("recover.x.isnull", locale));
                isError = true;
                return param;
            }
            if (y == null || y.isEmpty()) {
                param.setErrorDecription(LabelUtil.getKey("recover.y.isnull", locale));
                isError = true;
                return param;
            }

            //phuonghc modify date 15062020 accept save by english only
            String actionTypeTemp = actionType + "_" + "en_US";

            List<ApParam> apParamAction = new ApParamSupplier().findParam(cmPosSession, null, actionTypeTemp, null);
            if (apParamAction == null || apParamAction.isEmpty()) {
                param.setErrorDecription(LabelUtil.getKey("recover.action.type.invalid", locale));
                isError = true;
                return param;
            }
            List<ApParam> apParamReason = new ApParamSupplier().findParam(cmPosSession, actionTypeTemp, reasonCode, null);
            if (apParamReason == null || apParamReason.isEmpty()) {
                param.setErrorDecription(LabelUtil.getKey("recover.reason.invalid", locale));
                isError = true;
                return param;
            }

            List<Object> listSub = new SubAdslLeaselineDAO().findIsdnByContractId(paymentSession, contractId);
            if (listSub == null || listSub.isEmpty()) {
                param.setErrorDecription(LabelUtil.getKey("recover.contractId.invalid", locale));
                isError = true;
                return param;
            }

            Token tokenUser = new TokenBussiness().findByToken(cmPosSession, token);
            String userName = tokenUser.getUserName();
            if (ValidateUtils.isNumeric(userName)) {
                userName = userName.startsWith("855") ? userName.substring(3) : userName;
                userName = userName.startsWith("0") ? userName.substring(1) : userName;
                AccountAgent accountAgent = new ImSupplier().checkAccountAgentByIsdn(imSession, userName);
                userName = accountAgent.getOwnerCode();
            }

            String sql = "update update_recover_reason set status =0,MODIFY_DATE = sysdate, MODIFY_USER = ? where status = 1 and contract_id = ? ";
            Query query = cmPosSession.createSQLQuery(sql);
            query.setParameter(0, userName);
            query.setParameter(1, contractId);
            query.executeUpdate();

            UpdateRecoverReason updateRecoverReason = new UpdateRecoverReason();
            updateRecoverReason.setActionCode(actionType);
            updateRecoverReason.setAccount(listSub.get(0).toString());
            updateRecoverReason.setActionName(apParamAction.get(0).getParamName());
            updateRecoverReason.setContractId(contractId);
            updateRecoverReason.setCreateDate(new Date());
            updateRecoverReason.setCreateUser(userName);
            updateRecoverReason.setReasonCode(apParamReason.get(0).getParamCode());
            updateRecoverReason.setReasonName(apParamReason.get(0).getParamName());
            updateRecoverReason.setStatus(1l);
            updateRecoverReason.setX(x);
            updateRecoverReason.setY(y);
            cmPosSession.save(updateRecoverReason);

            param.setErrorCode(Constants.ERROR_CODE_0);
            param.setErrorDecription(LabelUtil.getKey("common.success", locale));
        } catch (Throwable ex) {
            param.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);

        } finally {
            if (cmPosSession != null) {
                if (!isError) {
                    commitTransactions(cmPosSession);
                } else {
                    rollBackTransactions(cmPosSession);
                }
                closeSessions(cmPosSession, imSession, paymentSession);
            }
        }
        return param;
    }
}
