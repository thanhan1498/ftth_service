package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.common.CacheBO;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.model.output.District;
import com.viettel.brcd.ws.model.output.DistrictOut;
import com.viettel.brcd.ws.model.output.Precinct;
import com.viettel.brcd.ws.model.output.PrecinctOut;
import com.viettel.brcd.ws.model.output.Province;
import com.viettel.brcd.ws.model.output.ProvinceOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class AreaController extends BaseController {

    public AreaController() {
        logger = Logger.getLogger(AreaController.class);
    }

    public List<Province> getProvinces(Session cmPosSession) {
        /*List<Province> result = null;
         List<Area> areas = new AreaBussiness().findProvinces(cmPosSession);
         if (areas == null || areas.isEmpty()) {
         return result;
         }
         result = new ArrayList<Province>();
         for (Area area : areas) {
         if (area != null) {
         result.add(new Province(area));
         }
         }
         return result;*/
        return CacheBO.getListProvince(cmPosSession);
    }

    public List<District> getDistricts(Session cmPosSession, String province) {
        /*List<District> result = null;
         List<Area> areas = new AreaBussiness().findDistricts(cmPosSession, province);
         if (areas == null || areas.isEmpty()) {
         return result;
         }
         result = new ArrayList<District>();
         for (Area area : areas) {
         if (area != null) {
         result.add(new District(area));
         }
         }
         return result;*/
        return CacheBO.getListDistrict(cmPosSession, province);
    }

    public List<Precinct> getPrecincts(Session cmPosSession, String province, String district) {
        /*List<Precinct> result = null;
        List<Area> areas = new AreaBussiness().findPrecincts(cmPosSession, province, district);
        if (areas == null || areas.isEmpty()) {
            return result;
        }
        result = new ArrayList<Precinct>();
        for (Area area : areas) {
            if (area != null) {
                result.add(new Precinct(area));
            }
        }
        return result;*/
        return CacheBO.getListPrevince(cmPosSession, province, district);
    }

    public ProvinceOut getProvinces(HibernateHelper hibernateHelper, String locale) {
        ProvinceOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Province> provinces = getProvinces(cmPosSession);
            result = new ProvinceOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), provinces);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ProvinceOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            //LogUtils.info(logger, "AreaController.getProvinces:result=" + LogUtils.toJson(result));
        }
    }

    public DistrictOut getDistricts(HibernateHelper hibernateHelper, String province, String locale) {
        DistrictOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<District> districts = getDistricts(cmPosSession, province);
            result = new DistrictOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), districts);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new DistrictOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            //LogUtils.info(logger, "AreaController.getDistricts:result=" + LogUtils.toJson(result));
        }
    }

    public PrecinctOut getPrecincts(HibernateHelper hibernateHelper, String province, String district, String locale) {
        PrecinctOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Precinct> precincts = getPrecincts(cmPosSession, province, district);
            result = new PrecinctOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), precincts);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new PrecinctOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            //LogUtils.info(logger, "AreaController.getDistricts:result=" + LogUtils.toJson(result));
        }
    }
}
