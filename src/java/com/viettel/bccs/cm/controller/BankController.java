package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.BankBussiness;
import com.viettel.bccs.cm.model.Bank;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.model.output.BankOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class BankController extends BaseController {

    public BankController() {
        logger = Logger.getLogger(BankController.class);
    }

    public BankOut searchBank(HibernateHelper hibernateHelper, String bankCode, String name, Long pageSize, Long pageNo, String locale) {
        BankOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            BankBussiness bankBussiness = new BankBussiness();
            Long count = bankBussiness.count(cmPosSession, bankCode, name);
            if (count == null || count <= 0L) {
                result = new BankOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
                return result;
            }
            Integer start = getStart(pageSize, pageNo);
            Integer max = getMax(pageSize, pageNo, count);
            List<Bank> banks = bankBussiness.find(cmPosSession, bankCode, name, start, max);
            result = new BankOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), banks);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new BankOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "BankController.searchBank:result=" + LogUtils.toJson(result));
        }
    }
}
