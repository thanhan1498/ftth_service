package com.viettel.bccs.cm.controller;

import com.google.gson.Gson;
import com.viettel.bccs.cm.bussiness.BaseBussiness;
import com.viettel.brcd.common.util.PerformanceLogger;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.util.StaticUtil;
import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vietnn6
 */
public class BaseController {

    protected Logger logger = Logger.getLogger(BaseController.class.getName()) ;
    protected PerformanceLogger logVTG = new PerformanceLogger();
    protected Gson gson = new Gson();
    protected String USER_NAME = "";
    protected String URI = "";
    protected String PATH = "";
    protected BaseBussiness baseBussiness = new BaseBussiness();

    public void rollBackTransactions(Session... sessions) {
        baseBussiness.rollBackTransactions(sessions);
    }

    public void commitTransactions(Session... sessions) {
        baseBussiness.commitTransactions(sessions);
    }

    public void flushSessions(Session... sessions) {
        baseBussiness.flushSessions(sessions);
    }

    public void beginTransactions(Session... sessions) {
        baseBussiness.beginTransactions(sessions);
    }

    public void clearSessions(Session... sessions) {
        baseBussiness.clearSessions(sessions);
    }

    public void closeSessions(Session... sessions) {
        baseBussiness.closeSessions(sessions);
    }

    public String validateLocale(String locale) {
        if (locale == null) {
            return LabelUtil.getKey("language.code.invalid", locale);
        }
        locale = locale.trim();
        if (locale.isEmpty()) {
            return LabelUtil.getKey("language.code.invalid", locale);
        }
        String[] locales = StaticUtil.getLocales();
        if (locales == null) {
            return LabelUtil.getKey("language.code.invalid", locale);
        }
        List<String> _locales = Arrays.asList(locales);
        if (!_locales.contains(locale)) {
            return LabelUtil.getKey("language.code.invalid", locale);
        }
        return null;
    }

    public Integer getStart(Long pageSize, Long pageNo) {
        Long start = null;
        if (pageNo != null && pageSize != null) {
            start = pageNo * pageSize;
        }
        Integer result = start == null ? null : (start < 0L ? 0 : start.intValue());
        return result;
    }

    public Integer getMax(Long pageSize, Long pageNo, Long count) {
        Integer result = null;
        if (count == null || count <= 0L) {
            return result;
        }
        result = pageSize == null ? null : (pageSize < count ? pageSize.intValue() : count.intValue());
        return result;
    }
}
