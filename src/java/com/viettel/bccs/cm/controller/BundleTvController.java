/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.google.gson.Gson;
import com.viettel.bccs.api.Task.BO.Contract;
import com.viettel.bccs.api.Task.DAO.ContractDAO;
import com.viettel.bccs.cm.dao.CustomerDAO;
import com.viettel.bccs.cm.dao.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubBundleTv;
import com.viettel.bccs.cm.supplier.ServiceSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.Request;
import com.viettel.brcd.ws.model.output.ServiceResponse;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class BundleTvController extends BaseController {

    public BundleTvController() {
        logger = Logger.getLogger(BundleTvController.class);
    }

    public WSRespone addUserBundleTv(HibernateHelper hibernateHelper, Long subId) {
        WSRespone result = new WSRespone();
        result.setErrorCode(Constants.ERROR_CODE_1);
        Session cmSession = null;
        try {
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            SubBundleTv subTv = new SubBundleTv();
            SubAdslLeaseline subAdslLL = new SubAdslLeaselineDAO().findById(cmSession, subId);
            if (subTv == null || subAdslLL == null) {
                result.setErrorDecription("Can not find sub bubdle tv with subId " + subId);
                return result;
            }
            if (subTv.getUserUniqueId() != null) {
                result.setErrorDecription("This account is exists");
                return result;
            }
            Contract contract = new ContractDAO().findById(subAdslLL.getContractId(), cmSession);
            if (contract == null) {
                result.setErrorDecription("Can not find contract for this account");
                return result;
            }
            Customer customer = new CustomerDAO().findById(cmSession, contract.getCustId());
            if (customer == null) {
                result.setErrorDecription("Can not find customer for this account");
                return result;
            }
            if ("true".equalsIgnoreCase(com.viettel.brcd.common.util.ResourceBundleUtils.getResource("provisioning.allow.send.request", "provisioning"))) {
                Request request = new Request();
                request.addUser(customer, subTv.getAccount());
                String data = new Gson().toJson(request);
                String response = new ServiceSupplier().sendRequest(Constants.SERVICE_METHOD_POST, ResourceUtils.getResource("BUNDLE_TV_SERVICE"), data, result, null);
                if (response == null || response.isEmpty()) {
                    result.setErrorCode(Constants.ERROR_CODE_1);
                    return result;
                } else {
                    WSRespone ws = new Gson().fromJson(response, WSRespone.class);
                    ServiceResponse serviceResponse = ws == null || ws.getErrorDecription() == null ? null : new Gson().fromJson(ws.getErrorDecription(), ServiceResponse.class);
                    if (serviceResponse == null || !Constants.RESPONSE_SUCCESS.equals(serviceResponse.getResult())
                            || serviceResponse.getContent() == null || serviceResponse.getContent().getUnique_id() != null) {
                        result.setErrorDecription("Error from call service addUser bunlde tv " + (serviceResponse != null ? serviceResponse.getMessage() : ""));
                        return result;
                    } else {
                        subTv.setUserUniqueId(serviceResponse.getContent().getUnique_id());
                        cmSession.save(subTv);
                        commitTransactions(cmSession);
                    }
                }
            }
        } catch (Throwable ex) {
            rollBackTransactions(cmSession);
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ParamOut(Constants.ERROR_CODE_1, ex.getMessage());
            return result;
        } finally {
            closeSessions(cmSession);
            LogUtils.info(logger, "BundleTvController.addUserBundleTv:result=" + LogUtils.toJson(result));
        }
        return result;
    }

}
