package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.BusTypeBussiness;
import com.viettel.bccs.cm.bussiness.common.CacheBO;
import com.viettel.bccs.cm.model.BusType;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.model.output.CustomType;
import com.viettel.brcd.ws.model.output.CustomTypeOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class BusTypeController extends BaseController {

    public BusTypeController() {
        logger = Logger.getLogger(BusTypeController.class);
    }

    public List<CustomType> getBusTypes(Session cmPosSession) {
        List<CustomType> result = null;
        List<BusType> busTypes = new BusTypeBussiness().findActives(cmPosSession);
        if (busTypes == null || busTypes.isEmpty()) {
            return result;
        }
        result = new ArrayList<CustomType>();
        for (BusType busType : busTypes) {
            if (busType != null) {
                result.add(new CustomType(busType));
            }
        }
        return result;
    }
    
    public List<CustomType> getBusTypesPre(Session cmPreSession) {
        List<CustomType> result = null;
        List<BusType> busTypes = new BusTypeBussiness().getListBusTypePre(cmPreSession);
        if (busTypes == null || busTypes.isEmpty()) {
            return result;
        }
        result = new ArrayList<CustomType>();
        for (BusType busType : busTypes) {
            if (busType != null) {
                result.add(new CustomType(busType));
            }
        }
        return result;
    }
    
    public CustomTypeOut getBusTypes(HibernateHelper hibernateHelper, String locale) {
        CustomTypeOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            //List<CustomType> customTypes = getBusTypes(cmPosSession);
            List<CustomType> customTypes = CacheBO.getListBusType(cmPosSession);
            result = new CustomTypeOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), customTypes);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CustomTypeOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
        }
    }
    
    public CustomTypeOut getBusTypesPre(HibernateHelper hibernateHelper, String locale) {
        CustomTypeOut result = null;
        Session cmPreSession = null;
        try {
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            List<CustomType> customTypes = getBusTypesPre(cmPreSession);
            result = new CustomTypeOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), customTypes);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CustomTypeOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPreSession);
        }
    }
}
