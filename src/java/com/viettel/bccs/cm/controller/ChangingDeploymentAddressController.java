/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.ChangeDeploymentAddressBusiness;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.ChangeDeploymentAddressIn;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.Session;

/**
 * ChangingDeploymentAddressController
 *
 * @author phuonghc
 */
public class ChangingDeploymentAddressController extends BaseController {

    private static final String DEFAULT_VALUE = "?";

    public ParamOut getListAccountForSubscriber(HibernateHelper hibernateHelper, String custId, String locale) {
        if (StringUtils.isEmpty(custId) || DEFAULT_VALUE.equals(custId)) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("common.custId.invalid", locale));
        }
        Session sessionCmPos = null;
        try {
            sessionCmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<SubAdslLeaseline> accountOfCusList = new ChangeDeploymentAddressBusiness().getListAccountForSubscriber(sessionCmPos, custId);
            ParamOut result = new ParamOut();
            result.setErrorCode(Constants.ERROR_CODE_0);
            result.setErrorDecription(LabelUtil.getKey("common.success", locale));
            result.setAccountOfCustomer(accountOfCusList);
            return result;
        } catch (Exception e) {
            logger.error("### An error occured while getListAccountForSubscriber with custId=" + custId, e);
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("common.custId.invalid", locale));
        } finally {
            closeSessions(sessionCmPos);
        }
    }

    public ParamOut getListReasonByType(HibernateHelper hibernateHelper, String custId, String account, String locale) {
        logger.info("### start function getListReasonByType");
        ParamOut out = new ParamOut();
        if (StringUtils.isEmpty(custId) || DEFAULT_VALUE.equals(custId)) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("common.custId.invalid", locale));
        }
        if (StringUtils.isEmpty(account) || DEFAULT_VALUE.equals(account)) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("common.account.invalid", locale));
        }
        Session sessionCmPos = null;
        try {
            sessionCmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Reason> reasonByTypeList = new ChangeDeploymentAddressBusiness().getListReasonByType(sessionCmPos, custId, account);
            out.setErrorCode(Constants.ERROR_CODE_0);
            out.setErrorDecription(LabelUtil.getKey("common.success", locale));
            out.setReasonChangeDeploymentAddress(reasonByTypeList);
            return out;
        } catch (Exception e) {
            logger.error("### An error occured while getListAccountForSubscriber with custId=" + custId, e);
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("common.custId.invalid", locale));
        } finally {
            closeSessions(sessionCmPos);
        }
    }

    public ParamOut doingChangeDeploymentAddress(HibernateHelper hibernateHelper, ChangeDeploymentAddressIn input, String locale) {
        if (input == null) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("a.error.has.occurred", locale));
        }
        Session sessionCmPos = null;
        Session sessionCmPre = null;
        Session sessionNims = null;
        Session sessionCc = null;
        Session sessionIm = null;
        ParamOut out = new ParamOut();
        if (StringUtils.isEmpty(input.getSubId()) || DEFAULT_VALUE.equals(input.getSubId())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("validate.required.subId", locale));
        }
        if (!NumberUtils.isNumber(input.getSubId())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("common.subId.invalid", locale));
        }
        if (StringUtils.isEmpty(input.getStationId()) || DEFAULT_VALUE.equals(input.getStationId())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("stationId.is.required", locale));
        }
        if (!NumberUtils.isNumber(input.getStationId())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("common.stationId.invalid", locale));
        }
        if (StringUtils.isEmpty(input.getConnectorId()) || DEFAULT_VALUE.equals(input.getConnectorId())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("connectorId.is.required", locale));
        }
        if (!NumberUtils.isNumber(input.getConnectorId())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("common.connectorId.invalid", locale));
        }
        if (StringUtils.isEmpty(input.getShopCode()) || DEFAULT_VALUE.equals(input.getShopCode())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("common.shopCode.invalid", locale));
        }
        if (StringUtils.isEmpty(input.getStaffCode()) || DEFAULT_VALUE.equals(input.getStaffCode())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("input.invalid.staffCode", locale));
        }
        if (StringUtils.isEmpty(input.getStaffId()) || DEFAULT_VALUE.equals(input.getStaffId())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("staffId.is.required", locale));
        }
        if (StringUtils.isEmpty(input.getReasonId()) || DEFAULT_VALUE.equals(input.getReasonId())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("input.invalid.reasonId", locale));
        }
        if (StringUtils.isEmpty(input.getTeamCode()) || DEFAULT_VALUE.equals(input.getTeamCode())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("teamCode.is.invalid", locale));
        }
        if (StringUtils.isEmpty(input.getProvinceCode()) || DEFAULT_VALUE.equals(input.getProvinceCode())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("common.provinceCode.invalid", locale));
        }
        if (StringUtils.isEmpty(input.getDistrictCode()) || DEFAULT_VALUE.equals(input.getDistrictCode())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("common.districtCode.invalid", locale));
        }
        if (StringUtils.isEmpty(input.getPrecinctCode()) || DEFAULT_VALUE.equals(input.getPrecinctCode())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("common.precinctCode.invalid", locale));
        }
        if (StringUtils.isNotEmpty(input.getStreetName()) && DEFAULT_VALUE.equals(input.getStreetName())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("common.streetName.invalid", locale));
        }
        if (StringUtils.isNotEmpty(input.getHome()) && DEFAULT_VALUE.equals(input.getHome())) {
            return new ParamOut(Constants.ERROR_CODE_2, LabelUtil.getKey("common.home.invalid", locale));
        }

        try {
            sessionCmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            sessionCmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            sessionNims = hibernateHelper.getSession(Constants.SESSION_NIMS);
            sessionCc = hibernateHelper.getSession(Constants.SESSION_CC);
            sessionIm = hibernateHelper.getSession(Constants.SESSION_IM);
            out = new ChangeDeploymentAddressBusiness().doingChangeDeploymentAddress(sessionCmPos, sessionCmPre, sessionCc, sessionNims, sessionIm, input, locale);
            if (Constants.ERROR_CODE_0.equals(out.getErrorCode())) {
                sessionCmPos.getTransaction().commit();
                sessionCmPre.getTransaction().commit();
                sessionNims.getTransaction().commit();
                sessionCc.getTransaction().commit();
                sessionIm.getTransaction().commit();
            }
            return out;
        } catch (Exception e) {
            logger.error("### An error occured while doingChangeDeploymentAddress:" + e.getMessage());
            out.setErrorCode(Constants.ERROR_CODE_1);
            out.setErrorDecription(LabelUtil.getKey("a.error.has.occurred", locale));
            return out;

        } finally {
            closeSessions(sessionCmPos);
            closeSessions(sessionCmPre);
            closeSessions(sessionNims);
            closeSessions(sessionCc);
            closeSessions(sessionIm);
        }
    }
}
