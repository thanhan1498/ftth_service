package com.viettel.bccs.cm.controller;

import com.viettel.bccs.api.Task.BO.TransLogsClient;
import com.viettel.bccs.cm.model.Codes;
import com.viettel.bccs.cm.model.FormChangeInformation;
import com.viettel.brcd.ws.model.input.ActivePreIn;
import com.viettel.brcd.ws.model.input.ActiveSubscriberPreIn;
import com.viettel.brcd.ws.model.input.CancelContractInput;
import com.viettel.brcd.ws.model.input.CancelRULInput;
import com.viettel.brcd.ws.model.input.CancelRequestInput;
import com.viettel.brcd.ws.model.input.CancelSubFbConfigInput;
import com.viettel.brcd.ws.model.input.ChangeDeploymentAddressIn;
import com.viettel.brcd.ws.model.input.ChangeSimIn;
import com.viettel.brcd.ws.model.input.ChangeSimInput;
import com.viettel.brcd.ws.model.input.ComplaintInput;
import com.viettel.brcd.ws.model.input.ContractIn;
import com.viettel.brcd.ws.model.input.Coordinate;
import com.viettel.brcd.ws.model.input.CustomerIn;
import com.viettel.brcd.ws.model.input.ImageCusInput;
import com.viettel.brcd.ws.model.input.ImageInput;
import com.viettel.brcd.ws.model.input.InfrastructureUpdateIn;
import com.viettel.brcd.ws.model.input.PaymentInput;
import com.viettel.brcd.ws.model.input.RevokeTaskIn;
import com.viettel.brcd.ws.model.input.SearchSubRequestInput;
import com.viettel.brcd.ws.model.input.SearchTaskForLookUpInput;
import com.viettel.brcd.ws.model.input.SearchTaskForUpdateInput;
import com.viettel.brcd.ws.model.input.SearchTaskInput;
import com.viettel.brcd.ws.model.input.SearchTaskRevokeIn;
import com.viettel.brcd.ws.model.input.SubAdslLeaselineOut;
import com.viettel.brcd.ws.model.input.TaskToAssignIn;
import com.viettel.brcd.ws.model.input.UpdateNoteIn;
import com.viettel.brcd.ws.model.input.UpdateTaskIn;
import com.viettel.brcd.ws.model.input.WSRequest;
import com.viettel.brcd.ws.model.output.ApDomainOut;
import com.viettel.brcd.ws.model.output.ApParamOut;
import com.viettel.brcd.ws.model.output.BankOut;
import com.viettel.brcd.ws.model.output.BundleTVInfo;
import com.viettel.brcd.ws.model.output.CableBoxOut;
import com.viettel.brcd.ws.model.output.ChangeInformationOut;
import com.viettel.brcd.ws.model.output.ClearDebitTransOut;
import com.viettel.brcd.ws.model.output.CompDepartmentGroupOut;
import com.viettel.brcd.ws.model.output.ComplainResultOut;
import com.viettel.brcd.ws.model.output.ComplaintAcceptSourceOut;
import com.viettel.brcd.ws.model.output.ComplaintAcceptTypeOut;
import com.viettel.brcd.ws.model.output.ComplaintGroupOut;
import com.viettel.brcd.ws.model.output.ComplaintTypeOut;
import com.viettel.brcd.ws.model.output.CoordinateOut;
import com.viettel.brcd.ws.model.output.CusLocationOut;
import com.viettel.brcd.ws.model.output.CustomTypeOut;
import com.viettel.brcd.ws.model.output.CustomerOut;
import com.viettel.brcd.ws.model.output.DashboardBroadbanTab;
import com.viettel.brcd.ws.model.output.DebitOut;
import com.viettel.brcd.ws.model.output.DebitStaffOut;
import com.viettel.brcd.ws.model.output.DebitTransHistoryOut;
import com.viettel.brcd.ws.model.output.DebitTransOut;
import com.viettel.brcd.ws.model.output.DepositOut;
import com.viettel.brcd.ws.model.output.DistrictOut;
import com.viettel.brcd.ws.model.output.DomainOut;
import com.viettel.brcd.ws.model.output.FileOut;
import com.viettel.brcd.ws.model.output.TaskToAssignOut;
import com.viettel.brcd.ws.model.output.GroupOut;
import com.viettel.brcd.ws.model.output.GroupReasonOut;
import com.viettel.brcd.ws.model.output.HistorySubFbConfigOut;
import com.viettel.brcd.ws.model.output.IdTypeOut;
import com.viettel.brcd.ws.model.output.ImageInfrastructureOut;
import com.viettel.brcd.ws.model.output.ImageRespone;
import com.viettel.brcd.ws.model.output.InforInfrastructureOut;
import com.viettel.brcd.ws.model.output.InforResolveComplainOut;
import com.viettel.brcd.ws.model.output.InforTaskToUpdateOut;
import com.viettel.brcd.ws.model.output.InvestOut;
import com.viettel.brcd.ws.model.output.InvoiceListOut;
import com.viettel.brcd.ws.model.output.LastUpdateConfigOut;
import com.viettel.brcd.ws.model.output.LimitValueOut;
import com.viettel.brcd.ws.model.output.LineTypeOut;
import com.viettel.brcd.ws.model.output.LinkFileOut;
import com.viettel.brcd.ws.model.output.ListReasonByCodePreOut;
import com.viettel.brcd.ws.model.output.LocalProductPpOut;
import com.viettel.brcd.ws.model.output.LockDebitTransOut;
import com.viettel.brcd.ws.model.output.LoginOut;
import com.viettel.brcd.ws.model.output.MaxRadiusOut;
import com.viettel.brcd.ws.model.output.NotificationTaskOut;
import com.viettel.brcd.ws.model.output.OTPOut;
import com.viettel.brcd.ws.model.output.OwnerOut;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.PayAdvanceBillOut;
import com.viettel.brcd.ws.model.output.PayContractOut;
import com.viettel.brcd.ws.model.output.PaymentOut;
import com.viettel.brcd.ws.model.output.PortOut;
import com.viettel.brcd.ws.model.output.PreRevokeOut;
import com.viettel.brcd.ws.model.output.PrecinctOut;
import com.viettel.brcd.ws.model.output.ProductOut;
import com.viettel.brcd.ws.model.output.ProductPricePlanOut;
import com.viettel.brcd.ws.model.output.PromotionOut;
import com.viettel.brcd.ws.model.output.ProvinceOut;
import com.viettel.brcd.ws.model.output.ReasonChangeSimOut;
import com.viettel.brcd.ws.model.output.ReasonConfigOut;
import com.viettel.brcd.ws.model.output.ReasonConnectOut;
import com.viettel.brcd.ws.model.output.ReasonOut;
import com.viettel.brcd.ws.model.output.ReasonPreConnectOut;
import com.viettel.brcd.ws.model.output.RequestStatusOut;
import com.viettel.brcd.ws.model.output.RequestUpdateLocationOut;
import com.viettel.brcd.ws.model.output.ResultTeamCode;
import com.viettel.brcd.ws.model.output.ResultUpdateTaskOut;
import com.viettel.brcd.ws.model.output.SalaryOut;
import com.viettel.brcd.ws.model.output.SearchSubRequestOut;
import com.viettel.brcd.ws.model.output.SearchTaskRevokeOut;
import com.viettel.brcd.ws.model.output.SerialOut;
import com.viettel.brcd.ws.model.output.ServiceInfoOut;
import com.viettel.brcd.ws.model.output.ServiceOut;
import com.viettel.brcd.ws.model.output.ServicePaymentOut;
import com.viettel.brcd.ws.model.output.SourceTypeOut;
import com.viettel.brcd.ws.model.output.StaffDetailOut;
import com.viettel.brcd.ws.model.output.StaffInfoOut;
import com.viettel.brcd.ws.model.output.StaffSalaryDetail;
import com.viettel.brcd.ws.model.output.StationInfo;
import com.viettel.brcd.ws.model.output.StepInfoOut;
import com.viettel.brcd.ws.model.output.SubFbConfigOut;
import com.viettel.brcd.ws.model.output.SubInfoByISDNPreOut;
import com.viettel.brcd.ws.model.output.SubTypeOut;
import com.viettel.brcd.ws.model.output.TaskDetailToAssignOut;
import com.viettel.brcd.ws.model.output.TaskForUpdateOut;
import com.viettel.brcd.ws.model.output.TaskHistoryOut;
import com.viettel.brcd.ws.model.output.TaskInfoDetailOut;
import com.viettel.brcd.ws.model.output.TaskInfrasDetailOut;
import com.viettel.brcd.ws.model.output.TaskLookForOut;
import com.viettel.brcd.ws.model.output.TaskProgressOut;
import com.viettel.brcd.ws.model.output.TaskWarningOut;
import com.viettel.brcd.ws.model.output.UpdateResultOut;
import com.viettel.brcd.ws.model.output.UploadImageOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import java.util.List;
import com.viettel.brcd.ws.model.output.AccountByStaffOut;
import com.viettel.brcd.ws.model.output.AccountInfrasOut;
import com.viettel.brcd.ws.model.output.PotentialCustomerOut;
import com.viettel.brcd.ws.model.output.TechInforOut;

/**
 * @author vietnn6 a part
 */
public interface CmController {

    String getHelloWorld();

    LoginOut login(String user, String pass, String serial, String locale);

    ServiceOut getServices(String token, String locale);

    MaxRadiusOut getMaxRadius(String token, String serviceCode, String locale);

    GroupOut getProductGroups(String token, Long telecomServiceId, String locale);

    ProductOut getProducts(String token, Long telecomServiceId, String groupProduct, String locale);

    ProvinceOut getProvinces(String token, String locale);

    DistrictOut getDistricts(String token, String province, String locale);

    PrecinctOut getPrecincts(String token, String province, String district, String locale);

    PaymentOut getPaymethods(String token, String locale);

    ReasonConnectOut getRegTypes(String token, Long channelTypeId, String province, Long numPort, String productCode, String serviceType, String locale, String infraType, String vasProduct);

    LimitValueOut getQuotas(String token, String serviceType, String locale);

    DepositOut getDeposits(String token, String locale);

    PromotionOut getPromotions(String token, String telService, String locale);

    CustomerOut searchCustomer(String token, String idNo, String isdn, String account, Long pageSize, Long pageNo, String locale, String customerName, String contact);

    CustomTypeOut getBusTypes(String token, String locale);

    IdTypeOut getIdTypes(String token, String busType, String locale);

    LineTypeOut getLineTypes(String token, String serviceType, String locale);

    SubTypeOut getSubTypes(String token, String productCode, String serviceType, String locale);

    DebitOut getCustomerDebit(String token, Long custId, String locale);

    ResultTeamCode getTeamCode(String token, Long stationId, String locale);

    ReasonOut getSignContractReasons(String token, String telService, String locale);

    DomainOut getNickDomains(String token, String locale);

    BankOut searchBank(String token, String bankCode, String name, Long pageSize, Long pageNo, String locale);

    ApDomainOut getNoticeCharges(String token, String locale);

    ApDomainOut getPrintMethods(String token, String locale);

    ProductPricePlanOut getListProductPP(String token, Long offerId, String locale);

    LocalProductPpOut getListLocalProductPP(String token, Long offerId, String locale);

    ApParamOut getMaxLengthSurvey(String token, String seviceCode, String locale);

    RequestStatusOut getSearchRequestStatus(String token, String locale);

    ReasonOut getCancelRequestReasons(String token, String locale);

    SearchSubRequestOut searchSubRequest(String token, SearchSubRequestInput searchSubRequestInput, String locale);

    StepInfoOut getNextStepInfo(String token, Long subReqId, String locale);

    ReasonOut getCancelContractReasons(String token, String locale);

    UpdateResultOut addCustomer(String token, CustomerIn cusIn, String locale, List<ImageInput> lstImage, String staffCode);

    WSRespone signContract(String token, WSRequest wsRequest, String locale);

    WSRespone activeSubscriber(String token, WSRequest wsRequest, String locale);

    WSRespone cancelRequest(String token, CancelRequestInput cancelRequestInput, String locale);

    WSRespone cancelContract(CancelContractInput cancelContractInput, String locale);

    UpdateResultOut addRequestContract(String token, ContractIn input, String locale);

    UpdateResultOut setCustomHouseCoord(String token, Long custId, String xLocation, String yLocation, String locale, List<ImageInput> lstImageList);

    TaskToAssignOut getListTaskToAssign(String token, String locale, SearchTaskInput searchTaskInput);

    SourceTypeOut getSourceType(String token, String locale, int type);

    TaskProgressOut getTaskProgress(String token, String locale, int type);

    TaskDetailToAssignOut getTaskToAssign(String token, String locale, String strTaskMngtId, String endDateValue, String strAssignTo);

    StaffInfoOut getStaffForUpdateTask(String token, String locale, Long shopId);

    ServiceInfoOut getServiceForTask(String token, String locale);

    TaskForUpdateOut getListTaskForUpdate(String token, String locale, SearchTaskForUpdateInput searchTaskInput);

    TaskProgressOut getProgressForUpdateTask(String token, String locale, int type);

    TaskProgressOut getProgressForUpdateInfoTask(String token, String locale, int type);

    WSRespone assignTask(String token, String local, TaskToAssignIn taskToAssignIn);

    InforTaskToUpdateOut getInforTaskToUpdate(String token, String locale, String userLogin, String taskMngtId, String staffTaskMngtId, String telServiceId);

    InforResolveComplainOut getInforForResolveComplain(String token, String locale, Long custReqId, Long taskId);

    InforInfrastructureOut getInforForInfrastructure(String token, String locale, Long taskStaffMngtId, Long taskMngtId, Long telServiceId);

    CableBoxOut getCableBox(String token, String locale, Long boardId, Long dslamId, Long stationId, Long telserviceId, Long subId);

    WSRespone updateNoteTask(String token, String locale, UpdateNoteIn updateNoteIn);

    WSRespone updateInfrastructure(String token, String locale, InfrastructureUpdateIn infrastructureUpdateIn);

    ResultUpdateTaskOut updateProgress(String token, String locale, UpdateTaskIn updateTaskIn);

    GroupReasonOut getLstReason(String token, String locale, String reasonGroup);

    PortOut getListPortByDevice(String token, String locale, String dslamId, String telServiceId, String subId, String stationId);

    TaskLookForOut searchTaskForLookUp(String token, String locale, SearchTaskForLookUpInput searchTaskInput);

    TaskInfoDetailOut showDetailInfoTask(String token, String locale, String taskMngtId);

    TaskInfrasDetailOut showDetailInfrasTask(String token, String locale, String taskMngtId);

    TaskHistoryOut searchHistoryTask(String token, String locale, String startDate, String endDate, String taskMngtId);

    ComplainResultOut showComplaintResult(String token, String locale, String custReqId);

    WSRespone deleteTask(String token, String locale, String strTaskMngtId, String reason, String loginName, String shopCodeLogin);

    SearchTaskRevokeOut searchTaskForRevoke(String token, String locale, SearchTaskRevokeIn searchTask);

    PreRevokeOut detailRevokeTask(String token, String locale, String taskStaffMngtId, String telServiceId, String revokeFor);

    WSRespone revokeTask(String token, String locale, RevokeTaskIn revokeTaskIn);

    WSRespone createRequestNoInfra(String token, Long custId, String serviceType, Number Lat, Number Lng, String locale);

    UploadImageOut uploadImage(String token, String locale, String userLogin, Long subId, String account, List<ImageInput> lstImage);

    NotificationTaskOut getNotificationTaskOut(String token, String locale, String serial);

    CoordinateOut getListCoordinate(String token, Long reqId, String locale);

    CoordinateOut getListCoordinateBySubId(String token, Long subId, String locale);

    WSRespone saveLocationAgain(String token, Long subId, List<Coordinate> listCoordinate, Double cableLenght, String locale);

    TaskWarningOut getListWarningTaskForStaff(String token, String locale, String shopId, String pStaffId);

    InvestOut getInvest(String token, Long reqId, Double cableLenght, Long capacity, Long boxType, String locale);

    InvestOut getInvestBySubID(String token, Long subId, String locale);

    CustomerOut getListCustomerPre(String token, String idNo, String isdn, String service, String locale);

    UpdateResultOut addCustomerPre(String token, CustomerIn cusIn, String locale, List<ImageInput> lstImage, String staffCode);

    CustomTypeOut getBusTypesPre(String token, String locale);

    ReasonPreConnectOut getRegTypesPre(String token, String user, Long channelTypeId, String province, String productCode, String serviceType, String locale);

    WSRespone activeSubscriberPre(String token, ActivePreIn wsRequest, String locale);

    WSRespone changeSim(String token, ChangeSimIn wsRequest, String locale);

    SalaryOut getInfoSalary(String token, String staffId, String type, String locale);

    FileOut getFileByLink(String token, String link, String locale);

    LinkFileOut getLinkFilePolicy(String token, String fromDate, String toDate, String objectType, String serviceType, String formType, String locale);

    ReasonChangeSimOut getListReasonChangeSim(String token, String isdn, String type, String locale);

    PayContractOut getCustomerToPayment(String token, String account, String idNo, String locale, String customerName, String contact);

    InvoiceListOut getInvoiceList(String token, String staffCode, String shopCode, String locale);

    ServicePaymentOut searchServicePayment(String token, Long staffId, Long invoiceListId, Long contractId, String locale);

    PayAdvanceBillOut paymentForService(String token, PaymentInput payIn, String locale);

    CusLocationOut findCusByLocation(String token, String lat, String lng, String province, String district, String precinct, Long distance, String name, String locale);

    WSRespone uploadImageCus(String token, String locale, ImageCusInput imageIn);

    WSRespone syncTransLogClient(List<TransLogsClient> lstLog);

    ImageRespone viewImageContract(String account);

    ImageRespone viewImageCustomer(String cusId);

    LoginOut loginEncryptRSA(String user, String pass, String locale, String deviceId);

    WSRespone getCodeAuthenCusByIsdn(String isdn, String serial);

    WSRespone checkCodeAuthenCusBySerial(String code, String serial);

    WSRespone checkIdNoCus(String idNo);

    WSRespone checkIdNoCusPre(String idNo);

    SerialOut getRangeSerial(String token, Long shopId, Long stockModelId, String locale);

    WSRespone changePasswordOnBccs(String isdn, String oldPass, String newPass, String token, String locale);

    ReasonOut getReasonPayAdvance(String token, String locale, Long contractId, String isdn);

    PayAdvanceBillOut payAdvance(String token, String locale, Long reasonId, Long staffId, Long contractId, Long invoiceListId, String isdn);

    RequestUpdateLocationOut getListRequestUpdateCusLocation(String token, String date, Long status, Long duration,
            Long pageNo, String locale, String createUser, String custName);

    WSRespone rejectOrAprroveRUL(String token, CancelRULInput cancelRULInput, String locale);

    RequestUpdateLocationOut getListTimesOfChange(String token, Long custId, Long pageNo, String locale);

    WSRespone approveAll(String token, String locale, String date, Long duration, String createUser, String custName);

    SubFbConfigOut getListSubFbConfig(String token, Long status, Long statusConfig, Long duration, String account, String createUser, String locale, Long pageNo, String province);

    WSRespone rejectOrApproveSubFbConfig(String token, CancelSubFbConfigInput cancelSubFbConfigInput, String locale);

    LastUpdateConfigOut getLastUpdateConfig(String token, Long taskMngtId, String account, String locale);

    ReasonConfigOut getListReasonConfig(String token, String locale);

    HistorySubFbConfigOut getListHistoryConfig(String token, Long id, String locale);

    ImageInfrastructureOut getListImageType(String token, String locale, String action, Long taskMngtId);

    DashboardBroadbanTab getDashboardBroadbandInfo(String token, String locale, Long staffId, Long shopId, String isManagement);

    WSRespone configConnectImt(String token, String locale, Long staffId, Long taskManagementId, String splitter);

    InforInfrastructureOut getInfoInfraToView(String token, String locale, Long taskMngtId);

    StaffDetailOut getStaffInfo(String token, String staffCode, String locale);

    ParamOut getImageRequestLocationCustomer(Long requestId, String typeImage);

    ParamOut getCCActionDetail(String token, String locale, String account, String actionType);

    ParamOut getAccountOfCustomer(String token, String locale, Long customerId);

    ParamOut getActionDetailType(String token, String locale);

    WSRespone sendNimsSubActiveInfo(Long token, Long locale);

    WSRespone changeStationInfraStructure(Long staffId, String newStation, String connector, String portNo, String account);

    ParamOut getStationByTeam(String token, String locale, String account, Long subId, Long staffId);

    ParamOut getPortOdf(String token, String locale, Long odfId);

    StationInfo getStationInfoOfAccount(String token, String locale, Long subId);

    StaffInfoOut getStaffofShop(String token, String locale, Long shopId);

    WSRespone changeServiceAtoF(String token, String locale, String account, String loginName, String shopCodeLogin);

    ParamOut getStaffMapconnector(String token, String locale);

    StaffSalaryDetail getStaffSalaryDetail(String token, String locale, String staffCode, int month, int year);

    ParamOut getKpiApParam(String token, String locale);

    ParamOut getBrasIpPool(String token, String locale, String account);

    ParamOut getNotificationLog(String token, String locale, Long staffId);

    OwnerOut getOwnerSabay(String token, String locale, String msisdn_n3_p3, String actionType);

    OwnerOut getMemberOwnerSabay(String token, String locale, String msisdn_n3_p3, String msisdn);

    OwnerOut getOTP_Sabay(String token, String locale, String msisdn_n3_p3, String msisdn);

    OwnerOut sendOTP_Sabay(String token, String locale, String msisdn_n3_p3, String msisdn, String members, String OTP);

    OwnerOut CheckOwner_Sabay(String token, String locale, String msisdn_n3_p3, String msisdn);

    OwnerOut Checkmember_Sabay(String token, String locale, String msisdn_n3_p3, String msisdn, String param);

    ParamOut topUp(String token, String encrypt, String signature);

    ParamOut getPinCode(String token, String encrypt, String signature);

    LoginOut getToken(String username, String password);

    ParamOut verifyChangeKit(String token, String locale, String msisdnAgent, String msisdnCust, String newMsisdnCust, String lastSerial);

    ParamOut verifyChangeSim(String token, String locale, String msisdnAgent, String msisdnCust, String lastSerial);

    WSRespone changeSim4G(String token, String locale, String pinCode, String transId);

    WSRespone createNewInfoCus(String token, String locale, String serial, String isdn, String dealerIsdn, String idType, String fullName, String idNumber, String dob, List<ImageInput> lstImage, String puk);

    ParamOut confirmPincode(String token, String encrypt, String signature);

    ParamOut confirmTopUp(String token, String requestId, String date);

    ParamOut getAccountInformation(String token, String locale, String account);

    ParamOut updateRequestAnalysSOC(String token, String locale, Long complainId, Long result, String description, List<String> listErrorCode);

    WSRespone checkSocComplaint(String token, String locale, Long taskStaffMngtId, String progress, String staffCode);

    DebitTransOut getListDebitTransaction(String fromDate, String toDate, Long staffId, String locale, String token, String currency);

    DebitStaffOut getListDebitStaff(String shopCode, String staffCode, Long staffId, Long shopId,
            Long status, String locale, String token);

    DebitTransHistoryOut transHistory(String payCode, String payMethod, Long staffId,
            Long status, Long pageNo, String locale, String token);

    ClearDebitTransOut clearDebitTrans(String token, String debitTransId, Long staffId, Double amount, String locale);

    ClearDebitTransOut confirmDebitTrans(String token, String debitTransId, Double amount, String paymentCode, String txPaymentTokenId,
            String currencyCode, String paymentContent, Long staffId, String locale);

    ClearDebitTransOut transferDebit(String token, String debitTransId, Double amount, String paymentCode,
            Long receiveDebtStaffId, Long staffId, String locale);

    LockDebitTransOut checkLockTransaction(String token, Double amount, Long staffId, String locale, String currency);

    ParamOut checkInforBeforeChangeSim(String token, String locale, String isdn, String idNo);

    ParamOut getReasonChangeSim(String token, String locale, Long shopId, String isdn);

    ParamOut changeSimPre(String token, String locale, ChangeSimInput changeSimInput);

    ComplaintAcceptTypeOut getListComplaintAcceptType(String token, String locale, String code);

    ComplaintAcceptSourceOut getListComplaintAcceptSource(String token, String locale);

    ComplaintTypeOut getListComplaintType(Long groupId, String locale, String token);

    ComplaintGroupOut getListComplaintGroupType(Long parentId, String locale, String token);

    ComplaintGroupOut getListComplaintGroup(String locale, String token);

    CompDepartmentGroupOut getListCompTeam(String locale, String token, Long subId);

    WSRespone submitComplaint(String token, ComplaintInput complaintInput, String locale);

    SubAdslLeaselineOut getListSubAdslLeaseline(String locale, String token, String isdn);

    ServicePaymentOut getSubInfoForPayment(String isdn);

    ParamOut payAdvanceSub(String staffCode, String encrypt, String signature);

    ParamOut confirmPayAdvance(String staffCode, String tid, String date);

    ProductOut getListProductPre(String token, Long telecomServiceId, String groupProduct, String locale);

    WSRespone activeNewSubscriberPre(String token, ActiveSubscriberPreIn activeSubscriberPreIn, String locale);

    SubInfoByISDNPreOut findSubInfoByISDNPre(String token, String locale, String serviceType, String ISDN);

    ListReasonByCodePreOut getListReasonByCodePre(String token, String locale, Codes codes);

    OTPOut getOTPChangeInformationPre(String token, String locale, String isdn);

    ChangeInformationOut doChangeInformationPre(String token, String locale,
            String otp, Long subId,
            String serviceType, Long custId,
            String name, Long idType,
            String province, String address,
            String DOB, String Sex,
            String isdn, String contact,
            FormChangeInformation form,
            FormChangeInformation lstImageIDCard,
            FormChangeInformation lstOldImageIDCard,
            String reasonCode,
            Long reasonId, String idNo);

    AccountByStaffOut getLstAccountByStaff(String token, String locale, String staffID, String account, String infrasType, String infrasStatus,
            String bts, Long pageNo, String connectorCode, String deviceCode);

    AccountInfrasOut getInfoFrasByAccount(String token, String locale, String account, String infraType, String stationCode);

    WSRespone updateInfrasByStaff(String token, String locale, String account, String staffId,
            String odfIndoorCode, Long odfIndoorPort, Long odfIndoorPort2,
            String odfOutdoorCode, Long odfOutdoorPort, Long odfOutdoorPort2,
            Double lat, Double longy, Long lengthCable, Long portLogic, String snCode,
            String splitterCode, Long splitterPort, String boxCableCode, String boxCablePort,
            String infrasType, String stationCode, Long deviceId, Long portId);

    AccountInfrasOut getLocationStationCode(String token, String locale, String stationCode);

    ParamOut getProductVasFTTH();

    ProductOut getBunldleTvProduct();

    PayAdvanceBillOut printInvoiceBluetooth(String token, String locale, String accBundleTv);

    BundleTVInfo getAccBundleTvInfo(String token, String locale, String accBundleTv);

    WSRespone addUserBundleTv(String token, String locale, Long subId);

    ImageRespone viewImageCustomerLocation(String cusId);

    ParamOut checkServiceOnline(String token);

    ParamOut searchRestoreIsdn(String locale, String token, String isdn);

    ReasonOut getReasonRestoreIsdn(String locale, String token);

    WSRespone restoreIsdn(String locale, String token, ChangeSimInput changeSimInput);

    ParamOut getRecoverActionType(String token, String locale);

    ParamOut getRecoverReason(String token, String locale, String actionType);

    ParamOut updateRecoverReason(String token, String locale, Long contractId, String actionType, String reasonCode, String x, String y);

    TechInforOut getListConnectorByStation(String token, String locale, Long stationId, String stationCode);

    PotentialCustomerOut getListPotentialCust(
            String token,
            String locale,
            String name,
            String phoneNumber,
            String address,
            Long status,
            Long pageNo,
            String province,
            String kindOfCustomer);

    WSRespone addNewPotentialCust(
            String token,
            String locale,
            String name,
            String phoneNumber,
            String address,
            String province,
            String email,
            String otherOperator,
            Double fee,
            String latitude,
            String longitude,
            String kindOfCustomer,
            String expectedService,
            String expectedScale,
            String serviceUsed);

    WSRespone updatePotentialCust(
            String token,
            String locale,
            Long custId,
            String name,
            String phoneNumber,
            String address,
            String province,
            String email,
            String otherOperator,
            Double fee,
            String latitude,
            String longitude,
            Long status,
            String kindOfCustomer,
            String expectedService,
            String expectedScale,
            String serviceUsed);

    WSRespone deletePotentialCust(
            String token,
            String locale,
            Long custId);

    ParamOut buyData(String token, String encrypt, String signature);

    ParamOut extendTelecomService(String token, String encrypt, String signature);

    ParamOut changeDeviceAccFtth(String token, String locale, String account, String deviceCode);

    ParamOut buyDataVAS(String token, String encrypt, String signature);

    ParamOut buyDataVASPartner(String token, String encrypt, String signature);

    StaffSalaryDetail getStaffSalaryDetailAPBus(String token, String locale, String staffCode, int month, int year);

    PotentialCustomerOut getPotentialCustomerHistory(String token, String locale, String custId);

    ParamOut getListParamForPotentialCustomer(String token, String locale);

    //phuonghc 02072020
    ParamOut getPaymentReport(String token, String locale, String staffCode, String codeReport, String isChiefCenter);

    ParamOut getUnpaidDetail(String token, String locale, String staffCode, String codeReport, String isChiefCenter);

    //phuonghc 03092020
    ParamOut getListReasonByType(String token, String locale, String custId, String account);

    ParamOut doingChangeDeploymentAddress(String token, String locale, ChangeDeploymentAddressIn input);

    ParamOut getListAccountForSubscriber(String token, String locale, String custId);
}
