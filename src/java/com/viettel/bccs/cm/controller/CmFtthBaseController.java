/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.CmFtthBaseBussiness;
import com.viettel.bccs.cm.dao.TechnicalConnectorDAO;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.TechnicalConnector;
import com.viettel.bccs.cm.model.TechnicalStation;
import com.viettel.bccs.cm.model.ftth.CustomerBTSConnectorBean;
import com.viettel.bccs.cm.model.ftth.InterruptReasonDetail;
import com.viettel.bccs.cm.model.ftth.InterruptReasonType;
import com.viettel.bccs.cm.model.ftth.Interruption;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.CreateInterruptFtthInput;
import com.viettel.brcd.ws.model.input.PushSmsInterruptFtthInput;
import com.viettel.brcd.ws.model.output.CmFtthWSOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author partner1
 */
public class CmFtthBaseController extends BaseController {

    public CmFtthBaseController() {
        logger = Logger.getLogger(CmFtthBaseController.class);
    }

    public Shop getCurBranchUser(Session cmPosSession, Long staffId) {
        Shop shop = new CmFtthBaseBussiness().getCurBranchUser(cmPosSession, staffId);
        return shop;
    }

    public CmFtthWSOut getCurBranchUser(HibernateHelper hibernateHelper, String locale, Long staffId) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            Shop shop = getCurBranchUser(cmPosSession, staffId);
            result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            result.setCurBranchUser(shop);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CmFtthBaseController.getCurBranchUser:result=" + LogUtils.toJson(result));
        }
    }
    
    public List<InterruptReasonType> getReasonType(Session cmPosSession) {
        List<InterruptReasonType> lstReasonTypes = new CmFtthBaseBussiness().getReasonType(cmPosSession);
        return lstReasonTypes;
    }

    public CmFtthWSOut getReasonType(HibernateHelper hibernateHelper, String locale) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<InterruptReasonType> lstReasonTypes = getReasonType(cmPosSession);
            result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            result.setLstReasonTypes(lstReasonTypes);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CmFtthBaseController.getReasonType:result=" + LogUtils.toJson(result));
        }
    }

    public List<InterruptReasonDetail> getReasonDetail(Session cmPosSession, Long reasonTypeId) {
        List<InterruptReasonDetail> lstReasonDetails = new CmFtthBaseBussiness().getReasonDetail(cmPosSession, reasonTypeId);
        return lstReasonDetails;
    }

    public CmFtthWSOut getReasonDetail(HibernateHelper hibernateHelper, String locale, Long reasonTypeId) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<InterruptReasonDetail> lstReasonDetail = getReasonDetail(cmPosSession, reasonTypeId);
            result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            result.setLstReasonDetails(lstReasonDetail);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CmFtthBaseController.getReasonDetail:result=" + LogUtils.toJson(result));
        }
    }

    public List<Shop> getBranchList(Session cmPosSession) {
        List<Shop> lstShops = new CmFtthBaseBussiness().getBranchList(cmPosSession);
        return lstShops;
    }

    public CmFtthWSOut getBranchList(HibernateHelper hibernateHelper, String locale) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Shop> lstBranch = getBranchList(cmPosSession);
            result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            result.setLstBranch(lstBranch);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CmFtthBaseController.getBranchList:result=" + LogUtils.toJson(result));
        }
    }

    public List<Staff> getStaffOfBranch(Session cmPosSession, Long branchShopId) {
        List<Staff> lstStaffs = new CmFtthBaseBussiness().getStaffOfBranch(cmPosSession, branchShopId);
        return lstStaffs;
    }

    public CmFtthWSOut getStaffOfBranch(HibernateHelper hibernateHelper, String locale, Long branchShopId) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Staff> lstStaff = getStaffOfBranch(cmPosSession, branchShopId);
            result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            result.setLstStaffOfBranch(lstStaff);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CmFtthBaseController.getStaffOfBranch:result=" + LogUtils.toJson(result));
        }
    }
    
    public List<TechnicalStation> getBtsByBranch(Session cmPosSession, Long branchShopId) {
        List<TechnicalStation> lstStations = new CmFtthBaseBussiness().getBtsByBranch(cmPosSession, branchShopId);
        return lstStations;
    }

    public CmFtthWSOut getBtsByBranch(HibernateHelper hibernateHelper, String locale, Long branchShopId) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<TechnicalStation> lstStations = getBtsByBranch(cmPosSession, branchShopId);
            result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            result.setLstTechnicalStation(lstStations);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CmFtthBaseController.getBtsByBranch:result=" + LogUtils.toJson(result));
        }
    }

    public CmFtthWSOut getConnectorByStation(HibernateHelper hibernateHelper, String locale, Long stationId) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<TechnicalConnector> lstConnector = new TechnicalConnectorDAO().findByStationId(cmPosSession, stationId);
            result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            result.setLstTechnicalConnector(lstConnector);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CmFtthBaseController.getConnectorByStation:result=" + LogUtils.toJson(result));
        }
    }

    public CmFtthWSOut createInterruptFtth(HibernateHelper hibernateHelper, String locale, CreateInterruptFtthInput createInterruptFtthInput) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        Session cmPreSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            String errorMessage = new CmFtthBaseBussiness().createInterruptFtth(cmPosSession, cmPreSession, createInterruptFtthInput);
            if (errorMessage == null || "".equals(errorMessage)) {
                result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            } else {
                result = new CmFtthWSOut(Constants.ERROR_CODE_1, errorMessage);
            }
            cmPosSession.flush();
            cmPreSession.flush();
            commitTransactions(cmPosSession, cmPreSession);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession, cmPreSession);
            LogUtils.info(logger, "CmFtthBaseController.createInterruptFtth:result=" + LogUtils.toJson(result));
        }
    }
    
    public List<Interruption> getInterruptFtth(Session cmPosSession, String staffCode, Long reasonTypeId, Long reasonDetailId, String influenceScope, String fromDate, String toDate, Long shopId) throws Exception {
        List<Interruption> lstInterruption = new CmFtthBaseBussiness().getInterruptFtth(cmPosSession, staffCode, reasonTypeId, reasonDetailId, influenceScope, fromDate, toDate, shopId);
        return lstInterruption;
    }

    public CmFtthWSOut getInterruptFtth(HibernateHelper hibernateHelper, String locale, String staffCode, Long reasonTypeId, Long reasonDetailId, String influenceScope, String fromDate, String toDate, Long shopId) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Interruption> lstInterruption = getInterruptFtth(cmPosSession, staffCode, reasonTypeId, reasonDetailId, influenceScope, fromDate, toDate, shopId);
            result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            result.setLstInterruption(lstInterruption);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CmFtthBaseController.getInterruptFtth:result=" + LogUtils.toJson(result));
        }
    }
    
    public List<Interruption> getDetailInterruptFtth(Session cmPosSession, Long interruptId) throws Exception {
        List<Interruption> lstInterruption = new CmFtthBaseBussiness().getDetailInterruptFtth(cmPosSession, interruptId);
        return lstInterruption;
    }

    public CmFtthWSOut getDetailInterruptFtth(HibernateHelper hibernateHelper, String locale, Long interruptId) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Interruption> lstInterruption = getDetailInterruptFtth(cmPosSession, interruptId);
            result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            result.setLstInterruptionDetail(lstInterruption);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CmFtthBaseController.getDetailInterruptFtth:result=" + LogUtils.toJson(result));
        }
    }
    
    public List<Interruption> getHistoryInterruptFtth(Session cmPosSession, Long interruptId) throws Exception {
        List<Interruption> lstInterruption = new CmFtthBaseBussiness().getHistoryInterruptFtth(cmPosSession, interruptId);
        return lstInterruption;
    }

    public CmFtthWSOut getHistoryInterruptFtth(HibernateHelper hibernateHelper, String locale, Long interruptId) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Interruption> lstInterruption = getHistoryInterruptFtth(cmPosSession, interruptId);
            result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            result.setLstInterruptionHistory(lstInterruption);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CmFtthBaseController.getHistoryInterruptFtth:result=" + LogUtils.toJson(result));
        }
    }
    
    public CmFtthWSOut approveInterruptFtth(HibernateHelper hibernateHelper, String locale, Long interruptId, String staffCode) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            String errorMessage = new CmFtthBaseBussiness().approveInterruptFtth(cmPosSession, interruptId, staffCode);
            if (errorMessage == null || "".equals(errorMessage)) {
                result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            } else {
                result = new CmFtthWSOut(Constants.ERROR_CODE_1, errorMessage);
            }
            cmPosSession.flush();
            commitTransactions(cmPosSession);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CmFtthBaseController.approveInterruptFtth:result=" + LogUtils.toJson(result));
        }
    }
    
    public CmFtthWSOut cancelInterruptFtth(HibernateHelper hibernateHelper, String locale, Long interruptId, String staffCode) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            String errorMessage = new CmFtthBaseBussiness().cancelInterruptFtth(cmPosSession, interruptId, staffCode);
            if (errorMessage == null || "".equals(errorMessage)) {
                result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            } else {
                result = new CmFtthWSOut(Constants.ERROR_CODE_1, errorMessage);
            }
            cmPosSession.flush();
            commitTransactions(cmPosSession);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CmFtthBaseController.cancelInterruptFtth:result=" + LogUtils.toJson(result));
        }
    }
    
    public CmFtthWSOut pushSmsInterruptFtth(HibernateHelper hibernateHelper, String locale, PushSmsInterruptFtthInput pushSmsInterruptFtthInput) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        Session cmPreSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            String errorMessage = new CmFtthBaseBussiness().pushSmsInterruptFtth(cmPosSession, cmPreSession, pushSmsInterruptFtthInput);
            if (errorMessage == null || "".equals(errorMessage)) {
                result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            } else {
                result = new CmFtthWSOut(Constants.ERROR_CODE_1, errorMessage);
            }
            cmPosSession.flush();
            cmPreSession.flush();
            commitTransactions(cmPosSession, cmPreSession);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession, cmPreSession);
            LogUtils.info(logger, "CmFtthBaseController.pushSmsInterruptFtth:result=" + LogUtils.toJson(result));
        }
    }
    
    public List<CustomerBTSConnectorBean> countCustomerBTSConnector(Session cmPosSession, Long interruptId) throws Exception {
        List<CustomerBTSConnectorBean> lstCustomerBTSConnectorBean = new CmFtthBaseBussiness().countCustomerBTSConnector(cmPosSession, interruptId);
        return lstCustomerBTSConnectorBean;
    }

    public CmFtthWSOut countCustomerBTSConnector(HibernateHelper hibernateHelper, String locale, Long interruptId) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<CustomerBTSConnectorBean> lstCustomerBTSConnectorBean = countCustomerBTSConnector(cmPosSession, interruptId);
            result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            result.setLstCustomerBTSConnectorBean(lstCustomerBTSConnectorBean);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CmFtthBaseController.countCustomerBTSConnector:result=" + LogUtils.toJson(result));
        }
    }
    
    public List<CustomerBTSConnectorBean> countCustomerServiceBtsConnector(Session cmPosSession, Long interruptId, Long btsId, Long connectorId) throws Exception {
        List<CustomerBTSConnectorBean> lstCustomerBTSConnectorBean = new CmFtthBaseBussiness().countCustomerServiceBtsConnector(cmPosSession, interruptId, btsId, connectorId);
        return lstCustomerBTSConnectorBean;
    }

    public CmFtthWSOut countCustomerServiceBtsConnector(HibernateHelper hibernateHelper, String locale, Long interruptId, Long btsId, Long connectorId) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<CustomerBTSConnectorBean> lstCustomerBTSConnectorBean = countCustomerServiceBtsConnector(cmPosSession, interruptId, btsId, connectorId );
            result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            result.setLstCustomerBTSConnectorBean(lstCustomerBTSConnectorBean);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CmFtthBaseController.countCustomerBTSConnector:result=" + LogUtils.toJson(result));
        }
    }
    
    public CmFtthWSOut rejectInterruptFtth(HibernateHelper hibernateHelper, String locale, Long interruptId, String staffCode) {
        CmFtthWSOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            String errorMessage = new CmFtthBaseBussiness().rejectInterruptFtth(cmPosSession, interruptId, staffCode);
            if (errorMessage == null || "".equals(errorMessage)) {
                result = new CmFtthWSOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            } else {
                result = new CmFtthWSOut(Constants.ERROR_CODE_1, errorMessage);
            }
            cmPosSession.flush();
            commitTransactions(cmPosSession);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CmFtthWSOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred.1", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CmFtthBaseController.rejectInterruptFtth:result=" + LogUtils.toJson(result));
        }
    }
}
