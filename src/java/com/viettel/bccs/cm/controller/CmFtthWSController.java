/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.brcd.ws.model.input.CreateInterruptFtthInput;
import com.viettel.brcd.ws.model.input.PushSmsInterruptFtthInput;
import com.viettel.brcd.ws.model.output.CmFtthWSOut;

/**
 *
 * @author partner1
 */
public interface CmFtthWSController {
    
    CmFtthWSOut getCurBranchUser(String token, String locale, Long staffId);
    CmFtthWSOut getReasonType(String token, String locale);
    CmFtthWSOut getReasonDetail(String token, String locale, Long reasonTypeId);
    CmFtthWSOut getBranchList(String token, String locale);
    CmFtthWSOut getBtsByBranch(String token, String locale, Long branchShopId);
    CmFtthWSOut getStaffOfBranch(String token, String locale, Long branchShopId);
    CmFtthWSOut getConnectorByStation(String token, String locale, Long stationId);
    CmFtthWSOut createInterruptFtth(String token, String locale, CreateInterruptFtthInput createInterruptFtthInput);
    CmFtthWSOut getInterruptFtth(String token, String locale, String staffCode, Long reasonTypeId, Long reasonDetailId, String influenceScope, String fromDate, String toDate, Long shopId);
    CmFtthWSOut getDetailInterruptFtth(String token, String locale, Long interruptId);
    CmFtthWSOut getHistoryInterruptFtth(String token, String locale, Long interruptId);
    CmFtthWSOut approveInterruptFtth(String token, String locale, Long interruptId, String staffCode);
    CmFtthWSOut cancelInterruptFtth(String token, String locale, Long interruptId, String staffCode);
    CmFtthWSOut pushSmsInterruptFtth(String token, String locale, PushSmsInterruptFtthInput pushSmsInterruptFtthInput);
    CmFtthWSOut countCustomerBTSConnector(String token, String locale, Long interruptId);
    CmFtthWSOut countCustomerServiceBtsConnector(String token, String locale, Long interruptId, Long btsId, Long connectorId);
    CmFtthWSOut rejectInterruptFtth(String token, String locale, Long interruptId, String staffCode);
}
