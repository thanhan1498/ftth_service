package com.viettel.bccs.cm.controller;

import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.bussiness.ContractBussiness;
import com.viettel.bccs.cm.bussiness.StaffBussiness;
import com.viettel.bccs.cm.database.BO.ContractPayment;
import com.viettel.bccs.cm.database.BO.InvoiceList;
import com.viettel.bccs.supplier.payment.ContractSupplier;
import com.viettel.bccs.cm.database.DAO.InvoiceListUtilsDAO;
import com.viettel.bccs.cm.database.DAO.PaymentDAO;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.brcd.ws.model.input.CancelContractInput;
import com.viettel.brcd.ws.model.input.WSRequest;
import com.viettel.brcd.ws.model.output.InvoiceListOut;
import com.viettel.brcd.ws.model.output.PayContractOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.im.database.DAO.StaffDAO;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ContractController extends BaseController {

    public ContractController() {
        logger = Logger.getLogger(ContractController.class);
    }

    public WSRespone signContract(HibernateHelper hibernateHelper, WSRequest wsRequest, String locale) {
        WSRespone result = null;
        Session cmPosSession = null;
        boolean hasErr = false;
        try {
            LogUtils.info(logger, "ContractController.signContract:wsRequest=" + LogUtils.toJson(wsRequest));
            if (wsRequest == null) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid", locale));
                return result;
            }
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            String mess = new ContractBussiness().signContract(cmPosSession, wsRequest.getShopId(), wsRequest.getStaffId(), wsRequest.getNoticeCharge(),
                    wsRequest.getEmail(), wsRequest.getTelMobile(), wsRequest.getTelFax(), wsRequest.getPrintMethod(), wsRequest.getNickName(),
                    wsRequest.getNickDomain(), wsRequest.getPayMethod(), wsRequest.getBankCode(), wsRequest.getAccount(), wsRequest.getAccountName(),
                    wsRequest.getBankContractNo(), wsRequest.getBankContractDate(), wsRequest.getCustId(), wsRequest.getReasonId(), wsRequest.getProvince(),
                    wsRequest.getDistrict(), wsRequest.getPrecinct(), wsRequest.getReqId(), wsRequest.getReceiveInvoice(), locale);
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, mess);
                }
            }
            commitTransactions(cmPosSession);
            result = new WSRespone(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ContractController.signContract:result=" + LogUtils.toJson(result));
        }
    }

    public WSRespone cancelContract(HibernateHelper hibernateHelper, CancelContractInput cancelContractInput, String locale) {
        WSRespone result = null;
        Session cmPosSession = null;
        Session imSession = null;
        Session pmSession = null;
        boolean hasErr = false;
        try {
            LogUtils.info(logger, "ContractController.cancelContract:wsRequest=" + LogUtils.toJson(cancelContractInput));
            if (cancelContractInput == null) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid", locale));
                return result;
            }
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            pmSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            String mess = new ContractBussiness().cancelContract(cmPosSession, imSession, pmSession, cancelContractInput.getContractId(), cancelContractInput.getToken(), cancelContractInput.getReasonId(), cancelContractInput.getReqId(), locale);
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, mess);
                }
            }
            commitTransactions(cmPosSession, imSession, pmSession);
            result = new WSRespone(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession, imSession, pmSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession, imSession, pmSession);
            LogUtils.info(logger, "ContractController.cancelContract:result=" + LogUtils.toJson(result));
        }
    }

    public PayContractOut getCustomerToPayment(HibernateHelper hibernateHelper, String account, String idNo, String locale, String customerName, String contact) {
        PayContractOut result = new PayContractOut();
        Session paymentSession = null;
        try {
            LogUtils.info(logger, "ContractController.getCustomerToPayment:account=" + LogUtils.toJson(account));
            if ((account == null && idNo == null && customerName == null && contact == null) 
                    || (account.isEmpty() && idNo.isEmpty() && customerName.isEmpty() && contact.isEmpty())) {
                result = new PayContractOut(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid", locale));
                return result;
            }
            paymentSession = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            List<ContractPayment> lstContract = new ContractSupplier().getListOfContractPayment(paymentSession, account, idNo.toUpperCase(), customerName, contact);
            if (lstContract != null) {
                result.setLstContract(lstContract);
                result.setErrorCode(Constants.ERROR_CODE_0);
                result.setErrorDecription(LabelUtil.getKey("common.success", locale));
            } else {
                result = new PayContractOut(Constants.ERROR_CODE_1, LabelUtil.getKey("not.found.data", locale));
            }
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new PayContractOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(paymentSession);
            LogUtils.info(logger, "ContractController.cancelContract:result=" + LogUtils.toJson(result));
        }
    }

    public InvoiceListOut getInvoiceList(HibernateHelper hibernateHelper, String staffCode, String shopCode, String locale) {
        InvoiceListOut result = new InvoiceListOut();
        Session paymentSession = null;
        Session imSession = null;
        try {
            LogUtils.info(logger, "ContractController.getInvoiceList:staffCode=" + LogUtils.toJson(staffCode));
            if ((staffCode == null && shopCode == null) || (staffCode.isEmpty() && shopCode.isEmpty())) {
                result = new InvoiceListOut(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid", locale));
                return result;
            }
            paymentSession = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            // check only staff or collaborator can be get invoice
            /*boolean checkValid = new InvoiceListUtilsDAO().checkStaffOrCollaborator07(imSession, staffCode);
             if (!checkValid) {
             return new InvoiceListOut(Constants.ERROR_CODE_0, "You are not staff/collaborator to payment.");
             }*/
            //Staff staff = new StaffBussiness().findByCode(cmPosSession, staffCode);
            List<com.viettel.im.database.BO.Staff> staff = new StaffDAO(imSession).findByProperty("staffCode", staffCode);
            if (staff == null || staff.isEmpty()) {
                return new InvoiceListOut(Constants.ERROR_CODE_1, "Your staff is not exists");
            }

            Double checkLimit = new PaymentDAO().getAmountLimit(imSession, staff.get(0).getStaffId());
            if (checkLimit == null && !Constants.SHOP_TYPE_AGENT_DELEGATE.equals(staff.get(0).getType())) {
                return new InvoiceListOut(Constants.ERROR_CODE_1, "You have no account agent");
            }
            if (checkLimit != null && checkLimit <= 0) {
                return new InvoiceListOut(Constants.ERROR_CODE_1, "Your limit amount is over");
            }
            List<InvoiceList> lstContract = new InvoiceListUtilsDAO().getAvailableInvoiceList(imSession, paymentSession, shopCode, staffCode);
            if (lstContract != null && !lstContract.isEmpty()) {
                result.setLstInvoice(lstContract);
                result.setErrorCode(Constants.ERROR_CODE_0);
                result.setErrorDecription(LabelUtil.getKey("common.success", locale));
            } else {
                result = new InvoiceListOut(Constants.ERROR_CODE_0, "All invoices do not exist for you, so you can not payment.");
            }
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new InvoiceListOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(paymentSession, imSession);
            LogUtils.info(logger, "ContractController.cancelContract:result=" + LogUtils.toJson(result));
        }
    }
}
