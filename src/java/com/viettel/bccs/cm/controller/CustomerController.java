package com.viettel.bccs.cm.controller;

import com.google.gson.Gson;
import com.viettel.bccs.api.Task.BO.Authenticate;
import com.viettel.bccs.api.Task.DAO.ContractDAO;
import com.viettel.bccs.api.Task.DAO.CustomerDAO;
import com.viettel.bccs.api.debit.DAO.DebitBusiness;
import com.viettel.bccs.bo.cm.pos.Subscriber;
import com.viettel.bccs.cm.bussiness.CustomerBussiness;
import com.viettel.bccs.cm.bussiness.RequestUpdateLocationBusiness;
import com.viettel.bccs.cm.bussiness.TokenBussiness;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.Token;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.bccs.supplier.cm.pre.PreChangeSimSupplier;
import com.viettel.brcd.common.util.FileSaveProcess;
import com.viettel.brcd.ws.model.input.CustomerIn;
import com.viettel.brcd.ws.model.input.ImageCusInput;
import com.viettel.brcd.ws.model.input.ImageInput;
import com.viettel.brcd.ws.model.output.CusLocationOut;
import com.viettel.brcd.ws.model.output.CustomerOut;
import com.viettel.brcd.ws.model.output.UpdateResultOut;
import com.viettel.brcd.ws.model.output.VBtsAccount;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import com.viettel.bccs.cm.bussiness.ApParamBussiness;
import com.viettel.bccs.cm.bussiness.common.DecryptBusiness;
import static com.viettel.bccs.cm.controller.EmoneyController.formatTransCode;
import com.viettel.bccs.cm.dao.ActionLogPrDAO;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.model.ActionLogPr;
import com.viettel.bccs.cm.dao.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.CustomerExtend;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.supplier.ApParamSupplier;
import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.bccs.merchant.dao.ActionLogDAO;
import com.viettel.bccs.cm.supplier.RequestChangeInformationSupplier;
import com.viettel.bccs.merchant.dao.MerchantDAO;
import com.viettel.bccs.merchant.dao.TransactionLogDAO;
import com.viettel.bccs.merchant.model.Merchant;
import com.viettel.brcd.dao.im.IMDAO;
import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.brcd.ws.model.output.AccountInformation;
import com.viettel.brcd.ws.model.output.CcActionInformation;
import com.viettel.brcd.ws.model.output.DataTopUpPinCode;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.common.ExchMsg;
import com.viettel.im.database.BO.AccountAgent;
import com.viettel.bccs.merchant.model.Discount;
import com.viettel.bccs.merchant.model.PinCode;
import com.viettel.brcd.common.util.ResourceBundleUtils;
import com.viettel.brcd.dao.im.SaleTransNotInvoice;
import com.viettel.brcd.ws.model.output.SubInfoSms;
import com.viettel.im.database.BO.Price;
import com.viettel.im.database.BO.SaleServices;
import com.viettel.im.database.BO.StockTrans;
import com.viettel.im.database.BO.SaleTrans;
import com.viettel.im.database.BO.SaleTransDetail;
import com.viettel.im.database.BO.SaleTransSerial;
import com.viettel.im.database.BO.StockModel;
import com.vtc.provisioning.client.Exchange;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class CustomerController extends BaseController {

    public CustomerController() {
        logger = Logger.getLogger(CustomerController.class);
    }

    public CustomerOut searchCustomer(HibernateHelper hibernateHelper, String idNo, String isdn, String account, Long pageSize, Long pageNo, String locale, String customerName, String contact) {
        CustomerOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            CustomerBussiness customerBussiness = new CustomerBussiness();
            Long count = customerBussiness.count(cmPosSession, idNo, isdn, account);
            if (count == null || count <= 0L) {
                result = new CustomerOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
                return result;
            }
            Integer start = getStart(pageSize, pageNo);
            Integer max = getMax(pageSize, pageNo, count);
            List<CustomerExtend> customers = customerBussiness.find(cmPosSession, idNo, isdn, account, start, max, customerName, contact);
            if (customers == null || customers.isEmpty()) {
                result = new CustomerOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
                return result;
            }
            List<CustomerIn> lstCustomer = new ArrayList<CustomerIn>();
            for (CustomerExtend customer : customers) {
                if (customer != null) {
                    lstCustomer.add(new CustomerIn(customer));
                }
            }
            result = new CustomerOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lstCustomer);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CustomerOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CustomerController.searchCustomer:result=" + LogUtils.toJson(result));
        }
    }

    public UpdateResultOut addCustomer(HibernateHelper hibernateHelper, String tokenVal, CustomerIn cusIn, String locale, List<ImageInput> lstImage, String staffCode) {
        UpdateResultOut result = null;
        Session cmPosSession = null;
        boolean hasErr = false;
        try {
            LogUtils.info(logger, "CustomerController.addCustomer:tokenVal=" + tokenVal + ";cusIn=" + LogUtils.toJson(cusIn));
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            String mess;
            Token token = new TokenBussiness().findByToken(cmPosSession, tokenVal);
            if (token == null) {
                mess = LabelUtil.getKey("common.token.invalid", locale);
                result = new UpdateResultOut(Constants.ERROR_CODE_1, mess, false);
                return result;
            }
            if (cusIn.getCustId() != null && cusIn.getCustId() > 0) {
                mess = new CustomerBussiness().update(cmPosSession, cusIn.getCustId(), cusIn.getIdType(), cusIn.getIdNo(), cusIn.getCustomType(), cusIn.getName(),
                        cusIn.getBirthDate(), cusIn.getIdIssueDate(), cusIn.getIdIssuePlace(), token.getStaffId(), cusIn.getProvinceCode(),
                        cusIn.getDistrictCode(), cusIn.getPrecinctCode(), cusIn.getStreetBlock(), cusIn.getStreetBlockName(), cusIn.getSex(), cusIn.getLat(), cusIn.getLng(), cusIn.getTelMobile(), locale, lstImage, staffCode);
            } else {
                mess = new CustomerBussiness().insert(cmPosSession, cusIn.getIdType(), cusIn.getIdNo(), cusIn.getCustomType(), cusIn.getName(),
                        cusIn.getBirthDate(), cusIn.getIdIssueDate(), cusIn.getIdIssuePlace(), token.getStaffId(), cusIn.getProvinceCode(),
                        cusIn.getDistrictCode(), cusIn.getPrecinctCode(), cusIn.getStreetBlock(), cusIn.getStreetBlockName(), cusIn.getSex(), cusIn.getLat(), cusIn.getLng(), cusIn.getTelMobile(), locale, lstImage, staffCode);
            }
            Long custId = ConvertUtils.toLong(mess);
            if (mess != null && custId == null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    hasErr = true;
                    result = new UpdateResultOut(Constants.ERROR_CODE_1, mess, false);
                    return result;
                }
            }

            commitTransactions(cmPosSession);

            LogUtils.info(logger, "CustomerController.addCustomer:custId=" + custId);
            result = new UpdateResultOut(Constants.ERROR_CODE_0, ConvertUtils.toStringValue(custId), true);
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new UpdateResultOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CustomerController.addCustomer:result=" + LogUtils.toJson(result));
        }
    }

    public UpdateResultOut updateCustomer(HibernateHelper hibernateHelper, String tokenVal, CustomerIn cusIn, String locale, List<ImageInput> lstImage, String staffCode) {
        UpdateResultOut result = null;
        Session cmPosSession = null;
        boolean hasErr = false;
        try {
            LogUtils.info(logger, "CustomerController.updateCustomInfor:tokenVal=" + tokenVal + ";cusIn=" + LogUtils.toJson(cusIn));
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            String mess;
            Token token = new TokenBussiness().findByToken(cmPosSession, tokenVal);
            if (token == null) {
                mess = LabelUtil.getKey("common.token.invalid", locale);
                result = new UpdateResultOut(Constants.ERROR_CODE_1, mess, false);
                return result;
            }

            mess = new CustomerBussiness().update(cmPosSession, cusIn.getCustId(), cusIn.getIdType(), cusIn.getIdNo(), cusIn.getCustomType(), cusIn.getName(),
                    cusIn.getBirthDate(), cusIn.getIdIssueDate(), cusIn.getIdIssuePlace(), token.getStaffId(), cusIn.getProvinceCode(),
                    cusIn.getDistrictCode(), cusIn.getPrecinctCode(), cusIn.getStreetBlock(), cusIn.getStreetBlockName(), cusIn.getSex(), cusIn.getLat(), cusIn.getLng(), cusIn.getTelMobile(), locale, lstImage, staffCode);
            Long custId = ConvertUtils.toLong(mess);
            if (mess != null && custId == null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    hasErr = true;
                    result = new UpdateResultOut(Constants.ERROR_CODE_1, mess, false);
                    return result;
                }
            }

            commitTransactions(cmPosSession);

            LogUtils.info(logger, "CustomerController.updateCustomInfor:custId=" + custId);
            result = new UpdateResultOut(Constants.ERROR_CODE_0, ConvertUtils.toStringValue(custId), true);
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new UpdateResultOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            LogUtils.info(logger, "CustomerController.updateCustomInfor:result=" + LogUtils.toJson(result));
        }
    }

    /*KHR
     * @Author:DuyetDK
     * @Since: 23/01/2018
     * @Desc: Set location in House Customer
     * 
     */
    public UpdateResultOut setCustomHouseCoord(HibernateHelper hibernateHelper, Long custId, String xLocation, String yLocation, String locale, String token, List<ImageInput> lstImageList) {
        UpdateResultOut result = null;
        Session cmPosSession = null;
        Session imPosSession = null;
        boolean hasErr = false;
        Customer cus;
        try {
            LogUtils.info(logger, "CustomerController.setCustomHouseCoord:custId=" + custId + ";xLocation=" + xLocation + ";yLocation=" + yLocation);
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            String mess;
            Token tokenObj = new TokenBussiness().findByToken(cmPosSession, token);
            String userUpdate = "";
            String staffOwnType = "";
            Long staffId = null;
            if (tokenObj != null) {
                userUpdate = tokenObj.getUserName();
                staffId = tokenObj.getStaffId();
            }
            /*
             * @author cuongdm
             * @since 180212
             * @des bo sung co kiem tra update location can approve
             */
            List<ApParam> listApParam = new ApParamBussiness().findByType(cmPosSession, Constants.FLAG_NEW_FLOW_UPDATE_LOCATION);
            VBtsAccount vbts = new VBtsAccount();
            if (listApParam != null && !listApParam.isEmpty() && listApParam.get(0).getParamValue().equals(Constants.STATUS_USE.toString())) {
                List<com.viettel.im.database.BO.Staff> staff = new com.viettel.im.database.DAO.StaffDAO(imPosSession).findByProperty("staffId", staffId);
                com.viettel.im.database.BO.Shop shop = new com.viettel.im.database.DAO.ShopDAO(imPosSession).findById(staff.get(0).getShopId());
                cus = new CustomerBussiness().findById(cmPosSession, custId);
                vbts = new CustomerBussiness().findCoorByCustId(cmPosSession, custId);
                if (new RequestUpdateLocationBusiness().isRequestWaiting30days(cmPosSession, custId)) {
                    result = new UpdateResultOut(Constants.ERROR_CODE_1, LabelUtil.getKey("request.update.location.waiting", locale), false);
                    return result;
                }
                if (new RequestUpdateLocationBusiness().isRequestWaitingOver30days(cmPosSession, custId)) {
                    if (staffId != null && staffId > 0L && staff.get(0).getStaffCode() != null && !staff.get(0).getStaffCode().trim().isEmpty()
                            && !shop.getShopCode().trim().isEmpty() && shop.getShopCode() != null) {
                        mess = new RequestUpdateLocationBusiness().rejectWaitingOver30days(cmPosSession, custId, staff.get(0).getStaffCode(), shop.getShopCode(), locale);
                        if (mess != null) {
                            mess = mess.trim();
                            if (!mess.isEmpty()) {
                                hasErr = true;
                            }
                        }
                    }
                }
                if (cus != null && vbts != null && cus.getxLocation() != null && cus.getyLocation() != null) {
                    if (vbts.getLatth() != null && vbts.getLongth() != null
                            && !vbts.getLatth().trim().isEmpty() && !vbts.getLongth().trim().isEmpty()) {
                        double d = distanceBetween2Points(Double.parseDouble(vbts.getLatth()),
                                Double.parseDouble(vbts.getLongth()), Double.parseDouble(xLocation),
                                Double.parseDouble(yLocation));
                        double d1 = Double.parseDouble(ResourceUtils.getResource("DISTANCE"));
                        int reval = Double.compare(d, d1);
                        if (reval > 0) {
                            if (staff.get(0).getStaffOwnType() != null && !staff.get(0).getStaffOwnType().trim().isEmpty()) {
                                staffOwnType = staff.get(0).getStaffOwnType();
                            }
                            mess = new RequestUpdateLocationBusiness().sendRequestUpdateLocation(
                                    cmPosSession, custId, cus.getName(), cus.getAddress(),
                                    staffId, xLocation, yLocation, vbts.getLatth(), vbts.getLongth(), locale,
                                    userUpdate, shop, staffOwnType, LabelUtil.formatKey("validate.error.distance", locale, d / 1000),
                                    lstImageList, vbts.getAccount(), vbts.getContractId());
                            if (mess != null) {
                                mess = mess.trim();
                                if (!mess.isEmpty()) {
                                    hasErr = true;
                                    result = new UpdateResultOut(Constants.ERROR_CODE_1, mess, false);
                                    return result;
                                }
                            }
                            commitTransactions(cmPosSession);
                            result = new UpdateResultOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("validate.error.distance", locale, d / 1000), false);
                            return result;
                        }
                    }
                }
            }

            /*duyetdk - luu anh khi cap nhat toa do nha KH*/
            List<String> lstAccount = new RequestUpdateLocationBusiness().getAccountByCustId(cmPosSession, custId);
            List<String> lstMobile = new RequestUpdateLocationBusiness().getMobileByCustId(cmPosSession, custId);
            List<String> lstHomephone = new RequestUpdateLocationBusiness().getHomephoneByCustId(cmPosSession, custId);

            String homephone = lstHomephone != null && !lstHomephone.isEmpty() ? String.valueOf(lstHomephone.get(0)) : "";
            String mobile = lstMobile != null && !lstMobile.isEmpty() ? String.valueOf(lstMobile.get(0)) : homephone;
            String account = lstAccount != null && !lstAccount.isEmpty() ? String.valueOf(lstAccount.get(0)) : mobile;

            mess = new CustomerBussiness().setCustomHouseCoord(cmPosSession, custId, xLocation, yLocation, locale, userUpdate,
                    lstImageList, vbts != null ? vbts.getContractId() : 0L, vbts != null && vbts.getAccount() != null ? vbts.getAccount() : account);
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    hasErr = true;
                    result = new UpdateResultOut(Constants.ERROR_CODE_1, mess, false);
                    return result;
                }
            }
            commitTransactions(cmPosSession);
            result = new UpdateResultOut(Constants.ERROR_CODE_0, ConvertUtils.toStringValue(custId), true);
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new UpdateResultOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            closeSessions(imPosSession);
            LogUtils.info(logger, "CustomerController.setCustomHouseCoord:result=" + LogUtils.toJson(result));
        }
    }

    public CustomerOut getListCustomerPre(HibernateHelper hibernateHelper, String idNo, String isdn, String service, String locale) {
        CustomerOut result = null;
        Session cmPreSession = null;
        try {
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            CustomerBussiness customerBussiness = new CustomerBussiness();
            List<Customer> customers = customerBussiness.find(cmPreSession, idNo, isdn, service);
            if (customers == null || customers.isEmpty()) {
                result = new CustomerOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
                return result;
            }
            List<CustomerIn> lstCustomer = new ArrayList<CustomerIn>();
            for (Customer customer : customers) {
                if (customer != null) {
                    lstCustomer.add(new CustomerIn(customer));
                }
            }
            result = new CustomerOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lstCustomer);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CustomerOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPreSession);
            LogUtils.info(logger, "CustomerController.searchCustomer:result=" + LogUtils.toJson(result));
        }
    }

    public UpdateResultOut addCustomerPre(HibernateHelper hibernateHelper, String tokenVal, CustomerIn cusIn, String locale, List<ImageInput> lstImage, String staffCode) {
        UpdateResultOut result = null;
        Session cmPreSession = null;
        Session cmPosSession = null;
        boolean hasErr = false;
        try {
            LogUtils.info(logger, "CustomerController.addCustomerMobile:tokenVal=" + tokenVal + ";cusIn=" + LogUtils.toJson(cusIn));
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            String mess;
            Token token = new TokenBussiness().findByToken(cmPosSession, tokenVal);
            if (token == null) {
                mess = LabelUtil.getKey("common.token.invalid", locale);
                result = new UpdateResultOut(Constants.ERROR_CODE_1, mess, false);
                return result;
            }
            if (cusIn.getCustId() != null && cusIn.getCustId() > 0) {
                mess = new CustomerBussiness().updatePre(cmPreSession, cusIn.getCustId(), cmPosSession, cusIn.getIdType(), cusIn.getIdNo(), cusIn.getCustomType(), cusIn.getName(),
                        cusIn.getBirthDate(), cusIn.getIdIssueDate(), cusIn.getIdIssuePlace(), token.getStaffId(), cusIn.getProvinceCode(),
                        cusIn.getDistrictCode(), cusIn.getPrecinctCode(), cusIn.getStreetBlock(), cusIn.getStreetBlockName(), cusIn.getSex(), cusIn.getLat(), cusIn.getLng(), cusIn.getTelMobile(), locale, lstImage, staffCode);
            } else {
                mess = new CustomerBussiness().insertPre(cmPreSession, cmPosSession, cusIn.getIdType(), cusIn.getIdNo(), cusIn.getCustomType(), cusIn.getName(),
                        cusIn.getBirthDate(), cusIn.getIdIssueDate(), cusIn.getIdIssuePlace(), token.getStaffId(), cusIn.getProvinceCode(),
                        cusIn.getDistrictCode(), cusIn.getPrecinctCode(), cusIn.getStreetBlock(), cusIn.getStreetBlockName(), cusIn.getSex(), cusIn.getLat(), cusIn.getLng(), cusIn.getTelMobile(), locale, lstImage, staffCode);
            }
            Long custId = ConvertUtils.toLong(mess);
            if (mess != null && custId == null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    hasErr = true;
                    result = new UpdateResultOut(Constants.ERROR_CODE_1, mess, false);
                    return result;
                }
            }

            commitTransactions(cmPreSession, cmPosSession);

            LogUtils.info(logger, "CustomerController.addCustomerMobile:custId=" + custId);
            result = new UpdateResultOut(Constants.ERROR_CODE_0, ConvertUtils.toStringValue(custId), true);
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new UpdateResultOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPreSession, cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPreSession, cmPosSession);
            LogUtils.info(logger, "CustomerController.addCustomer:result=" + LogUtils.toJson(result));
        }
    }

    public CusLocationOut findCusByLocation(HibernateHelper hibernateHelper, String lat, String lng, String province, String district, String precinct, Long distance, String name, String locale) {
        CusLocationOut result = null;
        Session cmPreSession = null;
        try {
            if (lat.isEmpty() && lng.isEmpty()
                    && province.isEmpty()
                    && district.isEmpty() && precinct.isEmpty()) {
                result = new CusLocationOut(Constants.ERROR_CODE_2, "Input is not empty");
                return result;
            }
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            CustomerDAO customerBussiness = new CustomerDAO();
            List<com.viettel.bccs.api.Task.BO.Customer> lstCustomer = new ArrayList<com.viettel.bccs.api.Task.BO.Customer>();
            if (!lat.isEmpty() && !lng.isEmpty()) {
                lstCustomer = customerBussiness.findCusByLocation(cmPreSession, lat, lng, distance, name);
            } else if (!province.isEmpty()) {
                lstCustomer = customerBussiness.findByIdArea(cmPreSession, province, district, precinct, name);
            }
            if (lstCustomer == null || lstCustomer.isEmpty()) {
                result = new CusLocationOut(Constants.ERROR_CODE_1, "Data not found");
                return result;
            }
            result = new CusLocationOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lstCustomer);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new CusLocationOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPreSession);
            LogUtils.info(logger, "CustomerController.searchCustomer:result=" + LogUtils.toJson(result));
        }
    }

    public WSRespone uploadImage(HibernateHelper hibernateHelper, String local, ImageCusInput imageInp) {
        WSRespone uploadImageOut = new WSRespone();
//        Session sessionPos = null;
        try {
//            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            FileSaveProcess fileProc = new FileSaveProcess();
            boolean res = fileProc.saveFileCus(imageInp.getImageName(), imageInp.getImageData());
            if (!res) {
                uploadImageOut.setErrorCode("-99");
                uploadImageOut.setErrorDecription("Upload image fail");
                return uploadImageOut;
            }
//            CustomerDAO customerBussiness = new CustomerDAO();
            int check = 1;
//            int check = customerBussiness.insertCusImage(sessionPos, imageInp.getCustId(), imageInp.getImageName(), imageInp.getUserCreate());
            if (check == 0) {
                uploadImageOut.setErrorCode("-99");
                uploadImageOut.setErrorDecription("Insert DB fail image " + imageInp.getImageName());
                return uploadImageOut;
            }
            uploadImageOut.setErrorCode("-99");
            uploadImageOut.setErrorDecription("Insert DB fail image " + imageInp.getImageName());
        } catch (Exception ex) {
            logger.error("uploadImage: " + ex.getMessage());
            uploadImageOut.setErrorCode(Constants.ERROR_CODE_0);
            uploadImageOut.setErrorDecription("Upload image success");
        } finally {
//            if (sessionPos != null) {
//                sessionPos.close();
//            }
        }
        return uploadImageOut;
    }

    public WSRespone getCodeAuthenCusByIsdn(HibernateHelper hibernateHelper, String isdn, String serial) {
        WSRespone reponse = new WSRespone();
        Session sessionPos = null;
        Session sessionPre = null;
        try {
            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            sessionPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            if (isdn == null || isdn.isEmpty()) {
                reponse.setErrorCode(Constants.ERROR_CODE_1);
                reponse.setErrorDecription("Isdn is not null");
                return reponse;
            }
            isdn = isdn.startsWith("0") ? isdn.substring(1) : isdn;
            isdn = isdn.startsWith("855") ? isdn.substring(3) : isdn;
            // get info sim prepaid
            Subscriber sub = new PreChangeSimSupplier().getSubMbPre(sessionPre.connection(), isdn);
            if (sub == null) {
                reponse.setErrorCode(Constants.ERROR_CODE_1);
                reponse.setErrorDecription("Isdn is not active");
                return reponse;
            }
            CustomerDAO customer = new CustomerDAO();
            Random rn = new Random();
            String code = customer.generateStringRandom(rn, "0123456789", 6);
            reponse.setErrorCode(Constants.ERROR_CODE_0);
            reponse.setErrorDecription("Get code success,you check code on your phone");
            // update old code status = 0
            customer.updateAuthenticate(sessionPre, serial);
            // insert code
            customer.insertAuthenticate(sessionPre, code, serial, isdn);
            // send message code
            String msCode = code + " is your verification code for mBCCS valid in 1 minute, thank you!";
            customer.sendSMSToCus(sessionPos, isdn, msCode);
        } catch (Exception ex) {
            logger.error("getCodeAuthenCusByIsdn: " + ex.getMessage());
            reponse.setErrorCode(Constants.ERROR_CODE_1);
            reponse.setErrorDecription("Error");
            rollBackTransactions(sessionPos, sessionPre);
        } finally {
            commitTransactions(sessionPos, sessionPre);
            closeSessions(sessionPos, sessionPre);
        }
        return reponse;
    }

    public WSRespone checkCodeAuthenCusBySerial(HibernateHelper hibernateHelper, String code, String serial) {
        WSRespone reponse = new WSRespone();
        Session sessionPre = null;
        try {
            sessionPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            if (code == null || code.isEmpty()) {
                reponse.setErrorCode(Constants.ERROR_CODE_1);
                reponse.setErrorDecription("Code is not null");
                return reponse;
            }
            if (serial == null || serial.isEmpty()) {
                reponse.setErrorCode(Constants.ERROR_CODE_1);
                reponse.setErrorDecription("Serial is not null");
                return reponse;
            }
            CustomerDAO customer = new CustomerDAO();
            // check code
            Authenticate authenticate = customer.checkAuthenticate(sessionPre, serial, code);
            if (authenticate == null) {
                reponse.setErrorCode(Constants.ERROR_CODE_2);
                reponse.setErrorDecription("Your code is not exists");
                return reponse;
            } else {
                // update status code = 0
                customer.updateAuthenticate(sessionPre, serial);
                Date createDate = authenticate.getCreateDate();
                Date nowDate = customer.getSysdate(sessionPre);
                System.out.println("createDate" + createDate);
                System.out.println("nowDate" + nowDate);
                long timeOut = (nowDate.getTime() - createDate.getTime()) / (1000);
                System.out.println("timeOut" + timeOut);

                if (timeOut > 60) {
                    reponse.setErrorCode(Constants.ERROR_CODE_2);
                    reponse.setErrorDecription("You overed 1 minute check code");
                    return reponse;
                }
            }
            reponse.setErrorCode(Constants.ERROR_CODE_0);
            reponse.setErrorDecription("Success");
            return reponse;

        } catch (Exception ex) {
            logger.error("checkCodeAuthenCusBySerial: " + ex.getMessage());
            reponse.setErrorCode(Constants.ERROR_CODE_1);
            reponse.setErrorDecription("Error");
            rollBackTransactions(sessionPre);
        } finally {
            commitTransactions(sessionPre);
            closeSessions(sessionPre);
        }
        return reponse;
    }

    public WSRespone checkIdNoCusPre(HibernateHelper hibernateHelper, String idNo) {
        WSRespone reponse = new WSRespone();
        Session sessionPre = null;
        try {
            sessionPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            //<editor-fold defaultstate="collapsed" desc="idNo">
            if (idNo == null || idNo.isEmpty()) {
                reponse.setErrorCode(Constants.ERROR_CODE_1);
                reponse.setErrorDecription("IdNo is not null");
                return reponse;
            }

            if (new CustomerBussiness().isUsedIdNoPre(sessionPre, idNo)) {
                reponse.setErrorCode(Constants.ERROR_CODE_1);
                reponse.setErrorDecription("This idNo exists already");
                return reponse;
            }
            //</editor-fold>
            reponse.setErrorCode(Constants.ERROR_CODE_0);
            reponse.setErrorDecription("Success");
            return reponse;
        } catch (Exception ex) {
            logger.error("checkIdNoCus: " + ex.getMessage());
            reponse.setErrorCode(Constants.ERROR_CODE_1);
            reponse.setErrorDecription("Error");
        } finally {
            commitTransactions(sessionPre);
            closeSessions(sessionPre);
        }
        return reponse;
    }

    public WSRespone checkIdNoCus(HibernateHelper hibernateHelper, String idNo) {
        WSRespone reponse = new WSRespone();
        Session sessionPos = null;
        try {
            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            //<editor-fold defaultstate="collapsed" desc="idNo">
            if (idNo == null || idNo.isEmpty()) {
                reponse.setErrorCode(Constants.ERROR_CODE_1);
                reponse.setErrorDecription("IdNo is not null");
                return reponse;
            }

            if (new CustomerBussiness().isUsedIdNo(sessionPos, idNo)) {
                reponse.setErrorCode(Constants.ERROR_CODE_1);
                reponse.setErrorDecription("This idNo exists already");
                return reponse;
            }
            //</editor-fold>
            reponse.setErrorCode(Constants.ERROR_CODE_0);
            reponse.setErrorDecription("Success");
            return reponse;
        } catch (Exception ex) {
            logger.error("checkIdNoCus: " + ex.getMessage());
            reponse.setErrorCode(Constants.ERROR_CODE_1);
            reponse.setErrorDecription("Error");
        } finally {
            commitTransactions(sessionPos);
            closeSessions(sessionPos);
        }
        return reponse;
    }

    //duyetdk
    public static double distanceBetween2Points(double la1, double lo1,
            double la2, double lo2) {
        double dLat = (la2 - la1) * (Math.PI / 180);
        double dLon = (lo2 - lo1) * (Math.PI / 180);
        double la1ToRad = la1 * (Math.PI / 180);
        double la2ToRad = la2 * (Math.PI / 180);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(la1ToRad)
                * Math.cos(la2ToRad) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = (Constants.EARTH_RADIUS) * c;
        return d;
    }

    public List<CcActionInformation> getCCActionDetail(HibernateHelper hibernateHelper, String account, String actionType) {
        Session sessionCC = null;
        Session sessionCmPos = null;
        Session payment = null;
        //WSRespone reponse = new WSRespone();
        try {
            sessionCC = hibernateHelper.getSession(Constants.SESSION_CC);
            sessionCmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            payment = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            //</editor-fold>
            if (Constants.CC_COMPLAIN_HISTORY.equals(actionType)) {
                return new CustomerBussiness().getComplainHistory(sessionCC, account);
            } else if (Constants.CC_BIILING_COLLECTION.equals(actionType)) {
                return new CustomerBussiness().getBillingCollectionHistory(payment, sessionCmPos, account);
            }
        } catch (Exception ex) {
            logger.error("checkIdNoCus: " + ex.getMessage());
        } finally {
            closeSessions(sessionCC, sessionCmPos, payment);
        }
        return null;
    }

    public List<String> getAccountOfCustomer(HibernateHelper hibernateHelper, Long customerId) {
        Session sessionCmPos = null;
        //WSRespone reponse = new WSRespone();
        try {
            sessionCmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            //</editor-fold>
            return new CustomerBussiness().getAccountOfCustomer(sessionCmPos, customerId);
        } catch (Exception ex) {
            logger.error("checkIdNoCus: " + ex.getMessage());
        } finally {
            closeSessions(sessionCmPos);
        }
        return null;
    }

    public List<ApParam> getActionDetailType(HibernateHelper hibernateHelper) {
        Session sessionCmPos = null;
        List<ApParam> list = new ArrayList<ApParam>();
        //WSRespone reponse = new WSRespone();
        try {
            sessionCmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            //</editor-fold>
            list = new ApParamSupplier().findByType(sessionCmPos, Constants.PARAM_TYPE_CC_ACTION_DETAIL, Constants.STATUS_USE);
            if (list != null && !list.isEmpty()) {
                for (ApParam ap : list) {
                    ap.setStatus(null);
                    ap.setParamCode(null);
                    ap.setParamId(null);
                    ap.setParamType(null);
                }
            }
        } catch (Exception ex) {
            logger.error("checkIdNoCus: " + ex.getMessage());
        } finally {
            closeSessions(sessionCmPos);
        }
        return list;
    }

    public ParamOut topUp(HibernateHelper hibernateHelper, DataTopUpPinCode dataDeObj) {
        Session sessionCmPos = null;
        Session sessionIM = null;
        Session sessionCmPre = null;
        Session seessionMerchant = null;
        ParamOut result = new ParamOut();
        result.setErrorCode(Constants.ERROR_CODE_1);
        boolean isErorr = false;
        try {
            logger.error("topup start: " + new Date());
            sessionCmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            sessionIM = hibernateHelper.getSession(Constants.SESSION_IM);
            sessionCmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            seessionMerchant = hibernateHelper.getSession(Constants.SESSION_MERCHANT);
            if (dataDeObj == null
                    || dataDeObj.getAmount() == null || dataDeObj.getAmount() < 0
                    || dataDeObj.getIsdn() == null || dataDeObj.getIsdn().isEmpty() || dataDeObj.getTime() == null
                    || dataDeObj.getRequestId() == null || dataDeObj.getRequestId().isEmpty()
                    || (dataDeObj.getAmount() > 50 && dataDeObj.getAmount() < 4000)) {
                result.setErrorDecription("Param input is not null or empty, amount must be > 0$ and <= 50$");
            } else {
                String isdn = dataDeObj.getIsdn();
                if (isdn.startsWith("0")) {
                    isdn = isdn.replaceFirst("0", "");
                }
                if (isdn.startsWith("855")) {
                    isdn = isdn.replaceFirst("855", "");
                }
                StringBuilder saleTransId = new StringBuilder();
                if (Math.floor(dataDeObj.getAmount()) != dataDeObj.getAmount()) {
                    result.setErrorDecription("Amount is not integer");
                } else {
                    int quantity = (int) (dataDeObj.getAmount() * 1);
                    String stockModelCode = Constants.STOCK_MODEL_CODE_TOPTUP;
                    if (dataDeObj.getAmount() % 1000 == 0) {
                        quantity = 1;
                        stockModelCode = Constants.STOCK_MODEL_CODE_TOPTUP_KHR + (dataDeObj.getAmount().intValue());
                    }
                    String message = checGoodInformation(sessionIM, sessionCmPos, sessionCmPre, seessionMerchant, dataDeObj.getMerchantAccount(), dataDeObj.getAmount(), quantity, stockModelCode, Constants.VTC_PAY_TOPUP, isdn, null, null, dataDeObj.getTime(), saleTransId, dataDeObj.getRequestId());

                    if (message == null) {
                        /*TopUp*/
                        String balance = String.valueOf(dataDeObj.getAmount().intValue());
                        if (dataDeObj.getAmount() % 1000 == 0) {
                            ApParamSupplier paramSup = new ApParamSupplier();
                            List<ApParam> param = paramSup.findByTypeMerchant(seessionMerchant, "TOP_UP_KHR", balance, null);
                            if (param == null || param.isEmpty()) {
                                result.setErrorCode(Constants.ERROR_CODE_1);
                                result.setErrorDecription("Can not find value for topup " + dataDeObj.getAmount().toString() + " KHR");
                                result.setSaleTransId(saleTransId.toString());
                                rollBackTransactions(sessionIM, sessionCmPos, sessionCmPre);
                                isErorr = true;
                                sessionIM.beginTransaction();
                                sessionCmPos.beginTransaction();
                                sessionCmPre.beginTransaction();
                                return result;
                            }
                            balance = param.get(0).getParamValue();
                        }
                        //balance = Float.parseFloat(balance) / 1000000.00f + "";
                        Exchange exchange = new Exchange();
                        StringBuilder requestStr = new StringBuilder();
                        StringBuilder reponseStr = new StringBuilder();
                        StringBuilder command = new StringBuilder();
                        String errorCode = payment(exchange, isdn, balance, requestStr, reponseStr, command);
                        if ("0".equals(errorCode)) {
                            result.setErrorCode(Constants.ERROR_CODE_0);
                            result.setErrorDecription("Topup success " + balance + "$ to " + isdn);
                            result.setSaleTransId(saleTransId.toString());
                            commitTransactions(sessionIM, sessionCmPos, sessionCmPre);
                            sessionIM.beginTransaction();
                            sessionCmPos.beginTransaction();
                            sessionCmPre.beginTransaction();
                        } else {
                            result.setErrorCode(Constants.ERROR_CODE_1);
                            result.setErrorDecription("Topup failed " + balance + "$ to " + isdn);
                            result.setSaleTransId(saleTransId.toString());
                            rollBackTransactions(sessionIM, sessionCmPos, sessionCmPre);
                            isErorr = true;
                            sessionIM.beginTransaction();
                            sessionCmPos.beginTransaction();
                            sessionCmPre.beginTransaction();
                        }
                        BaseSupplier supplier = new BaseSupplier();
                        ActionLogPr logPr = new ActionLogPr();
                        Long id = supplier.getSequence(sessionCmPos, "action_log_pr_seq");
                        logPr.setId(id);
                        logPr.setIsdn(isdn);
                        logPr.setCreateDate(new Date());
                        logPr.setRequest(requestStr.toString());
                        logPr.setResponse(reponseStr.toString());
                        logPr.setResponseCode(errorCode);
                        logPr.setShopCode(dataDeObj.getMerchantAccount());
                        logPr.setUserName("mBCCS");
                        sessionCmPos.save(logPr);
                        sessionCmPos.flush();
                        commitTransactions(sessionIM, sessionCmPos, sessionCmPre);
                        sessionIM.beginTransaction();
                        sessionCmPos.beginTransaction();
                        sessionCmPre.beginTransaction();
                    } else {
                        result.setErrorDecription(message);
                        isErorr = true;
                    }
                }
            }
        } catch (Exception ex) {
            result.setErrorCode("99");
            logger.error("topUp: " + ex.getMessage());
            result.setErrorDecription(ex.getMessage());
            isErorr = true;
        } finally {
            logger.error("topup end: " + new Date());
            if (isErorr) {
                rollBackTransactions(sessionIM, sessionCmPos, sessionCmPre, seessionMerchant);
            } else {
                commitTransactions(sessionIM, sessionCmPos, sessionCmPre, seessionMerchant);
            }
            closeSessions(sessionCmPos, sessionIM, sessionCmPre, seessionMerchant);
        }
        return result;
    }

    public ParamOut getPinCode(HibernateHelper hibernateHelper, DataTopUpPinCode dataDeObj) {
        Session sessionCmPos = null;
        Session sessionIM = null;
        Session sessionCmPre = null;
        Session seessionMerchant = null;
        ParamOut result = new ParamOut();
        result.setErrorCode(Constants.ERROR_CODE_1);
        boolean isErorr = false;
        try {
            logger.error("getPincode start: " + new Date());
            sessionCmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            sessionIM = hibernateHelper.getSession(Constants.SESSION_IM);
            sessionCmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            seessionMerchant = hibernateHelper.getSession(Constants.SESSION_MERCHANT);

            StringBuilder saleTransId = new StringBuilder();
            if (dataDeObj.getAmount() == null || dataDeObj.getAmount() < 0
                    || dataDeObj.getTime() == null || dataDeObj.getRequestId() == null || dataDeObj.getRequestId().isEmpty()) {
                result.setErrorDecription("Param input is not null or empty");
            } else {
                StringBuilder encrypt = new StringBuilder();
                StringBuilder signature = new StringBuilder();
                double t = dataDeObj.getAmount();
                int amoutemp = (int) t;
                String message = checGoodInformation(sessionIM, sessionCmPos, sessionCmPre, seessionMerchant, dataDeObj.getMerchantAccount(), dataDeObj.getAmount(), 1, String.valueOf(amoutemp), Constants.VTC_PAY_PINCODE, null, encrypt, signature, dataDeObj.getTime(), saleTransId, dataDeObj.getRequestId());
                if (message != null) {
                    result.setErrorDecription(message);
                    isErorr = true;
                } else {
                    result.setErrorCode(Constants.ERROR_CODE_0);
                    result.setEncrypt(encrypt.toString());
                    result.setSignature(signature.toString());
                }
                result.setSaleTransId(saleTransId.toString());
            }
        } catch (Exception ex) {
            isErorr = true;
            result.setErrorCode("99");
            result.setErrorDecription(ex.getMessage());
            logger.error("getPinCode: " + ex.getMessage());
        } finally {
            logger.error("getPincode end: " + new Date());
            if (isErorr) {
                rollBackTransactions(sessionIM, sessionCmPos, sessionCmPre, seessionMerchant);
            } else {
                commitTransactions(sessionIM, sessionCmPos, sessionCmPre, seessionMerchant);
            }
            closeSessions(sessionCmPos, sessionIM, sessionCmPre, seessionMerchant);
        }
        return result;
    }

    private String checGoodInformation(Session sessionIM, Session sessionCmPos, Session sessionCmPre, Session seessionMerchant, String merchantCode, Double amount, int quantity, String stockModelCode, String shopSaleCode, String isdn, StringBuilder encrypt, StringBuilder signature, String timeExecute, StringBuilder saleTransIdStr, String requestId) throws Exception {
        logger.error("checGoodInformation: " + requestId);
        if (amount <= 0) {
            return "amount must be bigger than 0.";
        }
        /*ActionLogDAO actionLogDAO = new ActionLogDAO();
         if (actionLogDAO.isRequestIdExists(seessionMerchant, requestId)) {
         return "reqeustId is used, please use another requestId";
         }*/
        IMDAO imDAO = new IMDAO();
        ApParamSupplier paramSup = new ApParamSupplier();
        List<Merchant> merchant = new MerchantDAO().getMerchantByAccount(seessionMerchant, merchantCode);
        if (merchant == null || merchant.isEmpty()) {
            return "Merchant code not exists";
        }
        /*Lay thong tin agent*/
        logger.error("checGoodInformation: " + requestId + " - " + "Lay thong tin agent");
        AccountAgent agentBuy = imDAO.getAccountAgentByIsdn(sessionIM, merchant.get(0).getIsdn());
        if (agentBuy == null) {
            return "Can not find agent with this shop code";
        } else {
            /*Kiem tra han muc*/
            logger.error("checGoodInformation: " + requestId + " - " + "Kiem tra han muc");
            /*if ((agentBuy.getLimitDebtPayment() == null ? 0d : agentBuy.getLimitDebtPayment()) - (agentBuy.getCurrentDebtPayment() == null ? 0d : agentBuy.getCurrentDebtPayment()) < amount) {
                return "Your amount remain " + (agentBuy.getLimitDebtPayment() - agentBuy.getCurrentDebtPayment()) + " , can not buy more";
            }*/
 /*Thong bao so tien cua doi tac sap het*/
            List<ApParam> apParam = paramSup.findByTypeMerchant(seessionMerchant, Constants.PARAM_TYPE_MIN_DEBT_PINCODE_TOPUP, null, null);
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            if (apParam != null && !apParam.isEmpty()) {
                try {
                    Date date = formatter.parse(apParam.get(0).getParamValue());
                    if (agentBuy.getLimitDebtPayment() - agentBuy.getCurrentDebtPayment() < Double.parseDouble(apParam.get(0).getParamName())
                            && new Date().getTime() - date.getTime() >= 60 * 60 * 1000) {
                        /*Thuc hien gui tin nhan thong bao cho nhan vien tai chinh*/
                        List<ApParam> apParamNumber = paramSup.findByTypeMerchant(seessionMerchant, Constants.PARAM_TYPE_WARNING_NUMBER, null, null);
                        List<String> list = new ArrayList<String>();
                        list.add(agentBuy.getOwnerCode());
                        list.add((agentBuy.getLimitDebtPayment() - agentBuy.getCurrentDebtPayment()) + "$");
                        for (ApParam param : apParamNumber) {
                            com.viettel.bccs.cm.bussiness.SendMessge.send(sessionCmPre, param.getParamValue(), list, new Date(), merchantCode, null, Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.WARNING_FINANCE);
                        }
                        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        apParam.get(0).setParamValue(dateFormat.format(new Date()));
                        paramSup.updateTimeSendMessageFinance(seessionMerchant, Constants.PARAM_TYPE_MIN_DEBT_PINCODE_TOPUP, dateFormat.format(new Date()));
                    }
                } catch (Exception ex) {
                }
            }
            try {
                Date date = formatter.parse(timeExecute);
                long range = Math.abs(new Date().getTime() - date.getTime());
                if (range >= 15 * 60 * 1000) {
                    return "Date time is expired";
                }
            } catch (Exception ex) {
                return "Date time is expired";
            }
            apParam = paramSup.findByTypeMerchant(seessionMerchant, Constants.PARAM_TYPE_STAFF_ONLINE, shopSaleCode, null);
            if (apParam == null || apParam.isEmpty()) {
                return "Can not find staff sale this product";
            }
            Staff staffVTC = new StaffDAO().findByCode(sessionCmPos, apParam.get(0).getParamValue());
            if (staffVTC == null) {
                return "Can not find " + shopSaleCode + "  to get stock model";
            } else {
                logger.error("checGoodInformation: " + requestId + " - " + "Lay thong tin mat hang");
                /*Lay thong tin mat hang*/
                apParam = paramSup.findByTypeMerchant(seessionMerchant, "STOCK_ONLINE", stockModelCode, null);
                if (apParam == null || apParam.isEmpty()) {
                    return "Can not find config stock model for " + stockModelCode;
                }
                stockModelCode = apParam.get(0).getParamValue();
                StockModel stockModel = imDAO.getStockModelIdByCode(sessionIM, stockModelCode, Constants.OWNER_TYPE_STAFF, staffVTC.getStaffId());
                if (stockModel == null) {
                    return "Can not find stock model";
                } else {
                    Price price = imDAO.getPriceOfStock(sessionIM, stockModel.getStockModelId(), staffVTC.getPricePolicy());
                    if (price == null) {
                        return "The price of Goods is incorrect";
                    } else {
                        /*check stock good*/
                        logger.error("checGoodInformation: " + requestId + " - " + "check stock good");
                        boolean checkStock = true;//imDAO.checkStockGood(sessionIM, stockModel.getStockModelId(), staffVTC.getStaffId(), Constants.OWNER_TYPE_STAFF, quantity);
                        if (!checkStock) {
                            return "Err! Quantity in stock is not enough ";
                        } else {
                            Shop shopAgent = new ShopDAO().findByCode(sessionCmPos, agentBuy.getOwnerCode());
                            /*Tinh tien chiet khau*/
                            logger.error("checGoodInformation: " + requestId + " - " + "Tinh tien chiet khau");
                            Discount discount = imDAO.getDiscount(sessionIM, amount, stockModel.getStockModelId(), shopAgent.getDiscountPolicy());

                            /*Max money per date*/
 /*apParam = paramSup.findByTypeMerchant(seessionMerchant, "MAX_MONEY_PER_DATE", merchantCode, null);
                            if (apParam != null && !apParam.isEmpty()) {
                                if (apParam.get(0).getParamValue() != null) {
                                    try {
                                        Double maxLimit = Double.parseDouble(apParam.get(0).getParamValue());
                                        Double temp = imDAO.getCurrentTopUpPinCodeInDate(sessionIM, shopAgent.getShopId(), null);
                                        if (maxLimit < temp + amount) {
                                            return "This Shop has TopUp or pincode with max money this date: " + maxLimit;
                                        }
                                    } catch (Exception ex) {
                                        int a = 1;
                                    }
                                }
                            }*/
 /*Max money isdn per date*/
 /*apParam = paramSup.findByTypeMerchant(seessionMerchant, "MAX_MONEY_ISDN_PER_DATE", merchantCode, null);
                            if (apParam != null && !apParam.isEmpty() && isdn != null) {
                                if (apParam.get(0).getParamValue() != null) {
                                    try {
                                        Double maxLimit = Double.parseDouble(apParam.get(0).getParamValue());
                                        Double temp = imDAO.getCurrentTopUpPinCodeInDate(sessionIM, null, isdn);
                                        if (maxLimit < temp + amount) {
                                            return "This Shop has TopUp or pincode with max money this date for this isdn " + isdn + " is " + maxLimit;
                                        }
                                    } catch (Exception ex) {
                                        int a = 1;
                                    }
                                }
                            }*/
                            Double discountAmount = 0d;
                            if (discount != null) {
                                discountAmount = discount.getDiscountRate() * amount / discount.getDiscountAmount();
                            }
                            amount = amount - discountAmount;
                            Double temp = 1.0 + (price.getVat() / 100.0);
                            Double notTax = amount / temp;
                            Double tax = amount - notTax;
                            BaseSupplier supplier = new BaseSupplier();

                            String serial = null;
                            StringBuilder id = new StringBuilder();
                            if (stockModelCode.contains("PINCODE")) {
                                serial = imDAO.getSerialStockCard(sessionIM, Constants.OWNER_TYPE_STAFF, staffVTC.getStaffId(), stockModel.getStockModelId());
                                if (serial == null) {
                                    return "Can not find serial for this " + stockModelCode + " in stockCard";
                                }
                                imDAO.getPinCode(sessionIM, serial, staffVTC.getStaffId(), Constants.OWNER_TYPE_STAFF, id, encrypt, signature);
                                if (id.toString().isEmpty()) {
                                    return "can not find pincode";
                                }

                                PinCode pinCode = new PinCode();
                                pinCode.setSerial(serial);
                                pinCode.setPincode(DecryptBusiness.decrypt(Constants.KEY_AES, signature.toString()));
                                String tempText = LogUtils.toJson(pinCode);

                                encrypt.delete(0, encrypt.length());
                                signature.delete(0, signature.length());
                                encrypt.append(CommonWebservice.Encrypt(tempText, merchant.get(0).getPublicKey()));
                                signature.append(CommonWebservice.Sign(tempText, Constants.METFONE_PRIVATE_KEY));
                            }
                            logger.error("checGoodInformation: " + requestId + " - " + "save stock_trans");
                            /*save stock_trans*/
                            StockTrans stockTrans = new StockTrans();
                            Long stockTransId = supplier.getSequence(sessionIM, "STOCK_TRANS_SEQ");
                            stockTrans.setStockTransId(stockTransId);
                            stockTrans.setFromOwnerType(Constants.OWNER_TYPE_STAFF);
                            stockTrans.setFromOwnerId(staffVTC.getStaffId());
                            stockTrans.setToOwnerType(Constants.OWNER_TYPE_SHOP);
                            stockTrans.setToOwnerId(shopAgent.getShopId());
                            stockTrans.setCreateDatetime(new Date());
                            stockTrans.setStockTransType(Constants.TRANS_EXPORT);
                            stockTrans.setStockTransStatus(Constants.TRANS_SALE_AGENT_VTC);
                            stockTrans.setNote("Sale transaction to agent");
                            stockTrans.setReasonId(4323l);
                            sessionIM.save(stockTrans);
                            sessionIM.flush();

                            logger.error("checGoodInformation: " + requestId + " - " + "Save sale_trans");
                            /*Save sale_trans*/
                            SaleTrans saleTrans = new SaleTrans();
                            Long saleTransId = supplier.getSequence(sessionIM, "SALE_TRANS_SEQ");
                            saleTransIdStr.append(saleTransId.toString());
                            saleTrans.setSaleTransId(saleTransId);
                            saleTrans.setSaleTransDate(new Date());
                            saleTrans.setSaleTransType(Constants.SALE_TRANS_TYPE_AGENT); //loai giao dich: ban cho CTV/ diem ban
                            saleTrans.setStatus(String.valueOf(Constants.SALE_PAY_NOT_BILL)); //da thanh toan nhung chua lap hoa don
                            saleTrans.setShopId(staffVTC.getShopId());/*shop_id cua nv ban hang*/
                            saleTrans.setStaffId(staffVTC.getStaffId());/*staff_id cua nv ban hang*/
                            saleTrans.setPayMethod("2");
                            /*1: cash, 2: account transfer, 3: emoney*/
                            saleTrans.setDiscount(discountAmount / 1.1); //tien chiet khau
                            saleTrans.setAmountTax(amount); //tong tien phai tra cua KH, = chua thue + thue - KM - C/Khau
                            saleTrans.setAmountNotTax(notTax); //tien chua thue
                            saleTrans.setVat(price.getVat()); //tien vat
                            saleTrans.setTax(tax);
                            saleTrans.setCustName(shopAgent.getShopCode() + " - " + shopAgent.getName()); //doi voi ban hang cho CTV, luu thong tin truong nay la ten CTV
                            saleTrans.setReceiverId(shopAgent.getShopId());
                            saleTrans.setTin(shopAgent.getTin());
                            saleTrans.setReceiverType(Constants.OWNER_TYPE_SHOP);
                            saleTrans.setAddress(saleTrans.getAddress()); //CM day sang
                            saleTrans.setReasonId(4323l);
                            saleTrans.setTelecomServiceId(18l);
                            saleTrans.setSaleTransCode(formatTransCode(saleTransId));
                            saleTrans.setCreateStaffId(staffVTC.getStaffId());/*staff_id cua nv ban hang*/
                            saleTrans.setStockTransId(stockTransId);
                            saleTrans.setIsdn(isdn);
                            saleTrans.setNote(staffVTC.getStaffCode());
                            saleTrans.setInTransId(requestId);
                            if (stockModel.getIsKHR() == null || 0 == stockModel.getIsKHR()) {
                                saleTrans.setCurrency("USD");
                            } else {
                                saleTrans.setCurrency("KHR");
                            }
                            sessionIM.save(saleTrans);
                            sessionIM.flush();

                            logger.error("checGoodInformation: " + requestId + " - " + "Save Sale_Trans_Detail");
                            /*Save Sale_Trans_Detail*/
                            SaleTransDetail saleTransDetail = new SaleTransDetail();
                            Long saleTransDetailId = supplier.getSequence(sessionIM, "SALE_TRANS_DETAIL_SEQ");
                            saleTransDetail.setSaleTransDetailId(saleTransDetailId);
                            saleTransDetail.setSaleTransId(saleTrans.getSaleTransId());
                            saleTransDetail.setSaleTransDate(saleTrans.getSaleTransDate());
                            saleTransDetail.setStockModelId(stockModel.getStockModelId());
                            saleTransDetail.setStateId(Constants.STATE_NEW);
                            saleTransDetail.setPriceId(price.getPriceId());
                            saleTransDetail.setQuantity(new Long(quantity));
                            saleTransDetail.setDiscountId(discount == null ? null : discount.getDiscountId());
                            saleTransDetail.setAmount(amount + discountAmount);
                            saleTransDetail.setStockModelCode(stockModel.getStockModelCode());
                            saleTransDetail.setStockModelName(stockModel.getName());
                            saleTransDetail.setPrice(price.getPrice());
                            saleTransDetail.setPriceVat(price.getVat());
                            saleTransDetail.setStockTypeId(stockModel.getStockTypeId());
                            saleTransDetail.setStockTypeName(imDAO.getStockTypeName(sessionIM, stockModel.getStockTypeId()));
                            saleTransDetail.setDiscountAmount(discountAmount / 1.1);
                            saleTransDetail.setVatAmount(tax);
                            if (stockModel.getIsKHR() == null || 0 == stockModel.getIsKHR()) {
                                saleTransDetail.setCurrencyCode("USD");
                            } else {
                                saleTransDetail.setCurrencyCode("KHR");
                            }
                            sessionIM.save(saleTransDetail);
                            sessionIM.flush();
                            int record = 0;

                            if (stockModelCode.contains("PINCODE")) {
                                SaleTransSerial saleSerial = new SaleTransSerial();
                                saleSerial.setFromSerial(serial);
                                saleSerial.setQuantity(1l);
                                saleSerial.setSaleTransDate(new Date());
                                saleSerial.setSaleTransDetailId(saleTransDetailId);
                                saleSerial.setSaleTransSerialId(supplier.getSequence(sessionIM, "sale_trans_serial_seq"));
                                saleSerial.setToSerial(serial);
                                saleSerial.setStockModelId(stockModel.getStockModelId());
                                sessionIM.save(saleSerial);
                                sessionIM.flush();

                                imDAO.updatePinCodeToUsed(sessionIM, id.toString(), saleTransId, merchantCode);
                                imDAO.updateStockCardToUsed(sessionIM, serial, shopAgent.getShopId(), Constants.OWNER_TYPE_SHOP);
                                Long vcRequestId = supplier.getSequence(sessionIM, "VC_REQ_ID_SEQ");
                                imDAO.saveVcRequest(sessionIM, serial, 4l/*REQUEST_TYPE_SALE_AGENTS*/, saleTransId, vcRequestId, staffVTC.getStaffId(), staffVTC.getShopId(), staffVTC.getStaffCode());

                                Exchange exchange = new Exchange();
                                String errorCode = activeCard(exchange, serial);
                            }
                            /*Nang han muc cua agent*/
                            logger.error("checGoodInformation: " + requestId + " - " + "Nang han muc cua agent");
                            /*record = imDAO.updateAccountAgent(sessionIM, amount, agentBuy.getOwnerCode(), isdn);
                            if (record == 0) {
                                return "update current debt for " + merchantCode + " failed";
                            }*/
 /*Tru han muc kho*/
                            logger.error("checGoodInformation: " + requestId + " - " + "Tru han muc kho");
                            /*record = imDAO.expStockTotal(sessionIM, Constants.OWNER_TYPE_STAFF, staffVTC.getStaffId(), 1l, stockModel.getStockModelId(), new Long(quantity));
                            if (record == 0) {
                                return "Not enough quantity for sale";
                            }*/
                        }
                    }
                }
            }
        }
        return null;
    }

    public String decryptData(Session cmPos, String data, String shopCode) {
        try {
            List<ApParam> apparam = new ApParamBussiness().findByTypeCode(cmPos, Constants.PARAM_TYPE_SHOP_KEY_AES, shopCode);
            if (apparam != null && !apparam.isEmpty()) {
                String key = DecryptBusiness.decrypt(Constants.KEY_AES, apparam.get(0).getParamValue());
                if (key != null) {
                    String dataDe = DecryptBusiness.decrypt(key, data);
                    if (dataDe != null) {
                        dataDe = DecryptBusiness.decrypt(Constants.KEY_AES, dataDe);
                        return dataDe;
                    }
                }
            }
        } catch (Exception ex) {
        }
        return null;
    }

    public String encryptData(Session cmPos, String data, String shopCode) {
        try {
            List<ApParam> apparam = new ApParamBussiness().findByTypeCode(cmPos, Constants.PARAM_TYPE_SHOP_KEY_AES, shopCode);
            if (apparam != null && !apparam.isEmpty()) {
                String key = DecryptBusiness.decrypt(Constants.KEY_AES, apparam.get(0).getParamValue());
                if (key != null) {
                    String dataDe = DecryptBusiness.encrypt(key, data);
                    return dataDe;
                }
            }
        } catch (Exception ex) {
        }
        return null;
    }

    public static String formatTransCode(Long transId) {
        return Constants.TRANS_CODE_PREFIX + String.format("%0" + Constants.TRANS_ID_LENGTH + "d", transId);
    }

    public String payment(Exchange exchange, String isdn, String balance, StringBuilder requestStr, StringBuilder reponseStr, StringBuilder command) {
        String responseCode = "0";
        ExchMsg request = new ExchMsg();
        ExchMsg response = null;
        if ("true".equalsIgnoreCase(ResourceBundleUtils.getResource("provisioning.allow.send.request", "provisioning"))) {
            try {
                HashMap<String, ExchMsg> lstResult;
                lstResult = exchange.payment(isdn, balance);
                request = lstResult.get("REQUEST");
                response = lstResult.get("RESPONSE");
                if (response != null) {
                    responseCode = (String) response.getError();
                }
            } catch (Exception ex) {
                logger.error("Error call provisioning");
                responseCode = "000";
            } finally {
                requestStr.append(request.toString());
                reponseStr.append(response == null ? "Response null" : response.toString());
                command.append(request.getCommand());
            }
        } else {
            requestStr.append("<ExchMsg Id='1' SystemTrace='2' ClientId='prm' Command='OCSHW_PAYMENT'>\n"
                    + "	<TransactionTime>Fri Oct 12 15:22:44 ICT 2018</TransactionTime>\n"
                    + "	<ISDN>978948480</ISDN>\n"
                    + "	<ClientTimeout>100000</ClientTimeout>\n"
                    + "	<BALANCE>0.1</BALANCE>\n"
                    + "</ExchMsg>");
            reponseStr.append("Khong goi pro");
        }
        return responseCode;
    }

    public String decryptPincode(String encryptedValue) throws Exception {

        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.DECRYPT_MODE, new SecretKeySpec(Constants.KEY_AES.getBytes(), "KEY_AES"));

        byte[] enctVal = new Base64().decode(encryptedValue.getBytes());
        System.out.println("enctVal length " + enctVal.length);

        byte[] decordedValue = c.doFinal(enctVal);

        return new String(decordedValue);
    }

    public ParamOut confirmPincode(HibernateHelper hibernateHelper, DataTopUpPinCode dataDeObj) {
        Session sessionIM = null;
        Session seessionMerchant = null;
        ParamOut result = new ParamOut();
        result.setErrorCode(Constants.ERROR_CODE_1);
        boolean isErorr = false;
        try {
            sessionIM = hibernateHelper.getSession(Constants.SESSION_IM);
            seessionMerchant = hibernateHelper.getSession(Constants.SESSION_MERCHANT);

            if (dataDeObj.getTime() == null || dataDeObj.getRequestId() == null || dataDeObj.getRequestId().isEmpty() || dataDeObj.getMerchantAccount() == null || dataDeObj.getMerchantAccount().isEmpty()) {
                result.setErrorDecription("Param input is not null or empty");
            } else {
                StringBuilder encrypt = new StringBuilder();
                StringBuilder signature = new StringBuilder();
                Long saleTransId = new TransactionLogDAO().getSaleTransIdFromRequestId(seessionMerchant, dataDeObj.getRequestId(), dataDeObj.getMerchantAccount());
                if (saleTransId == null) {
                    result.setErrorDecription("Can not find pincode for this request");
                } else {
                    new IMDAO().getPinCodeFromSaleTransId(sessionIM, encrypt/* serial*/, signature/*pinCode*/, saleTransId);
                    if (!signature.toString().isEmpty()) {
                        List<Merchant> merchant = new MerchantDAO().getMerchantByAccount(seessionMerchant, dataDeObj.getMerchantAccount());
                        PinCode pinCode = new PinCode();
                        pinCode.setSerial(encrypt.toString());
                        pinCode.setPincode(DecryptBusiness.decrypt(Constants.KEY_AES, signature.toString()));
                        String tempText = LogUtils.toJson(pinCode);
                        encrypt.delete(0, encrypt.length());
                        signature.delete(0, signature.length());
                        encrypt.append(CommonWebservice.Encrypt(tempText, merchant.get(0).getPublicKey()));
                        signature.append(CommonWebservice.Sign(tempText, Constants.METFONE_PRIVATE_KEY));
                        result.setEncrypt(encrypt.toString());
                        result.setSignature(signature.toString());
                        result.setErrorCode(Constants.ERROR_CODE_0);
                        result.setErrorDecription("Success");
                    } else {
                        result.setErrorDecription("Can not find pincode for this request");
                    }
                }
            }
        } catch (Exception ex) {
            result.setErrorCode("99");
            isErorr = true;
            result.setErrorDecription(ex.getMessage());
            logger.error("confirmPincode: " + ex.getMessage());
        } finally {
            if (isErorr) {
                rollBackTransactions(sessionIM, seessionMerchant);
            } else {
                commitTransactions(sessionIM, seessionMerchant);
            }
            closeSessions(sessionIM, seessionMerchant);
        }
        return result;
    }

    public String activeCard(Exchange exchange, String serial) {
        String responseCode = "0";
        ExchMsg request = new ExchMsg();
        ExchMsg response = null;
        if ("true".equalsIgnoreCase(ResourceBundleUtils.getResource("provisioning.allow.send.request", "provisioning"))) {
            try {
                HashMap<String, ExchMsg> lstResult;
                lstResult = exchange.activeCard(serial, serial, "0");
                request = lstResult.get("REQUEST");
                response = lstResult.get("RESPONSE");
                if (response != null) {
                    responseCode = (String) response.getError();
                }
            } catch (Exception ex) {
                logger.error("Error call provisioning");
                responseCode = "000";
            } finally {
                logger.error(request.toString());
                logger.error(response == null ? "Response null" : response.toString());
                //requestStr.append(request.toString());
                //reponseStr.append(response == null ? "Response null" : response.toString());
            }
        }
        return responseCode;
    }

    public ParamOut confirmTopup(HibernateHelper hibernateHelper, String requestId, String dateStr, String token) {
        Session sessionIM = null;
        Session seessionMerchant = null;
        Session seessionCM = null;
        ParamOut result = new ParamOut();
        result.setErrorCode(Constants.ERROR_CODE_1);
        try {
            sessionIM = hibernateHelper.getSession(Constants.SESSION_IM);
            seessionMerchant = hibernateHelper.getSession(Constants.SESSION_MERCHANT);
            seessionCM = hibernateHelper.getSession(Constants.SESSION_CM_POS);

            try {
                String[] arrToken = CommonWebservice.decryptData(seessionCM, token).split("/");
                String merchantAccount = arrToken[0];
                //result.setErrorCode(Constants.ERROR_CODE_0);
                String resultTopUp = new TransactionLogDAO().confimTopup(seessionMerchant, requestId, dateStr, merchantAccount);
                if (resultTopUp != null) {
                    result.setErrorCode(resultTopUp);
                }
            } catch (Exception ex) {
                result.setErrorDecription(ex.getMessage());
            }
        } catch (Exception ex) {
            result.setErrorCode("99");
            result.setErrorDecription(ex.getMessage());
            logger.error("confirmTopup: " + ex.getMessage());
        } finally {
            closeSessions(sessionIM, seessionMerchant, seessionCM);
        }
        return result;
    }

    public AccountInformation getAccountInformation(HibernateHelper hibernateHelper, String account) {
        Session sessionCmPos = null;
        AccountInformation infor = new AccountInformation();
        //WSRespone reponse = new WSRespone();
        try {
            sessionCmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            //</editor-fold>
            SubAdslLeaseline sub = new SubAdslLeaselineDAO().findByAccount(sessionCmPos, account);
            if (sub != null) {
                String custIdno = null;
                com.viettel.bccs.api.Task.BO.Contract contract = new ContractDAO().findById(sub.getContractId(), sessionCmPos);
                if (contract != null) {
                    Customer cust = new com.viettel.bccs.cm.dao.CustomerDAO().findById(sessionCmPos, contract.getCustId());
                    custIdno = cust.getIdNo();
                }
                infor.setCustName(sub.getUserUsing());
                infor.setDeployAddress(sub.getDeployAddress());
                infor.setProductCode(sub.getProductCode());
                infor.setPassword(sub.getPassword());
                infor.setUplinkAccount(null);
                infor.setActStatusText(sub.getActStatus());
                infor.setBusTypeObjValue(null);
                infor.setContractId(sub.getContractId().toString());
                infor.setCustIdNo(custIdno);
                infor.setServiceType(sub.getServiceType());
                infor.setTelecomServiceAlias(sub.getServiceType());
                infor.setSpeed(sub.getSpeed() == null ? null : sub.getSpeed().toString());
                infor.setStatus(sub.getStatus().toString());
                infor.setSubId(sub.getSubId().toString());
                infor.setContactMobile(sub.getTelMobile());
                infor.setContactTelFax(sub.getTelFax());
                infor.setIpStatic(sub.getIpStatic());
                infor.setIpBlock(null);
            }
        } catch (Exception ex) {
            logger.error("checkIdNoCus: " + ex.getMessage());
        } finally {
            closeSessions(sessionCmPos);
        }
        return infor;
    }

    public ParamOut makeSaleTrans(HibernateHelper hibernateHelper, DataTopUpPinCode dataDeObj) {
        Session sessionIM = null;
        Session seessionMerchant = null;
        Session seessionCm = null;
        Session seessionCmPre = null;
        ParamOut result = new ParamOut();
        result.setErrorCode(Constants.ERROR_CODE_1);
        boolean isErorr = false;
        HashMap<String, ExchMsg> lstResult = null;
        try {
            sessionIM = hibernateHelper.getSession(Constants.SESSION_IM);
            seessionMerchant = hibernateHelper.getSession(Constants.SESSION_MERCHANT);
            seessionCm = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            seessionCmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);

            if (dataDeObj == null
                    || dataDeObj.getQuantity() <= 0
                    || dataDeObj.getRequestId() == null || dataDeObj.getRequestId().isEmpty()
                    || dataDeObj.getStockModelCode() == null || dataDeObj.getStockModelCode().isEmpty()
                    || (dataDeObj.getServiceType() != null && !dataDeObj.getServiceType().isEmpty() && (dataDeObj.getIsdn() == null || dataDeObj.getIsdn().isEmpty()))
                    || (dataDeObj.getServiceType() != null && !dataDeObj.getServiceType().isEmpty() && (dataDeObj.getShopCode() == null || dataDeObj.getShopCode().isEmpty()))) {
                result.setErrorDecription("Param input is not null or empty");
            } else {
                ApParamSupplier paramSup = new ApParamSupplier();
                StringBuilder saleTransId = new StringBuilder();
                StringBuilder description = new StringBuilder();
                if (dataDeObj.getIsdn() != null && !dataDeObj.getIsdn().isEmpty()) {
                    List<com.viettel.bccs.cm.model.SubMb> subMb = new RequestChangeInformationSupplier().getSubMbFromISDN(seessionCmPre, dataDeObj.getIsdn());
                    if (subMb == null || subMb.isEmpty() || subMb.get(0).getStatus() != 2 || "03".equals(subMb.get(0).getActStatus())) {
                        result.setErrorDecription("isdn does not exist or is inactive");
                        return result;
                    }
                }
                String message = checGoodInformation(sessionIM, seessionMerchant, seessionCm, dataDeObj, saleTransId, description);
                if (message == null) {
                    String flag = ResourceBundleUtils.getResource("provisioning.allow.send.request", "provisioning");
                    if ("true".equalsIgnoreCase(flag)) {
                        if ("DATA".equals(dataDeObj.getServiceType())) {
                            String msisdn = dataDeObj.getIsdn();
                            msisdn = msisdn.startsWith("0") ? msisdn.substring(1) : msisdn;
                            msisdn = msisdn.startsWith("855") ? msisdn : "855" + msisdn;
                            Exchange exchange = new Exchange();
                            List<ApParam> apParam = paramSup.findByTypeMerchant(seessionMerchant, "DATA_PARTNER", dataDeObj.getMerchantAccount(), dataDeObj.getStockModelCode());
                            if (apParam == null || apParam.isEmpty()) {
                                isErorr = true;
                                result.setErrorCode(Constants.ERROR_CODE_017);
                                result.setErrorDecription(LabelUtil.getKey("ERROR_CODE_" + Constants.ERROR_CODE_017, "en_US"));
                                return result;
                            }
                            DataTopUpPinCode dataValue = new Gson().fromJson(apParam.get(0).getParamValue(), DataTopUpPinCode.class);
                            /*Lay ngay con su dung cua tai khoan, neu nho hon thi + bang ngay su dung data*/
                            lstResult = exchange.getInforSubsOcs(msisdn);
                            ExchMsg response = lstResult.get("RESPONSE");
                            String activeStop = (String) response.get("ACTIVE_STOP");
                            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                            Date nowDate = new Date();
                            String nowDateStr = format.format(nowDate);
                            Date expiredIsdn = format.parse(activeStop);
                            long date = 0;
                            if (expiredIsdn.before(format.parse(nowDateStr)) || expiredIsdn.equals(format.parse(nowDateStr))) {
                                expiredIsdn = format.parse(nowDateStr);
                                date = date - 1;
                            }
                            Calendar c = Calendar.getInstance();
                            c.setTime(new Date());
                            c.add(Calendar.DATE, dataValue.getDateExpired());
                            if (c.getTime().after(expiredIsdn)) {
                                date = date + (c.getTime().getTime() - expiredIsdn.getTime()) / (1000 * 60 * 60 * 24);
                                System.out.println("date: " + date);
                                if (date >= 0) {
                                    lstResult = exchange.addValidity(msisdn, String.valueOf(date));
                                }
                            }
                            String byteData = String.valueOf(dataValue.getAmount().longValue() * 1024l * 1024l);
                            //lstResult = exchange.addBalanceExp(msisdn, dataValue.getAccount(), byteData, dataValue.getDateExpired());
                            lstResult = exchange.addBalanceExpPartyCode(msisdn, dataValue.getAccount(), byteData, dataValue.getDateExpired(), dataDeObj.getStockModelCode());
                            if (lstResult != null) {
                                if (Integer.valueOf(response.getError()) == 0) {
                                    result.setErrorCode(Constants.ERROR_CODE_0);
                                    result.setErrorDecription("Success");
                                } else {
                                    result.setErrorCode(Constants.ERROR_CODE_1);
                                    result.setErrorDecription(response.getDescription());
                                }
                            } else {
                                result.setErrorCode(Constants.ERROR_CODE_2);
                                result.setErrorDecription("OCS failed");
                            }
                        }
                    }
                    result.setErrorCode(Constants.ERROR_CODE_0);
                    result.setErrorDecription("Success");
                    result.setAccount(description.toString());
                    result.setSaleTransId(saleTransId.toString());
                } else {
                    String errorMessage = LabelUtil.getKey("ERROR_CODE_" + message, "en_US");
                    if (errorMessage != null && !errorMessage.isEmpty()) {
                        result.setErrorCode(message);
                        result.setErrorDecription(errorMessage);
                    } else {
                        result.setErrorDecription(message);
                    }

                    isErorr = true;
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            result.setErrorCode("99");
            logger.error("makeSaleTrans: " + ex.getMessage());
            result.setErrorDecription(ex.getMessage());
            isErorr = true;
        } finally {
            logger.error("makeSaleTrans end: " + new Date());
            if (isErorr) {
                rollBackTransactions(sessionIM, seessionMerchant);
                try {
                    if ("DATA".equals(dataDeObj.getServiceType())) {
                        sessionIM.beginTransaction();
                        String sql = "select 1 from update_egifts_account_log where request_id = ?  and amount > 0";
                        Query query = sessionIM.createSQLQuery(sql);
                        query.setParameter(0, dataDeObj.getRequestId());
                        List list = query.list();
                        if (list != null && !list.isEmpty()) {
                            new IMDAO().updateAccountAgentEgifts(sessionIM, -dataDeObj.getAmount(), dataDeObj.getIsdn(), dataDeObj.getRequestId() + "_CR", dataDeObj.getShopCode());
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {
                commitTransactions(sessionIM, seessionMerchant);
            }
            /*try {
                if (lstResult != null && seessionCm != null && dataDeObj != null) {
                    seessionCm.beginTransaction();
                    new ActionLogPrDAO().insert(seessionCm, new Date(), dataDeObj.getMerchantAccount(), "mBCCS", dataDeObj.getIsdn(), lstResult.get("REQUEST").toString(), lstResult.get("RESPONSE").toString(), "0", null);
                     commitTransactions(seessionCm);
                }
            } catch (HibernateException ex) {
                logger.error("checGoodInformation: " + ex);
            }*/
            closeSessions(sessionIM, seessionMerchant, seessionCm, seessionCmPre);
        }
        return result;
    }

    private String checGoodInformation(Session sessionIM, Session seessionMerchant, Session sessionCmPos, DataTopUpPinCode data, StringBuilder saleTransIdStr, StringBuilder description) throws Exception {
        String requestId = data.getRequestId();
        String merchantCode = data.getMerchantAccount();
        String staffCode = data.getStaffCode();
        String stockModelCode = data.getStockModelCode();
        String custName = data.getCustName();
        int quantity = data.getQuantity();
        Double currentAmount = null;
        logger.error("checGoodInformation: " + requestId);
        Shop shopAgent = null;
        IMDAO imDAO = new IMDAO();
        ApParamSupplier paramSup = new ApParamSupplier();
        List<Merchant> merchant = new MerchantDAO().getMerchantByAccount(seessionMerchant, merchantCode);
        AccountAgent agentBuy = null;
        if (merchant == null || merchant.isEmpty()) {
            /*Merchant code not exists*/
            return Constants.ERROR_CODE_008;
        }
        if (staffCode == null) {
            List<ApParam> apParam = paramSup.findByTypeMerchant(seessionMerchant, Constants.PARAM_TYPE_STAFF_ONLINE, null, data.getMerchantAccount());
            if (apParam == null || apParam.isEmpty()) {
                /*return "Can not find staff sale this product";*/
                return Constants.ERROR_CODE_009;
            }
            if (data.getShopCode() == null || data.getShopCode().isEmpty()) {
                agentBuy = imDAO.getAccountAgentByIsdn(sessionIM, merchant.get(0).getIsdn());
                if (agentBuy == null) {
                    //return "Can not find agent with this shop code";
                    return Constants.ERROR_CODE_010;
                }
            } else {
                agentBuy = imDAO.getAccountAgentByIsdnforLock(sessionIM, null, data.getShopCode());
            }
            if (agentBuy == null) {
                return Constants.ERROR_CODE_020;
            }
            staffCode = apParam.get(0).getParamValue();
            shopAgent = new ShopDAO().findByCode(sessionCmPos, agentBuy.getOwnerCode());
            if (shopAgent == null) {
                return Constants.ERROR_CODE_020;
            }
        }
        Staff staffVTC = new StaffDAO().findByCode(sessionCmPos, staffCode);

        if (staffVTC == null) {
            //return "Can not find " + staffCode + "  to get stock model";
            return Constants.ERROR_CODE_011;
        } else {
            logger.error("checGoodInformation: " + requestId + " - " + "Lay thong tin mat hang");
            /*Lay thong tin mat hang*/
            List<ApParam> apParam = paramSup.findByTypeMerchant(seessionMerchant, "STOCK_ONLINE", stockModelCode, null);
            if (apParam == null || apParam.isEmpty()) {
                //return "Can not find config stock model for " + stockModelCode;
                return Constants.ERROR_CODE_012;
            }
            stockModelCode = apParam.get(0).getParamValue();
            String typeService = ResourceUtils.getResource("SALE_SERVICE_FOR_MODEL");
            BaseSupplier supplier = new BaseSupplier();
            if (typeService != null && typeService.contains(stockModelCode)) {
                String saleServiceCode = ResourceUtils.getResource("SALE_SERVICE_" + stockModelCode);
                if (saleServiceCode == null || saleServiceCode.isEmpty()) {
                    //return "can not find sale service code for " + stockModelCode;
                    return Constants.ERROR_CODE_013;
                }
                SaleTransNotInvoice saleTransNotInvoice = new SaleTransNotInvoice();
                SaleServices saleService = com.viettel.im.database.DAO.SaleServicesDAO.findByCode(sessionIM, saleServiceCode);
                if (saleService == null) {
                    //return "Sale service is not found";
                    return Constants.ERROR_CODE_014;
                }
                com.viettel.im.database.BO.SaleServicesPrice saleServicesPrice = saleTransNotInvoice.findSaleServicesPrice(saleService.getSaleServicesId(), "1", sessionIM);
                if (saleServicesPrice == null) {
                    //return "Sale service price is not found";
                    return Constants.ERROR_CODE_015;
                }
                Double amount = saleServicesPrice.getPrice() * data.getQuantity();
                boolean check = new DebitBusiness().checkLockTransaction(sessionIM, staffVTC.getStaffId(), amount, "USD");
                if (!check) {
                    return "Debit is over limit or access deadline, please send money to Metfone";
                }
                if (data.getShopCode() != null && !data.getShopCode().isEmpty() && data.getServiceType() != null) {
                    data.setAmount(amount);
                    currentAmount = imDAO.updateAccountAgentEgifts(sessionIM, data.getAmount(), data.getIsdn(), data.getRequestId(), data.getShopCode());
                    sessionIM.beginTransaction();
                    if (currentAmount == null) {
                        return Constants.ERROR_CODE_016;
                    }
                    description.append("{\"currentAmount\": " + currentAmount + ", \"transactionAmount\": " + amount + "}");
                }

                Double temp = 1.0 + (saleServicesPrice.getVat() / 100.0);
                Double notTax = amount / temp;
                Double tax = amount - notTax;

                /*Save sale_trans*/
                SaleTrans saleTrans = new SaleTrans();
                Long saleTransId = supplier.getSequence(sessionIM, "SALE_TRANS_SEQ");
                saleTransIdStr.append(saleTransId.toString());
                saleTrans.setSaleTransId(saleTransId);
                saleTrans.setSaleTransDate(new Date());
                saleTrans.setSaleTransType(Constants.SALE_TRANS_TYPE_SERVICE); //loai giao dich: lam dich vu
                saleTrans.setStatus(String.valueOf(Constants.SALE_PAY_NOT_BILL)); //da thanh toan nhung chua lap hoa don
                saleTrans.setShopId(staffVTC.getShopId());/*shop_id cua nv ban hang*/
                saleTrans.setStaffId(staffVTC.getStaffId());/*staff_id cua nv ban hang*/
                saleTrans.setPayMethod("3");
                /*1: cash, 2: account transfer, 3: emoney*/
                saleTrans.setSaleServiceId(saleService.getSaleServicesId());
                saleTrans.setSaleServicePriceId(saleServicesPrice.getSaleServicesPriceId());
                saleTrans.setAmountTax(amount); //tong tien phai tra cua KH, = chua thue + thue - KM - C/Khau
                saleTrans.setAmountNotTax(notTax); //tien chua thue
                saleTrans.setVat(saleServicesPrice.getVat()); //tien vat
                saleTrans.setTax(tax);
                saleTrans.setCustName(custName);
                saleTrans.setAddress(saleTrans.getAddress()); //CM day sang
                saleTrans.setTelecomServiceId(18l);
                saleTrans.setSaleTransCode(formatTransCode(saleTransId));
                saleTrans.setCreateStaffId(staffVTC.getStaffId());/*staff_id cua nv ban hang*/
                saleTrans.setIsdn(data.getIsdn());

                if (shopAgent != null) {
                    saleTrans.setReceiverId(shopAgent.getShopId());
                    saleTrans.setTin(shopAgent.getTin());
                    saleTrans.setReceiverType(Constants.OWNER_TYPE_SHOP);
                    saleTrans.setCustName(shopAgent.getName());
                }
                saleTrans.setNote(staffVTC.getStaffCode());
                saleTrans.setInTransId(requestId);
                sessionIM.save(saleTrans);
                sessionIM.flush();

                /*Save Sale_Trans_Detail*/
                SaleTransDetail saleTransDetail = new SaleTransDetail();
                Long saleTransDetailId = supplier.getSequence(sessionIM, "SALE_TRANS_DETAIL_SEQ");
                saleTransDetail.setSaleTransDetailId(saleTransDetailId);
                saleTransDetail.setSaleTransId(saleTrans.getSaleTransId());
                saleTransDetail.setSaleTransDate(saleTrans.getSaleTransDate());
                saleTransDetail.setStateId(Constants.STATE_NEW);
                saleTransDetail.setAmount(amount);
                saleTransDetail.setPrice(saleServicesPrice.getPrice());
                saleTransDetail.setPriceVat(saleServicesPrice.getVat());
                saleTransDetail.setSaleServicesCode(saleServiceCode);
                saleTransDetail.setSaleServicesName(saleService.getName());
                saleTransDetail.setSaleServicesPrice(saleServicesPrice.getPrice());
                saleTransDetail.setSaleServicesPriceVat(saleServicesPrice.getVat());
                saleTransDetail.setVatAmount(tax);
                saleTransDetail.setSaleServicesId(saleService.getSaleServicesId());
                saleTransDetail.setSaleServicesPriceId(saleServicesPrice.getSaleServicesPriceId());
                saleTransDetail.setQuantity(Integer.valueOf(data.getQuantity()).longValue());
                sessionIM.save(saleTransDetail);
                sessionIM.flush();
            } else {
                StockModel stockModel = imDAO.getStockModelIdByCode(sessionIM, stockModelCode, Constants.OWNER_TYPE_STAFF, staffVTC.getStaffId());
                if (stockModel == null) {
                    return "Can not find stock model";
                }
                Price price = imDAO.getPriceOfStock(sessionIM, stockModel.getStockModelId(), staffVTC.getPricePolicy());
                if (price == null) {
                    return "The price of Goods is incorrect";
                }

                Double amount = price.getPrice() * quantity;
                /*check stock good*/
                logger.error("checGoodInformation: " + requestId + " - " + "check stock good");
                boolean checkStock = imDAO.checkStockGood(sessionIM, stockModel.getStockModelId(), staffVTC.getStaffId(), Constants.OWNER_TYPE_STAFF, quantity);
                if (!checkStock) {
                    return "Err! Quantity in stock is not enough ";
                }

                /*Tinh tien chiet khau*/
                logger.error("checGoodInformation: " + requestId + " - " + "Tinh tien chiet khau");
                Discount discount = shopAgent != null ? imDAO.getDiscount(sessionIM, amount, stockModel.getStockModelId(), shopAgent.getDiscountPolicy()) : null;

                Double discountAmount = 0d;
                if (discount != null) {
                    discountAmount = discount.getDiscountRate() * amount / discount.getDiscountAmount();
                }
                amount = amount - discountAmount;
                Double temp = 1.0 + (price.getVat() / 100.0);
                Double notTax = amount / temp;
                Double tax = amount - notTax;

                boolean check = new DebitBusiness().checkLockTransaction(sessionIM, staffVTC.getStaffId(), amount, "USD");
                if (!check) {
                    return "Debit is over limit or access deadline, please send money to Metfone";
                }

                logger.error("checGoodInformation: " + requestId + " - " + "save stock_trans");

                logger.error("checGoodInformation: " + requestId + " - " + "Save sale_trans");
                /*Save sale_trans*/
                SaleTrans saleTrans = new SaleTrans();
                Long saleTransId = supplier.getSequence(sessionIM, "SALE_TRANS_SEQ");
                saleTransIdStr.append(saleTransId.toString());
                saleTrans.setSaleTransId(saleTransId);
                saleTrans.setSaleTransDate(new Date());
                if (agentBuy != null) {
                    saleTrans.setSaleTransType(Constants.SALE_TRANS_TYPE_AGENT); //loai giao dich: ban cho CTV/ diem ban
                } else {
                    saleTrans.setSaleTransType(Constants.SALE_TRANS_RETAIL); //loai giao dich: ban cho CTV/ diem ban
                }
                saleTrans.setStatus(String.valueOf(Constants.SALE_PAY_NOT_BILL)); //da thanh toan nhung chua lap hoa don
                saleTrans.setShopId(staffVTC.getShopId());/*shop_id cua nv ban hang*/
                saleTrans.setStaffId(staffVTC.getStaffId());/*staff_id cua nv ban hang*/
                saleTrans.setPayMethod("2");
                /*1: cash, 2: account transfer, 3: emoney*/
                saleTrans.setDiscount(discountAmount / 1.1); //tien chiet khau
                saleTrans.setAmountTax(amount); //tong tien phai tra cua KH, = chua thue + thue - KM - C/Khau
                saleTrans.setAmountNotTax(notTax); //tien chua thue
                saleTrans.setVat(price.getVat()); //tien vat
                saleTrans.setTax(tax);
                if (shopAgent != null) {
                    saleTrans.setCustName(shopAgent.getShopCode() + " - " + shopAgent.getName()); //doi voi ban hang cho CTV, luu thong tin truong nay la ten CTV
                    saleTrans.setReceiverId(shopAgent.getShopId());
                    saleTrans.setReceiverType(Constants.OWNER_TYPE_SHOP);
                    saleTrans.setTin(shopAgent.getTin());
                    saleTrans.setReasonId(4323l);
                } else {
                    saleTrans.setCustName(custName); //doi voi ban hang cho CTV, luu thong tin truong nay la ten CTV
                    saleTrans.setReasonId(4202l);
                }
                saleTrans.setTelecomServiceId(18l);
                saleTrans.setAddress(saleTrans.getAddress()); //CM day sang           
                saleTrans.setSaleTransCode(formatTransCode(saleTransId));
                saleTrans.setCreateStaffId(staffVTC.getStaffId());/*staff_id cua nv ban hang*/
                saleTrans.setNote(staffVTC.getStaffCode());
                saleTrans.setInTransId(requestId);
                sessionIM.save(saleTrans);
                sessionIM.flush();

                logger.error("checGoodInformation: " + requestId + " - " + "Save Sale_Trans_Detail");
                /*Save Sale_Trans_Detail*/
                SaleTransDetail saleTransDetail = new SaleTransDetail();
                Long saleTransDetailId = supplier.getSequence(sessionIM, "SALE_TRANS_DETAIL_SEQ");
                saleTransDetail.setSaleTransDetailId(saleTransDetailId);
                saleTransDetail.setSaleTransId(saleTrans.getSaleTransId());
                saleTransDetail.setSaleTransDate(saleTrans.getSaleTransDate());
                saleTransDetail.setStockModelId(stockModel.getStockModelId());
                saleTransDetail.setStateId(Constants.STATE_NEW);
                saleTransDetail.setPriceId(price.getPriceId());
                saleTransDetail.setQuantity(new Long(quantity));
                saleTransDetail.setDiscountId(discount == null ? null : discount.getDiscountId());
                saleTransDetail.setAmount(amount + discountAmount);
                saleTransDetail.setStockModelCode(stockModel.getStockModelCode());
                saleTransDetail.setStockModelName(stockModel.getName());
                saleTransDetail.setPrice(price.getPrice());
                saleTransDetail.setPriceVat(price.getVat());
                saleTransDetail.setStockTypeId(stockModel.getStockTypeId());
                saleTransDetail.setStockTypeName(imDAO.getStockTypeName(sessionIM, stockModel.getStockTypeId()));
                saleTransDetail.setDiscountAmount(discountAmount / 1.1);
                saleTransDetail.setVatAmount(tax);
                sessionIM.save(saleTransDetail);
                sessionIM.flush();
                /*Tru han muc kho*/
                logger.error("checGoodInformation: " + requestId + " - " + "Tru han muc kho");
                int record = imDAO.expStockTotal(sessionIM, Constants.OWNER_TYPE_STAFF, staffVTC.getStaffId(), 1l, stockModel.getStockModelId(), new Long(quantity));
                if (record == 0) {
                    return "Not enough quantity for sale";
                }
            }
        }
        return null;
    }

    public ParamOut getSubInfoSms(HibernateHelper hibernateHelper, DataTopUpPinCode dataSub) {
        Session sessionRa = null;
        ParamOut result = new ParamOut();
        result.setErrorCode(Constants.ERROR_CODE_1);
        try {
            sessionRa = hibernateHelper.getSession(Constants.SESSION_RA);
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT   sub_id subId,                                               ");
            sql.append("         isdn,                                                       ");
            sql.append("         sub_type subType,                                           ");
            sql.append("         status,                                                     ");
            sql.append("         gender,                                                     ");
            sql.append("         age,                                                        ");
            sql.append("         province,                                                   ");
            sql.append("         district,                                                   ");
            sql.append("         commune,                                                    ");
            sql.append("         latitude,                                                   ");
            sql.append("         longtitude,                                                 ");
            sql.append("         TO_CHAR (created_date, 'dd/mm/yyyy hh24:mi:ss') createdate, ");
            sql.append("         device,                                                     ");
            sql.append("         money_usage_usd arpu,                                       ");
            sql.append("         voice_domestic_minutes voice,                               ");
            sql.append("         sms_out_qty sms,                                            ");
            sql.append("         data_gb_usage data,                                         ");
            sql.append("         roaming,                                                    ");
            sql.append("         voice_inter_minutes interCal                                ");
            sql.append("  FROM   rms.rms_sms_brand_name_db SAMPLE(20)                        ");
            sql.append(" WHERE       1 = 1                                                   ");

            if (dataSub.getAge() != null && !dataSub.getAge().isEmpty()) {
                sql.append(" ").append(ResourceUtils.getResource("CONDITION_AGE_" + dataSub.getAge())).append(" ");
            }
            if (dataSub.getGender() != null && !dataSub.getGender().isEmpty()) {
                sql.append(" ").append(ResourceUtils.getResource("CONDITION_GENDER_" + dataSub.getGender())).append(" ");
            }
            if (dataSub.getInterCal() != null && !dataSub.getInterCal().isEmpty()) {
                sql.append(" ").append(ResourceUtils.getResource("CONDITION_INTERCAL_" + dataSub.getInterCal())).append(" ");
            }
            if (dataSub.getRoaming() != null && !dataSub.getRoaming().isEmpty()) {
                sql.append(" ").append(ResourceUtils.getResource("CONDITION_ROAMING_" + dataSub.getRoaming())).append(" ");
            }
            if (dataSub.getArpu() != null && !dataSub.getArpu().isEmpty()) {
                sql.append(" ").append(ResourceUtils.getResource("CONDITION_ARPU_" + dataSub.getArpu())).append(" ");
            }
            if (dataSub.getSms() != null && !dataSub.getSms().isEmpty()) {
                sql.append(" ").append(ResourceUtils.getResource("CONDITION_SMS_" + dataSub.getSms())).append(" ");
            }
            if (dataSub.getVoice() != null && !dataSub.getVoice().isEmpty()) {
                sql.append(" ").append(ResourceUtils.getResource("CONDITION_VOICE_" + dataSub.getVoice())).append(" ");
            }
            if (dataSub.getData() != null && !dataSub.getData().isEmpty()) {
                sql.append(" ").append(ResourceUtils.getResource("CONDITION_DATA_" + dataSub.getData())).append(" ");
            }
            if (dataSub.getSubType() != null && !dataSub.getSubType().isEmpty()) {
                sql.append(" ").append(ResourceUtils.getResource("CONDITION_SUB_TYPE_" + dataSub.getSubType())).append(" ");
            }
            if (dataSub.getDevice() != null && !dataSub.getDevice().isEmpty()) {
                sql.append(" ").append(ResourceUtils.getResource("CONDITION_DEVICE_" + dataSub.getDevice())).append(" ");
            }
            List param = new ArrayList();
            if (dataSub.getLocation() != null && !dataSub.getLocation().isEmpty()) {
                if (dataSub.getLocation().length() == 3) {
                    sql.append(" AND PROVINCE = ? ");
                    param.add(dataSub.getLocation());
                } else if (dataSub.getLocation().length() == 6) {
                    sql.append(" AND PROVINCE = ? ");
                    param.add(dataSub.getLocation().substring(0, 3));
                    sql.append(" AND DISTRICT = ? ");
                    param.add(dataSub.getLocation().substring(3, 6));
                } else if (dataSub.getLocation().length() == 9) {
                    sql.append(" AND PROVINCE = ? ");
                    param.add(dataSub.getLocation().substring(0, 3));
                    sql.append(" AND DISTRICT = ? ");
                    param.add(dataSub.getLocation().substring(3, 6));
                    sql.append(" AND commune = ? ");
                    param.add(dataSub.getLocation().substring(6, 9));
                }
            }
            if (dataSub.getNumRecord() != null && !dataSub.getNumRecord().isEmpty()) {
                sql.append(" and rownum <= ? ");
                Long num = Long.parseLong(dataSub.getNumRecord());
                if (num.compareTo(100000l) > 0) {
                    param.add(100000l);
                } else {
                    param.add(dataSub.getNumRecord());
                }

            } else {
                sql.append(" and rownum <= 100000  ");
            }
            Query query = sessionRa.createSQLQuery(sql.toString())
                    .addScalar("subId", Hibernate.STRING)
                    .addScalar("isdn", Hibernate.STRING)
                    .addScalar("subType", Hibernate.STRING)
                    .addScalar("status", Hibernate.STRING)
                    .addScalar("gender", Hibernate.STRING)
                    .addScalar("age", Hibernate.STRING)
                    .addScalar("province", Hibernate.STRING)
                    .addScalar("district", Hibernate.STRING)
                    .addScalar("commune", Hibernate.STRING)
                    .addScalar("latitude", Hibernate.STRING)
                    .addScalar("longtitude", Hibernate.STRING)
                    .addScalar("createDate", Hibernate.STRING)
                    .addScalar("device", Hibernate.STRING)
                    .addScalar("arpu", Hibernate.STRING)
                    .addScalar("voice", Hibernate.STRING)
                    .addScalar("sms", Hibernate.STRING)
                    .addScalar("data", Hibernate.STRING)
                    .addScalar("roaming", Hibernate.STRING)
                    .addScalar("interCal", Hibernate.STRING)
                    .setResultTransformer(Transformers.aliasToBean(SubInfoSms.class));
            for (int i = 0; i < param.size(); i++) {
                query.setParameter(i, param.get(i));
            }
            List<SubInfoSms> data = query.list();
            while (data.size() < Long.parseLong(dataSub.getNumRecord())) {
                List<SubInfoSms> dataTemp = query.list();
                for (int i = 0; i < dataTemp.size(); i++) {
                    boolean flag = false;
                    for (int j = 0; j < data.size(); j++) {
                        if (dataTemp.get(i).getSubId().equals(data.get(j).getSubId())) {
                            flag = true;
                            break;
                        }
                    }
                    if (data.size() < Long.parseLong(dataSub.getNumRecord())) {
                        if (!flag) {
                            data.add(dataTemp.get(i));
                        }
                    } else {
                        break;
                    }
                }
            }
            result.setListSub(data);
            result.setErrorCode(Constants.ERROR_CODE_0);
            result.setErrorDecription("Success");
        } catch (Exception ex) {
            ex.printStackTrace();
            result.setErrorCode("99");
            logger.error("getSubInfoSms: " + ex.getMessage());
            result.setErrorDecription(ex.getMessage());
        } finally {
            logger.error("getSubInfoSms end: " + new Date());
            closeSessions(sessionRa);
        }
        return result;
    }

    public ParamOut checkServiceOnline(HibernateHelper hibernateHelper, String merchantAccount) {
        Session seessionMerchant = null;
        ParamOut result = new ParamOut();
        result.setErrorCode(Constants.ERROR_CODE_1);
        try {
            logger.error("checkServiceOnline start: " + new Date());
            seessionMerchant = hibernateHelper.getSession(Constants.SESSION_MERCHANT);
            List<Merchant> merchant = new MerchantDAO().getMerchantByAccount(seessionMerchant, merchantAccount);
            if (merchant != null && !merchant.isEmpty()) {
                Exchange exchange = new Exchange();
                HashMap<String, ExchMsg> lstResult;
                lstResult = exchange.getInforSubsOcs(merchant.get(0).getIsdn());
                ExchMsg request = lstResult.get("REQUEST");
                ExchMsg response = lstResult.get("RESPONSE");
                if ("0".equals((String) response.getError()) || "000".equals((String) response.getError())) {
                    result.setErrorCode(Constants.ERROR_CODE_0);
                    result.setErrorDecription("Success");
                } else {
                    result.setErrorCode(Constants.ERROR_CODE_1);
                    result.setErrorDecription("Failed");
                }
            } else {
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription("Failed");
            }
        } catch (Exception ex) {
            result.setErrorCode("99");
            logger.error("checkServiceOnline: " + ex.getMessage());
            result.setErrorDecription(ex.getMessage());
        } finally {
            logger.error("checkServiceOnline end: " + new Date());
            closeSessions(seessionMerchant);
        }
        return result;
    }
}
