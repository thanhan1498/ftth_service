/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.api.debit.BO.DebitStaff;
import com.viettel.bccs.api.debit.BO.DebitTrans;
import com.viettel.bccs.api.debit.DAO.DebitBusiness;
import com.viettel.bccs.cm.bussiness.im.DebitIMBusiness;
import com.viettel.bccs.cm.util.AppCryptUtils;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.EmoneyPayment;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.common.EmoneyPaymentClient;
import com.viettel.brcd.ws.model.output.ClearDebitTransOut;
import com.viettel.brcd.ws.model.output.DebitStaffDetail;
import com.viettel.brcd.ws.model.output.DebitStaffOut;
import com.viettel.brcd.ws.model.output.DebitTransHistoryDetail;
import com.viettel.brcd.ws.model.output.DebitTransHistoryOut;
import com.viettel.brcd.ws.model.output.DebitTransOut;
import com.viettel.brcd.ws.model.output.EmoneyResponse;
import com.viettel.brcd.ws.model.output.InvoiceOuput;
import com.viettel.brcd.ws.model.output.LockDebitTransOut;
import com.viettel.brcd.ws.model.output.TxDetail;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.im.database.BO.BankReceipt;
import com.viettel.im.database.DAO.ShopDAO;
import com.viettel.im.database.DAO.StaffDAO;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author duyetdk
 */
public class DebitController extends BaseController {

    public DebitController() {
        logger = Logger.getLogger(DebitController.class);
    }

    //<editor-fold defaultstate="collapsed" desc="getListDebitTransaction">
    /**
     * @param currency
     * @author: duyetdk
     * @since 8/3/2019
     * @des: lay danh sach giao dich chua nop tien theo ma nhan vien
     * @param hibernateHelper
     * @param fromDate
     * @param toDate
     * @param staffId
     * @param locale
     * @param token
     * @return
     */
    public DebitTransOut getListDebitTransaction(HibernateHelper hibernateHelper, String fromDate, String toDate, Long staffId,
            String locale, String token, String currency) {
        DebitTransOut result = null;
        Session imPosSession = null;
        try {
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            if (staffId == null) {
                result = new DebitTransOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            /*get staff info*/
            List<com.viettel.im.database.BO.Staff> staff = new StaffDAO(imPosSession).findByProperty("staffId", staffId);
            if (staff == null) {
                result = new DebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("staff.does.not.exist", locale));
                return result;
            }
            DebitBusiness debitBusiness = new DebitBusiness();
            //lay tat ca giao dich 
            List<DebitTrans> lst = debitBusiness.findTransaction(imPosSession, staffId, staff.get(0).getShopId(), fromDate, toDate, Constants.NOT_OK, null, null, currency);
            if (lst == null || lst.isEmpty()) {
                result = new DebitTransOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            //lay giao dich dang cho phe duyet
            DebitIMBusiness debitIMBusiness = new DebitIMBusiness();
            List<BankReceipt> lstBankReceipt = debitIMBusiness.getListBankReceipt(imPosSession, staffId, "1");
            String strContent = "";
            List<Long> lstDebitTransIdEx = new ArrayList<Long>();
            List<DebitTrans> lstDebitTrans = new ArrayList<DebitTrans>();
            lstDebitTrans.addAll(lst);

            if (lstBankReceipt != null && !lstBankReceipt.isEmpty()) {
                for (BankReceipt br : lstBankReceipt) {
                    strContent += br.getContent();
                }
                String strDebitTransIdEx = strContent.substring(1);
                for (String w : strDebitTransIdEx.split(",", 0)) {
                    lstDebitTransIdEx.add(Long.valueOf(w));
                }
                for (Long debitTransIdEx : lstDebitTransIdEx) {
                    for (DebitTrans debitTrans : lst) {
                        if (debitTransIdEx.equals(debitTrans.getId())) {
                            lstDebitTrans.remove(debitTrans);
                        }
                    }
                }
            }

            if (lstDebitTrans == null || lstDebitTrans.isEmpty()) {
                result = new DebitTransOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }

            result = new DebitTransOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lstDebitTrans);
            return result;
        } catch (Throwable ex) {
            ex.printStackTrace();
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new DebitTransOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(imPosSession);
            LogUtils.info(logger, "DebitController.getListDebitTransaction:result=" + LogUtils.toJson(result));
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getListDebitStaff">
    /**
     * @author: duyetdk
     * @since 9/3/2019
     * @des: lay danh sach cong no cua nhan vien
     * @param hibernateHelper
     * @param staffCode
     * @param shopCode
     * @param shopId
     * @param status
     * @param locale
     * @param token
     * @return
     */
    public DebitStaffOut getListDebitStaff(HibernateHelper hibernateHelper, String staffCode, String shopCode, Long shopId,
            Long status, String locale, String token) {
        DebitStaffOut result = null;
        Session imPosSession = null;
        LogUtils.info(logger, "Request: " + staffCode + "\nResponse: " + shopCode + "\nTransId: " + shopId);
        try {
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            if (shopId == null) {
                result = new DebitStaffOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            /*get shop info*/
            com.viettel.im.database.BO.Shop shop;
            shop = new ShopDAO(imPosSession).findById(shopId);
            if (shop == null || shop.getShopPath() == null || shop.getShopPath().isEmpty()) {
                result = new DebitStaffOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            if (shopCode != null && !shopCode.trim().isEmpty()) {
                shop = (com.viettel.im.database.BO.Shop) new ShopDAO(imPosSession).findByShopCode(shopCode).get(0);
            }
            DebitIMBusiness debitIMBusiness = new DebitIMBusiness();
            List<DebitStaffDetail> lst = debitIMBusiness.getListDebitStaff(imPosSession, staffCode, shop.getShopPath(), status, null, null);
            if (lst == null || lst.isEmpty()) {
                result = new DebitStaffOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            result = new DebitStaffOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lst);
            return result;
        } catch (Throwable ex) {
            ex.printStackTrace();
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new DebitStaffOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(imPosSession);
            LogUtils.info(logger, "DebitController.getListDebitStaff:result=" + LogUtils.toJson(result));
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="transHistory">
    /**
     * @author: duyetdk
     * @since 11/3/2019
     * @des: lay lich su giao dich tung nhan vien
     * @param hibernateHelper
     * @param staffId
     * @param status
     * @param payMethod
     * @param payCode
     * @param pageNo
     * @param locale
     * @param token
     * @return
     */
    public DebitTransHistoryOut transHistory(HibernateHelper hibernateHelper, Long staffId, Long status,
            String payMethod, String payCode, Long pageNo, String locale, String token) {
        DebitTransHistoryOut result = null;
        Session imPosSession = null;
        try {
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            if (staffId == null) {
                result = new DebitTransHistoryOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }

            DebitIMBusiness debitIMBusiness = new DebitIMBusiness();
            DebitBusiness debitBusiness = new DebitBusiness();
            List<DebitStaff> debitStaff = debitBusiness.getListDebitStaff(imPosSession, staffId, null, null, null, null);
            if (debitStaff == null || debitStaff.isEmpty()) {
                result = new DebitTransHistoryOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            Long count = 0l;
            for (int i = 0; i < debitStaff.size(); i++) {
                count += debitIMBusiness.countTransHistory(imPosSession, debitStaff.get(i).getId(), payMethod, payCode, status);
            }

            if (count == 0L) {
                result = new DebitTransHistoryOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            Integer start = null;
            Integer max = null;
            if (pageNo != null) {
                start = getStart(Constants.PAGE_SIZE, pageNo);
                max = getMax(Constants.PAGE_SIZE, pageNo, count);
            }
            Long totalPage = 0L;
            if (count % Constants.PAGE_SIZE == 0) {
                totalPage = count / Constants.PAGE_SIZE;
            } else if (count % Constants.PAGE_SIZE > 0) {
                totalPage = count / Constants.PAGE_SIZE + 1;
            }
            List<DebitTransHistoryDetail> lst = new ArrayList<DebitTransHistoryDetail>();
            for (int i = 0; i < debitStaff.size(); i++) {
                List<DebitTransHistoryDetail> lstTemp = debitIMBusiness.transHistory(imPosSession, debitStaff.get(i).getId(), payMethod, payCode, status, start, max);
                if (lstTemp != null && !lstTemp.isEmpty()) {
                    lst.addAll(lstTemp);
                }
            }

            if (lst.isEmpty()) {
                result = new DebitTransHistoryOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            result = new DebitTransHistoryOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), totalPage, count, lst);
            return result;
        } catch (Throwable ex) {
            ex.printStackTrace();
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new DebitTransHistoryOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(imPosSession);
            LogUtils.info(logger, "DebitController.transHistory:result=" + LogUtils.toJson(result));
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="clearDebitTrans">
    /**
     * @author: duyetdk
     * @since 12/3/2019
     * @des: tao yeu cau thanh toan
     * @param amount
     * @param staffId
     * @param locale
     * @param token
     * @param hibernateHelper
     * @param strDebitTransId
     * @return
     */
    public ClearDebitTransOut clearDebitTrans(HibernateHelper hibernateHelper, Long staffId, String strDebitTransId, Double amount, String locale, String token) {
        ClearDebitTransOut result = null;
        List<Long> lstDebitTransId = new ArrayList<Long>();
        Session imPosSession = null;
        DebitBusiness debitBusiness = new DebitBusiness();
        List<com.viettel.bccs.api.Task.BO.Staff> staff = null;
        String descriptionFail = null;
        boolean hasErr = false;
        String currenctyTemp = null;
        Long debitLogId = null;
        try {
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            /*get staff info*/
            DecimalFormat df = new DecimalFormat("#.##");
            df.setRoundingMode(RoundingMode.CEILING);
            staff = new com.viettel.bccs.api.Task.DAO.StaffDAO().findByProperty("staff_Id", staffId, imPosSession);
            if (staff == null) {
                result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("staff.does.not.exist", locale));
                return result;
            }
            String phoneNumber = staff.get(0).getAccountEmoney();
            if (strDebitTransId != null && !strDebitTransId.trim().isEmpty()) {
                Double total = 0D;
                for (String w : strDebitTransId.split(",", 0)) {
                    lstDebitTransId.add(Long.valueOf(w));
                }
                for (Long debitTransId : lstDebitTransId) {
                    com.viettel.bccs.api.debit.BO.DebitTrans temp = debitBusiness.findTransaction(imPosSession, null, null, null, null, null, null, debitTransId, null).get(0);
                    if (currenctyTemp == null) {
                        currenctyTemp = temp.getCurrency();
                    } else {
                        if (!currenctyTemp.equals(temp.getCurrency())) {
                            result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("debit.currency.invalid", locale));
                            return result;
                        }
                    }
                    total += temp.getAmount();
                }
                if (amount == null || !df.format(total).equals(df.format(amount))) {
                    result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("amount.debit.not.ok", locale));
                    return result;
                }
            } else {
                result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("required.debit.transId", locale));
                return result;
            }

            //create invoice emoney - call API emoney
            /*Map<String, String> headers = new HashMap<String, String>();
            headers.put("e-language", "en");
            headers.put("Authorization", String.format("epa %s", "922831c6be57bef2e9b97b0b97d276ac240f5c972e8039cd9c564e53d643bb31"));
            Long debitLogId = Common.getSequence("DEBit_log_seq", imPosSession);
            
            String jsonBodyParam = "{\"paymentType\":" + Constants.TYPE_USSD
                    + ",\"content\":" + "\"" + Constants.CLEAR_DEBIT_DES + "\""
                    + ",\"transAmount\":" + amount
                    + ",\"refId\":" + "\"" + debitLogId.toString() + "\""
                    + ",\"currency\":" + "\"" + Constants.CURRENCY_CODE + "\""
                    + ",\"customerPhoneNumber\":" + "\"" + phoneNumber + "\""
                    + ",\"ccyAcceptPayment\":" + "\"" + Constants.CURRENCY_CODE + "\"}";

            EmoneyResponse response = EmoneyPaymentClient.shared().postRequest("http://10.79.94.207:8080/MF_DEBIT/payment/init", headers, jsonBodyParam);
             */
            debitLogId = Common.getSequence("DEBit_log_seq", imPosSession);
            String url = ResourceUtils.getResource("create_invoice_emoney");
            String authorization = ResourceUtils.getResource("Authorization");
            amount = Double.valueOf(df.format(amount));
            EmoneyResponse response = new EmoneyPayment().createInvoiceEmoney(debitLogId, url, authorization, amount, phoneNumber, Constants.CLEAR_DEBIT_DES, currenctyTemp);
            InvoiceOuput io = null;
            if (response != null) {
                io = response.getResponseObject(InvoiceOuput.class);
            }
            if (io != null) {
                if (Constants.SUCCESS_CODE.equals(io.getStatus()) && io.getTxDetail() != null) {
                    result = new ClearDebitTransOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale),
                            io.getTxDetail().getTransDetailId(), io.getTxDetail().getTxPaymentTokenId());
                } else {
                    descriptionFail = io.getMessage() + " (STATUS: " + io.getStatus() + " - " + io.getCode() + ")";
                    hasErr = true;
                    result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred",
                            locale, io.getMessage() + " (STATUS: " + io.getStatus() + " - " + io.getCode() + ")"));
                    return result;
                }
            } else {
                result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("a.error.has.occurred", locale, "Emoney fail"));
                return result;
            }

            return result;
        } catch (Throwable ex) {
            ex.printStackTrace();
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            Session imLogSession = null;
            if (hasErr) {
                try {
                    if (staff != null) {
                        imLogSession = hibernateHelper.getSession(Constants.SESSION_IM);
                        String param = " \n input:token=" + gson.toJson(token) + ";amount=" + gson.toJson(amount)
                                + ";staffId=" + gson.toJson(staffId) + ";debitTransId=" + gson.toJson(strDebitTransId);
                        debitBusiness.clearTransactionFailed(imLogSession, lstDebitTransId.get(0),
                                staff.get(0).getStaffCode(), descriptionFail, Constants.METHOD_EMONEY,
                                null, param, gson.toJson(result), staff.get(0).getAccountEmoney(), (result == null ? "1" : result.getErrorCode()), debitLogId, currenctyTemp);
                        commitTransactions(imLogSession);
                    }

                } catch (Exception ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(imPosSession);
            closeSessions(imLogSession);
            LogUtils.info(logger, "DebitController.clearDebitTrans:result=" + LogUtils.toJson(result));
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="confirmDebitTrans">
    /**
     * @author: duyetdk
     * @since 13/3/2019
     * @des: xac nhan giao dich sau khi Clear Debit
     * @param currencyCode
     * @param paymentContent
     * @param strDebitTransId
     * @param txPaymentTokenId
     * @param locale
     * @param staffId
     * @param amount
     * @param hibernateHelper
     * @param paymentCode
     * @param token
     * @return
     */
    public ClearDebitTransOut confirmDebitTrans(HibernateHelper hibernateHelper, String strDebitTransId, Double amount, String paymentCode, String txPaymentTokenId,
            String currencyCode, String paymentContent, Long staffId, String locale, String token) {
        ClearDebitTransOut result = null;
        List<Long> lstDebitTransId = new ArrayList<Long>();
        Session imPosSession = null;
        DebitBusiness debitBusiness = new DebitBusiness();
        List<com.viettel.bccs.api.Task.BO.Staff> staff = null;
        Long debitTransIdFail = null;
        String descriptionFail = null;
        boolean hasErr = true;
        Long debitLogId = null;
        String currency = null;
        try {
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            String mess = "";
            /*get staff info*/
            staff = new com.viettel.bccs.api.Task.DAO.StaffDAO().findByProperty("staff_Id", staffId, imPosSession);
            if (staff == null) {
                result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("staff.does.not.exist", locale));
                return result;
            }
            DecimalFormat df = new DecimalFormat("#.##");
            df.setRoundingMode(RoundingMode.CEILING);
            if (strDebitTransId != null && !strDebitTransId.trim().isEmpty()) {
                Double total = 0D;
                for (String w : strDebitTransId.split(",", 0)) {
                    lstDebitTransId.add(Long.valueOf(w));
                }
                String sql = "select id from debit_trans where trans_id = ? for update nowait ";
                Query query = imPosSession.createSQLQuery(sql);
                for (Long debitTransId : lstDebitTransId) {
                    query.setParameter(0, debitTransId);
                    List list = query.list();
                    DebitTrans trans = debitBusiness.findTransaction(imPosSession, null, null, null, null, null, null, debitTransId, null).get(0);
                    if (currency == null) {
                        currency = trans.getCurrency();
                    } else if (!currency.equals(trans.getCurrency())) {
                        return new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("debit.currency.invalid", locale));
                    }
                    Double temp = trans.getAmount();
                    total += temp;
                }
                if (amount == null || !df.format(total).equals(df.format(amount))) {
                    result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("amount.debit.not.ok", locale));
                    return result;
                }
            } else {
                result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("required.debit.transId", locale));
                return result;
            }
            amount = Double.valueOf(df.format(amount));
            InvoiceOuput io = null;
            EmoneyResponse response = null;
            //String jsonBodyParam = "";
            StringBuilder requestEmoney = new StringBuilder();
            StringBuilder responseEmoney = new StringBuilder();
            if (amount != 0) {
                //payment ussd emoney - call API emoney
                /*Map<String, String> headers = new HashMap<String, String>();
                headers.put("e-language", "en");
                headers.put("Authorization", String.format("epa %s", "922831c6be57bef2e9b97b0b97d276ac240f5c972e8039cd9c564e53d643bb31"));

                //Get file from resources folder
                ClassLoader classLoader = getClass().getClassLoader();

                String decrypt = AppCryptUtils.decryptRSA(txPaymentTokenId, classLoader.getResource(Constants.EMONEY_PRIVATE_KEY).getFile());

                String encrypt = AppCryptUtils.encryptRSA(decrypt, classLoader.getResource(Constants.EMONEY_PUBLIC_KEY).getFile());

                jsonBodyParam = "{\"txPaymentTokenId\":" + "\"" + encrypt + "\""
                        + ",\"paymentContent\":" + "\"" + paymentContent + "\"}";

                response = EmoneyPaymentClient.shared().postRequest("http://10.79.94.207:8080/MF_DEBIT/payment/ussd-pay", headers, jsonBodyParam);
                LogUtils.info(logger, "--------------------------------------" + "\n- Request: " + jsonBodyParam + "\n- Response: " + LogUtils.toJson(response) + "\n- TransId: " + strDebitTransId + "\n--------------------------------------");
                 */
                debitLogId = Common.getSequence("DEBit_log_seq", imPosSession);
                String url = ResourceUtils.getResource("confirm_pay_emoney");
                String authorization = ResourceUtils.getResource("Authorization");
                amount = Double.valueOf(df.format(amount));
                String privateKey = ResourceUtils.getResource("emoney.private.key");
                String publicKey = ResourceUtils.getResource("emoney.public.key");
                response = new EmoneyPayment().confirmPayEmoney(url, txPaymentTokenId, authorization, privateKey, publicKey, requestEmoney, responseEmoney, paymentContent);

                if (response != null) {
                    io = response.getResponseObject(InvoiceOuput.class);
                }
            } else if (amount == 0d) {
                io = new InvoiceOuput();
                io.setStatus(Constants.SUCCESS_CODE);
                TxDetail temp = new TxDetail();
                temp.setPaidTid("0");
                io.setTxDetail(temp);
                response = new EmoneyResponse();
                response.setJsonResponse("");
            }
            if (io != null) {
                if (Constants.SUCCESS_CODE.equals(io.getStatus()) && io.getTxDetail() != null) {
                    for (Long debitTransId : lstDebitTransId) {
                        mess = debitBusiness.clearDebitTransaction(imPosSession, staffId, debitTransId,
                                staff.get(0).getStaffCode(), Constants.METHOD_EMONEY, io.getTxDetail().getPaidTid(), null, null, null, currency);
                        if (mess != null) {
                            if (!mess.trim().isEmpty()) {
                                descriptionFail = mess;
                                debitTransIdFail = debitTransId;
                                hasErr = true;
                                result = new ClearDebitTransOut(Constants.ERROR_CODE_1, mess, debitTransId);
                                return result;
                            }
                        }
                    }

                    /*giam no cho nhan vien sau khi thanh toan*/
                    mess = debitBusiness.decreaseDebitStaff(imPosSession, staffId, staff.get(0).getStaffCode(), amount,
                            staff.get(0).getAccountEmoney(), requestEmoney.toString(), response.getJsonResponse(), "Clear debit for " + staff.get(0).getStaffCode(),
                            Constants.METHOD_EMONEY, Constants.ERROR_CODE_0, currency);
                    if (mess != null) {
                        if (!mess.trim().isEmpty()) {
                            descriptionFail = mess;
                            hasErr = true;
                            result = new ClearDebitTransOut(Constants.ERROR_CODE_1, mess);
                            return result;
                        }
                    }
                    hasErr = false;
                    commitTransactions(imPosSession);
                    result = new ClearDebitTransOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
                } else {
                    descriptionFail = io.getMessage() + " (STATUS: " + io.getStatus() + " - " + io.getCode() + ")";
                    hasErr = true;
                    result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred",
                            locale, io.getMessage() + " (STATUS: " + io.getStatus() + " - " + io.getCode() + ")"));
                }
            } else {
                result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("a.error.has.occurred", locale, "Emoney fail"));
                return result;
            }

            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            Session imLogSession = null;
            if (hasErr) {
                try {
                    rollBackTransactions(imPosSession);
                    if (staff != null) {
                        imLogSession = hibernateHelper.getSession(Constants.SESSION_IM);
                        String param = " \n input:token=" + gson.toJson(token) + ";debitTransId=" + gson.toJson(strDebitTransId) + ";txPaymentTokenId=" + gson.toJson(txPaymentTokenId)
                                + ";staffId=" + gson.toJson(staffId) + ";amount=" + gson.toJson(amount) + ";paymentContent=" + gson.toJson(paymentContent)
                                + ";paymentCode=" + gson.toJson(paymentCode) + ";currencyCode=" + gson.toJson(currencyCode);
                        if (debitTransIdFail != null) {
                            debitBusiness.clearTransactionFailed(imLogSession, debitTransIdFail,
                                    staff.get(0).getStaffCode(), descriptionFail, Constants.METHOD_EMONEY,
                                    paymentCode, param, gson.toJson(result), staff.get(0).getAccountEmoney(), (result == null ? "1" : result.getErrorCode()), debitLogId, currency);
                        } else {
                            debitBusiness.clearTransactionFailed(imLogSession, lstDebitTransId.get(0),
                                    staff.get(0).getStaffCode(), descriptionFail, Constants.METHOD_EMONEY,
                                    paymentCode, param, gson.toJson(result), staff.get(0).getAccountEmoney(), (result == null ? "1" : result.getErrorCode()), debitLogId, currency);
                        }
                        commitTransactions(imLogSession);
                    }
                } catch (Exception ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(imPosSession);
            closeSessions(imLogSession);
            LogUtils.info(logger, "DebitController.confirmDebitTrans:result=" + LogUtils.toJson(result));
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="transferDebit">
    /**
     *
     * @param hibernateHelper
     * @param token
     * @param strDebitTransId
     * @param amount
     * @param paymentCode
     * @param receiveDebtStaffId
     * @param staffId
     * @param locale
     * @return
     */
    public ClearDebitTransOut transferDebit(HibernateHelper hibernateHelper, String token, String strDebitTransId,
            Double amount, String paymentCode, Long receiveDebtStaffId, Long staffId, String locale) {
        ClearDebitTransOut result = null;
        List<Long> lstDebitTransId = new ArrayList<Long>();
        Session imPosSession = null;
        DebitBusiness debitBusiness = new DebitBusiness();
        List<com.viettel.im.database.BO.Staff> staff = null;
        Long debitTransIdFail = null;
        String descriptionFail = null;
        boolean hasErr = false;
        String currency = null;
        try {
            DecimalFormat df = new DecimalFormat("#.##");
            df.setRoundingMode(RoundingMode.CEILING);
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            String mess = "";

            if (strDebitTransId != null && !strDebitTransId.trim().isEmpty()) {
                Double total = 0D;
                for (String w : strDebitTransId.split(",", 0)) {
                    lstDebitTransId.add(Long.valueOf(w));
                }
                for (Long debitTransId : lstDebitTransId) {
                    DebitTrans trans = debitBusiness.findTransaction(imPosSession, null, null, null, null, null, null, debitTransId, null).get(0);
                    if (currency == null) {
                        currency = trans.getCurrency();
                    } else if (!currency.equals(trans.getCurrency())) {
                        return new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("debit.currency.invalid", locale));
                    }
                    total += trans.getAmount();
                }
                System.out.println("transferDebit : Amount = " + amount + ", Total = " + total + " result equal: " + amount.equals(total));

                if (amount == null || !df.format(total).equals(df.format(amount))) {
                    return result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("amount.debit.not.ok", locale));
                }

            } else {
                return result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("required.debit.transId", locale));
            }

            /*get staff info*/
            staff = new StaffDAO(imPosSession).findByProperty("staffId", staffId);
            List<com.viettel.im.database.BO.Staff> receiveDebtStaff = new StaffDAO(imPosSession).findByProperty("staffId", receiveDebtStaffId);

            if (staff == null || receiveDebtStaff == null) {
                result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("staff.does.not.exist", locale));
                return result;
            }

            DebitIMBusiness debitIMBusiness = new DebitIMBusiness();
            List<DebitStaffDetail> lstDebitStaff = debitIMBusiness.getListDebitStaff(imPosSession, null, null, null, receiveDebtStaffId, currency);
            if (lstDebitStaff == null || lstDebitStaff.isEmpty()) {
                debitBusiness.createDefaultDebitStaff(imPosSession, receiveDebtStaffId, receiveDebtStaff.get(0).getShopId(), currency);
            }
            List<DebitStaff> debitStaff = debitBusiness.getListDebitStaff(imPosSession, receiveDebtStaffId, null, null, null, currency);

            if (amount > ((debitStaff.get(0).getDebitAmountLimit() != null ? debitStaff.get(0).getDebitAmountLimit() : 0D)
                    - (debitStaff.get(0).getDebitCurrentAmount() != null ? debitStaff.get(0).getDebitCurrentAmount() : 0D))) {
                return result = new ClearDebitTransOut(Constants.ERROR_CODE_1, "You are over limit, pleasr clear transaction");
            }
            paymentCode = "RE_" + receiveDebtStaffId + "_" + new SimpleDateFormat("ddMMyyyyHHmmssSS").format(new Date());
            for (Long debitTransId : lstDebitTransId) {
                mess = debitBusiness.clearDebitTransaction(imPosSession, staffId, debitTransId,
                        staff.get(0).getStaffCode(), Constants.METHOD_RECEIVING_DEBT, paymentCode,
                        receiveDebtStaffId, receiveDebtStaff.get(0).getShopId(), receiveDebtStaff.get(0).getStaffCode(),currency);
                if (mess != null) {
                    if (!mess.trim().isEmpty()) {
                        descriptionFail = mess;
                        debitTransIdFail = debitTransId;
                        hasErr = true;
                        result = new ClearDebitTransOut(Constants.ERROR_CODE_1, mess, debitTransId);
                        return result;
                    }
                }
            }

            /*giam no cho nhan vien duoc nhan no*/
            mess = debitBusiness.decreaseDebitStaff(imPosSession, staffId, staff.get(0).getStaffCode(), amount,
                    null, null, null, "Move debit to " + receiveDebtStaff.get(0).getStaffCode(), Constants.METHOD_RECEIVING_DEBT, Constants.ERROR_CODE_0,currency);
            if (mess != null) {
                if (!mess.trim().isEmpty()) {
                    descriptionFail = mess;
                    hasErr = true;
                    result = new ClearDebitTransOut(Constants.ERROR_CODE_1, mess);
                    return result;
                }
            }

            /*tang no cho nhan vien nhan no*/
            mess = debitBusiness.increaseDebitStaff(imPosSession, receiveDebtStaffId, receiveDebtStaff.get(0).getStaffCode(),
                    amount, "Receive debit from " + staff.get(0).getStaffCode(), Constants.METHOD_RECEIVING_DEBT, currency);
            if (mess != null) {
                if (!mess.trim().isEmpty()) {
                    descriptionFail = mess;
                    hasErr = true;
                    result = new ClearDebitTransOut(Constants.ERROR_CODE_1, mess);
                    return result;
                }
            }

            commitTransactions(imPosSession);
            result = new ClearDebitTransOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ClearDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            Session imLogSession = null;
            if (hasErr) {
                try {
                    rollBackTransactions(imPosSession);
                    if (staff != null) {
                        imLogSession = hibernateHelper.getSession(Constants.SESSION_IM);
                        String param = " \n input:token=" + gson.toJson(token) + ";debitTransId=" + gson.toJson(strDebitTransId)
                                + ";receiveDebtStaffId=" + gson.toJson(receiveDebtStaffId)
                                + ";staffId=" + gson.toJson(staffId) + ";amount=" + gson.toJson(amount) + ";paymentCode=" + gson.toJson(paymentCode);
                        if (debitTransIdFail != null) {
                            debitBusiness.clearTransactionFailed(imLogSession, debitTransIdFail,
                                    staff.get(0).getStaffCode(), descriptionFail, Constants.METHOD_EMONEY,
                                    paymentCode, param, gson.toJson(result), null, (result == null ? "1" : result.getErrorCode()), null, currency);
                        } else {
                            debitBusiness.clearTransactionFailed(imLogSession, lstDebitTransId.get(0),
                                    staff.get(0).getStaffCode(), descriptionFail, Constants.METHOD_EMONEY,
                                    paymentCode, param, gson.toJson(result), null, (result == null ? "1" : result.getErrorCode()), null, currency);
                        }

                        commitTransactions(imLogSession);
                    }
                } catch (Exception ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(imLogSession);
            closeSessions(imPosSession);
            LogUtils.info(logger, "DebitController.transferDebit:result=" + LogUtils.toJson(result));
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="checkLockTransaction">
    /**
     * @author: duyetdk
     * @since 1/4/2019
     * @des: check giao dich phat sinh
     * @param hibernateHelper
     * @param token
     * @param amount
     * @param staffId
     * @param locale
     * @return
     */
    public LockDebitTransOut checkLockTransaction(HibernateHelper hibernateHelper, String token, Double amount, Long staffId, String locale, String currency) {
        LockDebitTransOut result = null;
        Session imPosSession = null;
        try {
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            if (staffId == null) {
                result = new LockDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.getKey("required.staffId", locale));
                return result;
            }
            DebitBusiness debitBusiness = new DebitBusiness();
            boolean check = debitBusiness.checkLockTransaction(imPosSession, staffId, amount, currency);
            boolean isLock = !check;
            result = new LockDebitTransOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), isLock);
            return result;
        } catch (Throwable ex) {
            ex.printStackTrace();
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new LockDebitTransOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(imPosSession);
            LogUtils.info(logger, "DebitController.checkLockTransaction:result=" + LogUtils.toJson(result));
        }
    }
    //</editor-fold>
}
