package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.DomainBussiness;
import com.viettel.bccs.cm.model.Domain;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.dao.im.IMDAO;
import com.viettel.brcd.ws.model.output.DomainOut;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class DomainController extends BaseController {

    public DomainController() {
        logger = Logger.getLogger(DomainController.class);
    }

    public DomainOut getNickDomains(HibernateHelper hibernateHelper, String locale) {
        DomainOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Domain> domains = new DomainBussiness().findActives(cmPosSession);
            result = new DomainOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), domains);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new DomainOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "DomainController.getNickDomains:result=" + LogUtils.toJson(result));
        }
    }

    public ParamOut getBasPool(HibernateHelper hibernateHelper, String locale, String account) {
        ParamOut result = new ParamOut();
        Session imSession = null;
        try {
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            result.setErrorCode(Constants.ERROR_CODE_0);
            result.setErrorDecription(LabelUtil.getKey("common.success", locale));
            result.setBrasIpPool(new IMDAO().getBrasPool(imSession, account));
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result.setErrorCode(Constants.ERROR_CODE_1);
            result.setErrorDecription(ex.getMessage());
            return result;
        } finally {
            closeSessions(imSession);
            LogUtils.info(logger, "DomainController.getNickDomains:result=" + LogUtils.toJson(result));
        }
    }
}
