/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.bccs.bo.cm.pos.Subscriber;
import com.viettel.bccs.cm.dao.ActionLogPrDAO;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.dao.im.ReasonDao;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.im.SaleAnyPayFile;
import com.viettel.bccs.cm.supplier.ApParamSupplier;
import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.bccs.cm.supplier.RequestChangeInformationSupplier;
import com.viettel.bccs.cm.supplier.im.ImSupplier;
import com.viettel.bccs.cm.util.CommonUtils;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.DateTimeUtils;
import com.viettel.bccs.merchant.dao.MerchantDAO;
import com.viettel.bccs.merchant.model.Merchant;
import com.viettel.brcd.common.util.ResourceBundleUtils;
import com.viettel.brcd.dao.im.SaleTransNotInvoice;
import com.viettel.brcd.ws.model.output.DataTopUpPinCode;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.im.database.BO.SaleServices;
import com.viettel.im.database.BO.SaleTrans;
import com.viettel.im.database.BO.SaleTransDetail;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import com.viettel.brcd.ws.bccsgw.model.Input;
import com.viettel.brcd.ws.bccsgw.model.Param;
import com.viettel.brcd.ws.common.WebServiceClient;
import com.viettel.brcd.ws.model.output.ExchangeEmoneyResponse;
import com.viettel.common.ExchMsg;
import com.viettel.im.database.BO.AccountAgent;
import com.vtc.provisioning.client.Exchange;
import java.util.Calendar;
import java.util.HashMap;
import java.util.logging.Level;

import com.viettel.bccs.merchant.dao.TopUpHRDAO;
import com.viettel.bccs.supplier.cm.pre.PreChangeSimSupplier;
import com.viettel.brcd.dao.im.IMDAO;
import com.viettel.im.database.BO.Reason;

/**
 *
 * @author cuongdm
 */
public class EmoneyController extends BaseController {

    private static final String SERVICE_TYPE_DATA = "DATA";
    private static final String SERVICE_TYPE_EXCHANGE = "EXCHANGE";
    private static final String SERVICE_TYPE_EXTEND_DATA = "DATA";
    private static final String SERVICE_TYPE_EXTEND_MONEY = "MONEY";
    private static final String SERVICE_TYPE_EXTEND_DAY = "DAY";
    private static final String ERROR_MISSING_PARAM = "01";
    private static final String ERROR_VALIDATION_PARAM = "02";
    private static final String ERROR_SUB = "03";
    private static final String ERROR_BCCS = "99";
    private static final String ERROR_ISDN_ALREADY_TOPUP = "01";
    private static final String VALIDITY_TOPUP_AMOUNT = "35";
    static java.util.logging.Logger log = java.util.logging.Logger.getLogger(EmoneyController.class.getName());

    public EmoneyController() {
        logger = Logger.getLogger(CustomerController.class);
    }

    public ParamOut buyData(HibernateHelper hibernateHelper, DataTopUpPinCode dataDeObj) {
        Session sessionCmPos = null;
        Session sessionIM = null;
        Session seessionMerchant = null;
        Session sessionCmPre = null;
        ParamOut result = new ParamOut();
        result.setErrorCode(ERROR_VALIDATION_PARAM);
        boolean isErorr = false;
        try {
            sessionCmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            sessionIM = hibernateHelper.getSession(Constants.SESSION_IM);
            seessionMerchant = hibernateHelper.getSession(Constants.SESSION_MERCHANT);
            sessionCmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            if (dataDeObj == null
                    || dataDeObj.getIsdn() == null || dataDeObj.getIsdn().isEmpty()
                    || dataDeObj.getTime() == null || dataDeObj.getTime().isEmpty()
                    || dataDeObj.getRequestId() == null || dataDeObj.getRequestId().isEmpty()
                    || dataDeObj.getPackageCode() == null || dataDeObj.getPackageCode().isEmpty()
                    || dataDeObj.getServiceType() == null || dataDeObj.getServiceType().isEmpty()) {
                result.setErrorDecription("Param input is not null or empty");
                result.setErrorCode(ERROR_MISSING_PARAM);
            } else {
                String isdn = dataDeObj.getIsdn();
                if (isdn.startsWith("0")) {
                    isdn = isdn.replaceFirst("0", "");
                }
                if (isdn.startsWith("855")) {
                    isdn = isdn.replaceFirst("855", "");
                }
                dataDeObj.setIsdn(isdn);
                StringBuilder saleTransId = new StringBuilder();
                String staff = SERVICE_TYPE_DATA.equals(dataDeObj.getServiceType()) ? Constants.VTC_DATA : Constants.VTC_EXCHANGE;
                String message = makeSaleTrans(sessionIM, sessionCmPos, sessionCmPre, seessionMerchant, dataDeObj, staff, saleTransId, dataDeObj.getPackageCode(), null);
                if (message == null) {
                    if ("true".equalsIgnoreCase(ResourceBundleUtils.getResource("provisioning.allow.send.request", "provisioning"))) {
                        String service;
                        List<ApParam> listParam = new ApParamSupplier().findByTypeMerchant(seessionMerchant, Constants.PARAM_TYPE_SALE_SERVICE_CODE, dataDeObj.getPackageCode(), null);
                        if (listParam == null || listParam.isEmpty()) {
                            result.setErrorDecription("Can not find command for " + dataDeObj.getPackageCode());
                            result.setErrorCode(ERROR_VALIDATION_PARAM);
                        }
                        if (SERVICE_TYPE_DATA.equals(dataDeObj.getServiceType())) {
                            service = "buyDataEmoney";
                        } else if (SERVICE_TYPE_EXCHANGE.equals(dataDeObj.getServiceType())) {
                            service = "exchangeMoneyEmoney";
                        } else {
                            isErorr = true;
                            result.setErrorCode(ERROR_MISSING_PARAM);
                            return result;
                        }
                        BaseSupplier supplier = new BaseSupplier();
                        Long id = supplier.getSequence(sessionCmPos, "action_log_pr_seq");
                        Input input = new Input();
                        List<Param> lstParam = new ArrayList<Param>();
                        lstParam.add(new Param("requestId", id.toString()));
                        lstParam.add(new Param("msisdn", "855" + isdn));
                        lstParam.add(new Param("command", listParam.get(0).getParamValue()));
                        input.setParam(lstParam);

                        ExchangeEmoneyResponse response = (ExchangeEmoneyResponse) new WebServiceClient().sendRequestViaBccsGW(input, service, ExchangeEmoneyResponse.class);

                        new ActionLogPrDAO().insert(sessionCmPos, new Date(), dataDeObj.getMerchantAccount(), "mBCCS", isdn, gson.toJson(lstParam), gson.toJson(response), response != null && response.getReturn() != null ? response.getReturn().getErrCode() : "", null);
                        commitTransactions(sessionCmPos);
                        sessionCmPos.beginTransaction();

                        if (response != null && response.getReturn() != null && "0".equals(response.getReturn().getErrCode())) {
                            result.setErrorCode(Constants.ERROR_CODE_0);
                            result.setErrorDecription("Success");
                            result.setSaleTransId(saleTransId.toString());
                        } else if ((response != null && response.getReturn() != null && !"0".equals(response.getReturn().getErrCode()))
                                || (response == null && response.getReturn() == null)) {
                            result.setErrorCode(Constants.ERROR_CODE_2);
                            String messageOcs = response != null && response.getReturn() != null && response.getReturn().getErrOcs() != null ? response.getReturn().getErrOcs() : "";
                            result.setErrorDecription("OCS failed " + messageOcs);
                        }
                    } else {
                        result.setErrorCode(Constants.ERROR_CODE_0);
                        result.setErrorDecription("Success");
                        result.setSaleTransId(saleTransId.toString());
                    }
                    /**/
                } else {
                    if (ERROR_SUB.equals(message)) {
                        result.setErrorDecription("Phone number is not existed or not correct. Please check again!");
                        result.setErrorCode(ERROR_SUB);
                    } else {
                        result.setErrorDecription(message);
                    }
                    isErorr = true;
                }
            }
        } catch (Exception ex) {
            result.setErrorCode(ERROR_BCCS);
            logger.error("exchangeMoney: " + ex.getMessage());
            result.setErrorDecription(ex.getMessage());
            isErorr = true;
        } finally {
            logger.error("exchangeMoney end: " + new Date());
            if (isErorr) {
                rollBackTransactions(sessionIM, sessionCmPos, seessionMerchant);
            } else {
                commitTransactions(sessionIM, sessionCmPos, seessionMerchant);
            }
            closeSessions(sessionCmPos, sessionIM, seessionMerchant, sessionCmPre);
        }
        return result;
    }

    private String makeSaleTrans(Session sessionIM, Session sessionCmPos, Session sessionCmPre, Session seessionMerchant, DataTopUpPinCode dataDeObj, String shopSaleCode, StringBuilder saleTransIdStr, String saleServiceCode, Long quantity) throws Exception {
        logger.error("makeSaleTrans: " + dataDeObj.getRequestId());
        /*ActionLogDAO actionLogDAO = new ActionLogDAO();
         if (actionLogDAO.isRequestIdExists(seessionMerchant, requestId)) {
         return "reqeustId is used, please use another requestId";
         }*/
        ApParamSupplier paramSup = new ApParamSupplier();
        /*Lay thong tin agent*/
        logger.error("makeSaleTrans: " + dataDeObj.getRequestId() + " - " + "Lay thong tin agent");

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            Date date = formatter.parse(dataDeObj.getTime());
            long range = Math.abs(new Date().getTime() - date.getTime());
            if (range >= 15 * 60 * 1000) {
                return "Date time is expired";
            }
        } catch (Exception ex) {
            return "Date time is expired";
        }

        List<ApParam> apParam = paramSup.findByTypeMerchant(seessionMerchant, Constants.PARAM_TYPE_STAFF_ONLINE, shopSaleCode, null);
        if (apParam == null || apParam.isEmpty()) {
            return "Can not find staff sale this product";
        }
        Staff staffVTC = new StaffDAO().findByCode(sessionCmPos, apParam.get(0).getParamValue());
        if (staffVTC == null) {
            return "Can not find " + shopSaleCode;
        } else {
            String name = "";
            Long receiverId = null;
            Long receiverType = null;
            logger.error("makeSaleTrans: " + dataDeObj.getRequestId() + " - " + "Lay thong tin mat hang");
            SaleTransNotInvoice saleTransNotInvoice = new SaleTransNotInvoice();
            SaleServices saleService = com.viettel.im.database.DAO.SaleServicesDAO.findByCode(sessionIM, saleServiceCode);
            if (saleService == null) {
                return "Sale service is not found";
            }
            //String pricePolicy = findPricePolicy(apSaleTrans.getShopId(), apSaleTrans.getStaffId(), sessionDB);
            com.viettel.im.database.BO.SaleServicesPrice saleServicesPrice = saleTransNotInvoice.findSaleServicesPrice(saleService.getSaleServicesId(), "1", sessionIM);
            if (saleServicesPrice == null) {
                return "Sale service price is not found";
            }
            if (dataDeObj.getDealerCode() != null && !dataDeObj.getDealerCode().isEmpty()) {
                AccountAgent accountAgent = new ImSupplier().checkAccountAgentByIsdn(sessionIM, dataDeObj.getDealerCode());
                if (accountAgent == null) {
                    Shop dealer = new ShopDAO().findByCode(sessionIM, dataDeObj.getDealerCode());
                    if (dealer == null) {
                        Staff staff = new StaffDAO().findByCode(sessionCmPos, dataDeObj.getDealerCode());
                        if (staff == null) {
                            return "Can not find dealer code";
                        } else {
                            name = staff.getStaffCode() + " - " + staff.getName();
                            receiverId = staff.getStaffId();
                            receiverType = Constants.OWNER_TYPE_STAFF;
                        }
                    } else {
                        name = dealer.getShopCode() + " - " + dealer.getName();
                        receiverId = dealer.getShopId();
                        receiverType = Constants.OWNER_TYPE_SHOP;
                    }
                } else {
                    if (Constants.OWNER_TYPE_SHOP.equals(accountAgent.getOwnerType())) {
                        Shop dealer = new ShopDAO().findByCode(sessionCmPos, accountAgent.getOwnerCode());
                        if (dealer == null) {
                            return "Can not find dealer code";
                        }
                        name = dealer.getShopCode() + " - " + dealer.getName();
                        receiverId = dealer.getShopId();
                        receiverType = Constants.OWNER_TYPE_SHOP;
                    } else {
                        Staff staff = new StaffDAO().findByCode(sessionCmPos, accountAgent.getOwnerCode());
                        if (staff == null) {
                            return "Can not find dealer code";
                        }
                        name = staff.getStaffCode() + " - " + staff.getName();
                        receiverId = staff.getStaffId();
                        receiverType = Constants.OWNER_TYPE_STAFF;
                    }
                }
            }
            List<com.viettel.bccs.cm.model.SubMb> subMb = new RequestChangeInformationSupplier().getSubMbFromISDN(sessionCmPre, dataDeObj.getIsdn());
            if (subMb == null || subMb.isEmpty()) {
                return ERROR_SUB;
            }
            Double amount = saleServicesPrice.getPrice();
            if (quantity != null) {
                amount = quantity * saleServicesPrice.getPrice();
            }
            Double temp = 1.0 + (saleServicesPrice.getVat() / 100.0);
            Double notTax = amount / temp;
            Double tax = amount - notTax;
            BaseSupplier supplier = new BaseSupplier();

            /*Save sale_trans*/
            SaleTrans saleTrans = new SaleTrans();
            Long saleTransId = supplier.getSequence(sessionIM, "SALE_TRANS_SEQ");
            saleTransIdStr.append(saleTransId.toString());
            saleTrans.setSaleTransId(saleTransId);
            saleTrans.setSaleTransDate(new Date());
            saleTrans.setSaleTransType(Constants.SALE_TRANS_TYPE_SERVICE); //loai giao dich: lam dich vu
            saleTrans.setStatus(String.valueOf(Constants.SALE_PAY_NOT_BILL)); //da thanh toan nhung chua lap hoa don
            saleTrans.setShopId(staffVTC.getShopId());/*shop_id cua nv ban hang*/
            saleTrans.setStaffId(staffVTC.getStaffId());/*staff_id cua nv ban hang*/
            saleTrans.setPayMethod("3");
            /*1: cash, 2: account transfer, 3: emoney*/
            saleTrans.setSaleServiceId(saleService.getSaleServicesId());
            saleTrans.setSaleServicePriceId(saleServicesPrice.getSaleServicesPriceId());
            saleTrans.setAmountTax(amount); //tong tien phai tra cua KH, = chua thue + thue - KM - C/Khau
            saleTrans.setAmountNotTax(notTax); //tien chua thue
            saleTrans.setVat(saleServicesPrice.getVat()); //tien vat
            saleTrans.setTax(tax);
            saleTrans.setSubId(subMb.get(0).getSubId());
            saleTrans.setIsdn(dataDeObj.getIsdn());
            saleTrans.setTelNumber(dataDeObj.getIsdn());

            //saleTrans.setCustName(shopAgent.getShopCode() + " - " + shopAgent.getName()); //doi voi ban hang cho CTV, luu thong tin truong nay la ten CTV
            if (receiverId != null) {
                saleTrans.setReceiverId(receiverId);
                saleTrans.setReceiverType(receiverType);
                saleTrans.setCustName(name);
            }
            saleTrans.setAddress(saleTrans.getAddress()); //CM day sang
            //saleTrans.setReasonId(4323l);
            saleTrans.setTelecomServiceId(18l);
            saleTrans.setSaleTransCode(formatTransCode(saleTransId));
            saleTrans.setCreateStaffId(staffVTC.getStaffId());/*staff_id cua nv ban hang*/

            saleTrans.setNote(staffVTC.getStaffCode());
            saleTrans.setInTransId(dataDeObj.getRequestId());
            sessionIM.save(saleTrans);
            sessionIM.flush();

            /*Save Sale_Trans_Detail*/
            SaleTransDetail saleTransDetail = new SaleTransDetail();
            Long saleTransDetailId = supplier.getSequence(sessionIM, "SALE_TRANS_DETAIL_SEQ");
            saleTransDetail.setSaleTransDetailId(saleTransDetailId);
            saleTransDetail.setSaleTransId(saleTrans.getSaleTransId());
            saleTransDetail.setSaleTransDate(saleTrans.getSaleTransDate());
            saleTransDetail.setStateId(Constants.STATE_NEW);
            saleTransDetail.setAmount(amount);
            saleTransDetail.setPrice(saleServicesPrice.getPrice());
            saleTransDetail.setPriceVat(saleServicesPrice.getVat());
            saleTransDetail.setSaleServicesCode(saleServiceCode);
            saleTransDetail.setSaleServicesName(saleService.getName());
            saleTransDetail.setSaleServicesPrice(saleServicesPrice.getPrice());
            saleTransDetail.setSaleServicesPriceVat(saleServicesPrice.getVat());
            saleTransDetail.setVatAmount(tax);
            saleTransDetail.setSaleServicesId(saleService.getSaleServicesId());
            saleTransDetail.setSaleServicesPriceId(saleServicesPrice.getSaleServicesPriceId());
            saleTransDetail.setQuantity(quantity == null ? 1 : quantity);
            sessionIM.save(saleTransDetail);
            sessionIM.flush();
        }
        return null;
    }

    public static String formatTransCode(Long transId) {
        return Constants.TRANS_CODE_PREFIX + String.format("%0" + Constants.TRANS_ID_LENGTH + "d", transId);
    }

    public ParamOut extendTelecomService(HibernateHelper hibernateHelper, DataTopUpPinCode dataDeObj) {
        ParamOut result = new ParamOut();
        result.setErrorCode(ERROR_VALIDATION_PARAM);

        Session sessionMerchant = null;
        Session sessionIM = null;
        Session sessionCmPre = null;
        Boolean isError = false;
        try {
            sessionMerchant = hibernateHelper.getSession(Constants.SESSION_MERCHANT);
            sessionCmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            sessionIM = hibernateHelper.getSession(Constants.SESSION_IM);

            if ("STAFFTOPUP".equals(dataDeObj.getReasonTopup())) {
                //Validaty parameters 
                if (dataDeObj == null
                        || dataDeObj.getIsdn() == null || dataDeObj.getIsdn().isEmpty()
                        || dataDeObj.getRequestId() == null || dataDeObj.getRequestId().isEmpty()
                        || dataDeObj.getServiceType() == null || dataDeObj.getServiceType().isEmpty()
                        || dataDeObj.getAmountBasic() == null || dataDeObj.getAmountPromotion() == null) {
                    result.setErrorDecription("Param input is not null or empty");
                    result.setErrorCode(ERROR_MISSING_PARAM);
                }
                String msisdn = dataDeObj.getIsdn();
                msisdn = msisdn.startsWith("0") ? msisdn.substring(1) : msisdn;
                msisdn = msisdn.startsWith("855") ? msisdn.substring(3) : msisdn;
                //step 1: check topup already
                TopUpHRDAO topUpHRDao = new TopUpHRDAO();
                boolean isTopupAlready = topUpHRDao.checkTopUpHoi(sessionIM, msisdn);
                if (isTopupAlready) {
                    result.setErrorDecription("ISDN has already TOPUP for this month.");
                    result.setErrorCode(ERROR_ISDN_ALREADY_TOPUP);
                    return result;
                }

                //step 2: check isdn invalid(deleted, inactive, wrong format)
                Subscriber sub = new PreChangeSimSupplier().getSubMbPre(sessionCmPre.connection(), msisdn);
                if (sub != null) {
                    if ("03".equals(sub.getActStatus())) { //inactive
                        result.setErrorDecription("ISDN does not exist.");
                        result.setErrorCode(ERROR_SUB);
                        return result;
                    }
                } else { //Cannot find isdn
                    result.setErrorDecription("ISDN does not exist.");
                    result.setErrorCode(ERROR_SUB);
                    return result;
                }

                //step 3: check limit ,id = 201343
                Reason reason = new ReasonDao().findById(sessionIM, 201343L);
                IMDAO imDao = new IMDAO();
                Long limitTopUpBasic = imDao.findAppParams(sessionIM, "TOPUP_COPR_LIMIT", reason.getReasonCode(), "TOPUP_BASIC");
                Long limitTopUpPromotion = imDao.findAppParams(sessionIM, "TOPUP_COPR_LIMIT", reason.getReasonCode(), "TOPUP_PROMOTION");

                /*validate limit*/
                if (limitTopUpBasic != null && dataDeObj.getAmountBasic() > limitTopUpBasic) {
                    result.setErrorDecription("Amount topup over limit!");
                    result.setErrorCode(ERROR_SUB);
                    return result;
                }

                if (limitTopUpPromotion != null && dataDeObj.getAmountPromotion() > limitTopUpPromotion) {
                    result.setErrorDecription("Amount promotion topup over limit!");
                    result.setErrorCode(ERROR_SUB);
                    return result;
                }

                //Step 4: Build data for insert
                Calendar c = Calendar.getInstance();
                String code = "GHR_" + (c.get(Calendar.MONTH) + 1) + "_" + c.get(Calendar.YEAR);

                //Ignore IP and REASON_CM_ID, apply for web only
                SaleAnyPayFile temp = new SaleAnyPayFile();
                temp.setCode(code);
                temp.setSubId(sub.getSubId());
                temp.setIsdn(msisdn);
                temp.setSerial(sub.getSerial());
                temp.setStatus(0L);
                temp.setCreateUser("VTC_AUTO_GHR");
                temp.setCreateDate(DateTimeUtils.getBeginingOfFirstDayOfCurrentMonth());
                temp.setTopupBasic(CommonUtils.checkIntAndLong(dataDeObj.getAmountBasic()).toString());
                temp.setTopupBasicValid(VALIDITY_TOPUP_AMOUNT);
                temp.setTopupPromotion(CommonUtils.checkIntAndLong(dataDeObj.getAmountPromotion()).toString());
                temp.setPromotionValid(VALIDITY_TOPUP_AMOUNT);
                String contentSMS = "You have just been added $" + dataDeObj.getAmountBasic().toString() + " in basic account, $" + dataDeObj.getAmountPromotion().toString() + " in promotion account, 0Mbps data, 35 validity dates";
                temp.setSmsContent(contentSMS);
                temp.setProgram("INTERNAL_TOPUP");
                temp.setDocumentNo("Top up for GHR");
                temp.setTransType("88");
                temp.setReasonId(reason.getReasonId());
                temp.setProcessStatus(0L);
                temp.setReasonName(reason.getReasonName());
                temp.setIsCorporate(0L);
                temp.setDescription("Wait");
                temp.setTopupData("0");
                temp.setDataValid(VALIDITY_TOPUP_AMOUNT);
                temp.setReasonCmId(13455L); // reasonCode = 'STAFFTOPUP' on cm.pre

                int numberOfRecordInsert = imDao.insertSaleAnypayFile(sessionIM, temp);
                if (numberOfRecordInsert == 1) {
                    result.setErrorCode(Constants.ERROR_CODE_0);
                    result.setErrorDecription("Success");
                } else {
                    result.setErrorCode(Constants.ERROR_CODE_1);
                    result.setErrorDecription("Error internal topup, please contact admin.!");
                }
                return result;
            } // Old flow
            else {
                if (dataDeObj == null
                        || dataDeObj.getIsdn() == null || dataDeObj.getIsdn().isEmpty()
                        || dataDeObj.getRequestId() == null || dataDeObj.getRequestId().isEmpty()
                        || dataDeObj.getServiceType() == null || dataDeObj.getServiceType().isEmpty()
                        || dataDeObj.getAmount() == null) {
                    result.setErrorDecription("Param input is not null or empty");
                    result.setErrorCode(ERROR_MISSING_PARAM);
                } else {
                    if (dataDeObj.getAmount() != dataDeObj.getAmount().intValue()) {
                        result.setErrorDecription("Amount must be integer");
                        return result;
                    }
                    Exchange exchange = new Exchange();
                    String msisdn = dataDeObj.getIsdn();
                    msisdn = msisdn.startsWith("0") ? msisdn.substring(1) : msisdn;
                    msisdn = msisdn.startsWith("855") ? msisdn : "855" + msisdn;
                    HashMap<String, ExchMsg> lstResult;
                    String flag = ResourceBundleUtils.getResource("provisioning.allow.send.request", "provisioning");
                    if ("true".equalsIgnoreCase(flag)) {
                        String amountStr = String.valueOf(dataDeObj.getAmount().intValue());
                        if (SERVICE_TYPE_EXTEND_DATA.equals(dataDeObj.getServiceType())) {
                            if (dataDeObj.getAccount() == null || dataDeObj.getIsdn().isEmpty()
                                    || dataDeObj.getDateExpired() == 0) {
                                result.setErrorDecription("Param input is not null or empty");
                                result.setErrorCode(ERROR_MISSING_PARAM);
                                return result;
                            }
                            if (dataDeObj.getAmount() != dataDeObj.getAmount().intValue()) {
                                result.setErrorDecription("Amount must be integer");
                                return result;
                            }
                            String byteData = String.valueOf(dataDeObj.getAmount().longValue() * 1024l * 1024l);
                            lstResult = exchange.addBalanceExp(msisdn, dataDeObj.getAccount(), byteData, dataDeObj.getDateExpired());
                        } else if (SERVICE_TYPE_EXTEND_DAY.equals(dataDeObj.getServiceType())) {
                            lstResult = exchange.addValidity(msisdn, amountStr);
                        } else if (SERVICE_TYPE_EXTEND_MONEY.equals(dataDeObj.getServiceType())) {
                            if (dataDeObj.getAccount() == null || dataDeObj.getIsdn().isEmpty()
                                    || dataDeObj.getDateExpired() == 0) {
                                result.setErrorDecription("Param input is not null or empty");
                                result.setErrorCode(ERROR_MISSING_PARAM);
                            }
                            String byteData = String.valueOf(dataDeObj.getAmount().longValue() * 1000000l);
                            lstResult = exchange.addBalanceExp(msisdn, dataDeObj.getAccount(), byteData, dataDeObj.getDateExpired());
                        } else {
                            result.setErrorDecription("Type is invalid");
                            return result;
                        }
                        if (lstResult != null) {
                            ExchMsg request = lstResult.get("REQUEST");
                            log.log(Level.INFO, request.toString());
                            ExchMsg response = lstResult.get("RESPONSE");
                            log.log(Level.INFO, response.toString());
                            if (Integer.valueOf(response.getError()) == 0) {
                                result.setErrorCode(Constants.ERROR_CODE_0);
                                result.setErrorDecription("Success");
                            } else {
                                result.setErrorCode(Constants.ERROR_CODE_1);
                                result.setErrorDecription(response.getDescription());
                            }
                        } else {
                            result.setErrorCode(Constants.ERROR_CODE_2);
                            result.setErrorDecription("OCS failed");
                        }
                    } else if ("false".equalsIgnoreCase(flag)) {
                        result.setErrorCode(Constants.ERROR_CODE_0);
                        result.setErrorDecription("Success");
                    } else {
                        result.setErrorCode(Constants.ERROR_CODE_2);
                        result.setErrorDecription("OCS failed");
                    }
                }
            }
        } catch (Exception ex) {
            result.setErrorCode(ERROR_BCCS);
            logger.error("extendTelecomService: " + ex.getMessage());
            result.setErrorDecription(ex.getMessage());
            isError = true;
        } finally {
            logger.error("exchangeMoney end: " + new Date());
            if (isError) {
                rollBackTransactions(sessionMerchant);
                rollBackTransactions(sessionIM);
                rollBackTransactions(sessionCmPre);
            } else {
                commitTransactions(sessionMerchant);
                commitTransactions(sessionIM);
                commitTransactions(sessionCmPre);
            }
            closeSessions(sessionMerchant);
            closeSessions(sessionIM);
            closeSessions(sessionCmPre);
        }
        return result;
    }

    public ParamOut buyDataVAS(HibernateHelper hibernateHelper, DataTopUpPinCode dataDeObj) {
        Session sessionCmPos = null;
        Session sessionIM = null;
        Session seessionMerchant = null;
        Session sessionCmPre = null;
        ParamOut result = new ParamOut();
        result.setErrorCode(ERROR_VALIDATION_PARAM);
        boolean isErorr = false;
        try {
            sessionCmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            sessionIM = hibernateHelper.getSession(Constants.SESSION_IM);
            seessionMerchant = hibernateHelper.getSession(Constants.SESSION_MERCHANT);
            sessionCmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            if (dataDeObj == null
                    || dataDeObj.getMerchantAccount() == null || dataDeObj.getMerchantAccount().isEmpty()
                    || dataDeObj.getIsdn() == null || dataDeObj.getIsdn().isEmpty()
                    || dataDeObj.getTime() == null || dataDeObj.getTime().isEmpty()
                    || dataDeObj.getRequestId() == null || dataDeObj.getRequestId().isEmpty()
                    || dataDeObj.getAmount() == null || dataDeObj.getAmount() <= 0) {
                result.setErrorDecription("Param input is not null or empty");
                result.setErrorCode(ERROR_MISSING_PARAM);
            } else {
                String isdn = dataDeObj.getIsdn();
                if (isdn.startsWith("0")) {
                    isdn = isdn.replaceFirst("0", "");
                }
                if (isdn.startsWith("855")) {
                    isdn = isdn.replaceFirst("855", "");
                }
                dataDeObj.setIsdn(isdn);
                ApParamSupplier paramSup = new ApParamSupplier();
                List<ApParam> apParam = paramSup.findByTypeMerchant(seessionMerchant, "DATA_BLOCK", dataDeObj.getMerchantAccount(), null);
                StringBuilder saleTransId = new StringBuilder();
                String staff = Constants.VTC_DATA_VAS;
                String salServiceCode = "DATA";
                List<Merchant> merchant = new MerchantDAO().getMerchantByAccount(seessionMerchant, dataDeObj.getMerchantAccount());
                if (merchant == null || merchant.isEmpty()) {
                    result.setErrorDecription("Merchant code not exists");
                    return result;
                }
                dataDeObj.setDealerCode(merchant.get(0).getIsdn());
                Long mbBlock = 50l;
                if (apParam != null && !apParam.isEmpty() && apParam.get(0).getParamValue() != null && !apParam.get(0).getParamValue().isEmpty()) {
                    mbBlock = Long.parseLong(apParam.get(0).getParamValue());
                }
                Double quantity = dataDeObj.getAmount() * 100 * mbBlock;
                String message = makeSaleTrans(sessionIM, sessionCmPos, sessionCmPre, seessionMerchant, dataDeObj, staff, saleTransId, salServiceCode, quantity.longValue());
                if (message == null) {
                    Long mbData = quantity.longValue();
                    if ("true".equalsIgnoreCase(ResourceBundleUtils.getResource("provisioning.allow.send.request", "provisioning"))) {
                        extendData(sessionCmPos, seessionMerchant, dataDeObj.getMerchantAccount(), isdn, "4560", mbData, result);
                        if (Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
                            result.setSaleTransId(saleTransId.toString());
                        }
                    } else {
                        result.setErrorCode(Constants.ERROR_CODE_0);
                        result.setErrorDecription("Success");
                        result.setSaleTransId(saleTransId.toString());
                        result.setMbData(mbData);
                        Calendar c = Calendar.getInstance();
                        c.add(Calendar.DAY_OF_MONTH, 7);
                        result.setExpiredDate(new SimpleDateFormat("dd/MM/yyyy").format(c.getTime()) + " 00:00:00");
                    }
                } else {
                    if (ERROR_SUB.equals(message)) {
                        result.setErrorDecription("Phone number is not existed or not correct. Please check again!");
                        result.setErrorCode(ERROR_SUB);
                    } else {
                        result.setErrorDecription(message);
                    }
                    isErorr = true;
                }
            }
        } catch (Exception ex) {
            result.setErrorCode(ERROR_BCCS);
            logger.error("exchangeMoney: " + ex.getMessage());
            result.setErrorDecription(ex.getMessage());
            isErorr = true;
        } finally {
            logger.error("exchangeMoney end: " + new Date());
            if (isErorr) {
                rollBackTransactions(sessionIM, sessionCmPos, seessionMerchant);
            } else {
                commitTransactions(sessionIM, sessionCmPos, seessionMerchant);
            }
            closeSessions(sessionCmPos, sessionIM, seessionMerchant, sessionCmPre);
        }
        return result;
    }

    public ParamOut buyDataVASPartner(HibernateHelper hibernateHelper, DataTopUpPinCode dataDeObj) {
        Session sessionCmPos = null;
        Session sessionIM = null;
        Session seessionMerchant = null;
        Session sessionCmPre = null;
        ParamOut result = new ParamOut();
        result.setErrorCode(ERROR_VALIDATION_PARAM);
        boolean isErorr = false;
        try {
            sessionCmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            sessionIM = hibernateHelper.getSession(Constants.SESSION_IM);
            seessionMerchant = hibernateHelper.getSession(Constants.SESSION_MERCHANT);
            sessionCmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            if (dataDeObj == null
                    || dataDeObj.getMerchantAccount() == null || dataDeObj.getMerchantAccount().isEmpty()
                    || dataDeObj.getIsdn() == null || dataDeObj.getIsdn().isEmpty()
                    || dataDeObj.getTime() == null || dataDeObj.getTime().isEmpty()
                    || dataDeObj.getRequestId() == null || dataDeObj.getRequestId().isEmpty()
                    || dataDeObj.getAmount() == null || dataDeObj.getAmount() <= 0) {
                result.setErrorDecription("Param input is not null or empty");
                result.setErrorCode(ERROR_MISSING_PARAM);
            } else {
                String isdn = dataDeObj.getIsdn();
                if (isdn.startsWith("0")) {
                    isdn = isdn.replaceFirst("0", "");
                }
                if (isdn.startsWith("855")) {
                    isdn = isdn.replaceFirst("855", "");
                }
                dataDeObj.setIsdn(isdn);
                StringBuilder saleTransId = new StringBuilder();
                String staff = Constants.VTC_DATA_VAS;
                String salServiceCode = "DATA";
                List<Merchant> merchant = new MerchantDAO().getMerchantByAccount(seessionMerchant, dataDeObj.getMerchantAccount());
                if (merchant == null || merchant.isEmpty()) {
                    result.setErrorDecription("Merchant code not exists");
                    return result;
                }
                dataDeObj.setDealerCode(merchant.get(0).getIsdn());
                String message = makeSaleTrans(sessionIM, sessionCmPos, sessionCmPre, seessionMerchant, dataDeObj, staff, saleTransId, salServiceCode, dataDeObj.getAmount().longValue());
                if (message == null) {
                    if ("true".equalsIgnoreCase(ResourceBundleUtils.getResource("provisioning.allow.send.request", "provisioning"))) {
                        extendData(sessionCmPos, seessionMerchant, dataDeObj.getMerchantAccount(), isdn, "4560", dataDeObj.getAmount().longValue(), result);
                        if (Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
                            result.setSaleTransId(saleTransId.toString());
                        }
                    } else {
                        result.setErrorCode(Constants.ERROR_CODE_0);
                        result.setErrorDecription("Success");
                        result.setSaleTransId(saleTransId.toString());
                    }
                } else {
                    if (ERROR_SUB.equals(message)) {
                        result.setErrorDecription("Phone number is not existed or not correct. Please check again!");
                        result.setErrorCode(ERROR_SUB);
                    } else {
                        result.setErrorDecription(message);
                    }
                    isErorr = true;
                }
            }
        } catch (Exception ex) {
            result.setErrorCode(ERROR_BCCS);
            logger.error("exchangeMoney: " + ex.getMessage());
            result.setErrorDecription(ex.getMessage());
            isErorr = true;
        } finally {
            logger.error("exchangeMoney end: " + new Date());
            if (isErorr) {
                rollBackTransactions(sessionIM, sessionCmPos, seessionMerchant);
            } else {
                commitTransactions(sessionIM, sessionCmPos, seessionMerchant);
            }
            closeSessions(sessionCmPos, sessionIM, seessionMerchant, sessionCmPre);
        }
        return result;
    }

    void extendData(Session sessionCmPos, Session seessionMerchant, String merchantAccount, String isdn, String account, Long mbData, ParamOut result) throws Exception {
        int dateExpired = 0;
        String byteData = String.valueOf(mbData * 1048576l);
        ApParamSupplier paramSup = new ApParamSupplier();
        List<ApParam> apParam = paramSup.findByTypeMerchant(seessionMerchant, "DATA_DATE_EXPIRED", merchantAccount, null);
        if (apParam == null || apParam.isEmpty()) {
            apParam = paramSup.findByTypeMerchant(seessionMerchant, "DATA_DATE_EXPIRED", "DEFAULT", null);
            dateExpired = Integer.parseInt(apParam.get(0).getParamValue());
        } else {
            String[] arr = apParam.get(0).getParamValue().split(";");
            for (int i = 0; i < arr.length; i++) {
                String[] arrTemp = arr[i].split(",");
                if (mbData.intValue() <= Integer.parseInt(arrTemp[0])) {
                    dateExpired = Integer.parseInt(arrTemp[1]);
                    break;
                }
            }
        }
        Exchange exchange = new Exchange();
        HashMap<String, ExchMsg> lstResult = exchange.addBalanceExp(isdn, account, byteData, dateExpired);
        if (lstResult != null) {
            ExchMsg request = lstResult.get("REQUEST");
            log.log(Level.INFO, request.toString());
            ExchMsg response = lstResult.get("RESPONSE");
            log.log(Level.INFO, response.toString());
            if (Integer.valueOf(response.getError()) == 0) {
                result.setErrorCode(Constants.ERROR_CODE_0);
                result.setErrorDecription("Success");
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DAY_OF_MONTH, dateExpired);
                result.setExpiredDate(new SimpleDateFormat("dd/MM/yyyy").format(c.getTime()) + " 00:00:00");
                result.setMbData(mbData);
            } else {
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription(response.getDescription());
            }
            new ActionLogPrDAO().insert(sessionCmPos, new Date(), merchantAccount, "mBCCS", isdn, lstResult.get("REQUEST").toString(), lstResult.get("RESPONSE").toString(), "0", null);
            commitTransactions(sessionCmPos);
            sessionCmPos.beginTransaction();
        } else {
            result.setErrorCode(Constants.ERROR_CODE_2);
            result.setErrorDecription("OCS failed");
        }
    }
}
