package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.LocalProductPpBussiness;
import com.viettel.bccs.cm.model.LocalProductPp;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.model.output.LocalProductPpOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class LocalProductPpController extends BaseController {

    public LocalProductPpController() {
        logger = Logger.getLogger(LocalProductPpController.class);
    }

    public LocalProductPpOut getListLocalProductPP(HibernateHelper hibernateHelper, Long offerId, String locale) {
        LocalProductPpOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<LocalProductPp> localProductPps = new LocalProductPpBussiness().findByOfferId(cmPosSession, offerId);
            result = new LocalProductPpOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), localProductPps);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new LocalProductPpOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "LocalProductPpController.getListLocalProductPP:result=" + LogUtils.toJson(result));
        }
    }
}
