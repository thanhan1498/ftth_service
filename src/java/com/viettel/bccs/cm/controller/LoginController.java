package com.viettel.bccs.cm.controller;

import com.viettel.bccs.api.Task.BO.ApParam;
import com.viettel.bccs.api.Task.DAO.ApParamDAO;
import com.viettel.bccs.cm.bussiness.CmUserTokenBusiness;
import com.viettel.bccs.cm.model.CmUserToken;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.Token;
import com.viettel.bccs.cm.bussiness.ShopBussiness;
import com.viettel.bccs.cm.bussiness.StaffBussiness;
import com.viettel.bccs.cm.bussiness.TokenBussiness;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.ws.model.output.LoginOut;
import com.viettel.bccs.cm.bussiness.common.DecryptBusiness;
import com.viettel.bccs.cm.supplier.im.ImSupplier;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ValidateUtils;
import com.viettel.bccs.merchant.dao.MerchantDAO;
import com.viettel.bccs.merchant.model.Merchant;
import com.viettel.brcd.common.util.PerformanceLogger;
import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.brcd.ws.vsa.ResourceBundleUtil;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.im.database.BO.AccountAgent;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class LoginController extends BaseController {

    public LoginController() {
        logger = Logger.getLogger(PerformanceLogger.class);
    }

    public LoginOut login(HibernateHelper hibernateHelper, String user, String pass, String serial, String locale) {
        LoginOut result = null;
        Session cmPosSession = null;
        Session imSession = null;
        boolean hasErr = false;
        try {
            //<editor-fold defaultstate="collapsed" desc="validate">
            //<editor-fold defaultstate="collapsed" desc="locale">
            String mess = validateLocale(locale);
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    result = new LoginOut(Constants.ERROR_CODE_1, mess);
                    return result;
                }
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="user">
            String userName = new DecryptBusiness().getUserName(user);
            if (userName == null || userName.isEmpty()) {
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("error.decrypt", locale));
                return result;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="pass">
            String passWord = new DecryptBusiness().getPassWord(pass, user);
            if (passWord == null || passWord.isEmpty()) {
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("error.decrypt", locale));
                return result;
            }
            //</editor-fold>
            //</editor-fold>
            Date sTime = new Date();
            LogUtils.info(logger, "Login SESSION_CM_POS: start time= " + sTime);
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            sTime = new Date();
            LogUtils.info(logger, "Login SESSION_CM_POS: end time= " + sTime);
            //<editor-fold defaultstate="collapsed" desc="staff">
            //</editor-fold>
            sTime = new Date();
            LogUtils.info(logger, "Login  SESSION_IM: start time= " + sTime);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            sTime = new Date();
            LogUtils.info(logger, "Login  SESSION_IM: end time= " + sTime);
            //<editor-fold defaultstate="collapsed" desc="token">
            //<editor-fold defaultstate="collapsed" desc="check user name clloaboratore">
            ImSupplier imSupplier = new ImSupplier();
            String userIsdn;
            Long ownerId;
            boolean isN3P3 = false;
            if (ValidateUtils.isNumeric(userName)) {
                userIsdn = userName;
                userName = userName.startsWith("855") ? userName.substring(3) : userName;
                userName = userName.startsWith("0") ? userName.substring(1) : userName;
                passWord = DecryptBusiness.encryptionPass(passWord);
                // check sim toolkit
                AccountAgent accountAgent = imSupplier.checkAccountAgentByIsdn(imSession, userName);
                if (accountAgent == null) {
                    result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("error.accountAgent", locale));
                    return result;
                }
                ownerId = accountAgent.getOwnerId();
                if (!imSupplier.checkPassword(imSession, userName, passWord)) {
                    result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("error.password", locale));
                    return result;
                }
                isN3P3 = imSupplier.checkAccountAgentIsN3_P3(imSession, accountAgent.getOwnerCode());
                userName = ResourceBundleUtil.getResource("USER_COL");
                passWord = ResourceBundleUtil.getResource("PASS_COL");
            } else {
                ownerId = null;
                userIsdn = "";
                // check sim toolkit
                if (!imSupplier.checkAccountAgentByOwnerCode(imSession, userName)) {
                    result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("error.accountAgent", locale));
                    return result;
                }
            }
            //</editor-fold>
            Staff staff;
            if (ownerId != null && ownerId > 0) {
                staff = new StaffBussiness().findById(cmPosSession, ownerId);
            } else {
                staff = new StaffBussiness().findByCode(cmPosSession, userName);
            }
            if (staff == null || !Constants.STATUS_USE.equals(staff.getStatus())) {
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("staff.does.not.exist.or.has.been.disabled", locale));
                return result;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="shop">
            Shop shop = new ShopBussiness().findById(cmPosSession, staff.getShopId());
            if (shop == null || !Constants.STATUS_USE.equals(shop.getStatus())) {
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("shop.does.not.exist.or.has.been.disabled", locale));
                return result;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="userToken">
            sTime = new Date();
            LogUtils.info(logger, "Login getUserToken : start time= " + sTime);
            CmUserToken userToken = new CmUserTokenBusiness().getUserToken(userName, passWord, locale, shop.getShopType(), isN3P3);
            sTime = new Date();
            LogUtils.info(logger, "Login getUserToken : end time= " + sTime);
            if (userToken == null) {
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("login.vsa.account.access.denie", locale));
                return result;
            }
            if (Constants.ERROR_CODE_1.equals(userToken.getResultCode())) {
                mess = userToken.getResultMessage();
                if (mess != null) {
                    mess = mess.trim();
                    if (!mess.isEmpty()) {
                        result = new LoginOut(Constants.ERROR_CODE_1, mess);
                        return result;
                    }
                }
            }
            Token token;
            System.out.println("------------->userIsdn" + userIsdn);
            if (userIsdn.isEmpty()) {
                token = new TokenBussiness().logToken(cmPosSession, imSession, userName, serial, staff.getStaffId(), staff.getName(), userToken, locale, null);
            } else {
                token = new TokenBussiness().logToken(cmPosSession, imSession, userIsdn, serial, staff.getStaffId(), staff.getName(), userToken, locale, null);
            }
            if (token == null) {
                hasErr = true;
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("login.vsa.account.access.denie", locale));
                return result;
            }
            if (Constants.ERROR_CODE_1.equals(token.getResultCode())) {
                mess = token.getResultMessage();
                if (mess != null) {
                    mess = mess.trim();
                    if (!mess.isEmpty()) {
                        hasErr = true;
                        result = new LoginOut(Constants.ERROR_CODE_1, mess);
                        return result;
                    }
                }
            }
            //</editor-fold>

            commitTransactions(cmPosSession, imSession);

            result = new LoginOut(staff, shop, token.getToken(), userToken, Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession, imSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession, imSession);
        }
    }

    public WSRespone changePasswordOnBccs(HibernateHelper hibernateHelper, String isdn, String oldPass, String newPass, String locale) {
        WSRespone result = new WSRespone();
        Session imSession = null;
        try {
            // validate
            if (isdn == null || isdn.isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("error.userName", locale));
                return result;
            }
            if (oldPass == null || oldPass.isEmpty() || newPass == null || newPass.isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("error.passWord", locale));
                return result;
            }
            if (!ValidateUtils.isNumeric(isdn)) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("error.isdn.invalid", locale));
                return result;
            }
            if (oldPass.equals(newPass)) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("error.same.pass", locale));
                return result;
            }
            if (!ValidateUtils.isNumeric(newPass)) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("error.pass.invalid", locale));
                return result;
            }
            if (newPass.length() != 6) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("error.pass.invalid.character", locale));
                return result;
            }
            Date sTime = new Date();
            LogUtils.info(logger, "Login  SESSION_IM: start time= " + sTime);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            sTime = new Date();
            LogUtils.info(logger, "Login  SESSION_IM: end time= " + sTime);
            //</editor-fold>
            // check password
            isdn = isdn.startsWith("855") ? isdn.substring(3) : isdn;
            isdn = isdn.startsWith("0") ? isdn.substring(1) : isdn;
            oldPass = DecryptBusiness.encryptionPass(oldPass);
            newPass = DecryptBusiness.encryptionPass(newPass);
            ImSupplier imSupplier = new ImSupplier();
            // check sim toolkit
            AccountAgent accountAgent = imSupplier.checkAccountAgentByIsdn(imSession, isdn);
            if (accountAgent == null) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("error.accountAgent", locale));
                return result;
            }
            // check old pass in DB
            if (!imSupplier.checkPassword(imSession, isdn, oldPass)) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("error.old.password.wrong", locale));
                return result;
            }
            // update pass
            accountAgent.setPassword(newPass);
            imSession.update(accountAgent);
            imSupplier.updatePassword(imSession, isdn, newPass);
            result.setErrorCode(Constants.ERROR_CODE_0);
            result.setErrorDecription(LabelUtil.getKey("common.success", locale));
            commitTransactions(imSession);
        } catch (Exception ex) {
            rollBackTransactions(imSession);
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(imSession);
        }
        return result;
    }

    public LoginOut loginEncryptRSA(HibernateHelper hibernateHelper, String userName, String pass, String locale, String deviceId) {
        LoginOut result = null;
        Session cmPosSession = null;
        Session imSession = null;
        boolean hasErr = false;
        try {
            //<editor-fold defaultstate="collapsed" desc="validate">
            //<editor-fold defaultstate="collapsed" desc="locale">
            System.out.println("listMember: " + locale);
            String mess = validateLocale(locale);
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    result = new LoginOut(Constants.ERROR_CODE_1, mess);
                    return result;
                }
            }
            //</editor-fold>
            String passWord = new DecryptBusiness().getPassWordDecryptRSA(pass);
            if (passWord == null || passWord.isEmpty()) {
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("error.decrypt", locale));
                return result;
            }
            //</editor-fold>
            Date sTime = new Date();
            LogUtils.info(logger, "Login SESSION_CM_POS: start time= " + sTime);
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            sTime = new Date();
            LogUtils.info(logger, "Login SESSION_CM_POS: end time= " + sTime);
            //</editor-fold>
            sTime = new Date();
            LogUtils.info(logger, "Login  SESSION_IM: start time= " + sTime);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            sTime = new Date();
            LogUtils.info(logger, "Login  SESSION_IM: end time= " + sTime);
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="check user name clloaboratore">
            ImSupplier imSupplier = new ImSupplier();
            String userIsdn;
            String ownerCode;
            boolean isN3P3 = false;
            if (ValidateUtils.isNumeric(userName)) {
                userName = userName.startsWith("855") ? userName.substring(3) : userName;
                userName = userName.startsWith("0") ? userName.substring(1) : userName;
                userIsdn = userName;
                passWord = DecryptBusiness.encryptionPass(passWord);
                // check sim toolkit
                AccountAgent accountAgent = imSupplier.checkAccountAgentByIsdn(imSession, userName);
                if (accountAgent == null) {
                    result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("error.accountAgent", locale));
                    return result;
                }
                ownerCode = accountAgent.getOwnerCode();
                if (!imSupplier.checkPassword(imSession, userName, passWord)) {
                    result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("error.password", locale));
                    return result;
                }
                isN3P3 = imSupplier.checkAccountAgentIsN3_P3(imSession, ownerCode);
                userName = ResourceBundleUtil.getResource("USER_COL");
                passWord = ResourceBundleUtil.getResource("PASS_COL");
            } else {
                ownerCode = "";
                userIsdn = "";
                /**
                 * @since 180221
                 * @des bo check sim toolkit voi nv
                 * @author Cuongdm
                 */
                // check sim toolkit
                /*if (!imSupplier.checkAccountAgentByOwnerCode(imSession, userName)) {
                 result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("error.accountAgent", locale));
                 return result;
                 }*/
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="token">
            //<editor-fold defaultstate="collapsed" desc="staff">
            Staff staff;
            System.out.println("------------->ownerCode" + ownerCode);
            if (!ownerCode.isEmpty()) {
                staff = new StaffBussiness().findByCode(cmPosSession, ownerCode);
            } else {
                staff = new StaffBussiness().findByCode(cmPosSession, userName);
            }
            if (staff == null || !Constants.STATUS_USE.equals(staff.getStatus())) {
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("staff.does.not.exist.or.has.been.disabled", locale));
                return result;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="shop">
            Shop shop = new ShopBussiness().findById(cmPosSession, staff.getShopId());
            if (shop == null || !Constants.STATUS_USE.equals(shop.getStatus())) {
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("shop.does.not.exist.or.has.been.disabled", locale));
                return result;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="userToken">
            sTime = new Date();
            LogUtils.info(logger, "Login getUserToken : start time= " + sTime);
            CmUserToken userToken = new CmUserTokenBusiness().getUserToken(userName, passWord, locale, shop.getShopType(), isN3P3);
            sTime = new Date();
            LogUtils.info(logger, "Login getUserToken : end time= " + sTime);
            if (userToken == null) {
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("login.vsa.account.access.denie", locale));
                return result;
            }
            if (Constants.ERROR_CODE_1.equals(userToken.getResultCode())) {
                mess = userToken.getResultMessage();
                if (mess != null) {
                    mess = mess.trim();
                    if (!mess.isEmpty()) {
                        result = new LoginOut(Constants.ERROR_CODE_1, mess);
                        return result;
                    }
                }
            }
            Token token;
            System.out.println("------------->userIsdn" + userIsdn);
            if (userIsdn.isEmpty()) {
                token = new TokenBussiness().logToken2(cmPosSession, imSession, userName, staff.getStaffId(), staff.getName(), userToken, locale, deviceId);
            } else {
                token = new TokenBussiness().logToken2(cmPosSession, imSession, userIsdn, staff.getStaffId(), staff.getName(), userToken, locale, deviceId);
            }
            if (token == null) {
                hasErr = true;
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("login.vsa.account.access.denie", locale));
                return result;
            }
            if (Constants.ERROR_CODE_1.equals(token.getResultCode())) {
                mess = token.getResultMessage();
                if (mess != null) {
                    mess = mess.trim();
                    if (!mess.isEmpty()) {
                        hasErr = true;
                        result = new LoginOut(Constants.ERROR_CODE_1, mess);
                        return result;
                    }
                }
            }
            //</editor-fold>

            commitTransactions(cmPosSession, imSession);

            result = new LoginOut(staff, shop, token.getToken(), userToken, Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            // get version IOS
            List<ApParam> lstApParam = ApParamDAO.findParamsByType(cmPosSession, "MBCCS_VERSION_UPDATE_IOS");
            if (lstApParam != null && lstApParam.size() > 0) {
                result.setVersionIos(lstApParam.get(0).getParamCode());
                result.setMessageIosEn(lstApParam.get(0).getParamName());
                result.setMessageIosKh(lstApParam.get(0).getParamValue());
            }
            lstApParam = ApParamDAO.findParamsByType(cmPosSession, "MBCCS_VERSION_UPDATE_ANDROID");
            if (lstApParam != null && lstApParam.size() > 0) {
                result.setVersionAndroid(lstApParam.get(0).getParamCode());
                result.setMessageAndroidEn(lstApParam.get(0).getParamName());
                result.setMessageAndroidKh(lstApParam.get(0).getParamValue());
            }
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession, imSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession, imSession);
        }
    }

    public LoginOut getToken(HibernateHelper hibernateHelper, String userName, String pass, StringBuilder merChantAccount) {
        LoginOut result = null;
        Session cmPosSession = null;
        Session merchantSession = null;
        Session imSession = null;
        boolean hasErr = false;
        String locale = "en_US";
        try {
            String passWord = new DecryptBusiness().getPassWordDecryptRSA(pass);
            if (passWord == null || passWord.isEmpty()) {
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("error.decrypt", locale));
                return result;
            }
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            merchantSession = hibernateHelper.getSession(Constants.SESSION_MERCHANT);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);

            List<Merchant> merchant = new MerchantDAO().getMerchantByAccount(merchantSession, userName);
            if (merchant == null || merchant.isEmpty()) {
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("login.vsa.account.incorect", locale));
                return result;
            }

            String passWordDB = CommonWebservice.decryptData(cmPosSession, merchant.get(0).getPassword());
            if (!passWord.equals(passWordDB)) {
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("login.vsa.account.incorect", locale));
                return result;
            }

            userName = userName.startsWith("855") ? userName.substring(3) : userName;
            userName = userName.startsWith("0") ? userName.substring(1) : userName;
            ImSupplier imSupplier = new ImSupplier();
            // check sim toolkit
            AccountAgent accountAgent = null;
            if (merchant.get(0).getIsdn() != null) {
                accountAgent = imSupplier.checkAccountAgentByIsdn(imSession, merchant.get(0).getIsdn());
                if (accountAgent == null) {
                    result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("error.accountAgent", locale));
                    return result;
                }
            }
            String merchantCode;
            Long id;
            if (accountAgent != null) {
                if (Constants.OWNER_TYPE_SHOP.equals(accountAgent.getOwnerType())) {
                    Shop shop = new ShopBussiness().findByShopCode(cmPosSession, accountAgent.getOwnerCode());
                    merchantCode = shop == null ? null : shop.getShopCode();
                    id = shop == null ? null : shop.getShopId();
                } else {
                    Staff staff = new StaffBussiness().findByCode(cmPosSession, accountAgent.getOwnerCode());
                    merchantCode = staff == null ? null : staff.getStaffCode();
                    id = staff == null ? null : staff.getStaffId();
                }
            } else {
                merchantCode = userName;
                id = 0l;
            }
            if (merchantCode == null) {
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("shop.does.not.exist.or.has.been.disabled", locale));
                return result;
            }
            merChantAccount.append(merchantCode);
            Token token = new TokenBussiness().logToken2(cmPosSession, imSession, merchantSession, userName, id, merchantCode, locale, null);

            if (token == null) {
                hasErr = true;
                result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.getKey("login.vsa.account.access.denie", locale));
                return result;
            }
            String mess = null;
            if (Constants.ERROR_CODE_1.equals(token.getResultCode())) {
                mess = token.getResultMessage();
                if (mess != null) {
                    mess = mess.trim();
                    if (!mess.isEmpty()) {
                        hasErr = true;
                        result = new LoginOut(Constants.ERROR_CODE_1, mess);
                        return result;
                    }
                }
            }
            //</editor-fold>
            commitTransactions(cmPosSession, imSession);

            result = new LoginOut(null, null, token.getToken(), null, Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
        } catch (Exception ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new LoginOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession, merchantSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession, merchantSession, imSession);
        }
        return result;
    }
}
