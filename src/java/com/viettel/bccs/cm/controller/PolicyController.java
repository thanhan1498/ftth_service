package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.PolicyBussiness;
import com.viettel.bccs.cm.model.PolicyFile;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.DateTimeUtils;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ValidateUtils;
import com.viettel.brcd.ws.model.output.FileOut;
import com.viettel.brcd.ws.model.output.LinkFileOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vietnn6
 */
public class PolicyController extends BaseController {

    public PolicyController() {
        logger = Logger.getLogger(PolicyController.class);
    }

    public FileOut getFileByLink(HibernateHelper hibernateHelper, String user, String pass, String server, String link, String locale) {
        FileOut result = new FileOut();
        Session salarySession = null;
        try {
            salarySession = hibernateHelper.getSession(Constants.SESSION_SALARY);
            List<String> data = new PolicyBussiness().getFilePolicy(user, pass, server, link);
            if (data == null || data.isEmpty()) {
                result = new FileOut(Constants.ERROR_CODE_1, "Data not found");
            } else {
                result.setData(data);
                result.setErrorCode(Constants.ERROR_CODE_0);
                result.setErrorDecription(LabelUtil.getKey("common.success", locale));
            }
            return result;
        } catch (Throwable ex) {
            result = new FileOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(salarySession);
        }
    }

    public LinkFileOut getLinkFilePolicy(HibernateHelper hibernateHelper, String fromDate, String toDate, String objectType, String serviceType, String formType, String locale) throws Exception {
        LinkFileOut result = new LinkFileOut();
        Session cmPre = null;
        try {

            //<editor-fold defaultstate="collapsed" desc="validate">
            if (objectType == null || "".equals(objectType)) {
                return new LinkFileOut(Constants.ERROR_CODE_1, "objectType is empty");
            }
            if (serviceType == null || "".equals(serviceType)) {
                return new LinkFileOut(Constants.ERROR_CODE_1, "serviceType is empty");
            }
            if (formType == null || "".equals(formType)) {
                return new LinkFileOut(Constants.ERROR_CODE_1, "formType is empty");
            }
            if (fromDate == null) {
                return new LinkFileOut(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid.fromDate", locale));
            }
            fromDate = fromDate.trim();
            if (fromDate.isEmpty()) {
                return new LinkFileOut(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid.fromDate", locale));
            }
            if (!ValidateUtils.isDateddMMyyyy(fromDate)) {
                return new LinkFileOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("fromDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy));
            }
            Date dFrom;
            try {
                dFrom = DateTimeUtils.toDateddMMyyyy(fromDate);
            } catch (Exception ex) {
                LogUtils.error(logger, ex.getMessage());
                return new LinkFileOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("fromDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy));
            }
            if (dFrom == null) {
                return new LinkFileOut(Constants.ERROR_CODE_1, LabelUtil.getKey("validate.required", locale, "fromDate"));
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="toDate">
            if (toDate == null) {
                return new LinkFileOut(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid.toDate", locale));
            }
            toDate = toDate.trim();
            if (toDate.isEmpty()) {
                return new LinkFileOut(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid.toDate", locale));
            }
            if (!ValidateUtils.isDateddMMyyyy(toDate)) {
                return new LinkFileOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("toDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy));
            }
            Date dTo;
            try {
                dTo = DateTimeUtils.toDateddMMyyyy(toDate);
            } catch (Exception ex) {
                LogUtils.error(logger, ex.getMessage());
                return new LinkFileOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("toDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy));
            }
            if (dTo == null) {
                return new LinkFileOut(Constants.ERROR_CODE_1, LabelUtil.getKey("validate.required", locale, "toDate"));
            }
            cmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            List<PolicyFile> data = new PolicyBussiness().getFilePolicy(cmPre, fromDate, toDate, Long.parseLong(objectType), Long.parseLong(serviceType), Long.parseLong(formType));
            if (data == null) {
                result = new LinkFileOut(Constants.ERROR_CODE_1, "Data not found");
            } else {
                result.setData(data);
                result.setErrorCode(Constants.ERROR_CODE_0);
                result.setErrorDecription(LabelUtil.getKey("common.success", locale));
            }
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new LinkFileOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPre);
        }
    }
}
