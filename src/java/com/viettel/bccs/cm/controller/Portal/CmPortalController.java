/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller.Portal;

import com.viettel.brcd.ws.model.output.ParamOut;

/**
 *
 * @author cuongdm
 */
public interface CmPortalController {

    ParamOut makeSaleTrans(String token, String encrypt, String signature);

    ParamOut getSubInfoSms(String token, String encrypt, String signature);
}
