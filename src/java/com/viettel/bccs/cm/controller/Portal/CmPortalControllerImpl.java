/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller.Portal;

import com.viettel.bccs.cm.controller.BaseController;
import com.viettel.bccs.cm.controller.CustomerController;
import com.viettel.bccs.cm.controller.TokenController;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.merchant.controller.MerchantController;
import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.brcd.ws.model.output.DataTopUpPinCode;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.eafs.util.SpringUtil;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author cuongdm
 */
public class CmPortalControllerImpl extends BaseController implements CmPortalController {

    private transient HibernateHelper hibernateHelper;

    public HibernateHelper getHibernateHelper() {
        if (this.hibernateHelper == null) {
            this.hibernateHelper = (HibernateHelper) SpringUtil.getBean("hibernateHelper");
            setHibernateHelper(this.hibernateHelper);
        }
        return hibernateHelper;
    }

    public void setHibernateHelper(HibernateHelper hibernateHelper) {
        this.hibernateHelper = hibernateHelper;
    }

    /**
     * @author cuongdm
     * @since 13/08/2019
     * @param token
     * @param encrypt
     * @param signature
     * @return
     */
    @Override
    public ParamOut makeSaleTrans(String token, String encrypt, String signature) {
        StringBuilder clearText = new StringBuilder();
        DataTopUpPinCode dataDeObj = new DataTopUpPinCode();
        WSRespone wsRespone = new TokenController().checkPermisssion(hibernateHelper, token, encrypt, signature, "makeSaleTrans", clearText);
        ParamOut result = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut();
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            try {
                dataDeObj = (DataTopUpPinCode) CommonWebservice.unmarshal(clearText.toString(), DataTopUpPinCode.class);
            } catch (Exception ex) {
            }
            result = new CustomerController().makeSaleTrans(hibernateHelper, dataDeObj);
        }
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("clearText", clearText.toString());
            new MerchantController().insertActionLog(hibernateHelper, wsRespone.getMerchantAccount(), dataDeObj.getRequestId(), "makeSaleTrans", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), null, result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription(), dataDeObj.getIp());
            Long transactionId = new MerchantController().insertTransactionLog(hibernateHelper, wsRespone.getMerchantAccount(), dataDeObj.getRequestId(), "makeSaleTrans", null, dataDeObj.getIsdn(), new Double(dataDeObj.getQuantity()), result.getSaleTransId() == null || result.getSaleTransId().isEmpty() ? null : Long.parseLong(result.getSaleTransId()), result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription(), dataDeObj.getIp());
            result.setTransactionId(transactionId == null ? null : transactionId.toString());
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut getSubInfoSms(String token, String encrypt, String signature) {
        StringBuilder clearText = new StringBuilder();
        DataTopUpPinCode dataSub = new DataTopUpPinCode();
        WSRespone wsRespone = new TokenController().checkPermisssion(hibernateHelper, token, encrypt, signature, "getSubInfoSms", clearText);
        ParamOut result = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut();
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            try {
                dataSub = (DataTopUpPinCode) CommonWebservice.unmarshal(clearText.toString(), DataTopUpPinCode.class);
            } catch (Exception ex) {
            }
            result = new CustomerController().getSubInfoSms(hibernateHelper, dataSub);
        }
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("clearText", clearText.toString());
            new MerchantController().insertActionLog(hibernateHelper, wsRespone.getMerchantAccount(), null, "getSubInfoSms", null, null, null, null, result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription(), null);
            Long transactionId = new MerchantController().insertTransactionLog(hibernateHelper, wsRespone.getMerchantAccount(), null, "getSubInfoSms", null, null, null, result.getSaleTransId() == null || result.getSaleTransId().isEmpty() ? null : Long.parseLong(result.getSaleTransId()), result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription(), null);
            result.setTransactionId(transactionId == null ? null : transactionId.toString());
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

}
