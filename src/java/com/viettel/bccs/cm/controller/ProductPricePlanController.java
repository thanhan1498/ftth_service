package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.ProductPricePlanBussiness;
import com.viettel.bccs.cm.model.ProductPricePlan;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.model.output.ProductPricePlanOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ProductPricePlanController extends BaseController {

    public ProductPricePlanController() {
        logger = Logger.getLogger(ProductPricePlanController.class);
    }

    public ProductPricePlanOut getListProductPP(HibernateHelper hibernateHelper, Long offerId, String locale) {
        ProductPricePlanOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<ProductPricePlan> productPricePlans = new ProductPricePlanBussiness().findByOfferId(cmPosSession, offerId);
            result = new ProductPricePlanOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), productPricePlans);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ProductPricePlanOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ProductPricePlanController.getListProductPP:result=" + LogUtils.toJson(result));
        }
    }
}
