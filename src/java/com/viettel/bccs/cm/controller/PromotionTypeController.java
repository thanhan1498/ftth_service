package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.PromotionTypeBussiness;
import com.viettel.bccs.cm.model.PromotionType;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.model.output.Promotion;
import com.viettel.brcd.ws.model.output.PromotionOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class PromotionTypeController extends BaseController {

    public PromotionTypeController() {
        logger = Logger.getLogger(PromotionTypeController.class);
    }

    public List<Promotion> getPromotions(Session cmPosSession, String telService) {
        List<Promotion> result = null;
        List<PromotionType> promotions = new PromotionTypeBussiness().findByService(cmPosSession, telService);
        if (promotions == null || promotions.isEmpty()) {
            return result;
        }
        result = new ArrayList<Promotion>();
        for (PromotionType promotion : promotions) {
            if (promotion != null) {
                result.add(new Promotion(promotion));
            }
        }
        return result;
    }

    public PromotionOut getPromotions(HibernateHelper hibernateHelper, String telService, String locale) {
        PromotionOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Promotion> promotions = getPromotions(cmPosSession, telService);
            result = new PromotionOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), promotions);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new PromotionOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "PromotionTypeController.getPromotions:result=" + LogUtils.toJson(result));
        }
    }
}
