package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.QuotaBussiness;
import com.viettel.bccs.cm.model.Quota;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.model.output.LimitValue;
import com.viettel.brcd.ws.model.output.LimitValueOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class QuotaController extends BaseController {

    public QuotaController() {
        logger = Logger.getLogger(QuotaController.class);
    }

    public List<LimitValue> getQuotas(Session cmPosSession, String serviceType) {
        List<LimitValue> result = null;
        List<Quota> quotas = new QuotaBussiness().findByService(cmPosSession, serviceType);
        if (quotas == null || quotas.isEmpty()) {
            return result;
        }
        result = new ArrayList<LimitValue>();
        for (Quota quota : quotas) {
            if (quota != null) {
                result.add(new LimitValue(quota.getQuotaValue()));
            }
        }
        return result;
    }

    public LimitValueOut getQuotas(HibernateHelper hibernateHelper, String serviceType, String locale) {
        LimitValueOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<LimitValue> quotas = getQuotas(cmPosSession, serviceType);
            result = new LimitValueOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), quotas);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new LimitValueOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "QuotaController.getQuotas:result=" + LogUtils.toJson(result));
        }
    }
}
