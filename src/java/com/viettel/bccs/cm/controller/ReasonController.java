package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.ReasonBussiness;
import com.viettel.bccs.cm.model.MapActiveInfo;
import com.viettel.bccs.cm.model.Mapping;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.ReasonDetail;
import com.viettel.bccs.cm.model.pre.ReasonPre;
import com.viettel.bccs.cm.supplier.ReasonSupplier;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import static com.viettel.brcd.dao.im.IMDAO.getDetailPriceFromSaleCode;
import com.viettel.brcd.ws.model.output.ReasonConnectOut;
import com.viettel.brcd.ws.model.output.ReasonOut;
import com.viettel.brcd.ws.model.output.ReasonPreConnectOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ReasonController extends BaseController {

    public ReasonController() {
        logger = Logger.getLogger(ReasonController.class);
    }

    public ReasonConnectOut getRegTypes(HibernateHelper hibernateHelper, Long channelTypeId, String province, Long numPort, String productCode, String serviceType, String locale, String infraType, String vasProduct) {
        ReasonConnectOut result = null;
        Session cmPosSession = null;
        Session imSession = null;
        try {
            List<ReasonDetail> regTypeDetails = new ArrayList<ReasonDetail>();
            LogUtils.info(logger, "ReasonController.getRegTypes:channelTypeId=" + channelTypeId + ";province=" + province + ";numPort=" + numPort + ";productCode=" + productCode + ";serviceType=" + serviceType);
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            ReasonBussiness reasonBussiness = new ReasonBussiness();
            if (vasProduct == null || vasProduct.isEmpty() || "?".equals(vasProduct)) {
                List<Reason> regTypes = reasonBussiness.getRegTypes(cmPosSession, numPort, productCode, serviceType, channelTypeId, province, infraType);
                if (regTypes == null) {
                    result = new ReasonConnectOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                    return result;
                }

                for (Reason re : regTypes) {
                    if (re != null) {
                        MapActiveInfo ma = reasonBussiness.findByReasonId(cmPosSession, re.getReasonId());
                        regTypeDetails.add(new ReasonDetail(re, ma));
                    }
                }
            } else {
                ReasonSupplier reasonSup = new ReasonSupplier();
                List<Reason> list = reasonSup.getMappingByOfferIdVas(cmPosSession, productCode, vasProduct);
                for (Reason re : list) {
                    if (re != null) {
                        List<Mapping> map = reasonSup.getMappingByOfferIdVas(cmPosSession, productCode, vasProduct, re.getReasonId());
                        if (map != null && !map.isEmpty()) {
                            Object[] saleFee = getDetailPriceFromSaleCode(imSession, map.get(0).getSaleServiceCode());
                            Double fee = 0d;
                            if (saleFee != null) {
                                if (saleFee[3] != null) {
                                    fee += Double.parseDouble(saleFee[3].toString());
                                }
                                if (saleFee[4] != null) {
                                    fee += Double.parseDouble(saleFee[4].toString());
                                }
                            }
                            re.setPayAdvAmount(re.getPayAdvAmount() + fee);
                            regTypeDetails.add(new ReasonDetail(re, null));
                        }
                    }
                }
            }
            result = new ReasonConnectOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), regTypeDetails);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ReasonConnectOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession, imSession);
            LogUtils.info(logger, "ReasonController.getRegTypes:result=" + LogUtils.toJson(result));
        }
    }

    public ReasonOut getSignContractReasons(HibernateHelper hibernateHelper, String telService, String locale) {
        ReasonOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Reason> reasons = new ReasonBussiness().find(cmPosSession, Constants.ACTION_CONTRACT_NEW, telService, null);
            result = new ReasonOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), reasons);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ReasonOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ReasonController.getSignContractReasons:result=" + LogUtils.toJson(result));
        }
    }

    public ReasonOut getCancelRequestReasons(HibernateHelper hibernateHelper, String locale) {
        ReasonOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Reason> reasons = new ReasonBussiness().find(cmPosSession, Constants.ACTION_CANCEL_REQUEST, null, null);
            result = new ReasonOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), reasons);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ReasonOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ReasonController.getCancelRequestReasons:result=" + LogUtils.toJson(result));
        }
    }

    public ReasonOut getCancelContractReasons(HibernateHelper hibernateHelper, String locale) {
        ReasonOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Reason> reasons = new ReasonBussiness().find(cmPosSession, Constants.ACTION_CONTRACT_CANCEL, null, null);
            result = new ReasonOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), reasons);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ReasonOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "ReasonController.getCancelContractReasons:result=" + LogUtils.toJson(result));
        }
    }

    public ReasonPreConnectOut getRegTypesPre(HibernateHelper hibernateHelper, String user, Long channelTypeId, String province, String productCode, String serviceType, String locale) {
        ReasonPreConnectOut result = null;
        Session cmPreSession = null;
        try {
            LogUtils.info(logger, "ReasonController.getRegTypesPre:channelTypeId=" + channelTypeId + ";province=" + province + ";productCode=" + productCode + ";serviceType=" + serviceType);
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            List<ReasonPre> regTypes = new ReasonBussiness().getRegTypesPre(cmPreSession, user, productCode, serviceType, channelTypeId, province);
            result = new ReasonPreConnectOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), regTypes);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ReasonPreConnectOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPreSession);
            LogUtils.info(logger, "ReasonController.getRegTypesPre:result=" + LogUtils.toJson(result));
        }
    }

    public ReasonOut getReasonPayAdvance(HibernateHelper hibernateHelper, String locale, Long contractId, String isdn) {
        ReasonOut result = null;
        Session cmPosSession = null;
        Session paymentSession = null;
        Session product = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            paymentSession = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            product = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            List<Reason> reasons = new ReasonBussiness().getReasonPayAdvance(cmPosSession, paymentSession, product, contractId, isdn);
            result = new ReasonOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), reasons);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ReasonOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            closeSessions(paymentSession);
            closeSessions(product);
            LogUtils.info(logger, "ReasonController.getCancelContractReasons:result=" + LogUtils.toJson(result));
        }
    }
}
