/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.brcd.ws.model.input.regOnline.RegOnlineInput;
import com.viettel.brcd.ws.model.output.ParamOut;

/**
 *
 * @author cuongdm
 */
public interface RegOnlineController {

    ParamOut regOnlineFtth(String token, String locale, RegOnlineInput input);

    ParamOut getInfoRegOnlineFtth(String token, String locale, Long requestId, String customerPhone, String customerName, String code, String source, String fromDate, String toDate, String province, String assignShop, String assignStaff, String staffCode, Long filterStatus, Long statusDeadline, Long statusReqCancel, int pageIndex, int pageSize, String requesterCancel);

    ParamOut getRegOnlineHis(String token, String locale, String staffCode, Long requestId);

    ParamOut updateProgressRegOnline(String token, String locale, Long requestId, String staffCode, String assignProvince, String assignShop, String assignStaff, Long status, String reason, String description, Long statusApprove);

    ParamOut getProgressRegOnline(String token, String locale, Long type);

    ParamOut getRequestIdFromIsdn(String token, String locale, String isdn);

    ParamOut getProvinceReqOnline(String token, String locale, String staffCode, Long filterStatus, Long statusDeadline);

    ParamOut getReqOnline(String token, String locale, String staffCode, Long filterStatus);

    ParamOut getReasonRegOnline(String token, String locale);

    ParamOut getListShowroom(String token, String locale, String staffCode, String province);

    ParamOut getListStaffShowroom(String token, String locale, String staffCode, String shopCode);
}
