/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.ApParamBussiness;
import com.viettel.bccs.cm.bussiness.StaffBussiness;
import com.viettel.bccs.cm.dao.RegOnlineDAO;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.RegOnline;
import com.viettel.bccs.cm.model.RegOnlineHis;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.bccs.cm.supplier.im.ImSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.regOnline.RegOnlineInput;
import com.viettel.brcd.ws.model.output.DashboardChart;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.ProvinceChart;
import com.viettel.brcd.ws.model.output.ProvinceChartDetail;
import com.viettel.brcd.ws.model.output.RegOnlineFtthInfo;
import com.viettel.brcd.ws.model.output.RegOnlineHisEx;
import com.viettel.brcd.ws.model.output.RegOnlineProgress;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.im.database.BO.AccountAgent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author cuongdm
 */
public class RegisterFTTHOnlineController extends BaseController {

    private static final String COLOR_NEW_REQUEST = "#135896";
    private static final String COLOR_ASSIGNED = "#00AC9C";
    private static final String COLOR_REGISTERED = "#B9C72D";
    private static final String COLOR_CONNECTED = "#0D7C3A";
    private static final String COLOR_CANCEL = "#F54522";
    private static final String COLOR_OVERTIME = "#F63C21";
    private static final String COLOR_ONTIME = "#00AC9C";
    private static final String PARAM_LIMIT_REQ = "REG_ONLINE_LIMIT";
    private static final String IP_LIMIT = "IP";
    private static final String ISDN_LIMIT = "ISDN";
    public static final Long STATUS_NEW_REQUEST = 0l;
    public static final Long STATUS_ASSIGNED = 1l;
    public static final Long STATUS_REGISTERED = 2l;
    public static final Long STATUS_CANCELED = 3l;
    public static final Long STATUS_CONNECTED = 4l;
    public static final Long STATUS_ONTIME = 10l;
    public static final Long STATUS_OVERTIME = 11l;
    public static final Long STATUS_CANCEL_WAITING = 0l;
    public static final Long STATUS_CANCEL_APPROVE = 1l;
    public static final Long STATUS_CANCEL_REJECT = 2l;
    private static final Long LEVEL_VTC = 1l;
    private static final Long LEVEL_BRANCH = 2l;
    private static final Long LEVEL_SHOWROOM = 3l;
    private static final String SOURCE_WEB_MF = "WMETFONE";
    private static final String SOURCE_MDEALER = "MDEALER";
    private static final String SOURCE_MBCCS = "MBCCS";
    private static final String STAFF_STATUS_NEW_REQUEST = "STAFF_STATUS_NEW_REQUEST";
    private static final String STAFF_STATUS_ASSIGNED = "STAFF_STATUS_ASSIGNED";
    private static final String STAFF_STATUS_REGISTERED = "STAFF_STATUS_REGISTERED";
    private static final String STAFF_STATUS_CONNECTED = "STAFF_STATUS_CONNECTED";
    private static final String STAFF_STATUS_CANCELED = "STAFF_STATUS_CANCELED";
    private static final String STAFF_STATUS_ONTIME = "STAFF_STATUS_ONTIME";
    private static final String STAFF_STATUS_OVERTIME = "STAFF_STATUS_OVERTIME";
    private static final String CUSTOMER_STATUS_NEW_REQUEST = "CUSTOMER_STATUS_NEW_REQUEST";
    private static final String CUSTOMER_STATUS_ASSIGNED = "CUSTOMER_STATUS_ASSIGNED";
    private static final String CUSTOMER_STATUS_REGISTERED = "CUSTOMER_STATUS_REGISTERED";
    private static final String CUSTOMER_STATUS_CONNECTED = "CUSTOMER_STATUS_CONNECTED";
    private static final String CUSTOMER_STATUS_CANCELED = "CUSTOMER_STATUS_CANCELED";
    private static final String CUSTOMER_STATUS_ONTIME = "CUSTOMER_STATUS_ONTIME";
    private static final String CUSTOMER_STATUS_OVERTIME = "CUSTOMER_STATUS_OVERTIME";

    public ParamOut regOnlineFtth(HibernateHelper hibernateHelper, String locale, RegOnlineInput input) {
        Session cmPosSession = null;
        Session imSession = null;
        Session cmPre = null;
        ParamOut param = new ParamOut();
        boolean isError = false;
        param.setErrorCode(Constants.ERROR_CODE_1);
        try {
            /*validate param*/
            if (input == null
                    || input.getAddress() == null || input.getAddress().trim().isEmpty()
                    || input.getCustomerName() == null || input.getCustomerName().trim().isEmpty()
                    || input.getCustomerPhone() == null || input.getCustomerPhone().trim().isEmpty()
                    || input.getPackageDetail() == null || input.getPackageDetail().trim().isEmpty()
                    || input.getProvince() == null || input.getProvince().trim().isEmpty()
                    || input.getSpeed() == null || input.getSpeed().trim().isEmpty()) {
                param.setErrorDecription(LabelUtil.getKey("e0112.im.response", locale));
                param.setMessageCode("e0112.im.response");
                return param;
            }
            String phoneNumber = input.getCustomerPhone();
            phoneNumber = phoneNumber.startsWith("0") ? phoneNumber.replaceFirst("0", "") : phoneNumber;
            phoneNumber = phoneNumber.startsWith("855") ? phoneNumber.replaceFirst("855", "") : phoneNumber;

            RegOnlineDAO regOnlineDAO = new RegOnlineDAO();
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            cmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            /*limit phone register per day*/
            List<ApParam> listParam = new ApParamBussiness().findByType(cmPosSession, PARAM_LIMIT_REQ);
            if (listParam != null && listParam.size() > 0) {
                List<RegOnline> list = null;
                for (ApParam config : listParam) {
                    if (IP_LIMIT.equals(config.getParamCode()) && input.getIp() != null && !input.getIp().isEmpty()) {
                        list = regOnlineDAO.getListRequestInDay(cmPosSession, "ip", input.getIp());
                    } else if (ISDN_LIMIT.equals(config.getParamCode())) {
                        list = regOnlineDAO.getListRequestInDay(cmPosSession, "customerPhone", phoneNumber);
                    }
                    if (list != null && list.size() > Long.parseLong(config.getParamValue())) {
                        param.setErrorDecription(LabelUtil.getKey("regOnline.limit", locale));
                        param.setMessageCode("regOnline.limit");
                        return param;
                    }
                }
            }

            /*validate introduction code*/
            if (((input.getType() != null && input.getType() > 0)
                    && (input.getIntroductionCode() == null || input.getIntroductionCode().isEmpty()))
                    || (input.getIntroductionCode() != null && !input.getIntroductionCode().isEmpty()
                    && (input.getType() == null || input.getType() == 0))) {
                param.setErrorDecription(LabelUtil.getKey("regOnline.invalid.code", locale));
                param.setMessageCode("regOnline.invalid.code");
                return param;
            }
            //daibq fix
            Staff staff = null;
            AccountAgent accountAgent = null;
            if (input.getType() != null) {
                if (Constants.OWNER_TYPE_STAFF.equals(input.getType())) {
                    staff = new StaffBussiness().findByCode(cmPosSession, input.getIntroductionCode().toUpperCase());
                    if (staff == null) {
                        param.setErrorDecription(LabelUtil.getKey("staff.does.not.exist.or.has.been.disabled", locale));
                        param.setMessageCode("staff.does.not.exist.or.has.been.disabled");
                        return param;
                    }
                } else if (Constants.OWNER_TYPE_SHOP.equals(input.getType())) {
                    String code = input.getIntroductionCode();
                    code = code.startsWith("0") ? code.replaceFirst("0", "") : code;
                    code = code.startsWith("855") ? code.replaceFirst("855", "") : code;
                    input.setIntroductionCode(code);
                    accountAgent = new ImSupplier().checkAccountAgentByIsdn(imSession, code);
                    if (accountAgent == null) {
                        param.setErrorDecription(LabelUtil.getKey("error.accountAgent", locale));
                        param.setMessageCode("error.accountAgent");
                        return param;
                    }
                    // lay staff assign
                    if (Constants.IS_AUTO_ASSIGN.equals(input.getIsAutoAssign())) {
                        staff = new StaffBussiness().findByCode(cmPosSession, accountAgent.getOwnerCode().toUpperCase());
                        if (staff == null) {
                            param.setErrorDecription(LabelUtil.getKey("staff.does.not.exist.or.has.been.disabled", locale));
                            param.setMessageCode("staff.does.not.exist.or.has.been.disabled");
                            return param;
                        }
                    }
                }
            }

            RegOnline regOnline = new RegOnline();
            Long id = new BaseSupplier().getSequence(cmPosSession, "REG_ONLINE_SEQ");
            regOnline.setId(id);
            regOnline.setCustomerName(input.getCustomerName());
            regOnline.setCustomerPhone(phoneNumber);
            regOnline.setCustomerMail(input.getCustomerMail());
            regOnline.setAddress(input.getAddress());
            regOnline.setIp(input.getIp());
            regOnline.setSpeed(input.getSpeed());
            regOnline.setPackageDetail(input.getPackageDetail());

            String province = input.getProvince();
            // Lay shop assign
            Shop assignShop = null;
            boolean checkAutoAssign = false;
            if (Constants.IS_AUTO_ASSIGN.equals(input.getIsAutoAssign()) && staff != null) {
                assignShop = new ShopDAO().findById(cmPosSession, staff.getShopId());
                if (assignShop != null && province.equals(assignShop.getProvince())) {
                    checkAutoAssign = true;
                    if (accountAgent != null && !Constants.OWNER_TYPE_STAFF.equals(accountAgent.getOwnerType())) {
                        checkAutoAssign = false;
                    }
                }
                if (checkAutoAssign) {
                    province = province.equals("PAI") ? "BAT" : province;
                    province = province.equals("KEB") ? "KAM" : province;
                    regOnline.setType(Constants.OWNER_TYPE_STAFF);
                    regOnline.setCode(input.getIntroductionCode() != null ? input.getIntroductionCode().toUpperCase() : input.getIntroductionCode());
                    regOnline.setProvince(province);
                    regOnline.setCreateDate(new Date());
                    regOnline.setStatus(STATUS_ASSIGNED);
                    regOnline.setAssignShop(assignShop.getShopCode());
                    regOnline.setAssignStaff(staff.getStaffCode());
                    regOnline.setUpdateUser("SYSTEM");
                    regOnline.setUpdateDate(new Date());
                    cmPosSession.save(regOnline);
                    cmPosSession.flush();
                    //luu log
                    regOnlineDAO.saveActionLog(cmPosSession, id, "STATUS", null, regOnline.getStatus().toString(), "SYSTEM", "Assign to Province");
                    regOnlineDAO.saveActionLog(cmPosSession, id, "PROVINCE", null, regOnline.getProvince(), "SYSTEM", "Assign to Province");

                    /*gui tin nhan cust*/
                    List<String> listParamSmsCust = new ArrayList<String>();
                    listParamSmsCust.add(input.getCustomerName());
                    listParamSmsCust.add(id.toString());
                    new com.viettel.bccs.cm.controller.payment.PaymentController().sendSMS(cmPre, phoneNumber, Constants.AP_PARAM_PARAM_TYPE_SMS, "REG_OLN_FTHH", listParamSmsCust);


                    /*gui tin nhan staff*/
                    List<String> listParamSmsStaff = new ArrayList<String>();
                    listParamSmsStaff.add(regOnline.getCustomerName());
                    listParamSmsStaff.add(regOnline.getCustomerPhone());
                    new com.viettel.bccs.cm.controller.payment.PaymentController().sendSMS(cmPre, staff.getIsdn() == null ? staff.getTel() : staff.getIsdn(), Constants.AP_PARAM_PARAM_TYPE_SMS, "ASSIGN_REG_ONLINE_SMS", listParamSmsStaff);

                    param.setErrorCode(Constants.ERROR_CODE_0);
                    param.setErrorDecription(LabelUtil.getKey("common.success", locale));
                    param.setMessageCode("common.success");
                    param.setTransactionId(id.toString());
                    return param;
                }

            }

            Shop shop = new ShopDAO().findByCode(cmPosSession, province);
            if (shop == null) {
                param.setErrorDecription(LabelUtil.getKey("shop.does.not.exist", locale));
                param.setMessageCode("shop.does.not.exist");
                return param;
            } else {
                province = province.equals("PAI") ? "BAT" : province;
                province = province.equals("KEB") ? "KAM" : province;
            }
            regOnline.setType(input.getType() > 0 ? input.getType() : null);
            regOnline.setCode(input.getIntroductionCode() != null ? input.getIntroductionCode().toUpperCase() : input.getIntroductionCode());
            regOnline.setProvince(province);
            regOnline.setCreateDate(new Date());
            regOnline.setUpdateDate(new Date());
            regOnline.setStatus(STATUS_NEW_REQUEST);
            cmPosSession.save(regOnline);
            cmPosSession.flush();

            /*gui tin nhan*/
            List<String> listParamSms = new ArrayList<String>();
            listParamSms.add(input.getCustomerName());
            listParamSms.add(id.toString());
            new com.viettel.bccs.cm.controller.payment.PaymentController().sendSMS(cmPre, phoneNumber, Constants.AP_PARAM_PARAM_TYPE_SMS, "REG_OLN_FTHH", listParamSms);
            param.setErrorCode(Constants.ERROR_CODE_0);
            param.setErrorDecription(LabelUtil.getKey("common.success", locale));
            param.setMessageCode("common.success");
            param.setTransactionId(id.toString());
        } catch (Exception ex) {
            param.setErrorDecription(ex.getMessage());
            param.setMessageCode("error");
            isError = true;
            ex.printStackTrace();
        } finally {
            if (cmPosSession != null) {
                if (!isError) {
                    commitTransactions(cmPosSession, cmPre);
                } else {
                    rollBackTransactions(cmPosSession, cmPre);
                }
                closeSessions(cmPosSession, imSession, cmPre);
            }
        }
        return param;
    }

    public ParamOut getInfoRegOnlineFtth(HibernateHelper hibernateHelper, String locale, Long requestId, String customerPhone, String customerName,
            String code, String source, String fromDate, String toDate, String province, String assignShop, String assignStaff, String staffCode,
            Long filterStatus, Long statusDeadline, Long statusReqCancel, int pageIndex, int pageSize, String requesterCancel) {
        Session cmPosSession = null;
        ParamOut param = new ParamOut();
        param.setErrorCode(Constants.ERROR_CODE_1);
        try {
            List listParam = new ArrayList();
            String body = "  FROM   reg_online WHERE 1 = 1 \n";
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            Shop shop = null;
            Staff staff = null;
            /*validate nguon goi service*/
            if (SOURCE_MBCCS.equals(source)) {
                staff = new StaffBussiness().findByCode(cmPosSession, staffCode == null ? "" : staffCode.toUpperCase());
                if (staff == null) {
                    param.setErrorDecription(LabelUtil.getKey("staff.does.not.exist.or.has.been.disabled", locale));
                    param.setMessageCode("staff.does.not.exist.or.has.been.disabled");
                    return param;
                }
                shop = new ShopDAO().findById(cmPosSession, staff.getShopId());
                if (LEVEL_BRANCH.toString().equals(shop.getShopType())) {
                    body += " and province = ? \n";
                    listParam.add(shop.getProvince());
                }
                if (LEVEL_SHOWROOM.toString().equals(shop.getShopType())) {
                    body += " and assign_staff = ? \n";
                    listParam.add(staffCode);
                }
            } else if (SOURCE_MDEALER.equals(source)) {
                if (source == null || source.isEmpty()) {
                    param.setErrorDecription(LabelUtil.getKey("e0112.im.response", locale));
                    param.setMessageCode("e0112.im.response");
                    return param;
                }
                body += " and code = ? \n";
                listParam.add(code);
            } else if (SOURCE_WEB_MF.equals(source)) {
                filterStatus = null;
                if (requestId == null || requestId == 0) {
                    param.setErrorDecription(LabelUtil.getKey("e0112.im.response", locale));
                    param.setMessageCode("e0112.im.response");
                    return param;
                }
                body += " and id = ? \n";
                listParam.add(requestId);
            } else {
                param.setErrorDecription(LabelUtil.getKey("e0112.im.response", locale));
                param.setMessageCode("e0112.im.response");
                return param;
            }

            String selectCount = "SELECT   count(1)\n";
            String selectData = "SELECT   id,\n"
                    + "         customer_name customerName,\n"
                    + "         customer_phone customerPhone,\n"
                    + "         customer_mail customerMail,\n"
                    + "         address,\n"
                    + "         speed,\n"
                    + "         package_detail packageDetail,\n"
                    + "         to_char(create_date, 'dd/mm/yyyy hh24:mi:ss') createDateStr,\n"
                    + "         province,\n"
                    + "         assign_shop assignShop,\n"
                    + "         assign_staff assignStaff,\n"
                    + "         status,\n"
                    + "         reason,\n"
                    + "         description,\n"
                    + "         account,\n"
                    + "         code,\n"
                    + "         case when status = 0 and (sysdate - update_date) *24 < 8 then '" + COLOR_NEW_REQUEST + "'\n"
                    + "         when status = 0 and (sysdate - update_date) *24 >= 8 and (sysdate - update_date) *24 < 24 then '" + COLOR_REGISTERED + "'\n"
                    + "         when status = 0 and (sysdate - update_date) *24 >= 24   then '" + COLOR_CANCEL + "'\n"
                    + "         when status = 1 and (sysdate - update_date) *24 < 24 then '" + COLOR_NEW_REQUEST + "'\n"
                    + "         when status = 1 and (sysdate - update_date) *24 >= 24 and (sysdate - update_date) *24 < 48 then '" + COLOR_REGISTERED + "'\n"
                    + "         when status = 1 and (sysdate - update_date) *24 >= 48 then '" + COLOR_CANCEL + "'\n"
                    + "         when status = 2 and (sysdate - update_date) *24 <= 48 then '" + COLOR_REGISTERED + "'\n"
                    + "         when status = 2 and (sysdate - update_date) *24 > 48 then '" + COLOR_CANCEL + "'\n"
                    + "         else '' end color,\n"
                    + "         case when 'MBCCS' = '" + source + "' then decode(status, 0, 'New Request', 1, 'Assigned', 2, 'Registered', 3 ,'Canceled', 'Connected')\n"
                    + "         else decode(status, 0, 'New', 1, 'Checking', 2, 'Contract signed', 3 ,'Canceled', 'Finish')\n"
                    + "         end statusDes,\n"
                    + "         case \n"
                    + "         when status =0 and (sysdate - update_date) *24 > 8 then to_char(round((sysdate -update_date) *24 - 8)) || ' hours'\n"
                    + "         when status =1 and (sysdate - update_date) > 1 then to_char(round(sysdate -update_date)*24 - 24) || ' hours'\n"
                    + "         when status =2 and (sysdate - update_date) > 2 then to_char(round(sysdate -update_date)*24 - 48) || ' hours'\n"
                    + "         else '' end overtime\n";
            if (SOURCE_MBCCS.equals(source) && shop != null) {
                selectData += "     ,case when '1' = '" + shop.getShopType() + "' and 5=" + staff.getType().toString() + " and (status =0 or status = 3) then 1 else null end showAssignProvince,\n"
                        + "     case when ('1' = '" + shop.getShopType() + "' or '2' = '" + shop.getShopType() + "') and 5=" + staff.getType().toString() + " and (status =0 or status = 1) and nvl(sub_Status,1) != 0 then 1 else null end showAssignStaff,\n"
                        + "     case when (status = 1 and nvl(sub_Status,1) != 0) then 1 else null end showCancel,\n"
                        + "     case when (status = 1 and nvl(sub_Status,1) != 0) then 1 else null end showConnect, \n"
                        + "     case when ('1' = '" + shop.getShopType() + "' or '2' = '" + shop.getShopType() + "') and 5=" + staff.getType().toString() + " and (status = 1 and sub_Status = 0) then 1 else null end showApprove, \n"
                        + "     case when ('1' = '" + shop.getShopType() + "' or '2' = '" + shop.getShopType() + "') and 5=" + staff.getType().toString() + " and (status = 1 and sub_Status = 0) then 1 else null end showReject, \n"
                        + "     case when (status = 1 and sub_Status = 0) then update_user else null end requesterCancel \n";
            } else {
                selectData += ", null showAssignProvince, null showAssignStaff, null showCancel, null showConnect, null showApprove, null showReject , null requesterCancel";
            }

            if (requestId != null && requestId > 0) {
                body += " and id = ? \n";
                listParam.add(requestId);
            }
            if (customerPhone != null && !customerPhone.trim().isEmpty()) {
                customerPhone = customerPhone.startsWith("0") ? customerPhone.replaceFirst("0", "") : customerPhone;
                customerPhone = customerPhone.startsWith("855") ? customerPhone.replaceFirst("855", "") : customerPhone;
                body += " and customer_phone = ? \n";
                listParam.add(customerPhone);
            }
            if (customerName != null && !customerName.trim().isEmpty()) {
                body += " and lower(customer_name) like ? \n";
                listParam.add("%" + customerName.toLowerCase() + "%");
            }
            if (code != null && !code.trim().isEmpty()) {
                body += " and code= ? \n";
                listParam.add(code.toUpperCase());
            }
            if (fromDate != null && !fromDate.trim().isEmpty() && statusReqCancel == null && (requesterCancel == null || requesterCancel.isEmpty())) {
                body += " AND create_date >= TO_DATE (?, 'dd/mm/yyyy') \n";
                listParam.add(fromDate);
            }
            if (fromDate != null && !fromDate.trim().isEmpty() && (statusReqCancel != null || (requesterCancel != null && !requesterCancel.isEmpty()))) {
                body += " AND REQ_CANCEL_DATE >= TO_DATE (?, 'dd/mm/yyyy') \n";
                listParam.add(fromDate);
            }
            if (toDate != null && !toDate.trim().isEmpty() && statusReqCancel == null && (requesterCancel == null || requesterCancel.isEmpty())) {
                body += " AND create_date < TO_DATE (?, 'dd/mm/yyyy') + 1 \n";
                listParam.add(toDate);
            }
            if (fromDate != null && !fromDate.trim().isEmpty() && (statusReqCancel != null || (requesterCancel != null && !requesterCancel.isEmpty()))) {
                body += " AND REQ_CANCEL_DATE < TO_DATE (?, 'dd/mm/yyyy') + 1 \n";
                listParam.add(toDate);
            }
            if (province != null && !province.trim().isEmpty()) {
                body += " and province= ? \n";
                listParam.add(province);
            }
            if (assignShop != null && !assignShop.trim().isEmpty()) {
                body += " and assign_shop= ? \n";
                listParam.add(assignShop);
            }
            if (assignStaff != null && !assignStaff.trim().isEmpty()) {
                body += " and assign_staff= ? \n";
                listParam.add(assignStaff.toUpperCase());
            }
            if (filterStatus != null && filterStatus >= 0 && filterStatus < 10) {
                body += " and status= ? \n";
                listParam.add(filterStatus);
            }
            if ((STATUS_ONTIME.equals(filterStatus) || STATUS_ONTIME.equals(statusDeadline)) && !STATUS_CANCELED.equals(filterStatus) && !STATUS_CONNECTED.equals(filterStatus)) {
                body += " and ((status =0 and (sysdate - update_date) *24 <= 8) or (status =1 and (sysdate - update_date) <= 1) or (status =2 and (sysdate - update_date) <= 2)) \n";
            }
            if ((STATUS_OVERTIME.equals(filterStatus) || STATUS_OVERTIME.equals(statusDeadline)) && !STATUS_CANCELED.equals(filterStatus) && !STATUS_CONNECTED.equals(filterStatus)) {
                body += " and ((status =0 and (sysdate - update_date) *24 > 8) or (status =1 and (sysdate - update_date) > 1) or (status =2 and (sysdate - update_date) > 2)) \n";
            }
            if (requesterCancel != null && !requesterCancel.isEmpty()) {
                body += " and update_user= ? and status = 1 and sub_status is not null ";
                listParam.add(requesterCancel);
            }
            if (statusReqCancel != null) {
                body += " and sub_status= ? ";
                listParam.add(statusReqCancel);
            }
            Query query = cmPosSession.createSQLQuery(selectCount + body);
            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
            }
            List list = query.list();
            Long totalRecord = 0l;
            if (list != null && !list.isEmpty()) {
                totalRecord = Long.parseLong(list.get(0).toString());
            }
            if (totalRecord > 0) {
                query = cmPosSession.createSQLQuery(selectData + body + "order by create_date desc ")
                        .addScalar("id", Hibernate.LONG)
                        .addScalar("customerName", Hibernate.STRING)
                        .addScalar("customerPhone", Hibernate.STRING)
                        .addScalar("customerMail", Hibernate.STRING)
                        .addScalar("address", Hibernate.STRING)
                        .addScalar("speed", Hibernate.STRING)
                        .addScalar("packageDetail", Hibernate.STRING)
                        .addScalar("createDateStr", Hibernate.STRING)
                        .addScalar("province", Hibernate.STRING)
                        .addScalar("assignShop", Hibernate.STRING)
                        .addScalar("assignStaff", Hibernate.STRING)
                        .addScalar("status", Hibernate.LONG)
                        .addScalar("reason", Hibernate.STRING)
                        .addScalar("description", Hibernate.STRING)
                        .addScalar("account", Hibernate.STRING)
                        .addScalar("code", Hibernate.STRING)
                        .addScalar("color", Hibernate.STRING)
                        .addScalar("statusDes", Hibernate.STRING)
                        .addScalar("overtime", Hibernate.STRING)
                        .addScalar("showAssignProvince", Hibernate.LONG)
                        .addScalar("showAssignStaff", Hibernate.LONG)
                        .addScalar("showCancel", Hibernate.LONG)
                        .addScalar("showConnect", Hibernate.LONG)
                        .addScalar("showApprove", Hibernate.LONG)
                        .addScalar("showReject", Hibernate.LONG)
                        .addScalar("requesterCancel", Hibernate.STRING)
                        .setResultTransformer(Transformers.aliasToBean(RegOnlineFtthInfo.class));
                for (int i = 0; i < listParam.size(); i++) {
                    query.setParameter(i, listParam.get(i));
                }
                if (pageIndex > 0 && pageSize > 0) {
                    query.setFirstResult(pageIndex * pageSize);
                    query.setMaxResults(pageSize);
                }
                List<RegOnlineFtthInfo> listData = query.list();
                if (listData == null || listData.isEmpty()) {
                    param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                    param.setMessageCode("common.find.nothing");
                    param.setTotalRequest(0l);
                    param.setTotalPage(0l);
                } else {
                    param.setErrorCode(Constants.ERROR_CODE_0);
                    param.setErrorDecription(LabelUtil.getKey("common.success", locale));
                    param.setMessageCode("common.success");
                    if (pageSize > 0) {
                        param.setTotalRequest(totalRecord);
                        param.setTotalPage(totalRecord / pageSize + (totalRecord % pageSize > 0 ? 1 : 0));
                    }
                    param.setRegOnlineFtthInfo(listData);
                }
            } else {
                param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                param.setMessageCode("common.find.nothing");
                param.setTotalRequest(0l);
                param.setTotalPage(0l);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            closeSessions(cmPosSession);

        }
        return param;
    }

    public ParamOut getRegOnlineHis(HibernateHelper hibernateHelper, String locale, String staffCode, Long requestId) {
        Session cmPosSession = null;
        ParamOut param = new ParamOut();
        param.setErrorCode(Constants.ERROR_CODE_1);
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            RegOnlineDAO regOnlineDAO = new RegOnlineDAO();
            RegOnline request = regOnlineDAO.getRegOnlineById(cmPosSession, requestId);
            if (request == null) {
                param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                param.setMessageCode("common.find.nothing");
            } else {
                Staff staff = new StaffBussiness().findByCode(cmPosSession, staffCode == null ? "" : staffCode.toUpperCase());
                if (staff == null) {
                    param.setErrorDecription(LabelUtil.getKey("staff.does.not.exist.or.has.been.disabled", locale));
                    param.setMessageCode("staff.does.not.exist.or.has.been.disabled");
                    return param;
                }
                Shop shop = new ShopDAO().findById(cmPosSession, staff.getShopId());
                if (LEVEL_BRANCH.toString().equals(shop.getShopType())) {
                    if (!shop.getProvince().equals(request.getProvince())) {
                        param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                        param.setMessageCode("common.find.nothing");
                        return param;
                    }
                } else if (LEVEL_SHOWROOM.toString().equals(shop.getShopType())) {
                    if (!shop.getShopCode().equals(request.getAssignShop())) {
                        param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                        param.setMessageCode("common.find.nothing");
                        return param;
                    }
                }
            }
            List<RegOnlineHis> list = regOnlineDAO.getRegOnlineHis(cmPosSession, requestId);
            List<RegOnlineHisEx> listData = new ArrayList<RegOnlineHisEx>();
            param.setErrorCode(Constants.ERROR_CODE_0);
            param.setErrorDecription(LabelUtil.getKey("common.success", locale));
            param.setRegOnlHis(listData);
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            if (list != null && !list.isEmpty()) {
                for (RegOnlineHis temp : list) {
                    RegOnlineHisEx t = new RegOnlineHisEx();
                    t.setColumn(temp.getColumn());
                    t.setDate(format.format(temp.getUpdateDate()));
                    t.setOldValue(temp.getOldValue());
                    t.setNewValue(temp.getNewValue());
                    t.setUser(temp.getUpdateUser());
                    listData.add(t);
                }
            } else {
                param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                param.setMessageCode("common.find.nothing");
                param.setTotalRequest(0l);
                param.setTotalPage(0l);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cmPosSession != null) {
                closeSessions(cmPosSession);
            }
        }
        return param;
    }

    public ParamOut updateProgressRegOnline(HibernateHelper hibernateHelper, String locale, Long requestId, String staffCode,
            String assignProvince, String assignShop, String assignStaff, Long status, String reason, String description, Long statusApprove) {
        Session cmPosSession = null;
        Session cmPre = null;
        ParamOut param = new ParamOut();
        boolean isError = false;
        param.setErrorCode(Constants.ERROR_CODE_1);
        try {
            Shop shop = null;
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            RegOnlineDAO regOnlineDAO = new RegOnlineDAO();
            RegOnline request = regOnlineDAO.getRegOnlineById(cmPosSession, requestId);
            if (request == null) {
                param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                param.setMessageCode("common.find.nothing");
            } else {
                Staff staff = new StaffBussiness().findByCode(cmPosSession, staffCode == null ? "" : staffCode.toUpperCase());
                if (staff == null) {
                    param.setErrorDecription(LabelUtil.getKey("staff.does.not.exist.or.has.been.disabled", locale));
                    param.setMessageCode("staff.does.not.exist.or.has.been.disabled");
                    return param;
                }
                staffCode = staffCode.toUpperCase();
                shop = new ShopDAO().findById(cmPosSession, staff.getShopId());
                if (LEVEL_BRANCH.toString().equals(shop.getShopType())) {
                    if (!shop.getProvince().equals(request.getProvince())) {
                        param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                        param.setMessageCode("common.find.nothing");
                        return param;
                    }
                } else if (LEVEL_SHOWROOM.toString().equals(shop.getShopType())) {
                    if (statusApprove != null) {
                        param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                        param.setMessageCode("common.find.nothing");
                        return param;
                    }
                    if (!shop.getShopCode().equals(request.getAssignShop())) {
                        param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                        param.setMessageCode("common.find.nothing");
                        return param;
                    }
                }
            }
            if ((status == null || status < 0) && (statusApprove == null || statusApprove < 0)) {
                param.setErrorDecription(LabelUtil.getKey("e0112.im.response", locale));
                param.setMessageCode("e0112.im.response");
                return param;
            }
            if (STATUS_NEW_REQUEST.equals(status)) {
                if ((!request.getStatus().equals(STATUS_CANCELED) && !request.getStatus().equals(STATUS_NEW_REQUEST)) || !LEVEL_VTC.toString().equals(shop.getShopType())) {
                    param.setErrorDecription(LabelUtil.getKey("regOnline.invalid.update", locale));
                    param.setMessageCode("regOnline.invalid.update");
                    return param;
                }
                if (assignProvince == null || assignProvince.isEmpty()) {
                    param.setErrorDecription(LabelUtil.getKey("e0112.im.response", locale));
                    param.setMessageCode("e0112.im.response");
                    return param;
                }
                regOnlineDAO.saveActionLog(cmPosSession, requestId, "STATUS", request.getStatus().toString(), status.toString(), staffCode, "Assign to Province");
                regOnlineDAO.saveActionLog(cmPosSession, requestId, "PROVINCE", request.getProvince(), assignProvince, staffCode, "Assign to Province");

                request.setStatus(STATUS_NEW_REQUEST);
                request.setAssignShop(null);
                request.setAssignStaff(null);
                request.setProvince(assignProvince);
                request.setUpdateUser(staffCode);
                request.setUpdateDate(new Date());
                cmPosSession.save(request);
                cmPosSession.flush();
            }
            if (STATUS_ASSIGNED.equals(status)) {
                if (assignShop == null || assignShop.isEmpty() || assignStaff == null || assignStaff.isEmpty()) {
                    param.setErrorDecription(LabelUtil.getKey("e0112.im.response", locale));
                    param.setMessageCode("e0112.im.response");
                    return param;
                }
                if (request.getStatus().equals(STATUS_CONNECTED)
                        || request.getStatus().equals(STATUS_CANCELED)
                        || request.getStatus().equals(STATUS_REGISTERED)
                        || LEVEL_SHOWROOM.toString().equals(shop.getShopType())
                        || (request.getAssignStaff() != null && request.getAssignStaff().equals(assignStaff))) {
                    param.setErrorDecription(LabelUtil.getKey("regOnline.invalid.update", locale));
                    param.setMessageCode("regOnline.invalid.update");
                    return param;
                }
                regOnlineDAO.saveActionLog(cmPosSession, requestId, "STATUS", request.getStatus().toString(), status.toString(), staffCode, "Assign to staff");
                regOnlineDAO.saveActionLog(cmPosSession, requestId, "ASSIGN_STAFF", request.getAssignStaff(), assignStaff, staffCode, "Assign to staff");

                request.setStatus(STATUS_ASSIGNED);
                request.setAssignShop(assignShop);
                request.setAssignStaff(assignStaff);
                request.setUpdateUser(staffCode);
                request.setUpdateDate(new Date());
                cmPosSession.save(request);
                cmPosSession.flush();

                /*gui tin nhan*/
                List<String> listParamSms = new ArrayList<String>();
                Staff staff = new StaffBussiness().findByCode(cmPosSession, assignStaff);
                listParamSms.add(request.getCustomerName());
                listParamSms.add(request.getCustomerPhone());
                new com.viettel.bccs.cm.controller.payment.PaymentController().sendSMS(cmPre, staff.getIsdn() == null ? staff.getTel() : staff.getIsdn(), Constants.AP_PARAM_PARAM_TYPE_SMS, "ASSIGN_REG_ONLINE_SMS", listParamSms);

            }
            if (STATUS_CANCELED.equals(status)) {
                if (!STATUS_ASSIGNED.equals(request.getStatus())
                        || (!staffCode.equals(request.getAssignStaff()) && LEVEL_SHOWROOM.toString().equals(shop.getShopType()))) {
                    param.setErrorDecription(LabelUtil.getKey("regOnline.invalid.update", locale));
                    param.setMessageCode("regOnline.invalid.update");
                    return param;
                }
                if (reason == null || reason.isEmpty()) {
                    param.setErrorDecription(LabelUtil.getKey("e0112.im.response", locale));
                    param.setMessageCode("e0112.im.response");
                    return param;
                }
                if (STATUS_CANCEL_WAITING.equals(request.getSubStatus())) {
                    param.setErrorDecription(LabelUtil.getKey("regOnline.request.cancel.invalid", locale));
                    param.setMessageCode("e0112.im.response");
                    return param;
                }
                regOnlineDAO.saveActionLog(cmPosSession, requestId, "SUB_STATUS", null, STATUS_CANCEL_WAITING.toString(), staffCode, "Request to cancel request");
                regOnlineDAO.saveActionLog(cmPosSession, requestId, "REASON", request.getReason(), reason, staffCode, "Request to cancel request");
                request.setSubStatus(STATUS_CANCEL_WAITING);
                request.setReqCancelDate(new Date());
                request.setUpdateUser(staffCode);
                request.setUpdateDate(new Date());
                request.setReason(reason);
                request.setDescription(description);
                cmPosSession.save(request);
                cmPosSession.flush();
            }
            if (statusApprove != null && STATUS_ASSIGNED.equals(request.getStatus())) {
                if (STATUS_CANCEL_APPROVE.equals(statusApprove)) {
                    regOnlineDAO.saveActionLog(cmPosSession, requestId, "SUB_STATUS", request.getSubStatus().toString(), statusApprove.toString(), staffCode, "Approve to cancel request");
                    regOnlineDAO.saveActionLog(cmPosSession, requestId, "STATUS", request.getStatus().toString(), STATUS_CANCELED.toString(), request.getUpdateUser(), "Cancel request");
                    request.setSubStatus(statusApprove);
                    request.setStatus(STATUS_CANCELED);
                    request.setApproveDate(new Date());
                    request.setApproveUser(staffCode);
                    cmPosSession.save(request);
                    cmPosSession.flush();
                } else if (STATUS_CANCEL_REJECT.equals(statusApprove)) {
                    regOnlineDAO.saveActionLog(cmPosSession, requestId, "SUB_STATUS", request.getSubStatus().toString(), statusApprove.toString(), staffCode, "Reject to cancel request");
                    request.setSubStatus(statusApprove);
                    cmPosSession.save(request);
                    cmPosSession.flush();
                }
            }
            param.setErrorCode(Constants.ERROR_CODE_0);
            param.setErrorDecription(LabelUtil.getKey("common.success", locale));
        } catch (Exception ex) {
            isError = true;
            ex.printStackTrace();
        } finally {
            if (cmPosSession != null) {
                if (!isError) {
                    commitTransactions(cmPosSession, cmPre);
                } else {
                    rollBackTransactions(cmPosSession, cmPre);
                }
                closeSessions(cmPosSession, cmPre);
            }
        }
        return param;
    }

    public ParamOut getProgressRegOnline(HibernateHelper hibernateHelper, String locale, Long type) {
        ParamOut param = new ParamOut();
        boolean isError = false;
        try {
            param.setErrorCode(Constants.ERROR_CODE_0);
            param.setErrorDecription(LabelUtil.getKey("common.success", locale));
            param.setMessageCode("common.success");
            List<RegOnlineProgress> list = new ArrayList<RegOnlineProgress>();
            RegOnlineProgress temp = new RegOnlineProgress();
            temp.setDescription(LabelUtil.getKey("STAFF_STATUS_ALL", locale));
            temp.setStatus(-1l);
            list.add(temp);
            if (type == null || type == 0) {
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_NEW_REQUEST, locale));
                temp.setStatus(STATUS_NEW_REQUEST);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_ASSIGNED, locale));
                temp.setStatus(STATUS_ASSIGNED);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_REGISTERED, locale));
                temp.setStatus(STATUS_REGISTERED);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_CANCELED, locale));
                temp.setStatus(STATUS_CANCELED);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_CONNECTED, locale));
                temp.setStatus(STATUS_CONNECTED);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_ONTIME, locale));
                temp.setStatus(STATUS_ONTIME);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_OVERTIME, locale));
                temp.setStatus(STATUS_OVERTIME);
                list.add(temp);
            } else if (type == 1) {
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_NEW_REQUEST, locale));
                temp.setStatus(STATUS_NEW_REQUEST);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_ASSIGNED, locale));
                temp.setStatus(STATUS_ASSIGNED);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_REGISTERED, locale));
                temp.setStatus(STATUS_REGISTERED);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_CANCELED, locale));
                temp.setStatus(STATUS_CANCELED);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_CONNECTED, locale));
                temp.setStatus(STATUS_CONNECTED);
                list.add(temp);
            } else if (type == 2) {
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_CANCELED, locale));
                temp.setStatus(STATUS_CANCELED);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_CONNECTED, locale));
                temp.setStatus(STATUS_CONNECTED);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_ONTIME, locale));
                temp.setStatus(STATUS_ONTIME);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(STAFF_STATUS_OVERTIME, locale));
                temp.setStatus(STATUS_OVERTIME);
                list.add(temp);
            } else if (type == 3) {
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(CUSTOMER_STATUS_NEW_REQUEST, locale));
                temp.setStatus(STATUS_NEW_REQUEST);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(CUSTOMER_STATUS_ASSIGNED, locale));
                temp.setStatus(STATUS_ASSIGNED);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(CUSTOMER_STATUS_REGISTERED, locale));
                temp.setStatus(STATUS_REGISTERED);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(CUSTOMER_STATUS_CANCELED, locale));
                temp.setStatus(STATUS_CANCELED);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey(CUSTOMER_STATUS_CONNECTED, locale));
                temp.setStatus(STATUS_CONNECTED);
                list.add(temp);
            } else if (type == 4) {
                list.clear();
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey("STATUS_CANCEL_WAITING", locale));
                temp.setStatus(STATUS_CANCEL_WAITING);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey("STATUS_CANCEL_APPROVE", locale));
                temp.setStatus(STATUS_CANCEL_APPROVE);
                list.add(temp);
                temp = new RegOnlineProgress();
                temp.setDescription(LabelUtil.getKey("STATUS_CANCEL_REJECT", locale));
                temp.setStatus(STATUS_CANCEL_REJECT);
                list.add(temp);
            }
            param.setListProgress(list);

        } catch (Exception ex) {
            isError = true;
            ex.printStackTrace();
        }
        return param;
    }

    public ParamOut getRequestIdFromIsdn(HibernateHelper hibernateHelper, String locale, String isdn) {
        Session cmPosSession = null;
        Session cmPre = null;
        ParamOut param = new ParamOut();
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            isdn = isdn.startsWith("0") ? isdn.replaceFirst("0", "") : isdn;
            isdn = isdn.startsWith("855") ? isdn.replaceFirst("855", "") : isdn;
            List<RegOnline> req = new RegOnlineDAO().getRegOnlineByIsdn(cmPosSession, isdn);
            if (req == null || req.isEmpty()) {
                param.setErrorCode(Constants.ERROR_CODE_1);
                param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                param.setMessageCode("common.find.nothing");
            } else {
                param.setErrorCode(Constants.ERROR_CODE_0);
                param.setErrorDecription(LabelUtil.getKey("common.success", locale));
                param.setMessageCode("common.success");
                param.setTransactionId(req.get(0).getId().toString());

                /*gui tin nhan*/
                for (RegOnline temp : req) {
                    List<String> listParamSms = new ArrayList<String>();
                    listParamSms.add(temp.getCustomerName());
                    listParamSms.add(temp.getId().toString());
                    new com.viettel.bccs.cm.controller.payment.PaymentController().sendSMS(cmPre, isdn, Constants.AP_PARAM_PARAM_TYPE_SMS, "REG_OLN_FTHH", listParamSms);
                    commitTransactions(cmPre);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cmPosSession != null) {
                closeSessions(cmPosSession, cmPre);
            }
        }
        return param;
    }

    public ParamOut getProvinceReqOnline(HibernateHelper hibernateHelper, String locale, String staffCode, Long filterStatus, Long statusDeadline) {
        Session cmPosSession = null;
        ParamOut param = new ParamOut();
        boolean isError = false;
        param.setErrorCode(Constants.ERROR_CODE_1);
        try {
            if (STATUS_CONNECTED.equals(filterStatus) || STATUS_CANCELED.equals(filterStatus)) {
                statusDeadline = null;
            }
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            String condition = "";
            if (filterStatus != null && filterStatus >= 0 && statusDeadline != null && statusDeadline > 0) {
                condition += " and status = ? \n";
            }
            Staff staff = new StaffBussiness().findByCode(cmPosSession, staffCode == null ? "" : staffCode.toUpperCase());
            if (staff == null) {
                param.setErrorDecription(LabelUtil.getKey("staff.does.not.exist.or.has.been.disabled", locale));
                param.setMessageCode("staff.does.not.exist.or.has.been.disabled");
                return param;
            }
            staffCode = staffCode.toUpperCase();
            Shop shop = new ShopDAO().findById(cmPosSession, staff.getShopId());
            if (LEVEL_BRANCH.toString().equals(shop.getShopType())) {
                condition += " and province = '" + shop.getProvince() + "'  \n";
            } else if (LEVEL_SHOWROOM.toString().equals(shop.getShopType())) {
                condition += " and assign_staff = '" + staffCode + "'  \n";
            }

            String sql = "select province\n"
                    + "       ,sum(connected) connected\n"
                    + "       ,sum(canceled) canceled\n"
                    + "       ,sum(overtime) overtime\n"
                    + "       ,sum(ontime) ontime\n"
                    + "from(\n"
                    + "SELECT  province, \n"
                    + "        case when status = 4 then 1 else 0 end connected, \n"
                    + "        case when status = 3 then 1 else 0 end canceled, \n"
                    + "        case when ((status =0 and (sysdate - update_date) *24 > 8) or (status =1 and (sysdate - update_date) > 1) or (status =2 and (sysdate - update_date) > 2)) then 1 else 0 end overtime,\n"
                    + "        case when ((status =0 and (sysdate - update_date) *24 <= 8) or (status =1 and (sysdate - update_date) <= 1)or (status =2 and (sysdate - update_date) <= 2)) then 1 else 0 end ontime\n"
                    + "  FROM   reg_online\n"
                    + "  where create_date >= trunc(sysdate - 30)\n"
                    + condition
                    + ") group by province\n"
                    + "order by province ";
            Query query = cmPosSession.createSQLQuery(sql);
            if (filterStatus != null && filterStatus >= 0 && statusDeadline != null && statusDeadline > 0) {
                query.setParameter(0, filterStatus);
            }
            List list = query.list();
            if (list != null && !list.isEmpty()) {
                param.setErrorCode(Constants.ERROR_CODE_0);
                param.setErrorDecription(LabelUtil.getKey("common.success", locale));
                param.setMessageCode("common.success");

                List<ProvinceChart> listChart = new ArrayList<ProvinceChart>();
                param.setChart(listChart);

                for (int i = 0; i < list.size(); i++) {
                    Object[] arr = (Object[]) list.get(i);
                    String description = arr[0].toString();
                    Long connected = Long.parseLong(arr[1].toString());
                    Long canceled = Long.parseLong(arr[2].toString());
                    Long overtime = Long.parseLong(arr[3].toString());
                    Long ontime = Long.parseLong(arr[4].toString());
                    if (connected + canceled + overtime + ontime == 0) {
                        continue;
                    }
                    ProvinceChart chart = new ProvinceChart();
                    listChart.add(chart);
                    chart.setProvince(description);
                    List<ProvinceChartDetail> listChartDetail = new ArrayList<ProvinceChartDetail>();
                    chart.setDetail(listChartDetail);
                    if ((filterStatus == null || filterStatus < 0) && (statusDeadline == null || statusDeadline < 0)) {
                        ProvinceChartDetail c1 = new ProvinceChartDetail();
                        c1.setDescription(LabelUtil.getKey(STAFF_STATUS_CONNECTED, locale));
                        c1.setTotal(connected);
                        c1.setColor(COLOR_CONNECTED);
                        listChartDetail.add(c1);

                        c1 = new ProvinceChartDetail();
                        c1.setDescription(LabelUtil.getKey(STAFF_STATUS_CANCELED, locale));
                        c1.setTotal(canceled);
                        c1.setColor(COLOR_CANCEL);
                        listChartDetail.add(c1);

                        c1 = new ProvinceChartDetail();
                        c1.setDescription(LabelUtil.getKey(STAFF_STATUS_ONTIME, locale));
                        c1.setTotal(ontime);
                        c1.setColor(COLOR_ONTIME);
                        listChartDetail.add(c1);

                        c1 = new ProvinceChartDetail();
                        c1.setDescription(LabelUtil.getKey(STAFF_STATUS_OVERTIME, locale));
                        c1.setTotal(overtime);
                        c1.setColor(COLOR_OVERTIME);
                        listChartDetail.add(c1);
                        chart.setTotal(connected + canceled + ontime + overtime);
                    }
                    if (filterStatus != null && filterStatus > 0 && (statusDeadline == null || statusDeadline < 0)) {
                        ProvinceChartDetail c1 = new ProvinceChartDetail();
                        if (STATUS_CONNECTED.equals(filterStatus)) {
                            c1.setDescription(LabelUtil.getKey(STAFF_STATUS_CONNECTED, locale));
                            c1.setTotal(connected);
                            c1.setColor(COLOR_CONNECTED);
                        }
                        if (STATUS_CANCELED.equals(filterStatus)) {
                            c1.setDescription(LabelUtil.getKey(STAFF_STATUS_CANCELED, locale));
                            c1.setTotal(canceled);
                            c1.setColor(COLOR_CANCEL);
                        }
                        if (STATUS_ONTIME.equals(filterStatus)) {
                            c1.setDescription(LabelUtil.getKey(STAFF_STATUS_ONTIME, locale));
                            c1.setTotal(ontime);
                            c1.setColor(COLOR_ONTIME);
                        }
                        if (STATUS_OVERTIME.equals(filterStatus)) {
                            c1.setDescription(LabelUtil.getKey(STAFF_STATUS_OVERTIME, locale));
                            c1.setTotal(overtime);
                            c1.setColor(COLOR_OVERTIME);
                        }
                        chart.setTotal(c1.getTotal());
                        listChartDetail.add(c1);
                        if (chart.getTotal() == 0) {
                            listChart.remove(listChart.size() - 1);
                        }
                    }
                    if (filterStatus != null && filterStatus >= 0 && statusDeadline != null && statusDeadline > 0) {
                        ProvinceChartDetail c1 = new ProvinceChartDetail();
                        if (STATUS_ONTIME.equals(filterStatus) || STATUS_ONTIME.equals(statusDeadline)) {
                            c1.setDescription(LabelUtil.getKey(STAFF_STATUS_ONTIME, locale));
                            c1.setTotal(ontime);
                            c1.setColor(COLOR_ONTIME);
                        }
                        if (STATUS_OVERTIME.equals(filterStatus) || STATUS_OVERTIME.equals(statusDeadline)) {
                            c1.setDescription(LabelUtil.getKey(STAFF_STATUS_OVERTIME, locale));
                            c1.setTotal(overtime);
                            c1.setColor(COLOR_OVERTIME);
                        }
                        chart.setTotal(c1.getTotal());
                        listChartDetail.add(c1);
                        if (chart.getTotal() == 0) {
                            listChart.remove(listChart.size() - 1);
                        }
                    }
                }
                if (listChart.isEmpty()) {
                    param.setErrorCode(Constants.ERROR_CODE_1);
                    param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                    param.setMessageCode("common.find.nothing");
                }
            } else {
                param.setErrorCode(Constants.ERROR_CODE_1);
                param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                param.setMessageCode("common.find.nothing");
            }
        } catch (Exception ex) {
            isError = true;
            ex.printStackTrace();
        } finally {
            if (cmPosSession != null) {
                if (!isError) {
                    commitTransactions(cmPosSession);
                } else {
                    rollBackTransactions(cmPosSession);
                }
                closeSessions(cmPosSession);
            }
        }
        return param;
    }

    public ParamOut getReqOnline(HibernateHelper hibernateHelper, String locale, String staffCode, Long filterStatus) {
        Session cmPosSession = null;
        ParamOut param = new ParamOut();
        boolean isError = false;
        param.setErrorCode(Constants.ERROR_CODE_1);
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            String condition = "";
            Staff staff = new StaffBussiness().findByCode(cmPosSession, staffCode == null ? "" : staffCode.toUpperCase());
            if (staff == null) {
                param.setErrorDecription(LabelUtil.getKey("staff.does.not.exist.or.has.been.disabled", locale));
                param.setMessageCode("staff.does.not.exist.or.has.been.disabled");
                return param;
            }
            staffCode = staffCode.toUpperCase();
            Shop shop = new ShopDAO().findById(cmPosSession, staff.getShopId());
            if (LEVEL_BRANCH.toString().equals(shop.getShopType())) {
                condition += " and province = '" + shop.getProvince() + "'  \n";
            } else if (LEVEL_SHOWROOM.toString().equals(shop.getShopType())) {
                condition += " and assign_staff = '" + staffCode + "'  \n";
            }

            String sql = "select status,\n"
                    + "       overtime,\n"
                    + "       ontime,\n"
                    + "       total\n"
                    + "from (\n"
                    + "SELECT  status,\n"
                    + "        sum(case when ((status =0 and (sysdate - update_date) *24 > 8) or (status =1 and (sysdate - update_date) > 1)or (status =2 and (sysdate - update_date) > 2)) then 1 else 0 end) overtime,\n"
                    + "        sum(case when ((status =0 and (sysdate - update_date) *24 <= 8) or (status =1 and (sysdate - update_date) <= 1)or (status =2 and (sysdate - update_date) <= 2)) then 1 else 0 end) ontime,\n"
                    + "        count(1) total\n"
                    + "  FROM   reg_online\n"
                    + "  where create_date >= trunc(sysdate - 30)\n"
                    + condition
                    + "  group by status\n"
                    + "  order by status\n"
                    + ")";
            Query query = cmPosSession.createSQLQuery(sql);
            List list = query.list();
            param.setErrorCode(Constants.ERROR_CODE_0);
            param.setErrorDecription(LabelUtil.getKey("common.success", locale));
            param.setMessageCode("common.success");

            Long total = 0l;
            List<ProvinceChart> listChart = new ArrayList<ProvinceChart>();
            DashboardChart dashboard = new DashboardChart();
            param.setDashboard(dashboard);
            dashboard.setRequest(listChart);

            /*New request*/
            ProvinceChart chart = createChartTemplate(STAFF_STATUS_NEW_REQUEST, STATUS_NEW_REQUEST, COLOR_NEW_REQUEST, locale);
            listChart.add(chart);

            /*Assigned*/
            chart = createChartTemplate(STAFF_STATUS_ASSIGNED, STATUS_ASSIGNED, COLOR_ASSIGNED, locale);
            listChart.add(chart);

            /*Registered*/
            chart = createChartTemplate(STAFF_STATUS_REGISTERED, STATUS_REGISTERED, COLOR_REGISTERED, locale);
            listChart.add(chart);

            /*Canceled*/
            chart = createChartTemplate(STAFF_STATUS_CANCELED, STATUS_CANCELED, COLOR_CANCEL, locale);
            listChart.add(chart);

            /*Connected*/
            chart = createChartTemplate(STAFF_STATUS_CONNECTED, STATUS_CONNECTED, COLOR_CONNECTED, locale);
            listChart.add(chart);

            if (list != null && !list.isEmpty()) {
                for (int i = 0; i < list.size(); i++) {
                    Object[] arr = (Object[]) list.get(i);
                    Long status = Long.parseLong(arr[0].toString());
                    Long overtime = Long.parseLong(arr[1].toString());
                    Long ontime = Long.parseLong(arr[2].toString());
                    Long count = Long.parseLong(arr[3].toString());
                    for (ProvinceChart chartTemp : listChart) {
                        if (chartTemp.getStatus().equals(status)) {
                            total += count;
                            chartTemp.setTotal(count);
                            chartTemp.getDetail().get(0).setTotal(ontime);
                            chartTemp.getDetail().get(1).setTotal(overtime);
                            break;
                        }
                    }
                }
            }
            param.setTotal(total);
        } catch (Exception ex) {
            isError = true;
            ex.printStackTrace();
        } finally {
            if (cmPosSession != null) {
                if (!isError) {
                    commitTransactions(cmPosSession);
                } else {
                    rollBackTransactions(cmPosSession);
                }
                closeSessions(cmPosSession);
            }
        }
        return param;
    }

    public ParamOut getReasonRegOnline(HibernateHelper hibernateHelper, String locale) {
        Session cmPosSession = null;
        ParamOut param = new ParamOut();
        boolean isError = false;
        try {
            param.setErrorCode(Constants.ERROR_CODE_0);
            param.setErrorDecription(LabelUtil.getKey("common.success", locale));
            String reasonMes = LabelUtil.getKey("LIST_REASON_REG_FTTH_ONLINE", locale);
            List<String> reason = Arrays.asList(reasonMes.split(";"));
            param.setReason(reason);
        } catch (Exception ex) {
            isError = true;
            ex.printStackTrace();
        } finally {
            if (cmPosSession != null) {
                if (!isError) {
                    commitTransactions(cmPosSession);
                } else {
                    rollBackTransactions(cmPosSession);
                }
                closeSessions(cmPosSession);
            }
        }
        return param;
    }

    public ParamOut getListShowroom(HibernateHelper hibernateHelper, String locale, String staffCode, String province) {
        Session cmPosSession = null;
        ParamOut param = new ParamOut();
        param.setErrorCode(Constants.ERROR_CODE_1);
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            Staff staff = new StaffBussiness().findByCode(cmPosSession, staffCode == null ? "" : staffCode.toUpperCase());
            if (staff == null) {
                param.setErrorDecription(LabelUtil.getKey("staff.does.not.exist.or.has.been.disabled", locale));
                param.setMessageCode("staff.does.not.exist.or.has.been.disabled");
                return param;
            }
            staffCode = staffCode.toUpperCase();
            Shop shop = new ShopDAO().findById(cmPosSession, staff.getShopId());
            if (!LEVEL_VTC.toString().equals(shop.getShopType())) {
                if (!shop.getProvince().equals(province)) {
                    param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                    param.setMessageCode("common.find.nothing");
                    return param;
                }
            }
            String sql = "from Shop where province = ? and shopType = 3 and status = 1 order by shopCode";
            Query query = cmPosSession.createQuery(sql);
            query.setParameter(0, province);
            List<Shop> list = query.list();
            if (list != null && list.isEmpty()) {
                for (Shop temp : list) {
                    temp.setName(temp.getShopCode());
                }
            }
            param.setShowroom(list);
            param.setErrorCode(Constants.ERROR_CODE_0);
            param.setErrorDecription(LabelUtil.getKey("common.success", locale));
            param.setMessageCode("common.success");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cmPosSession != null) {
                closeSessions(cmPosSession);
            }
        }
        return param;
    }

    public ParamOut getListStaffShowroom(HibernateHelper hibernateHelper, String locale, String staffCode, String shopCode) {
        Session cmPosSession = null;
        ParamOut param = new ParamOut();
        param.setErrorCode(Constants.ERROR_CODE_1);
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            Staff staff = new StaffBussiness().findByCode(cmPosSession, staffCode == null ? "" : staffCode.toUpperCase());
            if (staff == null) {
                param.setErrorDecription(LabelUtil.getKey("staff.does.not.exist.or.has.been.disabled", locale));
                param.setMessageCode("staff.does.not.exist.or.has.been.disabled");
                return param;
            }
            staffCode = staffCode.toUpperCase();
            Shop shop = new ShopDAO().findById(cmPosSession, staff.getShopId());
            Shop shopSearch = new ShopDAO().findByCode(cmPosSession, shopCode);
            if (shopSearch == null) {
                param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                param.setMessageCode("common.find.nothing");
                return param;
            }
            if (!LEVEL_VTC.toString().equals(shop.getShopType())) {
                if (!shop.getProvince().equals(shopSearch.getProvince())) {
                    param.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                    param.setMessageCode("common.find.nothing");
                    return param;
                }
            }

            String sql = "from Staff where shopId = ? and status = 1 order by staffCode";
            Query query = cmPosSession.createQuery(sql);
            query.setParameter(0, shopSearch.getShopId());
            List<Staff> list = query.list();
            param.setStaff(list);
            param.setErrorCode(Constants.ERROR_CODE_0);
            param.setErrorDecription(LabelUtil.getKey("common.success", locale));
            param.setMessageCode("common.success");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cmPosSession != null) {
                closeSessions(cmPosSession);
            }
        }
        return param;
    }

    private ProvinceChart createChartTemplate(String description, Long status, String color, String locale) {
        ProvinceChart chart = new ProvinceChart();
        chart.setTotal(0l);
        chart.setDescription(LabelUtil.getKey(description, locale));
        chart.setStatus(status);
        chart.setColor(color);
        List<ProvinceChartDetail> listChartDetail = new ArrayList<ProvinceChartDetail>();
        chart.setDetail(listChartDetail);
        ProvinceChartDetail c1 = new ProvinceChartDetail();
        c1.setDescription(LabelUtil.getKey(STAFF_STATUS_ONTIME, locale));
        c1.setTotal(0l);
        c1.setColor(COLOR_ONTIME);
        c1.setStatus(STATUS_ONTIME);
        listChartDetail.add(c1);

        c1 = new ProvinceChartDetail();
        c1.setDescription(LabelUtil.getKey(STAFF_STATUS_OVERTIME, locale));
        c1.setTotal(0l);
        c1.setColor(COLOR_CANCEL);
        c1.setStatus(STATUS_OVERTIME);
        listChartDetail.add(c1);
        return chart;
    }
}
