/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.RequestChangeInformationBusiness;
import com.viettel.bccs.cm.model.Codes;
import com.viettel.bccs.cm.model.FormChangeInformation;
import com.viettel.bccs.cm.model.OTPObject;
import com.viettel.bccs.cm.model.pre.ReasonPre;
import com.viettel.bccs.cm.model.SubMb;
import com.viettel.bccs.cm.model.SubMbOut;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.output.ChangeInformationOut;
import com.viettel.brcd.ws.model.output.ListReasonByCodePreOut;
import com.viettel.brcd.ws.model.output.OTPOut;
import com.viettel.brcd.ws.model.output.SubInfoByISDNPreOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author partner1
 */
public class RequestChangeInformationController extends BaseController {

    public RequestChangeInformationController() {
        logger = Logger.getLogger(RequestChangeInformationController.class);
    }

    public SubInfoByISDNPreOut getSubInfoByISDNPre(HibernateHelper hibernateHelper,
            String isdn, String locale) {
        SubInfoByISDNPreOut result = null;
        Session cmPreSession = null;
        Session cmProductSession = null;
        Session cmIMSession = null;
        try {
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            cmProductSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            cmIMSession = hibernateHelper.getSession(Constants.SESSION_IM);
            if (isdn == null || isdn.trim().isEmpty()) {
                result = new SubInfoByISDNPreOut(Constants.ERROR_CODE_1, "No subscriber found");
                return result;
            }
            if (locale == null || locale.trim().isEmpty()) {
                result = new SubInfoByISDNPreOut(Constants.ERROR_CODE_1, "Please input locale");
                return result;
            }
            RequestChangeInformationBusiness requestChangeInformationBusiness = new RequestChangeInformationBusiness();
            SubMb subMb = requestChangeInformationBusiness.getSubMbByISDN(cmPreSession, cmProductSession, cmIMSession, isdn);
            if (subMb == null) {
                result = new SubInfoByISDNPreOut(Constants.ERROR_CODE_1, "No subscriber found");
                return result;
            }

            if (subMb.getStatus().compareTo(3l) == 0) {
                result = new SubInfoByISDNPreOut(Constants.ERROR_CODE_1, "No subscriber found");
                return result;
            }

            if (subMb.getActStatus() != null) {
                if(!"00".equals(subMb.getActStatus())){
                    if("03".equals(subMb.getActStatus())){
                        result = new SubInfoByISDNPreOut(Constants.ERROR_CODE_1, "This number is inactive, cannot change information");
                        return result;
                    }else if("01".equals(subMb.getActStatus()) || "10".equals(subMb.getActStatus())
                            || "11".equals(subMb.getActStatus())){
                        result = new SubInfoByISDNPreOut(Constants.ERROR_CODE_1, "This number is blocked 1 way, cannot change information");
                        return result;
                    } else {
                        result = new SubInfoByISDNPreOut(Constants.ERROR_CODE_1, "This number is blocked 2 ways, cannot change information");
                        return result;
                    }
                }
            }

            String birthDate = "";
            String pattern = "dd/MM/yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            if (subMb != null && subMb.getBirthDate() != null) {
                birthDate = simpleDateFormat.format(subMb.getBirthDate());
            }
            SubMbOut subMbOut = new SubMbOut(subMb.getActStatus(), subMb.getCustId(),
                    subMb.getCustomerName(), subMb.getGroupName(),
                    subMb.getIdNo(), birthDate, subMb.getIsdn(), subMb.getOfferId(), subMb.getOfferName(),
                    subMb.getStatus(), subMb.getStatusName(), subMb.getSubId());
            result = new SubInfoByISDNPreOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), subMbOut);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new SubInfoByISDNPreOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPreSession);
            closeSessions(cmProductSession);
            closeSessions(cmIMSession);
        }

    }

    public ListReasonByCodePreOut getReasonByCodes(HibernateHelper hibernateHelper,
            Codes codes, String locale) {
        ListReasonByCodePreOut result = null;
        Session cmPreSession = null;
        try {
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            if (locale == null || locale.trim().isEmpty()) {
                result = new ListReasonByCodePreOut(Constants.ERROR_CODE_1, "Please input locale");
                return result;
            }
            if (codes == null || codes.getElement() == null || codes.getElement().size() <= 0) {
                result = new ListReasonByCodePreOut(Constants.ERROR_CODE_1, "Please input codes");
                return result;
            } else {
            }
            RequestChangeInformationBusiness requestChangeInformationBusiness = new RequestChangeInformationBusiness();
            List<ReasonPre> reasons = requestChangeInformationBusiness.getListReasonByCodes(cmPreSession, codes.getElement());
            result = new ListReasonByCodePreOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), reasons);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ListReasonByCodePreOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPreSession);
        }

    }

    public OTPOut getOTPChangeInformationPre(String locale, String isdn) {
        OTPOut result = null;
        try {
            if (isdn == null || isdn.trim().equals("")) {
                result = new OTPOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, "isdn is invalid"));
                return result;
            }
            if (locale == null || locale.trim().equals("")) {
                result = new OTPOut(Constants.ERROR_CODE_1, "Please input locale");
                return result;
            }
            if (isdn.startsWith("0") || isdn.startsWith("855")) {
                if (isdn.startsWith("0")) {
                    isdn = isdn.substring(1);
                }
                if (isdn.startsWith("855")) {
                    isdn = isdn.substring(3);
                }
            }
            RequestChangeInformationBusiness requestChangeInformationBusiness = new RequestChangeInformationBusiness();
            OTPObject otp = requestChangeInformationBusiness.getOTPChangeInformationPre(isdn);
            if(otp != null){
                if ("00".equals(otp.getErrorCode())) {
                    result = new OTPOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
                    return result;
                } else {
                    result = new OTPOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, otp.getDesc()));
                    return result;
                }
            }else{
                result = new OTPOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, "OTP system error"));
                return result;
            }
            

        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new OTPOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        }
    }

    public ChangeInformationOut changeInformationCustomer(HibernateHelper hibernateHelper, String locale,
            String otp, Long subId, String serviceType, Long custId, String name,
            Long idType, String province, String address, String DOB, String Sex,
            String isdn, String contact, FormChangeInformation form,
            FormChangeInformation lstImageIDCard, FormChangeInformation lstOldImageIDCard,
            String reasonCode, Long reasonId, String idNo, String staffCode) {
        ChangeInformationOut result = null;
        Session cmPreSession = null;
        Session cmIMSession = null;
        boolean hasErr = false;
        try {
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            cmIMSession = hibernateHelper.getSession(Constants.SESSION_IM);
            if (locale == null || locale.trim().equals("")) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Please input locale");
                return result;
            }
            if (subId == null || subId == 0L) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Please input subId");
                return result;
            }
            if (serviceType == null || serviceType.trim().equals("")) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Please input serviceType");
                return result;
            }
            if (province == null || province.trim().equals("")) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Please input province");
                return result;
            }
            if (address == null || address.trim().equals("")) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Please input address");
                return result;
            }
            if (DOB == null || DOB.trim().equals("")) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Please input DOB");
                return result;
            }
            if (name == null || name.trim().equals("")) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Please input name");
                return result;
            }
            if (otp == null || otp.trim().equals("")) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_4, "OTP is incorrect");
                return result;
            }
            if (idType == null || idType == 0L) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Please input idType");
                return result;
            }
            if (Sex == null || Sex.trim().equals("")) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Sex should be M or F");
                return result;
            } else {
                if (!Sex.equals("M") && !Sex.equals("F")) {
                    result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Sex should be M or F");
                    return result;
                }
            }
            if (reasonCode == null) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "reasonCode should be CTM or TRSP");
                return result;
            } else {
                if (!reasonCode.trim().equals("CTM") && !reasonCode.trim().equals("TRSP")) {
                    result = new ChangeInformationOut(Constants.ERROR_CODE_1, "reasonCode should be CTM or TRSP");
                    return result;
                }
            }
            if (isdn == null || isdn.trim().equals("")) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Please input isdn");
                return result;
            }
            if (form == null || form.getElement() == null || form.getElement().size() == 0) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Please input form");
                return result;
            }
            if (lstImageIDCard == null || lstImageIDCard.getElement() == null || lstImageIDCard.getElement().size() == 0) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Please input lstImageIDCard");
                return result;
            }
            if (idNo == null || idNo.trim().equals("")) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Please input idNo");
                return result;
            }
            if (reasonId == null || reasonId == 0L) {
                result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Please input reasonId");
                return result;
            }
            if (reasonCode != null && reasonCode.trim().equals("TRSP")) {
                if (lstOldImageIDCard == null || lstOldImageIDCard.getElement() == null || lstOldImageIDCard.getElement().size() == 0) {
                    result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Please input lstOldImageIDCard");
                    return result;
                }
            }
            RequestChangeInformationBusiness requestChangeInformationBusiness = new RequestChangeInformationBusiness();
            result = requestChangeInformationBusiness.changeInformationCustomer(cmIMSession, cmPreSession, locale,
                    otp, subId, serviceType, custId, name,
                    idType, province, address, DOB, Sex,
                    isdn, contact, form,
                    lstImageIDCard, lstOldImageIDCard,
                    reasonCode, reasonId, idNo, staffCode);
            if(Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
                commitTransactions(cmPreSession, cmIMSession);
            }else{
                hasErr = true;
            }
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ChangeInformationOut(Constants.ERROR_CODE_1, "Exception: " + ex.getMessage());
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPreSession, cmIMSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPreSession, cmIMSession);
            LogUtils.info(logger, "RequestChangeInformationController.changeInformationCustomer:result=" + LogUtils.toJson(result));
        }
    }
}
