package com.viettel.bccs.cm.controller;

import com.viettel.bccs.api.Task.BO.ResultBO;
import com.viettel.bccs.api.Task.DAO.ManagerTaskDAO;
import com.viettel.bccs.cm.bussiness.StepBussiness;
import com.viettel.bccs.cm.bussiness.VAdslRequestBussiness;
import com.viettel.bccs.cm.bussiness.VAdslRequestDAO;
import com.viettel.bccs.cm.dao.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.dao.SubReqAdslLlDAO;
import com.viettel.bccs.cm.dao.TechnicalConnectorDAO;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.StepInfo;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubReqAdslLl;
import com.viettel.bccs.cm.model.VAdslRequest;
import com.viettel.bccs.cm.model.pre.ApParamPre;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.bccs.cm.util.DateTimeUtils;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ValidateUtils;
import com.viettel.brcd.dao.pre.ApParamDAO;
import com.viettel.brcd.dao.util.RequestDAOUtil;
import com.viettel.brcd.ws.model.input.CancelRequestInput;
import com.viettel.brcd.ws.model.input.ContractIn;
import com.viettel.brcd.ws.model.input.Coordinate;
import com.viettel.brcd.ws.model.input.SearchSubRequestInput;
import com.viettel.brcd.ws.model.output.CoordinateOut;
import com.viettel.brcd.ws.model.output.InvestOut;
import com.viettel.brcd.ws.model.output.SearchSubRequestOut;
import com.viettel.brcd.ws.model.output.StepInfoOut;
import com.viettel.brcd.ws.model.output.UpdateResultOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.brcd.ws.supplier.nims.getInfoInfras.GetSnFromSplReponse;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class RequestController extends BaseController {

    public RequestController() {
        logger = Logger.getLogger(RequestController.class);
    }

    public String validateSearchSubRequest(SearchSubRequestInput searchSubRequestInput, String locale) {
        String mess;
        //<editor-fold defaultstate="collapsed" desc="staffCode">
        String staffCode = searchSubRequestInput.getStaffCode();
        if (staffCode == null) {
            mess = LabelUtil.getKey("validate.required", locale, "staffCode");
            return mess;
        }
        staffCode = staffCode.trim();
        if (staffCode.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "staffCode");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fromDate">
        String fromDate = searchSubRequestInput.getFromDate();
        if (fromDate == null) {
            mess = LabelUtil.getKey("input.invalid.fromDate", locale);
            return mess;
        }
        fromDate = fromDate.trim();
        if (fromDate.isEmpty()) {
            mess = LabelUtil.getKey("input.invalid.fromDate", locale);
            return mess;
        }
        if (!ValidateUtils.isDateddMMyyyy(fromDate)) {
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("fromDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        Date dFrom;
        try {
            dFrom = DateTimeUtils.toDateddMMyyyy(fromDate);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage());
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("fromDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        if (dFrom == null) {
            mess = LabelUtil.getKey("validate.required", locale, "fromDate");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="toDate">
        String toDate = searchSubRequestInput.getToDate();
        if (toDate == null) {
            mess = LabelUtil.getKey("input.invalid.toDate", locale);
            return mess;
        }
        toDate = toDate.trim();
        if (toDate.isEmpty()) {
            mess = LabelUtil.getKey("input.invalid.toDate", locale);
            return mess;
        }
        if (!ValidateUtils.isDateddMMyyyy(toDate)) {
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("toDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        Date dTo;
        try {
            dTo = DateTimeUtils.toDateddMMyyyy(toDate);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage());
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("toDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        if (dTo == null) {
            mess = LabelUtil.getKey("validate.required", locale, "toDate");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="chi tim kiem trong 30 ngay">
        Integer days = DateTimeUtils.getDayBetween(dFrom, dTo);
        if (days != null && days > 30) {
            mess = LabelUtil.formatKey("search.between", locale, 30);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="pageSize">
        Long pageSize = searchSubRequestInput.getPageSize();
        if (pageSize == null || pageSize < 0L) {
            mess = LabelUtil.getKey("input.invalid.pageSize", locale);
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="pageNo">
        Long pageNo = searchSubRequestInput.getPageNo();
        if (pageNo == null || pageNo < 0L) {
            mess = LabelUtil.getKey("input.invalid.pageNo", locale);
            return mess;
        }
        //</editor-fold>
        mess = null;
        return mess;
    }

    public SearchSubRequestOut searchSubRequest(Session cmPosSession, String addUser, String custName, String idNo, String serviceAlias, String account, String subReqStatus, Date fromDate, Date toDate, Long pageSize, Long pageNo, String locale) {
        SearchSubRequestOut result;

        Long isSmart = Constants.IS_SMART;
        List<Long> lstSubReqStatus = new ArrayList<Long>();
        lstSubReqStatus.add(Constants.SUB_REQ_STATUS_NEW);
        lstSubReqStatus.add(Constants.SUB_REQ_STATUS_SIGN_CONTRACT);
        lstSubReqStatus.add(Constants.SUB_REQ_STATUS_ACTIVE);
        lstSubReqStatus.add(Constants.SUB_REQ_STATUS_CANCEL);

        VAdslRequestBussiness bussiness = new VAdslRequestBussiness();

        Long count = bussiness.count(cmPosSession, isSmart, addUser, custName, idNo, serviceAlias, account, ConvertUtils.toLong(subReqStatus), fromDate, toDate, lstSubReqStatus);
        if (count == null || count <= 0L) {
            result = new SearchSubRequestOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        }

        Integer start = getStart(pageSize, pageNo);
        Integer max = getMax(pageSize, pageNo, count);
        List<VAdslRequest> subRequests = bussiness.find(cmPosSession, isSmart, addUser, custName, idNo, serviceAlias, account, ConvertUtils.toLong(subReqStatus), fromDate, toDate, lstSubReqStatus, start, max);

        result = new SearchSubRequestOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), subRequests, count);
        return result;
    }

    public SearchSubRequestOut searchSubRequest(HibernateHelper hibernateHelper, SearchSubRequestInput searchSubRequestInput, String locale) {
        SearchSubRequestOut result = null;
        Session cmPosSession = null;
        try {
            LogUtils.info(logger, "RequestController.searchSubRequest:searchSubRequestInput=" + LogUtils.toJson(searchSubRequestInput));
            String mess = validateSearchSubRequest(searchSubRequestInput, locale);
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    result = new SearchSubRequestOut(Constants.ERROR_CODE_1, mess);
                    return result;
                }
            }
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            result = searchSubRequest(cmPosSession, searchSubRequestInput.getStaffCode(),
                    searchSubRequestInput.getCustName(), searchSubRequestInput.getIdNo(),
                    searchSubRequestInput.getServiceAlias(), searchSubRequestInput.getAccount(),
                    searchSubRequestInput.getReqStatus(), DateTimeUtils.toDateddMMyyyy(searchSubRequestInput.getFromDate()),
                    DateTimeUtils.toDateddMMyyyy(searchSubRequestInput.getToDate()), searchSubRequestInput.getPageSize(),
                    searchSubRequestInput.getPageNo(), locale);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new SearchSubRequestOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "RequestController.searchSubRequest:result=" + LogUtils.toJson(result));
        }
    }

    public StepInfoOut getNextStepInfo(HibernateHelper hibernateHelper, Long subReqId, String locale) {
        StepInfoOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            StepInfo stepInfo = new StepBussiness().getNextStepInfo(cmPosSession, subReqId, locale);
            if (stepInfo == null) {
                result = new StepInfoOut(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid.subReqIsNull", locale));
                return result;
            }
            String mess = stepInfo.getMessage();
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    result = new StepInfoOut(Constants.ERROR_CODE_1, mess);
                    return result;
                }
            }
            result = new StepInfoOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), stepInfo);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new StepInfoOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "RequestController.getNextStepInfo:result=" + LogUtils.toJson(result));
        }
    }

    public WSRespone cancelRequest(HibernateHelper hibernateHelper, CancelRequestInput cancelRequestInput, String locale) {
        WSRespone result = null;
        Session cmPosSession = null;
        boolean hasErr = false;
        try {
            LogUtils.info(logger, "RequestController.cancelRequest:cancelRequestInput=" + LogUtils.toJson(cancelRequestInput));
            if (cancelRequestInput == null) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid", locale));
                return result;
            }
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            String mess = new VAdslRequestDAO().cancelSubRequest(cmPosSession, cancelRequestInput.getReqId(), cancelRequestInput.getStaffId(), cancelRequestInput.getShopId(), cancelRequestInput.getReasonId(), locale);
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, mess);
                }
            }
            commitTransactions(cmPosSession);
            result = new WSRespone(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            LogUtils.info(logger, "RequestController.cancelRequest:result=" + LogUtils.toJson(result));
        }
    }

    public UpdateResultOut addRequestContract(HibernateHelper hibernateHelper, ContractIn input, String locale) {
        UpdateResultOut result = null;
        Session cmPosSession = null;
        Session imSession = null;
        Session pmSession = null;
        Session paymentSession = null;
        Session nimsSession = null;
        boolean hasErr = false;
        try {
            LogUtils.info(logger, "RequestController.addRequestContract:input=" + LogUtils.toJson(input));
            if (input == null) {
                result = new UpdateResultOut(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid", locale), false);
                return result;
            }
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            pmSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            paymentSession = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            nimsSession = hibernateHelper.getSession(Constants.SESSION_NIMS);
            
            RequestDAOUtil req = new RequestDAOUtil();
            //start: duyetdk - check lock connector
            List<ApParam> ap = new ApParamDAO().getApParamByType(cmPosSession, "LOCK_CONNECTOR");
            if (ap != null && ap.size() > 0) {
                String connectorCode = "";
                List<String> lstA = req.getConnectorCodeById(nimsSession, input.getInfrastruct().getCableBoxId(), input.getInfrastruct().getTechnology());
                if ("4".equals(input.getInfrastruct().getTechnology())) {//4:GPON
                    String splCode = String.valueOf(lstA != null && !lstA.isEmpty() ? lstA.get(0) : "");
                    GetSnFromSplReponse snId = new TechnicalConnectorDAO().findBySplitterCode(splCode);
                    List<String> lstB = req.getConnectorCodeById(nimsSession, snId != null ? snId.getReturn() : 0L, input.getInfrastruct().getTechnology());
                    connectorCode = String.valueOf(lstB != null && !lstB.isEmpty() ? lstB.get(0) : "");
                } else {//con lai la AON
                    connectorCode = String.valueOf(lstA != null && !lstA.isEmpty() ? lstA.get(0) : "");
                }

                if (connectorCode != null && !connectorCode.trim().isEmpty()) {
                    List<String> list = req.getLockServiceByConnectorCode(cmPosSession, connectorCode, input.getInfrastruct().getTechnology());
                    String lockedService = "";
                    if (list != null && !list.isEmpty()) {
                        lockedService = String.valueOf(list.get(0));
                    }
                    if (lockedService.toUpperCase().trim().contains(input.getSubscriber().getLineType().toUpperCase().trim())) {
                        hasErr = true;
                        return new UpdateResultOut(Constants.ERROR_CODE_1, "The connector is locked for this service. Please select others!", false);
                    }
                }
            }
            //end: duyetdk - check lock connector
            
            return new RequestDAOUtil().addRequestContract(cmPosSession, imSession, pmSession, paymentSession, nimsSession, input, locale);
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new UpdateResultOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()), false);
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession, imSession, pmSession, paymentSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession, imSession, pmSession, paymentSession, nimsSession);
            LogUtils.info(logger, "RequestController.addRequestContract:result=" + LogUtils.toJson(result));
        }
    }

    public WSRespone createRequestNoInfra(HibernateHelper hibernateHelper, Long custId, String serviceType, Number Lat, Number Lng, String locale) {
        WSRespone result = new WSRespone("0", "Success");
        Session cmPosSession = null;
        boolean hasErr = false;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPosSession.beginTransaction();
            LogUtils.info(logger, "RequestController.createRequestNoInfra:custId=" + custId + ";serviceType =" + serviceType + ";Lat =" + Lat + ";Lng =" + Lng + ";locale =" + locale);
            if (custId == null) {
                custId = -1L;
            }
            if (serviceType == null || serviceType.isEmpty()) {
                result.setErrorCode("03");
                result.setErrorDecription("ServiceType is empty");
                return result;
            }
            if (Lat == null) {
                result.setErrorCode("04");
                result.setErrorDecription("Lat is empty");
                return result;
            }
            if (Lng == null) {
                result.setErrorCode("05");
                result.setErrorDecription("Lng is empty");
                return result;
            }

            return new RequestDAOUtil().addRequestNoInfra(cmPosSession, custId, serviceType, Lat, Lng, locale);
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result.setErrorCode("99");
            result.setErrorDecription(ex.getMessage());
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            LogUtils.info(logger, "RequestController.createRequestNoInfra:result=" + LogUtils.toJson(result));
        }
    }

    public CoordinateOut getListCoordinate(HibernateHelper hibernateHelper, Long reqId, String locale) {
        CoordinateOut result = new CoordinateOut("0", "Success");
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPosSession.beginTransaction();

            StringBuilder sql = new StringBuilder(" ");
            sql.append(" SELECT   lat as Lat,");
            sql.append(" lng as Lng,");
            sql.append(" location as location, ");
            sql.append(" order_number as orderNumber");
            sql.append(" FROM   sub_draw_cable");
            sql.append(" WHERE   req_id = ? and status = 1");
            sql.append(" ORDER BY   order_number ASC");
            Query query = cmPosSession.createSQLQuery(sql.toString()).addScalar("Lat", Hibernate.DOUBLE).addScalar("Lng", Hibernate.DOUBLE).addScalar("location", Hibernate.LONG).addScalar("orderNumber", Hibernate.LONG).setResultTransformer(Transformers.aliasToBean(Coordinate.class));
            query.setParameter(0, reqId);
            List<Coordinate> listItem = query.list();
            result.setListCoordinate(listItem);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result.setErrorCode("99");
            result.setErrorDecription(ex.getMessage());
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "RequestController.getListCoordinate:result=" + LogUtils.toJson(result));
        }
    }

    public CoordinateOut getListCoordinateBySubId(HibernateHelper hibernateHelper, Long subId, String locale) {
        CoordinateOut result = new CoordinateOut("0", "Success");
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPosSession.beginTransaction();

            //validate
            if (subId == null) {
                result.setErrorCode("01");
                result.setErrorDecription("subId is empty");
                return result;
            }
            SubReqAdslLlDAO subReqAdslLlDAO = new SubReqAdslLlDAO();
            SubReqAdslLl subReqAdslLl = subReqAdslLlDAO.findBySubId(cmPosSession, subId);
            if (subReqAdslLl == null) {
                result.setErrorCode("02");
                result.setErrorDecription("subReqAdslLl not found");
                return result;
            }

            StringBuilder sql = new StringBuilder(" ");
            sql.append(" SELECT   lat as Lat,");
            sql.append(" lng as Lng,");
            sql.append(" location as location, ");
            sql.append(" order_number as orderNumber");
            sql.append(" FROM   sub_draw_cable");
            sql.append(" WHERE   req_id = ? and status = 1");
            sql.append(" ORDER BY   order_number ASC");
            Query query = cmPosSession.createSQLQuery(sql.toString()).addScalar("Lat", Hibernate.DOUBLE).addScalar("Lng", Hibernate.DOUBLE).addScalar("location", Hibernate.LONG).addScalar("orderNumber", Hibernate.LONG).setResultTransformer(Transformers.aliasToBean(Coordinate.class));
            query.setParameter(0, subReqAdslLl.getId());
            List<Coordinate> listItem = query.list();
            result.setListCoordinate(listItem);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result.setErrorCode("99");
            result.setErrorDecription(ex.getMessage());
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "RequestController.getListCoordinate:result=" + LogUtils.toJson(result));
        }
    }

    public WSRespone saveLocationAgain(HibernateHelper hibernateHelper, String locale, Long subId, List<Coordinate> listCoordinate, Double cableLenght) {
        WSRespone respone = new WSRespone("0", "Success");
        Session cmPosSession = null;
        Session ImSession = null;
        try {
            LogUtils.info(logger, "RequestController.getInvest:subId=" + subId);
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            ImSession = hibernateHelper.getSession(Constants.SESSION_IM);
            cmPosSession.beginTransaction();
            ImSession.beginTransaction();
            //validate
            if (subId == null) {
                respone.setErrorCode("01");
                respone.setErrorDecription("subId is empty");
                return respone;
            }
            if (cableLenght == null) {
                respone.setErrorCode("01");
                respone.setErrorDecription("cableLenght is empty");
                return respone;
            }
            SubReqAdslLlDAO subReqAdslLlDAO = new SubReqAdslLlDAO();
            SubReqAdslLl subReqAdslLl = subReqAdslLlDAO.findBySubId(cmPosSession, subId);
            if (subReqAdslLl == null) {
                respone.setErrorCode("02");
                respone.setErrorDecription("subReqAdslLl not found");
                return respone;
            }

            StringBuilder sql = new StringBuilder(" select nvl(sum(value),0) from app_params where type = 'AVG_PRICE_CABLE' ");
            Query query = ImSession.createSQLQuery(sql.toString());
            Double priceAVG = Double.parseDouble(query.uniqueResult().toString());

            Double amountCable = priceAVG * cableLenght;
            Double amountCableOld;
            if (subReqAdslLl.getCabLen() == null || subReqAdslLl.getCabLen().isEmpty()) {
                amountCableOld = 0D;
            } else {
                amountCableOld = priceAVG * Double.parseDouble(subReqAdslLl.getCabLen());
            }
            Double totalProduct = subReqAdslLl.getAmountProduct() + (amountCable - amountCableOld);

            sql = new StringBuilder(" UPDATE   sub_req_adsl_ll SET cab_len = ?, amount_product = ? WHERE   id = ? ");
            query = cmPosSession.createSQLQuery(sql.toString());
            query.setParameter(0, cableLenght);
            query.setParameter(1, totalProduct);
            query.setParameter(2, subReqAdslLl.getId());
            query.executeUpdate();

            sql = new StringBuilder(" UPDATE   sub_adsl_ll SET cab_len = ?, amount_product = ?  WHERE   sub_id = ? ");
            query = cmPosSession.createSQLQuery(sql.toString());
            query.setParameter(0, cableLenght);
            query.setParameter(1, totalProduct);
            query.setParameter(2, subReqAdslLl.getSubId());
            query.executeUpdate();

            String err = new RequestDAOUtil().saveLocation(cmPosSession, listCoordinate, subReqAdslLl.getId(), 2L);
            if (err != null) {
                respone.setErrorCode("03");
                respone.setErrorDecription(err);
                return respone;
            }
            cmPosSession.beginTransaction().commit();
            return respone;

        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            respone.setErrorCode("1");
            respone.setErrorDecription(ex.getMessage());
            return respone;
        } finally {
            closeSessions(cmPosSession, ImSession);
            LogUtils.info(logger, "RequestController.getInvestBySubID:result=" + LogUtils.toJson(respone));
            return respone;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Tinh xuat dau tu">
    public InvestOut getInvest(HibernateHelper hibernateHelper, String locale, Long reqId, Double cableLenght, Long capacity, Long boxType) {
        InvestOut investOut = new InvestOut("0", "Success");
        Session cmPosSession = null;
        Session ImSession = null;
        try {
            LogUtils.info(logger, "RequestController.getInvest:subId=" + reqId);
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            ImSession = hibernateHelper.getSession(Constants.SESSION_IM);
            cmPosSession.beginTransaction();
            ImSession.beginTransaction();
            //validate
            if (reqId == null) {
                investOut.setErrorCode("01");
                investOut.setErrorDecription("reqId is empty");
                return investOut;
            }
            SubReqAdslLlDAO subReqAdslLlDAO = new SubReqAdslLlDAO();
            SubReqAdslLl subReqAdslLl = subReqAdslLlDAO.findById(cmPosSession, reqId);
            if (subReqAdslLl == null) {
                investOut.setErrorCode("02");
                investOut.setErrorDecription("subReqAdslLl not found");
                return investOut;
            }
            if (cableLenght == null) {

                if (subReqAdslLl.getCabLen() != null) {

                    cableLenght = Double.parseDouble(subReqAdslLl.getCabLen());
                } else {
                    cableLenght = 0D;
                }

            }
            if (capacity == null) {
                if (subReqAdslLl.getIb() != null) {
                    capacity = Long.parseLong(subReqAdslLl.getIb());
                } else {
                    capacity = 0L;
                }

            }
            if (boxType == null) {
                if (subReqAdslLl.getServiceType().equals(Constants.SERVICE_ALIAS_ADSL)) {
                    boxType = 2L;
                } else {
                    boxType = 3L;
                }
            }

            //lay gia dau tu cua hang hoa - cable
            StringBuilder sql = new StringBuilder(" SELECT   stock_model_id as stockModelId  FROM   sub_stock_model_rel  WHERE   serial IS NULL AND status = 1 and sub_id in (select sub_id from sub_req_adsl_ll where id = ?)");
//            Query query = cmPosSession.createSQLQuery(sql.toString()).addScalar("stockModelId", Hibernate.LONG).setResultTransformer(Transformers.aliasToBean(StockModel.class));
//            query.setParameter(0, subId);
//            List<StockModel> listStockModel = query.list();

            Query query = cmPosSession.createSQLQuery(sql.toString());
            query.setParameter(0, reqId);
            List listStockModel = query.list();

            sql = new StringBuilder(" SELECT   nvl(sum(price),0) ");
            sql.append(" FROM   price ");
            sql.append(" WHERE       1=1 ");
            sql.append(" AND TYPE = 69");
            sql.append(" AND price_policy = 1");
            sql.append(" AND sta_date <= SYSDATE");
            sql.append(" AND (end_date >= SYSDATE OR end_date IS NULL)");
            if (listStockModel != null && listStockModel.size() > 0) {

                String listId = null;
                for (int i = 0; i < listStockModel.size(); i++) {
                    if (listId == null) {
                        listId = listStockModel.get(i).toString();
                    } else {
                        listId = listId + "," + listStockModel.get(i);
                    }
                }
                sql.append(" AND stock_model_id in ( ");
                sql.append(listId + ")");
            }
            query = ImSession.createSQLQuery(sql.toString());
            Double amountProduct = Double.parseDouble(query.uniqueResult().toString());

            sql = new StringBuilder(" select nvl(sum(value),0) from app_params where type = 'AVG_PRICE_CABLE' ");
            query = ImSession.createSQLQuery(sql.toString());
            Double priceAVG = Double.parseDouble(query.uniqueResult().toString());

            Double amountCable = priceAVG * cableLenght;
            Double totalProduct = amountCable + amountProduct;

            //lay thong tin gia dau tu mang core
            sql = new StringBuilder(" select nvl(sum(total_amount/total_sub),0) from SUB_INVEST where sub_invest_type = 1 and status = 1");
            query = cmPosSession.createSQLQuery(sql.toString());
            Double subAVG = Double.parseDouble(query.uniqueResult().toString());

            sql = new StringBuilder(" SELECT   nvl(sum(rate),0)");
            sql.append(" FROM   sub_invest_detail");
            sql.append(" WHERE       sub_invest_id IN (SELECT   sub_invest_id");
            sql.append(" FROM   sub_invest");
            sql.append(" WHERE   sub_invest_type = 1 AND status = 1)");
            sql.append(" AND (SELECT   COUNT ( * ) + 1 ");
            sql.append(" FROM   all_tel_service_sub");
            sql.append(" WHERE   status_id = 2 and service_type in ('A','F','L','W')) BETWEEN from_sub and to_sub");
            query = cmPosSession.createSQLQuery(sql.toString());
            Double rate = Double.parseDouble(query.uniqueResult().toString());

            Double totalCore = subAVG * rate;
            //lay thong tin gia dau tu cua hop cap
            sql = new StringBuilder(" SELECT   NVL (SUM (total_amount / total_sub), 0)");
            sql.append(" FROM   (  SELECT   *");
            sql.append(" FROM   sub_invest");
            sql.append(" WHERE       sub_invest_type = ?");
            sql.append(" AND status = 1");
            sql.append(" AND total_sub <= ?");
            sql.append(" ORDER BY   total_sub DESC)");
            sql.append(" WHERE   ROWNUM <= 1");
            query = cmPosSession.createSQLQuery(sql.toString());
            query.setParameter(0, boxType);
            query.setParameter(1, capacity);
            Double totalBox = Double.parseDouble(query.uniqueResult().toString());

            investOut.setAmountBox(totalBox);
            investOut.setAmountCore(totalCore);
            investOut.setAmountProduct(totalProduct);
            //update gia tri vao cac bang

            sql = new StringBuilder(" UPDATE   sub_req_adsl_ll SET amount_box = ?, amount_core = ?, amount_product = ? WHERE   id = ? ");
            query = cmPosSession.createSQLQuery(sql.toString());
            query.setParameter(0, totalBox);
            query.setParameter(1, totalCore);
            query.setParameter(2, totalProduct);
            query.setParameter(3, reqId);
            query.executeUpdate();

            sql = new StringBuilder(" UPDATE   sub_adsl_ll SET amount_box = ?, amount_core = ?, amount_product = ? WHERE   sub_id = ? ");
            query = cmPosSession.createSQLQuery(sql.toString());
            query.setParameter(0, totalBox);
            query.setParameter(1, totalCore);
            query.setParameter(2, totalProduct);
            query.setParameter(3, subReqAdslLl.getSubId());
            query.executeUpdate();

            cmPosSession.getTransaction().commit();

            return investOut;

        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            investOut.setErrorCode("1");
            investOut.setErrorDecription(ex.getMessage());
            return investOut;
        } finally {
            closeSessions(cmPosSession, ImSession);
            LogUtils.info(logger, "RequestController.searchSubRequest:result=" + LogUtils.toJson(investOut));
            return investOut;
        }
    }

    public InvestOut getInvestBySubID(HibernateHelper hibernateHelper, String locale, Long subId) {
        InvestOut investOut = new InvestOut("0", "Success");
        Session cmPosSession = null;
        try {
            LogUtils.info(logger, "RequestController.getInvest:subId=" + subId);
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPosSession.beginTransaction();
            //validate
            if (subId == null) {
                investOut.setErrorCode("01");
                investOut.setErrorDecription("subId is empty");
                return investOut;
            }
            SubAdslLeaselineDAO subAdslLeaselineDAO = new SubAdslLeaselineDAO();
            SubAdslLeaseline subAdslLeaseline = subAdslLeaselineDAO.findById(cmPosSession, subId);
            if (subAdslLeaseline == null) {
                investOut.setErrorCode("02");
                investOut.setErrorDecription("SubAdslLeaseline not found");
                return investOut;
            }
            investOut.setAmountBox(subAdslLeaseline.getAmountBox());
            investOut.setAmountCore(subAdslLeaseline.getAmountCore());
            investOut.setAmountProduct(subAdslLeaseline.getAmountProduct());

            return investOut;

        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            investOut.setErrorCode("1");
            investOut.setErrorDecription(ex.getMessage());
            return investOut;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "RequestController.getInvestBySubID:result=" + LogUtils.toJson(investOut));
            return investOut;
        }
    }
    //</editor-fold>

    public WSRespone changeServiceAtoF(HibernateHelper hibernateHelper, String token, String locale, String account, String loginName, String shopCodeLogin) {
        Session cmSession = null;
        Session cmPreSession = null;
        Session ccSession = null;
        Session imSession = null;
        Session pmSession = null;
        Session paymentSession = null;
        Session biSession = null;
        Session nims = null;
        boolean hasErr = false;
        WSRespone respone = new WSRespone();
        try {
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            pmSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            paymentSession = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            biSession = hibernateHelper.getSession(Constants.SESSION_BI);
            nims = hibernateHelper.getSession(Constants.SESSION_NIMS);
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            ResultBO resultBO = managerTaskDAO.changeServiceAtoF(account, cmSession, biSession, paymentSession, imSession, loginName, shopCodeLogin);
            respone.setErrorDecription(resultBO.getDes() + " - " + resultBO.getParam() + " - " + resultBO.getResult());
        } catch (Exception ex) {
            logger.error("updateTask: " + ex.getMessage());
            respone.setErrorDecription(ex.getMessage());
            hasErr = true;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmSession, cmPreSession, ccSession, imSession, pmSession, paymentSession, biSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            } else {
                commitTransactions(cmSession, cmPreSession, ccSession, imSession, pmSession, paymentSession, biSession);
            }
            closeSessions(cmSession, cmPreSession, ccSession, imSession, pmSession, paymentSession, biSession, nims);
        }
        return respone;
    }
}
