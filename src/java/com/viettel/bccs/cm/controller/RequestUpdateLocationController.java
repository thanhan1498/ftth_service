/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.ApParamBussiness;
import com.viettel.bccs.cm.bussiness.CustomerBussiness;
import com.viettel.bccs.cm.bussiness.RequestUpdateLocationBusiness;
import com.viettel.bccs.cm.bussiness.TokenBussiness;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.ApParamCustom;
import com.viettel.bccs.cm.model.ApParamExtend;
import com.viettel.bccs.cm.model.PotentialCustomer;
import com.viettel.bccs.cm.model.PotentialCustomerHistory;
import com.viettel.bccs.cm.model.PotentialCustomerHistoryDto;
import com.viettel.bccs.cm.model.RequestUpdateCusLocationDetail;
import com.viettel.bccs.cm.model.RequestUpdateLocation;
import com.viettel.bccs.cm.model.Token;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.DateTimeUtils;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.CancelRULInput;
import com.viettel.brcd.ws.model.input.NewImageInput;
import com.viettel.brcd.ws.model.output.ImageRespone;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.PotentialCustomerOut;
import com.viettel.brcd.ws.model.output.RequestUpdateLocationOut;
import com.viettel.brcd.ws.model.output.VBtsAccount;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.im.database.DAO.ShopDAO;
import com.viettel.im.database.DAO.StaffDAO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author duyetdk
 */
public class RequestUpdateLocationController extends BaseController {

    public RequestUpdateLocationController() {
        logger = Logger.getLogger(RequestUpdateLocationController.class);
    }

    public RequestUpdateLocationOut getListRequestUpdateCusLocation(HibernateHelper hibernateHelper, String date, Long status, Long duration, Long pageNo,
            String locale, String token, String createUser, String custName) {
        RequestUpdateLocationOut result = null;
        Session cmPosSession = null;
        Session imPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            Token tokenObj = new TokenBussiness().findByToken(cmPosSession, token);
            Long staffId = null;
            if (tokenObj != null) {
                staffId = tokenObj.getStaffId();
            }
            /*get info of user*/
            List<com.viettel.im.database.BO.Staff> staff = new StaffDAO(imPosSession).findByProperty("staffId", staffId);
            com.viettel.im.database.BO.Shop shop = new ShopDAO(imPosSession).findById(staff.get(0).getShopId());

            if (shop.getProvince() == null || shop.getProvince().trim().isEmpty()) {
                result = new RequestUpdateLocationOut(Constants.ERROR_CODE_1, LabelUtil.getKey("province.does.not.exist", locale));
                return result;
            }

            RequestUpdateLocationBusiness requestUpdateLocationBusiness = new RequestUpdateLocationBusiness();
            Long count = requestUpdateLocationBusiness.count(cmPosSession, DateTimeUtils.toDateddMMyyyy(date), status, duration, "VTC".equals(shop.getProvinceCode()) ? "VTC" : shop.getProvince(), createUser, custName);
            if (count == null || count <= 0L) {
                result = new RequestUpdateLocationOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }

            Integer start = null;
            Integer max = null;
            if (pageNo != null) {
                start = getStart(Constants.PAGE_SIZE, pageNo);
                max = getMax(Constants.PAGE_SIZE, pageNo, count);
            }
            Long totalPage = 0L;
            if (count % Constants.PAGE_SIZE == 0) {
                totalPage = count / Constants.PAGE_SIZE;
            } else if (count % Constants.PAGE_SIZE > 0) {
                totalPage = count / Constants.PAGE_SIZE + 1;
            }
            List<RequestUpdateLocation> lst = requestUpdateLocationBusiness.find(cmPosSession, DateTimeUtils.toDateddMMyyyy(date), status, duration, start, max,
                    "VTC".equals(shop.getProvinceCode()) ? "VTC" : shop.getProvince(), createUser, custName);
            if (lst == null || lst.isEmpty()) {
                result = new RequestUpdateLocationOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            List<RequestUpdateCusLocationDetail> lst_detail = new ArrayList<RequestUpdateCusLocationDetail>();
            for (RequestUpdateLocation re : lst) {
                if (re != null) {
                    lst_detail.add(new RequestUpdateCusLocationDetail(re));
                }
            }
            result = new RequestUpdateLocationOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), totalPage, count, lst_detail);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new RequestUpdateLocationOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            closeSessions(imPosSession);
            //LogUtils.info(logger, "RequestUpdateLocationController.getListRequestUpdateLocation:result=" + LogUtils.toJson(result));
        }
    }

    public WSRespone rejectOrAprroveRUL(HibernateHelper hibernateHelper, CancelRULInput cancelRULInput, String locale, String token) {
        WSRespone result = null;
        Session cmPosSession = null;
        Session imPosSession = null;
        boolean hasErr = false;
        String mess;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            LogUtils.info(logger, "RequestUpdateLocationController.rejectOrAprroveRUL:wsRequest=" + LogUtils.toJson(cancelRULInput));
            Token tokenObj = new TokenBussiness().findByToken(cmPosSession, token);
            Long staffId = null;
            if (tokenObj != null) {
                staffId = tokenObj.getStaffId();
            }
            /*get info of user*/
            List<com.viettel.im.database.BO.Staff> staff = new StaffDAO(imPosSession).findByProperty("staffId", staffId);
            com.viettel.im.database.BO.Shop shop = new ShopDAO(imPosSession).findById(staff.get(0).getShopId());
            if (cancelRULInput == null) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.cancelRul", locale));
                return result;
            }
            if (cancelRULInput.getType() == null || cancelRULInput.getType() <= 0L) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.type", locale));
                return result;
            }
            if (Constants.REJECT.equals(cancelRULInput.getType())) {
                if (cancelRULInput.getDescription() == null || cancelRULInput.getDescription().trim().isEmpty()) {
                    result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.description", locale));
                    return result;
                }
            }
            if (!Constants.REJECT.equals(cancelRULInput.getType()) && !Constants.APPROVED.equals(cancelRULInput.getType())) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid.type", locale));
                return result;
            }
            if (shop.getProvince() == null || shop.getProvince().trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("province.does.not.exist", locale));
                return result;
            }

            //reject or approve
            mess = new RequestUpdateLocationBusiness().rejectOrApproveRUL(cmPosSession, cancelRULInput.getType(), cancelRULInput.getRulId(),
                    staff.get(0).getStaffCode(), shop.getShopCode(), "VTC".equals(shop.getProvinceCode()) ? "VTC" : shop.getProvince(), cancelRULInput.getDescription(), locale);
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, mess);
                }
            }
            commitTransactions(cmPosSession);
            result = new WSRespone(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            closeSessions(imPosSession);
            LogUtils.info(logger, "RequestUpdateLocationController.rejectOrAprroveRUL:result=" + LogUtils.toJson(result));
        }
    }

    public RequestUpdateLocationOut getListTimesOfChange(HibernateHelper hibernateHelper, Long custId, Long pageNo, String locale) {
        RequestUpdateLocationOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            if (custId == null || custId <= 0L) {
                result = new RequestUpdateLocationOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            RequestUpdateLocationBusiness requestUpdateLocationBusiness = new RequestUpdateLocationBusiness();
            Long count = requestUpdateLocationBusiness.countApprovedAndReject(cmPosSession, custId);
            if (count == null || count <= 0L) {
                result = new RequestUpdateLocationOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }

            Integer start = null;
            Integer max = null;
            if (pageNo != null && pageNo >= 0L) {
                start = getStart(Constants.PAGE_SIZE, pageNo);
                max = getMax(Constants.PAGE_SIZE, pageNo, count);
            }
            Long totalPage = 0L;
            if (count % Constants.PAGE_SIZE == 0) {
                totalPage = count / Constants.PAGE_SIZE;
            } else if (count % Constants.PAGE_SIZE > 0) {
                totalPage = count / Constants.PAGE_SIZE + 1;
            }
            List<RequestUpdateLocation> lst = requestUpdateLocationBusiness.findApprovedAndReject(cmPosSession, custId, start, max);
            if (lst == null || lst.isEmpty()) {
                result = new RequestUpdateLocationOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            List<RequestUpdateCusLocationDetail> lst_detail = new ArrayList<RequestUpdateCusLocationDetail>();
            for (RequestUpdateLocation re : lst) {
                if (re != null) {
                    lst_detail.add(new RequestUpdateCusLocationDetail(re));
                }
            }
            result = new RequestUpdateLocationOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), totalPage, count, lst_detail);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new RequestUpdateLocationOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "RequestUpdateLocationController.getListTimesOfChange:result=" + LogUtils.toJson(result));
        }
    }

    public WSRespone approveAll(HibernateHelper hibernateHelper, String locale, String token, String date, Long duration, String createUser, String custName) {
        WSRespone result = null;
        Session cmPosSession = null;
        Session imPosSession = null;
        boolean hasErr = false;
        String mess;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            Token tokenObj = new TokenBussiness().findByToken(cmPosSession, token);
            Long staffId = null;
            if (tokenObj != null) {
                staffId = tokenObj.getStaffId();
            }
            /*get info of user*/
            List<com.viettel.im.database.BO.Staff> staff = new StaffDAO(imPosSession).findByProperty("staffId", staffId);
            com.viettel.im.database.BO.Shop shop = new ShopDAO(imPosSession).findById(staff.get(0).getShopId());
            if (shop.getProvince() == null || shop.getProvince().trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("province.does.not.exist", locale));
                return result;
            }
            //approve all
            mess = new RequestUpdateLocationBusiness().approveAll(cmPosSession, staffId, staff.get(0).getShopId(), "VTC".equals(shop.getProvinceCode()) ? "VTC" : shop.getProvince(), locale,
                    DateTimeUtils.toDateddMMyyyy(date), duration, createUser, custName, staff.get(0).getStaffCode(), shop.getShopCode());
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, mess);
                }
            }
            commitTransactions(cmPosSession);
            result = new WSRespone(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            closeSessions(imPosSession);
            LogUtils.info(logger, "RequestUpdateLocationController.approveAll:result=" + LogUtils.toJson(result));
        }
    }

    public List<NewImageInput> getImageRequestLocationCustomer(HibernateHelper hibernateHelper, Long requestId, String typeImage) {
        List<NewImageInput> result = new ArrayList<NewImageInput>();
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            result = new RequestUpdateLocationBusiness().getImageRequestLocationCustomer(cmPosSession, requestId, typeImage);
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            closeSessions(cmPosSession);
        }
        return result;
    }

    /**
     * @author: duyetdk
     * @since 06/6/2019
     * @des hien thi anh nha KH
     * @param hibernateHelper
     * @param cusId
     * @return
     */
    public ImageRespone viewImageCustomerLocation(HibernateHelper hibernateHelper, String cusId) {
        ImageRespone ImageRespone = new ImageRespone();
        Session cmPosSession = null;
        try {
            if (cusId == null || cusId.trim().isEmpty() || cusId.trim().equals("?")) {
                ImageRespone.setErrorCode("-1");
                ImageRespone.setErrorDecription("cusId is not empty");
                return ImageRespone;
            }
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);

            VBtsAccount vbts = new CustomerBussiness().findCoorByCustId(cmPosSession, Long.valueOf(cusId));

            List<String> lstAccount = new RequestUpdateLocationBusiness().getAccountByCustId(cmPosSession, Long.valueOf(cusId));
            List<String> lstMobile = new RequestUpdateLocationBusiness().getMobileByCustId(cmPosSession, Long.valueOf(cusId));
            List<String> lstHomephone = new RequestUpdateLocationBusiness().getHomephoneByCustId(cmPosSession, Long.valueOf(cusId));

            String homephone = lstHomephone != null && !lstHomephone.isEmpty() ? String.valueOf(lstHomephone.get(0)) : "";
            String mobile = lstMobile != null && !lstMobile.isEmpty() ? String.valueOf(lstMobile.get(0)) : homephone;
            String account = lstAccount != null && !lstAccount.isEmpty() ? String.valueOf(lstAccount.get(0)) : mobile;
//            List<String> lstAccount = new RequestUpdateLocationBusiness().getAccountByCustId(cmPosSession, Long.valueOf(cusId));
//            String account = lstAccount != null && !lstAccount.isEmpty() ? String.valueOf(lstAccount.get(0)) : "";
            List<String> lstData = new RequestUpdateLocationBusiness().getImageLocationCustomer(cmPosSession, Long.valueOf(cusId),
                    vbts != null && vbts.getAccount() != null ? vbts.getAccount() : account);
            if (lstData != null) {
                ImageRespone.setErrorCode(Constants.RESPONSE_SUCCESS);
                ImageRespone.setErrorDecription(Constants.SUCCESS);
                ImageRespone.setData(lstData);
            } else {
                ImageRespone.setErrorCode("-2");
                ImageRespone.setErrorDecription("not found Image");
                return ImageRespone;
            }
        } catch (Exception ex) {
            logger.error("uploadImage: " + ex.getMessage());
            ImageRespone.setErrorCode("-99");
            ImageRespone.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            if (cmPosSession != null) {
                commitTransactions(cmPosSession);
                cmPosSession.close();
            }
        }
        return ImageRespone;
    }

    public PotentialCustomerOut getListPotentialCust(HibernateHelper hibernateHelper, String name, String phoneNumber,
            String address, Long status, Long pageNo, String locale, String token, String province, String kindOfCustomer) {
        PotentialCustomerOut result = null;
        Session cmPosSession = null;
        Session imPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            Token tokenObj = new TokenBussiness().findByToken(cmPosSession, token);
            Long staffId = null;
            if (tokenObj != null) {
                staffId = tokenObj.getStaffId();
            }
            /*get info user login*/
            List<com.viettel.im.database.BO.Staff> staff = new StaffDAO(imPosSession).findByProperty("staffId", staffId);
            if (staff == null || staff.isEmpty()) {
                result = new PotentialCustomerOut(Constants.ERROR_CODE_1, "Staff is not exist");
                return result;
            }

            RequestUpdateLocationBusiness requestUpdateLocationBusiness = new RequestUpdateLocationBusiness();
            Long count = requestUpdateLocationBusiness.countPotentialCust(cmPosSession, name, phoneNumber,
                    address, status, province, kindOfCustomer);
            if (count == null || count <= 0L) {
                result = new PotentialCustomerOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }

            Integer start = null;
            Integer max = null;
            if (pageNo != null) {
                start = getStart(Constants.PAGE_SIZE, pageNo);
                max = getMax(Constants.PAGE_SIZE, pageNo, count);
            }
            Long totalPage = 0L;
            if (count % Constants.PAGE_SIZE == 0) {
                totalPage = count / Constants.PAGE_SIZE;
            } else if (count % Constants.PAGE_SIZE > 0) {
                totalPage = count / Constants.PAGE_SIZE + 1;
            }
            List<PotentialCustomer> lst = requestUpdateLocationBusiness.getListPotentialCust(cmPosSession, start,
                    max, name, phoneNumber, address, status, province, kindOfCustomer);
            if (lst == null || lst.isEmpty()) {
                result = new PotentialCustomerOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            result = new PotentialCustomerOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), totalPage, count, lst);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new PotentialCustomerOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            closeSessions(imPosSession);
            LogUtils.info(logger, "RequestUpdateLocationController.getListPotentialCust:result=" + LogUtils.toJson(result));
        }
    }

    public WSRespone addNewPotentialCust(HibernateHelper hibernateHelper, String locale, String token,
            String name, String phoneNumber, String address, String province, String email,
            String otherOperator, Double fee, String latitude, String longitude,
            String kindOfCustomer, String expectedService, String expectedScale, String serviceUsed) {
        WSRespone result = null;
        Session cmPosSession = null;
        Session imPosSession = null;
        boolean hasErr = false;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            Token tokenObj = new TokenBussiness().findByToken(cmPosSession, token);
            Long staffId = null;
            if (tokenObj != null) {
                staffId = tokenObj.getStaffId();
            }
            /*get info user login*/
            List<com.viettel.im.database.BO.Staff> staff = new StaffDAO(imPosSession).findByProperty("staffId", staffId);
            if (staff == null || staff.isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Staff is not exist");
                return result;
            }
            if (name == null || name.trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Customer name is required");
                return result;
            }
            if (phoneNumber == null || phoneNumber.trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Phone number is required");
                return result;
            }
            if (province == null || province.trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Province is required");
                return result;
            }
            if (address == null || address.trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Address is required");
                return result;
            }
            if (fee < 0D || fee > 9999999D) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Fee is invalid");
                return result;
            }
            if (StringUtils.isEmpty(kindOfCustomer)) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Kind of customer is invalid");
                return result;
            }
            if (StringUtils.isEmpty(expectedService)) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Expected service is invalid");
                return result;
            }
            if (StringUtils.isEmpty(expectedScale)) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Expected scale is invalid");
                return result;
            }
            //Validate phone number
            if (phoneNumber.trim().length() > 3) {
                if (phoneNumber.substring(0, 3).matches("855")) {
                    phoneNumber = phoneNumber.substring(3);
                }
            }
            if (phoneNumber.indexOf("0") == 0) {
                phoneNumber = phoneNumber.substring(1);
            }
            Long phoneNumberFormat = convertStringToLong(phoneNumber);
            if (phoneNumberFormat == null) {
                return new WSRespone(Constants.ERROR_CODE_1, "Phone number is wrong");
            }
            //Check phone number is exist
//            PotentialCustomer cust = new RequestUpdateLocationBusiness().findPotentialCustByPhoneNumber(cmPosSession, phoneNumber);
//            if(cust != null){
//                return new WSRespone(Constants.ERROR_CODE_1, "This phone number was registed");
//            }
            //insert new customer
            new RequestUpdateLocationBusiness().addNewPotentialCust(cmPosSession, name, phoneNumber, address, province,
                    email, otherOperator, fee, latitude, longitude, staff.get(0).getStaffCode(),
                    kindOfCustomer, expectedService, expectedScale, serviceUsed);

            commitTransactions(cmPosSession);
            result = new WSRespone(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            closeSessions(imPosSession);
            LogUtils.info(logger, "RequestUpdateLocationController.addNewPotentialCust:result=" + LogUtils.toJson(result));
        }
    }

    public WSRespone updatePotentialCust(HibernateHelper hibernateHelper, String locale, String token,
            Long custId, String name, String phoneNumber, String address, String province, String email,
            String otherOperator, Double fee, String latitude, String longitude, Long status,
            String kindOfCustomer, String expectedService, String expectedScale, String serviceUsed) {
        WSRespone result = null;
        Session cmPosSession = null;
        Session imPosSession = null;
        boolean hasErr = false;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            Token tokenObj = new TokenBussiness().findByToken(cmPosSession, token);
            Long staffId = null;
            if (tokenObj != null) {
                staffId = tokenObj.getStaffId();
            }
            /*get info user login*/
            List<com.viettel.im.database.BO.Staff> staff = new StaffDAO(imPosSession).findByProperty("staffId", staffId);
            if (staff == null || staff.isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Staff is not exist");
                return result;
            }
            if (custId == null || custId <= 0L) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Cust ID is required");
                return result;
            }
            if (name == null || name.trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Customer name is required");
                return result;
            }
            if (phoneNumber == null || phoneNumber.trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Phone number is required");
                return result;
            }
            if (province == null || province.trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Province is required");
                return result;
            }
            if (address == null || address.trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Address is required");
                return result;
            }
            if (fee < 0D || fee > 9999999D) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Fee is invalid");
                return result;
            }
            if (StringUtils.isEmpty(kindOfCustomer)) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Kind of customer is invalid");
                return result;
            }
            if (StringUtils.isEmpty(expectedService)) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Expected service is invalid");
                return result;
            }
            if (StringUtils.isEmpty(expectedScale)) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Expected scale is invalid");
                return result;
            }
            //Validate phone number
            if (phoneNumber.trim().length() > 3) {
                if (phoneNumber.substring(0, 3).matches("855")) {
                    phoneNumber = phoneNumber.substring(3);
                }
            }
            if (phoneNumber.indexOf("0") == 0) {
                phoneNumber = phoneNumber.substring(1);
            }
            Long phoneNumberFormat = convertStringToLong(phoneNumber);
            if (phoneNumberFormat == null) {
                return new WSRespone(Constants.ERROR_CODE_1, "Phone number is wrong");
            }
//            //Check phone number is exist
//            PotentialCustomer cust = new RequestUpdateLocationBusiness().findPotentialCustByPhoneNumber(cmPosSession, phoneNumber);
//            if(cust != null && !custId.equals(cust.getCustId())){
//                return new WSRespone(Constants.ERROR_CODE_1, "This phone number was registed");
//            }
            //update customer
            new RequestUpdateLocationBusiness().updatePotentialCust(cmPosSession, custId, name, phoneNumber, address,
                    province, email, otherOperator, fee, latitude, longitude, status, staff.get(0).getStaffCode(),
                    kindOfCustomer, expectedService, expectedScale, serviceUsed);

            commitTransactions(cmPosSession);
            result = new WSRespone(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            closeSessions(imPosSession);
            LogUtils.info(logger, "RequestUpdateLocationController.updatePotentialCust:result=" + LogUtils.toJson(result));
        }
    }

    public WSRespone deletePotentialCust(HibernateHelper hibernateHelper, Long custId, String locale, String token) {
        WSRespone result = null;
        Session cmPosSession = null;
        Session imPosSession = null;
        boolean hasErr = false;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            Token tokenObj = new TokenBussiness().findByToken(cmPosSession, token);
            Long staffId = null;
            if (tokenObj != null) {
                staffId = tokenObj.getStaffId();
            }
            /*get info user login*/
            List<com.viettel.im.database.BO.Staff> staff = new StaffDAO(imPosSession).findByProperty("staffId", staffId);
            if (staff == null || staff.isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Staff is not exist");
                return result;
            }
            if (custId == null || custId <= 0L) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Cust ID is required");
                return result;
            }
            //remove customer
            new RequestUpdateLocationBusiness().deletePotentialCust(cmPosSession, custId, staff.get(0).getStaffCode());
            commitTransactions(cmPosSession);
            result = new WSRespone(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            closeSessions(imPosSession);
            LogUtils.info(logger, "RequestUpdateLocationController.updatePotentialCust:result=" + LogUtils.toJson(result));
        }
    }

    //phuonghc 15062020
    public PotentialCustomerOut getListPotentialCustomerHistory(HibernateHelper hibernateHelper, String token, String locale, String custId) {
        PotentialCustomerOut result = new PotentialCustomerOut();
        Session cmPosSession = null;
        Session imPosSession = null;
        boolean hasErr = false;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            Token tokenObj = new TokenBussiness().findByToken(cmPosSession, token);
            Long staffId = null;
            if (tokenObj != null) {
                staffId = tokenObj.getStaffId();
            }
            /*get info user login*/
            List<com.viettel.im.database.BO.Staff> staff = new StaffDAO(imPosSession).findByProperty("staffId", staffId);
            if (staff == null || staff.isEmpty()) {
                result = new PotentialCustomerOut(Constants.ERROR_CODE_1, "Staff is not exist");
                return result;
            }
            RequestUpdateLocationBusiness requestUpdateLocationBusiness = new RequestUpdateLocationBusiness();
            if (StringUtils.isEmpty(custId)) {
                result = new PotentialCustomerOut(Constants.ERROR_CODE_1, "Cust id is invalid");
                return result;
            }
            List<PotentialCustomerHistoryDto> historyList = requestUpdateLocationBusiness.getListHistoryByCustId(cmPosSession, custId);
            result = new PotentialCustomerOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), historyList);
            return result;
        } catch (Exception e) {
            LogUtils.error(logger, e.getMessage(), e);
            result = new PotentialCustomerOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, e.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            closeSessions(imPosSession);
            LogUtils.info(logger, "RequestUpdateLocationController.getListPotentialCustomerHistory:result=" + LogUtils.toJson(result));
        }
    }

    public ParamOut getListParamByParamType(HibernateHelper hibernateHelper, String token, String locale, String paramType) {
        ParamOut result = new ParamOut();
        Session cmPosSession = null;
        Session imPosSession = null;
        boolean hasErr = false;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imPosSession = hibernateHelper.getSession(Constants.SESSION_IM);
            Token tokenObj = new TokenBussiness().findByToken(cmPosSession, token);
            Long staffId = null;
            if (tokenObj != null) {
                staffId = tokenObj.getStaffId();
            }
            /*get info user login*/
            List<com.viettel.im.database.BO.Staff> staff = new StaffDAO(imPosSession).findByProperty("staffId", staffId);
            if (staff == null || staff.isEmpty()) {
                result = new ParamOut(Constants.ERROR_CODE_1, "Staff is not exist");
                return result;
            }
            ApParamBussiness apBusiness = new ApParamBussiness();
            List<ApParam> apParamLevel1 = apBusiness.findByType(cmPosSession, paramType);
            result = new ParamOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            result.setListParamPotentialCustomer(convertFromApParamToApParamCustom(cmPosSession, apBusiness, apParamLevel1));
            return result;
        } catch (Exception e) {
            LogUtils.error(logger, e.getMessage(), e);
            result = new ParamOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, e.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            closeSessions(imPosSession);
            LogUtils.info(logger, "RequestUpdateLocationController.getListPotentialCustomerHistory:result=" + LogUtils.toJson(result));

        }
    }

    private List<ApParamCustom> convertFromApParamToApParamCustom(Session cmPosSession, ApParamBussiness apBusiness, List<ApParam> apParamListLv1) {
        List<ApParamCustom> result = new ArrayList<ApParamCustom>();

        for (ApParam apParam : apParamListLv1) {
            List<ApParam> apParamListLv2 = apBusiness.findByType(cmPosSession, apParam.getParamCode());
            if (apParamListLv2.get(0).getParamType().equals(apParamListLv2.get(0).getParamCode())) {
                ApParamCustom ele = convertNoRelative(apParamListLv2);
                result.add(ele);
            } else {
                //IS OTHER_OPERATOR
                List<ApParamCustom> eleList = convertHaveRelative(apParamListLv2);
                result.addAll(eleList);
            }
        }
        return result;
    }

    private ApParamCustom convertNoRelative(List<ApParam> apParamListLv2) {
        ApParamCustom result = new ApParamCustom();
        List<String> value = new ArrayList<String>();
        for (ApParam element : apParamListLv2) {
            value.add(element.getParamName());
        }
        result.setValue(value);
        result.setCode(apParamListLv2.get(0).getParamCode());
        return result;
    }

    private List<ApParamCustom> convertHaveRelative(List<ApParam> apParamListLv2) {
        List<ApParamCustom> result = new ArrayList<ApParamCustom>();
        Map<String, List<String>> groupOperator = new HashMap<String, List<String>>();
        for (ApParam element : apParamListLv2) {
            if (groupOperator.containsKey(element.getParamCode())) {
                List<String> value = groupOperator.get(element.getParamCode());
                value.add(element.getParamName());
            } else {
                List<String> value = new ArrayList<String>();
                value.add(element.getParamName());
                groupOperator.put(element.getParamCode(), value);
            }
        }

        Set<String> keyParam = groupOperator.keySet();
        for (String key : keyParam) {
            ApParamCustom atom = new ApParamCustom();
            atom.setCode(apParamListLv2.get(0).getParamType());
            atom.setValue(groupOperator.get(key));
            atom.setServiceUsed(key);
            result.add(atom);
        }
        return result;
    }

    /**
     * @author duyetdk
     * @param number
     * @return
     */
    private Long convertStringToLong(String number) {
        Long isdn = null;
        try {
            isdn = Long.parseLong(number);
        } catch (Exception ex) {
            System.out.println(ex);
            return isdn;
        }
        return isdn;
    }
}
