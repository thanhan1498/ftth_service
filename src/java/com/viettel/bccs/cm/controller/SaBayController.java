/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.SabayBussiness;
import com.viettel.bccs.cm.bussiness.ServiceBusiness;
import com.viettel.bccs.cm.model.Owner;
import com.viettel.bccs.cm.model.ServiceInfo;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.output.OwnerOut;
import com.viettel.brcd.ws.model.output.ServiceInfoOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.brcd.ws.supplier.owner_family.sabay.AddGroupSabayOutput;
import com.viettel.brcd.ws.supplier.owner_family.sabay.GetListMemberOwnerSabayOutput;
import com.viettel.brcd.ws.supplier.owner_family.sabay.GetListOwnerSabay;
import com.viettel.brcd.ws.supplier.owner_family.sabay.SabayWsGetInfoBusiness;
import static com.viettel.brcd.ws.supplier.owner_family.sabay.SabayWsGetInfoBusiness.GetListOwnerSabayResponse;
import static com.viettel.brcd.ws.supplier.owner_family.sabay.SabayWsGetInfoBusiness.checkAddGroupN3_P3;
import static com.viettel.brcd.ws.supplier.owner_family.sabay.SabayWsGetInfoBusiness.getListMemberSabay;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import com.viettel.bccs.cm.util.LogUtils;
import static com.viettel.brcd.ws.supplier.owner_family.sabay.SabayWsGetInfoBusiness.AddGroupN3_P3;
import static com.viettel.brcd.ws.supplier.owner_family.sabay.SabayWsGetInfoBusiness.checkOwnerN3_P3;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.HashMap;
import org.hibernate.Query;

/**
 *
 * @author quangdm
 */
public class SaBayController extends BaseController {

    public SaBayController() {
        logger = Logger.getLogger(SaBayController.class);
    }

    public WSRespone sentOTP_Sabay(String isdn) {
        WSRespone response = new WSRespone();

        return response;
    }

    String fixMSISDN(String msisdn) {
        String countryCode = "855";
        msisdn = msisdn.startsWith("0") ? msisdn.substring(1) : msisdn;
        msisdn = msisdn.startsWith(countryCode) ? msisdn.substring(countryCode.length()) : msisdn;
        return msisdn;
    }

    public OwnerOut getLstOwnerSabay(String msisdn_n3_p3, String action_type, String locale) {
        OwnerOut ownerOut = new OwnerOut();
        try {

            AddGroupSabayOutput addGroupSabayOutput = GetListOwnerSabayResponse(ResourceUtils.getResource("userName_API_SaBay", "provisioning"),
                    ResourceUtils.getResource("password_API_SaBay", "provisioning"), "43242", fixMSISDN(msisdn_n3_p3), "ADD",
                    fixMSISDN(msisdn_n3_p3), "", action_type, "0");
            if (addGroupSabayOutput.getErrCode().equals("11")) {
                ownerOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                ownerOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
            } else if (addGroupSabayOutput.getErrCode().equals("0")) {
                ownerOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                ownerOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
                //   System.out.println("listMember: " + addGroupSabayOutput.getListOwner().get(0).getInsertDate().toString());
                ownerOut.setListOwner(addGroupSabayOutput.getListOwner());
            } else if (addGroupSabayOutput.getErrCode().equals("-99")) {
                ownerOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_FAIL);
                ownerOut.setErrorDecription(addGroupSabayOutput.getMessage());
            }

        } catch (Exception ex) {
            logger.error("getSourceType: " + ex.getMessage());
            ownerOut.setErrorCode("-99");
            ownerOut.setErrorDecription("system is busy: " + ex.getMessage());
        }
        return ownerOut;
    }

    public OwnerOut getLstMemberOwnerSabay(String msisdn_n3_p3, String msisdn) {
        OwnerOut ownerOut = new OwnerOut();
        try {
            System.out.println("listMember: " + msisdn_n3_p3 + ":" + msisdn);
            GetListMemberOwnerSabayOutput addGroupSabayOutput = getListMemberSabay(ResourceUtils.getResource("userName_API_SaBay", "provisioning"),
                    ResourceUtils.getResource("password_API_SaBay", "provisioning"),
                    "43242", fixMSISDN(msisdn), "ADD", fixMSISDN(msisdn_n3_p3), fixMSISDN(msisdn_n3_p3), "51", "0");
            if (addGroupSabayOutput.getErrCode().equals("11")) {
                ownerOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                ownerOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
            } else if (addGroupSabayOutput.getErrCode().equals("0")) {
                ownerOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                ownerOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
                ownerOut.setListMember(addGroupSabayOutput.getListMember());
            } else if (addGroupSabayOutput.getErrCode().equals("-99")) {
                ownerOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_FAIL);
                ownerOut.setErrorDecription(addGroupSabayOutput.getMessage());
            }

        } catch (Exception ex) {
            logger.error("getSourceType: " + ex.getMessage());
            ownerOut.setErrorCode("-99");
            ownerOut.setErrorDecription("system is busy: " + ex.getMessage());
        }
        return ownerOut;
    }

    public OwnerOut getOTP_N3_P3(HibernateHelper hibernateHelper, String msisdn_n3_p3,
            String msisdn) {
        OwnerOut ownerOut = new OwnerOut();
        String OTP = String.valueOf(OTP(4));
        // lưu OTP
        Session sessionPos = null;
        try {
            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            LogUtils.info(logger,
                    "getOTP_N3_P3.getNickDomains:sessionPos=" + sessionPos.connection());
            SabayBussiness sabayBussiness = new SabayBussiness();
            sabayBussiness.insert(sessionPos, OTP,fixMSISDN(msisdn));
            commitTransactions(sessionPos);

        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            closeSessions(sessionPos);
            ownerOut.setErrorCode("-99");
            ownerOut.setErrorDecription("system is busy: " + ex.getMessage());
            return ownerOut;
        } finally {
            closeSessions(sessionPos);
            LogUtils.info(logger, "getOTP_N3_P3.getNickDomains:result=" + LogUtils.toJson(ownerOut));
        }
        // sent SMS OTP
        try {
            SmsClient smsClient = new SmsClient();
            smsClient.sendSMS(msisdn, "Your OTP is: " + OTP +". It will expire after 60 seconds");
            ownerOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
            ownerOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
        } catch (Exception ex) {
            LogUtils.info(logger, "SendSMS:result=" + ex.toString());
        }
        return ownerOut;
    }

    public OwnerOut checkOwnerSabay(String msisdn_n3_p3, String msisdn, String locale) {
        OwnerOut ownerOut = new OwnerOut();
        try {
            System.out.println("Add member: " + msisdn_n3_p3 + ":" + msisdn);
            GetListMemberOwnerSabayOutput addGroupSabayOutput = checkAddGroupN3_P3(ResourceUtils.getResource("userName_API_SaBay", "provisioning"),
                    ResourceUtils.getResource("password_API_SaBay", "provisioning"),
                    "43242", fixMSISDN(msisdn), "ADD", fixMSISDN(msisdn_n3_p3), fixMSISDN(msisdn), "51", "0");
            if (addGroupSabayOutput.getErrCode().equals("0")) {
                ownerOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                ownerOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
            } else {
                ownerOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_FAIL);

                System.out.println("action_type: " + addGroupSabayOutput.getMessage() + ":" + LabelUtil.getKey(addGroupSabayOutput.getMessage(), locale) + ":" + locale);
                ownerOut.setErrorDecription(LabelUtil.getKey(addGroupSabayOutput.getMessage(), locale));
            }

        } catch (Exception ex) {
            logger.error("getSourceType: " + ex.getMessage());
            ownerOut.setErrorCode("-99");
            ownerOut.setErrorDecription("system is busy: " + ex.getMessage());
        }
        return ownerOut;
    }

    public OwnerOut checkMemberSabay(String msisdn_n3_p3, String msisdn, String param, String locale) {
        OwnerOut ownerOut = new OwnerOut();
        try {
            String isdnMem = param;
            GetListMemberOwnerSabayOutput addGroupSabayOutput = checkAddGroupN3_P3(ResourceUtils.getResource("userName_API_SaBay", "provisioning"),
                    ResourceUtils.getResource("password_API_SaBay", "provisioning"),
                    "43242", fixMSISDN(msisdn), "ADD", fixMSISDN(msisdn_n3_p3), fixMSISDN(isdnMem), "51", "0");
            if (addGroupSabayOutput.getErrCode().equals("0")) {
                ownerOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                ownerOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
            } else {
                ownerOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_FAIL);
                ownerOut.setErrorDecription(LabelUtil.getKey(addGroupSabayOutput.getMessage(), locale));
            }

        } catch (Exception ex) {
            logger.error("getSourceType: " + ex.getMessage());
            ownerOut.setErrorCode("-99");
            ownerOut.setErrorDecription("system is busy: " + ex.getMessage());
        }
        return ownerOut;
    }

    public OwnerOut sendOTP_N3_P3(HibernateHelper hibernateHelper, String msisdn_n3_p3, String msisdn, String members, String OTP, String locale) {
        OwnerOut ownerOut = new OwnerOut();
        try {
            // kiem tra OTP
            System.out.println("Add member: " + msisdn_n3_p3 + ":" + msisdn);
            Session sessionPos = null;
            try {
                sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
                LogUtils.info(logger, "sendOTP_N3_P3.getNickDomains:sessionPos=" + sessionPos.connection());
                SabayBussiness sabayBussiness = new SabayBussiness();
                if (sabayBussiness.checkOTP(sessionPos, OTP, fixMSISDN(msisdn))) {
                    String[] parts = members.split(",");
                    String strError ="";
                    for (int i = 0; i < parts.length; i++) {
                        String isdnMem = parts[i];
                        GetListMemberOwnerSabayOutput addGroupSabayOutput = AddGroupN3_P3(
                                ResourceUtils.getResource("userName_API_SaBay", "provisioning"),
                                ResourceUtils.getResource("password_API_SaBay", "provisioning"),
                                "43242", fixMSISDN(msisdn), "ADD", fixMSISDN(msisdn_n3_p3), fixMSISDN(isdnMem), "51", "0");
                        if (!addGroupSabayOutput.getErrCode().equals("0")) {
                            ownerOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_FAIL);
                            String errorDecs = addGroupSabayOutput.getMessage();
                            if (errorDecs.contains("MESS_MEM_ADD_ALREADY")) {
                                String[] errS = errorDecs.split(",");
                                if (errS.length == 2) {
                                    errorDecs = LabelUtil.getKey(errS[0], locale).replace("{X}", errS[1]);
                                }
                                System.out.println("error member: " + errorDecs);
                            }
                            else
                                errorDecs=LabelUtil.getKey(addGroupSabayOutput.getMessage(), locale);
                            strError +=isdnMem;
                            strError +="_";
                            strError +=errorDecs;
                            if (i < parts.length - 1) {
                                strError +="|";
                            }

                        }
                        System.out.println("error member: " + strError);
                    }
                    clearSessions(sessionPos);
                    ownerOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                    ownerOut.setErrorDecription(strError==""?com.viettel.bccs.cm.util.Constants.SUCCESS:strError);
                } else {
                    closeSessions(sessionPos);
                    ownerOut.setErrorCode("03");
                    ownerOut.setErrorDecription("OTP invalid");
                    return ownerOut;
                }
            } catch (Throwable ex) {
                LogUtils.error(logger, ex.getMessage(), ex);
                closeSessions(sessionPos);
                ownerOut.setErrorCode("-99");
                ownerOut.setErrorDecription("system is busy: " + ex.getMessage());
                return ownerOut;
            } finally {
                closeSessions(sessionPos);
                LogUtils.info(logger, "sendOTP_N3_P3.getNickDomains:result=" + LogUtils.toJson(ownerOut));
            }

        } catch (Exception ex) {
            logger.error("getSourceType: " + ex.getMessage());
            ownerOut.setErrorCode("-99");
            ownerOut.setErrorDecription("system is busy: " + ex.getMessage());
        }
        return ownerOut;
    }

    static char[] OTP(int len) {
        System.out.println("Generating OTP using random() : ");
        System.out.print("You OTP is : ");

        // Using numeric values
        String numbers = "0123456789";

        // Using random method
        Random rndm_method = new Random();

        char[] otp = new char[len];

        for (int i = 0; i < len; i++) {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            otp[i]
                    = numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return otp;
    }
}
