package com.viettel.bccs.cm.controller;

import com.viettel.bccs.api.salary.SalaryBusiness;
import com.viettel.bccs.cm.database.BO.SalaryStaff;
import com.viettel.bccs.cm.model.SalaryStaffAp;
import com.viettel.bccs.cm.model.SalaryStaffApDetail;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.ws.model.output.SalaryOut;
import com.viettel.brcd.ws.model.output.StaffSalaryDetail;
import com.viettel.brcd.ws.model.output.StaffSalaryDetailInfo;
import com.viettel.brcd.ws.model.output.StaffSalaryTable;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vietnn6
 */
public class SalaryController extends BaseController {

    public SalaryController() {
        logger = Logger.getLogger(SalaryController.class);
    }

    public SalaryOut getNickDomains(HibernateHelper hibernateHelper, String staffId, String type, String locale) {
        SalaryOut result = new SalaryOut();
        Session salarySession = null;
        Session imSession = null;
        try {
            salarySession = hibernateHelper.getSession(Constants.SESSION_SALARY);
            LogUtils.info(logger, "SalaryOut.getNickDomains:salarySession=" + salarySession.connection());
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            LogUtils.info(logger, "SalaryOut.getNickDomains:imSession=" + imSession.connection());
            List<SalaryStaff> lstStaff = new SalaryBusiness().getSalaryOfStaff(salarySession.connection(), imSession.connection(), staffId, type);
            if (lstStaff == null || lstStaff.isEmpty()) {
                result = new SalaryOut(Constants.ERROR_CODE_1, "Data not found");
            } else {
                result.setLstStaff(lstStaff);
                result.setErrorCode(Constants.ERROR_CODE_0);
                result.setErrorDecription(LabelUtil.getKey("common.success", locale));
            }

            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new SalaryOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(salarySession, imSession);
            LogUtils.info(logger, "SalaryOut.getNickDomains:result=" + LogUtils.toJson(result));
        }
    }

    public StaffSalaryDetail getStaffSalaryDetailAPBus(HibernateHelper hibernateHelper, String staffCode, String locale, int month, int year) {
        StaffSalaryDetail result = new StaffSalaryDetail();
        Session salarySession = null;
        try {
            salarySession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            String sql = "from SalaryStaffAp where status = 1 and staffCode = ? and salaryMonth = to_date(?,'dd/mm/yyyy')";
            Query query = salarySession.createQuery(sql);
            query.setParameter(0, staffCode);
            query.setParameter(1, "01/" + String.valueOf(month) + "/" + String.valueOf(year));
            List<SalaryStaffAp> ap = query.list();
            if (ap == null || ap.isEmpty()) {
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription(LabelUtil.getKey("not.found.data", locale));
            } else {
                sql = "from SalaryStaffApDetail where salaryStaffApId = ? order by type, description";
                query = salarySession.createQuery(sql);
                query.setParameter(0, ap.get(0).getId());
                List<SalaryStaffApDetail> detail = query.list();
                if (detail == null || detail.isEmpty()) {
                    result.setErrorCode(Constants.ERROR_CODE_1);
                    result.setErrorDecription(LabelUtil.getKey("not.found.data", locale));
                } else {
                    DecimalFormat decimalFormatter = new DecimalFormat("###,###.##");
                    StaffSalaryDetailInfo detailInfo = new StaffSalaryDetailInfo();
                    List<StaffSalaryDetailInfo> leaf = new ArrayList<StaffSalaryDetailInfo>();
                    List<StaffSalaryDetailInfo> node = new ArrayList<StaffSalaryDetailInfo>();
                    result.setDetail(detailInfo);
                    detailInfo.setLeaf(leaf);
                    detailInfo.setNode(node);
                    detailInfo.setTotal("$" + decimalFormatter.format(ap.get(0).getTotal()));
                    List<StaffSalaryTable> listCol;

                    String titleCol1Name = "Detail";
                    String titleCol2Name = "Value";

                    String col_1 = "1";
                    String col_2 = "2";
                    String col_3 = "3";

                    /*thong tin leaf co dinh*/
                    StaffSalaryDetailInfo temp = new StaffSalaryDetailInfo();
                    listCol = new ArrayList<StaffSalaryTable>();
                    temp.setCol(listCol);
                    listCol.add(new StaffSalaryTable(col_1, titleCol1Name, "Standard work date"));
                    listCol.add(new StaffSalaryTable(col_2, titleCol2Name, ap.get(0).getDateOfMonth().toString()));
                    leaf.add(temp);

                    temp = new StaffSalaryDetailInfo();
                    listCol = new ArrayList<StaffSalaryTable>();
                    temp.setCol(listCol);
                    listCol.add(new StaffSalaryTable(col_1, titleCol1Name, "Work date"));
                    listCol.add(new StaffSalaryTable(col_2, titleCol2Name, ap.get(0).getDateOfWork().toString()));
                    leaf.add(temp);

                    temp = new StaffSalaryDetailInfo();
                    listCol = new ArrayList<StaffSalaryTable>();
                    temp.setCol(listCol);
                    listCol.add(new StaffSalaryTable(col_1, titleCol1Name, "Basic salary"));
                    listCol.add(new StaffSalaryTable(col_2, titleCol2Name, "$" + decimalFormatter.format(ap.get(0).getBasicSalary())));

                    List<StaffSalaryDetailInfo> nodeBasicSalary = new ArrayList<StaffSalaryDetailInfo>();
                    temp.setNode(nodeBasicSalary);

                    StaffSalaryDetailInfo nodeKi = new StaffSalaryDetailInfo();
                    List<StaffSalaryTable> listColBasic = new ArrayList<StaffSalaryTable>();
                    listColBasic.add(new StaffSalaryTable(col_1, "name", "KI"));
                    listColBasic.add(new StaffSalaryTable(col_2, "value", ap.get(0).getKi() + " - " + ap.get(0).getKiValue().toString()));
                    nodeKi.setCol(listColBasic);
                    nodeBasicSalary.add(nodeKi);

                    StaffSalaryDetailInfo nodeSalaryLevel = new StaffSalaryDetailInfo();
                    listColBasic = new ArrayList<StaffSalaryTable>();
                    listColBasic.add(new StaffSalaryTable(col_1, "name", "Salary level"));
                    listColBasic.add(new StaffSalaryTable(col_2, "value", "$" + decimalFormatter.format(ap.get(0).getSalaryLevel())));
                    nodeSalaryLevel.setCol(listColBasic);
                    nodeBasicSalary.add(nodeSalaryLevel);

                    List<StaffSalaryDetailInfo> leafSalaryLevel = new ArrayList<StaffSalaryDetailInfo>();
                    nodeSalaryLevel.setLeaf(leafSalaryLevel);
                    StaffSalaryDetailInfo contract = new StaffSalaryDetailInfo();
                    listColBasic = new ArrayList<StaffSalaryTable>();
                    listColBasic.add(new StaffSalaryTable(col_1, "name", "Contract quantity"));
                    listColBasic.add(new StaffSalaryTable(col_2, "value", ap.get(0).getTotalContract().toString()));
                    contract.setCol(listColBasic);
                    leafSalaryLevel.add(contract);

                    node.add(temp);

                    Map<String, List<SalaryStaffApDetail>> map = new HashMap<String, List<SalaryStaffApDetail>>();
                    for (int i = 0; i < detail.size(); i++) {
                        if (map.containsKey(detail.get(i).getType())) {
                            map.get(detail.get(i).getType()).add(detail.get(i));
                        } else {
                            List<SalaryStaffApDetail> newList = new ArrayList<SalaryStaffApDetail>();
                            newList.add(detail.get(i));
                            map.put(detail.get(i).getType(), newList);
                        }
                    }
                    Map<String, List<SalaryStaffApDetail>> mapSort = new TreeMap<String, List<SalaryStaffApDetail>>(map);
                    for (Map.Entry<String, List<SalaryStaffApDetail>> entry : mapSort.entrySet()) {
                        List<SalaryStaffApDetail> listType = entry.getValue();
                        String type = entry.getKey();
                        //ResourceUtils.getResource("SALE_SERVICE_FOR_MODEL")
                        if (listType.size() == 1) {
                            temp = new StaffSalaryDetailInfo();
                            listCol = new ArrayList<StaffSalaryTable>();
                            temp.setCol(listCol);
                            listCol.add(new StaffSalaryTable(col_1, titleCol1Name, listType.get(0).getType()));
                            listCol.add(new StaffSalaryTable(col_2, titleCol2Name, "$" + decimalFormatter.format(listType.get(0).getMoney())));
                            node.add(temp);
                        } else {
                            temp = new StaffSalaryDetailInfo();
                            List<StaffSalaryDetailInfo> leafLevel2 = new ArrayList<StaffSalaryDetailInfo>();
                            Double total = 0.0;
                            temp.setLeaf(leafLevel2);
                            String tempName = type.replaceAll(" ", "").toLowerCase();
                            tempName = tempName.substring(2, tempName.length());
                            int numCol = Integer.parseInt(ResourceUtils.getResource(tempName + "_num_col"));
                            for (SalaryStaffApDetail detailLv2 : listType) {
                                //commission_num_col      
                                //commission_col_name_1
                                total += detailLv2.getMoney();
                                StaffSalaryDetailInfo leafLv2 = new StaffSalaryDetailInfo();
                                List<StaffSalaryTable> listColLev2 = new ArrayList<StaffSalaryTable>();
                                for (int i = 1; i <= numCol; i++) {
                                    StaffSalaryTable column = new StaffSalaryTable();
                                    column.setOrder(String.valueOf(i));
                                    column.setType(ResourceUtils.getResource(tempName + "_col_name_" + String.valueOf(i)));
                                    if (i == 1) {
                                        column.setValue(detailLv2.getDescription());
                                    } else if (numCol == 2) {
                                        column.setValue("$" + decimalFormatter.format(detailLv2.getMoney()));
                                    } else {
                                        if (i == 2) {
                                            column.setValue(detailLv2.getValue());
                                        } else {
                                            column.setValue("$" + decimalFormatter.format(detailLv2.getMoney()));
                                        }
                                    }
                                    listColLev2.add(column);
                                }
                                leafLv2.setCol(listColLev2);
                                leafLevel2.add(leafLv2);
                            }

                            listCol = new ArrayList<StaffSalaryTable>();
                            temp.setCol(listCol);
                            listCol.add(new StaffSalaryTable(col_1, titleCol1Name, type));
                            listCol.add(new StaffSalaryTable(col_2, titleCol2Name, "$" + decimalFormatter.format(total)));
                            node.add(temp);
                        }
                    }
                }
            }
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new StaffSalaryDetail(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(salarySession);
            LogUtils.info(logger, "SalaryOut.getNickDomains:result=" + LogUtils.toJson(result));
        }
    }
}
