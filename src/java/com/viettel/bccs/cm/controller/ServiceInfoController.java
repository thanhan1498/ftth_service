/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.ServiceBusiness;
import com.viettel.bccs.cm.model.ServiceInfo;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.output.ServiceInfoOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author Nguyen Tran Minh Nhut
 */
public class ServiceInfoController extends BaseController {

    public ServiceInfoController() {
        logger = Logger.getLogger(ServiceInfoController.class);
    }

    public ServiceInfoOut getServices(HibernateHelper hibernateHelper, String local) {
        ServiceInfoOut serviceInfoOut = new ServiceInfoOut();
        List<ServiceInfo> list = new ArrayList<ServiceInfo>();
        try {
            serviceInfoOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
            serviceInfoOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
            ServiceInfo serviceInfo = new ServiceInfo();
            serviceInfo.setServiceCode(com.viettel.bccs.cm.util.Constants.SERVICE_A_F_L);
            serviceInfo.setServiceName(LabelUtil.getKey("service.afl.update.task", local));
            list.add(serviceInfo);
            serviceInfo = new ServiceInfo();
            serviceInfo.setServiceCode(com.viettel.bccs.cm.util.Constants.SERVICE_P);
            serviceInfo.setServiceName(LabelUtil.getKey("service.pstn.update.task", local));
            list.add(serviceInfo);
            serviceInfo = new ServiceInfo();
            serviceInfo.setServiceCode(com.viettel.bccs.cm.util.Constants.SERVICE_DIGITAL);
            serviceInfo.setServiceName(LabelUtil.getKey("service.digital.update.task", local));
            list.add(serviceInfo);
            serviceInfoOut.setLstService(list);
        } catch (Exception ex) {
            logger.error("getSourceType: " + ex.getMessage());
            serviceInfoOut.setErrorCode("-99");
            serviceInfoOut.setErrorDecription("system is busy: " + ex.getMessage());
        }
        return serviceInfoOut;
    }

    public WSRespone configConnectImt(HibernateHelper hibernateHelper, Long staffId, Long taskManagementId, String locale, String splitter) {
        WSRespone response = new WSRespone();
        Session cmPosSession = null;
        Session nimSession = null;
        Session cmPre = null;
        boolean error = false;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            nimSession = hibernateHelper.getSession(Constants.SESSION_NIMS);
            cmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            response = new ServiceBusiness().configConnectImt(cmPosSession, nimSession, cmPre, staffId, taskManagementId, locale, splitter);
        } catch (Exception ex) {
            logger.error("getSourceType: " + ex.getMessage());
            response.setErrorCode(Constants.ERROR_CODE_1);
            response.setErrorDecription(ex.getMessage());
            error = true;
        } finally {
            if (!error) {
                commitTransactions(cmPosSession, cmPre);
            } else {
                rollBackTransactions(cmPosSession, cmPre);
            }
            closeSessions(cmPosSession, cmPre, nimSession);
        }
        return response;
    }

    public WSRespone sendNimsSubActiveInfo(HibernateHelper hibernateHelper, Long staffId, Long taskManagementId) {
        WSRespone response = new WSRespone();
        Session cmPosSession = null;
        Session nimSession = null;
        boolean error = false;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            nimSession = hibernateHelper.getSession(Constants.SESSION_NIMS);
            response = new ServiceBusiness().sendNimsSubActiveInfo(cmPosSession, nimSession, staffId, taskManagementId);
        } catch (Exception ex) {
            logger.error("getSourceType: " + ex.getMessage());
            error = true;
        } finally {
            if (!error) {
                commitTransactions(cmPosSession);
            } else {
                rollBackTransactions(cmPosSession);
            }
            closeSessions(cmPosSession);
            closeSessions(nimSession);
        }
        return response;
    }

    public WSRespone changeStationInfraStructure(HibernateHelper hibernateHelper, Long staffId, String newStation, String newSplitter, String portNo, String account) {
        WSRespone response = new WSRespone();
        Session cmPosSession = null;
        Session nimSession = null;
        boolean error = false;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            nimSession = hibernateHelper.getSession(Constants.SESSION_NIMS);
            response = new ServiceBusiness().changeStationInfraStructure(cmPosSession, nimSession, staffId, newStation, newSplitter, portNo, account);
        } catch (Exception ex) {
            logger.error("getSourceType: " + ex.getMessage());
            error = true;
        } finally {
            if (!error) {
                commitTransactions(cmPosSession);
            } else {
                rollBackTransactions(cmPosSession);
            }
            closeSessions(cmPosSession);
            closeSessions(nimSession);
        }
        return response;
    }
}
