/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.CustomerBussiness;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import sendmt.MtStub;

/**
 *
 * @author quangdm
 */
public final class SmsClient  extends BaseController {
     private MtStub mtStub = null;
     public static String SENDER;
     public SmsClient() {
        logger = Logger.getLogger(SmsClient.class);
        MtStub tempMtStub = initMtStub();
        setMtStub(tempMtStub);
     }
     private MtStub initMtStub() {
        String url = ResourceUtils.getResource("URL_SMS", "sms");
        logger.info(url);
        SENDER =ResourceUtils.getResource("SMS_ISDN_SEND", "sms");
        logger.info(SENDER);
        String userName =ResourceUtils.getResource("USER_NAME_SMS", "sms");
        logger.info(userName);
        String password =ResourceUtils.getResource("PASSWORD_SMS", "sms");
        logger.info(password);
        String xmnlsSms =ResourceUtils.getResource("XMNLS_SMS", "sms");
        logger.info(xmnlsSms);

        MtStub stub = new MtStub(url, xmnlsSms, userName, password);
        return stub;

    }

    public MtStub getMtStub() {
        return mtStub;
    }

    public void setMtStub(MtStub mtStub) {
        this.mtStub = mtStub;
    }
    private int sendSMS(String content) throws Exception {
        return -1;//sendSMS(Constant.RECEIPVER, content);
    }

    public int sendSMS(String isdn, String content) throws Exception {
        String msisdn =getMsisdn(isdn);
        String isdnSend = SENDER;
        return sendSMS(msisdn, content, isdnSend);
    }
    public String getMsisdn(String number) {
        number = getIsdn(number);
        if (!number.isEmpty()) {
            return "855" + number;
        }
        return number;
    }
    public String getIsdn(String number) {
           number = StringUtils.trimToEmpty(number);
           if (number.isEmpty()) {
               return number;
           }
           while (number.indexOf("0") == 0) {
               number = number.substring(1);
           }
           if (number.length() > 3 && number.indexOf("855") == 0) {
               return number.substring(3);
           }
           return number;
       }

    public int sendSMS(String isdn, String content, String sender) throws Exception {
        return sendSMS(isdn, content, sender,"VCI");
    }

    public int sendSMS(String isdn, String content, String sender, String serviceId) throws Exception {
        try {

            if (isdn == null || isdn.trim().length() == 0) {
                return -1;
            }

            if (content == null || content.trim().length() == 0) {
                return -1;
            }

            if (sender == null || sender.trim().length() == 0) {
                return -1;
            }

            if (serviceId == null || serviceId.trim().length() == 0) {
                return -1;
            }

            logger.info("Start Send SMS to isdn : " + isdn + " with content : " + content);
            MtStub stub = getMtStub();
            int statusSend = stub.send("0", serviceId, sender,
                    isdn, "0", content, "1");

            logger.info("Status send when return: " + statusSend);
            return statusSend;

        } catch (Exception e) {
            logger.error("Exception at sendSMS : " + e.getMessage(), e);
            throw e;
        }
    }
     public WSRespone checkIdNoCus(HibernateHelper hibernateHelper, String idNo) {
        WSRespone reponse = new WSRespone();
        Session sessionPos = null;
        try {
            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            //<editor-fold defaultstate="collapsed" desc="idNo">
            if (idNo == null || idNo.isEmpty()) {
                reponse.setErrorCode(Constants.ERROR_CODE_1);
                reponse.setErrorDecription("IdNo is not null");
                return reponse;
            }

            if (new CustomerBussiness().isUsedIdNo(sessionPos, idNo)) {
                reponse.setErrorCode(Constants.ERROR_CODE_1);
                reponse.setErrorDecription("This idNo exists already");
                return reponse;
            }
            //</editor-fold>
            reponse.setErrorCode(Constants.ERROR_CODE_0);
            reponse.setErrorDecription("Success");
            return reponse;
        } catch (Exception ex) {
            logger.error("checkIdNoCus: " + ex.getMessage());
            reponse.setErrorCode(Constants.ERROR_CODE_1);
            reponse.setErrorDecription("Error");
        } finally {
            commitTransactions(sessionPos);
            closeSessions(sessionPos);
        }
        return reponse;
    }

    public int sendErrorToIsdn(String content) throws Exception {

        return -1;//sendSMS(Constant.RECEIPVER, content);
    }
}
