/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.model.SourceType;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.output.SourceTypeOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author User-PC
 */
public class SourceTypeController extends BaseController {

    public SourceTypeController() {
        logger = Logger.getLogger(SourceTypeController.class);
    }

    public SourceTypeOut getSourceType(HibernateHelper hibernateHelper, int type, String local) {
        SourceTypeOut sourceTypeOutput = new SourceTypeOut();
        List<SourceType> list = new ArrayList<SourceType>();
        SourceType sourceType;
        try {
            switch (type) {
                case 1:
                    sourceTypeOutput.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                    sourceTypeOutput.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
                    sourceType = new SourceType();
                    sourceType.setCode(com.viettel.bccs.cm.util.Constants.TASK_DEPLOY);
                    sourceType.setDescription(LabelUtil.getKey("task.source.type.new", local)); 
                    list.add(sourceType);
                    sourceType = new SourceType();
                    sourceType.setCode(com.viettel.bccs.cm.util.Constants.TASK_TREAT);
                    sourceType.setDescription(LabelUtil.getKey("task.source.type.treat", local)); 
                    list.add(sourceType);
                    sourceTypeOutput.setListSourceType(list);
                    break;
                default:
                    sourceTypeOutput.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                    sourceTypeOutput.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
                    sourceType = new SourceType();
                    sourceType.setCode(com.viettel.bccs.cm.util.Constants.TASK_DEPLOY);
                    sourceType.setDescription(LabelUtil.getKey("task.source.type.new", local));
                    list.add(sourceType);
                    sourceType = new SourceType();
                    sourceType.setCode(com.viettel.bccs.cm.util.Constants.TASK_TREAT);
                    sourceType.setDescription(LabelUtil.getKey("task.source.type.treat", local));
                    list.add(sourceType);
                    sourceType = new SourceType();
                    sourceType.setCode(com.viettel.bccs.cm.util.Constants.TASK_SURVEY);
                    sourceType.setDescription(LabelUtil.getKey("task.source.type.survey", local));
                    list.add(sourceType);
                    sourceTypeOutput.setListSourceType(list);
                    break;
            }
        } catch (Exception ex) {
            logger.error("getSourceType: " + ex.getMessage());
            sourceTypeOutput.setErrorCode("-99");
            sourceTypeOutput.setErrorDecription("system is busy: " + ex.getMessage());
        }
        return sourceTypeOutput;
    }
}
