/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.bccs.api.Task.BO.Staff;
import com.viettel.bccs.api.Task.DAO.StaffDAO;
import com.viettel.bccs.cm.model.StaffInfo;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.ws.model.output.StaffInfoOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author Nguyen Tran Minh Nhut
 */
public class StaffInfoController extends BaseController {

    String local;

    public StaffInfoController() {
        logger = Logger.getLogger(StaffInfoController.class);
    }

    public StaffInfoOut getStaffInfos(HibernateHelper hibernateHelper, Long shopId, String local) {
        StaffInfoOut staffInfoOut = new StaffInfoOut();
        List<StaffInfo> list = new ArrayList<StaffInfo>();
        Session sessionPos = null;
        try {
            local = this.local;
            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            StaffDAO staffDAO = new StaffDAO();
            StaffInfo staffInfo = null;
            List<Staff> listStaff = staffDAO.findByShopId(shopId, sessionPos);
            if (listStaff != null && listStaff.size() > 0) {
                for (Staff staff : listStaff) {
                    staffInfo = new StaffInfo();
                    staffInfo.setStaffCode(staff.getStaffCode());
                    staffInfo.setStaffName(staff.getName());
                    staffInfo.setStaffId(staff.getStaffId());
                    list.add(staffInfo);
                }
            }
            staffInfoOut.setLstStaffInfo(list);
            staffInfoOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
            staffInfoOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
        } catch (Exception ex) {
            logger.error("getSourceType: " + ex.getMessage());
            staffInfoOut.setErrorCode("-99");
            staffInfoOut.setErrorDecription("system is busy: " + ex.getMessage());
        } finally {
            if (sessionPos != null) {
                closeSessions(sessionPos);
            }
        }
        return staffInfoOut;
    }

    public StaffInfoOut getStaffInfos(HibernateHelper hibernateHelper, Long shopId) {
        StaffInfoOut staffInfoOut = new StaffInfoOut();
        List<StaffInfo> list = new ArrayList<StaffInfo>();
        Session sessionPos = null;
        try {
            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            StaffDAO staffDAO = new StaffDAO();
            StaffInfo staffInfo = null;
            List<Staff> listStaff = staffDAO.findByShopId1(shopId, sessionPos);
            if (listStaff != null && listStaff.size() > 0) {
                for (Staff staff : listStaff) {
                    staffInfo = new StaffInfo();
                    staffInfo.setStaffCode(staff.getStaffCode());
                    staffInfo.setStaffName(staff.getName());
                    staffInfo.setStaffId(staff.getStaffId());
                    staffInfo.setShopCode(staff.getStaffCode());
                    staffInfo.setIsdn(staff.getTel());
                    staffInfo.setShopId(staff.getShopId().toString());
                    staffInfo.setShopCode(staff.getShopCode());
                    staffInfo.setShopName(staff.getShopName());
                    list.add(staffInfo);
                }
            }
            staffInfoOut.setLstStaffInfo(list);
            staffInfoOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
            staffInfoOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
        } catch (Exception ex) {
            logger.error("getSourceType: " + ex.getMessage());
            staffInfoOut.setErrorCode("-99");
            staffInfoOut.setErrorDecription("system is busy: " + ex.getMessage());
        }finally {
            if (sessionPos != null) {
                closeSessions(sessionPos);
            }
        }
        return staffInfoOut;
    }
}
