package com.viettel.bccs.cm.controller;

import com.viettel.bccs.api.Task.DAO.StockModelDAO;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.ws.model.output.SerialOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.im.database.BO.StockTransSerial;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class StockModelController extends BaseController {

    public StockModelController() {
        logger = Logger.getLogger(StockModelController.class);
    }

    public SerialOut getRangeSerial(HibernateHelper hibernateHelper, Long shopId, Long stockModelId) {
        SerialOut reponse = new SerialOut();
        Session sessionIM = null;
        Session sessionCM = null;
        try {
            sessionIM = hibernateHelper.getSession(Constants.SESSION_IM);
            sessionCM = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            if (shopId == null || shopId == 0) {
                reponse.setErrorCode(Constants.ERROR_CODE_1);
                reponse.setErrorDecription("shopId is not null");
                return reponse;
            }
            if (stockModelId == null || stockModelId == 0) {
                reponse.setErrorCode(Constants.ERROR_CODE_1);
                reponse.setErrorDecription("stockModelId is not null");
                return reponse;
            }
            // get info sim prepaid
            List<StockTransSerial> lstSerial = StockModelDAO.getRangeSerial(sessionIM, sessionCM, shopId, stockModelId);
            if (lstSerial == null || lstSerial.isEmpty()) {
                reponse.setErrorCode(Constants.ERROR_CODE_2);
                reponse.setErrorDecription("No data found");
                return reponse;
            }
            reponse.setErrorCode(Constants.ERROR_CODE_0);
            reponse.setErrorDecription("Success");
            reponse.setLstSerial(lstSerial);
        } catch (Exception ex) {
            logger.error("getCodeAuthenCusByIsdn: " + ex.getMessage());
            reponse.setErrorCode(Constants.ERROR_CODE_1);
            reponse.setErrorDecription("Error");
        } finally {
            closeSessions(sessionIM, sessionCM);
        }
        return reponse;
    }
}
