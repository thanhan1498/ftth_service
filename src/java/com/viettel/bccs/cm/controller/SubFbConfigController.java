/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.google.gson.Gson;
import com.viettel.bccs.cm.bussiness.ApParamBussiness;
import com.viettel.bccs.cm.bussiness.SubFbConfigBusiness;
import com.viettel.bccs.cm.bussiness.TokenBussiness;
import com.viettel.bccs.cm.dao.SubDeploymentDAO;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.StaffDetail;
import com.viettel.bccs.cm.model.SubDeployment;
import com.viettel.bccs.cm.model.SubFbConfig;
import com.viettel.bccs.cm.model.SubFbConfigDetail;
import com.viettel.bccs.cm.model.SubFbConfigLog;
import com.viettel.bccs.cm.model.Token;
import com.viettel.bccs.cm.supplier.ServiceSupplier;
import com.viettel.bccs.cm.supplier.SubFbConfigSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.CancelSubFbConfigInput;
import com.viettel.brcd.ws.model.output.HistorySubFbConfigOut;
import com.viettel.brcd.ws.model.output.LastUpdateConfigOut;
import com.viettel.brcd.ws.model.output.StaffDetailOut;
import com.viettel.brcd.ws.model.output.SubFbConfigOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author duyetdk
 */
public class SubFbConfigController extends BaseController {

    public SubFbConfigController() {
        logger = Logger.getLogger(SubFbConfigController.class);
    }

    public SubFbConfigOut getListSubFbConfig(HibernateHelper hibernateHelper, String token, Long status, Long statusConfig, Long duration, String account, String createUser, String locale, Long pageNo, String province) {
        SubFbConfigOut result = null;
        Session cmPosSession = null;
        Session nimsSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            nimsSession = hibernateHelper.getSession(Constants.SESSION_NIMS);
            SubFbConfigBusiness subFbConfigBusiness = new SubFbConfigBusiness();
            Long count = subFbConfigBusiness.count(cmPosSession, status, statusConfig, duration, createUser, account, province);
            if (count == null || count <= 0L) {
                result = new SubFbConfigOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            Integer start = null;
            Integer max = null;
            if (pageNo != null) {
                start = getStart(Constants.PAGE_SIZE, pageNo);
                max = getMax(Constants.PAGE_SIZE, pageNo, count);
            }
            Long totalPage = 0L;
            if (count % Constants.PAGE_SIZE == 0) {
                totalPage = count / Constants.PAGE_SIZE;
            } else if (count % Constants.PAGE_SIZE > 0) {
                totalPage = count / Constants.PAGE_SIZE + 1;
            }
            List<SubFbConfigDetail> lst = subFbConfigBusiness.findAll(cmPosSession, status, statusConfig, duration, start, max,
                    createUser, account, province);
            List<SubFbConfigDetail> lst_new = new ArrayList<SubFbConfigDetail>();
            Long connectorId = null;
            String connectorName = "";
            for (SubFbConfigDetail sub : lst) {
                if (sub != null) {
                    List<SubDeployment> subDep = new SubDeploymentDAO().findBySubId(cmPosSession, sub.getSubId());
                    if (subDep != null) {
                        if (Constants.IN_FRATYPE_GPON.equals(subDep.get(0).getInfraType())) {
                            connectorId = subDep.get(0).getSplitterId();
                        } else {
                            connectorId = subDep.get(0).getCableBoxId();
                        }
                        connectorName = new ServiceSupplier().getConnectorName(nimsSession, connectorId, subDep.get(0).getInfraType());
                    }
                    lst_new.add(new SubFbConfigDetail(sub, connectorName));
                }
            }
            result = new SubFbConfigOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), totalPage, count, lst_new);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new SubFbConfigOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            closeSessions(nimsSession);
            LogUtils.info(logger, "SubFbConfigController.getListSubFbConfig:result=" + LogUtils.toJson(result));
        }
    }

    public WSRespone rejectOrApproveSubFbConfig(HibernateHelper hibernateHelper, CancelSubFbConfigInput cancelSubFbConfigInput, String locale, String token) {
        WSRespone result = null;
        Session cmPosSession = null;
        Session cmPre = null;
        boolean hasErr = false;
        String mess;
        StringBuilder request = new StringBuilder();
        String userUpdate = "";
        SubFbConfig subFbConfig = new SubFbConfig();
        StringBuilder action = new StringBuilder();
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            LogUtils.info(logger, "SubFbConfigController.rejectOrApproveSubFbConfig:wsRequest=" + LogUtils.toJson(cancelSubFbConfigInput));
            Token tokenObj = new TokenBussiness().findByToken(cmPosSession, token);
            if (tokenObj != null) {
                userUpdate = tokenObj.getUserName();
            } else {
                userUpdate = cancelSubFbConfigInput.getStaffCode();
            }
            if (cancelSubFbConfigInput == null) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.cancelSubFbConfigInput", locale));
                return result;
            }
            if (cancelSubFbConfigInput.getType() == null || cancelSubFbConfigInput.getType() <= 0L) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.type", locale));
                return result;
            }
            if (Constants.REJECT.equals(cancelSubFbConfigInput.getType())) {
                if (cancelSubFbConfigInput.getDescription() == null || cancelSubFbConfigInput.getDescription().trim().isEmpty()) {
                    result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.description", locale));
                    return result;
                }
                if (cancelSubFbConfigInput.getReasonCode() == null || cancelSubFbConfigInput.getReasonCode().trim().isEmpty()) {
                    result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.reasonCode", locale));
                    return result;
                }
            }
            if (!Constants.REJECT.equals(cancelSubFbConfigInput.getType()) && !Constants.APPROVED.equals(cancelSubFbConfigInput.getType())) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid.type", locale));
                return result;
            }
            /*reject or approve*/
            mess = new SubFbConfigBusiness().rejectOrApproveSubFbConfig(cmPosSession, cmPre, cancelSubFbConfigInput.getType(), cancelSubFbConfigInput.getId(),
                    userUpdate, cancelSubFbConfigInput.getReasonCode(), cancelSubFbConfigInput.getDescription(), locale, request, subFbConfig, action);
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    hasErr = true;
                    result = new WSRespone(Constants.ERROR_CODE_1, mess);
                }
            }
            if (!hasErr) {

                result = new WSRespone(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            }
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession, cmPre);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            } else {
                if (cancelSubFbConfigInput != null && cancelSubFbConfigInput.getId() != null) {
                    new SubFbConfigSupplier().insertLog(cmPosSession, cancelSubFbConfigInput.getId(), subFbConfig.getStatusConf(), request.toString(), new Gson().toJson(result), action.toString(), result.getErrorDecription(), new Date(), userUpdate, "0");
                }
                commitTransactions(cmPosSession, cmPre);
            }
            closeSessions(cmPosSession, cmPre);
            LogUtils.info(logger, "SubFbConfigController.rejectOrApproveSubFbConfig:result=" + LogUtils.toJson(result));
        }
        return result;
    }

    public LastUpdateConfigOut getLastUpdateConfig(HibernateHelper hibernateHelper, String token, Long taskMngtId, String account, String locale) {
        LastUpdateConfigOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            SubFbConfigBusiness subFbConfigBusiness = new SubFbConfigBusiness();
            SubFbConfig sub = subFbConfigBusiness.findByTaskMngtIdAndAccount(cmPosSession, taskMngtId, account, Constants.TYPE_CONF_IMT);
            if (sub == null) {
                result = new LastUpdateConfigOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            ApParam ap = new ApParamBussiness().findReason(cmPosSession, sub.getReasonCode());
            String reasonName = null;
            if (ap != null) {
                reasonName = ap.getParamValue();
            }
            result = new LastUpdateConfigOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), String.valueOf(sub.getStatus()),
                    sub.getReasonCode(), reasonName, sub.getDescription(), sub.getCreateUser(), String.valueOf(sub.getCreateDate()));
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new LastUpdateConfigOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "SubFbConfigController.getLastUpdateConfig:result=" + LogUtils.toJson(result));
        }
    }

    public HistorySubFbConfigOut getListHistoryConfig(HibernateHelper hibernateHelper, String token, Long id, String locale) {
        HistorySubFbConfigOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<SubFbConfigLog> lst = new SubFbConfigBusiness().findAllHistoryById(cmPosSession, id);
            if (lst == null || lst.isEmpty()) {
                result = new HistorySubFbConfigOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }

            result = new HistorySubFbConfigOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), lst);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new HistorySubFbConfigOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "SubFbConfigController.getListHistoryConfig:result=" + LogUtils.toJson(result));
        }
    }

    public StaffDetailOut getStaffInfo(HibernateHelper hibernateHelper, String token, String staffCode, String locale) {
        StaffDetailOut result = null;
        Session imSession = null;
        try {
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            SubFbConfigBusiness subFbConfigBusiness = new SubFbConfigBusiness();
            StaffDetail staff = subFbConfigBusiness.findStaffByStaffCode(imSession, staffCode);
            if (staff == null) {
                result = new StaffDetailOut(Constants.ERROR_CODE_0, LabelUtil.getKey("not.found.data", locale));
                return result;
            }
            result = new StaffDetailOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), staff.getName(),
                    staff.getPhone(), staff.getTeam(), staff.getProvince());
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new StaffDetailOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(imSession);
            LogUtils.info(logger, "SubFbConfigController.getStaffInfo:result=" + LogUtils.toJson(result));
        }
    }
}
