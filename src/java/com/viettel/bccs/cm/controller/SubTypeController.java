package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.SubTypeBussiness;
import com.viettel.bccs.cm.model.SubType;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.model.output.SubTypeOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubTypeController extends BaseController {

    public SubTypeController() {
        logger = Logger.getLogger(SubTypeController.class);
    }

    public SubTypeOut getSubTypes(HibernateHelper hibernateHelper, String productCode, String serviceType, String locale) {
        SubTypeOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<SubType> subTypes = new SubTypeBussiness().findByMap(cmPosSession, serviceType, productCode);
            result = new SubTypeOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), subTypes);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new SubTypeOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "SubTypeController.getSubTypes:result=" + LogUtils.toJson(result));
        }
    }
}
