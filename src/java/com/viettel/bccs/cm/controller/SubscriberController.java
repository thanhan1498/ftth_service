package com.viettel.bccs.cm.controller;

import com.viettel.bccs.api.cm.pre.ConnecMobilePrePaid;
import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.api.common.InterfaceCmInventory;
import com.viettel.bccs.bean.common.MessageError;
import com.viettel.bccs.cm.bussiness.BaseBussiness;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.bussiness.SubscriberBussiness;
import com.viettel.bccs.cm.common.util.pre.Constant;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.pre.CustomerPre;
import com.viettel.bccs.cm.model.im.IsdnRestore;
import com.viettel.bccs.cm.model.pre.SubMbPre;
import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.bccs.cm.supplier.ConnectSubscriberSupplier;
import com.viettel.bccs.cm.supplier.RegisterSupplier;
import com.viettel.brcd.common.util.FileSaveProcess;
import com.viettel.brcd.ws.model.input.ActivePreIn;
import com.viettel.brcd.ws.model.input.ActiveSubscriberPreIn;
import com.viettel.brcd.ws.model.input.ChangeSimIn;
import com.viettel.brcd.ws.model.input.ImageInput;
import com.viettel.brcd.ws.model.input.WSRequest;
import com.viettel.brcd.ws.model.output.ConnectSubscriberResponse;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.ReasonChangeSimOut;
import com.viettel.brcd.ws.model.output.ReasonOut;
import com.viettel.brcd.ws.model.output.SubscriberRestoreInfo;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.im.database.DAO.StaffDAO;
import com.viettel.pm.database.BO.ProductOffer;
import com.viettel.pm.database.DAO.PMAPI;
import java.sql.CallableStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import com.viettel.bccs.cm.model.pre.ReasonPre;
import com.viettel.bccs.cm.model.pre.SubRelProductPre;
import com.viettel.bccs.cm.supplier.SubFbConfigSupplier;
import com.viettel.brcd.common.util.InterfacePr;
import com.viettel.brcd.dao.pre.ActionLog;
import com.viettel.brcd.dao.pre.ApParamDAO;
import com.viettel.brcd.dao.pre.CustomerPreDAO;
import com.viettel.brcd.dao.pre.CommonPre;
import com.viettel.brcd.ws.model.input.ChangeSimInput;
import com.viettel.common.OriginalViettelMsg;
import com.viettel.common.ViettelService;
import com.viettel.im.database.BO.StockSim;
import com.viettel.im.database.BO.StockSimBean;
import com.viettel.im.database.DAO.WebServiceIMDAO;
import java.util.ArrayList;

/**
 * @author vanghv1
 */
public class SubscriberController extends BaseController {

    public SubscriberController() {
        logger = Logger.getLogger(SubscriberController.class);
    }

    public WSRespone activeSubscriber(HibernateHelper hibernateHelper, WSRequest wsRequest, String locale) {
        WSRespone result = null;
        Session cmPosSession = null;
        Session cmPreSession = null;
        Session imSession = null;
        Session pmSession = null;
        Session paymentSession = null;
        Session ccSession = null;
        Session nimsSession = null;
        boolean hasErr = false;
        try {
            LogUtils.info(logger, "SubscriberController.activeSubscriber:wsRequest=" + LogUtils.toJson(wsRequest));
            if (wsRequest == null) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid", locale));
                return result;
            }
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            pmSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            paymentSession = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            nimsSession = hibernateHelper.getSession(Constants.SESSION_NIMS);
            String mess = new SubscriberBussiness().activeSubscriber(cmPosSession, cmPreSession, imSession, pmSession, paymentSession, ccSession, nimsSession, wsRequest.getReqId(), wsRequest.getShopId(),
                    wsRequest.getStaffId(), wsRequest.getCustId(), wsRequest.getAccount(), locale, wsRequest.getDateDeploy(), wsRequest.getReasonDelay());//duyetdk
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, mess);
                }
            }
            commitTransactions(cmPosSession, cmPreSession, imSession, pmSession, paymentSession, ccSession);
            result = new WSRespone(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession, cmPreSession, imSession, pmSession, paymentSession, ccSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession, cmPreSession, imSession, pmSession, paymentSession, ccSession, nimsSession);
            LogUtils.info(logger, "SubscriberController.activeSubscriber:result=" + LogUtils.toJson(result));
        }
    }

    public WSRespone activeSubscriberPre(HibernateHelper hibernateHelper, ActivePreIn wsRequest, String locale) {
        WSRespone result = null;
        Session anyPaySession = null;
        Session cmPosSession = null;
        Session cmPreSession = null;
        Session imSession = null;
        Session productSession = null;
        boolean hasErr = false;
        try {
            LogUtils.info(logger, "SubscriberController.activeSubscriberPre:wsRequest=" + LogUtils.toJson(wsRequest));
            if (wsRequest == null) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid", locale));
                return result;
            }
            anyPaySession = hibernateHelper.getSession(Constants.SESSION_ANY_PAY);
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            productSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            MessageError mess = new ConnecMobilePrePaid().connecMobilePrePaid(imSession.connection(), cmPreSession.connection(), cmPosSession.connection(), anyPaySession.connection(), productSession.connection(), wsRequest.getProductCode(), wsRequest.getReasonCode(), wsRequest.getIsdn(), wsRequest.getSerial(), wsRequest.getUserLogin(), wsRequest.getService());
            if (mess != null) {
                hasErr = true;
                return new WSRespone(Constants.ERROR_CODE_1, mess.getMessage());
            }
            commitTransactions(cmPosSession, cmPreSession, imSession, anyPaySession, productSession);
            result = new WSRespone(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession, cmPreSession, imSession, anyPaySession, productSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession, cmPreSession, imSession, anyPaySession, productSession);
            LogUtils.info(logger, "SubscriberController.activeSubscriber:result=" + LogUtils.toJson(result));
        }
    }

    public WSRespone changeSim(HibernateHelper hibernateHelper, ChangeSimIn wsRequest, String locale) {
        WSRespone result = null;
        Session cmPosSession = null;
        Session cmPreSession = null;
        Session imSession = null;
        Session ccSession = null;
        Session paymentSession = null;
        boolean hasErr = false;
        try {
            LogUtils.info(logger, "SubscriberController.activeSubscriberPre:wsRequest=" + LogUtils.toJson(wsRequest));
            if (wsRequest == null) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid", locale));
                return result;
            }
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            paymentSession = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            String mess = new SubscriberBussiness().changeSim(cmPosSession, cmPreSession, imSession, ccSession, paymentSession, wsRequest, locale);
            if (mess != null) {
                hasErr = true;
                if (mess.equals("PRODUCT_ERR")) {
                    mess = "ISDN has not registered information yet. You need input more product code.";
                    return new WSRespone(Constants.ERROR_CODE_2, mess);
                } else if (mess.equals("TOP_UP_ERR")) {
                    mess = "ISDN has not registered information yet. You need input more top up amount.";
                    return new WSRespone(Constants.ERROR_CODE_2, mess);
                } else {
                    return new WSRespone(Constants.ERROR_CODE_1, mess);
                }
            }
            commitTransactions(cmPosSession, cmPreSession, imSession, ccSession, paymentSession);
            result = new WSRespone(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession, cmPreSession, imSession, ccSession, paymentSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession, cmPreSession, imSession, ccSession, paymentSession);
            LogUtils.info(logger, "SubscriberController.activeSubscriber:result=" + LogUtils.toJson(result));
        }
    }

    public ReasonChangeSimOut getListReasonChangeSim(HibernateHelper hibernateHelper, String isdn, String type, String locale) {
        ReasonChangeSimOut result = null;
        Session cmPosSession = null;
        Session cmPreSession = null;
        try {
            LogUtils.info(logger, "SubscriberController.getListReasonChangeSim:wsRequest=" + LogUtils.toJson(result));
            if (isdn == null) {
                result = new ReasonChangeSimOut(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid", locale));
                return result;
            }
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            result = new SubscriberBussiness().getListReasonChangeSim(cmPosSession, cmPreSession, isdn, type, locale);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ReasonChangeSimOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession, cmPreSession);
            LogUtils.info(logger, "SubscriberController.activeSubscriber:result=" + LogUtils.toJson(result));
        }
    }

    /**
     * @author: duyetdk
     * @since 12/5/2019
     * @des kich hoat thue bao
     * @param hibernateHelper
     * @param activeSubscriberPreIn
     * @param locale
     * @return
     */
    public WSRespone activeNewSubscriberPre(HibernateHelper hibernateHelper, ActiveSubscriberPreIn activeSubscriberPreIn, String locale) {
        WSRespone result = null;
//        Session anyPaySession = null;
//        Session cmPosSession = null;
        Session cmPreSession = null;
        Session imSession = null;
//        Session productSession = null;
        boolean hasErr = false;
        try {
            LogUtils.info(logger, "SubscriberController.activeNewSubscriberPre:activeSubscriberPreIn=" + LogUtils.toJson(activeSubscriberPreIn));
            if (activeSubscriberPreIn == null) {
                result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("input.invalid", locale));
                return result;
            }
            if (activeSubscriberPreIn.getService() == null || activeSubscriberPreIn.getService().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Service type is required");
                return result;
            }
            if (activeSubscriberPreIn.getProductCode() == null || activeSubscriberPreIn.getProductCode().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Product is required");
                return result;
            }
            if (activeSubscriberPreIn.getReasonCode() == null || activeSubscriberPreIn.getReasonCode().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Reason is required");
                return result;
            }
            if (activeSubscriberPreIn.getIsdn() == null || activeSubscriberPreIn.getIsdn().trim().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "ISDN is required");
                return result;
            }
            if (activeSubscriberPreIn.getIdNumber() == null || activeSubscriberPreIn.getIdNumber().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "ID number is required");
                return result;
            }
            if (activeSubscriberPreIn.getLstImageIDCard() == null || activeSubscriberPreIn.getLstImageIDCard().size() < 2) {
                result = new WSRespone(Constants.ERROR_CODE_1, "ID Card image is required");
                return result;
            }
            if (activeSubscriberPreIn.getLstImageForm() == null || activeSubscriberPreIn.getLstImageForm().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Form image is required");
                return result;
            }
            if (activeSubscriberPreIn.getSerial() == null || activeSubscriberPreIn.getSerial().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Serial is required");
                return result;
            }
            if (activeSubscriberPreIn.getUserLogin() == null || activeSubscriberPreIn.getUserLogin().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Staff code is required");
                return result;
            }
            if (activeSubscriberPreIn.getName() == null || activeSubscriberPreIn.getName().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Customer name is required");
                return result;
            }
            if (activeSubscriberPreIn.getDob() == null || activeSubscriberPreIn.getDob().isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_1, "Dob is required");
                return result;
            }
//            anyPaySession = hibernateHelper.getSession(Constants.SESSION_ANY_PAY);
//            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
//            productSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            String isdn = activeSubscriberPreIn.getIsdn().replaceFirst("^0+(?!$)", "").trim();

            List<com.viettel.im.database.BO.Staff> staff = new StaffDAO(imSession).findByProperty("staffCode", activeSubscriberPreIn.getUserLogin());
//            AccountAgent accountAgent = new CommonConnecSub().getAccountAgentInfo(imSession.connection(), activeSubscriberPreIn.getUserLogin().toUpperCase());
            if (staff == null) {
                return new WSRespone(Constants.ERROR_CODE_1, "Not found Staff");
            }

            //check KH da ton tai chua
            List<CustomerPre> lstCustCheck = new RegisterSupplier().getCustomerInfoByIdNo(cmPreSession, activeSubscriberPreIn.getIdNumber().trim());

            Date nowDate = new BaseBussiness().getSysDateTime(cmPreSession);
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date bday = formatter.parse(activeSubscriberPreIn.getDob());
            Long custId = new BaseSupplier().getSequence(cmPreSession, Constants.SEQ_CUSTOMER);

            Long idImage = Common.getSequence("ACCOUNT_IMAGE_SEQ", cmPreSession);
            //<editor-fold defaultstate="collapsed" desc="upload image ID Card">
            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            for (ImageInput imageInput : activeSubscriberPreIn.getLstImageIDCard()) {
                Long idImageDetail = Common.getSequence("CUST_IMAGE_DETAIL_SEQ", cmPreSession);
                String imageDate = dateFormat.format(nowDate);
                String imageName = activeSubscriberPreIn.getUserLogin().toUpperCase() + "_" + imageDate + "_" + idImageDetail + ".jpg";
//                boolean checkUpload = new FileSaveProcess().saveFileNotFtp(imageName, imageInput.getData(), isdn, logger);
                boolean checkUpload = new FileSaveProcess().saveNewFile(imageName, imageInput.getData(), staff.get(0).getStaffCode());
                if (!checkUpload) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, "upload image ID Card fail");
                }

                int checkInsertDB = new RegisterSupplier().insertCustImageDetail(cmPreSession, idImageDetail, idImage, imageName, imageInput.getName(), Constants.ID_CARD);
                if (checkInsertDB == 0) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, "insert cust_image_detail fail");
                }
            }

            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="upload image Form">
            for (ImageInput imageInput : activeSubscriberPreIn.getLstImageForm()) {
                Long idImageDetail = Common.getSequence("CUST_IMAGE_DETAIL_SEQ", cmPreSession);
                String imageDate = dateFormat.format(nowDate);
                String imageName = activeSubscriberPreIn.getUserLogin().toUpperCase() + "_" + imageDate + "_" + idImageDetail + ".jpg";
//                boolean checkUpload = new FileSaveProcess().saveFileNotFtp(imageName, imageInput.getData(), isdn, logger);
                boolean checkUpload = new FileSaveProcess().saveNewFile(imageName, imageInput.getData(), staff.get(0).getStaffCode());
                if (!checkUpload) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, "upload image Form fail");
                }

                int checkInsertDB = new RegisterSupplier().insertCustImageDetail(cmPreSession, idImageDetail, idImage, imageName, Constants.SUBSCRIBER_FORM, Constants.SUBSCRIBER_FORM);
                if (checkInsertDB == 0) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, "insert cust_image_detail fail");
                }
            }
            //</editor-fold>

            //neu la KH moi thi thuc hien dk thong tin
            if (lstCustCheck == null || lstCustCheck.isEmpty()) {
                //insert customer info
                new RegisterSupplier().insertCustomer(cmPreSession, custId, activeSubscriberPreIn.getIdType(), activeSubscriberPreIn.getName(),
                        bday, activeSubscriberPreIn.getIdNumber(), activeSubscriberPreIn.getUserLogin().toUpperCase(), nowDate,
                        activeSubscriberPreIn.getGender(), activeSubscriberPreIn.getContact(), activeSubscriberPreIn.getProvince());

                int checkInsertDB = new RegisterSupplier().insertCustImage(cmPreSession, idImage, custId, staff.get(0).getStaffCode(), activeSubscriberPreIn.getIdNumber(),
                        activeSubscriberPreIn.getName(), activeSubscriberPreIn.getUserLogin().toUpperCase(), activeSubscriberPreIn.getSerial(),
                        isdn, 2L);
                if (checkInsertDB == 0) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, "insert cust_image fail");
                }

            } else {
                int checkInsertDB = new RegisterSupplier().insertCustImage(cmPreSession, idImage, lstCustCheck.get(0).getCustId(), staff.get(0).getIsdn(), activeSubscriberPreIn.getIdNumber(),
                        activeSubscriberPreIn.getName(), activeSubscriberPreIn.getUserLogin().toUpperCase(), activeSubscriberPreIn.getSerial(),
                        isdn, 2L);
                if (checkInsertDB == 0) {
                    hasErr = true;
                    return new WSRespone(Constants.ERROR_CODE_1, "insert cust_image fail");
                }
            }

            //dk thong tin KH moi thanh cong thi chuyen sang buoc dang ky thue bao
            ConnectSubscriberResponse response = new ConnectSubscriberSupplier().processResponse(isdn,
                    activeSubscriberPreIn.getProductCode(), activeSubscriberPreIn.getReasonCode(), activeSubscriberPreIn.getSerial(),
                    0L, staff.get(0).getStaffId(), "1", activeSubscriberPreIn.getName(), activeSubscriberPreIn.getIdNumber(), activeSubscriberPreIn.getDob());
            if (!"0".equals(response.getReturn().getErrorCode())) {
                hasErr = true;
                return new WSRespone(response.getReturn().getErrorCode(), response.getReturn().getErrorDecription());
            }

            //update SubMb
            List<SubMbPre> lstSub = new RegisterSupplier().checkSubActive(cmPreSession, isdn,
                    Constants.STATUS_USE_SUB_MB, Constants.ACT_STATUS_NOT_ACTIVE);
            if (lstSub != null && !lstSub.isEmpty()) {
                SubMbPre sub = lstSub.get(0);
                sub.setCustId((lstCustCheck != null && !lstCustCheck.isEmpty()) ? lstCustCheck.get(0).getCustId() : custId);
                sub.setSubName(activeSubscriberPreIn.getName());
                sub.setBirthDate(bday);
                sub.setGender(activeSubscriberPreIn.getGender());
                sub.setProvince(activeSubscriberPreIn.getProvince());
                sub.setTelFax(activeSubscriberPreIn.getContact());
                sub.setChangeDatetime(nowDate);
                cmPreSession.update(sub);
                cmPreSession.flush();
            }

            commitTransactions(cmPreSession, imSession);
            result = new WSRespone(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPreSession, imSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPreSession, imSession);
            LogUtils.info(logger, "SubscriberController.activeNewSubscriber:result=" + LogUtils.toJson(result));
        }
    }

    public ParamOut searchRestoreIsdn(HibernateHelper hibernateHelper, String locale, String token, String isdn) {
        Session imSession = null;
        Session pmSession = null;
        Session cmPreSession = null;
        Session cmPosSession = null;
        ParamOut result = new ParamOut(Constants.ERROR_CODE_1, "");
        try {

            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            pmSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);

            if (isdn.startsWith("0")) {
                isdn.replaceFirst("0", "");
            }
            if (isdn.startsWith("855")) {
                isdn.replaceFirst("855", "");
            }
            SubscriberRestoreInfo subscriberRestoreInfo = new SubscriberRestoreInfo();
            result.setSubscriberRestoreInfo(subscriberRestoreInfo);
            String sql = " from SubMbPre where isdn  = ? order by validDatetime desc";
            Query query = cmPreSession.createQuery(sql);
            query.setParameter(0, isdn);
            List<SubMbPre> list = query.list();
            if (list == null || list.isEmpty()) {
                result.setErrorDecription("Can not find subscriber: " + isdn);
            } else {
                StringBuilder errorCode = new StringBuilder();
                StringBuilder errorDescription = new StringBuilder();
                checkIsdnRestore(imSession, isdn, list.get(0).getSubId(), errorCode, errorDescription);

                if (!"00".equals(errorCode.toString())) {
                    result.setErrorDecription(errorCode + ": " + errorDescription.toString());
                } else {
                    if (list.get(0).getStatus() != 3) {
                        result.setErrorDecription("Subscriber status is not delete ");
                    } else {
                        CustomerPre customer = list.get(0).getCustId() == null ? null : new CustomerPreDAO().findByCustId(cmPreSession, list.get(0).getCustId());
                        if (customer.getName() == null || customer.getName().isEmpty()) {
                            customer.setName("NA");
                        }
                        if (customer == null) {
                            subscriberRestoreInfo.setHasCustomer("false");
                        } else {
                            subscriberRestoreInfo.setHasCustomer("true");
                        }
                        subscriberRestoreInfo.setCustomer(customer);
                        subscriberRestoreInfo.setSubscriber(list.get(0));
                        ProductOffer productOffer = PMAPI.findProductOfferById(list.get(0).getOfferId(), pmSession);
                        if (productOffer != null) {
                            subscriberRestoreInfo.setProductName(productOffer.getOfferName());
                            subscriberRestoreInfo.setOfferName(productOffer.getOfferName());
                        }

                        sql = "select id from cm_pre2.cust_image where cust_id = ? and isdn = ? order by create_date desc";
                        query = cmPreSession.createSQLQuery(sql);
                        query.setParameter(0, list.get(0).getCustId());
                        query.setParameter(1, isdn);
                        List lst = query.list();
                        if (lst != null && !lst.isEmpty()) {
                            subscriberRestoreInfo.setCustImgId(Long.parseLong(lst.get(0).toString().trim()));
                        }
                        result.setErrorCode(Constants.ERROR_CODE_0);
                        result.setErrorDecription(LabelUtil.getKey("common.success", locale));
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            result.setErrorDecription(ex.getMessage());
        } finally {
            closeSessions(imSession, pmSession, cmPreSession, cmPosSession);
            LogUtils.info(logger, "SubscriberController.searchRestoreIsdn:result=" + LogUtils.toJson(result));
        }
        return result;
    }

    public ReasonOut getReasonRestoreIsdn(HibernateHelper hibernateHelper, String locale, String token) {
        Session cmPreSession = null;
        ReasonOut result = new ReasonOut(Constants.ERROR_CODE_1, "");
        try {
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            String sql = "SELECT r.reason_id reasonId, r.name "
                    + "  FROM cm_pre2.Reason r, cm_pre2.Ap_Domain a "
                    + " WHERE a.type = '19' "
                    + "   and a.reason_Type = '86' "
                    + "   AND a.code = to_char(r.type) "
                    + "   AND a.status = 1 "
                    + "   AND r.status = 1 "
                    + "   and reason_id not in(10054) "
                    + " order by r.name";
            Query query = cmPreSession.createSQLQuery(sql)
                    .addScalar("reasonId", Hibernate.LONG)
                    .addScalar("name", Hibernate.STRING)
                    .setResultTransformer(Transformers.aliasToBean(Reason.class));

            result.setReasons(query.list());
            result.setErrorCode(Constants.ERROR_CODE_0);
            result.setErrorDecription(LabelUtil.getKey("common.success", locale));
        } catch (Exception ex) {
            ex.printStackTrace();
            result.setErrorDecription(ex.getMessage());
        } finally {
            closeSessions(cmPreSession);
            LogUtils.info(logger, "SubscriberController.getReasonRestoreIsdn:result=" + LogUtils.toJson(result));
        }
        return result;
    }

    public WSRespone restoreIsdn(HibernateHelper hibernateHelper, String locale, String token, ChangeSimInput changeSimInput) {
        WSRespone result = new WSRespone(Constants.ERROR_CODE_1, null);
        Session imSession = null;
        Session pmSession = null;
        Session cmPreSession = null;
        Session cmPosSession = null;
        boolean isError = false;
        IsdnRestore isdnRestore = null;
        try {
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            pmSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);

            if (changeSimInput == null) {
                result.setErrorDecription("Data input not null");
                return result;
            }
            if (changeSimInput.getStaffId() == null) {
                result.setErrorDecription("StaffId is not null");
                return result;
            }
            if (changeSimInput.getSubId() == null) {
                result.setErrorDecription("subId is not null");
                return result;
            }
            if (changeSimInput.getStaffCode() == null || changeSimInput.getStaffCode().trim().isEmpty()) {
                result.setErrorDecription("StaffCode is not null");
                return result;
            }
            if (changeSimInput.getIsdn() == null || changeSimInput.getIsdn().trim().isEmpty()) {
                result.setErrorDecription("ISDN is not null");
                return result;
            }
            if (changeSimInput.getShopId() == null) {
                result.setErrorDecription("Shop id is not null");
                return result;
            }
            String isdn = changeSimInput.getIsdn();
            changeSimInput.setIsdn(isdn);
            if (isdn.startsWith("0")) {
                isdn = isdn.replaceFirst("0", "");
            }
            if (isdn.startsWith("855")) {
                isdn = isdn.replaceFirst("855", "");
            }
            Staff staff = new com.viettel.bccs.cm.dao.StaffDAO().findById(cmPosSession, changeSimInput.getStaffId());
            if (staff == null) {
                result.setErrorDecription("staff is not exists");
                return result;
            }
            Shop shop = new com.viettel.bccs.cm.dao.ShopDAO().findById(cmPosSession, staff.getShopId());
            if (shop == null) {
                result.setErrorDecription("shop is not exists");
                return result;
            }
            String sql = " from SubMbPre where subId = ?";
            Query query = cmPreSession.createQuery(sql);
            query.setParameter(0, changeSimInput.getSubId());
            List<SubMbPre> listSubMb = query.list();
            if (listSubMb == null || listSubMb.isEmpty()) {
                result.setErrorDecription("Can not find this subscriber " + changeSimInput.getIsdn());
                return result;
            }
            SubMbPre subMb = listSubMb.get(0);

            isdnRestore = new IsdnRestore();
            isdnRestore.setSubId(changeSimInput.getSubId());
            isdnRestore.setCustId(subMb.getCustId());
            isdnRestore.setIsdn(isdn);
            isdnRestore.setOldImsi(subMb.getImsi());
            isdnRestore.setOldSerial(subMb.getSerial());
            isdnRestore.setRestoreDate(new Date());
            isdnRestore.setNewSerial(changeSimInput.getSerial());
            isdnRestore.setResStaffId(staff.getStaffId());
            isdnRestore.setResShopId(staff.getShopId());
            isdnRestore.setIsdnOwnerId(staff.getStaffId().toString());
            isdnRestore.setIsdnOwnerType("2");

            StringBuilder errorCode = new StringBuilder();
            StringBuilder errorDescription = new StringBuilder();
            checkIsdnRestore(imSession, isdn, changeSimInput.getSubId(), errorCode, errorDescription);
            String description = null;

            if (!"00".equals(errorCode.toString())) {
                result.setErrorDecription(errorCode + ": " + errorDescription.toString());
                isdnRestore.setErrorCode(errorCode.toString());
                isdnRestore.setErrorDes(errorCode + ": " + errorDescription.toString());
                return result;
            }
            ReasonPre reason = new com.viettel.brcd.dao.pre.ReasonPreDAO().findById(cmPreSession, changeSimInput.getReasonId());
            if (reason == null) {
                description = "Can not find this reason to restore isdn";
                result.setErrorDecription(description);
                isdnRestore.setErrorCode("99");
                isdnRestore.setErrorDes(description);
                return result;
            }
            String errorMessage;
            CustomerPreDAO customerPreDAO = new CustomerPreDAO();
            com.viettel.bccs.cm.controller.prepaid.PrepaidController prepaidController = new com.viettel.bccs.cm.controller.prepaid.PrepaidController();
            CommonPre commonPre = new CommonPre();
            String actionCode = "357";
            Date nowDate = new Date();
            BaseSupplier baseSupplier = new BaseSupplier();
            CustomerPre customer = new CustomerPreDAO().findByCustId(cmPreSession, subMb.getCustId());
            if (customer.getName() == null || customer.getName().isEmpty()) {
                customer.setName(" ");
            }
            if (customer != null) {
                customer.setStatus(Constants.STATUS_USE);
                cmPreSession.update(customer);

                /*check quota gioi han so luong thue bao cua customer*/
                errorMessage = customerPreDAO.checkProductCount(cmPreSession, Constants.SERVICE_ALIAS_MOBILE, customer.getIdNo(), subMb.getProductCode(), 1);
                if (errorMessage != null && !"".equals(errorMessage)) {
                    result.setErrorDecription(errorMessage);
                    isdnRestore.setErrorCode("99");
                    isdnRestore.setErrorDes(errorMessage);
                    isError = true;
                    return result;
                }
            }
            String saleServiceCode = new com.viettel.bccs.cm.dao.MappingDAO().getSaleServiceCodePre(cmPreSession, Constant.SERVICE_MOBILE_ID_WEBSERVICE, reason.getReasonId(), subMb.getProductCode(), Constant.ACTION_RECOVER_SUB_KH);
            if (saleServiceCode == null || "".equals(saleServiceCode)) {
                description = "reason does not match with sales service to generate transaction";
                result.setErrorDecription(description);
                isdnRestore.setErrorDes(description);
                isdnRestore.setErrorCode("99");
                isError = true;
                return result;
            }
            WebServiceIMDAO webIMDAO = new WebServiceIMDAO(imSession);
            StockSim stockSim = webIMDAO.findSimPrePaidByImsiOrSerial(null, changeSimInput.getSerial());
            String newImsi = null;
            if (stockSim != null) {
                newImsi = stockSim.getImsi();
            }
            if (newImsi == null || newImsi.isEmpty()) {
                description = "No IMSI corresponding to the  Serial " + changeSimInput.getSerial();
                result.setErrorDecription(description);
                isdnRestore.setErrorDes(description);
                isdnRestore.setErrorCode("99");
                isError = true;
                return result;
            }
            isdnRestore.setNewImsi(newImsi);
            com.viettel.bccs.cm.dao.interfaceCommon.InterfaceCommon interfaceCommon = new com.viettel.bccs.cm.dao.interfaceCommon.InterfaceCommon();
            List<StockSimBean> lstStockSimBean = new ArrayList<StockSimBean>();
            prepaidController.addToListStockSim(lstStockSimBean, newImsi, changeSimInput.getSerial(), WebServiceIMDAO.STOCK_SIM_PRE_PAID);

            logger.info(changeSimInput.getStaffCode() + " changeSim, Step 3, START : InterfaceCmInventory.checkImsiIsInStockByOwnerType");
            errorMessage = interfaceCommon.checkValidResourceByOwnerType(imSession, 2l,
                    changeSimInput.getStaffId(), saleServiceCode, null, lstStockSimBean, null);
            if (errorMessage != null && !"".equals(errorMessage)) {
                result.setErrorDecription(errorMessage);
                isdnRestore.setErrorDes(errorMessage);
                isdnRestore.setErrorCode("99");
                isError = true;
                return result;
            }
            Long oldInStatus = subMb.getStatus();
            String oldInActStatus = subMb.getActStatus();
            String oldImsi = subMb.getImsi();
            String oldSerial = subMb.getSerial();
            // update lại trạng thái thuê bao
            subMb.setStatus(Constants.SUB_STATUS_NORMAL_ACTIVED);
            subMb.setActStatus(Constants.ACTION_SUBSCRIBER_ACTIVE_NEW);
            subMb.setEndDatetime(null);
            subMb.setFinishReasonId(null);
            subMb.setChangeDatetime(nowDate);

            Long actionAuditId = baseSupplier.getSequence(cmPreSession, "SEQ_ACTION_AUDIT");
            List<String> lst = new ArrayList();
            lst.add(staff.getStaffCode());
            lst.add(shop.getShopCode());
            String strTemp = Constants.ACTION_AUDIT_DB_SW_PREFIX + ": " + "TDN staff {0} repair for shop: {1}";
            strTemp = commonPre.replaceTextResponse(strTemp, lst);

            //strTemp += ": Khôi phục thuê bao trả trước: " + subMb.getIsdn() + " , có subId: " + subId;
            lst.clear();
            lst.add(subMb.getIsdn());
            lst.add(changeSimInput.getSubId().toString());
            strTemp += ": " + Common.getLogText("log.restore.sub.prepaid", lst);
            /*Insert image*/

            ActionLog.logAction(cmPreSession, actionAuditId, changeSimInput.getSubId(), Constant.ACTION_AUDIT_PK_TYPE_SUBSCRIBER,
                    changeSimInput.getReasonId(), actionCode, strTemp, nowDate, staff.getStaffCode(), shop.getShopCode(), changeSimInput.getIp() == null || changeSimInput.getIp().isEmpty() ? "mBCCS" : changeSimInput.getIp(), null);

            ActionLog.logAuditDetail(cmPreSession, subMb.getSubId(), actionAuditId, "SUB_MB", "IN_STATUS", oldInStatus, subMb.getStatus(), nowDate);
            ActionLog.logAuditDetail(cmPreSession, subMb.getSubId(), actionAuditId, "SUB_MB", "IN_ACT_STATUS", oldInActStatus, subMb.getActStatus(), nowDate);
            ActionLog.logAuditDetail(cmPreSession, subMb.getSubId(), actionAuditId, "SUB_MB", "VALID_DATETIME", null, nowDate, nowDate);
            ActionLog.logAuditDetail(cmPreSession, subMb.getSubId(), actionAuditId, "SUB_MB", "IMSI", oldImsi, newImsi, nowDate);
            ActionLog.logAuditDetail(cmPreSession, subMb.getSubId(), actionAuditId, "SUB_MB", "SERIAL", oldSerial, changeSimInput.getSerial(), nowDate);
            //Khoi phuc thong tin bang Sub_isdn_mb
            commonPre.addSubIsdnMb(cmPreSession, changeSimInput.getSubId(), subMb.getIsdn(), nowDate);

            // Khoi phuc thong tin bang Sub_Sim_mb
            commonPre.addSubSimMb(cmPreSession, changeSimInput.getSubId(), subMb.getImsi(), subMb.getSerial(), nowDate);

            if (customer != null) {
                // Khoi phuc thong tin bang Sub_Id_No
                commonPre.recoverSubIdNo(changeSimInput.getSubId(), cmPreSession, actionAuditId, nowDate);
            }

            // Khoi phuc thong tin bang Sub_mb_fix_broadband
            //Thuc hien cap nhat trang thai so ve dang su dung neu so la so moi hoac so ngung su dung
            errorMessage = commonPre.updateIsdnToUsing(imSession, Constant.SERVICE_MOBILE_ID_WEBSERVICE, subMb.getIsdn());

            // update vas mac dinh, lay thong tin vas theo subId
            List<SubRelProductPre> lstSubRelProduct = commonPre.getListDefaultVas(subMb.getProductCode().trim(), subMb.getSubId(), pmSession, nowDate);
            for (int i = 0; i < lstSubRelProduct.size(); i++) {
                Long subRelProductId = baseSupplier.getSequence(cmPreSession, "SEQ_SUB_REL_PRODUCT");
                lstSubRelProduct.get(i).setSubRelProductId(subRelProductId);
                cmPreSession.save(lstSubRelProduct.get(i));
            }
            List<SubRelProductPre> lstSubRel = new ArrayList<SubRelProductPre>();
            //Thangpn - Them Gửi giao dịch sang IM
            errorMessage = interfaceCommon.executeSaleTrans(cmPreSession, imSession, subMb,
                    Constants.TRANS_OF_SUBSCRIBER, null, null, null, 10054l, actionCode, nowDate, staff.getStaffId(), staff.getShopId());
            if (errorMessage != null && !errorMessage.equals("")) {
                result.setErrorDecription(errorMessage);
                isdnRestore.setErrorDes(errorMessage);
                isdnRestore.setErrorCode("99");
                isError = true;
                return result;
            }
            /*update new imsi and serial*/
            subMb.setImsi(newImsi);
            subMb.setSerial(changeSimInput.getSerial());
            cmPreSession.update(subMb);
            //End - Thangpn

            //Dẩy dữ liệu và Database
            cmPreSession.flush();
            imSession.flush();

            //Thực hiện gửi lệnh lên tổng đài
            ViettelService activeResponse = commonPre.activePrePaidSubscriber(imSession, subMb, pmSession, cmPreSession, true, staff.getStaffCode(), shop.getShopCode());

            if (activeResponse != null
                    && activeResponse.get("responseCode") != null
                    && "0".equals(activeResponse.get("responseCode").toString().trim())) {

                if (lstSubRel.size() > 0) {
                    for (int i = 0; i < lstSubRel.size(); i++) {
                        SubRelProductPre subRelProduct = lstSubRel.get(i);

                        // update db
                        subRelProduct.setStatus(Constant.VAS_STATUS_NORMAL);
                        subRelProduct.setStaDatetime(nowDate);
                        subRelProduct.setEndDatetime(null);

                        cmPreSession.update(subRelProduct);
                        cmPreSession.flush();
                    }
                }

                commonPre.insertRecoveryRequest(cmPreSession, changeSimInput.getIsdn(), 3, customer.getName(), subMb.getSerial(),
                        subMb.getImsi(), changeSimInput.getReasonId(), customer.getIdNo(), changeSimInput.getSubId(), customer.getCustId(), 1,
                        shop.getShopId(), shop.getShopCode(), staff.getStaffCode());
                //pageForward = "repairWrongMobileResult";

                //active sub 
                // active first
                InterfacePr.activateFirstSub(cmPreSession, subMb.getIsdn(), subMb.getSerial(), com.viettel.bccs.cm.common.util.Constant.COMMAND_ACTIVATE_FIRST, staff.getStaffCode(), shop.getShopCode());

                errorMessage = interfaceCommon.executeSaleTrans(cmPreSession, imSession, subMb, Constant.TRANS_OF_SUBSCRIBER,
                        null, lstStockSimBean, null, reason.getReasonId(),
                        actionCode, nowDate, changeSimInput.getStaffId(), changeSimInput.getShopId());
                if (errorMessage != null && !errorMessage.equals("")) {
                    result.setErrorDecription(errorMessage);
                    isdnRestore.setErrorDes(errorMessage);
                    isdnRestore.setErrorCode("99");
                    isError = true;
                    return result;
                }
                InterfacePr interfacePr = new InterfacePr();
                String stockModelCode = prepaidController.getStockModelCode(imSession, subMb.getSerial());
                Long resultPr = 0l;
                if (stockModelCode != null) {
                    stockModelCode = stockModelCode.trim();
                    if (!stockModelCode.isEmpty() && prepaidController.isActiveSim4G(stockModelCode)) {
                        ViettelService activeService4G = interfacePr.activeService4G(subMb.getIsdn());
                        if (activeService4G != null && activeService4G.get("responseCode") != null
                                && "0".equals(activeService4G.get("responseCode").toString().trim())) {
                            resultPr = Long.valueOf(activeService4G.get("responseCode").toString().trim());
                        } else {
                            resultPr = 1L;
                        }
                        if (!resultPr.equals(0L)) {
                            logger.info(changeSimInput.getStaffCode() + " Active 4G fail");
                        } else {
                            logger.info(changeSimInput.getStaffCode() + " Active 4G success");
                        }
                    }
                }
                if (resultPr.equals(0L)) {
                    String accountId;
                    String amount;
                    try {
                        accountId = new ApParamDAO().getValueByTypeCode(cmPreSession, "CHANGE_SIM_RESTORE", "ACCOUNT_ADD");
                        amount = new ApParamDAO().getValueByTypeCode(cmPreSession, "CHANGE_SIM_RESTORE", "AMOUNT_ADD");
                    } catch (Exception ex) {
                        accountId = "2000";
                        amount = "2";
                    }
                    Long imageId = null;
                    if ((changeSimInput.getCommitForm() != null && !changeSimInput.getCommitForm().isEmpty() && !"?".equals(changeSimInput.getCommitForm()))
                            || (changeSimInput.getSaleServiceForm() != null && !changeSimInput.getSaleServiceForm().isEmpty() && !"?".equals(changeSimInput.getSaleServiceForm()))
                            || (changeSimInput.getIdCardFront() != null && !changeSimInput.getIdCardFront().isEmpty() && !"?".equals(changeSimInput.getIdCardFront()))) {
                        FileSaveProcess fileSaveProcess = new FileSaveProcess();
                        imageId = Common.getSequence("CUST_IMAGE_SEQ", cmPosSession);
                        SubFbConfigSupplier supplier = new SubFbConfigSupplier();
                        if (changeSimInput.getCommitForm() != null && !changeSimInput.getCommitForm().isEmpty() && !"?".equals(changeSimInput.getCommitForm())) {
                            String imageName = imageId + "_commit.jpg";
                            fileSaveProcess.saveFileNotFtp(imageName, changeSimInput.getCommitForm(), "", logger, Constants.FTP_DIR_SUB_ACTION);
                            supplier.insertImageDetail(cmPosSession, imageId, imageName, "RESTORE_ISDN", null, staff.getStaffCode(), null, null);
                        }
                        if (changeSimInput.getSaleServiceForm() != null && !changeSimInput.getSaleServiceForm().isEmpty() && !"?".equals(changeSimInput.getSaleServiceForm())) {
                            String imageName = imageId + "_sale.jpg";
                            fileSaveProcess.saveFileNotFtp(imageName, changeSimInput.getSaleServiceForm(), "", logger, Constants.FTP_DIR_SUB_ACTION);
                            supplier.insertImageDetail(cmPosSession, imageId, imageName, "RESTORE_ISDN", null, staff.getStaffCode(), null, null);
                        }
                        if (changeSimInput.getIdCardFront() != null && !changeSimInput.getIdCardFront().isEmpty() && !"?".equals(changeSimInput.getIdCardFront())) {
                            String imageName = imageId + "_id1.jpg";
                            fileSaveProcess.saveFileNotFtp(imageName, changeSimInput.getIdCardFront(), "", logger, Constants.FTP_DIR_SUB_ACTION);
                            supplier.insertImageDetail(cmPosSession, imageId, imageName, "RESTORE_ISDN", null, staff.getStaffCode(), null, null);
                        }
                    }
                    actionAuditId = baseSupplier.getSequence(cmPreSession, "SEQ_ACTION_AUDIT");
                    ActionLog.logAction(cmPreSession, actionAuditId, subMb.getSubId(), Constant.ACTION_AUDIT_PK_TYPE_SUBSCRIBER,
                            13013l, Constant.ACTION_SUBSCRIBER_CHANGE_SIM, "Unblock one way by change sim", new Date(), changeSimInput.getStaffCode(), shop.getShopCode(), changeSimInput.getIp() == null || changeSimInput.getIp().isEmpty() ? "mBCCS" : changeSimInput.getIp(), imageId);

                    prepaidController.addAccountLog(cmPreSession, subMb.getIsdn(), amount, reason.getReasonId(), changeSimInput.getStaffCode(), "add money change sim restore");
                    actionAuditId = baseSupplier.getSequence(cmPreSession, "SEQ_ACTION_AUDIT");
                    ActionLog.logAction(cmPreSession, actionAuditId, subMb.getSubId(), Constant.ACTION_AUDIT_PK_TYPE_SUBSCRIBER,
                            13031l, Constant.ACTION_SUBCRIBER_ACTIVE_1_WAY, "Add money 2$ by change sim ", new Date(), changeSimInput.getStaffCode(), shop.getShopCode(), changeSimInput.getIp() == null || changeSimInput.getIp().isEmpty() ? "mBCCS" : changeSimInput.getIp(), null);

                    OriginalViettelMsg resultLock = interfacePr.addMoney(cmPreSession, accountId, subMb.getIsdn(), amount, changeSimInput.getStaffCode(), shop.getShopCode());
                    ActionLog.saveActionLogPr(cmPreSession, resultLock, changeSimInput.getStaffCode(), shop.getShopCode());

                    result.setErrorCode(Constants.ERROR_CODE_0);
                    result.setErrorDecription(LabelUtil.getKey("common.success", locale));

                    isdnRestore.setErrorDes(LabelUtil.getKey("common.success", locale));
                    isdnRestore.setErrorCode("00");
                } else {
                    result.setErrorDecription("call pro active service 4g failed");
                    isError = true;
                    return result;
                }
            } else {
                result.setErrorDecription("Restore the connection fail");
                isError = true;
                return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            result.setErrorDecription(ex.getMessage());
            isError = true;
        } finally {
            if (isError) {
                cmPreSession.clear();
                cmPreSession.beginTransaction().rollback();
                cmPreSession.beginTransaction();

                imSession.clear();
                imSession.beginTransaction().rollback();
                imSession.beginTransaction();

                cmPosSession.clear();
                cmPosSession.beginTransaction().rollback();
                cmPosSession.beginTransaction();
            } else {
                cmPreSession.beginTransaction().commit();
                cmPreSession.beginTransaction();

                imSession.beginTransaction().commit();
                imSession.beginTransaction();

                cmPosSession.beginTransaction().commit();
                cmPosSession.beginTransaction();
            }
            if (isdnRestore != null) {
                imSession.save(isdnRestore);
                imSession.beginTransaction().commit();
                imSession.beginTransaction();
            }
            closeSessions(imSession, pmSession, cmPreSession, cmPosSession);
        }
        return result;
    }

    private void checkIsdnRestore(Session imSession, String isdn, Long subId, StringBuilder errorCode, StringBuilder description) throws Exception {
        CallableStatement cs = imSession.connection().prepareCall("{ call pkg_isdn_vtc.check_isdn_restore(?,?,?,?)}");
        cs.setString(1, subId.toString());
        cs.setLong(2, Long.parseLong(isdn));
        cs.registerOutParameter(3, java.sql.Types.VARCHAR);
        cs.registerOutParameter(4, java.sql.Types.VARCHAR);
        cs.execute();
        errorCode.append(cs.getString(3));
        description.append(cs.getString(4));
        cs.close();
    }
}
