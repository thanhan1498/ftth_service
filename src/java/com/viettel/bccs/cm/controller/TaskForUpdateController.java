/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.google.gson.Gson;
import com.viettel.bccs.api.Task.BO.AssignTaskBO;
import com.viettel.bccs.api.Task.BO.ComplainResult;
import com.viettel.bccs.api.Task.BO.Contract;
import com.viettel.bccs.api.Task.BO.DetailInfoTask;
import com.viettel.bccs.api.Task.BO.DetailInfrasLookFor;
import com.viettel.bccs.api.Task.BO.DetailRevokeTask;
import com.viettel.bccs.api.Task.BO.EquipmentPort;
import com.viettel.bccs.api.Task.BO.Infastructure;
import com.viettel.bccs.api.Task.BO.InfastructureDetail;
import com.viettel.bccs.api.Task.BO.ResolveComplain;
import com.viettel.bccs.api.Task.BO.ResultBO;
import com.viettel.bccs.api.Task.BO.ResultDeleteTask;
import com.viettel.bccs.api.Task.BO.ResultRevokeTask;
import com.viettel.bccs.api.Task.BO.SubDeployment;
import com.viettel.bccs.api.Task.BO.SubStockModelRel;
import com.viettel.bccs.api.Task.BO.TaskHistory;
import com.viettel.bccs.api.Task.BO.TaskManagement;
import com.viettel.bccs.api.Task.BO.ViewTaskStaff;
import com.viettel.bccs.api.Task.BO.ViewUpdateTask;
import com.viettel.bccs.api.Task.DAO.ApParamDAO;
import com.viettel.bccs.api.Task.DAO.ContractDAO;
import com.viettel.bccs.api.Task.DAO.ManagerTaskDAO;
import com.viettel.bccs.api.Task.DAO.SubDeploymentDAO;
import com.viettel.bccs.api.Task.DAO.SubStockModelRelDAO;
import com.viettel.bccs.api.Util.Constant;
import com.viettel.bccs.api.Util.DateTimeUtils;
import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.api.supplier.nims.getInfrasGpon.GetBranchNodeByStationResutForm;
import com.viettel.bccs.api.supplier.nims.getInfrasGpon.GetPortBySplitterResultForm;
import com.viettel.bccs.api.supplier.nims.getInfrasGpon.GetSplitterBySubNodeResultForm;
import com.viettel.bccs.api.supplier.nims.getInfrasGpon.GetSubNodeByBranchNodeResultForm;
import com.viettel.bccs.cm.bussiness.ApParamBussiness;
import com.viettel.bccs.cm.bussiness.SubAdslLeaselineBussines;
import com.viettel.bccs.cm.bussiness.SubFbConfigBusiness;
import com.viettel.bccs.cm.bussiness.TokenBussiness;
import com.viettel.bccs.cm.bussiness.common.CacheBO;
import com.viettel.bccs.cm.controller.digital.TaskForUpdateControllerCamera;
import com.viettel.bccs.cm.controller.digital.TaskForUpdateControllerDigital;
import com.viettel.bccs.cm.dao.CorporateWhiteListDAO;
import com.viettel.bccs.cm.dao.CustImageDetailDAO;
import com.viettel.bccs.cm.dao.CustomerDAO;
import com.viettel.bccs.cm.dao.NotificationMessageDAO;
import com.viettel.bccs.cm.dao.ReasonDAO;
import com.viettel.bccs.cm.dao.RegOnlineDAO;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.dao.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.dao.SubBundleTvDAO;
import com.viettel.bccs.cm.dao.SubDigitalDAO;
import com.viettel.bccs.cm.dao.TaskDAO;
import com.viettel.bccs.cm.dao.TaskManagementDAO;
import com.viettel.bccs.cm.dao.TaskShopManagementDAO;
import com.viettel.bccs.cm.dao.TaskStaffManagementDAO;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.CorporateWhiteList;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.GroupReason;
import com.viettel.bccs.cm.model.InforTaskToUpdate;
import com.viettel.bccs.cm.model.KpiBonusTask;
import com.viettel.bccs.cm.model.KpiDeadlineTask;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.ReasonComplaint;
import com.viettel.bccs.cm.model.RegOnline;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubBundleTv;
import com.viettel.bccs.cm.model.SubBundleTvLog;
import com.viettel.bccs.cm.model.SubDigital;
import com.viettel.bccs.cm.model.SubFbConfig;
import com.viettel.bccs.cm.model.TaskShopManagement;
import com.viettel.bccs.cm.model.TaskStaffManagement;
import com.viettel.bccs.cm.model.Token;
import com.viettel.bccs.cm.supplier.ApParamSupplier;
import com.viettel.bccs.cm.supplier.ServiceSupplier;
import com.viettel.bccs.cm.supplier.SubFbConfigSupplier;
import com.viettel.bccs.cm.supplier.im.ImSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.dao.pre.IsdnSendSmsDAO;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.InfrastructureUpdateIn;
import com.viettel.brcd.ws.model.input.NewImageInput;
import com.viettel.brcd.ws.model.input.RevokeTaskIn;
import com.viettel.brcd.ws.model.input.SearchTaskForLookUpInput;
import com.viettel.brcd.ws.model.input.SearchTaskForUpdateInput;
import com.viettel.brcd.ws.model.input.SearchTaskRevokeIn;
import com.viettel.brcd.ws.model.output.InforInfrastructureOut;
import com.viettel.brcd.ws.model.output.InforResolveComplainOut;
import com.viettel.brcd.ws.model.input.UpdateNoteIn;
import com.viettel.brcd.ws.model.input.UpdateTaskIn;
import com.viettel.brcd.ws.model.output.BonusSalary;
import com.viettel.brcd.ws.model.output.CableBoxOut;
import com.viettel.brcd.ws.model.output.ComplainResultOut;
import com.viettel.brcd.ws.model.output.CoordinateSubDrawCable;
import com.viettel.brcd.ws.model.output.DetailRevokeTaskOut;
import com.viettel.brcd.ws.model.output.GroupReasonOut;
import com.viettel.brcd.ws.model.output.InforTaskToUpdateOut;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.PortOut;
import com.viettel.brcd.ws.model.output.PreRevokeOut;
import com.viettel.brcd.ws.model.output.Request;
import com.viettel.brcd.ws.model.output.ResultUpdateTaskOut;
import com.viettel.brcd.ws.model.output.SearchTaskRevokeOut;
import com.viettel.brcd.ws.model.output.ServiceResponse;
import com.viettel.brcd.ws.model.output.TaskForUpdateOut;
import com.viettel.brcd.ws.model.output.TaskHistoryOut;
import com.viettel.brcd.ws.model.output.TaskInfoDetailOut;
import com.viettel.brcd.ws.model.output.TaskInfrasDetailOut;
import com.viettel.brcd.ws.model.output.TaskLookFor;
import com.viettel.brcd.ws.model.output.TaskLookForOut;
import com.viettel.brcd.ws.model.output.TaskUpdate;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.brcd.ws.supplier.nims.lockinfras.LockAndUnlockInfrasResponse;
import com.viettel.brcd.ws.supplier.nims.lockinfras.NimsWsLockUnlockInfrasBusiness;
import com.viettel.brcd.ws.supplier.soc.ErrorKnowledgeCataForm;
import com.viettel.brcd.ws.supplier.soc.RequestAnalysSocResponse;
import com.viettel.brcd.ws.supplier.soc.SocService;
import com.viettel.cc.client.bean.CompProcess;
import com.viettel.cc.client.bean.InfoComplain;
import com.viettel.cc.database.BO.CompCause;
import com.viettel.cc.database.BO.CompSatisfiedLevel;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.im.database.BO.AccountAgent;
import com.viettel.im.database.BO.Boards;
import com.viettel.im.database.BO.CableBox;
import com.viettel.im.database.BO.Dslam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Nguyen Ngoc viet
 */
public class TaskForUpdateController extends BaseController {

    String local;

    public TaskForUpdateController() {
        logger = Logger.getLogger(TaskForUpdateController.class);
    }

    public TaskForUpdateOut searchTask(HibernateHelper hibernateHelper, SearchTaskForUpdateInput searchTaskInput, String local) {
        TaskForUpdateOut taskToAssign = new TaskForUpdateOut();
        Session sessionPos = null;

        try {
            if (searchTaskInput.getShopId() == null) {
                return new TaskForUpdateOut("-1", "Not found user login");
            }
            StringBuilder total = new StringBuilder();
            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            Date nowDate = Common.getDateTimeFromDB(sessionPos);
            List lstTask = new ArrayList();
            List<ViewUpdateTask> lst;
            List<CoordinateSubDrawCable> lstCoor;
            SubFbConfig subFbConfig;
            List<ApParam> apParams;
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            Shop shop = new ShopDAO().findById(sessionPos, searchTaskInput.getShopId());
            if (shop != null) {
                List<ApParam> listParam = new ApParamBussiness().findByType(sessionPos, "VIEW_TASK_STAFF");
                if (listParam != null && !listParam.isEmpty()) {
                    if (!listParam.get(0).getParamValue().toUpperCase().contains(shop.getProvince())) {
                        searchTaskInput.setStaffId(null);
                    }
                }
            }
            //<editor-fold defaultstate="collapsed" desc="call lib get stakk">
            lst = managerTaskDAO.getListStaffForUpdateTask(sessionPos, searchTaskInput.getCustReqId(),
                    searchTaskInput.getShopId(), searchTaskInput.getSourceType(),
                    searchTaskInput.getAccount(), searchTaskInput.getProgress(),
                    searchTaskInput.getStaDate(), searchTaskInput.getEndDate(),
                    nowDate, searchTaskInput.getService(), searchTaskInput.getStaffId(),
                    searchTaskInput.getPage(), searchTaskInput.getPageSize(), total);
            taskToAssign.setTotal(total.toString());
            if (searchTaskInput.getStatus() == null && searchTaskInput.getStatusConfig() == null) {
                lstTask = lst;
            }
            if ((searchTaskInput.getStatus() != null || searchTaskInput.getStatusConfig() != null) && lst != null && lst.size() > 0) {
                List<SubFbConfig> lstSub = new SubFbConfigBusiness().findByStatus(sessionPos, searchTaskInput.getStatus(), searchTaskInput.getStatusConfig(), Constants.TYPE_CONF_IMT);
                for (ViewUpdateTask task : lst) {
                    for (SubFbConfig sub : lstSub) {
                        if (task.getTaskMngtId().equals(sub.getTaskManagementId())) {
                            lstTask.add(task);
                        }
                    }
                }
            }
            //</editor-fold>
            List listResult = new ArrayList();
            if (lstTask != null && lstTask.size() > 0) {
                taskToAssign.setErrorCode("0");
                taskToAssign.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
                HashMap<String, List<KpiBonusTask>> listKpiBonusTask = CacheBO.getListKpiBonusTask(sessionPos);
                for (Iterator iterator = lstTask.iterator(); iterator.hasNext();) {
                    TaskUpdate taskUpdateOut = new TaskUpdate();
                    ViewUpdateTask taskManagement = (ViewUpdateTask) iterator.next();
                    SubAdslLeaseline sub = new SubAdslLeaselineDAO().findByAccount(sessionPos, taskManagement.getAccount());
                    boolean isVip = sub == null ? false : new SubAdslLeaselineDAO().isAccountVip(sessionPos, sub.getSubType(), sub.getProductCode());
                    taskUpdateOut.setIsVip(isVip ? 1 : 0);
                    if (listKpiBonusTask != null) {
                        //JOB_TYPE || '_' || SERVICE_TYPE || '_' || nvl(INFRA_TYPE,'AON') || '_' || IS_VIP
                        //com.viettel.bccs.cm.model.TaskManagement task = new TaskManagementDAO().findById(sessionPos, taskManagement.getTaskMngtId());
                        String key = sub == null ? "_" : sub.getProvince()
                                + "_" + taskManagement.getJobName()
                                + "_" + sub.getServiceType()
                                + "_" + (sub.getInfraType() == null ? "AON" : sub.getInfraType())
                                + "_" + (isVip ? "1" : "0");
                        List<KpiBonusTask> listBonus = listKpiBonusTask.get(key);
                        if (listBonus != null && !listBonus.isEmpty()) {
                            BonusSalary bonusSalary = new BonusSalary();
                            Double totalHour = ((new Date()).getTime() - taskManagement.getTaskCreateDate().getTime()) / (1000.0 * 60.0 * 60.0);
                            bonusSalary.setTotalTime(totalHour);
                            for (int i = 0; i < listBonus.size(); i++) {
                                Double minus = listBonus.get(i).getKpi() - totalHour;
                                if (minus >= 0 && listBonus.get(i).getLastLevel() == null) {
                                    bonusSalary.setCurrentBonus(listBonus.get(i).getAmount());
                                    bonusSalary.setCurrentDeadline(listBonus.get(i).getKpi());
                                    if (i < listBonus.size() - 1) {
                                        bonusSalary.setNextBonus(listBonus.get(i + 1).getAmount());
                                        bonusSalary.setNextDeadline(listBonus.get(i + 1).getKpi());
                                    }
                                    break;
                                }
                                if (minus < 0 && listBonus.get(i).getLastLevel() != null) {
                                    bonusSalary.setCurrentBonus(listBonus.get(i).getAmount());
                                    bonusSalary.setCurrentDeadline(listBonus.get(i).getKpi());
                                }
                            }
                            long factor = (long) Math.pow(10, 1);
                            totalHour = totalHour * factor;
                            long tmp = Math.round(totalHour);
                            taskUpdateOut.setBonusSalary(bonusSalary);
                            bonusSalary.setTotalTime((double) tmp / factor);
                            if (bonusSalary.getCurrentBonus() == null) {
                                taskUpdateOut.setBonusSalary(null);
                            }
                        }
                    }

                    lstCoor = new SubAdslLeaselineBussines().findCoordinateBySubId(sessionPos, taskManagement.getAccount());

                    taskUpdateOut.setAccount(taskManagement.getAccount());
                    taskUpdateOut.setContractNo(taskManagement.getContractNo());
                    taskUpdateOut.setDeployAddress(taskManagement.getDeployAddress());
                    taskUpdateOut.setLimitDate(DateTimeUtils.convertDateTimeToString(taskManagement.getFinishDate()));
                    taskUpdateOut.setTaskMngtId(taskManagement.getTaskMngtId());
                    taskUpdateOut.setTaskName(taskManagement.getTaskName());
                    taskUpdateOut.setTaskStaffMngtId(taskManagement.getTaskStaffMngtId());
                    taskUpdateOut.setCustName(taskManagement.getCustName());
                    taskUpdateOut.setCustTelFax(taskManagement.getTelFax());
                    taskUpdateOut.setProgress(taskManagement.getTaskStaffProgress());
                    if (Constants.STAFF_START.equals(taskManagement.getTaskStaffProgress())) {
                        taskUpdateOut.setProgressName(LabelUtil.getKey("progress.start.update.task", local));
                    }
                    if (Constants.STAFF_PERFORM.equals(taskManagement.getTaskStaffProgress())) {
                        taskUpdateOut.setProgressName(LabelUtil.getKey("progress.perform.update.task", local));
                    }
                    if (Constants.REQUEST_CONFIG.equals(taskManagement.getTaskStaffProgress())) {
                        taskUpdateOut.setProgressName(LabelUtil.getKey("progress.request.task.config.task", local));
                    }
                    if (Constants.FINISH_TASK.equals(taskManagement.getTaskStaffProgress())) {
                        taskUpdateOut.setProgressName(LabelUtil.getKey("progress.finish.update.task", local));
                    }
                    taskUpdateOut.setIsSystemError(taskManagement.getIsSystemError());
                    taskUpdateOut.setTextColor(taskManagement.getTextColour());
                    taskUpdateOut.setTelServiceId(searchTaskInput.getService());
                    taskUpdateOut.setBackgroundColor(taskManagement.getBackGroundColour());
                    taskUpdateOut.setSourceType(taskManagement.getSourceType());
                    taskUpdateOut.setCoordinates(lstCoor);
                    KpiDeadlineTask kpi = sub == null ? null : new TaskDAO().getKpiDeadLineTask(sessionPos, taskManagement.getJobName(), sub.getInfraType(), sub.getProductCode(), sub.getSubType(), sub.getServiceType());
                    if (kpi != null && !"red".equalsIgnoreCase(taskManagement.getBackGroundColour())) {
                        if (kpi.getWarningTime() * 60 * 60 * 1000 <= nowDate.getTime() - taskManagement.getTaskCreateDate().getTime()) {
                            taskUpdateOut.setTextColor("yellow");
                            taskUpdateOut.setBackgroundColor("yellow");
                        } else {
                            taskUpdateOut.setBackgroundColor("green");
                        }
                    }
                    subFbConfig = new SubFbConfigBusiness().findByTaskMngtId(sessionPos, taskManagement.getTaskMngtId(), null);
                    if (subFbConfig != null) {
                        taskUpdateOut.setStatus(String.valueOf(subFbConfig.getStatus()));
                        taskUpdateOut.setStatusConfig(subFbConfig.getStatusConf() == null ? null : String.valueOf(subFbConfig.getStatusConf()));
                        taskUpdateOut.setDescriptionConfig(subFbConfig.getDescription());
                        if (subFbConfig.getReasonCode() != null && !subFbConfig.getReasonCode().trim().isEmpty()) {
                            apParams = new ApParamBussiness().findReasonAndSuggest(sessionPos, subFbConfig.getReasonCode());
                            for (ApParam ap : apParams) {
                                if (Constants.REASON_CONFIG_IMT.equals(ap.getParamType())) {
                                    taskUpdateOut.setReason(ap.getParamValue());
                                }
                                if (Constants.SUGGEST_CONFIG_IMT.equals(ap.getParamType())) {
                                    taskUpdateOut.setSuggest(ap.getParamValue());
                                }
                            }
                        }
                    }
                    if (lstCoor != null && lstCoor.size() >= 2) {
                        double d = distanceBetween2Points(lstCoor.get(0).getLat(), lstCoor.get(0).getLng(), lstCoor.get(1).getLat(), lstCoor.get(1).getLng());
                        taskUpdateOut.setDistanceToInfra(String.valueOf(d));
                    } else {
                        taskUpdateOut.setDistanceToInfra("N/A");
                    }

                    listResult.add(taskUpdateOut);
                }
                taskToAssign.setListTask(listResult);

            } else {
                taskToAssign.setErrorCode("-2");
                taskToAssign.setErrorDecription(LabelUtil.getKey("not.found.task", local));
                taskToAssign.setTotal("0");
            }
        } catch (Exception ex) {
            logger.error("searchTask: " + ex.getMessage());
            taskToAssign.setErrorCode("-99");
            taskToAssign.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            if (sessionPos != null) {
                sessionPos.close();
            }
        }
        return taskToAssign;
    }

    //truongpv edit  20/07/2017 (deduct time)updateInfrastructure
    public InforTaskToUpdateOut getInforTaskToUpdate(HibernateHelper hibernateHelper, String locale, String userLogin, String taskMngtId, String staffTaskMngtId, String telServiceId) {
        InforTaskToUpdateOut inforTaskToUpdateOut = new InforTaskToUpdateOut();
        Long sTime = System.currentTimeMillis();
        Session cmSession = null;
        Session pmSession = null;
        Session imSession = null;
        try {
            logger.error("-------->   open connection getInforTaskToUpdate ");
            logger.error("-------->   open connection SESSION_CM_POS ");
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            logger.error("-------->   open connection SESSION_PRODUCT ");
            pmSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            logger.error("-------->   open connection SESSION_IM ");
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            logger.error("-------->   open connection success getInforTaskToUpdate ");
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            ResultBO resultBO = managerTaskDAO.preUpdateTask(cmSession, imSession, pmSession, userLogin, staffTaskMngtId, telServiceId, taskMngtId);
            if (Constants.RESPONSE_SUCCESS.equals(resultBO.getResult())) {
                inforTaskToUpdateOut.setErrorCode(Constants.RESPONSE_SUCCESS);
                inforTaskToUpdateOut.setErrorDecription(Constants.SUCCESS);
                InforTaskToUpdate inforTaskToUpdate = setResultInforTaskToUpdate(resultBO.getAssignTaskBO());
                com.viettel.bccs.cm.model.TaskManagement task = new TaskManagementDAO().findById(cmSession, Long.parseLong(taskMngtId));

                if (Constants.SERVICE_ALIAS_ADSL.equals(task.getServiceType())
                        || Constants.SERVICE_ALIAS_FTTH.equals(task.getServiceType())
                        || Constants.SERVICE_ALIAS_LEASEDLINE.equals(task.getServiceType())) {
                    /* duyetdk: Bo sung them truong status duoc lay tu bang SubFBConf - start */
                    if (inforTaskToUpdate != null && inforTaskToUpdate.getTaskMngtId() != null && inforTaskToUpdate.getTaskMngtId() > 0L) {
                        SubFbConfig subFbConfig = new SubFbConfigBusiness().findByTaskMngtId(cmSession, inforTaskToUpdate.getTaskMngtId(), Constants.TYPE_CONF_IMT);
                        if (subFbConfig != null) {
                            inforTaskToUpdate.setStatus(subFbConfig.getStatus());
                        }
                    }
                    /* duyetdk -end */
                    inforTaskToUpdateOut.setInforTaskToUpdate(inforTaskToUpdate);
                    inforTaskToUpdateOut.getInforTaskToUpdate().setAccBundleTv(task.getPathFileAttach());
                    SubAdslLeaseline sub = new SubAdslLeaselineDAO().findById(cmSession, inforTaskToUpdate.getSubId());
                    boolean isVip = new SubAdslLeaselineDAO().isAccountVip(cmSession, sub.getSubType(), sub.getProductCode());
                    inforTaskToUpdate.setIsVip(isVip ? 1 : 0);
                    inforTaskToUpdate.setCustomerName(sub.getUserUsing());
                    inforTaskToUpdate.setDeployAddress(sub.getDeployAddress());
                    String tel = "";
                    if (sub.getTelFax() == null || sub.getTelFax().isEmpty()) {
                        tel = sub.getTelMobile();
                    }
                    if (sub.getTelMobile() == null || sub.getTelMobile().isEmpty()) {
                        tel = sub.getTelFax();
                    }
                    if (sub.getTelFax() != null && !sub.getTelFax().isEmpty() && sub.getTelMobile() != null && !sub.getTelMobile().isEmpty()) {
                        tel = sub.getTelMobile() + " - " + sub.getTelFax();
                    }
                    inforTaskToUpdate.setTel(tel);
                } else {
                    if (inforTaskToUpdate != null && inforTaskToUpdate.getTaskMngtId() != null && inforTaskToUpdate.getTaskMngtId() > 0L) {
                        SubFbConfig subFbConfig = new SubFbConfigBusiness().findByTaskMngtId(cmSession, inforTaskToUpdate.getTaskMngtId(), Constants.TYPE_CONF_DIGITAL);
                        if (subFbConfig != null) {
                            inforTaskToUpdate.setStatus(subFbConfig.getStatus());
                        }
                    }
                    inforTaskToUpdateOut.setInforTaskToUpdate(inforTaskToUpdate);
                    SubDigital sub = new SubDigitalDAO().findById(cmSession, inforTaskToUpdate.getSubId());
                    inforTaskToUpdate.setCustomerName(sub.getUserUsing());
                    inforTaskToUpdate.setDeployAddress(sub.getDeployAddress());
                }

                List<ReasonComplaint> lstReasonCompt = new ArrayList<ReasonComplaint>();
                List<ReasonComplaint> lstReasonDeductTime = new ArrayList<ReasonComplaint>();
                List<GroupReason> lstReasonGroup = new ArrayList<GroupReason>();
                if (inforTaskToUpdate.getIsComplaint() == 1l) {
                    inforTaskToUpdate.setCheckSOC(needCheckSOC(cmSession, inforTaskToUpdate.getTaskMngtId(), inforTaskToUpdate.getTaskStaffMngtId(), inforTaskToUpdate.getSubId()));
                }
                for (Iterator iterator = resultBO.getAssignTaskBO().getLstReason().iterator(); iterator.hasNext();) {
                    com.viettel.bccs.api.Task.BO.Reason reason = (com.viettel.bccs.api.Task.BO.Reason) iterator.next();
                    ReasonComplaint reasonComplaint = new ReasonComplaint();
                    reasonComplaint.setReasonId(reason.getReasonId());
                    reasonComplaint.setCode(reason.getCode());
                    reasonComplaint.setDescription(reason.getDescription());
                    reasonComplaint.setName(reason.getName());
                    reasonComplaint.setType(reason.getType());
                    lstReasonCompt.add(reasonComplaint);
                }
                for (Iterator iterator = resultBO.getAssignTaskBO().getLstReasonDeductTime().iterator(); iterator.hasNext();) {
                    com.viettel.bccs.api.Task.BO.Reason reason = (com.viettel.bccs.api.Task.BO.Reason) iterator.next();
                    ReasonComplaint reasonDeductTime = new ReasonComplaint();
                    reasonDeductTime.setReasonId(reason.getReasonId());
                    reasonDeductTime.setCode(reason.getCode());
                    reasonDeductTime.setDescription(reason.getDescription());
                    reasonDeductTime.setName(reason.getName());
                    reasonDeductTime.setType(reason.getType());
                    lstReasonDeductTime.add(reasonDeductTime);
                }
                for (Iterator iterator = resultBO.getAssignTaskBO().getLstGroupReason().iterator(); iterator.hasNext();) {
                    com.viettel.bccs.api.Task.BO.ApParam apParam = (com.viettel.bccs.api.Task.BO.ApParam) iterator.next();
                    GroupReason groupReason = new GroupReason();
                    groupReason.setParamId(apParam.getParamId());
                    groupReason.setGroupCode(apParam.getParamCode());
                    groupReason.setGroupName(apParam.getParamName());
                    lstReasonGroup.add(groupReason);
                }

                inforTaskToUpdateOut.setLstReason(lstReasonCompt);
                inforTaskToUpdateOut.setLstGroupReason(lstReasonGroup);
                inforTaskToUpdateOut.setLstReasonDeductTime(lstReasonDeductTime);
            } else {
                inforTaskToUpdateOut.setErrorCode(resultBO.getResult());
                inforTaskToUpdateOut.setErrorDecription(Constants.SUCCESS);
                int err;
                err = Integer.parseInt(resultBO.getResult());
                switch (err) {
                    case -1:
                        inforTaskToUpdateOut.setErrorDecription(LabelUtil.getKey("not.found.task", locale));
                        break;
                    case -2:
                        inforTaskToUpdateOut.setErrorDecription(LabelUtil.getKey("update.task.error.is.processing", locale));
                        break;
                    case -3:
                        inforTaskToUpdateOut.setErrorDecription(LabelUtil.getKey("task.assign.system.old", locale));
                        break;
                    case -4:
                        inforTaskToUpdateOut.setErrorDecription(LabelUtil.getKey("not.found.sub.for.task", locale));
                        break;
                    case -1000:
                        inforTaskToUpdateOut.setErrorDecription(LabelUtil.getKey("error.processing", locale));
                        break;
                    default:

                }
            }

        } catch (Exception ex) {
            logger.error("getInforTaskToUpdate: " + ex.getMessage());
            inforTaskToUpdateOut.setErrorCode("-99");
            inforTaskToUpdateOut.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            closeSessions(cmSession, pmSession, imSession);
            Long eTime = System.currentTimeMillis();
            LogUtils.info(logger, "getInforTaskToUpdate:time=" + (eTime - sTime));
        }
        return inforTaskToUpdateOut;
    }

    public InforResolveComplainOut getInforForResolveComplain(HibernateHelper hibernateHelper, String locale, Long custReqId, Long taskId) {
        InforResolveComplainOut inforTaskToUpdateOut = new InforResolveComplainOut();
        Session cmSession = null;
        Session ccSession = null;
        try {
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            String paramType = Constants.PARAM_TYPE_KH_LANGUAGE;
            ResolveComplain resolveComplain = new ManagerTaskDAO().preUpdateComplaint(cmSession, ccSession, custReqId, taskId);
            List<CompCause> lstCompCause = resolveComplain.getLstCompCause();

            if (lstCompCause != null && !lstCompCause.isEmpty() && Constants.PARAM_TYPE_LOCALE.equals(locale)) {
                for (CompCause compCause : lstCompCause) {
                    String paramCode = Long.toString(compCause.getCompCauseId());
                    List<ApParam> listParam = new ApParamBussiness().findByTypeCode(cmSession, paramType, paramCode);
                    if (listParam != null && !listParam.isEmpty()) {
                        ApParam apParam = listParam.get(0);
                        compCause.setName(apParam.getParamValue());
                    }
                }
            }
            if (lstCompCause != null && !lstCompCause.isEmpty()) {
                inforTaskToUpdateOut.setLstCompCause(lstCompCause);
            }

            List<CompSatisfiedLevel> lstSatisfileLevel = resolveComplain.getLstSatisfileLevel();
            if (lstSatisfileLevel != null && !lstSatisfileLevel.isEmpty()) {
                inforTaskToUpdateOut.setLstSatisfileLevel(lstSatisfileLevel);
            }
            List<CompProcess> lstCompProcess = resolveComplain.getListCompProcess();
            if (lstCompProcess != null && !lstCompProcess.isEmpty()) {
                inforTaskToUpdateOut.setLstCompProcess(lstCompProcess);
            }
            InfoComplain detailComplain = resolveComplain.getDetailComplain();
            if (detailComplain != null) {
                inforTaskToUpdateOut.setDetailComplain(detailComplain);
            }
            inforTaskToUpdateOut.setErrorCode("0");
            inforTaskToUpdateOut.setErrorDecription(Constants.SUCCESS);
        } catch (Exception ex) {
            logger.error("getInforTaskToUpdate: " + ex.getMessage());
            inforTaskToUpdateOut.setErrorCode("-99");
            inforTaskToUpdateOut.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            closeSessions(cmSession);
            closeSessions(ccSession);
        }
        return inforTaskToUpdateOut;
    }

    public InforInfrastructureOut getInforForInfrastructure(HibernateHelper hibernateHelper, String locale, Long taskStaffMngtId, Long taskMngtId, Long telServiceId, boolean isView) throws Exception {
        InforInfrastructureOut inforInfrastructureOut = new InforInfrastructureOut();
        Session cmSession = null;
        Session imSession = null;
        Session pmSession = null;
        Session nims = null;
        try {
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            pmSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            nims = hibernateHelper.getSession(Constants.SESSION_NIMS);
            List<com.viettel.bccs.cm.model.TaskManagement> list = new TaskManagementDAO().findByProperty(cmSession, "taskMngtId", taskMngtId);
            if (isView) {
                if (list == null || list.isEmpty()) {
                    return inforInfrastructureOut;
                }
                List<TaskShopManagement> taskShop = new TaskShopManagementDAO().findByProperty(cmSession, "taskMngtId", taskMngtId);
                if (taskShop == null || taskShop.isEmpty()) {
                    return inforInfrastructureOut;
                }
                List<TaskStaffManagement> taskStaff = new TaskStaffManagementDAO().findByProperty(cmSession, "taskShopMngtId", taskShop.get(0).getTaskShopMngtId());
                if (taskStaff == null || taskStaff.isEmpty()) {
                    return inforInfrastructureOut;
                }
                taskStaffMngtId = taskStaff.get(0).getTaskStaffMngtId();
                telServiceId = "P".equals(list.get(0).getServiceType()) ? Constant.SERVICE_PSTN_ID_WEBSERVICE : Constant.SERVICE_ADSL_ID_WEBSERVICE;
                if (Constant.SERVICE_ALIAS_CAMERA.equals(list.get(0).getServiceType())) {
                    telServiceId = Constant.SERVICE_DIGITAL_ID_WEBSERVICE;
                }
            }
            Infastructure infastructure = new ManagerTaskDAO().preUpdateInfastructure(cmSession, imSession, pmSession, nims, taskStaffMngtId, taskMngtId, telServiceId, isView);
//            Infastructure infastructure = new Infastructure();
            String error = infastructure.getResult();
            if (Constants.ERROR_CODE_0.equals(error)) {
                inforInfrastructureOut.setServiceType(infastructure.getServiceType());
                inforInfrastructureOut.setCouplerNo(infastructure.getCouplerNo());
                List<ApParam> listParam = new ApParamBussiness().findByType(cmSession, Constants.MAX_DISTANCE_IMAGE);
                String maxDistanceImage = listParam == null || listParam.isEmpty() ? "50" : listParam.get(0).getParamValue();
                inforInfrastructureOut.setMaxDistanceImage(maxDistanceImage);

                List<Boards> lstBoard = infastructure.getLstBoard();
                if (lstBoard != null && !lstBoard.isEmpty()) {
                    inforInfrastructureOut.setLstBoard(lstBoard);
                }
                List<CableBox> lstCableBox = infastructure.getLstCableBox();
                if (lstCableBox != null && !lstCableBox.isEmpty()) {
                    inforInfrastructureOut.setLstCableBox(lstCableBox);
                }
                List<SubStockModelRel> lstGood = infastructure.getLstGood();
                if (lstGood != null && !lstGood.isEmpty()) {
                    inforInfrastructureOut.setLstGood(lstGood);
                }
                List<Dslam> lstDslam = infastructure.getDsLam();
                if (lstDslam != null && !lstDslam.isEmpty()) {
                    inforInfrastructureOut.setLstDslam(lstDslam);
                }
                List<EquipmentPort> lstEquipmentPort = infastructure.getLstPort();
                if (lstEquipmentPort != null && !lstEquipmentPort.isEmpty()) {
                    inforInfrastructureOut.setLstPort(lstEquipmentPort);
                }
                List<GetBranchNodeByStationResutForm> lstBranchGpon = infastructure.getLstBranchGpon();
                if (lstBranchGpon != null && !lstBranchGpon.isEmpty()) {
                    inforInfrastructureOut.setLstBranchGpon(lstBranchGpon);
                }
                List<GetSubNodeByBranchNodeResultForm> lstSubBranchGpon = infastructure.getLstSubBranchGpon();
                if (lstSubBranchGpon != null && !lstSubBranchGpon.isEmpty()) {
                    inforInfrastructureOut.setLstSubBranchGpon(lstSubBranchGpon);
                }
                List<GetSplitterBySubNodeResultForm> lstSpitterGpon = infastructure.getLstSpitterGpon();
                if (lstSpitterGpon != null && !lstSpitterGpon.isEmpty()) {
                    inforInfrastructureOut.setLstSpitterGpon(lstSpitterGpon);
                }
                List<GetPortBySplitterResultForm> lstPortGpon = infastructure.getLstPortGpon();
                if (lstPortGpon != null && !lstPortGpon.isEmpty()) {
                    inforInfrastructureOut.setLstPortGpon(lstPortGpon);
                }
                List<com.viettel.bccs.api.Task.BO.APStockModelBean> lstItemLib = infastructure.getLstItem();
                inforInfrastructureOut.setLstItem(lstItemLib);

                InfastructureDetail detail = new InfastructureDetail();
                detail.setIsComplainTask(infastructure.getIsComplaint());
                detail.setNeedInfa(infastructure.getNeedInfa());
                SubDeployment subDep = infastructure.getSubDep();
                if (subDep != null) {
                    if (infastructure.getIsComplaint()) {
                        if (infastructure.getIsGpon()) {
                            if (infastructure.getLstSubBranchGpon() != null && !infastructure.getLstSubBranchGpon().isEmpty()) {
                                detail.setCableBoxId(infastructure.getLstSubBranchGpon().get(0).getSubNodeId());
                            }
                            if (infastructure.getLstSpitterGpon() != null && !infastructure.getLstSpitterGpon().isEmpty()) {
                                detail.setSpitterId(infastructure.getLstSpitterGpon().get(0).getSplitterId());
                            }
                            if (infastructure.getLstPortGpon() != null && !infastructure.getLstPortGpon().isEmpty()) {
                                detail.setPortNo(infastructure.getLstPortGpon().get(0).getPortId());
                            }
                        } else {
                            if (infastructure.getDsLam() != null && !infastructure.getDsLam().isEmpty()) {
                                detail.setDslamId(infastructure.getDsLam().get(0).getDslamId());
                            }
                            if (infastructure.getLstPort() != null && !infastructure.getLstPort().isEmpty()) {
                                detail.setPortNo(infastructure.getLstPort().get(0).getId());
                            }
                            if (infastructure.getLstBoard() != null && !infastructure.getLstBoard().isEmpty()) {
                                detail.setBoardId(infastructure.getLstBoard().get(0).getBoardId());
                            }
                            if (infastructure.getLstCableBox() != null && !infastructure.getLstCableBox().isEmpty()) {
                                detail.setCableBoxId(infastructure.getLstCableBox().get(0).getCableBoxId());
                            }
                        }
                    } else {
                        detail.setBoardId(subDep.getBoardId());
                        detail.setPlateA(subDep.getPlateA());
                        detail.setPlateB(subDep.getPlateB());
                        detail.setPlateCableBoxA(subDep.getPlateCableBoxA());
                        detail.setPlateCableBoxB(subDep.getPlateCableBoxB());
                        detail.setLineNo(subDep.getLineNo());
                        detail.setPortNo(subDep.getPortNo());
                        detail.setDslamId(subDep.getDslamId());/*giá trị dùng để search Cable theo dslamId*/
                        detail.setCableBoxId(subDep.getCableBoxId());
                        detail.setStationId(subDep.getStationId());
                        detail.setSpitterId(subDep.getSpitterId());
                    }
                }
                inforInfrastructureOut.setIsGpon(infastructure.getIsGpon());
                inforInfrastructureOut.setIsSplitter(infastructure.getIsSplitter());
                inforInfrastructureOut.setInfastructureDetail(detail);
                inforInfrastructureOut.setErrorCode("0");
                inforInfrastructureOut.setErrorDecription(Constants.SUCCESS);


                /*Get image for infra and good*/
                SubFbConfig subFbConf = new SubFbConfigBusiness().findBySubId(cmSession, list.get(0).getSubId(), null, Constants.TYPE_CONF_IMT);
                if (Constants.SERVICE_ALIAS_CAMERA.equals(list.get(0).getServiceType())) {
                    subFbConf = new SubFbConfigBusiness().findBySubId(cmSession, list.get(0).getSubId(), null, Constants.TYPE_CONF_DIGITAL);
                }
                if (list.get(0).getJobCode().equals(Constants.OTHER_PRO_BUNDLE_TV)) {
                    subFbConf = new SubFbConfigBusiness().findByTaskMngtId(cmSession, taskMngtId, Constants.TYPE_CONF_TV);
                }
                if (subFbConf != null && !isView) {
                    CustImageDetailDAO custImageDetailDAO = new CustImageDetailDAO();
                    /*Get cust image id -> imageGood*/
                    List<NewImageInput> listImage;
                    if (subFbConf.getCustImgId() != null) {
                        listImage = custImageDetailDAO.findById(cmSession, subFbConf.getCustImgId(), Constants.GOOD_IMG + "_" + subFbConf.getAccount(), logger, Constants.FTP_DIR_TASK);
                        inforInfrastructureOut.setImageGood(listImage);
                    }
                    /*Get infra image id -> imageInfra*/
                    if (subFbConf.getIfraImgId() != null) {
                        listImage = custImageDetailDAO.findById(cmSession, subFbConf.getIfraImgId(), Constants.INFRA_IMG + "_" + subFbConf.getAccount(), logger, Constants.FTP_DIR_TASK);
                        inforInfrastructureOut.setImageInfra(listImage);
                    }
                }
            } else {
                inforInfrastructureOut.setErrorCode(error);
                int err;
                err = Integer.parseInt(error);
                switch (err) {
                    case -1:
                        inforInfrastructureOut.setErrorDecription(LabelUtil.getKey("not.found.task", locale));
                        break;
                    case -2:
                        inforInfrastructureOut.setErrorDecription(LabelUtil.getKey("a0865.cannot.find.sub.of.task", locale));
                        break;
                    case -3:
                        inforInfrastructureOut.setErrorDecription(LabelUtil.getKey("a0862.cannot.find.assigned.shop", locale));
                        break;
                    case -4:
                        inforInfrastructureOut.setErrorDecription(LabelUtil.getKey("a0907.task.not.begin", locale));
                        break;
                    case -5:
                        inforInfrastructureOut.setErrorDecription(LabelUtil.getKey("a0908.cannot.find.contract.of.task", locale));
                        break;
                    case -6:
                        inforInfrastructureOut.setErrorDecription(LabelUtil.getKey("a0961.cannot.find.sub.deployment", locale));
                        break;
                    default:
                }

            }
        } catch (Exception ex) {
            logger.error("getInforTaskToUpdate: " + ex.getMessage());
            inforInfrastructureOut.setErrorCode("-99");
            inforInfrastructureOut.setErrorDecription("System is busy :" + ex.getMessage());
            throw ex;
        } finally {
            closeSessions(cmSession);
            closeSessions(imSession);
            closeSessions(nims);
            closeSessions(pmSession);

        }
        return inforInfrastructureOut;
    }

    public CableBoxOut getCableBox(HibernateHelper hibernateHelper, String locale, Long boardId, Long dslamId, Long stationId, Long telserviceId, Long subId) {
        CableBoxOut cableBoxOut = new CableBoxOut();
        Session imSession = null;
        Session cmSession = null;
        Session pmSession = null;
        try {
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            pmSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            List<CableBox> lstCableBox = new ManagerTaskDAO().getCableBox(imSession, cmSession, pmSession, boardId, dslamId, stationId, telserviceId, subId);
            if (lstCableBox != null && !lstCableBox.isEmpty()) {
                cableBoxOut.setLstCableBox(lstCableBox);
                cableBoxOut.setErrorCode("0");
                cableBoxOut.setErrorDecription(Constants.SUCCESS);
            } else {
                cableBoxOut.setErrorCode("0");
                cableBoxOut.setErrorDecription(Constants.SUCCESS);
            }
        } catch (Exception ex) {
            logger.error("cableBoxOut: " + ex.getMessage());
            cableBoxOut.setErrorCode("-99");
            cableBoxOut.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            closeSessions(imSession, cmSession, pmSession);
        }
        return cableBoxOut;
    }

    /**
     *
     * @param inforTaskToUpdate
     * @param assignTaskBO
     */
    InforTaskToUpdate setResultInforTaskToUpdate(AssignTaskBO assignTaskBO) {
        InforTaskToUpdate inforTaskToUpdate = new InforTaskToUpdate();
        inforTaskToUpdate.setCustReqId(assignTaskBO.getCustReqId());
        inforTaskToUpdate.setEndDateValue(assignTaskBO.getEndDateValue());
        inforTaskToUpdate.setIsChangeEquipment(assignTaskBO.getIsChangeEquipment());
        inforTaskToUpdate.setIsComplaint(assignTaskBO.getIsComplaint());
        inforTaskToUpdate.setIsFTTH(assignTaskBO.getIsFTTH());
        inforTaskToUpdate.setIsKNSC(assignTaskBO.getIsKNSC());
        inforTaskToUpdate.setIsLackPlaceCableBox(assignTaskBO.getIsLackPlaceCableBox());
        inforTaskToUpdate.setIsLackSerial(assignTaskBO.getIsLackSerial());
        inforTaskToUpdate.setReqType(assignTaskBO.getReqType());
        inforTaskToUpdate.setStaffName(assignTaskBO.getStaffName());
        inforTaskToUpdate.setStartDateActual(assignTaskBO.getStartDateActual());
        inforTaskToUpdate.setStartDateValue(assignTaskBO.getStartDateValue());
        inforTaskToUpdate.setSubId(assignTaskBO.getSubId());
        inforTaskToUpdate.setTaskMngtId(assignTaskBO.getTaskMngtId());
        inforTaskToUpdate.setTaskName(assignTaskBO.getTaskName());
        inforTaskToUpdate.setTaskStaffMngtId(assignTaskBO.getTaskStaffMngtId());
        inforTaskToUpdate.setTaskStaffProgress(assignTaskBO.getTaskStaffProgress());
        inforTaskToUpdate.setTelServiceId(assignTaskBO.getTelServiceId());
        inforTaskToUpdate.setNote(assignTaskBO.getDescription());
        inforTaskToUpdate.setReasonId(assignTaskBO.getReasonId());
        inforTaskToUpdate.setGroupReasonCode(assignTaskBO.getReasonGroup());
        inforTaskToUpdate.setServiceType(assignTaskBO.getServiceType());
        inforTaskToUpdate.setJobCode(assignTaskBO.getJobCode());
        return inforTaskToUpdate;
    }

    public ResultUpdateTaskOut updateTask(HibernateHelper hibernateHelper, String locale, UpdateTaskIn updateTaskIn, String token) {
        ResultUpdateTaskOut resultUpdateTaskOut = new ResultUpdateTaskOut();
        Session cmSession = null;
        Session cmPreSession = null;
        Session ccSession = null;
        Session imSession = null;
        Session pmSession = null;
        Session paymentSession = null;
        Session biSession = null;
        Session nims = null;
        boolean hasErr = false;
        StringBuilder request = new StringBuilder();
        String userUpdate = "";
        SubFbConfig subFbConfig = new SubFbConfig();
        SubBundleTvLog log = null;
        try {
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            pmSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            paymentSession = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            biSession = hibernateHelper.getSession(Constants.SESSION_BI);
            nims = hibernateHelper.getSession(Constants.SESSION_NIMS);
            Token tokenObj = new TokenBussiness().findByToken(cmSession, token);
            if (tokenObj != null) {
                userUpdate = tokenObj.getUserName();
            }
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            Long subId;
            Long taskStaffMngtId;
            Long telServiceId;
            Long reasonId;
            Long compCauseId;
            Long satisfileLevel;
            Long isSystemError;
            try {
                try {
                    subId = updateTaskIn.getSubId().isEmpty() ? null : Long.valueOf(updateTaskIn.getSubId());
                } catch (Exception ex) {
                    logger.error("updateTask: " + ex.getMessage());
                    resultUpdateTaskOut.setErrorCode("-98");
                    resultUpdateTaskOut.setErrorDecription("subId is invalid: " + updateTaskIn.getSubId());
                    return resultUpdateTaskOut;
                }
                try {
                    taskStaffMngtId = updateTaskIn.getTaskStaffMngtId().isEmpty() ? null : Long.valueOf(updateTaskIn.getTaskStaffMngtId());
                } catch (Exception ex) {
                    logger.error("updateTask: " + ex.getMessage());
                    resultUpdateTaskOut.setErrorCode("-98");
                    resultUpdateTaskOut.setErrorDecription("iaskStaffMngtId param is invalid : " + updateTaskIn.getTaskStaffMngtId());
                    return resultUpdateTaskOut;
                }
                try {
                    telServiceId = updateTaskIn.getTelServiceId().isEmpty() ? null : Long.valueOf(updateTaskIn.getTelServiceId());
                } catch (Exception ex) {
                    logger.error("updateTask: " + ex.getMessage());
                    resultUpdateTaskOut.setErrorCode("-98");
                    resultUpdateTaskOut.setErrorDecription("TelServiceId input param is invalid + " + updateTaskIn.getTelServiceId());
                    return resultUpdateTaskOut;
                }
                try {
                    reasonId = updateTaskIn.getReasonId().isEmpty() ? null : Long.valueOf(updateTaskIn.getReasonId());
                } catch (Exception ex) {
                    logger.error("updateTask: " + ex.getMessage());
                    resultUpdateTaskOut.setErrorCode("-98");
                    resultUpdateTaskOut.setErrorDecription("ReasonId input param is invalid + " + updateTaskIn.getReasonId());
                    return resultUpdateTaskOut;
                }
                try {
                    compCauseId = updateTaskIn.getCompCauseId().isEmpty() ? null : Long.valueOf(updateTaskIn.getCompCauseId());
                } catch (Exception ex) {
                    logger.error("updateTask: " + ex.getMessage());
                    resultUpdateTaskOut.setErrorCode("-98");
                    resultUpdateTaskOut.setErrorDecription("CompCauseId input param is invalid: " + updateTaskIn.getCompCauseId());
                    return resultUpdateTaskOut;
                }
                try {
                    satisfileLevel = updateTaskIn.getSatisfileLevel().isEmpty() ? null : Long.valueOf(updateTaskIn.getSatisfileLevel());
                } catch (Exception ex) {
                    logger.error("updateTask: " + ex.getMessage());
                    resultUpdateTaskOut.setErrorCode("-98");
                    resultUpdateTaskOut.setErrorDecription("SatisfileLevel param is invalid: " + updateTaskIn.getSatisfileLevel());
                    return resultUpdateTaskOut;
                }
                try {
                    isSystemError = updateTaskIn.getIsSystemError().isEmpty() ? null : Long.valueOf(updateTaskIn.getIsSystemError());
                } catch (Exception ex) {
                    logger.error("updateTask: " + ex.getMessage());
                    resultUpdateTaskOut.setErrorCode("-98");
                    resultUpdateTaskOut.setErrorDecription("IsSystemError param is invalid: " + updateTaskIn.getIsSystemError());
                    return resultUpdateTaskOut;
                }
            } catch (Exception ex) {
                logger.error("updateTask: " + ex.getMessage());
                resultUpdateTaskOut.setErrorCode("-98");
                resultUpdateTaskOut.setErrorDecription("input param is invalid");
                return resultUpdateTaskOut;
            }
            //duyetdk edit 05/04/2018
            com.viettel.bccs.api.Task.BO.TaskStaffManagement tsm = new com.viettel.bccs.api.Task.DAO.TaskStaffManagementDAO().findById(taskStaffMngtId, cmSession);
            com.viettel.bccs.api.Task.BO.TaskShopManagement taskShop = new com.viettel.bccs.api.Task.DAO.TaskShopManagementDAO().findByTaskShopId(cmSession, tsm.getTaskShopMngtId());
            com.viettel.bccs.api.Task.BO.TaskManagement task = new com.viettel.bccs.api.Task.DAO.TaskManagementDAO().findById(cmSession, taskShop.getTaskMngtId());

            /*xu ly luong cong viec cho thue bao k thuoc vien thong*/
            if (!Constants.SERVICE_ALIAS_ADSL.equals(task.getServiceType())
                    && !Constants.SERVICE_ALIAS_FTTH.equals(task.getServiceType())
                    && !Constants.SERVICE_ALIAS_LEASEDLINE.equals(task.getServiceType())) {
                TaskForUpdateControllerDigital taskController = null;
                /*camera*/
                if (Constants.SERVICE_ALIAS_CAMERA.equals(task.getServiceType())) {
                    taskController = new TaskForUpdateControllerCamera();
                }
                if (taskController != null) {
                    resultUpdateTaskOut = taskController.updateTask(cmPreSession, cmSession, ccSession,
                            imSession, pmSession, paymentSession, biSession, nims, locale, updateTaskIn, task);
                }
                if (!Constants.RESPONSE_SUCCESS.equals(resultUpdateTaskOut.getErrorCode())) {
                    hasErr = true;
                }
                return resultUpdateTaskOut;
            }

            subFbConfig = new SubFbConfigBusiness().findByTaskMngtId(cmSession, taskShop.getTaskMngtId(), Constants.TYPE_CONF_IMT);
            SubFbConfig subFbConfigTv = new SubFbConfigBusiness().findByTaskMngtId(cmSession, taskShop.getTaskMngtId(), Constants.TYPE_CONF_TV);
            Long status = null;
            Long statusConfig = null;
            if (subFbConfig != null && subFbConfig.getTaskManagementId().equals(task.getTaskMngtId())) {
                status = subFbConfig.getStatus();
                statusConfig = subFbConfig.getStatusConf();
                if (subFbConfigTv != null && Constants.APPROVED.equals(status)) {
                    if (Constants.APPROVED.equals(statusConfig) && !statusConfig.equals(subFbConfigTv.getStatusConf())) {
                        statusConfig = subFbConfigTv.getStatusConf();
                    }
                }
            } else {
                if (subFbConfigTv != null) {
                    status = subFbConfigTv.getStatus();
                    statusConfig = subFbConfigTv.getStatusConf();
                }
            }


            /*
             * @since 06092018u
             * @author Cuongdm
             * @des Bo sung check SOC truoc khi dong compla Cuongdm
             * @des Bo sung cin
             */
 /*Kiem tra dung dan cua progress*/
            ResultBO resultBO = new ResultBO();
            resultBO.setResult("1");
            StringBuilder statusConfSocStr = new StringBuilder();
            resultBO = checkSocComplaint(cmSession, taskStaffMngtId, updateTaskIn.getTaskStaffProgress(), updateTaskIn.getLoginName(), statusConfSocStr, locale);
            Long statusConfSoc = Long.parseLong(statusConfSocStr.toString());
            TaskForUpdateControllerCamera taskForUpdateControllerCamera = new TaskForUpdateControllerCamera();
            SubAdslLeaseline subAdslLL = new SubAdslLeaselineDAO().findById(cmSession, subId);
            SubBundleTv subTv = new SubBundleTvDAO().findBundleTvByAccount(cmSession, task.getPathFileAttach());
            if (subAdslLL.getAccBundleTv() != null && subTv != null && subTv.getUserUniqueId() == null) {
                Contract contract = new ContractDAO().findById(subAdslLL.getContractId(), cmSession);
                Customer customer = new CustomerDAO().findById(cmSession, contract.getCustId());
                if ("true".equalsIgnoreCase(com.viettel.brcd.common.util.ResourceBundleUtils.getResource("provisioning.allow.send.request", "provisioning"))) {
                    Request requestBundle = new Request();
                    requestBundle.addUser(customer, subTv.getAccount());
                    String data = new Gson().toJson(requestBundle);
                    String response = new ServiceSupplier().sendRequest(Constants.SERVICE_METHOD_POST, ResourceUtils.getResource("BUNDLE_TV_SERVICE"), data, null, null);
                    if (response == null || response.isEmpty()) {
                        hasErr = true;
                        resultUpdateTaskOut.setErrorDecription("Error from call service addUser bunlde tv");
                    } else {
                        WSRespone ws = new Gson().fromJson(response, WSRespone.class);
                        ServiceResponse serviceResponse = ws == null || ws.getErrorDecription() == null ? null : new Gson().fromJson(ws.getErrorDecription(), ServiceResponse.class);
                        if (serviceResponse == null || !Constants.RESPONSE_SUCCESS.equals(serviceResponse.getResult())
                                || serviceResponse.getContent() == null) {
                            resultUpdateTaskOut.setErrorDecription("Error from call service addUser bunlde tv " + (serviceResponse != null ? serviceResponse.getMessage() : ""));
                            hasErr = true;
                        } else {
                            subTv.setUserUniqueId(serviceResponse.getContent().getUnique_id());
                            cmSession.save(subTv);
                            cmSession.flush();
                        }
                    }

                    log = new SubBundleTvLog();
                    log.setSubId(subId);
                    log.setAction(Request.ACTION_ADD_USER);
                    log.setAccount(subAdslLL.getAccBundleTv());
                    log.setRequest(data);
                    log.setResponse(response);
                    log.setCreateUser(userUpdate);
                    log.setCreateDate(new Date());
                }
            }
            //truongpv edit 20/07/2017 (deduct time)
            if (Constants.APPROVED.equals(statusConfSoc) && !hasErr) {
                resultBO = managerTaskDAO.updateProgress(cmPreSession, cmSession, ccSession, imSession, pmSession, paymentSession, biSession,
                        subId, taskStaffMngtId, telServiceId, updateTaskIn.getTaskStaffProgress(), isSystemError,
                        updateTaskIn.getNote(), updateTaskIn.getLoginName(), updateTaskIn.getShopCodeLogin(),
                        reasonId, updateTaskIn.getReasonGroup(), updateTaskIn.getComplaintResult(), satisfileLevel, compCauseId,
                        updateTaskIn.getIsDeductTime(), updateTaskIn.getStartTimeDeduct(), updateTaskIn.getEndTimeDeduct(),
                        updateTaskIn.getReasonDeduct(), status, statusConfig, nims);
            }

            if (Constants.RESPONSE_SUCCESS.equals(resultBO.getResult())) {
                if (Constants.FINISH_TASK.equals(updateTaskIn.getTaskStaffProgress())) {
                    if (subAdslLL.getReqOnlineId() != null) {
                        RegOnlineDAO regOnlineDAO = new RegOnlineDAO();
                        RegOnline requestOnl = regOnlineDAO.getRegOnlineById(cmSession, subAdslLL.getReqOnlineId());
                        if (requestOnl != null) {
                            regOnlineDAO.saveActionLog(cmSession, subAdslLL.getReqOnlineId(), "STATUS", requestOnl.getStatus().toString(), RegisterFTTHOnlineController.STATUS_CONNECTED.toString(), updateTaskIn.getLoginName(), "Connected account");
                            requestOnl.setStatus(RegisterFTTHOnlineController.STATUS_CONNECTED);
                            requestOnl.setUpdateUser(updateTaskIn.getLoginName());
                            requestOnl.setUpdateDate(new Date());
                            /*add commission cho dealer*/
                            if (Constants.SHOP_DIRECT.equals(requestOnl.getType())) {
                                String sql = "SELECT product_code FROM product p, product_offer o WHERE p.product_id = o.product_id and offer_code = ?";
                                Query query = pmSession.createSQLQuery(sql);
                                query.setParameter(0, subAdslLL.getProductCode());
                                List list = query.list();
                                if (list == null || list.isEmpty()) {
                                    requestOnl.setDescription("01: can not pay commission");
                                } else {
                                    sql = "select ROUND (daily_charge * 30 * 1.1) fee from rt_daily_charge_config where product_code = ?";
                                    query = cmSession.createSQLQuery(sql);
                                    query.setParameter(0, list.get(0).toString());
                                    list = query.list();
                                    if (list == null || list.isEmpty()) {
                                        requestOnl.setDescription("02: can not pay commission");
                                    } else {
                                        Double fee = Double.parseDouble(list.get(0).toString());
                                        Reason reason = new ReasonDAO().findByCode(cmSession, subAdslLL.getRegType());
                                        Double commission = null;
                                        if (reason == null || reason.getPayAdvAmount() == null) {
                                            requestOnl.setDescription("02: can not pay commission");
                                        } else {
                                            Double month = reason.getPayAdvAmount() / fee;
                                            List<ApParam> listCommission = new ApParamBussiness().findByType(cmSession, "REG_FTTH_COMMISSION_DEALER");
                                            Collections.sort(listCommission, new Comparator<ApParam>() {
                                                @Override
                                                public int compare(ApParam param1, ApParam param2) {
                                                    Long l1 = Long.parseLong(param1.getParamCode());
                                                    Long l2 = Long.parseLong(param2.getParamCode());
                                                    return l1.compareTo(l2);
                                                }
                                            });
                                            for (ApParam param : listCommission) {
                                                /*check fee*/
                                                if (fee <= Double.parseDouble(param.getParamName())) {
                                                    /*check month payment*/
                                                    String[] arr = param.getParamValue().split(";");
                                                    for (int i = 0; i < arr.length; i++) {
                                                        String[] val = arr[i].split("-");
                                                        if (month <= Double.parseDouble(val[0].toString())) {
                                                            AccountAgent accountAgent = new ImSupplier().checkAccountAgentByIsdn(imSession, requestOnl.getCode());
                                                            commission = Double.parseDouble(val[1].toString());
                                                            sql = "insert into bccs_im.log_pay_emoney(id,sender,account_emoney,create_date,status,amount_commision,description,type, owner_code)\n"
                                                                    + "values(bccs_im.log_pay_emoney_seq.nextval, ?,?,sysdate,1,?,?,'REG_FTTH_ONL', ?)";
                                                            query = imSession.createSQLQuery(sql);
                                                            query.setParameter(0, requestOnl.getCode());
                                                            query.setParameter(1, requestOnl.getCode());
                                                            query.setParameter(2, commission * 100);
                                                            /*doi ra cent*/
                                                            query.setParameter(3, "Commission for account " + subAdslLL.getAccount() + " with reason: " + reason.getCode());
                                                            query.setParameter(4, accountAgent.getOwnerCode());
                                                            query.executeUpdate();

                                                            /*gui tin nhan*/
                                                            List<String> listParamSms = new ArrayList<String>();
                                                            listParamSms.add(requestOnl.getCustomerName());
                                                            listParamSms.add(requestOnl.getId().toString());
                                                            listParamSms.add(commission.toString());
                                                            new com.viettel.bccs.cm.controller.payment.PaymentController().sendSMS(cmPreSession, requestOnl.getCode(), Constants.AP_PARAM_PARAM_TYPE_SMS, "REG_OLN_FTHH_COMISSION", listParamSms);

                                                            break;
                                                        }
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            cmSession.save(requestOnl);
                            cmSession.flush();
                        }
                    }
                }
                if (Constants.REQUEST_CONFIG.equals(updateTaskIn.getTaskStaffProgress())) {
                    if (!Constants.OTHER_PRO_BUNDLE_TV.equals(task.getJobCode())) {
                        String mess = new SubFbConfigBusiness().requestConfig(cmSession, subId, userUpdate, locale, request, subFbConfig);
                        if (mess != null) {
                            mess = mess.trim();
                            if (!mess.isEmpty()) {
                                hasErr = true;
                                resultUpdateTaskOut = new ResultUpdateTaskOut(Constants.ERROR_CODE_1, mess);
                            }
                        } else {
                            /*push notification thong bao cho nhan vien vao config*/
                            taskForUpdateControllerCamera.processStepRequestConfig(cmSession, cmPreSession, subFbConfig.getAccount(), updateTaskIn.getLoginName(),
                                    taskStaffMngtId, Constants.PARAM_TYPE_STAFF_IMT, locale);
                        }
                    } else {
                        if (subFbConfigTv != null && (Constants.PENDING.equals(subFbConfigTv.getStatus()) || Constants.REJECT.equals(subFbConfigTv.getStatus()))) {
                            subFbConfigTv.setStatus(Constants.WAITING);
                            cmSession.save(subFbConfigTv);
                        }
                    }
                }
                if (!hasErr) {
                    resultUpdateTaskOut.setErrorCode(Constants.RESPONSE_SUCCESS);
                    resultUpdateTaskOut.setErrorDecription(LabelUtil.getKey("common.success", locale));
                }
            } else {
                String des = taskForUpdateControllerCamera.processResultError(resultBO, locale);
                resultUpdateTaskOut.setErrorCode(resultBO.getResult());
                resultUpdateTaskOut.setErrorDecription(des);
                hasErr = true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("updateTask: " + ex.getMessage());
            resultUpdateTaskOut.setErrorCode("-99");
            resultUpdateTaskOut.setErrorDecription("System is busy :" + ex.getMessage());
            hasErr = true;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmSession, cmPreSession, ccSession, imSession, pmSession, paymentSession, biSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            } else {
                //Chuyen commit trong lib
                if (Constants.REQUEST_CONFIG.equals(updateTaskIn.getTaskStaffProgress()) && subFbConfig != null && subFbConfig.getId() != null) {
                    new SubFbConfigSupplier().insertLog(cmSession, subFbConfig.getId(), subFbConfig.getStatusConf(), request.toString(), new Gson().toJson(resultUpdateTaskOut), "CREATE_REQ_CONFIG", resultUpdateTaskOut.getErrorDecription(), new Date(), userUpdate, "0");
                }
                commitTransactions(cmSession, cmPreSession, ccSession, imSession, pmSession, paymentSession, biSession);
            }
            if (log != null) {
                cmSession.save(log);
                cmSession.flush();
                commitTransactions(cmSession);
            }
            closeSessions(cmSession, cmPreSession, ccSession, imSession, pmSession, paymentSession, biSession, nims);
        }
        return resultUpdateTaskOut;
    }

    public WSRespone updateNoteTask(HibernateHelper hibernateHelper, String locale, UpdateNoteIn updateNoteIn) {
        WSRespone wSRespone = new WSRespone();
        Session cmSession = null;
        Session ccSession = null;
        try {
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            Long subId = null;
            Long taskMngtId = null;
            Long taskStaffMngtId = null;
            Long reasonId = null;
            try {
                subId = updateNoteIn.getSubId().isEmpty() ? null : Long.valueOf(updateNoteIn.getSubId());
                taskMngtId = updateNoteIn.getTaskMngtId().isEmpty() ? null : Long.valueOf(updateNoteIn.getTaskMngtId());
                taskStaffMngtId = updateNoteIn.getTaskStaffMngtId().isEmpty() ? null : Long.valueOf(updateNoteIn.getTaskStaffMngtId());
                reasonId = updateNoteIn.getReasonId().isEmpty() ? null : Long.valueOf(updateNoteIn.getReasonId());
            } catch (Exception ex) {
                logger.error("updateNoteTask: " + ex.getMessage());
                wSRespone.setErrorCode("-98");
                wSRespone.setErrorDecription("input param is invalid");
                return wSRespone;
            }

            ResultBO resultBO = new ManagerTaskDAO().updateNote(cmSession, ccSession, subId,
                    taskMngtId, taskStaffMngtId, updateNoteIn.getNote(), reasonId, updateNoteIn.getReasonGroup(), updateNoteIn.getLoginName(), updateNoteIn.getShopCodeLogin());
            if (resultBO.getResult().equals(Constants.RESPONSE_SUCCESS)) {
                wSRespone.setErrorCode(Constants.RESPONSE_SUCCESS);
                wSRespone.setErrorDecription(LabelUtil.getKey("common.success", local));

                commitTransactions(cmSession);
                commitTransactions(ccSession);
            } else {
                wSRespone.setErrorCode(resultBO.getResult());
                int result = Integer.parseInt(resultBO.getResult());
                switch (result) {
                    case -1:
                        wSRespone.setErrorDecription(LabelUtil.getKey("update.fail.not.found.task", locale));
                        break;
                    case -2:
                        wSRespone.setErrorDecription(LabelUtil.getKey("update.fail.task.finish", locale));
                        break;
                    case -3:
                        wSRespone.setErrorDecription(LabelUtil.getKey("update.fail.not.found.team", locale));
                        break;
                    case -4:
                        wSRespone.setErrorDecription(LabelUtil.getKey("update.fail.not.found.task.staff", locale));
                        break;
                    case -5:
                        wSRespone.setErrorDecription(LabelUtil.getKey("update.fail.error.cc", locale));
                        break;
                    default:
                        wSRespone.setErrorDecription(LabelUtil.getKey("update.task.error.is.processing", locale));

                }

                rollBackTransactions(cmSession);
                rollBackTransactions(ccSession);
            }
        } catch (Exception ex) {
            rollBackTransactions(cmSession);
            rollBackTransactions(ccSession);
            wSRespone.setErrorCode("-99");
            wSRespone.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            closeSessions(cmSession);
            closeSessions(ccSession);
        }
        return wSRespone;
    }

    public WSRespone updateInfrastructure(HibernateHelper hibernateHelper, String locale, InfrastructureUpdateIn infrastructureUpdateIn, String token) {
        Session cmSession = null;
        Session imSession = null;
        Session pmSession = null;
        Session nimsSession = null;
        WSRespone wSRespone = new WSRespone();
        boolean hasErr = false;
        StringBuilder request = new StringBuilder();
        String userUpdate = "";
        SubFbConfig subFbConf = new SubFbConfig();
        List<SubBundleTvLog> listLog = new ArrayList<SubBundleTvLog>();
        try {
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            pmSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            nimsSession = hibernateHelper.getSession(Constants.SESSION_NIMS);
            Token tokenObj = new TokenBussiness().findByToken(cmSession, token);

            Long staffId = null;
            if (tokenObj != null) {
                userUpdate = tokenObj.getUserName();
                staffId = tokenObj.getStaffId();
            }
            Long taskMngtId = null;
            Long taskStaffMngtId = null;
            Long subId = null;
            Long telServiceId = null;
            Long cableBoxId = null;
            Long plateA = null;
            Long plateB = null;
            Long plateCableBoxA = null;
            Long plateCableBoxB = null;
            Long dslamId = null;
            Long boardId = null;
            Long lineNo = null;
            Long portNo = null;
            Long stationId = null;
            Long shopId = null;
            Map mapGoods = null;
            Map mapNewSerial = null;
            Map mapItem = null;
            com.viettel.bccs.api.Task.BO.TaskManagement list = null;
            try {
                taskMngtId = infrastructureUpdateIn.getTaskMngtId().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getTaskMngtId());
                taskStaffMngtId = infrastructureUpdateIn.getTaskStaffMngtId().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getTaskStaffMngtId());
                subId = infrastructureUpdateIn.getSubId().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getSubId());
                telServiceId = infrastructureUpdateIn.getTelServiceId().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getTelServiceId());
                cableBoxId = infrastructureUpdateIn.getCableBoxId().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getCableBoxId());
                plateA = infrastructureUpdateIn.getPlateA().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getPlateA());
                plateB = infrastructureUpdateIn.getPlateB().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getPlateB());
                plateCableBoxA = infrastructureUpdateIn.getPlateCableBoxA().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getPlateCableBoxA());
                plateCableBoxB = infrastructureUpdateIn.getPlateCableBoxB().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getPlateCableBoxB());
                dslamId = infrastructureUpdateIn.getDsLamId().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getDsLamId());
                boardId = infrastructureUpdateIn.getBoardId().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getBoardId());
                lineNo = infrastructureUpdateIn.getLineNo().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getLineNo());
                portNo = infrastructureUpdateIn.getPortNo().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getPortNo());
                stationId = infrastructureUpdateIn.getStationId().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getStationId());
                mapGoods = convertListToMap(infrastructureUpdateIn.getListGood());
                mapNewSerial = convertListToMap(infrastructureUpdateIn.getListNewSerial());
                mapItem = convertListToMap(infrastructureUpdateIn.getListItem());
                Shop shop = new ShopDAO().findByCode(cmSession, infrastructureUpdateIn.getShopCodeLogin());
                shopId = shop.getShopId();
                list = new com.viettel.bccs.api.Task.DAO.TaskManagementDAO().findById(cmSession, taskMngtId);
                /*duyetdk*/
                if ("true".equals(infrastructureUpdateIn.getIsGpon())
                        && "4".equals(infrastructureUpdateIn.getTab())
                        && !com.viettel.bccs.api.Task.DAO.TaskManagementDAO.isTaskForComplain(list)
                        && !Constants.OTHER_PRO_BUNDLE_TV.equals(list.getJobCode())) {
                    if (infrastructureUpdateIn.getGponSerial() == null || infrastructureUpdateIn.getGponSerial().trim().isEmpty()) {
                        wSRespone = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.serialGpon", locale));
                        return wSRespone;
                    }
                }
                /*if ("4".equals(infrastructureUpdateIn.getTab())) {
                 if (mapNewSerial == null || Double.compare(mapNewSerial.size(), infrastructureUpdateIn.getListNewSerial().size()) != 0) {
                 wSRespone = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("required.serialProduct", locale));
                 return wSRespone;
                 }
                 }*/
            } catch (Exception ex) {
                wSRespone.setErrorCode("-98");
                wSRespone.setErrorDecription("Input value is invalid");
                return wSRespone;
            }

            com.viettel.bccs.api.Task.BO.TaskStaffManagement tsm = new com.viettel.bccs.api.Task.DAO.TaskStaffManagementDAO().findById(taskStaffMngtId, cmSession);
            com.viettel.bccs.api.Task.BO.TaskShopManagement taskShop = new com.viettel.bccs.api.Task.DAO.TaskShopManagementDAO().findByTaskShopId(cmSession, tsm.getTaskShopMngtId());
            com.viettel.bccs.api.Task.BO.TaskManagement task = new com.viettel.bccs.api.Task.DAO.TaskManagementDAO().findById(cmSession, taskShop.getTaskMngtId());

            /*xu ly luong cong viec cho thue bao k thuoc vien thong*/
            if (!Constants.SERVICE_ALIAS_ADSL.equals(task.getServiceType())
                    && !Constants.SERVICE_ALIAS_FTTH.equals(task.getServiceType())
                    && !Constants.SERVICE_ALIAS_LEASEDLINE.equals(task.getServiceType())) {
                TaskForUpdateControllerDigital taskController = null;
                /*camera*/
                if (Constants.SERVICE_ALIAS_CAMERA.equals(task.getServiceType())) {
                    taskController = new TaskForUpdateControllerCamera();
                }
                if (taskController != null) {
                    wSRespone = taskController.updateInfrastructure(cmSession, imSession, pmSession, nimsSession, locale, infrastructureUpdateIn);
                }
                if (!Constants.RESPONSE_SUCCESS.equals(wSRespone.getErrorCode())) {
                    hasErr = true;
                }
                return wSRespone;
            }

            if (Constants.TASK_JOB_CODE_DEFAULT.equals(task.getJobCode()) && "1".equals(task.getReqType())) {
                //change address ftth
                subFbConf = new SubFbConfigBusiness().findByTaskMngtId(cmSession, taskMngtId, Constants.TYPE_CONF_IMT);
            } else {
                subFbConf = new SubFbConfigBusiness().findBySubId(cmSession, subId, null, Constants.TYPE_CONF_IMT);
            }
            if (subFbConf != null
                    && (subFbConf.getStatus().equals(Constants.WAITING) || subFbConf.getStatus().equals(Constants.APPROVED))
                    && subFbConf.getTaskManagementId().equals(taskMngtId)) {
                wSRespone.setErrorCode("1");
                wSRespone.setErrorDecription("Status request config is not reject, you can not update infrastructure");
                return wSRespone;
            }
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            wSRespone.setErrorCode(Constants.RESPONSE_SUCCESS);
            wSRespone.setErrorDecription(Constants.SUCCESS);
            SubAdslLeaseline subAd = new SubAdslLeaselineBussines().findById(cmSession, subId);
            if (subAd == null) {
                wSRespone.setErrorCode("-98");
                wSRespone.setErrorDecription("subId is invalid");
                return wSRespone;
            }
            List<SubDeployment> lstSubDep = new SubDeploymentDAO().findBySubId(subId, cmSession);
            ResultBO resultBO = new ResultBO();
            /*add device to user bundle tv*/
            String url = ResourceUtils.getResource("BUNDLE_TV_SERVICE");
            String stockBundleTv = ResourceUtils.getResource("BUNDLE_TV_STOCK_MODEL_ID");
            SubBundleTv subTv = new SubBundleTvDAO().findBundleTvByAccount(cmSession, list.getPathFileAttach());
            List<SubStockModelRel> lstOldGood = new SubStockModelRelDAO().findBySubAndSpace(subId, list.getSourceId(), cmSession);
            Long subStockModelRelIdTv = null;
            if (lstOldGood != null && !lstOldGood.isEmpty()) {
                for (SubStockModelRel temp : lstOldGood) {
                    if (stockBundleTv.equals(temp.getStockModelId().toString()) && taskMngtId.equals(temp.getTaskMngId())) {
                        subStockModelRelIdTv = temp.getSubStockModelRelId();
                        break;
                    }
                }
            }
            if (subTv != null && !infrastructureUpdateIn.getIsUpdateDeployment().equals("1")) {
                String serialBundle = null;
                Iterator newSerialKeyMap = mapNewSerial.keySet().iterator();
                while (subStockModelRelIdTv != null && newSerialKeyMap.hasNext()) {
                    Object goodId = newSerialKeyMap.next();
                    Object serial = mapNewSerial.get(goodId);
                    if (subStockModelRelIdTv.toString().equals(goodId.toString())) {
                        serialBundle = serial.toString();
                        break;
                    }
                }
                if (serialBundle == null) {
                    resultBO.setResult(Constants.RESPONSE_FAIL);
                    wSRespone.setErrorDecription("Can not find serial for bundle tv with stockModelId config " + stockBundleTv);
                } else {
                    if ("true".equalsIgnoreCase(com.viettel.brcd.common.util.ResourceBundleUtils.getResource("provisioning.allow.send.request", "provisioning"))) {
                        if (subTv.getBoxSerial() == null) {
                            addDeviceToUserBundle(serialBundle, url, resultBO, wSRespone, subTv, cmSession, listLog);
                        } else if (!subTv.getBoxSerial().equals(serialBundle)) {
                            Request requestBundle = new Request();
                            requestBundle.deleteDeviceToUser(subTv.getUserUniqueId(), subTv.getBoxSerial());
                            String data = new Gson().toJson(requestBundle);
                            ServiceSupplier serviceSup = new ServiceSupplier();
                            String response = serviceSup.sendRequest(Constants.SERVICE_METHOD_POST, url, data, null, null);
                            SubBundleTvLog log = new SubBundleTvLog();
                            log.setSubId(subTv.getSubId());
                            log.setAction(Request.ACTION_DELETE_DEVICE_TO_USER);
                            log.setAccount(subTv.getAccount());
                            log.setRequest(data);
                            log.setResponse(response);
                            log.setCreateDate(new Date());
                            listLog.add(log);
                            if (response == null || response.isEmpty()) {
                                resultBO.setResult(Constants.RESPONSE_FAIL);
                                wSRespone.setErrorDecription("Error from call service deleteDeviceToUser bunlde tv");
                            } else {
                                WSRespone ws = new Gson().fromJson(response, WSRespone.class);
                                ServiceResponse serviceResponse = ws == null || ws.getErrorDecription() == null ? null : new Gson().fromJson(ws.getErrorDecription(), ServiceResponse.class);
                                if (serviceResponse == null || !Constants.RESPONSE_SUCCESS.equals(serviceResponse.getResult())) {
                                    resultBO.setResult(Constants.RESPONSE_FAIL);
                                    wSRespone.setErrorDecription("Error from call service deleteDeviceToUser bunlde tv " + (serviceResponse != null ? serviceResponse.getMessage() : ""));
                                } else {
                                    addDeviceToUserBundle(serialBundle, url, resultBO, wSRespone, subTv, cmSession, listLog);
                                }
                            }
                        }
                    }
                }
            }
            if (!Constants.RESPONSE_FAIL.equals(resultBO.getResult())) {
                resultBO = managerTaskDAO.updateInfrastructure(cmSession, imSession, pmSession, nimsSession, taskMngtId, taskStaffMngtId, subId, telServiceId,
                        cableBoxId, plateA, plateB, plateCableBoxA, plateCableBoxB, boardId, lineNo, portNo, infrastructureUpdateIn.getLoginName(),
                        infrastructureUpdateIn.getShopCodeLogin(), mapGoods, mapNewSerial, dslamId, mapItem, boardId, infrastructureUpdateIn.getIsUpdateDeployment(), stationId,
                        infrastructureUpdateIn.getIsGpon(), infrastructureUpdateIn.getGponSerial(), infrastructureUpdateIn.getStbSerial(), infrastructureUpdateIn.getPortLogic(),
                        infrastructureUpdateIn.getSpitterId(), infrastructureUpdateIn.getIsSplitter(), shopId, infrastructureUpdateIn.getCouplerNo());
            }
            /*-4: truong hop khong co thay doi ha tang van cho thay doi anh*/
            if (resultBO.getResult().equals(Constants.RESPONSE_SUCCESS)
                    || "-4".equals(resultBO.getResult())
                    || "112".equals(resultBO.getResult()))   {
                String mess = null;
                String isComplaint = "";
                List<com.viettel.bccs.cm.model.TaskManagement> lst_taskmngt = new TaskManagementDAO().findByProperty(cmSession, "taskMngtId", taskMngtId);
                if (lst_taskmngt != null) {
                    isComplaint = lst_taskmngt.get(0).getSourceType();
                }
                if ("0".equals(isComplaint)) {
                    List<com.viettel.im.database.BO.Staff> staff = new com.viettel.im.database.DAO.StaffDAO(imSession).findByProperty("staffId", staffId);
                    com.viettel.im.database.BO.Shop shop = new com.viettel.im.database.DAO.ShopDAO(imSession).findById(staff.get(0).getShopId());
                    SubFbConfigBusiness subFbConfigBuss = new SubFbConfigBusiness();

                    String account = subAd.getAccount();

                    if (!Constants.OTHER_PRO_BUNDLE_TV.equals(task.getJobCode())) {
                        /*unlock ha tang cu*/
                        if (lstSubDep != null && !lstSubDep.isEmpty()) {
                            try {
                                LockAndUnlockInfrasResponse infrasResponse = NimsWsLockUnlockInfrasBusiness.
                                        lockUnlockInfras(NimsWsLockUnlockInfrasBusiness.INFRAS_ACTION_UNLOCK,
                                                account,
                                                null,
                                                lstSubDep.get(0).getCableBoxId() == null ? 0l : lstSubDep.get(0).getCableBoxId(),
                                                null, subAd.getServiceType(), null, null, null, null);
                                infrasResponse = NimsWsLockUnlockInfrasBusiness.
                                        lockUnlockInfras(NimsWsLockUnlockInfrasBusiness.INFRAS_ACTION_UNLOCK,
                                                account,
                                                null,
                                                lstSubDep.get(0).getSpitterId() == null ? 0l : lstSubDep.get(0).getSpitterId(),
                                                null, subAd.getServiceType(), null, null, null, null);
                            } catch (Exception ex) {
                            }
                            if (subId != null && subId > 0L) {
                                subAd = new SubAdslLeaselineBussines().findById(cmSession, subId);
                            }
                            if (subFbConf != null) {//neu la pending hoac reject thi cho phep cap nhat lai 
                                if ((subFbConf.getStatus().equals(Constants.PENDING)
                                        || subFbConf.getStatus().equals(Constants.REJECT))) {
                                    mess = subFbConfigBuss.updateRequestSubFbConfig(cmSession, taskMngtId, subId, account,
                                            infrastructureUpdateIn.getImgInfras(), infrastructureUpdateIn.getImgGoods(),
                                            userUpdate, locale, request, subFbConf, Constants.TYPE_CONF_IMT);
                                }

                            } else {//neu ko phai cac truong hop tren thi them moi 
                                mess = subFbConfigBuss.sendRequestSubFbConfig(cmSession, taskMngtId, subId, account,
                                        infrastructureUpdateIn.getImgInfras(), infrastructureUpdateIn.getImgGoods(),
                                        userUpdate, locale, infrastructureUpdateIn.getTab(), shop.getProvince(), request, subFbConf);
                            }
                            if (mess != null) {
                                mess = mess.trim();
                                if (!mess.isEmpty()) {
                                    hasErr = true;
                                    wSRespone = new WSRespone(Constants.ERROR_CODE_1, mess);
                                }
                            }

                        }
                    } else {
                        if (new SubFbConfigBusiness().findByTaskMngtId(cmSession, taskMngtId, Constants.TYPE_CONF_TV) == null) {
                            new SubFbConfigSupplier().insert(cmSession, taskMngtId, subId, task.getPathFileAttach(), null,
                                    null, null, null, null, null, new Date(), userUpdate, shop.getProvince(), Constants.TYPE_CONF_TV, Constants.WAITING, null);
                        }
                        mess = subFbConfigBuss.updateRequestSubFbConfig(cmSession, taskMngtId, subId, task.getPathFileAttach(),
                                infrastructureUpdateIn.getImgInfras(), infrastructureUpdateIn.getImgGoods(),
                                userUpdate, locale, request, subFbConf, Constants.TYPE_CONF_TV);

                    }
                }
            } else {
                String des = new TaskForUpdateControllerCamera().processResultError(resultBO, locale);
                wSRespone.setErrorCode(resultBO.getResult());
                wSRespone.setErrorDecription(des);
                hasErr = true;
            }

        } catch (Exception ex) {
            hasErr = true;
            wSRespone.setErrorCode("-99");
            wSRespone.setErrorDecription("System is busy :" + ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            if (subFbConf != null && subFbConf.getId() != null) {
                new SubFbConfigSupplier().insertLog(cmSession, subFbConf.getId(), subFbConf.getStatusConf(), !"".equals(request.toString()) ? request.toString() : new Gson().toJson(infrastructureUpdateIn.clone()), new Gson().toJson(wSRespone), "UPDATE_INFRASTRUCTURE", wSRespone.getErrorDecription(), new Date(), userUpdate, "0");
            }
            if (hasErr) {
                try {
                    rollBackTransactions(cmSession, imSession, pmSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            } else {
                commitTransactions(cmSession, imSession, pmSession);
            }
            if (!listLog.isEmpty()) {
                cmSession.beginTransaction();
                for (SubBundleTvLog log : listLog) {
                    log.setCreateUser(infrastructureUpdateIn.getLoginName());
                    cmSession.save(log);
                    cmSession.flush();
                }
                commitTransactions(cmSession);
            }
            closeSessions(cmSession, imSession, pmSession, nimsSession);
        }
        return wSRespone;
    }

    /**
     *
     * @param lstConvert
     * @return
     * @throws Exception
     */
    public Map convertListToMap(HashMap<String, String> lstConvert) throws Exception {
        try {
            Map mapConvert = new HashMap();
            if (lstConvert == null) {
                return mapConvert;
            }
            Set<String> keySet = lstConvert.keySet();
            Iterator<String> keySetIterator = keySet.iterator();
            while (keySetIterator.hasNext()) {
                String key = keySetIterator.next();
                if (key != null && !key.isEmpty() && lstConvert.get(key) != null && !lstConvert.get(key).isEmpty()) {
                    mapConvert.put(key, lstConvert.get(key));
                }
            }
            return mapConvert;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        }
    }

    public GroupReasonOut getLstReason(HibernateHelper hibernateHelper, String locale, String reasonGroup) {
        GroupReasonOut groupReasonOut = new GroupReasonOut();
        Session cmSession = null;
        try {
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Reason> listReason = new ReasonDAO().getListReasonByGroup(cmSession, reasonGroup);
            if (listReason != null && !listReason.isEmpty()) {
                groupReasonOut.setReasons(listReason);
                groupReasonOut.setErrorCode("0");
                groupReasonOut.setErrorDecription(Constants.SUCCESS);
            } else {
                groupReasonOut.setErrorCode("0");
                groupReasonOut.setErrorDecription(Constants.SUCCESS);
            }
        } catch (Exception ex) {
            logger.error("groupReasonOut: " + ex.getMessage());
            groupReasonOut.setErrorCode("-99");
            groupReasonOut.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            closeSessions(cmSession);
        }
        return groupReasonOut;
    }

    public PortOut getListPortByDevice(HibernateHelper hibernateHelper, String locale, String dslamId, String telServiceId, String subId, String stationId) {
        PortOut portOut = new PortOut();
        Session cmSession = null;
        Session imSession = null;
        try {
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            List<EquipmentPort> listEquipmentPort = managerTaskDAO.getListPortByDevice(cmSession, imSession, dslamId, telServiceId, subId, stationId);
            if (listEquipmentPort != null && !listEquipmentPort.isEmpty()) {
                portOut.setPort(listEquipmentPort);
                portOut.setErrorCode("0");
                portOut.setErrorDecription(Constants.SUCCESS);
            } else {
                portOut.setErrorCode("0");
                portOut.setErrorDecription(Constants.SUCCESS);
            }
        } catch (Exception ex) {
            logger.error("groupReasonOut: " + ex.getMessage());
            portOut.setErrorCode("-99");
            portOut.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            closeSessions(cmSession, imSession);
        }
        return portOut;
    }

    public TaskLookForOut searchTaskForLookUp(HibernateHelper hibernateHelper, SearchTaskForLookUpInput searchTaskInput, String local) {
        TaskLookForOut taskLookFor = new TaskLookForOut();
        Session sessionPos = null;
        StringBuilder total = new StringBuilder();
        try {
            if (searchTaskInput.getShopId() == null) {
                return new TaskLookForOut("-1", "Not found user login");
            }
            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            Date nowDate = Common.getDateTimeFromDB(sessionPos);
            List lstTask = null;
            List<CoordinateSubDrawCable> lstCoor = null;
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            //<editor-fold defaultstate="collapsed" desc="call lib get stakk">
            lstTask = managerTaskDAO.searchTaskForLookUp(sessionPos,
                    searchTaskInput.getShopId(), searchTaskInput.getSourceType(),
                    searchTaskInput.getAccount(), searchTaskInput.getProgress(),
                    searchTaskInput.getStaDate(), searchTaskInput.getEndDate(),
                    nowDate, searchTaskInput.getService(), searchTaskInput.getPage(), searchTaskInput.getPageSize(), total);
            taskLookFor.setTotal(total.toString());
            //</editor-fold>
            List listResult = new ArrayList();
            if (lstTask != null && lstTask.size() > 0) {
                taskLookFor.setErrorCode("0");
                taskLookFor.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
                for (Iterator iterator = lstTask.iterator(); iterator.hasNext();) {
                    TaskLookFor taskLookForOut = new TaskLookFor();
                    TaskManagement taskManagement = (TaskManagement) iterator.next();
                    lstCoor = new SubAdslLeaselineBussines().findCoordinateBySubId(sessionPos, taskManagement.getAccount());
                    taskLookForOut.setAccount(taskManagement.getAccount());
                    taskLookForOut.setContractNo(taskManagement.getContractNo());
                    taskLookForOut.setDeployAddress(taskManagement.getDeployAddress());
                    taskLookForOut.setTaskMngtId(taskManagement.getTaskMngtId());
                    taskLookForOut.setTaskName(taskManagement.getTaskName());
                    taskLookForOut.setTaskStaffMngtId(taskManagement.getTaskStaffMngtId());
                    taskLookForOut.setCustTelFax(taskManagement.getTelFax());
                    taskLookForOut.setIsSystemError(taskManagement.getIsSystemError());
                    taskLookForOut.setTextColor(taskManagement.getTextColour());
                    taskLookForOut.setTelServiceId(searchTaskInput.getService());
                    taskLookForOut.setBackgroundColor(taskManagement.getBackGroundColour());
                    taskLookForOut.setStatus(taskManagement.getStatus());
                    taskLookForOut.setCustReqId(taskManagement.getCustReqId());
                    taskLookForOut.setTaskShopMngtId(taskManagement.getTaskShopMngtId());
                    taskLookForOut.setCustReqId(taskManagement.getCustReqId());
                    taskLookForOut.setProgress(taskManagement.getProgress());
                    taskLookForOut.setCoordinates(lstCoor);
                    SubFbConfig subFbConfig = new SubFbConfigBusiness().findByTaskMngtId(sessionPos, taskManagement.getTaskMngtId(), Constants.TYPE_CONF_IMT);
                    if (subFbConfig != null) {
                        taskLookForOut.setStatus(subFbConfig.getStatus() == null ? null : subFbConfig.getStatus());
                        taskLookForOut.setStatusConfig(subFbConfig.getStatusConf() == null ? null : String.valueOf(subFbConfig.getStatusConf()));
                        taskLookForOut.setDescriptionConfig(subFbConfig.getDescription());
                    }
                    if (lstCoor != null && lstCoor.size() >= 2) {
                        double d = distanceBetween2Points(lstCoor.get(0).getLat(), lstCoor.get(0).getLng(), lstCoor.get(1).getLat(), lstCoor.get(1).getLng());
                        taskLookForOut.setDistanceToInfra(String.valueOf(d));
                    } else {
                        taskLookForOut.setDistanceToInfra("N/A");
                    }
                    listResult.add(taskLookForOut);
                }
                taskLookFor.setListTask(listResult);

            } else {
                taskLookFor.setErrorCode("-2");
                taskLookFor.setErrorDecription("Not found task");
            }
        } catch (Exception ex) {
            logger.error("searchTask: " + ex.getMessage());
            taskLookFor.setErrorCode("-99");
            taskLookFor.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            closeSessions(sessionPos);
        }
        return taskLookFor;
    }

    public TaskInfoDetailOut showDetailInfoTask(HibernateHelper hibernateHelper, String taskMngtId, String local) {
        TaskInfoDetailOut taskLookFor = new TaskInfoDetailOut();
        Session cmSession = null;
        Session imSession = null;
        Session ccSession = null;
        try {
            if (taskMngtId == null) {
                return new TaskInfoDetailOut("-1", "lost taskMngtId");
            }
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            DetailInfoTask detailInfoTask = null;
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            //<editor-fold defaultstate="collapsed" desc="call lib get stakk">
            detailInfoTask = managerTaskDAO.showDetailInfoTask(cmSession, imSession, ccSession, taskMngtId);
            if ("0".equals(detailInfoTask.getError())) {
                taskLookFor.setErrorCode(detailInfoTask.getError());
                taskLookFor.setErrorDecription(Constants.SUCCESS);
                taskLookFor.setInfoDetailTask(detailInfoTask);
            } else {
                taskLookFor.setErrorCode(detailInfoTask.getError());
                taskLookFor.setErrorDecription(detailInfoTask.getResult());
            }

            //</editor-fold>
        } catch (Exception ex) {
            logger.error("searchTask: " + ex.getMessage());
            taskLookFor.setErrorCode("-99");
            taskLookFor.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            closeSessions(cmSession);
            closeSessions(ccSession);
            closeSessions(imSession);
        }
        return taskLookFor;
    }

    public TaskInfrasDetailOut showDetailInfrasTask(HibernateHelper hibernateHelper, String taskMngtId, String local) {
        TaskInfrasDetailOut taskLookFor = new TaskInfrasDetailOut();
        Session cmSession = null;
        Session imSession = null;
        try {
            if (taskMngtId == null) {
                return new TaskInfrasDetailOut("-1", "lost taskMngtId");
            }
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            DetailInfrasLookFor detailInfoTask = null;
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            //<editor-fold defaultstate="collapsed" desc="call lib get stakk">
            detailInfoTask = managerTaskDAO.showDetailInfrasTask(cmSession, imSession, taskMngtId);
            if ("0".equals(detailInfoTask.getError())) {
                taskLookFor.setErrorCode(detailInfoTask.getError());
                taskLookFor.setErrorDecription(Constants.SUCCESS);
                taskLookFor.setLstGood(detailInfoTask.getLstGood());
                taskLookFor.setLstItem(detailInfoTask.getLstItem());
                taskLookFor.setSubDep(detailInfoTask.getSubDep());
            } else {
                taskLookFor.setErrorCode(detailInfoTask.getError());
                taskLookFor.setErrorDecription(detailInfoTask.getResultMess());
            }

            //</editor-fold>
        } catch (Exception ex) {
            logger.error("searchTask: " + ex.getMessage());
            taskLookFor.setErrorCode("-99");
            taskLookFor.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            closeSessions(cmSession);
            closeSessions(imSession);
        }
        return taskLookFor;
    }

    public TaskHistoryOut searchHistoryTask(HibernateHelper hibernateHelper, String startDate, String endDate, String taskMngtId, String local) {
        TaskHistoryOut taskHis = new TaskHistoryOut();
        Session cmSession = null;
        try {
            if (taskMngtId == null) {
                return new TaskHistoryOut("-1", "lost taskMngtId");
            }
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<TaskHistory> listTaskHis = null;
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            //<editor-fold defaultstate="collapsed" desc="call lib get stakk">
            listTaskHis = managerTaskDAO.searchHistoryTask(cmSession, startDate, endDate, taskMngtId);
            if (listTaskHis != null && !listTaskHis.isEmpty()) {
                taskHis.setErrorDecription(Constants.SUCCESS);
                taskHis.setLstTaskHistory(listTaskHis);
                taskHis.setErrorCode("0");
            } else {
                taskHis.setErrorDecription(LabelUtil.getKey("not.found.data", local));
                taskHis.setErrorCode("-2");
            }
            //</editor-fold>
        } catch (Exception ex) {
            logger.error("searchTask: " + ex.getMessage());
            taskHis.setErrorCode("-99");
            taskHis.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            closeSessions(cmSession);
        }
        return taskHis;
    }

    public ComplainResultOut showComplaintResult(HibernateHelper hibernateHelper, String custReqId, String local) {
        ComplainResultOut complainResultOut = new ComplainResultOut();
        Session ccSession = null;
        try {
            if (custReqId == null) {
                return new ComplainResultOut("-1", "lost custReqId");
            }
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            ComplainResult complainResult = new ComplainResult();
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            //<editor-fold defaultstate="collapsed" desc="call lib get stakk">
            complainResult = managerTaskDAO.showComplaintResult(ccSession, custReqId);
            if (complainResult != null && !"".equals(complainResult)) {
                complainResultOut.setComplainResult(complainResult);
                complainResultOut.setErrorDecription(Constants.SUCCESS);
                complainResultOut.setErrorCode("0");
            } else {
                complainResultOut.setErrorDecription(LabelUtil.getKey("not.found.data", local));
                complainResultOut.setErrorCode(Constants.NOT_FOUND_TASK);
            }

            //</editor-fold>
        } catch (Exception ex) {
            logger.error("searchTask: " + ex.getMessage());
            complainResultOut.setErrorCode("-99");
            complainResultOut.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            closeSessions(ccSession);
        }
        return complainResultOut;
    }

    public WSRespone deleteTask(HibernateHelper hibernateHelper, String strTaskMngtId, String reason, String loginName, String shopCodeLogin) {
        WSRespone respone = new WSRespone();
        Session ccSession = null;
        Session imSession = null;
        Session cmSession = null;
        try {
            if (strTaskMngtId == null) {
                return new WSRespone("-9", "lost strTaskMngtId");
            }
            if (reason == null) {
                return new WSRespone("-10", "lost reason");
            }
            if (loginName == null) {
                return new WSRespone("-11", "lost loginName");
            }
            if (shopCodeLogin == null) {
                return new WSRespone("-12", "lost shopCodeLogin");
            }
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            //<editor-fold defaultstate="collapsed" desc="call lib get stakk">
            ResultDeleteTask resultDelete = managerTaskDAO.deleteTask(cmSession, imSession, ccSession, strTaskMngtId, reason, loginName, shopCodeLogin);
            respone.setErrorCode(resultDelete.getError());
            respone.setErrorDecription(resultDelete.getResultMess());
            if ("0".equals(resultDelete.getError())) {
                commitTransactions(cmSession);
                commitTransactions(ccSession);
                commitTransactions(imSession);
            } else {
                rollBackTransactions(cmSession);
                rollBackTransactions(ccSession);
                rollBackTransactions(imSession);
            }
            //</editor-fold>
        } catch (Exception ex) {
            logger.error("searchTask: " + ex.getMessage());
            respone.setErrorCode("-99");
            respone.setErrorDecription("System is busy :" + ex.getMessage());
            rollBackTransactions(cmSession);
            rollBackTransactions(ccSession);
            rollBackTransactions(imSession);
        } finally {
            closeSessions(cmSession);
            closeSessions(ccSession);
            closeSessions(imSession);
        }
        return respone;
    }

    public SearchTaskRevokeOut searchTaskForRevoke(HibernateHelper hibernateHelper, SearchTaskRevokeIn searchTask, String local) {
        SearchTaskRevokeOut searchTaskOut = new SearchTaskRevokeOut();
        Session cmSession = null;
        try {
            if (searchTask == null) {
                return new SearchTaskRevokeOut("-1", "null input");
            }
            StringBuilder total = new StringBuilder();
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            //<editor-fold defaultstate="collapsed" desc="call lib get stakk">
            List<ViewTaskStaff> lstStaff = managerTaskDAO.searchTaskForRevoke(cmSession, searchTask.getShopId(), searchTask.getStaffId(), searchTask.getTaskStaffProgress(), searchTask.getStartDate(), searchTask.getEndDate(), searchTask.getCustReqId(), searchTask.getContractNo(), searchTask.getAccount(), searchTask.getCustName(), searchTask.getTelFax(), searchTask.getTelServiceId(), searchTask.getRevokeFor(), searchTask.getPage(), searchTask.getPageSize(), total);
            if (lstStaff != null && !lstStaff.isEmpty()) {
                searchTaskOut.setLstStaff(lstStaff);
                searchTaskOut.setErrorCode("0");
                searchTaskOut.setErrorDecription(Constants.SUCCESS);
            } else {
                searchTaskOut.setErrorCode("-2");
                searchTaskOut.setErrorDecription(LabelUtil.getKey("not.found.data", local));
            }
            searchTaskOut.setTotal(total.toString());
            //</editor-fold>
        } catch (Exception ex) {
            logger.error("searchTask: " + ex.getMessage());
            searchTaskOut.setErrorCode("-99");
            searchTaskOut.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            closeSessions(cmSession);
        }
        return searchTaskOut;
    }

    public PreRevokeOut detailRevokeTask(HibernateHelper hibernateHelper, String strTaskStaffMngtId, String strTelServiceId, String strRevokeFor, String local) {
        PreRevokeOut detailTask = new PreRevokeOut();
        Session cmSession = null;
        try {
            if (strTaskStaffMngtId == null) {
                return new PreRevokeOut("-1", "null strTaskStaffMngtId");
            }
            if (strTelServiceId == null) {
                return new PreRevokeOut("-2", "null strTelServiceId");
            }
            if (strRevokeFor == null) {
                return new PreRevokeOut("-3", "null strRevokeFor");
            }
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            //<editor-fold defaultstate="collapsed" desc="call lib get stakk">
            DetailRevokeTask detailTaskOut = managerTaskDAO.preRevokeTask(cmSession, strTaskStaffMngtId, strTelServiceId, strRevokeFor);
            if (!"0".equals(detailTaskOut.getError())) {
                detailTask.setErrorCode(detailTaskOut.getError());
                detailTask.setErrorDecription(detailTaskOut.getResultMess());
            } else {
                DetailRevokeTaskOut taskOut = new DetailRevokeTaskOut();
                if (detailTask != null) {
//                    detailTask.setDetailTask(detailTaskOut);
                    taskOut.setDeployAddress(detailTaskOut.getDeployAddress());
                    taskOut.setEndDate(detailTaskOut.getEndDate());

                    List<com.viettel.bccs.api.Task.BO.Reason> lstReasonOut = detailTaskOut.getLstReason();
                    List<Reason> lstReason = new ArrayList<Reason>();
                    if (lstReasonOut != null && !lstReasonOut.isEmpty()) {
                        for (Iterator iterator = lstReasonOut.iterator(); iterator.hasNext();) {
                            Reason reason = new Reason();
                            com.viettel.bccs.api.Task.BO.Reason reasonOut = (com.viettel.bccs.api.Task.BO.Reason) iterator.next();
                            reason.setReasonId(reasonOut.getReasonId());
                            reason.setCode(reasonOut.getCode());
                            reason.setName(reasonOut.getName());
                            reason.setDescription(reasonOut.getDescription());
                            lstReason.add(reason);
                        }
                    }
                    detailTask.setLstReason(lstReason);
                    taskOut.setRevokeFor(detailTaskOut.getRevokeFor());
                    taskOut.setShopId(detailTaskOut.getShopId());
                    taskOut.setShopName(detailTaskOut.getShopName());
                    taskOut.setSourceType(detailTaskOut.getSourceType());
                    taskOut.setStaffId(detailTaskOut.getStaffId());
                    taskOut.setStaffName(detailTaskOut.getStaffName());
                    taskOut.setStartDate(detailTaskOut.getStartDate());
                    taskOut.setSubId(detailTaskOut.getSubId());
                    taskOut.setTaskMngtId(detailTaskOut.getTaskMngtId());
                    taskOut.setTaskName(detailTaskOut.getTaskName());
                    taskOut.setTaskStaffMngtId(detailTaskOut.getTaskStaffMngtId());
                }
                detailTask.setErrorCode("0");
                detailTask.setErrorDecription(Constants.SUCCESS);
                detailTask.setDetailTask(taskOut);
            }
            //</editor-fold>
        } catch (Exception ex) {
            logger.error("searchTask: " + ex.getMessage());
            detailTask.setErrorCode("-99");
            detailTask.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            closeSessions(cmSession);
        }
        return detailTask;
    }

    public WSRespone revokeTask(HibernateHelper hibernateHelper, RevokeTaskIn revokeTaskIn) {
        WSRespone respone = new WSRespone();
        Session imSession = null;
        Session cmSession = null;
        try {
            if (revokeTaskIn == null) {
                return new WSRespone("-1", "lost revokeTaskIn");
            }
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            //<editor-fold defaultstate="collapsed" desc="call lib get stakk">
            ResultRevokeTask resultRevoke = managerTaskDAO.revokeTask(cmSession, imSession, revokeTaskIn.getTaskStaffMngtId(), revokeTaskIn.getReason(), revokeTaskIn.getLoginName(), revokeTaskIn.getShopCodeLogin(), revokeTaskIn.getSubIdClient(), revokeTaskIn.getTelServiceId(), revokeTaskIn.getRevokeFor(), revokeTaskIn.getNote());
            respone.setErrorCode(resultRevoke.getError());
            respone.setErrorDecription(resultRevoke.getResultMess());
            if ("0".equals(resultRevoke.getError())) {
                commitTransactions(cmSession);
                commitTransactions(imSession);
            } else {
                rollBackTransactions(cmSession);
                rollBackTransactions(imSession);
            }
            //</editor-fold>
        } catch (Exception ex) {
            logger.error("searchTask: " + ex.getMessage());
            respone.setErrorCode("-99");
            respone.setErrorDecription("System is busy :" + ex.getMessage());
            rollBackTransactions(cmSession);
            rollBackTransactions(imSession);
        } finally {
            closeSessions(cmSession);
            closeSessions(imSession);
        }
        return respone;
    }

    /**
     * @author : duyetdk
     * @des: ham tinh khoang cach giua hai vi tri da biet toa do
     * @since 05-03-2018
     */
    public static double distanceBetween2Points(double la1, double lo1,
            double la2, double lo2) {
        double dLat = (la2 - la1) * (Math.PI / 180);
        double dLon = (lo2 - lo1) * (Math.PI / 180);
        double la1ToRad = la1 * (Math.PI / 180);
        double la2ToRad = la2 * (Math.PI / 180);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(la1ToRad)
                * Math.cos(la2ToRad) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = (Constants.EARTH_RADIUS) * c;
        return d;
    }

    public InforInfrastructureOut getInfoInfraToView(HibernateHelper hibernateHelper, String locale, Long taskMngtId) throws Exception {
        return getInforForInfrastructure(hibernateHelper, locale, null, taskMngtId, null, true /*view*/);
    }

    public String updateRequestAnalysSOC(HibernateHelper hibernateHelper, Long complaintId, Long result, String description, List<String> lstErrorCode) {
        Session cmSession = null;
        Session cmPre = null;
        boolean isError = false;
        try {
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            SubFbConfig configSoc = new SubFbConfigBusiness().findByTaskMngtId(cmSession, complaintId, Constants.TYPE_CONF_SOC);
            if (configSoc != null) {
                Staff staff = new StaffDAO().findByCode(cmSession, configSoc.getCreateUser());
                String tel = staff.getTel() != null ? staff.getTel() : staff.getIsdn() != null ? staff.getIsdn() : "0";
                com.viettel.bccs.api.Task.BO.TaskShopManagement taskShop = new com.viettel.bccs.api.Task.DAO.TaskShopManagementDAO().findByTaskId(cmSession, configSoc.getTaskManagementId());
                com.viettel.bccs.api.Task.BO.TaskStaffManagement tsm = new com.viettel.bccs.api.Task.DAO.TaskStaffManagementDAO().findByTaskShopId(taskShop.getTaskShopMngtId(), cmSession);
                HashMap paramTr = new HashMap();
                paramTr.put("complaintId", complaintId);
                paramTr.put("result", result);
                paramTr.put("description", description);
                paramTr.put("listErrorCode", lstErrorCode);
                new SubFbConfigSupplier().insertLog(cmSession, configSoc.getId(), result, new Gson().toJson(paramTr), new Gson().toJson(paramTr), Constants.ACTION_REQUEST_ANALYSIS_SOC, null, new Date(), "SOC", "000");
                if (Constants.APPROVED.equals(result)) {
                    configSoc.setStatusConf(result);
                    /*Gui tin nhan va notification thong bao thanh cong den nhan vien*/
                    List<String> paramMessage = Arrays.asList(configSoc.getAccount());
                    sendAnnouneStaff(cmSession, cmPre, "en_US", "SOC", paramMessage, "SOC_SUCCESS", tel, paramMessage, "SOC_SUCCESS", staff.getStaffId(), staff.getStaffCode(), configSoc.getAccount(), tsm.getTaskStaffMngtId());
                } else {
                    List<CorporateWhiteList> whiteListError = new CorporateWhiteListDAO().findWhiteListReason(cmSession);
                    List<ErrorKnowledgeCataForm> listSocReason = CacheBO.getListSocReasonFail();
                    if (whiteListError == null || whiteListError.isEmpty()) {
                        configSoc.setStatusConf(result);
                        configSoc.setDescription(new Gson().toJson(lstErrorCode));
                        /*Gui tin nhan va notification thong bao that bai den nhan vien*/
                        String content = "";
                        for (String temp : lstErrorCode) {
                            for (ErrorKnowledgeCataForm reason : listSocReason) {
                                if (reason.getErrorCode().equalsIgnoreCase(temp)) {
                                    content += Constants.NEW_LINE + temp + " - " + reason.getDescription() /*+ ". Solution: " + reason.getSolution()*/;
                                    break;
                                }
                            }

                        }
                        List<String> paramMessage = Arrays.asList(configSoc.getAccount(), content);
                        sendAnnouneStaff(cmSession, cmPre, "en_US", "SOC", paramMessage, "SOC_FAILED", tel, paramMessage, "SOC_FAILED", staff.getStaffId(), staff.getStaffCode(), configSoc.getAccount(), tsm.getTaskStaffMngtId());
                    } else {
                        boolean isPass = true;
                        for (String temp : lstErrorCode) {
                            boolean pass = false;
                            for (CorporateWhiteList whitelist : whiteListError) {
                                if (whitelist.getIsdn().equalsIgnoreCase(temp)) {
                                    pass = true;
                                    break;
                                }
                            }
                            if (!pass) {
                                isPass = false;
                                break;
                            }
                        }
                        if (isPass) {
                            configSoc.setStatusConf(Constants.APPROVED);
                            configSoc.setDescription("Reasons are in whitelist");
                            /*Gui tin nhan va notification thong bao thanh cong den nhan vien*/
                            List<String> paramMessage = Arrays.asList(configSoc.getAccount());
                            sendAnnouneStaff(cmSession, cmPre, "en_US", "SOC", paramMessage, "SOC_SUCCESS", tel, paramMessage, "SOC_SUCCESS", staff.getStaffId(), staff.getStaffCode(), configSoc.getAccount(), tsm.getTaskStaffMngtId());
                        } else {
                            configSoc.setStatusConf(Constants.REJECT);
                            configSoc.setDescription(new Gson().toJson(lstErrorCode));
                            /*Gui tin nhan va notification thong bao that bai den nhan vien*/
                            String content = "";
                            for (String temp : lstErrorCode) {
                                for (ErrorKnowledgeCataForm reason : listSocReason) {
                                    if (reason.getErrorCode().equalsIgnoreCase(temp)) {
                                        content += Constants.NEW_LINE + temp + " - " + reason.getDescription() + ". Solution: " + reason.getSolution();
                                        break;
                                    }
                                }

                            }
                            List<String> paramMessage = Arrays.asList(configSoc.getAccount(), content);
                            sendAnnouneStaff(cmSession, cmPre, "en_US", "SOC", paramMessage, "SOC_FAILED", tel, paramMessage, "SOC_FAILED", staff.getStaffId(), staff.getStaffCode(), configSoc.getAccount(), tsm.getTaskStaffMngtId());
                        }
                    }
                }
                cmSession.update(configSoc);
                cmSession.flush();
            } else {
                return "can not find request for update";
            }
        } catch (Exception ex) {
            isError = true;
            return ex.getMessage();
        } finally {
            if (isError) {
                rollBackTransactions(cmSession, cmPre);
            } else {
                commitTransactions(cmSession, cmPre);
            }
            closeSessions(cmPre, cmSession);
        }
        return null;
    }

    private void sendAnnouneStaff(Session cmSession, Session cmPreSession, String locale, String userCreate, List<String> paramMessage, String typeMessage, String tel, List<String> paramNotification, String typeNotification, Long staffId, String staffCode, String account, Long taskStaffMngtId) throws Exception {
        /*send message*/
        com.viettel.bccs.cm.bussiness.SendMessge.send(cmPreSession, tel, paramMessage, new Date(), userCreate, null, Constants.AP_PARAM_PARAM_TYPE_SMS, typeMessage);

        /*push notification*/
        List<String> listContentSms = new IsdnSendSmsDAO().genarateSMSContent(cmPreSession, null, Constants.AP_PARAM_PARAM_TYPE_NOTIFICATION, typeNotification, paramNotification);
        String tempNotification = "";
        for (String tempMessage : listContentSms) {
            tempNotification += tempMessage + Constants.NEW_LINE;
        }
        if (!tempNotification.isEmpty()) {
            StringBuilder stringBui = new StringBuilder(tempNotification);
            stringBui.replace(tempNotification.lastIndexOf(Constants.NEW_LINE), tempNotification.lastIndexOf(Constants.NEW_LINE) + Constants.NEW_LINE.length(), "");
            tempNotification = stringBui.toString();
            new NotificationMessageDAO().generateNotificationData(
                    cmSession,
                    staffId,
                    staffCode,
                    userCreate,
                    LabelUtil.getKey("Notification.UpdateTask.title", locale),
                    tempNotification,/*message*/
                    account,/*account*/
                    taskStaffMngtId,
                    null,/*taskStaffMngtId*/
                    Constants.NOTIFICATION_ACTION_TYPE_REQUEST_CONFIG/*actionType*/);
        }
    }

    public ResultBO checkSocComplaint(Session cmSession, Long taskStaffMngtId, String progress, String staffCode, StringBuilder statusConfigSocStr, String locale) throws Exception {
        ResultBO responseCheckSoc = new ResultBO();
        Long statusConfSoc = Constants.APPROVED;
        com.viettel.bccs.api.Task.BO.TaskStaffManagement tsm = new com.viettel.bccs.api.Task.DAO.TaskStaffManagementDAO().findById(taskStaffMngtId, cmSession);
        com.viettel.bccs.api.Task.BO.TaskShopManagement taskShop = new com.viettel.bccs.api.Task.DAO.TaskShopManagementDAO().findByTaskShopId(cmSession, tsm.getTaskShopMngtId());
        TaskManagement task = new com.viettel.bccs.api.Task.DAO.TaskManagementDAO().findById(cmSession, taskShop.getTaskMngtId());
        if (Constant.TASK_SOURCE_TYPE_CC.equals(task.getSourceType()) && progress.equals(Constant.TASK_STAFF_PROGRESS_FINISH)) {
            String returnMessage = new ManagerTaskDAO().checkProgress(tsm.getProgress(), progress, null, null, task.getSourceType());
            if (returnMessage == null) {
                CorporateWhiteListDAO corporateWhiteListDAO = new CorporateWhiteListDAO();
                SubAdslLeaseline sub = new SubAdslLeaselineDAO().findById(cmSession, task.getSubId());
                if (sub != null) {
                    /*Kiem tra account co trong white_list*/
                    List<CorporateWhiteList> whiteList = corporateWhiteListDAO.findWhiteListTask(cmSession, task.getAccount());
                    if (whiteList == null || whiteList.isEmpty()) {
                        /*Step 1: Kiem tra xem loai dich vu này co can check soc hay khong*/
                        List<ApParam> param = new ApParamSupplier().findByEachProperty(cmSession, "SERVICE_CHECK_SOC", null, sub.getInfraType() == null ? Constants.INFRA_AON : sub.getInfraType(), null);
                        if (param != null && !param.isEmpty()
                                && param.get(0).getParamValue() != null
                                && param.get(0).getParamValue().toLowerCase().contains(sub.getServiceType().toLowerCase())) {


                            /*Step 2: Kiem tra da tao request analysis sang soc chua*/
                            List<SubFbConfig> requestSoc = new SubFbConfigSupplier().find(cmSession, null, task.getTaskMngtId(), Constants.STATUS_USE, null, null, null, Constants.TYPE_CONF_SOC);
                            if (requestSoc == null || requestSoc.isEmpty()) {
                                /*Step 3: tao request phan tich soc*/
                                StringBuilder requestSocStr = new StringBuilder();
                                RequestAnalysSocResponse response = SocService.requestAnalysSoc(task.getAccount(), task.getTaskMngtId(), requestSocStr);
                                SubFbConfig subFbConfSoc = new SubFbConfigSupplier().insert(cmSession, task.getTaskMngtId(), task.getSubId(), task.getAccount(), null, null, null, null, null, null, new Date(), staffCode, null, Constants.TYPE_CONF_SOC, Constants.STATUS_USE, Constants.SOC_KEY_FAIL.equals(response.getReturn().getKey()) ? Constants.REJECT : Constants.WAITING);
                                new SubFbConfigSupplier().insertLog(cmSession, subFbConfSoc.getId(), Constants.SOC_KEY_FAIL.equals(response.getReturn().getKey()) ? Constants.REJECT : Constants.WAITING, requestSocStr.toString(), new Gson().toJson(response), Constants.ACTION_REQUEST_ANALYSIS_SOC, null, new Date(), staffCode, "000");
                                statusConfSoc = Constants.SOC_KEY_FAIL.equals(response.getReturn().getKey()) ? Constants.REJECT : Constants.WAITING;
                                responseCheckSoc.setResult(Constants.SOC_KEY_FAIL.equals(response.getReturn().getKey()) ? "-20" : "108");
                                responseCheckSoc.setDes(LabelUtil.getKey("soc_waiting_result", locale));
                            } else {
                                /*Step 4: Kiem tra ket qua tu soc*/
                                if (Constants.WAITING.equals(statusConfSoc)) {
                                    responseCheckSoc.setResult("108");
                                    responseCheckSoc.setDes(LabelUtil.getKey("soc_waiting_result", locale));
                                } else if (Constants.REJECT.equals(statusConfSoc)) {
                                    StringBuilder requestSocStr = new StringBuilder();
                                    RequestAnalysSocResponse response = SocService.requestAnalysSoc(task.getAccount(), task.getTaskMngtId(), requestSocStr);
                                    new SubFbConfigSupplier().insertLog(cmSession, requestSoc.get(0).getId(), Constants.SOC_KEY_FAIL.equals(response.getReturn().getKey()) ? Constants.REJECT : Constants.WAITING, requestSocStr.toString(), new Gson().toJson(response), Constants.ACTION_REQUEST_ANALYSIS_SOC, null, new Date(), staffCode, "000");
                                    statusConfSoc = Constants.SOC_KEY_FAIL.equals(response.getReturn().getKey()) ? Constants.REJECT : Constants.WAITING;
                                    requestSoc.get(0).setStatusConf(Constants.WAITING);
                                    responseCheckSoc.setResult(Constants.SOC_KEY_FAIL.equals(response.getReturn().getKey()) ? "-20" : "108");
                                    responseCheckSoc.setDes(LabelUtil.getKey("soc_waiting_result", locale));
                                } else {
                                    responseCheckSoc.setDes("Check SOC successfully, out and back to this screen to update again");
                                }
                            }
                        }
                    } else {
                        corporateWhiteListDAO.disabledWhiteListTask(cmSession, task.getAccount(), staffCode);
                        List<SubFbConfig> requestSoc = new SubFbConfigSupplier().find(cmSession, null, task.getTaskMngtId(), Constants.STATUS_USE, null, null, null, Constants.TYPE_CONF_SOC);
                        if (requestSoc == null || requestSoc.isEmpty()) {
                            SubFbConfig subFbConfSoc = new SubFbConfigSupplier().insert(cmSession, task.getTaskMngtId(), task.getSubId(), task.getAccount(), null, null, null, null, null, null, new Date(), staffCode, null, Constants.TYPE_CONF_SOC, Constants.STATUS_USE, Constants.APPROVED);
                            subFbConfSoc.setDescription("account in white list");
                            cmSession.save(subFbConfSoc);
                        } else {
                            requestSoc.get(0).setDescription("account in white list");
                            requestSoc.get(0).setStatusConf(Constants.APPROVED);
                            cmSession.save(requestSoc.get(0));
                        }
                    }
                    cmSession.beginTransaction().commit();
                    cmSession.beginTransaction();
                }
            }
        }
        statusConfigSocStr.append(statusConfSoc);
        return responseCheckSoc;
    }

    public WSRespone checkSocComplain(HibernateHelper hibernateHelper, String locale, Long taskStaffMngtId, String progress, String staffCode) {
        Session cmSession = null;
        WSRespone wsResponse = new WSRespone();
        boolean hasErr = false;
        try {
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            StringBuilder confStr = new StringBuilder();
            ResultBO resultBo = checkSocComplaint(cmSession, taskStaffMngtId, progress, staffCode, confStr, locale);
            wsResponse.setErrorCode(confStr.toString());
            wsResponse.setErrorDecription(resultBo.getDes());
        } catch (Exception ex) {
            hasErr = true;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            } else {
                commitTransactions(cmSession);
            }
            closeSessions(cmSession);
        }
        return wsResponse;
    }

    public int needCheckSOC(Session cmSession, Long taskMngtId, Long taskStaffMngtId, Long subId) {
        Long flag;
        CorporateWhiteListDAO corporateWhiteListDAO = new CorporateWhiteListDAO();
        SubAdslLeaseline sub = new SubAdslLeaselineDAO().findById(cmSession, subId);
        /*Kiem tra account co trong white_list*/
        List<CorporateWhiteList> whiteList = corporateWhiteListDAO.findWhiteListTask(cmSession, sub.getAccount());
        if (whiteList == null || whiteList.isEmpty()) {
            /*Step 1: Kiem tra xem loai dich vu này co can check soc hay khong*/
            List<ApParam> param = new ApParamSupplier().findByEachProperty(cmSession, Constants.PARAM_TYPE_SERVICE_CHECK_SOC, null, sub.getInfraType() == null ? Constants.INFRA_AON : sub.getInfraType(), null);
            if (param != null && !param.isEmpty()
                    && param.get(0).getParamValue() != null
                    && param.get(0).getParamValue().toLowerCase().contains(sub.getServiceType().toLowerCase())) {
                /*Step 2: Kiem tra da tao request analysis sang soc chua*/
                List<SubFbConfig> requestSoc = new SubFbConfigSupplier().find(cmSession, null, taskMngtId, Constants.STATUS_USE, null, null, null, Constants.TYPE_CONF_SOC);
                if (requestSoc != null && !requestSoc.isEmpty()) {
                    /*Kiem tra ket qua tu soc*/
                    Long statusConfSoc = requestSoc.get(0).getStatusConf();
                    if (Constants.WAITING.equals(statusConfSoc) || Constants.REJECT.equals(statusConfSoc)) {
                        flag = Constants.STATUS_USE;
                    } else {
                        flag = Constants.STATUS_NOT_USE;
                    }
                } else {
                    flag = Constants.STATUS_USE;
                }
            } else {
                flag = Constants.STATUS_NOT_USE;
            }
        } else {
            flag = Constants.STATUS_NOT_USE;
        }
        return flag.intValue();
    }

    public void addDeviceToUserBundle(String serialBundle, String url, ResultBO resultBO, WSRespone wSRespone, SubBundleTv subTv, Session cmSession, List<SubBundleTvLog> listLog) {
        Request requestBundle = new Request();
        requestBundle.findDeviceBySerial(serialBundle);
        String data = new Gson().toJson(requestBundle);
        ServiceSupplier serviceSup = new ServiceSupplier();
        String response = serviceSup.sendRequest(Constants.SERVICE_METHOD_POST, url, data, null, null);
        SubBundleTvLog log = new SubBundleTvLog();
        log.setSubId(subTv.getSubId());
        log.setAction(Request.ACTION_FIND_DEVICE_BY_SERIAL);
        log.setAccount(subTv.getAccount());
        log.setRequest(data);
        log.setResponse(response);
        log.setCreateDate(new Date());
        listLog.add(log);
        if (response == null || response.isEmpty()) {
            resultBO.setResult(Constants.RESPONSE_FAIL);
            wSRespone.setErrorDecription("Error from call service findDeviceBySerial bunlde tv");
        } else {
            WSRespone ws = new Gson().fromJson(response, WSRespone.class);
            ServiceResponse serviceResponse = ws == null || ws.getErrorDecription() == null ? null : new Gson().fromJson(ws.getErrorDecription(), ServiceResponse.class);
            if (serviceResponse == null || !Constants.RESPONSE_SUCCESS.equals(serviceResponse.getResult())
                    || serviceResponse.getContent() == null || serviceResponse.getContent().getUnique_id() == null) {
                resultBO.setResult(Constants.RESPONSE_FAIL);
                wSRespone.setErrorDecription("Error from call service findDeviceBySerial bunlde tv " + (serviceResponse != null ? serviceResponse.getMessage() : ""));
            } else {
                requestBundle = new Request();
                String deviceUniqueId = serviceResponse.getContent().getUnique_id();
                requestBundle.addDeviceToUser(subTv.getUserUniqueId(), serialBundle);
                data = new Gson().toJson(requestBundle);
                response = serviceSup.sendRequest(Constants.SERVICE_METHOD_POST, url, data, null, null);
                SubBundleTvLog log1 = new SubBundleTvLog();
                log1.setSubId(subTv.getSubId());
                log1.setAction(Request.ACTION_ADD_DEVICE_TO_USER);
                log1.setAccount(subTv.getAccount());
                log1.setRequest(data);
                log1.setResponse(response);
                log1.setCreateDate(new Date());
                listLog.add(log1);
                if (response == null || response.isEmpty()) {
                    resultBO.setResult(Constants.RESPONSE_FAIL);
                    wSRespone.setErrorDecription("Error from call service addDeviceToUser bunlde tv");
                } else {
                    ws = new Gson().fromJson(response, WSRespone.class);
                    serviceResponse = ws == null || ws.getErrorDecription() == null ? null : new Gson().fromJson(ws.getErrorDecription(), ServiceResponse.class);
                    if (serviceResponse == null || !Constants.RESPONSE_SUCCESS.equals(serviceResponse.getResult())) {
                        resultBO.setResult(Constants.RESPONSE_FAIL);
                        wSRespone.setErrorDecription("Error from call service addDeviceToUser bunlde tv " + (serviceResponse != null ? serviceResponse.getMessage() : ""));
                    } else {
                        subTv.setDeviceUniqueId(deviceUniqueId);
                        subTv.setBoxSerial(serialBundle);
                        cmSession.save(subTv);
                        cmSession.flush();
                    }
                }
            }
        }
    }

    public ParamOut changeDeviceAccFtth(HibernateHelper hibernateHelper, String locale, String account, String deviceCode) {
        Session cmSession = null;
        ParamOut wsResponse = new ParamOut();
        boolean hasErr = false;
        wsResponse.setErrorCode(Constants.ERROR_CODE_1);
        try {
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            /*check device*/
            StringBuilder sql = new StringBuilder("select device_id from nims_fcn.infra_device where device_code = ?");
            Query query = cmSession.createSQLQuery(sql.toString());
            query.setParameter(0, deviceCode);
            List list = query.list();
            if (list == null || list.isEmpty()) {
                return new ParamOut(Constants.ERROR_CODE_1, "Can not find device " + deviceCode);
            }
            Long deviceId = Long.parseLong(list.get(0).toString());
            /*check task is progressing and type is TKHDM*/
            sql = new StringBuilder("select sub_id from task_management where account = ? and job_code = 'TKHDM' and progress = 1");
            query = cmSession.createSQLQuery(sql.toString());
            query.setParameter(0, account);
            list = query.list();
            if (list == null || list.isEmpty()) {
                return new ParamOut(Constants.ERROR_CODE_1, "Can not find task for " + account);
            }
            Long subId = Long.parseLong(list.get(0).toString());
            /*update sub_deployment*/
            sql = new StringBuilder("update sub_deployment set dslam_id = ? where sub_id = ? and status = 1");
            query = cmSession.createSQLQuery(sql.toString());
            query.setParameter(0, deviceId);
            query.setParameter(1, subId);
            query.executeUpdate();
            wsResponse.setErrorCode(Constants.ERROR_CODE_0);
            wsResponse.setErrorDecription("Success");
        } catch (Exception ex) {
            wsResponse.setErrorDecription(ex.getMessage());
            ex.printStackTrace();
            hasErr = true;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            } else {
                commitTransactions(cmSession);
            }
            closeSessions(cmSession);
        }
        return wsResponse;
    }
}
