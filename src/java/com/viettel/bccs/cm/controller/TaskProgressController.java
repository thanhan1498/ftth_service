/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.bccs.api.Task.BO.WarningTaskView;
import com.viettel.bccs.api.Task.DAO.AccountAgentDAO;
import com.viettel.bccs.api.Task.DAO.CustomerDAO;
import com.viettel.bccs.api.Task.DAO.ManagerTaskDAO;
import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.TaskProgress;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.common.util.FileSaveProcess;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.ImageInput;
import com.viettel.brcd.ws.model.output.ImageRespone;
import com.viettel.brcd.ws.model.output.NotificationTaskOut;
import com.viettel.brcd.ws.model.output.TaskProgressOut;
import com.viettel.brcd.ws.model.output.TaskWarningOut;
import com.viettel.brcd.ws.model.output.UploadImageOut;
import com.viettel.brcd.ws.model.output.WarningTask;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.im.database.BO.AccountAgent;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author Nguyen Ngoc Viet
 */
public class TaskProgressController extends BaseController {

    public TaskProgressController() {
        logger = Logger.getLogger(TaskProgressController.class);
    }

    public TaskProgressOut getTaskProgress(HibernateHelper hibernateHelper, int type, String local) {
        TaskProgressOut taskProgressOut = new TaskProgressOut();
        List<TaskProgress> list = new ArrayList<TaskProgress>();
        try {
            switch (type) {
                case 0:
                    taskProgressOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                    taskProgressOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
                    TaskProgress taskProgress = new TaskProgress();
                    taskProgress.setCode(com.viettel.bccs.cm.util.Constants.STAFF_START);
                    taskProgress.setDescription(LabelUtil.getKey("task.progress.team", local));
                    list.add(taskProgress);
                    taskProgress = new TaskProgress();
                    taskProgress.setCode(com.viettel.bccs.cm.util.Constants.TEAM_START);
                    taskProgress.setDescription(LabelUtil.getKey("task.progress.staff", local));
                    list.add(taskProgress);
                    taskProgressOut.setLstProgress(list);
                    break;
                case 1:
                    taskProgressOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                    taskProgressOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
                    TaskProgress taskProgressTeam = new TaskProgress();
                    taskProgressTeam.setCode(com.viettel.bccs.cm.util.Constants.STAFF_START);
                    taskProgressTeam.setDescription(LabelUtil.getKey("progress.start.cancel.task.start", local));
                    list.add(taskProgressTeam);
                    taskProgressTeam = new TaskProgress();
                    taskProgressTeam.setCode(com.viettel.bccs.cm.util.Constants.TEAM_START);
                    taskProgressTeam.setDescription(LabelUtil.getKey("task.progress.team", local));
                    list.add(taskProgressTeam);
                    taskProgressOut.setLstProgress(list);
                    break;
                case 2:
                    taskProgressOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                    taskProgressOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
                    TaskProgress taskProgressRevoke = new TaskProgress();
                    taskProgressRevoke.setCode(com.viettel.bccs.cm.util.Constants.STAFF_START);
                    taskProgressRevoke.setDescription(LabelUtil.getKey("progress.start.update.task", local));
                    list.add(taskProgressRevoke);
                    taskProgressRevoke = new TaskProgress();
                    taskProgressRevoke.setCode(com.viettel.bccs.cm.util.Constants.STAFF_PERFORM);
                    taskProgressRevoke.setDescription(LabelUtil.getKey("progress.perform.update.task", local));
                    list.add(taskProgressRevoke);
                    taskProgressRevoke = new TaskProgress();
//                    taskProgressRevoke.setCode(com.viettel.bccs.cm.util.Constants.REQUEST_TO_CHANGE_STAFF);
//                    taskProgressRevoke.setDescription(LabelUtil.getKey("progress.request.task.update.task", local));
                    taskProgressRevoke.setCode(com.viettel.bccs.cm.util.Constants.REQUEST_CONFIG);
                    taskProgressRevoke.setDescription(LabelUtil.getKey("progress.request.task.config.task", local));
                    list.add(taskProgressRevoke);
                    taskProgressOut.setLstProgress(list);
                    break;
                default:
                    taskProgressOut.setErrorCode("-3");
                    taskProgressOut.setErrorDecription("err:" + "input is invalid");
                    break;
            }
        } catch (Exception ex) {
            logger.error("getSourceType: " + ex.getMessage());
            taskProgressOut.setErrorCode("-99");
            taskProgressOut.setErrorDecription("system is busy: " + ex.getMessage());
        }
        return taskProgressOut;
    }

    public TaskProgressOut getProgressForUpdateTask(HibernateHelper hibernateHelper, int type, String local) {
        TaskProgressOut taskProgressOut = new TaskProgressOut();
        List<TaskProgress> list = new ArrayList<TaskProgress>();
        try {
            switch (type) {
                case 1:
                    taskProgressOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                    taskProgressOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
                    TaskProgress taskProgress = new TaskProgress();
                    taskProgress.setCode(com.viettel.bccs.cm.util.Constants.STAFF_START);
                    taskProgress.setDescription(LabelUtil.getKey("progress.start.update.task", local));
                    list.add(taskProgress);
                    taskProgress = new TaskProgress();
                    taskProgress.setCode(com.viettel.bccs.cm.util.Constants.STAFF_PERFORM);
                    taskProgress.setDescription(LabelUtil.getKey("progress.perform.update.task", local));
                    list.add(taskProgress);
                    taskProgressOut.setLstProgress(list);
                    break;
                case 2:
                    taskProgressOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                    taskProgressOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
                    TaskProgress taskProgressCancelTask = new TaskProgress();
                    taskProgressCancelTask.setCode(com.viettel.bccs.cm.util.Constants.PERFORMING);
                    taskProgressCancelTask.setDescription(LabelUtil.getKey("progress.start.cancel.task.processing", local));
                    list.add(taskProgressCancelTask);
                    taskProgressCancelTask = new TaskProgress();
                    taskProgressCancelTask.setCode(com.viettel.bccs.cm.util.Constants.START);
                    taskProgressCancelTask.setDescription(LabelUtil.getKey("progress.start.cancel.task.start", local));
                    list.add(taskProgressCancelTask);
                    taskProgressCancelTask = new TaskProgress();
                    taskProgressCancelTask.setCode(com.viettel.bccs.cm.util.Constants.FINISH);
                    taskProgressCancelTask.setDescription(LabelUtil.getKey("progress.start.cancel.task.finish", local));
                    list.add(taskProgressCancelTask);
                    taskProgressCancelTask = new TaskProgress();
                    taskProgressCancelTask.setCode(com.viettel.bccs.cm.util.Constants.CANCELED);
                    taskProgressCancelTask.setDescription(LabelUtil.getKey("progress.start.cancel.task.cancelled", local));
                    list.add(taskProgressCancelTask);
                    taskProgressOut.setLstProgress(list);
                    break;
                default:
                    taskProgressOut.setErrorCode("-3");
                    taskProgressOut.setErrorDecription("err:" + "input is invalid");
                    break;
            }
        } catch (Exception ex) {
            logger.error("getSourceType: " + ex.getMessage());
            taskProgressOut.setErrorCode("-99");
            taskProgressOut.setErrorDecription("system is busy: " + ex.getMessage());
        }
        return taskProgressOut;
    }

    public TaskProgressOut getProgressForUpdateInfoTask(HibernateHelper hibernateHelper, int type, String local) {
        TaskProgressOut taskProgressOut = new TaskProgressOut();
        List<TaskProgress> list = new ArrayList<TaskProgress>();
        try {
            TaskProgress taskProgress = new TaskProgress();
            taskProgressOut.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
            taskProgressOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
            taskProgress.setCode(com.viettel.bccs.cm.util.Constants.STAFF_START);
            taskProgress.setDescription(LabelUtil.getKey("progress.start.update.task", local));
            list.add(taskProgress);
            taskProgress = new TaskProgress();
            taskProgress.setCode(com.viettel.bccs.cm.util.Constants.STAFF_PERFORM);
            taskProgress.setDescription(LabelUtil.getKey("progress.perform.update.task", local));
            list.add(taskProgress);
            taskProgress = new TaskProgress();
            taskProgress.setCode(com.viettel.bccs.cm.util.Constants.REQUEST_TO_CHANGE_STAFF);
            taskProgress.setDescription(LabelUtil.getKey("progress.request.task.update.task", local));
            list.add(taskProgress);
            taskProgress = new TaskProgress();
            taskProgress.setCode(com.viettel.bccs.cm.util.Constants.FINISH_TASK);
            taskProgress.setDescription(LabelUtil.getKey("progress.finish.update.task", local));
            list.add(taskProgress);
            taskProgressOut.setLstProgress(list);
            taskProgress = new TaskProgress();
            taskProgress.setCode(com.viettel.bccs.cm.util.Constants.REQUEST_TO_CHANGE_TEAM);
            taskProgress.setDescription(LabelUtil.getKey("progress.request.team.update.task", local));
            list.add(taskProgress);
            if (type == 1) {
                /*complaint*/
            } else {
                taskProgress = new TaskProgress();
                taskProgress.setCode(com.viettel.bccs.cm.util.Constants.REQUEST_CONFIG);
                taskProgress.setDescription(LabelUtil.getKey("progress.request.config.update.task", local));
                list.add(taskProgress);
                taskProgressOut.setLstProgress(list);
            }
        } catch (Exception ex) {
            logger.error("getSourceType: " + ex.getMessage());
            taskProgressOut.setErrorCode("-99");
            taskProgressOut.setErrorDecription("system is busy: " + ex.getMessage());
        }
        return taskProgressOut;
    }

    public UploadImageOut uploadImage(HibernateHelper hibernateHelper, String local, String userLogin, Long subId, String account, List<ImageInput> lstImage) {
        UploadImageOut uploadImageOut = new UploadImageOut();
        Session sessionPos = null;
        try {
            if (subId == null) {
                uploadImageOut.setErrorCode("-1");
                uploadImageOut.setErrorDecription("subId is not empty");
                return uploadImageOut;
            }
            if (account == null || account.trim().isEmpty() || account.trim().equals("?")) {
                uploadImageOut.setErrorCode("-1");
                uploadImageOut.setErrorDecription("account is not empty");
                return uploadImageOut;
            }
            if (userLogin == null || userLogin.trim().isEmpty() || userLogin.trim().equals("?")) {
                uploadImageOut.setErrorCode("-1");
                uploadImageOut.setErrorDecription("userLogin is not empty");
                return uploadImageOut;
            }
            if (lstImage == null || lstImage.isEmpty()) {
                uploadImageOut.setErrorCode("-1");
                uploadImageOut.setErrorDecription("image is not empty");
                return uploadImageOut;
            }
            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            for (ImageInput imageInput : lstImage) {
                boolean checkUpload = new FileSaveProcess().saveFile(imageInput.getImageName(), imageInput.getImage(), account, logger);
                if (!checkUpload) {
                    logger.info("\n--------------\nUpload image Faile\n--------------\n");
                    uploadImageOut.setErrorCode("-1");
                    uploadImageOut.setErrorDecription("Upload image fail");
                    return uploadImageOut;
                }
                Long idImage = Common.getSequence("CUST_IMAGE_SEQ", sessionPos);
                int checkInsertDB = new CustomerDAO().insertCusImage(sessionPos, subId, account, imageInput.getImageName(), "CONTRACT_IMG", userLogin, idImage);
                if (checkInsertDB == 0) {
                    uploadImageOut.setErrorCode("-99");
                    uploadImageOut.setErrorDecription("Insert DB fail image " + imageInput.getImageName());
                    return uploadImageOut;
                }
            }
            uploadImageOut.setErrorCode(Constants.RESPONSE_SUCCESS);
            uploadImageOut.setErrorDecription(Constants.SUCCESS);
        } catch (Exception ex) {
            logger.error("uploadImage: " + ex.getMessage());
            uploadImageOut.setErrorCode("-99");
            uploadImageOut.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            if (sessionPos != null) {
                commitTransactions(sessionPos);
                sessionPos.close();
            }
        }
        return uploadImageOut;
    }

    public NotificationTaskOut getNotificationTaskOut(HibernateHelper hibernateHelper, String local, String serial) {
        NotificationTaskOut notificationOut = new NotificationTaskOut();
        Session sessionPos = null;
        Session imSession = null;
        try {
            if (serial == null || Common.isNullOrEmpty(serial)) {
                return new NotificationTaskOut("-1", "Not found serial");
            }
            String isdn = Common.isNullOrEmpty(serial) ? "" : serial;

            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);

            AccountAgentDAO accountAgentDAO = new AccountAgentDAO();

            AccountAgent agent = accountAgentDAO.findAccountBySerial(imSession, isdn);

            Long staffId = 0L;
            if (agent != null) {
                staffId = agent.getOwnerId();
                ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();

                Staff staff = new StaffDAO().findById(sessionPos, agent.getOwnerId());
                Long shopId = 0L;

                if (staff != null) {
                    shopId = staff.getShopId();
                }

                int reVal = managerTaskDAO.getNotificationTaskForStaff(sessionPos, shopId, staffId);
                notificationOut.setTotalTaskNew(String.valueOf(reVal));
                notificationOut.setErrorCode("0");
                notificationOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);

            } else {
                notificationOut.setErrorCode("-2");
                notificationOut.setErrorDecription("Not found agent with serial");
            }
        } catch (Exception ex) {
            logger.error("getNotificationTaskOut: " + ex.getMessage());
            notificationOut.setErrorCode("-99");
            notificationOut.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            if (sessionPos != null) {
                commitTransactions(sessionPos);

                sessionPos.close();
            }
            if (imSession != null) {

                imSession.close();
            }
        }
        return notificationOut;
    }

    public TaskWarningOut getListWarningTaskForStaff(HibernateHelper hibernateHelper, String local, String shopId, String pStaffId) {
        TaskWarningOut taskWarningOut = new TaskWarningOut();
        List<WarningTask> lstWarningTask = new ArrayList<WarningTask>();
        Session sessionPos = null;
        try {
            if (Common.isNullOrEmpty(shopId)) {
                return new TaskWarningOut("-1", "Not found user login");
            }
            Long staffId = Common.isNullOrEmpty(pStaffId) ? 0L : Long.valueOf(pStaffId);
            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);

            List lstTask = null;
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();

            lstTask = managerTaskDAO.getListWarningTaskForStaff(sessionPos, staffId);

            if (lstTask != null && lstTask.size() > 0) {
                taskWarningOut.setErrorCode("0");
                taskWarningOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
                for (Iterator iterator = lstTask.iterator(); iterator.hasNext();) {
                    WarningTask warningTask = new WarningTask();
                    WarningTaskView vWarningTask = (WarningTaskView) iterator.next();
                    warningTask.setTotalTask(vWarningTask.getTotalTask());
                    warningTask.setSourceType(vWarningTask.getSourceType());
                    warningTask.setTotalTaskExpire(vWarningTask.getTotalTaskExpire());
                    warningTask.setTodoTask(vWarningTask.getTodoTask());
                    warningTask.setTotalTaskFinishInMonth(vWarningTask.getTotalTaskFinishInMonth());
                    warningTask.setTotalTaskInMonth(vWarningTask.getTotalTaskInMonth());
                    warningTask.setTotalTaskExpireName(vWarningTask.getTotalTaskExpire().toString() + "/" + vWarningTask.getTotalTaskInMonth());
                    warningTask.setTotalTaskTodoName(vWarningTask.getTodoTask().toString() + "/" + vWarningTask.getTotalTaskInMonth());
                    warningTask.setTotalTaskFinishName(vWarningTask.getTotalTaskFinishInMonth().toString() + "/" + vWarningTask.getTotalTaskInMonth());
                    lstWarningTask.add(warningTask);
                }
                taskWarningOut.setLstWarningTask(lstWarningTask);

            } else {
                taskWarningOut.setErrorCode("0");
                taskWarningOut.setErrorDecription(com.viettel.bccs.cm.util.Constants.SUCCESS);
                taskWarningOut.setLstWarningTask(new ArrayList<WarningTask>());
            }
        } catch (Exception ex) {
            logger.error("searchTask: " + ex.getMessage());
            taskWarningOut.setErrorCode("-99");
            taskWarningOut.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            if (sessionPos != null) {
                sessionPos.close();
            }
        }
        return taskWarningOut;
    }

    public ImageRespone viewImage(HibernateHelper hibernateHelper, String account) {
        ImageRespone ImageRespone = new ImageRespone();
        Session sessionPos = null;
        try {
            if (account == null || account.trim().isEmpty() || account.trim().equals("?")) {
                ImageRespone.setErrorCode("-1");
                ImageRespone.setErrorDecription("userLogin is not empty");
                return ImageRespone;
            }
            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);

            List<String> lstImage = new CustomerDAO().getImageName(sessionPos, account);
            List<String> lstData = new ArrayList<String>();
            if (lstImage != null) {
                for (String imageName : lstImage) {
                    String imageData = new FileSaveProcess().getFile(imageName, account, logger);
                    lstData.add(imageData);
                }
                ImageRespone.setErrorCode(Constants.RESPONSE_SUCCESS);
                ImageRespone.setErrorDecription(Constants.SUCCESS);
                ImageRespone.setData(lstData);
            } else {
                ImageRespone.setErrorCode("-2");
                ImageRespone.setErrorDecription("not found Image");
                return ImageRespone;
            }
        } catch (Exception ex) {
            logger.error("uploadImage: " + ex.getMessage());
            ImageRespone.setErrorCode("-99");
            ImageRespone.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            if (sessionPos != null) {
                commitTransactions(sessionPos);
                sessionPos.close();
            }
        }
        return ImageRespone;
    }

    public ImageRespone viewImageCustomer(HibernateHelper hibernateHelper, String cusId) {
        ImageRespone ImageRespone = new ImageRespone();
        Session sessionPos = null;
        try {
            if (cusId == null || cusId.trim().isEmpty() || cusId.trim().equals("?")) {
                ImageRespone.setErrorCode("-1");
                ImageRespone.setErrorDecription("cusId is not empty");
                return ImageRespone;
            }
            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);

            List<String> lstImage = new CustomerDAO().getImageNameByCustId(sessionPos, cusId);
            List<String> lstData = new ArrayList<String>();
            if (lstImage != null) {
                for (String imageName : lstImage) {
                    String imageData = new FileSaveProcess().getFile(imageName, cusId, logger);
                    lstData.add(imageData);
                }
                ImageRespone.setErrorCode(Constants.RESPONSE_SUCCESS);
                ImageRespone.setErrorDecription(Constants.SUCCESS);
                ImageRespone.setData(lstData);
            } else {
                ImageRespone.setErrorCode("-2");
                ImageRespone.setErrorDecription("not found Image");
                return ImageRespone;
            }
        } catch (Exception ex) {
            logger.error("uploadImage: " + ex.getMessage());
            ImageRespone.setErrorCode("-99");
            ImageRespone.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            if (sessionPos != null) {
                commitTransactions(sessionPos);
                sessionPos.close();
            }
        }
        return ImageRespone;
    }
}
