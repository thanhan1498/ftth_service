/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller;

import com.viettel.bccs.api.Task.BO.AssignTaskBO;
import com.viettel.bccs.api.Task.BO.ResultBO;
import com.viettel.bccs.api.Task.BO.Shop;
import com.viettel.bccs.api.Task.BO.Staff;
import com.viettel.bccs.api.Task.BO.TaskManagement;
import com.viettel.bccs.api.Task.BO.ViewAssignTask;
import com.viettel.bccs.api.Task.DAO.ManagerTaskDAO;
import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.api.Util.Constant;
import com.viettel.bccs.api.Util.DateTimeUtils;
import com.viettel.bccs.api.common.ParameterOut;
import com.viettel.bccs.cm.bussiness.SubAdslLeaselineBussines;
import com.viettel.bccs.cm.dao.NotificationMessageDAO;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.dao.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.dao.TaskDAO;
import com.viettel.bccs.cm.dao.TaskManagementDAO;
import com.viettel.bccs.cm.model.AssignToObject;
import com.viettel.bccs.cm.model.KpiDeadlineTask;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.TaskInfoDetail;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.dao.pre.IsdnSendSmsDAO;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.SearchTaskInput;
import com.viettel.brcd.ws.model.input.TaskToAssignIn;
import com.viettel.brcd.ws.model.output.BroadbandTabInfo;
import com.viettel.brcd.ws.model.output.CoordinateSubDrawCable;
import com.viettel.brcd.ws.model.output.DashboardBroadbanTab;
import com.viettel.brcd.ws.model.output.TaskAssign;
import com.viettel.brcd.ws.model.output.TaskDetailToAssignOut;
import com.viettel.brcd.ws.model.output.TaskToAssignOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Nguyen ngoc viet
 */
public class TaskToAssignController extends BaseController {

    String local;

    public TaskToAssignController() {
        logger = Logger.getLogger(TaskToAssignController.class);
    }

    //edit by duyetdk
    public TaskToAssignOut searchTask(HibernateHelper hibernateHelper, SearchTaskInput searchTaskInput, String locale) {
        TaskToAssignOut taskToAssign = new TaskToAssignOut();
        Session sessionPos = null;
        try {
            if (searchTaskInput.getShopIdToken() == null) {
                return new TaskToAssignOut("-1", "Not found user login");
            }
            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            Date nowDate = Common.getDateTimeFromDB(sessionPos);
            Long assignTo = (searchTaskInput.getAssignTo() != null) ? searchTaskInput.getAssignTo() : Constant.FOR_STAFF;
            boolean isNOC = false;
            List lstTask = null;
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            Long custReqId = 0L;
            Long shopId = 0L;
            Long shopIdToken = 0L;
            List<CoordinateSubDrawCable> lstCoor = null;
            StringBuilder total = new StringBuilder();
            try {
                custReqId = searchTaskInput.getCustReqId().isEmpty() ? null : Long.valueOf(searchTaskInput.getCustReqId());
            } catch (Exception ex) {
                taskToAssign.setErrorCode("-98");
                taskToAssign.setErrorDecription("CustReqId value is invalid");
                return taskToAssign;
            }
            try {
                shopId = searchTaskInput.getShopId().isEmpty() ? null : Long.valueOf(searchTaskInput.getShopId());
            } catch (Exception ex) {
                taskToAssign.setErrorCode("-98");
                taskToAssign.setErrorDecription("shopId value is invalid");
                return taskToAssign;
            }
            try {
                shopIdToken = searchTaskInput.getShopIdToken().isEmpty() ? null : Long.valueOf(searchTaskInput.getShopIdToken());
            } catch (Exception ex) {
                taskToAssign.setErrorCode("-98");
                taskToAssign.setErrorDecription("shopIdToken value is invalid");
                return taskToAssign;
            }
            if (assignTo.equals(Constant.FOR_STAFF)) {
                /*Giao việc cho nhân viên*/
                lstTask = managerTaskDAO.getAssignTaskForStaff(sessionPos, custReqId,
                        shopId,
                        searchTaskInput.getContractNo(), searchTaskInput.getSourceType(),
                        searchTaskInput.getAccount(), searchTaskInput.getProgress(),
                        searchTaskInput.getStaDate(), searchTaskInput.getEndDate(),
                        isNOC, nowDate, shopIdToken, searchTaskInput.getPage(), searchTaskInput.getPageSize(), total);
            } else {
                /*Giao việc cho tổ*/
                lstTask = managerTaskDAO.getAssignTaskForShop(sessionPos, custReqId,
                        searchTaskInput.getContractNo(), searchTaskInput.getSourceType(),
                        searchTaskInput.getAccount(), searchTaskInput.getProgress(),
                        searchTaskInput.getStaDate(), searchTaskInput.getEndDate(),
                        isNOC, nowDate, shopIdToken, searchTaskInput.getPage(), searchTaskInput.getPageSize(), total);
            }
            List listResult = new ArrayList();
            taskToAssign.setTotal(total.toString());
            if (lstTask != null && lstTask.size() > 0) {
                taskToAssign.setErrorCode("0");
                taskToAssign.setErrorDecription(LabelUtil.getKey("common.success", locale));
                TaskDAO task = new TaskDAO();
                for (Iterator iterator = lstTask.iterator(); iterator.hasNext();) {
                    TaskAssign taskAssignOut = new TaskAssign();
                    SubAdslLeaseline sub;
                    if (assignTo.equals(Constant.FOR_STAFF)) {
                        ViewAssignTask viewAssignTask = (ViewAssignTask) iterator.next();
                        lstCoor = new SubAdslLeaselineBussines().findCoordinateBySubId(sessionPos, viewAssignTask.getAccount());
                        taskAssignOut.setAccount(viewAssignTask.getAccount());
                        sub = new SubAdslLeaselineDAO().findByAccount(sessionPos, viewAssignTask.getAccount());
                        taskAssignOut.setContractNo(viewAssignTask.getContractNo());
                        taskAssignOut.setDeployAddress(viewAssignTask.getDeployAddress());
                        taskAssignOut.setIsSystemError(viewAssignTask.getIsSystemError());
                        taskAssignOut.setLimitDate(DateTimeUtils.convertDateTimeToString(viewAssignTask.getLimitDate(), "dd/MM/yyyy HH:mm"));
                        taskAssignOut.setStaffName(viewAssignTask.getStaffName());
                        taskAssignOut.setTaskCreateDate(DateTimeUtils.convertDateTimeToString(viewAssignTask.getTaskCreateDate(), "dd/MM/yyyy HH:mm"));
                        taskAssignOut.setTaskMngtId(viewAssignTask.getTaskMngtId());
                        taskAssignOut.setTaskName(viewAssignTask.getTaskName());
                        taskAssignOut.setTaskShopProgress(Long.parseLong(viewAssignTask.getTaskShopProgress()));
                        taskAssignOut.setProgress(searchTaskInput.getProgress());
                        taskAssignOut.setAssignTo(String.valueOf(Constant.FOR_STAFF));
                        taskAssignOut.setCustReqId(viewAssignTask.getCustReqId());
                        taskAssignOut.setCustName(viewAssignTask.getCustName());
                        taskAssignOut.setCustTelFax(viewAssignTask.getTelFax());
                        taskAssignOut.setTextColor(viewAssignTask.getTextColour());
                        taskAssignOut.setBackgroundColor(viewAssignTask.getBackGroundColour());
                        taskAssignOut.setCoordinates(lstCoor);
                        if (lstCoor != null && lstCoor.size() >= 2) {
                            double d = distanceBetween2Points(lstCoor.get(0).getLat(), lstCoor.get(0).getLng(), lstCoor.get(1).getLat(), lstCoor.get(1).getLng());
                            taskAssignOut.setDistanceToInfra(String.valueOf(d));
                        } else {
                            taskAssignOut.setDistanceToInfra("N/A");
                        }
                        KpiDeadlineTask kpi = sub == null ? null : task.getKpiDeadLineTask(sessionPos, viewAssignTask.getJobCode(), sub.getInfraType(), sub.getProductCode(), sub.getSubType(), sub.getServiceType());
                        if (kpi != null && !"red".equalsIgnoreCase(viewAssignTask.getBackGroundColour())) {
                            if (kpi.getWarningTime() * 60 * 60 * 1000 <= nowDate.getTime() - viewAssignTask.getTaskCreateDate().getTime()) {
                                taskAssignOut.setTextColor("yellow");
                                taskAssignOut.setBackgroundColor("yellow");
                            } else {
                                taskAssignOut.setBackgroundColor("green");
                            }
                        }
                    } else {
                        TaskManagement taskManagement = (TaskManagement) iterator.next();
                        lstCoor = new SubAdslLeaselineBussines().findCoordinateBySubId(sessionPos, taskManagement.getAccount());
                        taskAssignOut.setAccount(taskManagement.getAccount());
                        sub = new SubAdslLeaselineDAO().findByAccount(sessionPos, taskManagement.getAccount());
                        taskAssignOut.setContractNo(taskManagement.getContractNo());
                        taskAssignOut.setDeployAddress(taskManagement.getDeployAddress());
                        taskAssignOut.setLimitDate(DateTimeUtils.convertDateTimeToString(taskManagement.getLimitDate(), "dd/MM/yyyy HH:mm"));
                        taskAssignOut.setShopName(taskManagement.getShopName());
                        taskAssignOut.setTaskCreateDate(DateTimeUtils.convertDateTimeToString(taskManagement.getTaskCreateDate(), "dd/MM/yyyy HH:mm"));
                        taskAssignOut.setTaskMngtId(taskManagement.getTaskMngtId());
                        taskAssignOut.setTaskName(taskManagement.getTaskName());
                        taskAssignOut.setTaskShopProgress(Long.parseLong(taskManagement.getProgress()));
                        taskAssignOut.setProgress(searchTaskInput.getProgress());
                        taskAssignOut.setAssignTo(String.valueOf(Constant.FOR_SHOP));
                        taskAssignOut.setCustReqId(taskManagement.getCustReqId());
                        taskAssignOut.setCustName(taskManagement.getName());
                        taskAssignOut.setCustTelFax(taskManagement.getTelFax());
                        taskAssignOut.setTextColor(taskManagement.getTextColour());
                        taskAssignOut.setBackgroundColor(taskManagement.getBackGroundColour());
                        taskAssignOut.setCoordinates(lstCoor);
                        if (lstCoor != null && lstCoor.size() >= 2) {
                            double d = distanceBetween2Points(lstCoor.get(0).getLat(), lstCoor.get(0).getLng(), lstCoor.get(1).getLat(), lstCoor.get(1).getLng());
                            taskAssignOut.setDistanceToInfra(String.valueOf(d));
                        } else {
                            taskAssignOut.setDistanceToInfra("N/A");
                        }
                        KpiDeadlineTask kpi = sub == null ? null : task.getKpiDeadLineTask(sessionPos, taskManagement.getJobCode(), sub.getInfraType(), sub.getProductCode(), sub.getSubType(), sub.getServiceType());
                        if (kpi != null && !"red".equalsIgnoreCase(taskManagement.getBackGroundColour())) {
                            if (kpi.getWarningTime() * 60 * 60 * 1000 <= nowDate.getTime() - taskManagement.getTaskCreateDate().getTime()) {
                                taskAssignOut.setTextColor("yellow");
                                taskAssignOut.setBackgroundColor("yellow");
                            } else {
                                taskAssignOut.setBackgroundColor("green");
                            }
                        }
                    }
                    boolean isVip = sub == null ? false : new SubAdslLeaselineDAO().isAccountVip(sessionPos, sub.getSubType(), sub.getProductCode());
                    taskAssignOut.setIsVip(isVip ? 1 : 0);
                    listResult.add(taskAssignOut);
                }
                taskToAssign.setListTask(listResult);

            } else {
                taskToAssign.setErrorCode("-2");
                taskToAssign.setErrorDecription(LabelUtil.getKey("not.found.data", locale));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("searchTask: " + ex.getMessage());
            taskToAssign.setErrorCode("-99");
            taskToAssign.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            if (sessionPos != null) {
                sessionPos.close();
            }
        }
        return taskToAssign;
    }

    public TaskDetailToAssignOut getTaskToAssign(HibernateHelper hibernateHelper, String locale, String strTaskMngtId, String endDateValue, String strAssignTo) {
        TaskDetailToAssignOut taskToAssign = new TaskDetailToAssignOut();
        Long startTime = System.currentTimeMillis();
        Session sessionPos = null;
        Session sessionIm = null;
        Session sessionPm = null;
        Session sessionCc = null;
        Session sessionNims = null;
        try {
            locale = this.local;
            logger.error("-------->   open connection getTaskToAssign ");
            logger.error("-------->   open connection SESSION_CM_POS ");
            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            logger.error("-------->   open connection SESSION_IM ");
            sessionIm = hibernateHelper.getSession(Constants.SESSION_IM);
            logger.error("-------->   open connection SESSION_CC ");
            sessionCc = hibernateHelper.getSession(Constants.SESSION_CC);
            logger.error("-------->   open connection SESSION_PRODUCT ");
            sessionPm = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            logger.error("-------->   open connection successs getTaskToAssign ");
            sessionNims = hibernateHelper.getSession(Constants.SESSION_NIMS);
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            ResultBO resultBO = managerTaskDAO.preAssignTask(sessionPos, sessionPm, sessionCc, sessionIm, strTaskMngtId, endDateValue, strAssignTo, sessionNims);
            if (resultBO.getResult() == null) {
                com.viettel.bccs.cm.model.TaskManagement task = new TaskManagementDAO().findById(sessionPos, Long.parseLong(strTaskMngtId));
                taskToAssign.setErrorCode(Constants.RESPONSE_SUCCESS);
                taskToAssign.setErrorDecription(LabelUtil.getKey("common.success", local));
                if (strAssignTo.equals(Constant.FOR_STAFF)) {
                    copyObject(taskToAssign, resultBO.getAssignTaskBO());
                } else {
                    copyObject(taskToAssign, resultBO.getAssignTaskBO());
                }
                taskToAssign.getTaskInfo().setAccBundleTv(task.getPathFileAttach());
            } else {
                taskToAssign.setErrorCode(Constants.RESPONSE_FAIL);
                if (Constants.TASK_ASSIGN_SYSTEM_OLD.equals(resultBO.getResult())) {
                    taskToAssign.setErrorDecription(LabelUtil.getKey("task.assign.system.old", local));
                } else {
                    if (Constants.NOT_FOUND_TASK.equals(resultBO.getResult())) {
                        taskToAssign.setErrorDecription(LabelUtil.getKey("not.found.task", local));
                    } else {
                        if (Constants.ERROR_PROCESSING.equals(resultBO.getResult())) {
                            taskToAssign.setErrorDecription(LabelUtil.getKey("error.processing", local));
                        } else {
                            if (Constants.NOT_FOUND_SUB_FOR_TASK.equals(resultBO.getResult())) {
                                taskToAssign.setErrorDecription(LabelUtil.getKey("not.found.sub.for.task", local));
                            } else {
                                if (Constants.NOT_FOUND_TEAM_TO_ASSIGN_TASK.equals(resultBO.getResult())) {
                                    taskToAssign.setErrorDecription(LabelUtil.getKey("not.found.team.to.assign.task", local));
                                } else {
                                    if (Constants.NOT_FOUND_SHOP_ASSIGNED_TASK.equals(resultBO.getResult())) {
                                        taskToAssign.setErrorDecription(LabelUtil.getKey("not.found.shop.assigned.to.task", local));
                                    } else {
                                        if (Constants.NOT_FOUND_INFOR_SHOP_ASSIGNED.equals(resultBO.getResult())) {
                                            taskToAssign.setErrorDecription(LabelUtil.getKey("not.found.infor.shop.assigned", local));
                                        } else {
                                            if (Constants.NOT_FOUND_STAFF_ASSIGNED_TASK.equals(resultBO.getResult())) {
                                                taskToAssign.setErrorDecription(LabelUtil.getKey("not.found.staff.assigned.task", local));
                                            } else {
                                                if (Constants.NOT_FOUND_CUST_FOR_TASK.equals(resultBO.getResult())) {
                                                    taskToAssign.setErrorDecription(LabelUtil.getKey("not.found.cust.for.task", local));
                                                } else {
                                                    if (Constants.NOT_FOUND_STAFF_TO_ASSIGN.equals(resultBO.getResult())) {
                                                        taskToAssign.setErrorDecription(LabelUtil.getKey("not.found.staff.to.assign", local));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception ex) {
            logger.error("searchTask: " + ex.getMessage());
            taskToAssign.setErrorCode("-99");
            taskToAssign.setErrorDecription("System is busy :" + ex.getMessage());
        } finally {
            closeSessions(sessionPos, sessionIm, sessionPm, sessionCc, sessionNims);
            Long endTime = System.currentTimeMillis();
            logger.error("-------->    getTaskToAssign time =  " + (endTime - startTime));
        }
        return taskToAssign;
    }

    private void copyObject(TaskDetailToAssignOut taskDetailToAssignOut, AssignTaskBO assignTaskBO) {
        TaskInfoDetail taskInfo = new TaskInfoDetail();
        taskInfo.setCompContent(assignTaskBO.getCompContent());
        taskInfo.setContractNo(assignTaskBO.getContractNo());
        taskInfo.setCustName(assignTaskBO.getCustName());
        taskInfo.setCustReqId(assignTaskBO.getCustReqId());
        taskInfo.setCustTelFax(assignTaskBO.getCustTelFax());
        taskInfo.setDeployAddress(assignTaskBO.getDeployAddress());
        taskInfo.setDsLamName(assignTaskBO.getDsLamName());
        taskInfo.setEndDateValue(assignTaskBO.getEndDateValue());
        taskInfo.setLimitDate(assignTaskBO.getLimitDate());
        taskInfo.setLocalPricePlanName(assignTaskBO.getLocalPricePlanName());
        taskInfo.setPricePlanName(assignTaskBO.getPricePlanName());
        taskInfo.setPricePlanSpeed(assignTaskBO.getPricePlanSpeed());
        taskInfo.setProductName(assignTaskBO.getProductName());
        taskInfo.setProgress(assignTaskBO.getProgress().equals(Constant.FOR_STAFF) ? LabelUtil.getKey("task.progress.staff", local) : LabelUtil.getKey("task.progress.team", local));
        taskInfo.setReqType(assignTaskBO.getReqType());
        taskInfo.setStaffId(assignTaskBO.getStaffId());
        taskInfo.setSourceType(assignTaskBO.getSourceType());
        taskInfo.setStartDateValue(assignTaskBO.getStartDateValue());
        taskInfo.setSubId(assignTaskBO.getSubId());
        taskInfo.setTaskMngtId(assignTaskBO.getTaskMngtId());
        taskInfo.setTaskName(assignTaskBO.getTaskName());
        taskInfo.setTaskShopMngtId(assignTaskBO.getTaskShopMngtId());
        taskInfo.setTelServiceId(assignTaskBO.getTelServiceId());
        taskDetailToAssignOut.setTaskInfo(taskInfo);
        taskInfo.setStationCode(assignTaskBO.getStationCode());
        taskInfo.setConnector(assignTaskBO.getConnector());
        List<AssignToObject> listObject = new ArrayList<AssignToObject>();
        if (!(assignTaskBO.getLstStaff() == null || assignTaskBO.getLstStaff().isEmpty())) {
//           taskDetailToAssignOut.setListObject(assignTaskBO.getLstStaff());
            List<Staff> lstStaff = assignTaskBO.getLstStaff();
            for (Iterator iterator = lstStaff.iterator(); iterator.hasNext();) {
                Staff staff = (Staff) iterator.next();
                AssignToObject assignToObject = new AssignToObject();
                assignToObject.setId(staff.getStaffId());
                assignToObject.setName(staff.getName());
                assignToObject.setCode(staff.getStaffCode());
                assignToObject.setTel(staff.getTel());
                listObject.add(assignToObject);
            }
        } else {
            if (!(assignTaskBO.getLstShop() == null || assignTaskBO.getLstShop().isEmpty())) {
                List<Shop> lstShop = assignTaskBO.getLstShop();
                for (Iterator iterator = lstShop.iterator(); iterator.hasNext();) {
                    Shop shop = (Shop) iterator.next();
                    AssignToObject assignToObject = new AssignToObject();
                    assignToObject.setId(shop.getShopId());
                    assignToObject.setName(shop.getName());
                    assignToObject.setCode(shop.getShopCode());
                    listObject.add(assignToObject);
                }
            }

        }
        taskDetailToAssignOut.setListObject(listObject);
    }

    /**
     *
     * @param hibernateHelper
     * @param locale
     * @param taskToAssignIn
     * @return
     */
    public WSRespone assignTask(HibernateHelper hibernateHelper, String locale, TaskToAssignIn taskToAssignIn) {
        WSRespone wsRespone = new WSRespone();
        Session cmSession = null;
        Session cmPreSession = null;
        Session imSession = null;
        Session ccSession = null;
        try {

            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            ccSession = hibernateHelper.getSession(Constants.SESSION_CC);
            ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
            ParameterOut paramOut = new ParameterOut();
            if (taskToAssignIn.getEndDate() != null && !taskToAssignIn.getEndDate().isEmpty()) {
                try {
                    Date endDateTime = DateTimeUtils.convertStringToTime(taskToAssignIn.getEndDate(), "dd/MM/yyyy HH:mm");
                    if (endDateTime == null) {
                        wsRespone.setErrorCode("-98");
                        wsRespone.setErrorDecription("EndDate value is invalid");
                        return wsRespone;
                    }
                } catch (Exception ex) {
                    wsRespone.setErrorCode("-98");
                    wsRespone.setErrorDecription("EndDate value is invalid");
                    return wsRespone;
                }
            }
            if (taskToAssignIn.getStaDate() != null && !taskToAssignIn.getStaDate().isEmpty()) {
                try {
                    Date endDateTime = DateTimeUtils.convertStringToTime(taskToAssignIn.getStaDate(), "dd/MM/yyyy HH:mm");
                    if (endDateTime == null) {
                        wsRespone.setErrorCode("-98");
                        wsRespone.setErrorDecription("getStaDate value is wrong format");
                        return wsRespone;
                    }
                } catch (Exception ex) {
                    wsRespone.setErrorCode("-98");
                    wsRespone.setErrorDecription("getStaDate value is wrong format");
                    return wsRespone;
                }
            }
            ResultBO resultBO = managerTaskDAO.assignTask(cmSession, imSession, ccSession, taskToAssignIn.getIsReAssign(),
                    taskToAssignIn.getAssignTo(), taskToAssignIn.getTaskShopMngtId(),
                    taskToAssignIn.getUserNameLogin(), taskToAssignIn.getStaffIdLogin(),
                    taskToAssignIn.getShopCodeLogin(), taskToAssignIn.getStaffAssignId(),
                    taskToAssignIn.getStaDate(), taskToAssignIn.getEndDate(), taskToAssignIn.getTelToAssign(),
                    taskToAssignIn.getTaskMngtId(), taskToAssignIn.getShopAssignId(), paramOut);
            if (!resultBO.getResult().equals("0") && !resultBO.getResult().equals("1")) {
                wsRespone.setErrorCode(resultBO.getResult());
                String des = "";
                int result = Integer.parseInt(resultBO.getResult());
                Object[] lstParam = new Object[1];
                lstParam[0] = resultBO.getDes();
                switch (result) {
                    case -1:
                        wsRespone.setErrorDecription(LabelUtil.getKey("assign.fail.not.belong.team", local));
                        break;
                    case -2:
                        wsRespone.setErrorDecription(LabelUtil.getKey("assign.fail.not.found.task", local));
                        break;
                    case -3:
                        wsRespone.setErrorDecription(LabelUtil.getKey("assign.fail.task.is.closed", local));
                        break;
                    case -4:
                        wsRespone.setErrorDecription(LabelUtil.getKey("assign.fail.reassign.for.old.staff", local));
                        break;
                    case -5:
                        wsRespone.setErrorDecription(LabelUtil.getKey("reassign.fail", lstParam, local));
                        break;
                    case -11:
                        wsRespone.setErrorDecription(LabelUtil.getKey("assign.task.fail.not.found.infor.team", local));
                        break;
                    case -12:
                        wsRespone.setErrorDecription(LabelUtil.getKey("reassign.fail", lstParam, local));
                        break;
                    case -13:
                        wsRespone.setErrorDecription(LabelUtil.getKey("assign.task.fail.reassign.old.team", local));
                        break;
                    case -14:
                        wsRespone.setErrorDecription(LabelUtil.getKey("reassign.task.fail", lstParam, local));
                        break;
                    case -15:
                        wsRespone.setErrorDecription(LabelUtil.getKey("reassign.task.fail.because.not.found.old.team", local));
                        break;
                    case -16:
                        wsRespone.setErrorDecription(LabelUtil.getKey("reassign.task.fail.because.update.infor.fail", lstParam, local));
                        break;
                    default:
                        wsRespone.setErrorDecription(LabelUtil.getKey("assign.task.fail.processing.error", local));
                        break;
                }
                rollBackTransactions(cmSession);
                rollBackTransactions(ccSession);
                rollBackTransactions(imSession);
            } else {
                /*send notification*/

                if (Constant.FOR_STAFF.toString().equals(taskToAssignIn.getAssignTo())) {
                    com.viettel.bccs.cm.model.Staff staff = new StaffDAO().findById(cmSession, taskToAssignIn.getStaffAssignId());
                    List<String> listStr = Arrays.asList(paramOut.getAccount().toString(), staff.getName(), taskToAssignIn.getEndDate());
                    List<String> listContentSms = new IsdnSendSmsDAO().genarateSMSContent(cmPreSession, null, Constants.AP_PARAM_PARAM_TYPE_NOTIFICATION, "ASSIGN_TASK", listStr);
                    try {
                        String tempNotification = "";
                        for (String temp : listContentSms) {
                            tempNotification += temp + Constants.NEW_LINE;
                        }
                        if (!tempNotification.isEmpty()) {
                            StringBuilder stringBui = new StringBuilder(tempNotification);
                            stringBui.replace(tempNotification.lastIndexOf(Constants.NEW_LINE), tempNotification.lastIndexOf(Constants.NEW_LINE) + Constants.NEW_LINE.length(), "");
                            tempNotification = stringBui.toString();
                            new NotificationMessageDAO().generateNotificationData(
                                    cmSession,
                                    staff.getStaffId(),
                                    staff.getStaffCode(),
                                    taskToAssignIn.getUserNameLogin(),
                                    LabelUtil.getKey("Notification.AssignTask.title", locale),
                                    tempNotification,/*message*/
                                    paramOut.getAccount().toString(),/*account*/
                                    Long.valueOf(taskToAssignIn.getTaskMngtId()),
                                    Long.valueOf(paramOut.getTaskStaffMngtId().toString()),/*taskStaffMngtId*/
                                    Constants.NOTIFICATION_ACTION_TYPE_UPDATE_TASK/*actionType*/);
                        }

                    } catch (Exception ex) {
                        logger.error("searchTask: " + ex.getMessage());
                    }
                } else {
                    List<com.viettel.bccs.cm.model.Staff> listStaff = new StaffDAO().findByShop(cmSession, taskToAssignIn.getShopAssignId());
                    com.viettel.bccs.cm.model.Shop shop = new ShopDAO().findById(cmSession, taskToAssignIn.getShopAssignId());
                    if (listStaff != null && !listStaff.isEmpty()) {
                        List<String> listStr = Arrays.asList(paramOut.getAccount().toString(), shop.getShopCode(), taskToAssignIn.getEndDate());
                        List<String> listContentSms = new IsdnSendSmsDAO().genarateSMSContent(cmPreSession, null, Constants.AP_PARAM_PARAM_TYPE_NOTIFICATION, "ASSIGN_TASK", listStr);

                        for (com.viettel.bccs.cm.model.Staff staff : listStaff) {
                            try {
                                String tempNotification = "";
                                for (String temp : listContentSms) {
                                    tempNotification += temp + Constants.NEW_LINE;
                                }
                                if (!tempNotification.isEmpty()) {
                                    StringBuilder stringBui = new StringBuilder(tempNotification);
                                    stringBui.replace(tempNotification.lastIndexOf(Constants.NEW_LINE), tempNotification.lastIndexOf(Constants.NEW_LINE) + Constants.NEW_LINE.length(), "");
                                    tempNotification = stringBui.toString();
                                    new NotificationMessageDAO().generateNotificationData(
                                            cmSession,
                                            staff.getStaffId(),
                                            staff.getStaffCode(),
                                            taskToAssignIn.getUserNameLogin(),
                                            LabelUtil.getKey("Notification.AssignTask.title", locale),
                                            tempNotification,/*message*/
                                            paramOut.getAccount().toString(),/*account*/
                                            Long.valueOf(taskToAssignIn.getTaskMngtId()),
                                            Long.valueOf(paramOut.getTaskStaffMngtId().toString()),/*taskStaffMngtId*/
                                            Constants.NOTIFICATION_ACTION_TYPE_ASSIGN_TASK_FOR_TEAM/*actionType*/);
                                }

                            } catch (Exception ex) {
                            }
                        }
                    }
                }

                wsRespone.setErrorCode(Constants.RESPONSE_SUCCESS);

                wsRespone.setErrorDecription(LabelUtil.getKey("common.success", local));
                commitTransactions(cmSession);

                commitTransactions(ccSession);

                commitTransactions(imSession);

                commitTransactions(cmPreSession);
            }
        } catch (Exception ex) {
            wsRespone.setErrorCode("-99");
            wsRespone.setErrorDecription("err process: " + ex.getMessage());
        } finally {
            closeSessions(cmSession);
            closeSessions(ccSession);
            closeSessions(imSession);
            closeSessions(cmPreSession);
        }
        return wsRespone;
    }
    //duyetdk

    public static double distanceBetween2Points(double la1, double lo1,
            double la2, double lo2) {
        double dLat = (la2 - la1) * (Math.PI / 180);
        double dLon = (lo2 - lo1) * (Math.PI / 180);
        double la1ToRad = la1 * (Math.PI / 180);
        double la2ToRad = la2 * (Math.PI / 180);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(la1ToRad)
                * Math.cos(la2ToRad) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = (Constants.EARTH_RADIUS) * c;
        return d;
    }

    public DashboardBroadbanTab getDashboardBroadbandInfo(HibernateHelper hibernateHelper, Long staffId, Long shopId, String isManagement, String locale) {
        DashboardBroadbanTab result = new DashboardBroadbanTab();
        List<BroadbandTabInfo> listTab = new ArrayList<BroadbandTabInfo>();
        result.setErrorCode("1");
        result.setListTab(listTab);
        Session sessionPos = null;
        try {
            sessionPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            if (staffId == null) {
                result.setErrorDecription(LabelUtil.getKey("staff.does.not.exist", locale));
            } else if (shopId == null) {
                result.setErrorDecription(LabelUtil.getKey("shop.does.not.exist", locale));
            } else if (isManagement == null || isManagement.isEmpty() || "?".equals(isManagement)) {
                result.setErrorDecription("isManagement is not null");
            } else {
                if ("1".equals(isManagement)) {
                    /*for management*/
                    String sql = "select sum(case "
                            + "             when tsm.progress = 0 and tm.job_code = 'COMPLAIN' then "
                            + "              1 "
                            + "             else "
                            + "              0 "
                            + "           end) as not_Assign_Complain, "
                            + "       sum(case "
                            + "             when tsm.progress = 0 and tm.job_code != 'COMPLAIN' then "
                            + "              1 "
                            + "             else "
                            + "              0 "
                            + "           end) as not_Assign_New, "
                            + "       sum(case "
                            + "             when tsf.progress = 0 and "
                            + "                  tm.job_code = 'COMPLAIN' then "
                            + "              1 "
                            + "             else "
                            + "              0 "
                            + "           end) as Assign_Complain, "
                            + "       sum(case "
                            + "             when tsf.progress = 0 and "
                            + "                  tm.job_code != 'COMPLAIN' then "
                            + "              1 "
                            + "             else "
                            + "              0 "
                            + "           end) as Assign_New "
                            + "  from task_management tm "
                            + "  join task_shop_management tsm "
                            + "    on tm.task_mngt_id = tsm.task_mngt_id "
                            + "  left join task_staff_management tsf "
                            + "    on tsf.task_shop_mngt_id = tsm.task_shop_mngt_id "
                            + "    and tsf.status = 1"
                            + "  join shop s "
                            + "    on s.shop_id = tsm.shop_id "
                            + " where tsm.create_date >= trunc(sysdate - 30) "
                            + " and instr(s.shop_path || '_', ?) > 0 /*shop_id*/ "
                            + " and tm.status = 1";
                    Query query = sessionPos.createSQLQuery(sql);
                    query.setParameter(0, "_" + shopId.toString() + "_");
                    List list = query.list();
                    for (Object obj : list) {
                        Object[] arr = (Object[]) obj;
                        listTab.add(new BroadbandTabInfo(
                                LabelUtil.getKey("total", locale),
                                (arr[3] == null ? 0l : Long.parseLong(arr[3].toString())) + (arr[1] == null ? 0l : Long.parseLong(arr[1].toString())),
                                (arr[2] == null ? 0l : Long.parseLong(arr[2].toString())) + (arr[0] == null ? 0l : Long.parseLong(arr[0].toString()))));
                        listTab.add(new BroadbandTabInfo(
                                LabelUtil.getKey("notAssign", locale),
                                arr[1] == null ? 0l : Long.parseLong(arr[1].toString()),
                                arr[0] == null ? 0l : Long.parseLong(arr[0].toString())));
                        listTab.add(new BroadbandTabInfo(
                                LabelUtil.getKey("assign", locale),
                                arr[3] == null ? 0l : Long.parseLong(arr[3].toString()),
                                arr[2] == null ? 0l : Long.parseLong(arr[2].toString())));
                    }
                } else {
                    /*for staff*/
                    String start = "Start";
                    String onGoing = "Processing";
                    String finish = "Finish";
                    String requestConfig = "RequestConfig";

                    String sql = "select *  "
                            + "  from (SELECT DECODE(tst.progress,  "
                            + "                      '0',  "
                            + "                      '" + start + "',  "
                            + "                      '1',  "
                            + "                      '" + onGoing + "',  "
                            + "                      '3',  "
                            + "                      '" + finish + "',  "
                            + "                      '10',  "
                            + "                      '" + requestConfig + "') AS NAME,  "
                            + "               case  "
                            + "                 when t.job_code = 'COMPLAIN' then  "
                            + "                  'COMPLAIN'  "
                            + "                 else  "
                            + "                  'TKHDM'  "
                            + "               end job  "
                            + "          FROM cm_pos2.task_management       t,  "
                            + "               cm_pos2.task_shop_management  tsh,  "
                            + "               cm_pos2.task_staff_management tst,  "
                            + "               shop                          s  "
                            + "         WHERE t.task_mngt_id = tsh.task_mngt_id  "
                            + "           AND tst.task_shop_mngt_id = tsh.task_shop_mngt_id  "
                            + "           AND s.shop_id = tsh.shop_id  "
                            + "           and instr(s.shop_path || '_', ? ) > 0 /*shop_id*/  "
                            + "           AND tst.status = 1  "
                            + "           AND tsh.status = 1  "
                            + "           AND t.status = 1  "
                            //+ "           AND tst.staff_id = ? "
                            + "           and t.create_date >= trunc(sysdate - 30)) pivot(count(*) for JOB in('COMPLAIN' COMPLAIN, 'TKHDM' TKHDM))";
                    Query query = sessionPos.createSQLQuery(sql);
                    query.setParameter(0, "_" + shopId.toString() + "_");
                    //query.setParameter(1, staffId);
                    List list = query.list();

                    listTab.add(new BroadbandTabInfo(start, 0l, 0l));
                    listTab.add(new BroadbandTabInfo(onGoing, 0l, 0l));
                    listTab.add(new BroadbandTabInfo(requestConfig, 0l, 0l));
                    listTab.add(new BroadbandTabInfo(finish, 0l, 0l));

                    for (Object obj : list) {
                        Object[] arr = (Object[]) obj;
                        for (BroadbandTabInfo tab : listTab) {
                            if (arr[0] != null && arr[0].toString().equals(tab.getTitle())) {
                                tab.setNewTask(arr[2] == null ? 0l : Long.parseLong(arr[2].toString()));
                                tab.setComplainTask(arr[1] == null ? 0l : Long.parseLong(arr[1].toString()));
                                tab.setTotal(tab.getComplainTask() + tab.getNewTask());
                                break;
                            }
                        }
                    }
                    for (BroadbandTabInfo tab : listTab) {
                        if ("Start".equals(tab.getTitle())) {
                            tab.setTitle(LabelUtil.getKey("start", locale));
                        }
                        if ("Processing".equals(tab.getTitle())) {
                            tab.setTitle(LabelUtil.getKey("processing", locale));
                        }
                        if ("Finish".equals(tab.getTitle())) {
                            tab.setTitle(LabelUtil.getKey("finish", locale));
                        }
                        if ("RequestConfig".equals(tab.getTitle())) {
                            tab.setTitle(LabelUtil.getKey("RequestConfig", locale));
                        }
                    }
                }
                result.setErrorCode("0");
            }
        } catch (Exception ex) {
            result.setErrorDecription("err process: " + ex.getMessage());
        } finally {
            if (sessionPos != null) {
                closeSessions(sessionPos);
            }
        }
        return result;
    }
}
