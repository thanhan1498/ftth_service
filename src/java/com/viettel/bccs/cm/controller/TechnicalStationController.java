package com.viettel.bccs.cm.controller;

import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.api.ws.bccsgw.model.PortOdf;
import com.viettel.bccs.cm.bussiness.TechnicalStationBussiness;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.dao.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.dao.SubDeploymentDAO;
import com.viettel.bccs.cm.dao.TechnicalConnectorDAO;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubDeployment;
import com.viettel.bccs.cm.model.TechnicalStation;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.model.output.ResultTeamCode;
import com.viettel.brcd.ws.model.output.StaffManageConnectorInfo;
import com.viettel.brcd.ws.model.output.StationInfo;
import com.viettel.brcd.ws.supplier.nims.getInfoInfras.NimsWsGetOdfCouplerList;
import com.viettel.brcd.ws.supplier.nims.getInfoInfras.OdfCouplerListResponse;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class TechnicalStationController extends BaseController {

    public TechnicalStationController() {
        logger = Logger.getLogger(TechnicalStationController.class);
    }

    public String getTeamCode(Session cmPosSession, Long stationId) {
        TechnicalStation technicalStation = new TechnicalStationBussiness().findByStation(cmPosSession, stationId);
        String result = "Not Found";
        String teamCode = technicalStation.getTeamCode();
        if (teamCode == null) {
            return result;
        }
        teamCode = teamCode.trim();
        if (teamCode.isEmpty()) {
            return result;
        }
        result = teamCode;
        return result;
    }

    public ResultTeamCode getTeamCode(HibernateHelper hibernateHelper, Long stationId, String locale) {
        ResultTeamCode result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            String teamCode = getTeamCode(cmPosSession, stationId);
            result = new ResultTeamCode(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), teamCode);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ResultTeamCode(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "BusTypeController.getTeamCode:result=" + LogUtils.toJson(result));
        }
    }

    public List<TechnicalStation> getStationOfTeam(HibernateHelper hibernateHelper, String account, Long subId, Long staffId) {
        List<TechnicalStation> result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);

            Staff staff = new StaffDAO().findById(cmPosSession, staffId);
            if (staff != null) {
                Shop shop = new ShopDAO().findById(cmPosSession, staff.getShopId());
                /*if (Constants.SHOP_TYPE_AGENT_DELEGATE.toString().equals(shop.getShopType())) {
                 return new TechnicalStationBussiness().findStationOfTeam(cmPosSession, shop.getShopId(), null);
                 } else */ if ("VTC".equals(shop.getProvinceCode())) {
                    return new TechnicalStationBussiness().findStationOfTeam(cmPosSession, null, null);
                } else {
                    return new TechnicalStationBussiness().findStationOfTeam(cmPosSession, null, shop.getProvince());
                }
            }
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);

        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "BusTypeController.getTeamCode:result=" + LogUtils.toJson(result));
        }
        return result;
    }

    public List<PortOdf> getStationOfTeam(HibernateHelper hibernateHelper, Long portOdf) {
        List<PortOdf> result = new ArrayList<PortOdf>();
        Session nims = null;
        try {
            nims = hibernateHelper.getSession(Constants.SESSION_NIMS);
            OdfCouplerListResponse response = new NimsWsGetOdfCouplerList().getOdfCouplerList(portOdf);
            if (response != null && response.getReturn() != null && !response.getReturn().isEmpty()) {
                for (String coupler : response.getReturn()) {
                    PortOdf odf = new PortOdf();
                    odf.setCouplerNo(coupler);
                    odf.setOdfId(0l);
                    result.add(odf);
                }
            }
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);

        } finally {
            closeSessions(nims);
            LogUtils.info(logger, "BusTypeController.getTeamCode:result=" + LogUtils.toJson(result));
        }
        return result;
    }

    public StationInfo getStationInfoOfAccount(HibernateHelper hibernateHelper, Long subId) {
        Session nims = null;
        Session cmPos = null;
        StationInfo info = new StationInfo();
        try {
            nims = hibernateHelper.getSession(Constants.SESSION_NIMS);
            cmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            SubAdslLeaseline sub = null;
            if (subId != null && subId > 0) {
                sub = new SubAdslLeaselineDAO().findById(cmPos, subId);
                if (sub != null) {
                    info.setAccount(sub.getAccount());
                    info.setSubId(subId);
                    List<SubDeployment> subDept = new SubDeploymentDAO().findBySubId(cmPos, subId);
                    if (subDept != null && !subDept.isEmpty()) {
                        info.setStationCode(Common.getStationCode(nims, subDept.get(0).getStationId()));
                        info.setConnector(Common.getInraDeviceCode(nims, subDept.get(0).getSplitterId(), true));
                        info.setPort(Common.getPortNoFromDeviceAndPortId(nims, subDept.get(0).getSplitterId(), subDept.get(0).getPortNo()));
                    }
                }
            }
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);

        } finally {
            closeSessions(nims, cmPos);
        }
        return info;
    }

    public List<StaffManageConnectorInfo> getStaffMapconnector(HibernateHelper hibernateHelper) {
        Session cmPos = null;
        try {
            cmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            return new TechnicalConnectorDAO().getStaffMapConnector(cmPos);
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);

        } finally {
            closeSessions(cmPos);
        }
        return new ArrayList<StaffManageConnectorInfo>();
    }
}
