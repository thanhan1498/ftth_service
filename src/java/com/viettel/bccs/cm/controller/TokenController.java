package com.viettel.bccs.cm.controller;

import com.viettel.bccs.cm.bussiness.TokenBussiness;
import com.viettel.bccs.cm.supplier.TokenSupplier;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.merchant.dao.MerchantDAO;
import com.viettel.bccs.merchant.dao.MerchantPermissionDAO;
import com.viettel.bccs.merchant.model.Merchant;
import com.viettel.bccs.merchant.model.MerchantPermission;
import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.brcd.ws.model.output.DataTopUpPinCode;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class TokenController extends BaseController {

    public TokenController() {
        logger = Logger.getLogger(TokenController.class);
    }

    public WSRespone validateToken(HibernateHelper hibernateHelper, String token, String locale) {
        WSRespone result = null;
        Session cmPosSession = null;
        boolean hasErr = false;
        try {
            //<editor-fold defaultstate="collapsed" desc="locale">
            String mess = validateLocale(locale);
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    result = new WSRespone(Constants.ERROR_CODE_1, mess);
                    return result;
                }
            }
            //</editor-fold>
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            //<editor-fold defaultstate="collapsed" desc="token">
            if (Constants.TOKEN_WEB.equals(token)) {
            } else {
                mess = new TokenBussiness().validateToken(cmPosSession, token, locale);
                if (mess != null) {
                    mess = mess.trim();
                    if (!mess.isEmpty()) {
                        result = new WSRespone(Constants.ERROR_CODE_3, mess);
                        return result;
                    }
                }
                //</editor-fold>
                commitTransactions(cmPosSession);
            }
            result = new WSRespone(Constants.ERROR_CODE_0);
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            LogUtils.info(logger, "TokenController.validateToken:result=" + LogUtils.toJson(result));
        }
    }

    public WSRespone checkPermisssion(HibernateHelper hibernateHelper, String token, String encrypt, String signature, String serviceCode, StringBuilder text) {
        WSRespone result = null;
        Session cmPosSession = null;
        Session merchantSession = null;
        boolean hasErr = false;
        String locale = "en_US";
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            merchantSession = hibernateHelper.getSession(Constants.SESSION_MERCHANT);

            /*check espired token*/
            String mess = new TokenBussiness().validateTokenNoExpired(cmPosSession, token, locale);
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    result = new WSRespone(Constants.ERROR_CODE_3, mess);
                    return result;
                }
            }

            /*check permission*/
            String[] arrToken = CommonWebservice.decryptData(cmPosSession, token).split("/");
            String merchantAccount = arrToken[0];

            if (merchantAccount == null) {
                new TokenSupplier().disableToken(cmPosSession, token);
                result = new WSRespone(Constants.ERROR_CODE_3, LabelUtil.getKey("common.token.invalid", locale));
                result.setMerchantAccount(merchantAccount);
                return result;
            }

            List<Merchant> merchant = new MerchantDAO().getMerchantByAccount(merchantSession, merchantAccount);
            if (merchant == null || merchant.isEmpty()) {
                new TokenSupplier().disableToken(cmPosSession, token);
                result = new WSRespone(Constants.ERROR_CODE_3, LabelUtil.getKey("common.token.invalid", locale));
                result.setMerchantAccount(merchantAccount);
                return result;
            }

            List<MerchantPermission> listService = new MerchantPermissionDAO().getRoleOfAccount(merchantSession, merchantAccount, serviceCode);
            if (listService == null || listService.isEmpty()) {
                new TokenSupplier().disableToken(cmPosSession, token);
                result = new WSRespone(Constants.ERROR_CODE_3, LabelUtil.getKey("common.token.invalid", locale));
                result.setMerchantAccount(merchantAccount);
                return result;
            }

            /*check sign*/
            if (encrypt != null) {
                String clearText = CommonWebservice.Decrypt(encrypt, Constants.METFONE_PRIVATE_KEY);
                boolean verifySign = CommonWebservice.Verify(clearText, signature, merchant.get(0).getPublicKey());
                if (!verifySign) {
                    result = new WSRespone(Constants.ERROR_CODE_1, "Can not verify signature");
                    result.setMerchantAccount(merchantAccount);
                    return result;
                }
                DataTopUpPinCode data = (DataTopUpPinCode) CommonWebservice.unmarshal(clearText.toString(), DataTopUpPinCode.class);
                if (data == null || data.getMerchantAccount() == null || !data.getMerchantAccount().equals(merchantAccount)) {
                    new TokenSupplier().disableToken(cmPosSession, token);
                    result = new WSRespone(Constants.ERROR_CODE_3, LabelUtil.getKey("common.token.invalid", locale));

                    return result;
                }
                text.append(clearText);
            } else {
                text.append(merchantAccount);
            }
            result = new WSRespone(Constants.ERROR_CODE_0);
            result.setMerchantAccount(merchantAccount);
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            } else {
                commitTransactions(cmPosSession);
            }
            closeSessions(cmPosSession, merchantSession);
            LogUtils.info(logger, "TokenController.checkPermisssion:result=" + LogUtils.toJson(result) + " - " + new Date());
        }
    }

    public WSRespone checkPermisssion(HibernateHelper hibernateHelper, String encrypt, String signature, String serviceCode, StringBuilder text, String staffCode) {
        WSRespone result = null;
        Session cmPosSession = null;
        Session merchantSession = null;
        boolean hasErr = false;
        String locale = "en_US";
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            merchantSession = hibernateHelper.getSession(Constants.SESSION_MERCHANT);

            /*check permission*/
            String merchantAccount = staffCode;

            if (merchantAccount == null) {
                result = new WSRespone(Constants.ERROR_CODE_3, LabelUtil.getKey("login.vsa.account.access.denie", locale));
                result.setMerchantAccount(merchantAccount);
                return result;
            }

            List<Merchant> merchant = new MerchantDAO().getMerchantByAccount(merchantSession, merchantAccount);
            if (merchant == null || merchant.isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_3, LabelUtil.getKey("login.vsa.account.access.denie", locale));
                result.setMerchantAccount(merchantAccount);
                return result;
            }

            List<MerchantPermission> listService = new MerchantPermissionDAO().getRoleOfAccount(merchantSession, merchantAccount, serviceCode);
            if (listService == null || listService.isEmpty()) {
                result = new WSRespone(Constants.ERROR_CODE_3, "User can not access this service");
                result.setMerchantAccount(merchantAccount);
                return result;
            }

            /*check sign*/
            if (encrypt != null) {
                String clearText = CommonWebservice.Decrypt(encrypt, Constants.METFONE_PRIVATE_KEY);
                boolean verifySign = CommonWebservice.Verify(clearText, signature, merchant.get(0).getPublicKey());
                if (!verifySign) {
                    result = new WSRespone(Constants.ERROR_CODE_1, "Can not verify signature");
                    result.setMerchantAccount(merchantAccount);
                    return result;
                }
                DataTopUpPinCode data = (DataTopUpPinCode) CommonWebservice.unmarshal(clearText.toString(), DataTopUpPinCode.class);
                text.append(clearText);
            }
            result = new WSRespone(Constants.ERROR_CODE_0);
            result.setMerchantAccount(merchantAccount);
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            } else {
                commitTransactions(cmPosSession);
            }
            closeSessions(cmPosSession, merchantSession);
            LogUtils.info(logger, "TokenController.checkPermisssion:result=" + LogUtils.toJson(result) + " - " + new Date());
        }
    }
}
