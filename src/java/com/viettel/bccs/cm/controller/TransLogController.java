package com.viettel.bccs.cm.controller;

import com.viettel.bccs.api.Task.BO.TransLogsClient;
import com.viettel.bccs.api.Task.DAO.TransLogClientDAO;
import com.viettel.bccs.cm.bussiness.TransLogBussiness;
import com.viettel.bccs.cm.model.TransLog;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class TransLogController extends BaseController {

    public TransLogController() {
        logger = Logger.getLogger(TransLogController.class);
    }

    public TransLog insert(HibernateHelper hibernateHelper, String token, String className, String method, String params, Long excute, String response) {
        TransLog result = null;
        Session cmPosSession = null;
        boolean hasErr = false;
        try {
            //LogUtils.info(logger, "TransLogController.insert:token=" + token + ";className=" + className + ";method=" + method + ";params=" + params + ";excute=" + excute + ";response=" + response);
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            result = new TransLogBussiness().insert(cmPosSession, token, className, method, params, excute, response);
            commitTransactions(cmPosSession);
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            //LogUtils.info(logger, "TransLogController.insert:result=" + LogUtils.toJson(result));
        }
    }

    public WSRespone insertLogClient(HibernateHelper hibernateHelper, List<TransLogsClient> lstLog) {
        WSRespone result = null;
        Session cmPosSession = null;
        boolean hasErr = false;
        ////System.out.println(LogUtils.toJson(lstLog));
        try {
            //LogUtils.info(logger, "TransLogController.insertLogClient:lstLog=" + lstLog.size());
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            result = new WSRespone();
            if (!lstLog.isEmpty()) {
                int countSuccess = 0;
                for (TransLogsClient transLogsClient : lstLog) {
                    int rs = new TransLogClientDAO().insertCusImage(cmPosSession, transLogsClient);
                    if (rs == 1) {
                        countSuccess++;
                    }
                }
                result.setErrorCode(Constants.RESPONSE_SUCCESS);
                result.setErrorDecription(Constants.SUCCESS + ":" + (countSuccess / lstLog.size()));

            } else {
                result.setErrorCode(Constants.RESPONSE_FAIL);
                result.setErrorDecription("List log is null");
            }
            commitTransactions(cmPosSession);
            return result;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
            return result;
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(cmPosSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(cmPosSession);
            //LogUtils.info(logger, "TransLogController.insertLogClient:result=" + LogUtils.toJson(result));
        }
    }
}
