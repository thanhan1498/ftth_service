/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller.digital;

import com.viettel.bccs.api.Task.BO.ResultBO;
import com.viettel.bccs.api.Task.DAO.ManagerTaskDAO;
import com.viettel.bccs.cm.bussiness.SubFbConfigBusiness;
import com.viettel.bccs.cm.controller.TaskForUpdateController;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.dao.SubDigitalDAO;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.SubDigital;
import com.viettel.bccs.cm.model.SubFbConfig;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.InfrastructureUpdateIn;
import com.viettel.brcd.ws.model.input.UpdateTaskIn;
import com.viettel.brcd.ws.model.output.ResultUpdateTaskOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import java.util.Date;
import java.util.Map;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class TaskForUpdateControllerCamera extends TaskForUpdateControllerDigital {

    private static final String PARAM_TYPE_STAFF_CAMERA = "STAFF_CONFIG_CAMERA";

    @Override
    public ResultUpdateTaskOut updateTask(Session cmPreSession, Session cmSession, Session ccSession,
            Session imSession, Session pmSession, Session paymentSession, Session biSession, Session nims, String locale, UpdateTaskIn updateTaskIn,
            com.viettel.bccs.api.Task.BO.TaskManagement task) throws Exception {
        ResultUpdateTaskOut resultUpdateTaskOut = new ResultUpdateTaskOut();

        resultUpdateTaskOut.setErrorCode(Constants.RESPONSE_SUCCESS);
        resultUpdateTaskOut.setErrorDecription(LabelUtil.getKey("common.success", locale));
        SubFbConfig subFbConfig = new SubFbConfigBusiness().findByTaskMngtId(cmSession, task.getTaskMngtId(), Constants.TYPE_CONF_DIGITAL);
        Long status = null;
        Long statusConfig = null;
        if (subFbConfig != null && subFbConfig.getTaskManagementId().equals(task.getTaskMngtId())) {
            status = subFbConfig.getStatus();
            statusConfig = subFbConfig.getStatusConf();
        }
        Long subId = updateTaskIn.getSubId().isEmpty() ? null : Long.valueOf(updateTaskIn.getSubId());
        Long taskStaffMngtId = updateTaskIn.getTaskStaffMngtId().isEmpty() ? null : Long.valueOf(updateTaskIn.getTaskStaffMngtId());
        Long telServiceId = updateTaskIn.getTelServiceId().isEmpty() ? null : Long.valueOf(updateTaskIn.getTelServiceId());
        Long reasonId = updateTaskIn.getReasonId().isEmpty() ? null : Long.valueOf(updateTaskIn.getReasonId());
        Long compCauseId = updateTaskIn.getCompCauseId().isEmpty() ? null : Long.valueOf(updateTaskIn.getCompCauseId());
        Long satisfileLevel = updateTaskIn.getSatisfileLevel().isEmpty() ? null : Long.valueOf(updateTaskIn.getSatisfileLevel());

        ResultBO resultBO = new ManagerTaskDAO().updateProgress(cmPreSession, cmSession, ccSession, imSession, pmSession, paymentSession, biSession,
                subId, taskStaffMngtId, telServiceId, updateTaskIn.getTaskStaffProgress(), null,
                updateTaskIn.getNote(), updateTaskIn.getLoginName(), updateTaskIn.getShopCodeLogin(),
                reasonId, updateTaskIn.getReasonGroup(), updateTaskIn.getComplaintResult(), satisfileLevel, compCauseId,
                updateTaskIn.getIsDeductTime(), updateTaskIn.getStartTimeDeduct(), updateTaskIn.getEndTimeDeduct(),
                updateTaskIn.getReasonDeduct(), status, statusConfig, nims);

        if (Constants.RESPONSE_SUCCESS.equals(resultBO.getResult())) {
            if (Constants.REQUEST_CONFIG.equals(updateTaskIn.getTaskStaffProgress())) {
                String mess = null;
                if (subFbConfig.getCustImgId() == null || subFbConfig.getCustImgId() < 0L) {
                    mess = LabelUtil.getKey("validate.required.lstImageGood", locale);
                }

                if (Constants.PENDING.equals(subFbConfig.getStatus()) || Constants.REJECT.equals(subFbConfig.getStatus())) {
                    new SubFbConfigBusiness().updateStatus(cmSession, subFbConfig, Constants.WAITING, null, updateTaskIn.getLoginName(), null, null, new Date());
                } else {
                    mess = LabelUtil.getKey("task.not.permit.request.config", locale);
                }
                if (mess != null) {
                    mess = mess.trim();
                    if (!mess.isEmpty()) {
                        resultUpdateTaskOut = new ResultUpdateTaskOut(Constants.ERROR_CODE_1, mess);
                    }
                } else {
                    processStepRequestConfig(cmSession, cmPreSession, subFbConfig.getAccount(), updateTaskIn.getLoginName(), taskStaffMngtId, PARAM_TYPE_STAFF_CAMERA, locale);
                }
            }
        } else {
            String des = processResultError(resultBO, locale);
            resultUpdateTaskOut.setErrorCode(resultBO.getResult());
            resultUpdateTaskOut.setErrorDecription(des);
        }

        return resultUpdateTaskOut;
    }

    @Override
    public WSRespone updateInfrastructure(Session cmSession, Session imSession, Session pmSession, Session nimsSession, String locale, InfrastructureUpdateIn infrastructureUpdateIn) throws Exception {
        if ("4".equals(infrastructureUpdateIn.getTab())) {
            return updateCustomerInfo(cmSession, imSession, pmSession, nimsSession, locale, infrastructureUpdateIn);
        } else {
            return updateInfraInfo(cmSession, imSession, pmSession, nimsSession, locale, infrastructureUpdateIn);
        }
    }

    @Override
    public WSRespone updateInfraInfo(Session cmSession, Session imSession, Session pmSession, Session nimsSession, String locale, InfrastructureUpdateIn infrastructureUpdateIn) throws Exception {
        return new WSRespone(Constants.RESPONSE_SUCCESS, Constants.SUCCESS);
    }

    @Override
    public WSRespone updateCustomerInfo(Session cmSession, Session imSession, Session pmSession, Session nimsSession, String locale, InfrastructureUpdateIn infrastructureUpdateIn) throws Exception {
        WSRespone wsRespone = new WSRespone();
        wsRespone.setErrorCode(Constants.RESPONSE_SUCCESS);
        wsRespone.setErrorDecription(Constants.SUCCESS);
        ManagerTaskDAO managerTaskDAO = new ManagerTaskDAO();
        Long taskMngtId = infrastructureUpdateIn.getTaskMngtId().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getTaskMngtId());
        Long taskStaffMngtId = infrastructureUpdateIn.getTaskStaffMngtId().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getTaskStaffMngtId());
        Long subId = infrastructureUpdateIn.getSubId().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getSubId());
        Long telServiceId = infrastructureUpdateIn.getTelServiceId().isEmpty() ? null : Long.parseLong(infrastructureUpdateIn.getTelServiceId());
        Shop shop = new ShopDAO().findByCode(cmSession, infrastructureUpdateIn.getShopCodeLogin());
        Long shopId = shop.getShopId();
        TaskForUpdateController taskForUpdateController = new TaskForUpdateController();
        Map mapGoods = taskForUpdateController.convertListToMap(infrastructureUpdateIn.getListGood());
        Map mapNewSerial = taskForUpdateController.convertListToMap(infrastructureUpdateIn.getListNewSerial());
        Map mapItem = taskForUpdateController.convertListToMap(infrastructureUpdateIn.getListItem());

        ResultBO resultBO = managerTaskDAO.updateInfrastructure(cmSession, imSession, pmSession, nimsSession, taskMngtId, taskStaffMngtId, subId, telServiceId,
                null, null, null, null, null, null, null, null, infrastructureUpdateIn.getLoginName(),
                infrastructureUpdateIn.getShopCodeLogin(), mapGoods, mapNewSerial, null, mapItem, null, infrastructureUpdateIn.getIsUpdateDeployment(), null,
                infrastructureUpdateIn.getIsGpon(), infrastructureUpdateIn.getGponSerial(), infrastructureUpdateIn.getStbSerial(), infrastructureUpdateIn.getPortLogic(),
                infrastructureUpdateIn.getSpitterId(), infrastructureUpdateIn.getIsSplitter(), shopId, infrastructureUpdateIn.getCouplerNo());

        if (Constants.RESPONSE_SUCCESS.equals(resultBO.getResult())) {
            SubDigital sub = new SubDigitalDAO().findById(cmSession, subId);
            SubFbConfig subFbConf = new SubFbConfigBusiness().findByTaskMngtId(cmSession, taskMngtId, null);
            SubFbConfigBusiness subFbConfigBuss = new SubFbConfigBusiness();
            String mess = null;
            if (subFbConf != null) {//neu la pending hoac reject thi cho phep cap nhat lai 
                if ((subFbConf.getStatus().equals(Constants.PENDING)
                        || subFbConf.getStatus().equals(Constants.REJECT))) {
                    mess = subFbConfigBuss.updateRequestSubFbConfig(cmSession, taskMngtId, subId, sub.getAccount(),
                            infrastructureUpdateIn.getImgInfras(), infrastructureUpdateIn.getImgGoods(),
                            infrastructureUpdateIn.getLoginName(), locale, new StringBuilder(), subFbConf, Constants.TYPE_CONF_DIGITAL);
                }

            } else {//neu ko phai cac truong hop tren thi them moi 
                mess = subFbConfigBuss.sendRequestSubFbConfig(cmSession, taskMngtId, subId, sub.getAccount(),
                        infrastructureUpdateIn.getImgInfras(), infrastructureUpdateIn.getImgGoods(),
                        infrastructureUpdateIn.getLoginName(), locale, infrastructureUpdateIn.getTab(), shop.getProvince(), new StringBuilder(), subFbConf);
                subFbConf = new SubFbConfigBusiness().findByTaskMngtId(cmSession, taskMngtId, null);
                subFbConf.setTypeConfig(Constants.TYPE_CONF_DIGITAL);
                cmSession.flush();

            }
            if (mess != null) {
                mess = mess.trim();
                if (!mess.isEmpty()) {
                    wsRespone.setErrorCode(Constants.ERROR_CODE_1);
                    wsRespone.setErrorDecription(mess);
                }
            }
        } else {
            String des = processResultError(resultBO, locale);
            wsRespone.setErrorCode(resultBO.getResult());
            wsRespone.setErrorDecription(des);
        }
        return wsRespone;
    }
}
