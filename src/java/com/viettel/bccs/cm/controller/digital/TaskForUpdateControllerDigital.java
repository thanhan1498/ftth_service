/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller.digital;

import com.viettel.bccs.api.Task.BO.ResultBO;
import com.viettel.bccs.api.Task.DAO.ApParamDAO;
import com.viettel.bccs.cm.dao.NotificationMessageDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.dao.pre.IsdnSendSmsDAO;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.InfrastructureUpdateIn;
import com.viettel.brcd.ws.model.input.UpdateTaskIn;
import com.viettel.brcd.ws.model.output.ResultUpdateTaskOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public abstract class TaskForUpdateControllerDigital {

    public ResultUpdateTaskOut updateTask(Session cmPreSession, Session cmSession, Session ccSession,
            Session imSession, Session pmSession, Session paymentSession, Session biSession, Session nims, String locale,
            UpdateTaskIn updateTaskIn, com.viettel.bccs.api.Task.BO.TaskManagement task) throws Exception {
        return null;
    }

    public WSRespone updateInfrastructure(Session cmSession, Session imSession, Session pmSession, Session nimsSession, String locale, InfrastructureUpdateIn infrastructureUpdateIn) throws Exception{
        if ("4".equals(infrastructureUpdateIn.getTab())) {
            return updateCustomerInfo(cmSession, imSession, pmSession, nimsSession, locale, infrastructureUpdateIn);
        } else {
            return updateInfraInfo(cmSession, imSession, pmSession, nimsSession, locale, infrastructureUpdateIn);
        }
    }

    public WSRespone updateInfraInfo(Session cmSession, Session imSession, Session pmSession, Session nimsSession, String locale, InfrastructureUpdateIn infrastructureUpdateIn) throws Exception{
        return null;
    }

    public WSRespone updateCustomerInfo(Session cmSession, Session imSession, Session pmSession, Session nimsSession, String locale, InfrastructureUpdateIn infrastructureUpdateIn) throws Exception{
        return null;
    }

    public void processStepRequestConfig(Session cmSession, Session cmPreSession, String account, String userCreate,
            Long taskStaffMngtId, String paramConfig, String locale) {
        List<com.viettel.bccs.api.Task.BO.ApParam> ap = ApParamDAO.findParamsByType(cmSession, paramConfig);
        if (ap != null && !ap.isEmpty()) {
            for (com.viettel.bccs.api.Task.BO.ApParam temp : ap) {
                Staff staffImt = new StaffDAO().findByCode(cmSession, temp.getParamValue());
                if (staffImt != null) {
                    /*send message*/
                    if (staffImt.getTel() != null || staffImt.getIsdn() != null) {
                        Staff staff = new StaffDAO().findByCode(cmSession, staffImt.getStaffCode());
                        List<String> list = new ArrayList<String>();
                        list.add(account);
                        list.add(staffImt.getStaffCode());
                        list.add(staff.getTel() == null ? "NA" : staff.getTel());
                        com.viettel.bccs.cm.bussiness.SendMessge.send(cmPreSession, staffImt.getTel() == null ? staffImt.getIsdn() : staff.getTel(), list, new Date(), userCreate, null, Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.NEW_REQUEST_CONFIG_IMT);
                    }

                    /*push notification*/
                    try {
                        List<String> listStr = Arrays.asList(account);
                        List<String> listContentSms = new IsdnSendSmsDAO().genarateSMSContent(cmPreSession, null, Constants.AP_PARAM_PARAM_TYPE_NOTIFICATION, "CONFIG_IMT", listStr);
                        String tempNotification = "";
                        for (String tempMessage : listContentSms) {
                            tempNotification += tempMessage + Constants.NEW_LINE;
                        }
                        if (!tempNotification.isEmpty()) {
                            StringBuilder stringBui = new StringBuilder(tempNotification);
                            stringBui.replace(tempNotification.lastIndexOf(Constants.NEW_LINE), tempNotification.lastIndexOf(Constants.NEW_LINE) + Constants.NEW_LINE.length(), "");
                            tempNotification = stringBui.toString();
                            new NotificationMessageDAO().generateNotificationData(
                                    cmSession,
                                    staffImt.getStaffId(),
                                    staffImt.getStaffCode(),
                                    userCreate,
                                    LabelUtil.getKey("Notification.ConfigImt.title", locale),
                                    tempNotification,/*message*/
                                    account,/*account*/
                                    taskStaffMngtId,
                                    null,/*taskStaffMngtId*/
                                    Constants.NOTIFICATION_ACTION_TYPE_REQUEST_CONFIG/*actionType*/);
                        }

                    } catch (Exception ex) {
                    }
                }
            }
        }
    }

    public String processResultError(ResultBO resultBO, String locale) {
        String des = "";
        int result = Integer.parseInt(resultBO.getResult());
        switch (result) {
            case -1:
                des = LabelUtil.getKey("update.fail.not.found.assigned.staff", locale);
                break;
            case -2:
                des = LabelUtil.getKey("update.fail.not.permit.to.change.status.start", locale);
                break;
            case -3:
                des = LabelUtil.getKey("update.fail.not.permit.to.change.status.finish", locale);
                break;
            case -4:
                des = LabelUtil.getKey("update.fail.request.change.staff", locale);
                break;
            case -5:
                des = LabelUtil.getKey("update.fail.request.change.team", locale);
                break;
            case -6:
                des = LabelUtil.getKey("update.fail.task.progress.not.updated", locale);
                break;
            case -7:
                des = LabelUtil.getKey("update.fail.task.finish.not.permit", locale);
                break;
            case -8:
                des = LabelUtil.getKey("update.fail.not.found.team.asigned", locale);
                break;
            case -9:
                des = LabelUtil.getKey("update.fail.not.infor.task", locale);
                break;
            case -10:
                des = LabelUtil.getKey("update.fail.adsl.not.active", locale);
                break;
            case -11:
                des = LabelUtil.getKey("update.fail.cc", locale);
                break;
            case -12:
                des = LabelUtil.getKey("update.fail.complaint.not.update", locale);
                break;
            case -13:
                des = LabelUtil.getKey("not.found.task", locale);
                break;
            case -14:
                des = LabelUtil.getKey("not.found.task", locale);
                break;
            case -15:
                des = LabelUtil.getKey("update.fail.error.nims", locale);
                break;
            case -16:
                des = LabelUtil.getKey("update.fail.finish.task.fail", locale);
                break;
            case -17:
                des = LabelUtil.getKey("update.fail.finish.task.fail", locale);
                break;
            case -20:
                des = resultBO.getDes();
                break;
            case -21:
                des = LabelUtil.getKey("error.validate.startTime.null", locale);
                break;
            case -22:
                des = LabelUtil.getKey("error.validate.endTime.null", locale);
                break;
            case -23:
                des = LabelUtil.getKey("error.validate.endtime.less.start", locale);
                break;
            case -24:
                des = LabelUtil.getKey("error.validate.startTimeDeduct.more.startTime", locale) + (resultBO.getDes() != null ? (" - " + resultBO.getDes()) : "");
                break;
            case -25:
                des = LabelUtil.getKey("error.validate.endTimeDeduct.less.endTime", locale) + (resultBO.getDes() != null ? (" - " + resultBO.getDes()) : "");
                break;
            case -26:
                des = LabelUtil.getKey("updateTask.deduct.time.validate.reason", locale);
                break;
            case -27:
                des = LabelUtil.getKey("updateTask.error.checkAAA", locale);
                break;
            case -28:
                des = LabelUtil.getKey("updateTask.error.close.vitual.task", locale);
                break;
            case -29:
                des = LabelUtil.getKey("updateTask.error.start.to.request.config", locale);
                break;
            case -30:
                des = LabelUtil.getKey("update.fail.not.permit.to.change.status.ongoing", locale);
                break;
            case 100:
                des = LabelUtil.getKey("updateTask.error.requestConfig", locale);
                break;
            case 101:
                des = LabelUtil.getKey("updateTask.error.requestConfig.check", locale);
                break;
            case 102:
                des = LabelUtil.getKey("updateTask.error.requestConfig.custImage", locale);
                break;
            case 103:
                des = LabelUtil.getKey("updateTask.error.requestConfig.infraImage", locale);
                break;
            case 104:
                des = LabelUtil.getKey("updateTask.error.requestConfig.infraImage.invalid", locale) + resultBO.getDes();
                break;
            case 105:
                des = LabelUtil.getKey("updateTask.error.requestConfig.infraImage.connector.invalid", locale) + resultBO.getDes();
                break;
            case 106:
                des = LabelUtil.getKey("updateTask.error.requestConfig.custmage.invalid", locale) + resultBO.getDes();
                break;
            case 107:
                des = LabelUtil.getKey("updateTask.error.requestConfig.custmage.connector.invalid", locale) + resultBO.getDes();
                break;
            case 108:
                des = LabelUtil.getKey("soc_waiting_result", locale);
                break;
            case 109:
                des = LabelUtil.getKey("update.fras.fail.without.cable.box", locale);
                break;
            case 110:
                des = LabelUtil.getKey("update.fras.fail.without.cable.box", locale);
                break;
            case 111:
                des = LabelUtil.getKey("update.fras.fail.not.find.fras", locale);
                break;
            case 112:
                des = LabelUtil.getKey("update.fras.fail.no.infor.update", locale);
                break;
            case 113:
                des = LabelUtil.getKey("update.fras.fail.not.find.sub", locale);
                break;
            case 114:
                des = LabelUtil.getKey("update.fras.fail.nims", locale);
                break;
            case 115:
                des = resultBO.getParam();
                break;
            default:
                des = LabelUtil.getKey("error.processing", locale) + resultBO.getDes();
                break;
        }
        return des;
    }
}
