package com.viettel.bccs.cm.controller.impl;

import com.google.gson.Gson;
import com.viettel.bccs.api.Task.BO.TransLogsClient;
import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.api.ws.bccsgw.model.PortOdf;
import com.viettel.bccs.cc.controller.ComplaintController;
import com.viettel.bccs.cm.bussiness.TokenBussiness;
import com.viettel.bccs.cm.controller.ApDomainController;
import com.viettel.bccs.cm.controller.ApParamController;
import com.viettel.bccs.cm.controller.AreaController;
import com.viettel.bccs.cm.controller.BankController;
import com.viettel.bccs.cm.controller.BaseController;
import com.viettel.bccs.cm.controller.BundleTvController;
import com.viettel.bccs.cm.controller.BusTypeController;
import com.viettel.bccs.cm.controller.CmController;
import com.viettel.bccs.cm.controller.ContractController;
import com.viettel.bccs.cm.controller.CustomerController;
import com.viettel.bccs.cm.controller.DebitController;
import com.viettel.bccs.cm.controller.DomainController;
import com.viettel.bccs.cm.controller.LocalProductPpController;
import com.viettel.bccs.cm.controller.LoginController;
import com.viettel.bccs.cm.controller.PolicyController;
import com.viettel.bccs.cm.controller.ProductPricePlanController;
import com.viettel.bccs.cm.controller.PromotionTypeController;
import com.viettel.bccs.cm.controller.QuotaController;
import com.viettel.bccs.cm.controller.ReasonController;
import com.viettel.bccs.cm.controller.RequestChangeInformationController;
import com.viettel.bccs.cm.controller.RequestController;
import com.viettel.bccs.cm.controller.RequestUpdateLocationController;
import com.viettel.bccs.cm.controller.SaBayController;
import com.viettel.bccs.cm.controller.SalaryController;
import com.viettel.bccs.cm.controller.ServiceInfoController;
import com.viettel.bccs.cm.controller.SourceTypeController;
import com.viettel.bccs.cm.controller.StaffInfoController;
import com.viettel.bccs.cm.controller.StockModelController;
import com.viettel.bccs.cm.controller.SubFbConfigController;
import com.viettel.bccs.cm.controller.SubTypeController;
import com.viettel.bccs.cm.controller.SubscriberController;
import com.viettel.bccs.cm.controller.TaskForUpdateController;
import com.viettel.bccs.cm.controller.TaskProgressController;
import com.viettel.bccs.cm.controller.TaskToAssignController;
import com.viettel.bccs.cm.controller.TechnicalStationController;
import com.viettel.bccs.cm.controller.TokenController;
import com.viettel.bccs.cm.controller.TransLogController;
import com.viettel.bccs.cm.controller.payment.PaymentController;
import com.viettel.bccs.cm.controller.prepaid.PrepaidController;
import com.viettel.bccs.cm.controller.product.ProductController;
import com.viettel.bccs.cm.dao.NotificationMessageDAO;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.Codes;
import com.viettel.bccs.cm.model.FTPBean;
import com.viettel.bccs.cm.model.FormChangeInformation;
import com.viettel.bccs.cm.model.NotificationLog;
import com.viettel.bccs.cm.model.TechnicalStation;
import com.viettel.bccs.cm.model.Token;
import com.viettel.bccs.cm.supplier.ServiceSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.merchant.controller.MerchantController;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.brcd.ws.model.input.ActivePreIn;
import com.viettel.brcd.ws.model.input.ActiveSubscriberPreIn;
import com.viettel.brcd.ws.model.input.CancelContractInput;
import com.viettel.brcd.ws.model.input.CancelRULInput;
import com.viettel.brcd.ws.model.input.CancelRequestInput;
import com.viettel.brcd.ws.model.input.CancelSubFbConfigInput;
import com.viettel.brcd.ws.model.input.ChangeSimIn;
import com.viettel.brcd.ws.model.input.ChangeSimInput;
import com.viettel.brcd.ws.model.input.ComplaintInput;
import com.viettel.brcd.ws.model.input.ContractIn;
import com.viettel.brcd.ws.model.input.Coordinate;
import com.viettel.brcd.ws.model.input.CustomerIn;
import com.viettel.brcd.ws.model.input.ImageCusInput;
import com.viettel.brcd.ws.model.input.ImageInput;
import com.viettel.brcd.ws.model.input.InfrastructureUpdateIn;
import com.viettel.brcd.ws.model.input.PaymentInput;
import com.viettel.brcd.ws.model.input.RevokeTaskIn;
import com.viettel.brcd.ws.model.input.SearchSubRequestInput;
import com.viettel.brcd.ws.model.input.SearchTaskForLookUpInput;
import com.viettel.brcd.ws.model.input.SearchTaskForUpdateInput;
import com.viettel.brcd.ws.model.input.SearchTaskInput;
import com.viettel.brcd.ws.model.input.SearchTaskRevokeIn;
import com.viettel.brcd.ws.model.input.SubAdslLeaselineOut;
import com.viettel.brcd.ws.model.input.TaskToAssignIn;
import com.viettel.brcd.ws.model.input.UpdateNoteIn;
import com.viettel.brcd.ws.model.input.UpdateTaskIn;
import com.viettel.brcd.ws.model.input.WSRequest;
import com.viettel.brcd.ws.model.output.ApDomainOut;
import com.viettel.brcd.ws.model.output.ApParamOut;
import com.viettel.brcd.ws.model.output.BankOut;
import com.viettel.brcd.ws.model.output.BundleTVInfo;
import com.viettel.brcd.ws.model.output.CableBoxOut;
import com.viettel.brcd.ws.model.output.ComplainResultOut;
import com.viettel.brcd.ws.model.output.CoordinateOut;
import com.viettel.brcd.ws.model.output.CusLocationOut;
import com.viettel.brcd.ws.model.output.CustomTypeOut;
import com.viettel.brcd.ws.model.output.CustomerOut;
import com.viettel.brcd.ws.model.output.DashboardBroadbanTab;
import com.viettel.brcd.ws.model.output.DebitOut;
import com.viettel.brcd.ws.model.output.DepositOut;
import com.viettel.brcd.ws.model.output.DistrictOut;
import com.viettel.brcd.ws.model.output.DomainOut;
import com.viettel.brcd.ws.model.output.FileOut;
import com.viettel.brcd.ws.model.output.TaskToAssignOut;
import com.viettel.brcd.ws.model.output.GroupOut;
import com.viettel.brcd.ws.model.output.GroupReasonOut;
import com.viettel.brcd.ws.model.output.HistorySubFbConfigOut;
import com.viettel.brcd.ws.model.output.IdTypeOut;
import com.viettel.brcd.ws.model.output.ImageInfrastructureOut;
import com.viettel.brcd.ws.model.output.ImageRespone;
import com.viettel.brcd.ws.model.output.InforInfrastructureOut;
import com.viettel.brcd.ws.model.output.InforResolveComplainOut;
import com.viettel.brcd.ws.model.output.InforTaskToUpdateOut;
import com.viettel.brcd.ws.model.output.InvestOut;
import com.viettel.brcd.ws.model.output.InvoiceListOut;
import com.viettel.brcd.ws.model.output.LastUpdateConfigOut;
import com.viettel.brcd.ws.model.output.LimitValueOut;
import com.viettel.brcd.ws.model.output.LineTypeOut;
import com.viettel.brcd.ws.model.output.LinkFileOut;
import com.viettel.brcd.ws.model.output.LocalProductPpOut;
import com.viettel.brcd.ws.model.output.LoginOut;
import com.viettel.brcd.ws.model.output.MaxRadiusOut;
import com.viettel.brcd.ws.model.output.NotificationTaskOut;
import com.viettel.brcd.ws.model.output.PayAdvanceBillOut;
import com.viettel.brcd.ws.model.output.PayContractOut;
import com.viettel.brcd.ws.model.output.PaymentOut;
import com.viettel.brcd.ws.model.output.PortOut;
import com.viettel.brcd.ws.model.output.PreRevokeOut;
import com.viettel.brcd.ws.model.output.PrecinctOut;
import com.viettel.brcd.ws.model.output.ProductOut;
import com.viettel.brcd.ws.model.output.ProductPricePlanOut;
import com.viettel.brcd.ws.model.output.PromotionOut;
import com.viettel.brcd.ws.model.output.ProvinceOut;
import com.viettel.brcd.ws.model.output.ReasonChangeSimOut;
import com.viettel.brcd.ws.model.output.ReasonConfigOut;
import com.viettel.brcd.ws.model.output.ReasonConnectOut;
import com.viettel.brcd.ws.model.output.ReasonOut;
import com.viettel.brcd.ws.model.output.ReasonPreConnectOut;
import com.viettel.brcd.ws.model.output.RequestStatusOut;
import com.viettel.brcd.ws.model.output.RequestUpdateLocationOut;
import com.viettel.brcd.ws.model.output.ResultTeamCode;
import com.viettel.brcd.ws.model.output.ResultUpdateTaskOut;
import com.viettel.brcd.ws.model.output.SalaryOut;
import com.viettel.brcd.ws.model.output.SearchSubRequestOut;
import com.viettel.brcd.ws.model.output.SearchTaskRevokeOut;
import com.viettel.brcd.ws.model.output.SerialOut;
import com.viettel.brcd.ws.model.output.ServiceInfoOut;
import com.viettel.brcd.ws.model.output.ServiceOut;
import com.viettel.brcd.ws.model.output.ServicePaymentOut;
import com.viettel.brcd.ws.model.output.SourceTypeOut;
import com.viettel.brcd.ws.model.output.StaffDetailOut;
import com.viettel.brcd.ws.model.output.StaffInfoOut;
import com.viettel.brcd.ws.model.output.StepInfoOut;
import com.viettel.brcd.ws.model.output.CcActionInformation;
import com.viettel.brcd.ws.model.output.ChangeInformationOut;
import com.viettel.brcd.ws.model.output.ClearDebitTransOut;
import com.viettel.brcd.ws.model.output.CompDepartmentGroupOut;
import com.viettel.brcd.ws.model.output.ComplaintAcceptSourceOut;
import com.viettel.brcd.ws.model.output.ComplaintAcceptTypeOut;
import com.viettel.brcd.ws.model.output.ComplaintGroupOut;
import com.viettel.brcd.ws.model.output.ComplaintTypeOut;
import com.viettel.brcd.ws.model.output.DataTopUpPinCode;
import com.viettel.brcd.ws.model.output.DebitStaffOut;
import com.viettel.brcd.ws.model.output.DebitTransHistoryOut;
import com.viettel.brcd.ws.model.output.DebitTransOut;
import com.viettel.brcd.ws.model.output.ListReasonByCodePreOut;
import com.viettel.brcd.ws.model.output.LockDebitTransOut;
import com.viettel.brcd.ws.model.output.OTPOut;
import com.viettel.brcd.ws.model.output.OwnerOut;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.StaffManageConnectorInfo;
import com.viettel.brcd.ws.model.output.StaffSalaryDetail;
import com.viettel.brcd.ws.model.output.StaffSalaryImtResponse;
import com.viettel.brcd.ws.model.output.StationInfo;
import com.viettel.brcd.ws.model.output.SubFbConfigOut;
import com.viettel.brcd.ws.model.output.SubInfoByISDNPreOut;
import com.viettel.brcd.ws.model.output.SubTypeOut;
import com.viettel.brcd.ws.model.output.TaskDetailToAssignOut;
import com.viettel.brcd.ws.model.output.TaskForUpdateOut;
import com.viettel.brcd.ws.model.output.TaskHistoryOut;
import com.viettel.brcd.ws.model.output.TaskInfoDetailOut;
import com.viettel.brcd.ws.model.output.TaskInfrasDetailOut;
import com.viettel.brcd.ws.model.output.TaskLookForOut;
import com.viettel.brcd.ws.model.output.TaskProgressOut;
import com.viettel.brcd.ws.model.output.TaskWarningOut;
import com.viettel.brcd.ws.model.output.UpdateResultOut;
import com.viettel.brcd.ws.model.output.UploadImageOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.eafs.util.SpringUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Session;
import com.viettel.bccs.cm.controller.AccountInfrasController;
import com.viettel.bccs.cm.controller.ChangingDeploymentAddressController;
import com.viettel.bccs.cm.controller.EmoneyController;
import com.viettel.bccs.cm.dao.ActionLogPrDAO;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.ws.model.input.ChangeDeploymentAddressIn;
import com.viettel.brcd.ws.model.output.AccountByStaffOut;
import com.viettel.brcd.ws.model.output.AccountInfrasLocationOut;
import com.viettel.brcd.ws.model.output.AccountInfrasOut;
import com.viettel.brcd.ws.model.output.PotentialCustomerOut;
import com.viettel.brcd.ws.model.output.TechInforOut;
import com.viettel.brcd.ws.model.output.UnpaidPaymentDetailExpand;
import static com.viettel.brcd.ws.supplier.accountinfras.NimWsGetInfoBusines.getStationAccountInfras;
import java.util.Arrays;

/**
 * @author vietnn6 a part
 */
public class CmControllerImpl extends BaseController implements CmController {

    private transient HibernateHelper hibernateHelper;
    private static final String PARAM_TYPE = "FBB_POTENTIAL_CUSTOMER";

    public HibernateHelper getHibernateHelper() {
        if (this.hibernateHelper == null) {
            this.hibernateHelper = (HibernateHelper) SpringUtil.getBean("hibernateHelper");
            setHibernateHelper(this.hibernateHelper);
        }
        return hibernateHelper;
    }

    public void setHibernateHelper(HibernateHelper hibernateHelper) {
        this.hibernateHelper = hibernateHelper;
    }

    @Override
    public String getHelloWorld() {
        return "WE ARE BCCS TEAM";
    }

    @Override
    public LoginOut login(String user, String pass, String serial, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = "user=" + gson.toJson(user) + ";pass=" + gson.toJson(pass)
                + ";serial=" + gson.toJson(serial);
        LoginOut result = new LoginController().login(hibernateHelper, user, pass, serial, locale);
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", "login");
            paramTr.put("user", user);
            paramTr.put("pass", pass);
            paramTr.put("serial", serial);
            new TransLogController().insert(hibernateHelper, "login", CmControllerImpl.class.getName(), "login", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n login", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ServiceOut getServices(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ServiceOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ServiceOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ApParamController().getServices(hibernateHelper, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getServices", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getServices", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public GroupOut getProductGroups(String token, Long telecomServiceId, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";telecomServiceId=" + gson.toJson(telecomServiceId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        GroupOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new GroupOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ProductController().getProductGroups(hibernateHelper, telecomServiceId, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("telecomServiceId", telecomServiceId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getProductGroups", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getProductGroups", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ProductOut getProducts(String token, Long telecomServiceId, String groupProduct, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";telecomServiceId=" + gson.toJson(telecomServiceId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ProductOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ProductOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ProductController().getProducts(hibernateHelper, telecomServiceId, groupProduct, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("telecomServiceId", telecomServiceId);
            paramTr.put("groupProduct", groupProduct);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getProducts", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getProducts", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ProvinceOut getProvinces(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ProvinceOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ProvinceOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new AreaController().getProvinces(hibernateHelper, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getProvinces", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getProvinces", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public DistrictOut getDistricts(String token, String province, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";province=" + gson.toJson(province);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        DistrictOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new DistrictOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new AreaController().getDistricts(hibernateHelper, province, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("province", province);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getDistricts", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getDistricts", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public PrecinctOut getPrecincts(String token, String province, String district, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";province=" + gson.toJson(province) + ";district=" + gson.toJson(district);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        PrecinctOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new PrecinctOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new AreaController().getPrecincts(hibernateHelper, province, district, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("province", province);
            paramTr.put("district", district);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getPrecincts", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getPrecincts", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public PaymentOut getPaymethods(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        PaymentOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new PaymentOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ApDomainController().getPaymethods(hibernateHelper, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getPaymethods", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getPaymethods", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ReasonConnectOut getRegTypes(String token, Long channelTypeId, String province, Long numPort, String productCode, String serviceType, String locale, String infraType, String vasProduct) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";channelTypeId=" + gson.toJson(channelTypeId) + ";province=" + gson.toJson(province)
                + "numPort=" + gson.toJson(numPort) + ";productCode=" + gson.toJson(productCode) + ";serviceType=" + gson.toJson(serviceType);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ReasonConnectOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ReasonConnectOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ReasonController().getRegTypes(hibernateHelper, channelTypeId, province, numPort, productCode, serviceType, locale, infraType, vasProduct);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("channelTypeId", channelTypeId);
            paramTr.put("province", province);
            paramTr.put("numPort", numPort);
            paramTr.put("productCode", productCode);
            paramTr.put("serviceType", serviceType);
            paramTr.put("vasProduct", vasProduct);
            paramTr.put("locale", locale);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getRegTypes", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getRegTypes", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public LimitValueOut getQuotas(String token, String serviceType, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";serviceType=" + gson.toJson(serviceType);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        LimitValueOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new LimitValueOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new QuotaController().getQuotas(hibernateHelper, serviceType, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("serviceType", serviceType);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getQuotas", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getQuotas", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public DepositOut getDeposits(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        DepositOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new DepositOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ApDomainController().getDeposits(hibernateHelper, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getDeposits", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getDeposits", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public PromotionOut getPromotions(String token, String telService, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";telService=" + gson.toJson(telService);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        PromotionOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new PromotionOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new PromotionTypeController().getPromotions(hibernateHelper, telService, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("telService", telService);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getPromotions", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getPromotions", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public CustomerOut searchCustomer(String token, String idNo, String isdn, String account, Long pageSize, Long pageNo, String locale, String customerName, String contact) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";idNo=" + gson.toJson(idNo) + ";isdn=" + gson.toJson(isdn)
                + "account=" + gson.toJson(account) + ";pageSize=" + gson.toJson(pageSize) + ";pageNo=" + gson.toJson(pageNo);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CustomerOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CustomerOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CustomerController().searchCustomer(hibernateHelper, idNo, isdn, account, pageSize, pageNo, locale, customerName, contact);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("isdn", isdn);
            paramTr.put("idNo", idNo);
            paramTr.put("account", account);
            paramTr.put("pageSize", pageSize);
            paramTr.put("pageNo", pageNo);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "searchCustomer", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n searchCustomer", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public CustomTypeOut getBusTypes(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CustomTypeOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CustomTypeOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new BusTypeController().getBusTypes(hibernateHelper, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getBusTypes", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getBusTypes", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public IdTypeOut getIdTypes(String token, String busType, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";busType=" + gson.toJson(busType);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        IdTypeOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new IdTypeOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ApDomainController().getIdTypes(hibernateHelper, busType, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("busType", busType);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getIdTypes", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getIdTypes", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public LineTypeOut getLineTypes(String token, String serviceType, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";serviceType=" + gson.toJson(serviceType);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        LineTypeOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new LineTypeOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ApDomainController().getLineTypes(hibernateHelper, serviceType, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("serviceType", serviceType);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getLineTypes", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getLineTypes", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public SubTypeOut getSubTypes(String token, String productCode, String serviceType, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";productCode=" + gson.toJson(productCode) + ";serviceType=" + gson.toJson(serviceType);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        SubTypeOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new SubTypeOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new SubTypeController().getSubTypes(hibernateHelper, productCode, serviceType, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("productCode", productCode);
            paramTr.put("serviceType", serviceType);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getSubTypes", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getSubTypes", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public DebitOut getCustomerDebit(String token, Long custId, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";custId=" + gson.toJson(custId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        DebitOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new DebitOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new PaymentController().getCustomerDebit(hibernateHelper, custId, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("custId", custId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getCustomerDebit", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getCustomerDebit", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ResultTeamCode getTeamCode(String token, Long stationId, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";stationId=" + gson.toJson(stationId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ResultTeamCode result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ResultTeamCode(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TechnicalStationController().getTeamCode(hibernateHelper, stationId, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("stationId", stationId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getTeamCode", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getTeamCode", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ReasonOut getSignContractReasons(String token, String telService, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";telService=" + gson.toJson(telService);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ReasonOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ReasonOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ReasonController().getSignContractReasons(hibernateHelper, telService, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("telService", telService);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getSignContractReasons", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getSignContractReasons", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public DomainOut getNickDomains(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        DomainOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new DomainOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new DomainController().getNickDomains(hibernateHelper, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getNickDomains", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getNickDomains", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public BankOut searchBank(String token, String bankCode, String name, Long pageSize, Long pageNo, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";bankCode=" + gson.toJson(bankCode) + ";name=" + gson.toJson(name)
                + "pageSize=" + gson.toJson(pageSize) + ";pageNo=" + gson.toJson(pageNo);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        BankOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new BankOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new BankController().searchBank(hibernateHelper, bankCode, name, pageSize, pageNo, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("bankCode", bankCode);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "searchBank", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n searchBank", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ApDomainOut getNoticeCharges(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ApDomainOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ApDomainOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ApDomainController().getNoticeCharges(hibernateHelper, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getNoticeCharges", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getNoticeCharges", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ApDomainOut getPrintMethods(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ApDomainOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ApDomainOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ApDomainController().getPrintMethods(hibernateHelper, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getPrintMethods", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getPrintMethods", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ProductPricePlanOut getListProductPP(String token, Long offerId, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";offerId=" + gson.toJson(offerId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ProductPricePlanOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ProductPricePlanOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ProductPricePlanController().getListProductPP(hibernateHelper, offerId, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("offerId", offerId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListProductPP", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListProductPP", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public LocalProductPpOut getListLocalProductPP(String token, Long offerId, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";offerId=" + gson.toJson(offerId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        LocalProductPpOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new LocalProductPpOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new LocalProductPpController().getListLocalProductPP(hibernateHelper, offerId, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("offerId", offerId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListLocalProductPP", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListLocalProductPP", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ApParamOut getMaxLengthSurvey(String token, String seviceCode, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";seviceCode=" + gson.toJson(seviceCode);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ApParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ApParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ApParamController().getMaxLengthSurvey(hibernateHelper, seviceCode, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("seviceCode", seviceCode);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getMaxLengthSurvey", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getMaxLengthSurvey", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public RequestStatusOut getSearchRequestStatus(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        RequestStatusOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new RequestStatusOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ApParamController().getSearchRequestStatus(hibernateHelper, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getSearchRequestStatus", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getSearchRequestStatus", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ReasonOut getCancelRequestReasons(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ReasonOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ReasonOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ReasonController().getCancelRequestReasons(hibernateHelper, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getCancelRequestReasons", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getCancelRequestReasons", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public SearchSubRequestOut searchSubRequest(String token, SearchSubRequestInput searchSubRequestInput, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";searchSubRequestInput=" + gson.toJson(searchSubRequestInput);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        SearchSubRequestOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new SearchSubRequestOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RequestController().searchSubRequest(hibernateHelper, searchSubRequestInput, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "searchSubRequest", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n searchSubRequest", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public StepInfoOut getNextStepInfo(String token, Long subReqId, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";subReqId=" + gson.toJson(subReqId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        StepInfoOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new StepInfoOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RequestController().getNextStepInfo(hibernateHelper, subReqId, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("subReqId", subReqId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getNextStepInfo", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getNextStepInfo", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ReasonOut getCancelContractReasons(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ReasonOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ReasonOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ReasonController().getCancelContractReasons(hibernateHelper, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getCancelContractReasons", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getCancelContractReasons", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public UpdateResultOut addCustomer(String token, CustomerIn cusIn, String locale, List<ImageInput> lstImage, String staffCode) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";cusIn=" + gson.toJson(cusIn);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        UpdateResultOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new UpdateResultOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CustomerController().addCustomer(hibernateHelper, token, cusIn, locale, lstImage, staffCode);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("CustomerIn", gson.toJson(cusIn));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "addCustomer", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n addCustomer", PATH, param, sTime, eTime, gson.toJson(result));
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("idNo", cusIn.getIdNo());
            paramTr.put("custId", cusIn.getCustId());
            paramTr.put("busPermitNo", cusIn.getBusPermitNo());
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "addCustomer", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public WSRespone signContract(String token, WSRequest wsRequest, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";wsRequest=" + gson.toJson(wsRequest);
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result == null || Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            result = new ContractController().signContract(hibernateHelper, wsRequest, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("wsRequest", gson.toJson(wsRequest));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "signContract", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n signContract", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone activeSubscriber(String token, WSRequest wsRequest, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";wsRequest=" + gson.toJson(wsRequest);
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result == null || Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            result = new SubscriberController().activeSubscriber(hibernateHelper, wsRequest, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("wsRequest", gson.toJson(wsRequest));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "activeSubscriber", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n activeSubscriber", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone cancelRequest(String token, CancelRequestInput cancelRequestInput, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";cancelRequestInput=" + gson.toJson(cancelRequestInput);
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result == null || Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            result = new RequestController().cancelRequest(hibernateHelper, cancelRequestInput, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("CancelRequestInput", gson.toJson(cancelRequestInput));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "cancelRequest", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n cancelRequest", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone cancelContract(CancelContractInput cancelContractInput, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = "cancelContractInput=" + gson.toJson(cancelContractInput);
        WSRespone result = new TokenController().validateToken(hibernateHelper, cancelContractInput.getToken(), locale);
        if (result == null || Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            result = new ContractController().cancelContract(hibernateHelper, cancelContractInput, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("cancelContractInput", cancelContractInput);
            new TransLogController().insert(hibernateHelper, cancelContractInput.getToken(), CmControllerImpl.class.getName(), "cancelContract", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n cancelContract", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public UpdateResultOut addRequestContract(String token, ContractIn input, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";ContractIn=" + gson.toJson(input);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        UpdateResultOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new UpdateResultOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RequestController().addRequestContract(hibernateHelper, input, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("input", gson.toJson(input));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "addRequestContract", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n addRequestContract", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public UpdateResultOut setCustomHouseCoord(String token, Long custId, String xLocation, String yLocation, String locale, List<ImageInput> lstImageList) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";custId=" + gson.toJson(custId) + ";xLocation=" + gson.toJson(xLocation) + ";yLocation=" + gson.toJson(yLocation);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        UpdateResultOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new UpdateResultOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CustomerController().setCustomHouseCoord(hibernateHelper, custId, xLocation, yLocation, locale, token, lstImageList);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("custId", custId);
            paramTr.put("xLocation", xLocation);
            paramTr.put("yLocation", yLocation);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "setCustomHouseCoord", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n setCustomHouseCoord", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public MaxRadiusOut getMaxRadius(String token, String serviceCode, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";serviceCode=" + gson.toJson(serviceCode);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        MaxRadiusOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new MaxRadiusOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ApParamController().getMaxRadius(hibernateHelper, serviceCode, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("serviceCode", serviceCode);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getMaxRadius", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getMaxRadius", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public TaskToAssignOut getListTaskToAssign(String token, String locale, SearchTaskInput searchTaskInput) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";getListTaskToAssign=" + gson.toJson(searchTaskInput);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        TaskToAssignOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new TaskToAssignOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskToAssignController().searchTask(hibernateHelper, searchTaskInput, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("searchTaskInput", gson.toJson(searchTaskInput));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListTaskToAssign", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListTaskToAssign", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public SourceTypeOut getSourceType(String token, String locale, int type) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";type=" + gson.toJson(type);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        SourceTypeOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new SourceTypeOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new SourceTypeController().getSourceType(hibernateHelper, type, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getSourceType", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getSourceType", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public TaskProgressOut getTaskProgress(String token, String locale, int type) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";type=" + gson.toJson(type);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        TaskProgressOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new TaskProgressOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskProgressController().getTaskProgress(hibernateHelper, type, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getTaskProgress", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getTaskProgress", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public TaskDetailToAssignOut getTaskToAssign(String token, String locale, String strTaskMngtId, String endDateValue, String strAssignTo) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";strTaskMngtId=" + gson.toJson(strTaskMngtId) + ";endDateValue=" + gson.toJson(endDateValue) + ";strAssignTo=" + gson.toJson(strAssignTo);

        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        TaskDetailToAssignOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new TaskDetailToAssignOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskToAssignController().getTaskToAssign(hibernateHelper, locale, strTaskMngtId, endDateValue, strAssignTo);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("strTaskMngtId", strTaskMngtId);
            paramTr.put("endDateValue", endDateValue);
            paramTr.put("strAssignTo", strAssignTo);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getTaskToAssign", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getTaskToAssign", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public StaffInfoOut getStaffForUpdateTask(String token, String locale, Long shopId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";shopId=" + gson.toJson(shopId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        StaffInfoOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new StaffInfoOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new StaffInfoController().getStaffInfos(hibernateHelper, shopId, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("shopId", shopId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getStaffForUpdateTask", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getStaffForUpdateTask", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public ServiceInfoOut getServiceForTask(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ServiceInfoOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ServiceInfoOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ServiceInfoController().getServices(hibernateHelper, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getServiceForTask", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getServiceForTask", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public TaskForUpdateOut getListTaskForUpdate(String token, String locale, SearchTaskForUpdateInput searchTaskInput) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";getListTaskForUpdate=" + gson.toJson(searchTaskInput);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        TaskForUpdateOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new TaskForUpdateOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().searchTask(hibernateHelper, searchTaskInput, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("searchTaskInput", gson.toJson(searchTaskInput));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListTaskForUpdate", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListTaskForUpdate", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public TaskProgressOut getProgressForUpdateTask(String token, String locale, int type) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";type=" + gson.toJson(type);

        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        TaskProgressOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new TaskProgressOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskProgressController().getProgressForUpdateTask(hibernateHelper, type, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getProgressForUpdateTask", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getProgressForUpdateTask", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public TaskProgressOut getProgressForUpdateInfoTask(String token, String locale, int type) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";type=" + gson.toJson(type);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        TaskProgressOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new TaskProgressOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskProgressController().getProgressForUpdateInfoTask(hibernateHelper, type, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getProgressForUpdateInfoTask", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getProgressForUpdateInfoTask", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone assignTask(String token, String locale, TaskToAssignIn taskToAssignIn) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";assignTask=" + gson.toJson(taskToAssignIn);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            return wsRespone;
        } else {
            wsRespone = new TaskToAssignController().assignTask(hibernateHelper, locale, taskToAssignIn);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("taskToAssignIn", gson.toJson(taskToAssignIn));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "assignTask", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(wsRespone));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n assignTask", PATH, param, sTime, eTime, gson.toJson(wsRespone));
        return wsRespone;
    }

    @Override
    public InforTaskToUpdateOut getInforTaskToUpdate(String token, String locale, String userLogin, String taskMngtId, String staffTaskMngtId, String telServiceId) {
        //truongpv mark
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";userLogin=" + gson.toJson(userLogin) + ";taskMngtId=" + gson.toJson(taskMngtId)
                + "staffTaskMngtId=" + gson.toJson(staffTaskMngtId) + ";telServiceId=" + gson.toJson(telServiceId);
        InforTaskToUpdateOut result = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new InforTaskToUpdateOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().getInforTaskToUpdate(hibernateHelper, locale, userLogin, taskMngtId, staffTaskMngtId, telServiceId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("userLogin", userLogin);
            paramTr.put("taskMngtId", taskMngtId);
            paramTr.put("staffTaskMngtId", staffTaskMngtId);
            paramTr.put("telServiceId", telServiceId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getInforTaskToUpdate", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getInforTaskToUpdate", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone updateNoteTask(String token, String locale, UpdateNoteIn updateNoteIn) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";updateNoteTask=" + gson.toJson(updateNoteIn);
        WSRespone result = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new WSRespone(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().updateNoteTask(hibernateHelper, locale, updateNoteIn);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("updateNoteTask", gson.toJson(updateNoteIn));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "updateNoteTask", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n updateNoteTask", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public InforResolveComplainOut getInforForResolveComplain(String token, String locale, Long custReqId, Long taskId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";custReqId=" + gson.toJson(custReqId) + ";taskId=" + gson.toJson(taskId);
        InforResolveComplainOut result = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new InforResolveComplainOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().getInforForResolveComplain(hibernateHelper, locale, custReqId, taskId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("custReqId", custReqId);
            paramTr.put("taskId", taskId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getInforForResolveComplain", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getInforForResolveComplain", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public InforInfrastructureOut getInforForInfrastructure(String token, String locale, Long taskStaffMngtId, Long taskMngtId, Long telServiceId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";taskStaffMngtId=" + gson.toJson(taskStaffMngtId) + ";taskMngtId=" + gson.toJson(taskMngtId) + ";telServiceId=" + gson.toJson(telServiceId);

        InforInfrastructureOut result = null;
        InforInfrastructureOut cloneResult = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new InforInfrastructureOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            try {
                result = new TaskForUpdateController().getInforForInfrastructure(hibernateHelper, locale, taskStaffMngtId, taskMngtId, telServiceId, false /*not view*/);
            } catch (Exception ex) {
                LogUtils.error(logger, ex.getMessage(), ex);
            }
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            if (result != null) {
                cloneResult = result.clone();
            }
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("taskStaffMngtId", taskStaffMngtId);
            paramTr.put("taskMngtId", taskMngtId);
            paramTr.put("telServiceId", telServiceId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getInforForInfrastructure", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(cloneResult));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getInforForInfrastructure", PATH, param, sTime, eTime, gson.toJson(cloneResult));
        return result;
    }

    @Override
    public CableBoxOut getCableBox(String token, String locale, Long boardId, Long dslamId, Long stationId, Long telserviceId, Long subId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";boardId=" + gson.toJson(boardId) + ";dslamId=" + gson.toJson(dslamId)
                + "stationId=" + gson.toJson(stationId) + ";telserviceId=" + gson.toJson(telserviceId) + ";subId=" + gson.toJson(subId);
        CableBoxOut result = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CableBoxOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().getCableBox(hibernateHelper, locale, boardId, dslamId, stationId, telserviceId, subId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getCableBox", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getCableBox", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone updateInfrastructure(String token, String locale, InfrastructureUpdateIn infrastructureUpdateIn) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";infrastructureUpdateIn=" + gson.toJson(infrastructureUpdateIn);
        WSRespone result = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new WSRespone(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().updateInfrastructure(hibernateHelper, locale, infrastructureUpdateIn, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            infrastructureUpdateIn.setImgGoods(null);
            infrastructureUpdateIn.setImgInfras(null);
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("infrastructureUpdateIn", gson.toJson(infrastructureUpdateIn));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "updateInfrastructure", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n updateInfrastructure", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ResultUpdateTaskOut updateProgress(String token, String locale, UpdateTaskIn updateTaskIn) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";updateTaskIn=" + gson.toJson(updateTaskIn);

        ResultUpdateTaskOut result = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ResultUpdateTaskOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().updateTask(hibernateHelper, locale, updateTaskIn, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("updateTaskIn", gson.toJson(updateTaskIn));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "updateProgress", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n updateProgress", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public GroupReasonOut getLstReason(String token, String locale, String reasonGroup) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";reasonGroup=" + gson.toJson(reasonGroup);
        GroupReasonOut result = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new GroupReasonOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().getLstReason(hibernateHelper, locale, reasonGroup);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("reasonGroup", reasonGroup);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getLstReason", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getLstReason", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public PortOut getListPortByDevice(String token, String locale, String dslamId, String telServiceId, String subId, String stationId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";dslamId=" + gson.toJson(dslamId) + ";telServiceId=" + gson.toJson(telServiceId)
                + "subId=" + gson.toJson(subId) + ";stationId=" + gson.toJson(stationId);
        PortOut result = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new PortOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().getListPortByDevice(hibernateHelper, locale, dslamId, telServiceId, subId, stationId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListPortByDevice", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListPortByDevice", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public TaskLookForOut searchTaskForLookUp(String token, String locale, SearchTaskForLookUpInput searchTaskInput) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";searchTaskInput=" + gson.toJson(searchTaskInput);

        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        TaskLookForOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new TaskLookForOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().searchTaskForLookUp(hibernateHelper, searchTaskInput, locale);
        }

        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("searchTaskInput", gson.toJson(searchTaskInput));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "searchTaskForLookUp", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n searchTaskForLookUp", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public TaskInfoDetailOut showDetailInfoTask(String token, String locale, String taskMngtId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";taskMngtId=" + gson.toJson(taskMngtId);

        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        TaskInfoDetailOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new TaskInfoDetailOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().showDetailInfoTask(hibernateHelper, taskMngtId, locale);
        }

        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("taskMngtId", taskMngtId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "showDetailInfoTask", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n showDetailInfoTask", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public TaskInfrasDetailOut showDetailInfrasTask(String token, String locale, String taskMngtId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";taskMngtId=" + gson.toJson(taskMngtId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        TaskInfrasDetailOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new TaskInfrasDetailOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().showDetailInfrasTask(hibernateHelper, taskMngtId, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("taskMngtId", taskMngtId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "showDetailInfrasTask", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n showDetailInfrasTask", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public TaskHistoryOut searchHistoryTask(String token, String locale, String startDate, String endDate, String taskMngtId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";startDate=" + gson.toJson(startDate) + ";endDate=" + gson.toJson(endDate) + ";taskMngtId=" + gson.toJson(taskMngtId);

        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        TaskHistoryOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new TaskHistoryOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().searchHistoryTask(hibernateHelper, startDate, endDate, taskMngtId, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("taskMngtId", taskMngtId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "searchHistoryTask", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n searchHistoryTask", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public ComplainResultOut showComplaintResult(String token, String locale, String custReqId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";custReqId=" + gson.toJson(custReqId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ComplainResultOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ComplainResultOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().showComplaintResult(hibernateHelper, custReqId, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("custReqId", custReqId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "showComplaintResult", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n showComplaintResult", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public WSRespone deleteTask(String token, String locale, String strTaskMngtId, String reason, String loginName, String shopCodeLogin) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";strTaskMngtId=" + gson.toJson(strTaskMngtId) + ";reason=" + gson.toJson(reason)
                + ";loginName=" + gson.toJson(loginName) + ";shopCodeLogin=" + gson.toJson(shopCodeLogin);

        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            result = new WSRespone(result.getErrorCode(), result.getErrorDecription());
        } else {
            result = new TaskForUpdateController().deleteTask(hibernateHelper, strTaskMngtId, reason, loginName, shopCodeLogin);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("strTaskMngtId", strTaskMngtId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "deleteTask", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n deleteTask", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public SearchTaskRevokeOut searchTaskForRevoke(String token, String locale, SearchTaskRevokeIn searchTask) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";searchTask=" + gson.toJson(searchTask);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        SearchTaskRevokeOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new SearchTaskRevokeOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().searchTaskForRevoke(hibernateHelper, searchTask, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("searchTask", gson.toJson(searchTask));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "searchTaskForRevoke", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n searchTaskForRevoke", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public PreRevokeOut detailRevokeTask(String token, String locale, String strTaskStaffMngtId, String strTelServiceId, String strRevokeFor) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";strTaskStaffMngtId=" + gson.toJson(strTaskStaffMngtId) + ";strTelServiceId=" + gson.toJson(strTelServiceId) + ";strRevokeFor=" + gson.toJson(strRevokeFor);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        PreRevokeOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new PreRevokeOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().detailRevokeTask(hibernateHelper, strTaskStaffMngtId, strTelServiceId, strRevokeFor, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("strTaskStaffMngtId", strTaskStaffMngtId);
            paramTr.put("strTelServiceId", strTelServiceId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "detailRevokeTask", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n detailRevokeTask", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public WSRespone revokeTask(String token, String locale, RevokeTaskIn revokeTaskIn) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";revokeTaskIn=" + gson.toJson(revokeTaskIn);
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            result = new WSRespone(result.getErrorCode(), result.getErrorDecription());
        } else {
            result = new TaskForUpdateController().revokeTask(hibernateHelper, revokeTaskIn);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("revokeTaskIn", gson.toJson(revokeTaskIn));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "revokeTask", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n revokeTask", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public WSRespone createRequestNoInfra(String token, Long custId, String serviceType, Number Lat, Number Lng, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";custId=" + gson.toJson(custId) + ";serviceType=" + gson.toJson(serviceType)
                + "Lat=" + gson.toJson(Lat) + ";Lng=" + gson.toJson(Lng);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            return wsRespone;
        } else {
            wsRespone = new RequestController().createRequestNoInfra(hibernateHelper, custId, serviceType, Lat, Lng, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "createRequestNoInfra", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(wsRespone));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n createRequestNoInfra", PATH, param, sTime, eTime, gson.toJson(wsRespone));
        return wsRespone;
    }

    public UploadImageOut uploadImage(String token, String locale, String userLogin, Long subId, String account, List<ImageInput> lstImage) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";subId=" + subId + ";account=" + gson.toJson(account) + ";imageInSize=" + lstImage.size();
        UploadImageOut result = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new UploadImageOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskProgressController().uploadImage(hibernateHelper, locale, userLogin, subId, account, lstImage);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "uploadImage", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n uploadImage", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public NotificationTaskOut getNotificationTaskOut(String token, String locale, String serial) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";serial=" + gson.toJson(serial);
        NotificationTaskOut result = null;
        result = new TaskProgressController().getNotificationTaskOut(hibernateHelper, locale, serial);
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getNotificationTaskOut", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getNotificationTaskOut", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public CoordinateOut getListCoordinate(String token, Long reqId, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";reqId=" + gson.toJson(reqId);

        CoordinateOut result = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CoordinateOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RequestController().getListCoordinate(hibernateHelper, reqId, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("reqId", reqId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListCoordinate", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListCoordinate", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public CoordinateOut getListCoordinateBySubId(String token, Long subId, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";subId=" + gson.toJson(subId);
        CoordinateOut result = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CoordinateOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RequestController().getListCoordinateBySubId(hibernateHelper, subId, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("subId", subId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListCoordinateBySubId", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListCoordinateBySubId", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public WSRespone saveLocationAgain(String token, Long subId, List<Coordinate> listCoordinate, Double cableLenght, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";subId=" + gson.toJson(subId) + ";listCoordinate=" + gson.toJson(listCoordinate) + ";cableLenght=" + gson.toJson(cableLenght);

        WSRespone respone = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            respone = new WSRespone(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            respone = new RequestController().saveLocationAgain(hibernateHelper, locale, subId, listCoordinate, cableLenght);
        }

        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "saveLocationAgain", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(respone));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n saveLocationAgain", PATH, param, sTime, eTime, gson.toJson(respone));
        return respone;
    }

    public TaskWarningOut getListWarningTaskForStaff(String token, String locale, String shopId, String pStaffId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";shopId=" + gson.toJson(shopId) + ";pStaffId=" + gson.toJson(pStaffId);
        TaskWarningOut result = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new TaskWarningOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskProgressController().getListWarningTaskForStaff(hibernateHelper, locale, shopId, pStaffId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListWarningTaskForStaff", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListWarningTaskForStaff", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public InvestOut getInvestBySubID(String token, Long subId, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + token + ";subId=" + subId;
        InvestOut investOut = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            investOut = new InvestOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            investOut = new RequestController().getInvestBySubID(hibernateHelper, locale, subId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("subId", subId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getInvestBySubID", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(investOut));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getInvestBySubID", PATH, param, sTime, eTime, gson.toJson(investOut));
        return investOut;
    }

    public InvestOut getInvest(String token, Long reqId, Double cableLenght, Long capacity, Long boxType, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + token + ";reqId=" + reqId + ";cableLenght=" + cableLenght
                + "capacity=" + capacity + ";boxType=" + boxType;
        InvestOut investOut = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            investOut = new InvestOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            investOut = new RequestController().getInvest(hibernateHelper, locale, reqId, cableLenght, capacity, boxType);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getInvest", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(investOut));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getInvest", PATH, param, sTime, eTime, gson.toJson(investOut));
        return investOut;
    }

    @Override
    public CustomerOut getListCustomerPre(String token, String idNo, String isdn, String service, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + token + ";idNo=" + idNo + ";isdn=" + isdn
                + "service=" + service;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CustomerOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CustomerOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CustomerController().getListCustomerPre(hibernateHelper, idNo, isdn, service, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("idNo", idNo);
            paramTr.put("isdn", isdn);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListCustomerPre", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListCustomerPre", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public UpdateResultOut addCustomerPre(String token, CustomerIn cusIn, String locale, List<ImageInput> lstImage, String staffCode) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";cusIn=" + gson.toJson(cusIn);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        UpdateResultOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new UpdateResultOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CustomerController().addCustomerPre(hibernateHelper, token, cusIn, locale, lstImage, staffCode);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("idNo", cusIn.getIdNo());
            paramTr.put("custId", cusIn.getCustId());
            paramTr.put("busPermitNo", cusIn.getBusPermitNo());
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "addCustomer", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n addCustomerPre", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public CustomTypeOut getBusTypesPre(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CustomTypeOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CustomTypeOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new BusTypeController().getBusTypesPre(hibernateHelper, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getBusTypesPre", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getBusTypes", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ReasonPreConnectOut getRegTypesPre(String token, String user, Long channelTypeId, String province, String productCode, String serviceType, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + token + "user=" + user + ";channelTypeId=" + channelTypeId + ";province=" + province
                + ";productCode=" + productCode + ";serviceType=" + serviceType;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ReasonPreConnectOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ReasonPreConnectOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ReasonController().getRegTypesPre(hibernateHelper, user, channelTypeId, province, productCode, serviceType, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("channelTypeId", channelTypeId);
            paramTr.put("province", province);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getRegTypesPre", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getRegTypesPre", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone activeSubscriberPre(String token, ActivePreIn wsRequest, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";wsRequest=" + gson.toJson(wsRequest);
        WSRespone result = new TokenController().checkPermisssion(hibernateHelper, token, null, null, "activeSubscriberPre", null);
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            result = new TokenController().validateToken(hibernateHelper, token, locale);
        }
        if (result == null || Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            result = new SubscriberController().activeSubscriberPre(hibernateHelper, wsRequest, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("wsRequest", gson.toJson(wsRequest));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "activeSubscriberPre", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n activeSubscriberPre", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone changeSim(String token, ChangeSimIn wsRequest, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";changeSim=" + gson.toJson(wsRequest);
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result == null || Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            result = new SubscriberController().changeSim(hibernateHelper, wsRequest, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("wsRequest", gson.toJson(wsRequest));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "changeSim", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n changeSim", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public SalaryOut getInfoSalary(String token, String staffId, String type, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + token + ";staffId=" + staffId + ";type=" + type + " \n ";
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        SalaryOut salaryOut = null;
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            salaryOut = new SalaryOut(result.getErrorCode(), result.getErrorDecription());
        } else {
            salaryOut = new SalaryController().getNickDomains(hibernateHelper, staffId, type, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getInfoSalary", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n  getInfoSalary \n ", PATH, param, sTime, eTime, gson.toJson(salaryOut));
        return salaryOut;
    }

    @Override
    public FileOut getFileByLink(String token, String link, String locale) {
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        FileOut policyOut = null;
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            policyOut = new FileOut(result.getErrorCode(), result.getErrorDecription());
        } else {
            try {
                FTPBean ftpBean = new FTPBean();
                policyOut = new PolicyController().getFileByLink(hibernateHelper, ftpBean.getUsername(), ftpBean.getPassword(), ftpBean.getHost(), link, locale);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return policyOut;
    }

    @Override
    public ReasonChangeSimOut getListReasonChangeSim(String token, String isdn, String type, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + token + ";isdn=" + isdn + ";type=" + type;
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        ReasonChangeSimOut changeSimOut = null;
        if (result == null || Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            changeSimOut = new SubscriberController().getListReasonChangeSim(hibernateHelper, isdn, type, locale);
        } else {
            changeSimOut = new ReasonChangeSimOut(result.getErrorCode(), result.getErrorDecription());
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListReasonChangeSim", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListReasonChangeSim", PATH, param, sTime, eTime, gson.toJson(changeSimOut));
        return changeSimOut;
    }

    @Override
    public PayContractOut getCustomerToPayment(String token, String account, String idNo, String locale, String customerName, String contact) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + token + ";account=" + account + ";idNo=" + idNo;
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        PayContractOut contractOut = null;
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            contractOut = new PayContractOut(result.getErrorCode(), result.getErrorDecription());
        } else {
            contractOut = new ContractController().getCustomerToPayment(hibernateHelper, account, idNo, locale, customerName, contact);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getCustomerToPayment", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getCustomerToPayment", PATH, param, sTime, eTime, gson.toJson(contractOut));
        return contractOut;
    }

    @Override
    public InvoiceListOut getInvoiceList(String token, String staffCode, String shopCode, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + token + ";staffCode=" + staffCode + ";shopCode=" + shopCode;
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        InvoiceListOut invoiceOut = null;
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            invoiceOut = new InvoiceListOut(result.getErrorCode(), result.getErrorDecription());
        } else {
            invoiceOut = new ContractController().getInvoiceList(hibernateHelper, staffCode, shopCode, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getInvoiceList", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getInvoiceList", PATH, param, sTime, eTime, gson.toJson(invoiceOut));
        return invoiceOut;
    }

    @Override
    public ServicePaymentOut searchServicePayment(String token, Long staffId, Long invoiceListId, Long contractId, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + token + ";staffId=" + staffId + ";invoiceListId=" + invoiceListId + ";contractId=" + contractId;
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        ServicePaymentOut serviceOut = null;
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            serviceOut = new ServicePaymentOut(result.getErrorCode(), result.getErrorDecription());
        } else {
            serviceOut = new PaymentController().searchServicePayment(hibernateHelper, staffId, invoiceListId, contractId, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "searchServicePayment", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n searchServicePayment", PATH, param, sTime, eTime, gson.toJson(serviceOut));
        return serviceOut;
    }

    @Override
    public PayAdvanceBillOut paymentForService(String token, PaymentInput payIn, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";payIn=" + gson.toJson(payIn);
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        PayAdvanceBillOut paymentResponse;
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            paymentResponse = new PayAdvanceBillOut(result.getErrorCode(), result.getErrorDecription());
        } else {
            paymentResponse = new PaymentController().paymentForService(hibernateHelper, payIn, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("payIn", gson.toJson(payIn));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "paymentForService", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n paymentForService", PATH, param, sTime, eTime, gson.toJson(paymentResponse));
        return paymentResponse;
    }

    @Override
    public LinkFileOut getLinkFilePolicy(String token, String fromDate, String toDate, String objectType, String serviceType, String formType, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";fromDate="
                + fromDate + ";todate=" + toDate + ";objectType=" + objectType + ";serviceType="
                + serviceType + ";formType=" + formType;
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        LinkFileOut policyOut = null;
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            policyOut = new LinkFileOut(result.getErrorCode(), result.getErrorDecription());
        } else {
            try {
                policyOut = new PolicyController().getLinkFilePolicy(hibernateHelper, fromDate, toDate, objectType, serviceType, formType, locale);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getLinkFilePolicy", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n  getFileByLink \n ", PATH, param, sTime, eTime, gson.toJson(policyOut));
        return policyOut;
    }

    @Override
    public CusLocationOut findCusByLocation(String token, String lat, String lng, String province, String district, String precinct, Long distance, String name, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";lat="
                + lat + ";lng=" + lng + ";province=" + province + ";district="
                + district + ";precinct=" + precinct + ";distance=" + distance;
        WSRespone tokenCheck = new TokenController().validateToken(hibernateHelper, token, locale);
        CusLocationOut result;
        if (tokenCheck != null && !Constants.ERROR_CODE_0.equals(tokenCheck.getErrorCode())) {
            return new CusLocationOut(tokenCheck.getErrorCode(), tokenCheck.getErrorDecription());
        } else {
            result = new CustomerController().findCusByLocation(hibernateHelper, lat, lng, province, district, precinct, distance, name, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("lat", gson.toJson(lat));
            paramTr.put("lng", gson.toJson(lng));
            paramTr.put("province", gson.toJson(province));
            paramTr.put("district", gson.toJson(district));
            paramTr.put("precinct", gson.toJson(precinct));
            paramTr.put("distance", gson.toJson(distance));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "findCusByLocation", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(token));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n findCusByLocation", PATH, param, sTime, eTime, gson.toJson(token));
        return result;
    }

    public WSRespone uploadImageCus(String token, String locale, ImageCusInput imageIn) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";imageIn=" + gson.toJson(imageIn);
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            return result;
        } else {
            result = new CustomerController().uploadImage(hibernateHelper, locale, imageIn);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "uploadImage", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n uploadImage", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone syncTransLogClient(List<TransLogsClient> lstLog) {
        Date sTime = new Date();
        String param = " \n input:lstLog=" + lstLog.size();
        WSRespone result = new TransLogController().insertLogClient(hibernateHelper, lstLog);
        Date eTime = new Date();
        logVTG.saveLog(USER_NAME, URI, " \n syncTransLogClient", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public ImageRespone viewImageContract(String account) {
        ImageRespone response = new TaskProgressController().viewImage(hibernateHelper, account);
        return response;
    }

    @Override
    public LoginOut loginEncryptRSA(String user, String pass, String locale, String deviceId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = "user=" + gson.toJson(user) + ";pass=" + gson.toJson(pass);
        LoginOut result = new LoginController().loginEncryptRSA(hibernateHelper, user.toUpperCase(), pass, locale, deviceId);
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", "login");
            paramTr.put("user", user);
            paramTr.put("pass", pass);
            new TransLogController().insert(hibernateHelper, "login", CmControllerImpl.class.getName(), "login", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n login", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public ImageRespone viewImageCustomer(String cusId) {
        ImageRespone response = new TaskProgressController().viewImageCustomer(hibernateHelper, cusId);
        return response;
    }

    public WSRespone getCodeAuthenCusByIsdn(String isdn, String serial) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:isdn=" + gson.toJson(isdn);

        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        WSRespone result = new CustomerController().getCodeAuthenCusByIsdn(hibernateHelper, isdn, serial);
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("isdn", isdn);
            new TransLogController().insert(hibernateHelper, "", CmControllerImpl.class.getName(), "getCodeAuthenCusByIsdn", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getCodeAuthenCusByIsdn", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public WSRespone checkCodeAuthenCusBySerial(String code, String serial) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:code=" + gson.toJson(code) + " \n input:serial=" + gson.toJson(serial);
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        WSRespone result = new CustomerController().checkCodeAuthenCusBySerial(hibernateHelper, code, serial);
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("code", code);
            paramTr.put("serial", serial);
            new TransLogController().insert(hibernateHelper, "", CmControllerImpl.class.getName(), "checkCodeAuthenCusBySerial", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n checkCodeAuthenCusBySerial", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public WSRespone checkIdNoCus(String idNo) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:idNo=" + gson.toJson(idNo);
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        WSRespone result = new CustomerController().checkIdNoCus(hibernateHelper, idNo);
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("idNo", idNo);
            new TransLogController().insert(hibernateHelper, "", CmControllerImpl.class.getName(), "checkIdNoCus", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n checkIdNoCus", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public WSRespone checkIdNoCusPre(String idNo) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:idNo=" + gson.toJson(idNo);
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        WSRespone result = new CustomerController().checkIdNoCusPre(hibernateHelper, idNo);
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("idNo", idNo);
            new TransLogController().insert(hibernateHelper, "", CmControllerImpl.class.getName(), "checkIdNoCus", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n checkIdNoCusPre", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    public SerialOut getRangeSerial(String token, Long shopId, Long stockModelId, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:shopId=" + gson.toJson(shopId);
        SerialOut response = new SerialOut();
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            response.setErrorCode(Constants.ERROR_CODE_3);
            response.setErrorDecription("Session time out");
            return response;
        } else {
            response = new StockModelController().getRangeSerial(hibernateHelper, shopId, stockModelId);
        }

        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();

        try {
            HashMap paramTr = new HashMap();
            paramTr.put("shopId", shopId);
            paramTr.put("stockModelId", stockModelId);
            new TransLogController().insert(hibernateHelper, "", CmControllerImpl.class.getName(), "checkIdNoCus", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getRangeSerial", PATH, param, sTime, eTime, gson.toJson(result));
        return response;
    }

    public WSRespone changePasswordOnBccs(String isdn, String oldPass, String newPass, String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:isdn=" + gson.toJson(isdn);
        WSRespone response = new WSRespone();
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            response.setErrorCode(Constants.ERROR_CODE_3);
            response.setErrorDecription("Session time out");
            return response;
        } else {
            response = new LoginController().changePasswordOnBccs(hibernateHelper, isdn, oldPass, newPass, locale);
        }

        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();

        try {
            HashMap paramTr = new HashMap();
            paramTr.put("isdn", isdn);
            new TransLogController().insert(hibernateHelper, "", CmControllerImpl.class.getName(), "changePasswordOnBccs", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n changePasswordOnBccs", PATH, param, sTime, eTime, gson.toJson(result));
        return response;
    }

    @Override
    public ReasonOut getReasonPayAdvance(String token, String locale, Long contractId, String isdn) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + token + ";locale=" + locale + ";contractId=" + contractId + "; isdn = " + isdn;
        ReasonOut reasonOut = null;
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            reasonOut = new ReasonOut(result.getErrorCode(), result.getErrorDecription());
        } else {
            reasonOut = new ReasonController().getReasonPayAdvance(hibernateHelper, locale, contractId, isdn);
        }
        //reasonOut = new ReasonController().getReasonPayAdvance(hibernateHelper, locale, contractId);
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("contractId", contractId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getReasonPayAdvance", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getReasonPayAdvance", PATH, param, sTime, eTime, gson.toJson(reasonOut));
        return reasonOut;
    }

    @Override
    public PayAdvanceBillOut payAdvance(String token, String locale, Long reasonId, Long staffId, Long contractId, Long invoiceListId, String isdn) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + token + ";locale=" + locale + ";reasonId=" + reasonId + ";staffId=" + staffId + ";contractId=" + contractId + ";invoiceListId=" + invoiceListId;
        PayAdvanceBillOut paymentOut = null;
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            paymentOut = new PayAdvanceBillOut(result.getErrorCode(), result.getErrorDecription());
        } else {
            paymentOut = new PaymentController().payAdvance(hibernateHelper, locale, reasonId, staffId, contractId, invoiceListId, isdn);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("reasonId", reasonId);
            paramTr.put("staffId", staffId);
            paramTr.put("contractId", contractId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "payAdvance", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n payAdvance", PATH, param, sTime, eTime, gson.toJson(paymentOut));
        return paymentOut;
    }

    /**
     * @author : duyetdk
     * @des: get list request update location
     * @since 20-01-2018
     */
    @Override
    public RequestUpdateLocationOut getListRequestUpdateCusLocation(String token, String date, Long status, Long duration, Long pageNo,
            String locale, String createUser, String custName) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";date=" + gson.toJson(date) + ";status=" + gson.toJson(status)
                + ";duration=" + gson.toJson(duration) + ";createUser=" + gson.toJson(createUser)
                + ";custName=" + gson.toJson(custName);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        RequestUpdateLocationOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new RequestUpdateLocationOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RequestUpdateLocationController().getListRequestUpdateCusLocation(hibernateHelper, date, status, duration, pageNo, locale, token, createUser, custName);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("date", date);
            paramTr.put("status", status);
            paramTr.put("duration", duration);
            paramTr.put("createUser", createUser);
            paramTr.put("custName", custName);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListRequestUpdateLocation", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListRequestUpdateLocation", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author : duyetdk
     * @des: reject or aprrove request update customer location
     * @since 20-01-2018
     */
    @Override
    public WSRespone rejectOrAprroveRUL(String token, CancelRULInput cancelRULInput, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";cancelRULInput=" + gson.toJson(cancelRULInput);
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result == null || Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            result = new RequestUpdateLocationController().rejectOrAprroveRUL(hibernateHelper, cancelRULInput, locale, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("cancelRULInput", gson.toJson(cancelRULInput));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "rejectOrAprroveRUL", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n rejectOrAprroveRUL", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author : duyetdk
     * @des: get list times of change
     * @since 20-01-2018
     */
    @Override
    public RequestUpdateLocationOut getListTimesOfChange(String token, Long custId, Long pageNo, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";custId=" + gson.toJson(custId)
                + ";pageNo=" + gson.toJson(pageNo);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        RequestUpdateLocationOut result;
        logger.info("request: " + param);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new RequestUpdateLocationOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RequestUpdateLocationController().getListTimesOfChange(hibernateHelper, custId, pageNo, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("custId", custId);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListTimesOfChange", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListTimesOfChange", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone approveAll(String token, String locale, String date, Long duration, String createUser, String custName) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";duration=" + gson.toJson(duration)
                + ";createUser=" + gson.toJson(createUser) + ";custName=" + gson.toJson(custName);
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result == null || Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            result = new RequestUpdateLocationController().approveAll(hibernateHelper, locale, token, date, duration, createUser, custName);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("duration", duration);
            paramTr.put("createUser", createUser);
            paramTr.put("custName", custName);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "approveAll", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n approveAll", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author : duyetdk
     * @des: getListSubFbConfig
     * @since 15-03-2018
     */
    @Override
    public SubFbConfigOut getListSubFbConfig(String token, Long status, Long statusConfig, Long duration, String account, String createUser, String locale, Long pageNo, String province) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";status=" + gson.toJson(status)
                + ";duration=" + gson.toJson(duration) + ";createUser=" + gson.toJson(createUser)
                + ";account=" + gson.toJson(account) + ";statusConfig=" + gson.toJson(statusConfig);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        SubFbConfigOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new SubFbConfigOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new SubFbConfigController().getListSubFbConfig(hibernateHelper, token, status, statusConfig, duration, account, createUser, locale, pageNo, province);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("status", status);
            paramTr.put("statusConfig", statusConfig);
            paramTr.put("duration", duration);
            paramTr.put("createUser", createUser);
            paramTr.put("account", account);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListSubFbConfig", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListSubFbConfig", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author : duyetdk
     * @des: rejectOrApproveSubFbConfig
     * @since 08-03-2018
     */
    @Override
    public WSRespone rejectOrApproveSubFbConfig(String token, CancelSubFbConfigInput cancelSubFbConfigInput, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";cancelSubFbConfigInput=" + gson.toJson(cancelSubFbConfigInput);
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result == null || Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            result = new SubFbConfigController().rejectOrApproveSubFbConfig(hibernateHelper, cancelSubFbConfigInput, locale, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("cancelSubFbConfigInput", gson.toJson(cancelSubFbConfigInput));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "rejectOrApproveSubFbConfig", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n rejectOrApproveSubFbConfig", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public LastUpdateConfigOut getLastUpdateConfig(String token, Long taskMngtId, String account, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";taskMngtId=" + gson.toJson(taskMngtId)
                + ";account=" + gson.toJson(account);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        LastUpdateConfigOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new LastUpdateConfigOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new SubFbConfigController().getLastUpdateConfig(hibernateHelper, token, taskMngtId, account, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("taskMngtId", taskMngtId);
            paramTr.put("account", account);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getLastUpdateConfig", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getLastUpdateConfig", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ReasonConfigOut getListReasonConfig(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ReasonConfigOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ReasonConfigOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ApParamController().getListReasonConfig(hibernateHelper, token, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListReasonConfig", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListReasonConfig", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public HistorySubFbConfigOut getListHistoryConfig(String token, Long id, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";id=" + gson.toJson(id);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        HistorySubFbConfigOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new HistorySubFbConfigOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new SubFbConfigController().getListHistoryConfig(hibernateHelper, token, id, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("id", id);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListHistoryConfig", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListHistoryConfig", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ImageInfrastructureOut getListImageType(String token, String locale, String action, Long taskMngtId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token + " action: " + action);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ImageInfrastructureOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ImageInfrastructureOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ApParamController().getListImageType(hibernateHelper, token, locale, action, taskMngtId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListImageType", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListImageType", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public DashboardBroadbanTab getDashboardBroadbandInfo(String token, String locale, Long staffId, Long shopId, String isManagement) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";locale=" + locale + ";staffId=" + staffId + ";shopId=" + shopId + ";isManagement=" + isManagement;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        DashboardBroadbanTab result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new DashboardBroadbanTab();
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            result = new TaskToAssignController().getDashboardBroadbandInfo(hibernateHelper, staffId, shopId, isManagement, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("staffId", staffId);
            paramTr.put("shopId", shopId);
            paramTr.put("isManagement", isManagement);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getDashboardBroadbandInfo", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getDashboardBroadbandInfo", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone configConnectImt(String token, String locale, Long staffId, Long taskManagementId, String splitter) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";locale=" + locale + ";staffId=" + staffId + ";taskManagementId=" + taskManagementId;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        WSRespone result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new WSRespone(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ServiceInfoController().configConnectImt(hibernateHelper, staffId, taskManagementId, locale, splitter);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("staffId", staffId);
            paramTr.put("taskManagementId", taskManagementId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "configConnectImt", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n configConnectImt", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public InforInfrastructureOut getInfoInfraToView(String token, String locale, Long taskMngtId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";taskMngtId=" + gson.toJson(taskMngtId);

        InforInfrastructureOut result = null;
        InforInfrastructureOut cloneResult = null;
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new InforInfrastructureOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            try {
                result = new TaskForUpdateController().getInfoInfraToView(hibernateHelper, locale, taskMngtId);
            } catch (Exception ex) {
                LogUtils.error(logger, ex.getMessage(), ex);
            }
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            if (result != null) {
                cloneResult = result.clone();
            }
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("taskMngtId", taskMngtId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getInfoInfraToView", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(cloneResult));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getInfoInfraToView", PATH, param, sTime, eTime, gson.toJson(cloneResult));
        return result;
    }

    @Override
    public StaffDetailOut getStaffInfo(String token, String staffCode, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";staffCode=" + gson.toJson(staffCode);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        StaffDetailOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new StaffDetailOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new SubFbConfigController().getStaffInfo(hibernateHelper, token, staffCode, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("staffCode", staffCode);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getStaffInfo", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getStaffInfo", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ParamOut getImageRequestLocationCustomer(Long requestId, String typeImage) {
        ParamOut paramOut = new ParamOut();
        paramOut.setErrorCode(Constants.ERROR_CODE_0);
        paramOut.setErrorDecription("Success");
        paramOut.setImgeList(new RequestUpdateLocationController().getImageRequestLocationCustomer(hibernateHelper, requestId, typeImage));
        return paramOut;
    }

    @Override
    public ParamOut getCCActionDetail(String token, String locale, String account, String actionType) {
        Date sTime = new Date();
        ParamOut paramOut = new ParamOut();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";account=" + gson.toJson(account) + ";actionType=" + gson.toJson(actionType);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        List<CcActionInformation> result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ArrayList<CcActionInformation>();
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
            paramOut.setCcActionDetail(result);
        } else {
            result = new CustomerController().getCCActionDetail(hibernateHelper, account, actionType);
            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription("SUCCESS");
            paramOut.setCcActionDetail(result);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("account", account);
            paramTr.put("actionType", actionType);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getCCActionDetail", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getCCActionDetail", PATH, param, sTime, eTime, gson.toJson(result));
        return paramOut;
    }

    @Override
    public ParamOut getAccountOfCustomer(String token, String locale, Long customerId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        ParamOut paramOut = new ParamOut();
        String param = " \n input:token=" + gson.toJson(token) + ";customerId=" + gson.toJson(customerId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        List<String> result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ArrayList<String>();
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
            paramOut.setAccountCustomer(result);
        } else {
            result = new CustomerController().getAccountOfCustomer(hibernateHelper, customerId);
            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription("SUCCESS");
            paramOut.setAccountCustomer(result);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("customerId", customerId);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getAccountOfCustomer", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getAccountOfCustomer", PATH, param, sTime, eTime, gson.toJson(result));
        return paramOut;
    }

    @Override
    public ParamOut getActionDetailType(String token, String locale) {
        Date sTime = new Date();
        ParamOut paramOut = new ParamOut();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        List<ApParam> result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ArrayList<ApParam>();
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
            paramOut.setCcActionType(result);
        } else {
            result = new CustomerController().getActionDetailType(hibernateHelper);
            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription("SUCCESS");
            paramOut.setCcActionType(result);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getActionDetailType", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getActionDetailType", PATH, param, sTime, eTime, gson.toJson(result));
        return paramOut;
    }

    @Override
    public WSRespone sendNimsSubActiveInfo(Long staffId, Long taskManagementId) {
        return new ServiceInfoController().sendNimsSubActiveInfo(hibernateHelper, staffId, taskManagementId);
    }

    @Override
    public WSRespone changeStationInfraStructure(Long staffId, String newStation, String connector, String portNo, String account) {
        return new ServiceInfoController().changeStationInfraStructure(hibernateHelper, staffId, newStation, connector, portNo, account);
    }

    @Override
    public ParamOut getStationByTeam(String token, String locale, String account, Long subId, Long staffId) {
        Date sTime = new Date();
        ParamOut paramOut = new ParamOut();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        List<TechnicalStation> result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ArrayList<TechnicalStation>();
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
            paramOut.setListStation(result);
        } else {
            result = new TechnicalStationController().getStationOfTeam(hibernateHelper, account, subId, staffId);
            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription(LabelUtil.getKey("common.success", locale));
            paramOut.setListStation(result);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getActionDetailType", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getActionDetailType", PATH, param, sTime, eTime, gson.toJson(result));
        return paramOut;
    }

    @Override
    public ParamOut getPortOdf(String token, String locale, Long odfId) {
        Date sTime = new Date();
        ParamOut paramOut = new ParamOut();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        List<PortOdf> result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ArrayList<PortOdf>();
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
            paramOut.setPortOdf(result);
        } else {
            result = new TechnicalStationController().getStationOfTeam(hibernateHelper, odfId);
            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription(LabelUtil.getKey("common.success", locale));
            paramOut.setPortOdf(result);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getActionDetailType", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getActionDetailType", PATH, param, sTime, eTime, gson.toJson(result));
        return paramOut;
    }

    @Override
    public StationInfo getStationInfoOfAccount(String token, String locale, Long subId) {
        return new TechnicalStationController().getStationInfoOfAccount(hibernateHelper, subId);
    }

    @Override
    public StaffInfoOut getStaffofShop(String token, String locale, Long shopId) {
        return new StaffInfoController().getStaffInfos(hibernateHelper, shopId);
    }

    @Override
    public WSRespone changeServiceAtoF(String token, String locale, String account, String loginName, String shopCodeLogin) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            return new RequestController().changeServiceAtoF(hibernateHelper, token, locale, account, loginName, shopCodeLogin);
        }
        return wsRespone;
    }

    @Override
    public ParamOut getStaffMapconnector(String token, String locale) {
        ParamOut param = new ParamOut();
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        param.setErrorCode(wsRespone.getErrorCode());
        param.setErrorDecription(wsRespone.getErrorDecription());
        if (Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            List<StaffManageConnectorInfo> list = new TechnicalStationController().getStaffMapconnector(hibernateHelper);
            param.setErrorCode(Constants.ERROR_CODE_0);
            param.setStaffMangeConnector(list);
        }

        return param;
    }

    @Override
    public StaffSalaryDetail getStaffSalaryDetail(String token, String locale, String staffCode, int month, int year) {
        StaffSalaryDetail dataSalary = new StaffSalaryDetail();
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            String url = ResourceUtils.getResource(Constants.URL_IMT_SALARY) + "staff_code=" + staffCode/*"BATTEAM1_BAT001_13_012"*/ + "&month=" + month + "&year=" + year;
            String response = new ServiceSupplier().sendRequest(Constants.SERVICE_METHOD_GET, url, null, wsRespone, null);
            StaffSalaryImtResponse serviceResponse = new Gson().fromJson(response, StaffSalaryImtResponse.class);
            if (serviceResponse != null) {
                return serviceResponse.getData();
            } else {
                dataSalary.setErrorCode(Constants.ERROR_CODE_1);
                dataSalary.setErrorDecription(LabelUtil.getKey("not.found.data", locale));
                dataSalary.setMessage(LabelUtil.getKey("not.found.data", locale));
            }

        } else {
            dataSalary.setErrorCode(wsRespone.getErrorCode());
            dataSalary.setErrorDecription(wsRespone.getErrorDecription());
        }
        return dataSalary;
    }

    @Override
    public ParamOut getKpiApParam(String token, String locale) {
        return new ApParamController().getListKpi(hibernateHelper, token, locale);
    }

    @Override
    public ParamOut getBrasIpPool(String token, String locale, String account) {
        return new DomainController().getBasPool(hibernateHelper, locale, account);
    }

    @Override
    public ParamOut getNotificationLog(String token, String locale, Long staffId) {
        Date sTime = new Date();
        ParamOut paramOut = new ParamOut();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        List<NotificationLog> result;
        Session cmPosSession = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ArrayList<NotificationLog>();
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
            paramOut.setListNotificationLog(result);
        } else {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            result = new NotificationMessageDAO().getNotificationLog(cmPosSession, staffId);
            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription(LabelUtil.getKey("common.success", locale));
            paramOut.setListNotificationLog(result);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            if (cmPosSession != null) {
                closeSessions(cmPosSession);
            }
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getNotificationLog", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getActionDetailType", PATH, param, sTime, eTime, gson.toJson(result));
        return paramOut;
    }

    @Override
    public OwnerOut getOwnerSabay(String token, String locale, String msisdn_n3_p3, String actionType) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        OwnerOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new OwnerOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new SaBayController().getLstOwnerSabay(msisdn_n3_p3, actionType, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getOwnerSabay", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getOwnerSabay", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public OwnerOut getMemberOwnerSabay(String token, String locale, String msisdn_n3_p3, String msisdn) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        OwnerOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new OwnerOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new SaBayController().getLstMemberOwnerSabay(msisdn_n3_p3, msisdn);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getMemberOwnerSabay", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getMemberOwnerSabay", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public OwnerOut getOTP_Sabay(String token, String locale, String msisdn_n3_p3, String msisdn) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        OwnerOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new OwnerOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new SaBayController().getOTP_N3_P3(hibernateHelper, msisdn_n3_p3, msisdn);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getOTP_Sabay", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getOTP_Sabay", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public OwnerOut sendOTP_Sabay(String token, String locale, String msisdn_n3_p3, String msisdn, String members, String OTP) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        OwnerOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new OwnerOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new SaBayController().sendOTP_N3_P3(hibernateHelper, msisdn_n3_p3, msisdn, members, OTP, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "sendOTP_Sabay", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n sendOTP_Sabay", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public OwnerOut CheckOwner_Sabay(String token, String locale, String msisdn_n3_p3, String msisdn) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        OwnerOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new OwnerOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new SaBayController().checkOwnerSabay(msisdn_n3_p3, msisdn, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "CheckOwner_Sabay", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n CheckOwner_Sabay", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public OwnerOut Checkmember_Sabay(String token, String locale, String msisdn_n3_p3, String msisdn, String param) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param_token = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        OwnerOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new OwnerOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new SaBayController().checkMemberSabay(msisdn_n3_p3, msisdn, param, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "Checkmember_Sabay", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n Checkmember_Sabay", PATH, param_token, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ParamOut topUp(String token, String encrypt, String signature) {
        StringBuilder clearText = new StringBuilder();
        DataTopUpPinCode dataDeObj = new DataTopUpPinCode();
        WSRespone wsRespone = new TokenController().checkPermisssion(hibernateHelper, token, encrypt, signature, "topUp", clearText);
        ParamOut result = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut();
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            try {
                dataDeObj = (DataTopUpPinCode) CommonWebservice.unmarshal(clearText.toString(), DataTopUpPinCode.class);
            } catch (Exception ex) {
            }
            result = new CustomerController().topUp(hibernateHelper, dataDeObj);
        }
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("clearText", clearText.toString());
            new MerchantController().insertActionLog(hibernateHelper, wsRespone.getMerchantAccount(), dataDeObj.getRequestId(), "topUp", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), null, result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            Long transactionId = new MerchantController().insertTransactionLog(hibernateHelper, wsRespone.getMerchantAccount(), dataDeObj.getRequestId(), "topUp", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), result.getSaleTransId() == null || result.getSaleTransId().isEmpty() ? null : Long.parseLong(result.getSaleTransId()), result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            result.setTransactionId(transactionId == null ? null : transactionId.toString());
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut getPinCode(String token, String encrypt, String signature) {
        StringBuilder clearText = new StringBuilder();
        DataTopUpPinCode dataDeObj = new DataTopUpPinCode();
        WSRespone wsRespone = new TokenController().checkPermisssion(hibernateHelper, token, encrypt, signature, "getPinCode", clearText);
        ParamOut result = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut();
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            try {
                dataDeObj = (DataTopUpPinCode) CommonWebservice.unmarshal(clearText.toString(), DataTopUpPinCode.class);
            } catch (Exception ex) {
            }
            result = new CustomerController().getPinCode(hibernateHelper, dataDeObj);
        }
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("clearText", clearText.toString());
            new MerchantController().insertActionLog(hibernateHelper, dataDeObj.getMerchantAccount(), dataDeObj.getRequestId(), "getPinCode", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), null, result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            Long transactionId = new MerchantController().insertTransactionLog(hibernateHelper, dataDeObj.getMerchantAccount(), dataDeObj.getRequestId(), "getPinCode", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), result.getSaleTransId() == null || result.getSaleTransId().isEmpty() ? null : Long.parseLong(result.getSaleTransId()), result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            result.setTransactionId(transactionId == null ? null : transactionId.toString());
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public LoginOut getToken(String username, String password) {
        StringBuilder merChantAccount = new StringBuilder();
        LoginOut result = new LoginController().getToken(hibernateHelper, username.toUpperCase(), password, merChantAccount);
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("user", username);
            paramTr.put("pass", password);
            new MerchantController().insertActionLog(hibernateHelper, merChantAccount.toString(), null, "getToken", null, null, null, null, result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut verifyChangeKit(String token, String locale, String msisdnAgent, String msisdnCust, String newMsisdnCust, String lastSerial) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param_token = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().checkPermisssion(hibernateHelper, token, null, null, "verifyChangeKit", null);
        ParamOut result = new ParamOut();
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            result = new PrepaidController().verifyChangeKit(hibernateHelper, locale, msisdnAgent, msisdnCust, newMsisdnCust, lastSerial);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("msisdnAgent", msisdnAgent);
            paramTr.put("msisdnCust", msisdnCust);
            paramTr.put("newMsisdnCust", newMsisdnCust);
            paramTr.put("lastSerial", lastSerial);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "verifyChangeKit", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n verifyChangeKit", PATH, param_token, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ParamOut verifyChangeSim(String token, String locale, String msisdnAgent, String msisdnCust, String lastSerial) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param_token = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().checkPermisssion(hibernateHelper, token, null, null, "verifyChangeSim", null);
        ParamOut result = new ParamOut();
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            result = new PrepaidController().verifyChangeSim(locale, msisdnAgent, msisdnCust, lastSerial);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("msisdnAgent", msisdnAgent);
            paramTr.put("msisdnCust", msisdnCust);
            paramTr.put("lastSerial", lastSerial);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "verifyChangeSim", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n verifyChangeSim", PATH, param_token, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone changeSim4G(String token, String locale, String pinCode, String transId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param_token = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().checkPermisssion(hibernateHelper, token, null, null, "changeSim4G", null);
        WSRespone result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new WSRespone(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new PrepaidController().changeSim4G(hibernateHelper, locale, pinCode, transId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("pinCode", pinCode);
            paramTr.put("transId", transId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "changeSim4G", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n changeSim4G", PATH, param_token, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone createNewInfoCus(String token, String locale, String serial, String isdn, String dealerIsdn, String idType, String fullName, String idNumber, String dob, List<ImageInput> lstImage, String puk) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param_token = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().checkPermisssion(hibernateHelper, token, null, null, "createNewInfoCus", null);
        WSRespone result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new WSRespone(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new PrepaidController().createNewInfoCus(token, serial, isdn, dealerIsdn, idType, fullName, idNumber, dob, lstImage, puk);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("serial", serial);
            paramTr.put("isdn", isdn);
            paramTr.put("dealerIsdn", dealerIsdn);
            paramTr.put("idType", idType);
            paramTr.put("fullName", fullName);
            paramTr.put("idNumber", idNumber);
            paramTr.put("dob", dob);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "createNewInfoCus", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n createNewInfoCus", PATH, param_token, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ParamOut confirmPincode(String token, String encrypt, String signature) {
        StringBuilder clearText = new StringBuilder();
        DataTopUpPinCode dataDeObj = new DataTopUpPinCode();
        WSRespone wsRespone = new TokenController().checkPermisssion(hibernateHelper, token, encrypt, signature, "confirmPincode", clearText);
        ParamOut result = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut();
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            try {
                dataDeObj = (DataTopUpPinCode) CommonWebservice.unmarshal(clearText.toString(), DataTopUpPinCode.class);
            } catch (Exception ex) {
            }
            result = new CustomerController().confirmPincode(hibernateHelper, dataDeObj);
        }
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("clearText", clearText.toString());
            new MerchantController().insertActionLog(hibernateHelper, dataDeObj.getMerchantAccount(), dataDeObj.getRequestId(), "confirmPincode", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), null, result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            Long transactionId = new MerchantController().insertTransactionLog(hibernateHelper, dataDeObj.getMerchantAccount(), dataDeObj.getRequestId(), "confirmPincode", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), result.getSaleTransId() == null ? null : Long.parseLong(result.getSaleTransId()), result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            result.setTransactionId(transactionId == null ? null : transactionId.toString());
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut confirmTopUp(String token, String requestId, String date) {
        ParamOut result = null;
        result = new CustomerController().confirmTopup(hibernateHelper, requestId, date, token);
        return result;
    }

    @Override
    public ParamOut getAccountInformation(String token, String locale, String account) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        Long sTimeTr = System.currentTimeMillis();
        ParamOut paramOut = new ParamOut();
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription(LabelUtil.getKey("common.success", locale));
            paramOut.setAccountInfo(new CustomerController().getAccountInformation(hibernateHelper, account));
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getAccountInformation", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(paramOut));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return paramOut;
    }

    @Override
    public ParamOut updateRequestAnalysSOC(String token, String locale, Long complainId, Long result, String description, List<String> listErrorCode) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        Long sTimeTr = System.currentTimeMillis();
        ParamOut paramOut = new ParamOut();
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            String resultUpdate = new TaskForUpdateController().updateRequestAnalysSOC(hibernateHelper, complainId, result, description, listErrorCode);
            if (resultUpdate == null || resultUpdate.isEmpty()) {
                paramOut.setErrorCode(Constants.ERROR_CODE_0);
                paramOut.setErrorDecription(LabelUtil.getKey("common.success", locale));
            } else {
                paramOut.setErrorCode(Constants.ERROR_CODE_1);
                paramOut.setErrorDecription(resultUpdate);
            }

        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("result", result);
            paramTr.put("complainId", complainId);
            paramTr.put("listErrorCode", listErrorCode);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "updateRequestAnalysSOC", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(paramOut));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return paramOut;
    }

    @Override
    public WSRespone checkSocComplaint(String token, String locale, Long taskStaffMngtId, String progress, String staffCode) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        Long sTimeTr = System.currentTimeMillis();
        if (wsRespone == null || Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            wsRespone = new TaskForUpdateController().checkSocComplain(hibernateHelper, locale, taskStaffMngtId, progress, staffCode);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("taskStaffMngtId", taskStaffMngtId);
            paramTr.put("progress", progress);
            paramTr.put("staffCode", staffCode);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "checkSocComplaint", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(wsRespone));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return wsRespone;
    }

    /**
     * @author: duyetdk
     * @since 8/3/2019
     * @des: lay danh sach giao dich chua nop tien theo ma nhan vien
     * @param fromDate
     * @param toDate
     * @param staffId
     * @param locale
     * @param token
     * @return
     */
    @Override
    public DebitTransOut getListDebitTransaction(String fromDate, String toDate, Long staffId, String locale, String token, String currency) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";fromDate=" + gson.toJson(fromDate) + ";toDate=" + gson.toJson(toDate)
                + ";staffId=" + gson.toJson(staffId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        DebitTransOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new DebitTransOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new DebitController().getListDebitTransaction(hibernateHelper, fromDate, toDate, staffId, locale, token, currency);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("fromDate", fromDate);
            paramTr.put("toDate", toDate);
            paramTr.put("staffId", staffId);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListDebitTransaction", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListDebitTransaction", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author: duyetdk
     * @since 9/3/2019
     * @des: lay danh sach cong no cua nhan vien
     * @param shopCode
     * @param staffCode
     * @param staffId
     * @param shopId
     * @param status
     * @param locale
     * @param token
     * @return
     */
    @Override
    public DebitStaffOut getListDebitStaff(String shopCode, String staffCode, Long staffId, Long shopId, Long status, String locale, String token) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";shopCode=" + gson.toJson(shopCode) + ";staffCode=" + gson.toJson(staffCode)
                + ";staffId=" + gson.toJson(staffId) + ";shopId=" + gson.toJson(shopId) + ";status=" + gson.toJson(status);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        DebitStaffOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new DebitStaffOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new DebitController().getListDebitStaff(hibernateHelper, staffCode, shopCode, shopId, status, locale, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("staffCode", staffCode);
            paramTr.put("shopCode", shopCode);
            paramTr.put("shopId", shopId);
            paramTr.put("staffId", staffId);
            paramTr.put("status", status);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListDebitStaff", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListDebitStaff", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author: duyetdk
     * @since 11/3/2019
     * @des: lay lich su giao dich tung nhan vien
     * @param payCode
     * @param payMethod
     * @param staffId
     * @param status
     * @param pageNo
     * @param locale
     * @param token
     * @return
     */
    @Override
    public DebitTransHistoryOut transHistory(String payCode, String payMethod, Long staffId, Long status, Long pageNo, String locale, String token) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";payCode=" + gson.toJson(payCode) + ";payMethod=" + gson.toJson(payMethod)
                + ";staffId=" + gson.toJson(staffId) + ";status=" + gson.toJson(status);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        DebitTransHistoryOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new DebitTransHistoryOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new DebitController().transHistory(hibernateHelper, staffId, status, payMethod, payCode, pageNo, locale, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("payCode", payCode);
            paramTr.put("payMethod", payMethod);
            paramTr.put("staffId", staffId);
            paramTr.put("status", status);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "transHistory", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n transHistory", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author: duyetdk
     * @since 12/3/2019
     * @des: tao yeu cau thanh toan
     * @param token
     * @param staffId
     * @param amount
     * @param locale
     * @return
     */
    @Override
    public ClearDebitTransOut clearDebitTrans(String token, String debitTransId, Long staffId, Double amount, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";amount=" + gson.toJson(amount)
                + ";staffId=" + gson.toJson(staffId) + ";debitTransId=" + gson.toJson(debitTransId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ClearDebitTransOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ClearDebitTransOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new DebitController().clearDebitTrans(hibernateHelper, staffId, debitTransId, amount, locale, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("amount", amount);
            paramTr.put("staffId", staffId);
            paramTr.put("debitTransId", debitTransId);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "clearDebitTrans", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n clearDebitTrans", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author: duyetdk
     * @since 13/3/2019
     * @des: xac nhan giao dich sau khi Clear Debit
     * @param currencyCode
     * @param paymentContent
     * @param token
     * @param debitTransId
     * @param txPaymentTokenId
     * @param staffId
     * @param locale
     * @param amount
     * @param paymentCode
     * @return
     */
    @Override
    public ClearDebitTransOut confirmDebitTrans(String token, String debitTransId, Double amount, String paymentCode, String txPaymentTokenId,
            String currencyCode, String paymentContent, Long staffId, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";debitTransId=" + gson.toJson(debitTransId) + ";txPaymentTokenId=" + gson.toJson(txPaymentTokenId)
                + ";staffId=" + gson.toJson(staffId) + ";amount=" + gson.toJson(amount) + ";paymentContent=" + gson.toJson(paymentContent)
                + ";paymentCode=" + gson.toJson(paymentCode) + ";currencyCode=" + gson.toJson(currencyCode);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ClearDebitTransOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ClearDebitTransOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new DebitController().confirmDebitTrans(hibernateHelper, debitTransId, amount, paymentCode,
                    txPaymentTokenId, currencyCode, paymentContent, staffId, locale, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("debitTransId", debitTransId);
            paramTr.put("txPaymentTokenId", txPaymentTokenId);
            paramTr.put("currencyCode", currencyCode);
            paramTr.put("paymentContent", paymentContent);
            paramTr.put("amount", amount);
            paramTr.put("paymentCode", paymentCode);
            paramTr.put("staffId", staffId);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "confirmDebitTrans", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n confirmDebitTrans", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author: duyetdk
     * @since 14/3/2019
     * @des: nhan no tu nhan vien khac
     * @param token
     * @param debitTransId
     * @param amount
     * @param paymentCode
     * @param receiveDebtStaffId
     * @param staffId
     * @param locale
     * @return
     */
    @Override
    public ClearDebitTransOut transferDebit(String token, String debitTransId, Double amount, String paymentCode,
            Long receiveDebtStaffId, Long staffId, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";debitTransId=" + gson.toJson(debitTransId) + ";receiveDebtStaffId=" + gson.toJson(receiveDebtStaffId)
                + ";staffId=" + gson.toJson(staffId) + ";amount=" + gson.toJson(amount) + ";paymentCode=" + gson.toJson(paymentCode);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ClearDebitTransOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ClearDebitTransOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new DebitController().transferDebit(hibernateHelper, token, debitTransId, amount, paymentCode, receiveDebtStaffId, staffId, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("debitTransId", debitTransId);
            paramTr.put("receiveDebtStaffId", receiveDebtStaffId);
            paramTr.put("amount", amount);
            paramTr.put("paymentCode", paymentCode);
            paramTr.put("staffId", staffId);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "transferDebit", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n transferDebit", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author: duyetdk
     * @since 1/4/2019
     * @des: check giao dich phat sinh
     * @param token
     * @param amount
     * @param staffId
     * @param locale
     * @return
     */
    @Override
    public LockDebitTransOut checkLockTransaction(String token, Double amount, Long staffId, String locale, String currency) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token)
                + ";staffId=" + gson.toJson(staffId) + ";amount=" + gson.toJson(amount);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        LockDebitTransOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new LockDebitTransOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new DebitController().checkLockTransaction(hibernateHelper, token, amount, staffId, locale, currency);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("amount", amount);
            paramTr.put("staffId", staffId);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "checkLockTransaction", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n checkLockTransaction", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ParamOut checkInforBeforeChangeSim(String token, String locale, String isdn, String idNo) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        Long sTimeTr = System.currentTimeMillis();
        ParamOut paramOut = new ParamOut();
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription(LabelUtil.getKey("common.success", locale));
            return new PrepaidController().checkChangeSim(hibernateHelper, isdn, idNo);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "checkInforBeforeChangeSim", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(paramOut));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return paramOut;
    }

    @Override
    public ParamOut getReasonChangeSim(String token, String locale, Long shopId, String isdn) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        Long sTimeTr = System.currentTimeMillis();
        ParamOut paramOut = new ParamOut();
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription(LabelUtil.getKey("common.success", locale));
            paramOut.setListReasonChangeSim(new PrepaidController().getReasonChangeSim(hibernateHelper, shopId, isdn));
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getReasonChangeSim", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(paramOut));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return paramOut;
    }

    @Override
    public ParamOut changeSimPre(String token, String locale, ChangeSimInput changeSimInput) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        Long sTimeTr = System.currentTimeMillis();
        ParamOut paramOut = new ParamOut();
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            return new PrepaidController().changeSimPre(hibernateHelper, changeSimInput);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getAccountInformation", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(paramOut));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return paramOut;

    }

    /**
     *
     * @param token
     * @param locale
     * @param code
     * @return
     */
    @Override
    public ComplaintAcceptTypeOut getListComplaintAcceptType(String token, String locale, String code) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ComplaintAcceptTypeOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ComplaintAcceptTypeOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ComplaintController().getListComplaintAcceptType(hibernateHelper, locale, token, code);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListComplaintAcceptType", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListComplaintAcceptType", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     *
     * @param groupId
     * @param locale
     * @param token
     * @return
     */
    @Override
    public ComplaintTypeOut getListComplaintType(Long groupId, String locale, String token) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";groupId=" + gson.toJson(groupId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ComplaintTypeOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ComplaintTypeOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ComplaintController().getListComplaintType(hibernateHelper, groupId, locale, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("groupId", groupId);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListComplaintType", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListComplaintType", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     *
     * @param parentId
     * @param locale
     * @param token
     * @return
     */
    @Override
    public ComplaintGroupOut getListComplaintGroupType(Long parentId, String locale, String token) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";parentId=" + gson.toJson(parentId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ComplaintGroupOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ComplaintGroupOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ComplaintController().getListComplaintGroupType(hibernateHelper, parentId, locale, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("parentId", parentId);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListComplaintGroupType", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListComplaintGroupType", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     *
     * @param locale
     * @param token
     * @return
     */
    @Override
    public ComplaintGroupOut getListComplaintGroup(String locale, String token) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ComplaintGroupOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ComplaintGroupOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ComplaintController().getListComplaintGroup(hibernateHelper, locale, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListComplaintGroup", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListComplaintGroup", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author: duyetdk
     * @since 18/4/2019 get team giai quyet su co
     * @param locale
     * @param token
     * @param subId
     * @return
     */
    @Override
    public CompDepartmentGroupOut getListCompTeam(String locale, String token, Long subId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";subId=" + gson.toJson(subId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CompDepartmentGroupOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CompDepartmentGroupOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ComplaintController().getListCompTeam(hibernateHelper, subId, locale, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("subId", subId);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListCompTeam", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListCompTeam", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019 lay danh sach nguon complaint
     * @param token
     * @param locale
     * @return
     */
    @Override
    public ComplaintAcceptSourceOut getListComplaintAcceptSource(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ComplaintAcceptSourceOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ComplaintAcceptSourceOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ComplaintController().getListComplaintAcceptSource(hibernateHelper, locale, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListComplaintAcceptSource", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListComplaintAcceptSource", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author: duyetdk
     * @since 18/4/2019 tiep nhan khieu nai
     * @param token
     * @param complaintInput
     * @param locale
     * @return
     */
    @Override
    public WSRespone submitComplaint(String token, ComplaintInput complaintInput, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";complaintInput=" + gson.toJson(complaintInput);
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result == null || Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            result = new ComplaintController().submitComplaint(hibernateHelper, complaintInput, locale, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("complaintInput", gson.toJson(complaintInput));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "submitComplaint", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n submitComplaint", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author: duyetdk
     * @since 18/4/2019
     * @des lay thong tin subAdslLL
     * @param locale
     * @param token
     * @param isdn
     * @return
     */
    @Override
    public SubAdslLeaselineOut getListSubAdslLeaseline(String locale, String token, String isdn) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";isdn=" + gson.toJson(isdn);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        SubAdslLeaselineOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new SubAdslLeaselineOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ComplaintController().getListSubAdslLeaseline(hibernateHelper, isdn, locale, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("isdn", isdn);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListSubAdslLeaseline", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListSubAdslLeaseline", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ServicePaymentOut getSubInfoForPayment(String isdn) {
        return new PaymentController().searchServicePayment(hibernateHelper, isdn);
    }

    @Override
    public ParamOut payAdvanceSub(String staffCode, String encrypt, String signature) {
        StringBuilder clearText = new StringBuilder();
        DataTopUpPinCode dataDeObj = new DataTopUpPinCode();
        WSRespone wsRespone = new TokenController().checkPermisssion(hibernateHelper, encrypt, signature, "payAdvanceSub", clearText, staffCode);
        ParamOut result = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut();
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            try {
                dataDeObj = (DataTopUpPinCode) CommonWebservice.unmarshal(clearText.toString(), DataTopUpPinCode.class);
            } catch (Exception ex) {
            }
            result = new PaymentController().payAdvanceSub(hibernateHelper, "en_US", dataDeObj, staffCode);
        }
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("data", dataDeObj);
            new MerchantController().insertActionLog(hibernateHelper, wsRespone.getMerchantAccount(), dataDeObj.getTid(), "payAdvanceSub", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), null, result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            Long transactionId = new MerchantController().insertTransactionLog(hibernateHelper, wsRespone.getMerchantAccount(), dataDeObj.getTid(), "payAdvanceSub", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), result.getSaleTransId() == null || result.getSaleTransId().isEmpty() ? null : Long.parseLong(result.getSaleTransId()), result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            result.setTransactionId(transactionId == null ? null : transactionId.toString());
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut confirmPayAdvance(String staffCode, String tid, String date) {
        return new PaymentController().confirmPayAdvance(hibernateHelper, staffCode, tid, date);
    }

    /**
     * @author: duyetdk
     * @since 12/5/2019
     * @des kich hoat thue bao
     * @param token
     * @param activeSubscriberPreIn
     * @param locale
     * @return
     */
    @Override
    public WSRespone activeNewSubscriberPre(String token, ActiveSubscriberPreIn activeSubscriberPreIn, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";activeSubscriberPreIn=" + gson.toJson(activeSubscriberPreIn);
        WSRespone result = new TokenController().validateToken(hibernateHelper, token, locale);
        if (result != null && !Constants.ERROR_CODE_0.equals(result.getErrorCode())) {
            result = new WSRespone(result.getErrorCode(), result.getErrorDecription());
        } else {
            result = new SubscriberController().activeNewSubscriberPre(hibernateHelper, activeSubscriberPreIn, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("activeSubscriberPreIn", gson.toJson(activeSubscriberPreIn));
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "activeNewSubscriberPre", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n activeNewSubscriberPre", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author duyetdk
     * @param token
     * @param telecomServiceId
     * @param groupProduct
     * @param locale
     * @return
     */
    @Override
    public ProductOut getListProductPre(String token, Long telecomServiceId, String groupProduct, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";telecomServiceId=" + gson.toJson(telecomServiceId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ProductOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ProductOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ProductController().getListProductPre(hibernateHelper, telecomServiceId, groupProduct, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("telecomServiceId", telecomServiceId);
            paramTr.put("groupProduct", groupProduct);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListProductPre", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListProductPre", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author anhnv
     * @param token
     * @param locale
     * @param serviceType
     * @param ISDN
     * @des tim thong tin thue bao
     * @return
     */
    @Override
    public SubInfoByISDNPreOut findSubInfoByISDNPre(String token, String locale, String serviceType, String ISDN) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";locale=" + gson.toJson(locale) + ";serviceType=" + gson.toJson(serviceType)
                + ";ISDN=" + gson.toJson(ISDN);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        SubInfoByISDNPreOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new SubInfoByISDNPreOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            if (serviceType == null || serviceType.trim().isEmpty()) {
                result = new SubInfoByISDNPreOut(Constants.ERROR_CODE_1, "Please input serviceType");
            } else {
                result = new RequestChangeInformationController().getSubInfoByISDNPre(hibernateHelper, ISDN, locale);
            }

        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("serviceType", serviceType);
            paramTr.put("ISDN", ISDN);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "findSubInfoByISDNPre", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
            //duyetdk: ghi log truy van thong tin KH
            Session cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            Session cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            Token tokenObj = new TokenBussiness().findByToken(cmPosSession, token);
            String userLogin = "";
            if (tokenObj != null) {
                userLogin = tokenObj.getUserName();
            }
            Date nowDate = Common.getDateTimeFromDB(cmPreSession);
            new ActionLogPrDAO().insertQueryLog(cmPreSession, "Change information",
                    LogUtils.toJson(paramTr), nowDate, userLogin);
            closeSessions(cmPosSession);
            closeSessions(cmPreSession);
        }
        logVTG.saveLog(USER_NAME, URI, " \n findSubInfoByISDNPre", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author anhnv
     * @param token
     * @param locale
     * @param codes
     * @des Lay danh sach reason
     * @return
     */
    @Override
    public ListReasonByCodePreOut getListReasonByCodePre(String token, String locale, Codes codes) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";locale=" + gson.toJson(locale) + ";codes=" + gson.toJson(codes);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ListReasonByCodePreOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ListReasonByCodePreOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RequestChangeInformationController().getReasonByCodes(hibernateHelper, codes, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("codes", codes);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListReasonByCodePre", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListReasonByCodePre", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    /**
     * @author anhnv
     * @param token
     * @param locale
     * @param codes
     * @des gui OTP va tra ve so OTP
     * @return
     */
    @Override
    public OTPOut getOTPChangeInformationPre(String token, String locale, String isdn) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";locale=" + gson.toJson(locale) + ";isdn=" + gson.toJson(isdn);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        OTPOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new OTPOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RequestChangeInformationController().getOTPChangeInformationPre(locale, isdn);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("isdn", isdn);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getOTPChangeInformationPre", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getOTPChangeInformationPre", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ChangeInformationOut doChangeInformationPre(String token, String locale,
            String otp, Long subId, String serviceType, Long custId, String name,
            Long idType, String province, String address, String DOB, String Sex,
            String isdn, String contact, FormChangeInformation form,
            FormChangeInformation lstImageIDCard, FormChangeInformation lstOldImageIDCard,
            String reasonCode, Long reasonId, String idNo) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";locale=" + gson.toJson(locale)
                + ";otp=" + gson.toJson(otp) + ";subId=" + gson.toJson(subId)
                + ";serviceType=" + gson.toJson(serviceType)
                + ";custId=" + gson.toJson(custId)
                + ";name=" + gson.toJson(name)
                + ";idType=" + gson.toJson(idType)
                + ";province=" + gson.toJson(province)
                + ";address=" + gson.toJson(address)
                + ";DOB=" + gson.toJson(DOB)
                + ";Sex=" + gson.toJson(Sex)
                + ";isdn=" + gson.toJson(isdn)
                + ";contact=" + gson.toJson(contact)
                + ";reasonCode=" + gson.toJson(reasonCode)
                + ";reasonId=" + gson.toJson(reasonId)
                + ";idNo=" + gson.toJson(idNo);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ChangeInformationOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ChangeInformationOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            String staffCode = null;
            Session cmPosSession = null;
            try {
                cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
                Token tokenObj = new TokenBussiness().findByToken(cmPosSession, token);
                Long staffId = null;
                if (tokenObj != null) {
                    staffCode = tokenObj.getUserName();
                }
                result = new RequestChangeInformationController().changeInformationCustomer(hibernateHelper, locale,
                        otp, subId, serviceType, custId, name,
                        idType, province, address, DOB, Sex,
                        isdn, contact, form,
                        lstImageIDCard, lstOldImageIDCard,
                        reasonCode, reasonId, idNo, staffCode);
            } catch (Exception e) {
                result = new ChangeInformationOut();
                result.setErrorDecription(e.getMessage());
                result.setErrorCode(Constants.ERROR_CODE_1);
            } finally {
                try {
                    if (cmPosSession != null) {
                        rollBackTransactions(cmPosSession);
                    }
                } catch (Exception ex) {
                }
            }
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("otp", otp);
            paramTr.put("subId", subId);
            paramTr.put("serviceType", serviceType);
            paramTr.put("custId", custId);
            paramTr.put("name", name);
            paramTr.put("idType", idType);
            paramTr.put("province", province);
            paramTr.put("address", address);
            paramTr.put("DOB", DOB);
            paramTr.put("Sex", Sex);
            paramTr.put("isdn", isdn);
            paramTr.put("contact", contact);
            paramTr.put("reasonCode", reasonCode);
            paramTr.put("reasonId", reasonId);
            paramTr.put("idNo", idNo);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getOTPChangeInformationPre", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n doChangeInformationPre", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    // Tony Nhan
    // getLstAccountByStaff
    @Override
    public AccountByStaffOut getLstAccountByStaff(String token, String locale,
            String staffID, String account, String infrasType, String infrasStatus, String btsCode, Long pageNo,
            String connectorCode, String deviceCode) {
        LogUtils.info(logger, "getLstAccountByStaff:result= ok");
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        // WSRespone wsRespone = null;
        Long sTimeTr = System.currentTimeMillis();
        AccountByStaffOut accountByStaffOut = new AccountByStaffOut();
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            accountByStaffOut.setErrorCode(wsRespone.getErrorCode());
            accountByStaffOut.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            accountByStaffOut = new AccountInfrasController().searchAccountByStaff(hibernateHelper,
                    locale, staffID, account, infrasType, infrasStatus, btsCode, pageNo,
                    connectorCode, deviceCode, token);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("staffID", staffID);
            paramTr.put("account", account);
            paramTr.put("infrasType", infrasType);
            paramTr.put("infrasStatus", infrasStatus);
            paramTr.put("btsCode", btsCode);
            paramTr.put("pageNo", pageNo);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(),
                    "getLstAccountByStaff", LogUtils.toJson(paramTr),
                    (eTimeTr - sTimeTr), LogUtils.toJson(accountByStaffOut));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return accountByStaffOut;
    }

    @Override
    public AccountInfrasOut getInfoFrasByAccount(String token, String locale,
            String account, String infraType, String stationCode) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        // WSRespone wsRespone = null;
        Long sTimeTr = System.currentTimeMillis();
        AccountInfrasOut accountInfrasOut = new AccountInfrasOut();
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            accountInfrasOut.setErrorCode(wsRespone.getErrorCode());
            accountInfrasOut.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            try {
                if (account == null || account.isEmpty() || account.equals("?")) {
                    accountInfrasOut = new AccountInfrasOut(Constants.ERROR_CODE_1, LabelUtil.getKey("infras.account.is.required", locale));
                    return accountInfrasOut;
                }
                if (stationCode == null || stationCode.isEmpty() || stationCode.equals("?")) {
                    accountInfrasOut = new AccountInfrasOut(Constants.ERROR_CODE_1, LabelUtil.getKey("infras.station.is.required", locale));
                    return accountInfrasOut;
                }
                if (infraType == null || infraType.isEmpty() || infraType.equals("?")) {
                    accountInfrasOut = new AccountInfrasOut(Constants.ERROR_CODE_1, LabelUtil.getKey("infras.is.required", locale));
                    return accountInfrasOut;
                }
                if (infraType.equals("GPON")) {
                    accountInfrasOut = new AccountInfrasController().getAccountInfrasGpon(hibernateHelper, account, locale);
                    AccountInfrasOut accountInfrasOut_Temp = new AccountInfrasController().getInfraStructureOfAccount(hibernateHelper,
                            locale, stationCode);
                    accountInfrasOut.setLstSubBranchGpon(accountInfrasOut_Temp.getLstSubBranchGpon());
                    accountInfrasOut.setLstSpitterGpon(accountInfrasOut_Temp.getLstSpitterGpon());
                    accountInfrasOut.setLstPortGpon(accountInfrasOut_Temp.getLstPortGpon());

                } else if (infraType.equals("AON") || infraType.equals("FCN")) {
                    accountInfrasOut = new AccountInfrasController().getAccountInfrasAON(account, locale, hibernateHelper);
                } else if (infraType.equals("CCN")) {
                    accountInfrasOut = new AccountInfrasController().getAccountInfrasADSL(hibernateHelper, account, locale);
                    AccountInfrasOut accountInfrasOut_Temp = new AccountInfrasController().getLstCabinet(hibernateHelper, stationCode, locale);
                    if (!accountInfrasOut_Temp.getLstBoxCode().isEmpty()) {
                        accountInfrasOut.setLstBoxCode(accountInfrasOut_Temp.getLstBoxCode());
                    }
                    if (!accountInfrasOut_Temp.getLstBoxPort().isEmpty()) {
                        accountInfrasOut.setLstBoxPort(accountInfrasOut_Temp.getLstBoxPort());
                    }

                }
                accountInfrasOut = new AccountInfrasController().getLenghtCable(hibernateHelper,
                        account, locale, accountInfrasOut);

            } catch (Exception ex) {
                LogUtils.error(logger, ex.getMessage(), ex);
            }

        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("account", account);
            paramTr.put("infraType", infraType);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(),
                    "getInfoFrasByAccount", LogUtils.toJson(paramTr),
                    (eTimeTr - sTimeTr), LogUtils.toJson(accountInfrasOut));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return accountInfrasOut;
    }

    @Override
    public WSRespone updateInfrasByStaff(String token, String locale, String account, String staffId,
            String odfIndoorCode, Long odfIndoorPort, Long odfIndoorPort2,
            String odfOutdoorCode, Long odfOutdoorPort, Long odfOutdoorPort2,
            Double lat, Double longy, Long lengthCable, Long portLogic, String snCode,
            String splitterCode, Long portSpliiter, String boxCableCode, String boxCablePort, String infrasType, String stationCode,
            Long deviceId, Long portId) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        WSRespone wsResult = new WSRespone();
        Long sTimeTr = System.currentTimeMillis();
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            wsResult = new WSRespone(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            if (account == null || account.isEmpty() || account.equals("?")) {
                wsResult = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("infras.account.is.required", locale));
                return wsResult;
            }
            if (staffId == null || staffId.isEmpty() || staffId.equals("?")) {
                wsResult = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("staffId.is.required", locale));
                return wsResult;
            }
            if (stationCode == null || stationCode.isEmpty() || stationCode.equals("?")) {
                wsResult = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("infras.station.is.required", locale));
                return wsResult;
            }
            if (infrasType == null || infrasType.isEmpty() || infrasType.equals("?")) {
                wsResult = new WSRespone(Constants.ERROR_CODE_1, LabelUtil.getKey("infras.is.required", locale));
                return wsResult;
            }
            wsResult = new AccountInfrasController().UpdateAccountByStaffInfras(hibernateHelper,
                    locale, account, staffId, odfIndoorCode, odfIndoorPort, odfIndoorPort2,
                    odfOutdoorCode, odfOutdoorPort, odfOutdoorPort2, lat, longy, lengthCable,
                    portLogic, snCode, splitterCode, portSpliiter, boxCableCode, boxCablePort,
                    stationCode, infrasType, deviceId, portId);

        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("account", account);
            paramTr.put("staffId", staffId);
            paramTr.put("odfIndoorCode", odfIndoorCode);
            paramTr.put("odfIndoorPort", odfIndoorPort);
            paramTr.put("odfOutdoorCode", odfOutdoorCode);
            paramTr.put("odfOutdoorPort", odfOutdoorPort);
            paramTr.put("lat", lat);
            paramTr.put("longy", longy);
            paramTr.put("lengthCable", lengthCable);
            paramTr.put("portLogic", portLogic);
            paramTr.put("snCode", snCode);
            paramTr.put("splitterCode", splitterCode);
            paramTr.put("boxCableCode", boxCableCode);
            paramTr.put("boxCablePort", boxCablePort);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(),
                    "updateInfrasByStaff", LogUtils.toJson(paramTr),
                    (eTimeTr - sTimeTr), LogUtils.toJson(wsRespone));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return wsResult;
    }

    @Override
    public AccountInfrasOut getLocationStationCode(String token, String locale, String stationCode) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        AccountInfrasOut wsResult = new AccountInfrasOut();
        Long sTimeTr = System.currentTimeMillis();
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            wsResult = new AccountInfrasOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            AccountInfrasLocationOut accountInfrasLocationOut;
            try {
                accountInfrasLocationOut = getStationAccountInfras(stationCode);
                if (accountInfrasLocationOut != null) {
                    wsResult.setLatXSN(accountInfrasLocationOut.getX());
                    wsResult.setLatYSN(accountInfrasLocationOut.getY());
                }
                wsResult.setStationCode(stationCode);
                wsResult.setErrorCode(com.viettel.bccs.cm.util.Constants.RESPONSE_SUCCESS);
                wsResult.setErrorDecription(LabelUtil.getKey("common.success", locale));
            } catch (Exception ex) {
                LogUtils.error(logger, ex.getMessage(), ex);
                wsResult = new AccountInfrasOut(Constants.ERROR_CODE_1,
                        LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            }

        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("stationCode", stationCode);

            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(),
                    "getLocationStationCode", LogUtils.toJson(paramTr),
                    (eTimeTr - sTimeTr), LogUtils.toJson(wsRespone));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return wsResult;
    }

    @Override
    public ParamOut getProductVasFTTH() {
        return new ProductController().getProductVasFTTH(hibernateHelper);
    }

    @Override
    public ProductOut getBunldleTvProduct() {
        return new ProductController().getBunldleTvProduct(hibernateHelper);
    }

    @Override
    public PayAdvanceBillOut printInvoiceBluetooth(String token, String locale, String accBundleTv) {
        return new PaymentController().printInvoiceBluetooth(hibernateHelper, locale, accBundleTv);
    }

    @Override
    public BundleTVInfo getAccBundleTvInfo(String token, String locale, String accBundleTv) {
        return new ProductController().getAccBundleTvInfo(hibernateHelper, accBundleTv);
    }

    @Override
    public WSRespone addUserBundleTv(String token, String locale, Long subId) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        Long sTimeTr = System.currentTimeMillis();
        if (wsRespone == null || Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            wsRespone = new BundleTvController().addUserBundleTv(hibernateHelper, subId);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("taskStaffMngtId", subId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "addUserBundleTv", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(wsRespone));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return wsRespone;
    }

    /**
     * @author: duyetdk
     * @since 06/06/2019
     * @des hien thi anh nha KH
     * @param cusId
     * @return
     */
    @Override
    public ImageRespone viewImageCustomerLocation(String cusId) {
        ImageRespone response = new RequestUpdateLocationController().viewImageCustomerLocation(hibernateHelper, cusId);
        return response;
    }

    @Override
    public ParamOut checkServiceOnline(String token) {
        StringBuilder clearText = new StringBuilder();
        DataTopUpPinCode dataDeObj = new DataTopUpPinCode();
        WSRespone wsRespone = new TokenController().checkPermisssion(hibernateHelper, token, null, null, "checkServiceOnline", clearText);
        ParamOut result = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut();
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            result = new CustomerController().checkServiceOnline(hibernateHelper, clearText.toString());
        }
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("clearText", clearText.toString());
            new MerchantController().insertActionLog(hibernateHelper, wsRespone.getMerchantAccount(), dataDeObj.getRequestId(), "checkServiceOnline", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), null, result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut searchRestoreIsdn(String locale, String token, String isdn) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        Long sTimeTr = System.currentTimeMillis();
        ParamOut result = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            return new SubscriberController().searchRestoreIsdn(hibernateHelper, locale, token, isdn);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("isdn", isdn);

            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(),
                    "searchRestoreIsdn", LogUtils.toJson(paramTr),
                    (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ReasonOut getReasonRestoreIsdn(String locale, String token) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        Long sTimeTr = System.currentTimeMillis();
        ReasonOut result = new ReasonOut();
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            return new SubscriberController().getReasonRestoreIsdn(hibernateHelper, locale, token);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);

            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(),
                    "getReasonRestoreIsdn", LogUtils.toJson(paramTr),
                    (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public WSRespone restoreIsdn(String locale, String token, ChangeSimInput changeSimInput) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        Long sTimeTr = System.currentTimeMillis();
        WSRespone result = new WSRespone();
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            return new SubscriberController().restoreIsdn(hibernateHelper, locale, token, changeSimInput);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);

            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(),
                    "getReasonRestoreIsdn", LogUtils.toJson(paramTr),
                    (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut getRecoverActionType(String token, String locale) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        Long sTimeTr = System.currentTimeMillis();
        ParamOut result = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            return new ApParamController().getRecoverActionType(hibernateHelper, token, locale);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);

            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(),
                    "searchRestoreIsdn", LogUtils.toJson(paramTr),
                    (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut getRecoverReason(String token, String locale, String actionType) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        Long sTimeTr = System.currentTimeMillis();
        ParamOut result = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            return new ApParamController().getRecoverReason(hibernateHelper, token, locale, actionType);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);

            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(),
                    "searchRestoreIsdn", LogUtils.toJson(paramTr),
                    (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut updateRecoverReason(String token, String locale, Long contractId, String actionType, String reasonCode, String x, String y) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        Long sTimeTr = System.currentTimeMillis();
        ParamOut result = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            return new ApParamController().updateRecoverReason(hibernateHelper, token, locale, contractId, actionType, reasonCode, x, y);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);

            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(),
                    "searchRestoreIsdn", LogUtils.toJson(paramTr),
                    (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public TechInforOut getListConnectorByStation(String token, String locale, Long stationId, String stationCode) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";stationId=" + gson.toJson(stationId)
                + ";stationCode=" + gson.toJson(stationCode);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        TechInforOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new TechInforOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new AccountInfrasController().getAllConnectorOfStation(hibernateHelper, token, locale, stationId, stationCode);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("stationId", stationId);
            paramTr.put("stationCode", stationCode);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListConnectorByStation", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListConnectorByStation", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public PotentialCustomerOut getListPotentialCust(String token, String locale, String name, String phoneNumber,
            String address, Long status, Long pageNo, String province, String kindOfCustomer) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token)
                + ";locale=" + gson.toJson(locale)
                + ";name=" + gson.toJson(name)
                + ";phoneNumber=" + gson.toJson(phoneNumber)
                + ";address=" + gson.toJson(address)
                + ";status=" + gson.toJson(status)
                + ";pageNo=" + gson.toJson(pageNo)
                + ";province=" + gson.toJson(province)
                + ";kindOfCustomer" + gson.toJson(kindOfCustomer);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        PotentialCustomerOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new PotentialCustomerOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RequestUpdateLocationController().getListPotentialCust(hibernateHelper, name, phoneNumber,
                    address, status, pageNo, locale, token, province, kindOfCustomer);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("name", name);
            paramTr.put("phoneNumber", phoneNumber);
            paramTr.put("address", address);
            paramTr.put("status", status);
            paramTr.put("pageNo", pageNo);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListPotentialCust",
                    LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListPotentialCust", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone addNewPotentialCust(String token, String locale, String name, String phoneNumber, String address,
            String province, String email, String otherOperator, Double fee, String latitude, String longitude,
            String kindOfCustomer, String expectedService, String expectedScale, String serviceUsed) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token)
                + ";locale=" + gson.toJson(locale)
                + ";name=" + gson.toJson(name)
                + ";phoneNumber=" + gson.toJson(phoneNumber)
                + ";address=" + gson.toJson(address)
                + ";province=" + gson.toJson(province)
                + ";email=" + gson.toJson(email)
                + ";otherOperator=" + gson.toJson(otherOperator)
                + ";fee=" + gson.toJson(fee)
                + ";latitude=" + gson.toJson(latitude)
                + ";longitude=" + gson.toJson(longitude)
                + ";kindOfCustomer=" + gson.toJson(kindOfCustomer)
                + ";expectedService=" + gson.toJson(expectedService)
                + ";expectedScale=" + gson.toJson(expectedScale)
                + ";serviceUsed=" + gson.toJson(serviceUsed);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        WSRespone result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new WSRespone(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RequestUpdateLocationController().addNewPotentialCust(hibernateHelper, locale, token, name,
                    phoneNumber, address, province, email, otherOperator, fee, latitude, longitude,
                    kindOfCustomer, expectedService, expectedScale, serviceUsed);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("name", name);
            paramTr.put("phoneNumber", phoneNumber);
            paramTr.put("address", address);
            paramTr.put("province", province);
            paramTr.put("email", email);
            paramTr.put("otherOperator", otherOperator);
            paramTr.put("fee", fee);
            paramTr.put("latitude", latitude);
            paramTr.put("longitude", longitude);
            paramTr.put("kindOfCustomer", kindOfCustomer);
            paramTr.put("expectedService", expectedService);
            paramTr.put("expectedScale", expectedScale);
            paramTr.put("serviceUsed", serviceUsed);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "addNewPotentialCust",
                    LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n addNewPotentialCust", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone updatePotentialCust(String token, String locale, Long custId, String name,
            String phoneNumber, String address, String province, String email,
            String otherOperator, Double fee, String latitude, String longitude, Long status,
            String kindOfCustomer, String expectedService, String expectedScale, String serviceUsed) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token)
                + ";custId=" + gson.toJson(custId)
                + ";status=" + gson.toJson(status)
                + ";locale=" + gson.toJson(locale)
                + ";name=" + gson.toJson(name)
                + ";phoneNumber=" + gson.toJson(phoneNumber)
                + ";address=" + gson.toJson(address)
                + ";province=" + gson.toJson(province)
                + ";email=" + gson.toJson(email)
                + ";otherOperator=" + gson.toJson(otherOperator)
                + ";fee=" + gson.toJson(fee)
                + ";latitude=" + gson.toJson(latitude)
                + ";longitude=" + gson.toJson(longitude)
                + ";kindOfCustomer=" + gson.toJson(kindOfCustomer)
                + ";expectedService=" + gson.toJson(expectedService)
                + ";expectedScale=" + gson.toJson(expectedScale)
                + ";serviceUsed=" + gson.toJson(serviceUsed);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        WSRespone result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new WSRespone(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RequestUpdateLocationController().updatePotentialCust(hibernateHelper, locale, token, custId,
                    name, phoneNumber, address, province, email, otherOperator, fee, latitude, longitude, status,
                    kindOfCustomer, expectedService, expectedScale, serviceUsed);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("name", name);
            paramTr.put("phoneNumber", phoneNumber);
            paramTr.put("address", address);
            paramTr.put("province", province);
            paramTr.put("email", email);
            paramTr.put("otherOperator", otherOperator);
            paramTr.put("fee", fee);
            paramTr.put("latitude", latitude);
            paramTr.put("longitude", longitude);
            paramTr.put("custId", custId);
            paramTr.put("status", status);
            paramTr.put("kindOfCustomer", kindOfCustomer);
            paramTr.put("expectedService", expectedService);
            paramTr.put("expectedScale", expectedScale);
            paramTr.put("serviceUsed", serviceUsed);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "updatePotentialCust",
                    LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n updatePotentialCust", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public WSRespone deletePotentialCust(String token, String locale, Long custId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token)
                + ";custId=" + gson.toJson(custId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        WSRespone result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new WSRespone(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RequestUpdateLocationController().deletePotentialCust(hibernateHelper, custId, locale, token);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("custId", custId);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "deletePotentialCust",
                    LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n deletePotentialCust", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ParamOut buyData(String token, String encrypt, String signature) {
        StringBuilder clearText = new StringBuilder();
        DataTopUpPinCode dataDeObj = new DataTopUpPinCode();
        WSRespone wsRespone = new TokenController().checkPermisssion(hibernateHelper, token, encrypt, signature, "buyData", clearText);
        ParamOut result = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut();
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            try {
                dataDeObj = (DataTopUpPinCode) CommonWebservice.unmarshal(clearText.toString(), DataTopUpPinCode.class);
            } catch (Exception ex) {
            }
            result = new EmoneyController().buyData(hibernateHelper, dataDeObj);
        }
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("clearText", clearText.toString());
            new MerchantController().insertActionLog(hibernateHelper, wsRespone.getMerchantAccount(), dataDeObj.getRequestId(), "buyData", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), null, result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            Long transactionId = new MerchantController().insertTransactionLog(hibernateHelper, wsRespone.getMerchantAccount(), dataDeObj.getRequestId(), "buyData", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), result.getSaleTransId() == null || result.getSaleTransId().isEmpty() ? null : Long.parseLong(result.getSaleTransId()), result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            result.setTransactionId(transactionId == null ? null : transactionId.toString());
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut extendTelecomService(String token, String encrypt, String signature) {
        StringBuilder clearText = new StringBuilder();
        DataTopUpPinCode dataDeObj = new DataTopUpPinCode();
        WSRespone wsRespone = new TokenController().checkPermisssion(hibernateHelper, token, encrypt, signature, "extendTelecomService", clearText);
        ParamOut result = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut();
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            try {
                dataDeObj = (DataTopUpPinCode) CommonWebservice.unmarshal(clearText.toString(), DataTopUpPinCode.class);
            } catch (Exception ex) {
            }
            result = new EmoneyController().extendTelecomService(hibernateHelper, dataDeObj);
        }
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("clearText", clearText.toString());
            new MerchantController().insertActionLog(hibernateHelper, wsRespone.getMerchantAccount(), dataDeObj.getRequestId(), "extendTelecomService", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), null, result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            Long transactionId = new MerchantController().insertTransactionLog(hibernateHelper, wsRespone.getMerchantAccount(), dataDeObj.getRequestId(), "extendTelecomService", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), result.getSaleTransId() == null || result.getSaleTransId().isEmpty() ? null : Long.parseLong(result.getSaleTransId()), result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            result.setTransactionId(transactionId == null ? null : transactionId.toString());
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut changeDeviceAccFtth(String token, String locale, String account, String deviceCode) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";account=" + gson.toJson(account) + ";deviceCode=" + gson.toJson(deviceCode);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new TaskForUpdateController().changeDeviceAccFtth(hibernateHelper, locale, account, deviceCode);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("account", account);
            paramTr.put("deviceCode", deviceCode);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "changeDeviceAccFtth", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListDebitStaff", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ParamOut buyDataVAS(String token, String encrypt, String signature) {
        StringBuilder clearText = new StringBuilder();
        DataTopUpPinCode dataDeObj = new DataTopUpPinCode();
        WSRespone wsRespone = new TokenController().checkPermisssion(hibernateHelper, token, encrypt, signature, "buyDataVAS", clearText);
        ParamOut result = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut();
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            try {
                dataDeObj = (DataTopUpPinCode) CommonWebservice.unmarshal(clearText.toString(), DataTopUpPinCode.class);
            } catch (Exception ex) {
            }
            result = new EmoneyController().buyDataVAS(hibernateHelper, dataDeObj);
        }
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("clearText", clearText.toString());
            new MerchantController().insertActionLog(hibernateHelper, wsRespone.getMerchantAccount(), dataDeObj.getRequestId(), "buyDataVAS", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), null, result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            Long transactionId = new MerchantController().insertTransactionLog(hibernateHelper, wsRespone.getMerchantAccount(), dataDeObj.getRequestId(), "buyDataVAS", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), result.getSaleTransId() == null || result.getSaleTransId().isEmpty() ? null : Long.parseLong(result.getSaleTransId()), result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            result.setTransactionId(transactionId == null ? null : transactionId.toString());
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut buyDataVASPartner(String token, String encrypt, String signature) {
        StringBuilder clearText = new StringBuilder();
        DataTopUpPinCode dataDeObj = new DataTopUpPinCode();
        WSRespone wsRespone = new TokenController().checkPermisssion(hibernateHelper, token, encrypt, signature, "buyDataVASPartner", clearText);
        ParamOut result = null;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut();
            result.setErrorCode(wsRespone.getErrorCode());
            result.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            try {
                dataDeObj = (DataTopUpPinCode) CommonWebservice.unmarshal(clearText.toString(), DataTopUpPinCode.class);
            } catch (Exception ex) {
            }
            result = new EmoneyController().buyDataVASPartner(hibernateHelper, dataDeObj);
        }
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("clearText", clearText.toString());
            new MerchantController().insertActionLog(hibernateHelper, wsRespone.getMerchantAccount(), dataDeObj.getRequestId(), "buyDataVASPartner", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), null, result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            Long transactionId = new MerchantController().insertTransactionLog(hibernateHelper, wsRespone.getMerchantAccount(), dataDeObj.getRequestId(), "buyDataVASPartner", null, dataDeObj.getIsdn(), dataDeObj.getAmount(), result.getSaleTransId() == null || result.getSaleTransId().isEmpty() ? null : Long.parseLong(result.getSaleTransId()), result.getErrorCode(), LogUtils.toJson(paramTr), LogUtils.toJson(result), new Date(), result.getErrorDecription());
            result.setTransactionId(transactionId == null ? null : transactionId.toString());
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public StaffSalaryDetail getStaffSalaryDetailAPBus(String token, String locale, String staffCode, int month, int year) {
        /*String btext = "<return><total>100$</total><detail><leaf><col><order>1</order><type>Detail</type><value>Standardworkdate</value></col><col><order>2</order><type>value</type><value>23</value></col></leaf><leaf><col><order>1</order><type>Detail</type><value>Workdate</value></col><col><order>2</order><type>value</type><value>23</value></col></leaf><leaf><col><order>1</order><type>Detail</type><value>Supportincome</value></col><col><order>2</order><type>value</type><value>23</value></col></leaf><leaf><col><order>1</order><type>Detail</type><value>Penalty</value></col><col><order>2</order><type>value</type><value>23</value></col></leaf><node><col><order>1</order><type>Detail</type><value>Basicsalary</value></col><col><order>2</order><type>value</type><value>$100</value></col><node><col><order>1</order><type>name</type><value>Salarylevel</value></col><col><order>2</order><type>value</type><value>2</value></col><leaf><col><order>1</order><type>name</type><value>Contractquantity</value></col><col><order>2</order><type>value</type><value>12</value></col></leaf><leaf><col><order>1</order><type>name</type><value>Money</value></col><col><order>2</order><type>value</type><value>12$</value></col></leaf></node><node><col><order>1</order><type>name</type><value>KI</value></col><col><order>2</order><type>value</type><value>C</value></col></node></node><node><col><order>1</order><type>Detail</type><value>Comission</value></col><col><order>2</order><type>value</type><value>100</value></col><leaf><col><order>1</order><type>Kindofcommission</type><value>Newcontract</value></col><col><order>2</order><type>Quantity</type><value>12</value></col><col><order>3</order><type>Money</type><value>4$</value></col></leaf><leaf><col><order>1</order><type>Kindofcommission</type><value>ChangeAtoF</value></col><col><order>2</order><type>Quantity</type><value>0</value></col><col><order>3</order><type>Money</type><value>0$</value></col></leaf><leaf><col><order>1</order><type>Kindofcommission</type><value>Changepackage</value></col><col><order>2</order><type>Quantity</type><value>0</value></col><col><order>3</order><type>Money</type><value>0$</value></col></leaf><leaf><col><order>1</order><type>Kindofcommission</type><value>BundleTv</value></col><col><order>2</order><type>Quantity</type><value>0</value></col><col><order>3</order><type>Money</type><value>0$</value></col></leaf><leaf><col><order>1</order><type>Kindofcommission</type><value>BundleMobileFBB</value></col><col><order>2</order><type>Quantity</type><value>0</value></col><col><order>3</order><type>Money</type><value>0$</value></col></leaf></node><node><col><order>1</order><type>Detail</type><value>Otherincome</value></col><col><order>2</order><type>value</type><value>100</value></col><leaf><col><order>1</order><type>Incomefrom</type><value>Receover</value></col><col><order>2</order><type>Quantity</type><value>12</value></col><col><order>3</order><type>Money</type><value>4$</value></col></leaf><leaf><col><order>1</order><type>Incomefrom</type><value>Collectpayment</value></col><col><order>2</order><type>Quantity</type><value>0</value></col><col><order>3</order><type>Money</type><value>0$</value></col></leaf><leaf><col><order>1</order><type>Incomefrom</type><value>Bonuscollectpayment</value></col><col><order>2</order><type>Quantity</type><value>0</value></col><col><order>3</order><type>Money</type><value>0$</value></col></leaf><leaf><col><order>1</order><type>Incomefrom</type><value>Updatelocationpayment</value></col><col><order>2</order><type>Quantity</type><value>0</value></col><col><order>3</order><type>Money</type><value>0$</value></col></leaf><leaf><col><order>1</order><type>Incomefrom</type><value>gasolineforcollectpayment</value></col><col><order>2</order><type>Quantity</type><value>0</value></col><col><order>3</order><type>Money</type><value>0$</value></col></leaf></node><node><col><order>1</order><type>Detail</type><value>Support</value></col><col><order>2</order><type>value</type><value>100</value></col><leaf><col><order>1</order><type>Kindofsupport</type><value>Senoriity</value></col><col><order>2</order><type>Money</type><value>4$</value></col></leaf><leaf><col><order>1</order><type>Kindofsupport</type><value>Businessgasoline</value></col><col><order>2</order><type>Money</type><value>0$</value></col></leaf><leaf><col><order>1</order><type>Kindofsupport</type><value>Phoneallowance</value></col><col><order>2</order><type>Money</type><value>0$</value></col></leaf></node></detail></return>";
        try {
            StaffSalaryDetail dataSalary = (StaffSalaryDetail) CommonWebservice.unmarshal(btext, StaffSalaryDetail.class);
            return dataSalary;
        } catch (Exception ex) {
            return new StaffSalaryDetail();
        }*/
        Long sTimeTr = System.currentTimeMillis();
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        StaffSalaryDetail result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new StaffSalaryDetail(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new SalaryController().getStaffSalaryDetailAPBus(hibernateHelper, staffCode, locale, month, year);
        }
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("staffCode", staffCode);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "deletePotentialCust",
                    LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        return result;
    }

//phuonghc 15062020
    @Override
    public PotentialCustomerOut getPotentialCustomerHistory(String token, String locale, String custId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token)
                + ";locale=" + gson.toJson(locale)
                + ";custId=" + gson.toJson(custId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        PotentialCustomerOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new PotentialCustomerOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RequestUpdateLocationController().getListPotentialCustomerHistory(hibernateHelper, token, locale, custId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("custId", custId);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListPotentialCustomerHistory",
                    LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListPotentialCustomerHistory", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ParamOut getListParamForPotentialCustomer(String token, String locale) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token)
                + ";locale=" + gson.toJson(locale)
                + ";paramType=" + gson.toJson(PARAM_TYPE);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RequestUpdateLocationController().getListParamByParamType(hibernateHelper, token, locale, PARAM_TYPE);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("paramType", PARAM_TYPE);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListPotentialCustomerHistory",
                    LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListPotentialCustomerHistory", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ParamOut getPaymentReport(String token, String locale, String staffCode, String codeReport, String isChiefCenter) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token)
                + ";staffCode=" + gson.toJson(staffCode)
                + ";codeReport=" + gson.toJson(codeReport);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new PaymentController().getPaymentReport(hibernateHelper, locale, codeReport, staffCode, Boolean.valueOf(isChiefCenter));
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("staffCode", staffCode);
            paramTr.put("codeReport", codeReport);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "changeDeviceAccFtth", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListDebitStaff", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ParamOut getUnpaidDetail(String token, String locale, String staffCode, String codeReport, String isChiefCenter) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token)
                + ";staffCode=" + gson.toJson(staffCode)
                + ";codeReport=" + gson.toJson(codeReport);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new PaymentController().getUnpaidDetail(hibernateHelper, locale, codeReport, staffCode, Boolean.valueOf(isChiefCenter));
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("account", "");
            paramTr.put("deviceCode", "");
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "changeDeviceAccFtth", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListDebitStaff", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    //phuonghc 03092020
    @Override
    public ParamOut doingChangeDeploymentAddress(String token, String locale, ChangeDeploymentAddressIn input) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token) + ";locale=" + gson.toJson(locale);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ChangingDeploymentAddressController().doingChangeDeploymentAddress(hibernateHelper, input, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "doingChangeDeploymentAddress",
                    LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n doingChangeDeploymentAddress", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ParamOut getListReasonByType(String token, String locale, String custId, String account) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token)
                + ";locale=" + gson.toJson(locale)
                + ";custId=" + gson.toJson(custId);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ChangingDeploymentAddressController().getListReasonByType(hibernateHelper, custId, account, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("custId", custId);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListReasonByType",
                    LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListReasonByType", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }

    @Override
    public ParamOut getListAccountForSubscriber(String token, String locale, String custId) {
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token)
                + ";locale=" + gson.toJson(locale)
                + ";paramType=" + gson.toJson(PARAM_TYPE);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ChangingDeploymentAddressController().getListAccountForSubscriber(hibernateHelper, custId, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("paramType", PARAM_TYPE);
        } catch (Exception ex) {
            result.setErrorDecription(ex.getMessage());
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getListPotentialCustomerHistory",
                    LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        }
        logVTG.saveLog(USER_NAME, URI, " \n getListAccountForSubscriber", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
}
