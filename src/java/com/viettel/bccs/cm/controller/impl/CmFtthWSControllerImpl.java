/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller.impl;

import com.viettel.bccs.cm.controller.BaseController;
import com.viettel.bccs.cm.controller.CmFtthWSController;
import com.viettel.bccs.cm.controller.TokenController;
import com.viettel.bccs.cm.controller.TransLogController;
import com.viettel.bccs.cm.controller.CmFtthBaseController;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.model.input.CreateInterruptFtthInput;
import com.viettel.brcd.ws.model.input.PushSmsInterruptFtthInput;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.brcd.ws.model.output.CmFtthWSOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.eafs.util.SpringUtil;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author partner1
 */
public class CmFtthWSControllerImpl extends BaseController implements CmFtthWSController {
    private transient HibernateHelper hibernateHelper;

    public HibernateHelper getHibernateHelper() {
        if (this.hibernateHelper == null) {
            this.hibernateHelper = (HibernateHelper) SpringUtil.getBean("hibernateHelper");
            setHibernateHelper(this.hibernateHelper);
        }
        return hibernateHelper;
    }

    public void setHibernateHelper(HibernateHelper hibernateHelper) {
        this.hibernateHelper = hibernateHelper;
    }
    
    @Override
    public CmFtthWSOut getCurBranchUser(String token, String locale, Long staffId){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().getCurBranchUser(hibernateHelper, locale, staffId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "getCurBranchUser", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getCurBranchUser", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    
    @Override
    public CmFtthWSOut getReasonType(String token, String locale){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().getReasonType(hibernateHelper, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "getReasonType", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getReasonType", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    
    @Override
    public CmFtthWSOut getReasonDetail(String token, String locale, Long reasonTypeId){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().getReasonDetail(hibernateHelper, locale, reasonTypeId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "getReasonDetail", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getReasonDetail", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    
    @Override
    public CmFtthWSOut getBranchList(String token, String locale){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().getBranchList(hibernateHelper, locale);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "getBranchList", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getBranchList", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    
    @Override
    public CmFtthWSOut getBtsByBranch(String token, String locale, Long branchShopId){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().getBtsByBranch(hibernateHelper, locale, branchShopId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "getBtsByBranch", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getBtsByBranch", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    
    @Override
    public CmFtthWSOut getStaffOfBranch(String token, String locale, Long branchShopId){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().getStaffOfBranch(hibernateHelper, locale, branchShopId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "getStaffOfBranch", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getStaffOfBranch", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    
    @Override
    public CmFtthWSOut getConnectorByStation(String token, String locale, Long stationId){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().getConnectorByStation(hibernateHelper, locale, stationId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "getConnectorByStation", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getConnectorByStation", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    
    @Override
    public CmFtthWSOut createInterruptFtth(String token, String locale, CreateInterruptFtthInput createInterruptFtthInput){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().createInterruptFtth(hibernateHelper, locale, createInterruptFtthInput);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "createInterruptFtth", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n createInterruptFtth", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    @Override
    public CmFtthWSOut getInterruptFtth(String token, String locale, String staffCode, Long reasonTypeId, Long reasonDetailId, String influenceScope, String fromDate, String toDate, Long shopId){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().getInterruptFtth(hibernateHelper, locale, staffCode, reasonTypeId, reasonDetailId, influenceScope, fromDate, toDate, shopId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "getInterruptFtth", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getInterruptFtth", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    
    @Override
    public CmFtthWSOut getDetailInterruptFtth(String token, String locale, Long interruptId){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().getDetailInterruptFtth(hibernateHelper, locale, interruptId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "getDetailInterruptFtth", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getDetailInterruptFtth", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    
    @Override
    public CmFtthWSOut getHistoryInterruptFtth(String token, String locale, Long interruptId){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().getHistoryInterruptFtth(hibernateHelper, locale, interruptId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "getHistoryInterruptFtth", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n getHistoryInterruptFtth", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    
    @Override
    public CmFtthWSOut approveInterruptFtth(String token, String locale, Long interruptId, String staffCode){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().approveInterruptFtth(hibernateHelper, locale, interruptId, staffCode);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "approveInterruptFtth", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n approveInterruptFtth", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    
    @Override
    public CmFtthWSOut cancelInterruptFtth(String token, String locale, Long interruptId, String staffCode){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().cancelInterruptFtth(hibernateHelper, locale, interruptId, staffCode);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "cancelInterruptFtth", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n cancelInterruptFtth", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    
    @Override
    public CmFtthWSOut pushSmsInterruptFtth(String token, String locale, PushSmsInterruptFtthInput pushSmsInterruptFtthInput){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().pushSmsInterruptFtth(hibernateHelper, locale, pushSmsInterruptFtthInput);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "pushSmsInterruptFtth", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n pushSmsInterruptFtth", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    
    @Override
    public CmFtthWSOut countCustomerBTSConnector(String token, String locale, Long interruptId){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().countCustomerBTSConnector(hibernateHelper, locale, interruptId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "countCustomerBTSConnector", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n countCustomerBTSConnector", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    
    @Override
    public CmFtthWSOut countCustomerServiceBtsConnector(String token, String locale, Long interruptId, Long btsId, Long connectorId){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().countCustomerServiceBtsConnector(hibernateHelper, locale, interruptId, btsId, connectorId);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "countCustomerServiceBtsConnector", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n countCustomerServiceBtsConnector", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
    
    @Override
    public CmFtthWSOut rejectInterruptFtth(String token, String locale, Long interruptId, String staffCode){
        Date sTime = new Date();
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input:token=" + gson.toJson(token);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        CmFtthWSOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new CmFtthWSOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new CmFtthBaseController().rejectInterruptFtth(hibernateHelper, locale, interruptId, staffCode);
        }
        Date eTime = new Date();
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            new TransLogController().insert(hibernateHelper, token, CmFtthWSControllerImpl.class.getName(), "rejectInterruptFtth", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        logVTG.saveLog(USER_NAME, URI, " \n rejectInterruptFtth", PATH, param, sTime, eTime, gson.toJson(result));
        return result;
    }
}
