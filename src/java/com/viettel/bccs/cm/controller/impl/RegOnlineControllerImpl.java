/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller.impl;

import com.viettel.bccs.cm.controller.BaseController;
import com.viettel.bccs.cm.controller.RegOnlineController;
import com.viettel.bccs.cm.controller.RegisterFTTHOnlineController;
import com.viettel.bccs.cm.controller.TokenController;
import com.viettel.bccs.cm.controller.TransLogController;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.model.input.regOnline.RegOnlineInput;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.eafs.util.SpringUtil;
import java.util.HashMap;

/**
 *
 * @author cuongdm
 */
public class RegOnlineControllerImpl extends BaseController implements RegOnlineController {

    private transient HibernateHelper hibernateHelper;

    public HibernateHelper getHibernateHelper() {
        if (this.hibernateHelper == null) {
            this.hibernateHelper = (HibernateHelper) SpringUtil.getBean("hibernateHelper");
            setHibernateHelper(this.hibernateHelper);
        }
        return hibernateHelper;
    }

    public void setHibernateHelper(HibernateHelper hibernateHelper) {
        this.hibernateHelper = hibernateHelper;
    }

    @Override
    public ParamOut regOnlineFtth(String token, String locale, RegOnlineInput input) {
        Long sTimeTr = System.currentTimeMillis();
        String param = " \n input=" + gson.toJson(input);
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RegisterFTTHOnlineController().regOnlineFtth(hibernateHelper, locale, input);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("param", param);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "regOnlineFtth", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut getInfoRegOnlineFtth(String token, String locale, Long requestId, String customerPhone, String customerName,
            String code, String source, String fromDate, String toDate, String province, String assignShop, String assignStaff, String staffCode,
            Long filterStatus, Long statusDeadline, Long statusReqCancel, int pageIndex, int pageSize, String requesterCancel) {
        Long sTimeTr = System.currentTimeMillis();
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RegisterFTTHOnlineController().getInfoRegOnlineFtth(hibernateHelper, locale, requestId, customerPhone, customerName, code, source, fromDate, toDate, province, assignShop, assignStaff, staffCode, filterStatus, statusDeadline, statusReqCancel, pageIndex, pageSize, requesterCancel);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("requestId", requestId);
            paramTr.put("customerPhone", customerPhone);
            paramTr.put("customerName", customerName);
            paramTr.put("code", code);
            paramTr.put("source", source);
            paramTr.put("fromDate", fromDate);
            paramTr.put("toDate", toDate);
            paramTr.put("province", province);
            paramTr.put("assignShop", assignShop);
            paramTr.put("assignStaff", assignStaff);
            paramTr.put("staffCode", staffCode);
            paramTr.put("filterStatus", filterStatus);
            paramTr.put("statusDeadline", statusDeadline);
            paramTr.put("pageIndex", pageIndex);
            paramTr.put("pageSize", pageSize);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getInfoRegOnlineFtth", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut getRegOnlineHis(String token, String locale, String staffCode, Long requestId) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RegisterFTTHOnlineController().getRegOnlineHis(hibernateHelper, locale, staffCode, requestId);
        }
        return result;
    }

    @Override
    public ParamOut updateProgressRegOnline(String token, String locale, Long requestId, String staffCode, String assignProvince, String assignShop, String assignStaff, Long status, String reason, String description, Long statusApprove) {
        Long sTimeTr = System.currentTimeMillis();
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RegisterFTTHOnlineController().updateProgressRegOnline(hibernateHelper, locale, requestId, staffCode, assignProvince, assignShop, assignStaff, status, reason, description, statusApprove);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("staffCode", staffCode);
            paramTr.put("requestId", requestId);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "updateProgressRegOnline", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut getProgressRegOnline(String token, String locale, Long type) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RegisterFTTHOnlineController().getProgressRegOnline(hibernateHelper, locale, type);
        }
        return result;
    }

    @Override
    public ParamOut getRequestIdFromIsdn(String token, String locale, String isdn) {
        Long sTimeTr = System.currentTimeMillis();
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RegisterFTTHOnlineController().getRequestIdFromIsdn(hibernateHelper, locale, isdn);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("isdn", isdn);
            new TransLogController().insert(hibernateHelper, token, CmControllerImpl.class.getName(), "getRequestIdFromIsdn", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut getProvinceReqOnline(String token, String locale, String staffCode, Long filterStatus, Long statusDeadline) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RegisterFTTHOnlineController().getProvinceReqOnline(hibernateHelper, locale, staffCode, filterStatus, statusDeadline);
        }
        return result;
    }

    @Override
    public ParamOut getReqOnline(String token, String locale, String staffCode, Long filterStatus) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RegisterFTTHOnlineController().getReqOnline(hibernateHelper, locale, staffCode, filterStatus);
        }
        return result;
    }

    @Override
    public ParamOut getReasonRegOnline(String token, String locale) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RegisterFTTHOnlineController().getReasonRegOnline(hibernateHelper, locale);
        }
        return result;
    }

    @Override
    public ParamOut getListShowroom(String token, String locale, String staffCode, String province) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RegisterFTTHOnlineController().getListShowroom(hibernateHelper, locale, staffCode, province);
        }
        return result;
    }

    @Override
    public ParamOut getListStaffShowroom(String token, String locale, String staffCode, String shopCode) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new RegisterFTTHOnlineController().getListStaffShowroom(hibernateHelper, locale, staffCode, shopCode);
        }
        return result;
    }
}
