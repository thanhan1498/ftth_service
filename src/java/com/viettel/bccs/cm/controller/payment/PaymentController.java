package com.viettel.bccs.cm.controller.payment;

import com.viettel.bccs.api.debit.DAO.DebitBusiness;
import com.viettel.bccs.api.Task.DAO.ContractDAO;
import com.viettel.bccs.cm.bussiness.StaffBussiness;
import com.viettel.bccs.cm.bussiness.payment.PaymentBussiness;
import com.viettel.bccs.cm.common.util.Constant;
import com.viettel.bccs.cm.controller.BaseController;
import com.viettel.bccs.cm.dao.CustomerDAO;
import com.viettel.bccs.cm.dao.ReasonDAO;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.dao.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.dao.SubBundleTvDAO;
import com.viettel.bccs.cm.database.BO.ServicePayment;
import com.viettel.bccs.cm.database.DAO.PaymentDAO;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.StaffInfo;
import com.viettel.bccs.cm.supplier.ReasonSupplier;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubBundleTv;
import com.viettel.bccs.cm.supplier.ApParamSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.merchant.dao.TransactionLogDAO;
import com.viettel.brcd.dao.pre.IsdnSendSmsDAO;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.PaymentInput;
import com.viettel.brcd.ws.model.output.CustomerPayment;
import com.viettel.brcd.ws.model.output.DataTopUpPinCode;
import com.viettel.brcd.ws.model.output.Debit;
import com.viettel.brcd.ws.model.output.DebitOut;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.PayAdvanceBillOut;
import com.viettel.brcd.ws.model.output.PaymentReport;
import com.viettel.brcd.ws.model.output.RecoverInfo;
import com.viettel.brcd.ws.model.output.ServicePaymentOut;
import com.viettel.brcd.ws.model.output.UnpaidPaymentDetailExpand;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.im.database.BO.AccountAgent;
import java.sql.CallableStatement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class PaymentController extends BaseController {

    private static final String EMPTY_STR = "";

    public PaymentController() {
        logger = Logger.getLogger(PaymentController.class);
    }

    public Debit getCustomerDebit(Session paymentSession, Long custId, String locale) throws Exception {
        Debit result = new Debit();
        String debit = new PaymentBussiness().getCustomerDebit(paymentSession, custId);
        if (debit != null) {
            debit = debit.trim();
        }
        if (debit == null || debit.isEmpty()) {
            result.setDebitDecripsion(debit);
            result.setResult(false);
            return result;
        }
        result.setDebitDecripsion(LabelUtil.formatKey("customer.is.having.dept.charges", locale, debit));
        result.setResult(true);
        return result;
    }

    public DebitOut getCustomerDebit(HibernateHelper hibernateHelper, Long custId, String locale) {
        DebitOut result = null;
        Session paymentSession = null;
        try {
            paymentSession = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            Debit debit = getCustomerDebit(paymentSession, custId, locale);
            result = new DebitOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), debit);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new DebitOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(paymentSession);
            LogUtils.info(logger, "PaymentController.getCustomerDebit:result=" + LogUtils.toJson(result));
        }
    }

    public ServicePaymentOut searchServicePayment(HibernateHelper hibernateHelper, Long staffId, Long invoiceListId, Long contractId, String locale) {
        ServicePaymentOut result = null;
        Session paymentSession = null;
        Session cmSession = null;
        Session imSession = null;
        try {
            paymentSession = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            ServicePayment service = new PaymentDAO().searchServicePayment(paymentSession, imSession, cmSession, staffId, invoiceListId, contractId);
            if (!"".equals(service.getMssError()) && service.getMssError() != null) {
                result = new ServicePaymentOut(Constants.ERROR_CODE_1, service.getMssError());
                return result;
            }
            result = new ServicePaymentOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), service);
            String sql = "select ACTION_TYPE_CODE actionType, "
                    + "       REASON_CODE reason, "
                    + "       CREATE_USER staffCode, "
                    + "       to_char(CREATE_DATE, 'dd/mm/yyyy hh24:mi:ss') time "
                    + "  from update_recover_reason "
                    + " where contract_id = ? "
                    + "   and status = 1";
            Query query = cmSession.createSQLQuery(sql)
                    .addScalar("actionType", Hibernate.STRING)
                    .addScalar("reason", Hibernate.STRING)
                    .addScalar("staffCode", Hibernate.STRING)
                    .addScalar("time", Hibernate.STRING)
                    .setResultTransformer(Transformers.aliasToBean(RecoverInfo.class));
            query.setParameter(0, contractId);
            List<RecoverInfo> list = query.list();
            if (list != null && !list.isEmpty()) {
                RecoverInfo recoverInfo = list.get(0);
                //phuonghc 15062020 Modify show information of action_type and reason => basing on locale
                List<ApParam> apParamAction = new ApParamSupplier().findParam(cmSession, null, recoverInfo.getActionType() + "_" + locale, null);
                if (!apParamAction.isEmpty()) {
                    String paramNameActionType = apParamAction.get(0).getParamName();
                    List<ApParam> apParam = new ApParamSupplier().findParam(cmSession, apParamAction.get(0).getParamCode(), recoverInfo.getReason(), null);
                    recoverInfo.setActionType(paramNameActionType);
                    if (!apParam.isEmpty()) {
                        String paramNameReason = apParam.get(0).getParamName();
                        recoverInfo.setReason(paramNameReason);
                    }
                }
                result.setRecoverInfo(recoverInfo);
            }


            return result;
        } catch (Throwable ex) {
            rollBackTransactions(paymentSession, cmSession, imSession);
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ServicePaymentOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            commitTransactions(paymentSession, cmSession, imSession);
            closeSessions(paymentSession, cmSession, imSession);
            LogUtils.info(logger, "PaymentController.getCustomerDebit:result=" + LogUtils.toJson(result));
        }
    }

    public PayAdvanceBillOut paymentForService(HibernateHelper hibernateHelper, PaymentInput payIn, String locale) {
        PayAdvanceBillOut result = null;
        Session paymentSession = null;
        Session imSession = null;
        Session cmPosSession = null;
        boolean error = false;
        if (payIn == null) {
            result = new PayAdvanceBillOut(Constants.ERROR_CODE_1, "Input null");
        }
        try {
            HashMap paymentInfo = new HashMap();
            paymentSession = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            Staff staff = new StaffBussiness().findById(cmPosSession, payIn.getStaffId());
            /*check limit cong no cua nhan vien va xem co giao dich nao nhan vien chua nop tien ve metfone*/
            if (Constants.SHOP_TYPE_AGENT_DELEGATE.equals(staff.getType())) {
                if (!new DebitBusiness().checkLockTransaction(imSession, staff.getStaffId(), Double.valueOf(payIn.getTransferCurrency()),"USD")) {
                    result = new PayAdvanceBillOut(Constants.ERROR_CODE_1, "You are over limit or transaction is expred, please transfer money to Metfone");
                    error = false;
                    return result;
                }
            }
            String resultOut = new PaymentDAO().paymentForServicePage(paymentSession, imSession, payIn.getInvoiceId(), payIn.getStaffId(),
                    payIn.getLoginName(), payIn.getShopId(), payIn.getContractId(),
                    payIn.getTransferCurrency(), payIn.getReceiptDate(), payIn.getStaffCode(), payIn.getShopCode(), paymentInfo);
            if (!"".equals(resultOut) && resultOut != null) {
                result = new PayAdvanceBillOut(Constants.ERROR_CODE_1, resultOut);
                error = false;
                return result;
            }
            result = new PayAdvanceBillOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));

            /*set infor for staff*/
            com.viettel.im.database.DAO.AccountAgentDAO accAgentDAO = new com.viettel.im.database.DAO.AccountAgentDAO();
            accAgentDAO.setSession(imSession);
            AccountAgent accAgent = accAgentDAO.findByIdOwner(payIn.getStaffId(), Constant.OBJECT_TYPE_STAFF, Constant.STATUS_USE);

            StaffInfo staffInfo = new StaffInfo();
            if (accAgent != null) {
                staffInfo.setIsdn(accAgent.getIsdn());
                staffInfo.setStaffName(accAgent.getOwnerName());
            } else {
                staffInfo.setIsdn(staff.getIsdn());
                staffInfo.setStaffName(staff.getName());
            }
            result.setLstStaffInfo(staffInfo);
            /*Set customer info and payment*/
            CustomerPayment cusInfo = new CustomerPayment();
            cusInfo.setCustomerName(paymentInfo.containsKey("CustomerName") ? paymentInfo.get("CustomerName").toString() : "");
            cusInfo.setIsdn(paymentInfo.containsKey("ISDN") ? paymentInfo.get("ISDN").toString() : "");
            cusInfo.setContractNo(paymentInfo.containsKey("ContractNo") ? paymentInfo.get("ContractNo").toString() : "");
            cusInfo.setTinNo(paymentInfo.containsKey("TinNo") ? paymentInfo.get("TinNo").toString() : "");
            cusInfo.setPayMethod("Single Payment");
            cusInfo.setPayAmount(payIn.getTransferCurrency());
            cusInfo.setDiscount("0");
            cusInfo.setTotalAmount(payIn.getTransferCurrency());
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            cusInfo.setCreateDateStr(dateFormat.format(new Date()));
            //cusInfo.setReason((paymentInfo.containsKey("DateBillCycle") ? "Bill cycle from " + paymentInfo.get("DateBillCycle").toString() : ""));

            result.setCustomerPayment(cusInfo);

            error = true;
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new PayAdvanceBillOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            error = false;
            return result;
        } finally {
            if (error) {
                commitTransactions(paymentSession, imSession);
            } else {
                rollBackTransactions(paymentSession, imSession);
            }
            closeSessions(paymentSession, imSession, cmPosSession);
            LogUtils.info(logger, "PaymentController.getCustomerDebit:result=" + LogUtils.toJson(result));
        }
    }

    public PayAdvanceBillOut payAdvance(HibernateHelper hibernateHelper, String locale, Long reasonId, Long staffId, Long contractId, Long invoiceListId, String isdn) {

        PayAdvanceBillOut result = null;
        Session cmPosSession = null;
        Session imSession = null;
        Session paymentSession = null;
        boolean error = false;
        try {
            paymentSession = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            result = new PaymentBussiness().payAdvance(paymentSession, imSession, cmPosSession, reasonId, staffId, contractId, invoiceListId, isdn, null, null, null);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new PayAdvanceBillOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            error = true;
            return result;
        } finally {
            if (!error) {
                commitTransactions(paymentSession, imSession, cmPosSession);
            } else {
                rollBackTransactions(paymentSession, imSession, cmPosSession);
            }
            closeSessions(paymentSession, imSession, cmPosSession);
            LogUtils.info(logger, "PaymentController.getCustomerDebit:result=" + LogUtils.toJson(result));
        }
    }

    public ServicePaymentOut searchServicePayment(HibernateHelper hibernateHelper, String isdn) {
        ServicePaymentOut result = null;
        Session paymentSession = null;
        Session prodcutSession = null;
        Session cmSession = null;
        Session imSession = null;
        String locale = "en_US";
        try {
            paymentSession = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            prodcutSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            if (isdn != null) {
                if (isdn.startsWith("0")) {
                    isdn = isdn.replaceFirst("0", "");
                }
                if (isdn.startsWith("855")) {
                    isdn = isdn.replaceFirst("855", "");
                }
            }
            /*Find contractId by ISDN*/
            String sql = "select contract_id  from payment.subscriber where isdn = ? and status in (2, 7) ";
            Query query = paymentSession.createSQLQuery(sql);
            query.setParameter(0, isdn);
            List list = query.list();
            if (list == null || list.isEmpty()) {
                return new ServicePaymentOut(Constants.ERROR_CODE_EMONEY_004, "Can not find contract in payment for " + isdn);
            }
            Long contractId = Long.parseLong(list.get(0).toString());

            ServicePayment service = new PaymentDAO().searchServicePayment(paymentSession, imSession, cmSession, null, null, contractId);
            if (!"".equals(service.getMssError()) && service.getMssError() != null) {
                result = new ServicePaymentOut(Constants.ERROR_CODE_EMONEY_005, service.getMssError());
                return result;
            }
            result = new ServicePaymentOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), service);

            /*Get reason payadvane*/
            List<Reason> reasons = new ReasonSupplier().getReasonPayAdvanceEmoney(cmSession, prodcutSession, isdn);
            result.setReasons(reasons);

            return result;
        } catch (Throwable ex) {
            rollBackTransactions(paymentSession, cmSession, imSession);
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ServicePaymentOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            commitTransactions(paymentSession, cmSession, imSession);
            closeSessions(paymentSession, cmSession, imSession, prodcutSession);
            LogUtils.info(logger, "PaymentController.searchServicePayment:result=" + LogUtils.toJson(result));
        }
    }

    public ParamOut payAdvanceSub(HibernateHelper hibernateHelper, String locale, DataTopUpPinCode data, String staffCode) {

        PayAdvanceBillOut result = null;
        Session cmPosSession = null;
        Session cmPreSession = null;
        Session imSession = null;
        Session paymentSession = null;
        Session prodcutSession = null;
        boolean error = false;
        ParamOut paramOut = new ParamOut();
        try {
            paymentSession = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            imSession = hibernateHelper.getSession(Constants.SESSION_IM);
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            cmPreSession = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            prodcutSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            Staff staff = new StaffDAO().findByCode(cmPosSession, staffCode);
            DecimalFormat df = new DecimalFormat("####0.0000");
            if (staff == null) {
                paramOut.setErrorCode(Constants.ERROR_CODE_1);
                paramOut.setErrorDecription("staff " + staffCode + " is not exists");
                return paramOut;
            }
            Shop shop = new ShopDAO().findById(cmPosSession, staff.getShopId());
            if (shop == null) {
                paramOut.setErrorCode(Constants.ERROR_CODE_1);
                paramOut.setErrorDecription("Shop is not exists");
                return paramOut;
            }
            if (data.getAmount() == null || data.getAmount() <= 0) {
                paramOut.setErrorCode(Constants.ERROR_CODE_1);
                paramOut.setErrorDecription("Amount is not null");
                return paramOut;
            }
            if (data.getIsdn() == null || data.getIsdn().trim().isEmpty()) {
                paramOut.setErrorCode(Constants.ERROR_CODE_1);
                paramOut.setErrorDecription("ISDN is not null");
                return paramOut;
            }
            if (data.getTid() == null || data.getTid().trim().isEmpty()) {
                paramOut.setErrorCode(Constants.ERROR_CODE_1);
                paramOut.setErrorDecription("TID is not null");
                return paramOut;
            }
            if (data.getTime() == null || data.getTime().trim().isEmpty()) {
                paramOut.setErrorCode(Constants.ERROR_CODE_1);
                paramOut.setErrorDecription("Time is not null");
                return paramOut;
            } else {
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    Date date = formatter.parse(data.getTime());
                    long range = Math.abs(new Date().getTime() - date.getTime());
                    if (range >= 15 * 60 * 1000) {
                        paramOut.setErrorCode(Constants.ERROR_CODE_1);
                        paramOut.setErrorDecription("Date time is expired");
                        return paramOut;
                    }
                } catch (ParseException ex) {
                    paramOut.setErrorCode(Constants.ERROR_CODE_1);
                    paramOut.setErrorDecription("Date time is expired " + ex.getMessage());
                    return paramOut;
                }
            }
            String temp = data.getIsdn();
            if (temp.startsWith("0")) {
                temp = temp.replaceFirst("0", "");
            }
            if (temp.startsWith("855")) {
                temp = temp.replaceFirst("855", "");
            }
            data.setIsdn(temp);
            /*Find contractId by ISDN*/
            String sql = "select contract_id from payment.subscriber where isdn = ? and status in (2, 7)";
            Query query = paymentSession.createSQLQuery(sql);
            query.setParameter(0, data.getIsdn());
            List list = query.list();
            if (list == null || list.isEmpty()) {
                return new ParamOut(Constants.ERROR_CODE_EMONEY_004, "Can not find contract in payment for " + data.getIsdn());
            }
            Long contractId = Long.parseLong(list.get(0).toString());

            sql = "select TEL_MOBILE  from payment.contract where contract_id = ? and status in (2, 7)";
            query = paymentSession.createSQLQuery(sql);
            query.setParameter(0, contractId);
            list = query.list();
            String tidTemp = data.getTid().trim().replaceFirst("IN", "");
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("tid", tidTemp);
            map.put("paymentMethod", "09");
            if (data.getReasonId() == null) {
                ServicePayment service = new PaymentDAO().searchServicePayment(paymentSession, imSession, cmPosSession, null, null, contractId);
                Double debt = service.getTransferCurrency() == null || service.getTransferCurrency().isEmpty() ? 0.0d : Double.parseDouble(service.getTransferCurrency().replaceAll(",", ""));

                if (debt.compareTo(data.getAmount()) >= 0) {
                    /*Gach no don le*/
                    String resultOut = new PaymentDAO().paymentForServicePage(paymentSession, imSession, null, staff.getStaffId(),
                            staff.getStaffCode(), staff.getShopId(), contractId,
                            data.getAmount().toString(), data.getTime().split(" ")[0], staff.getStaffCode(), shop.getShopCode(), map);
                    if (!"".equals(resultOut) && resultOut != null) {
                        error = true;
                        return new ParamOut(Constants.ERROR_CODE_EMONEY_006, resultOut);
                    }
                    if (list != null && !list.isEmpty() && list.get(0) != null) {
                        List<String> listParam = new ArrayList<String>();
                        listParam.add(data.getIsdn());
                        listParam.add(data.getAmount().toString());
                        sendSMS(cmPreSession, list.get(0).toString(), Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.PARAM_TYPE_PAY_SINGLE_EMONEY, listParam);
                    }
                } else {
                    Double remain = data.getAmount() - debt;
                    Double amount = debt;

                    List<Reason> reasons = new ReasonSupplier().getReasonPayAdvanceEmoney(cmPosSession, prodcutSession, data.getIsdn());
                    Reason select = null;
                    if (reasons != null && !reasons.isEmpty()) {
                        sql = "SELECT   ROUND (c.daily_charge * 30 * 1.1), "
                                + "         ROUND ( (c.daily_charge * 30) * 1.1 "
                                + "                - NVL ( (SELECT   ROUND (amount * 1.1, 5) "
                                + "                           FROM   contract_discount_adsl_ll s "
                                + "                          WHERE   s.contract_id = b.contract_id"
                                + "                          and effect_from <= trunc(sysdate) and nvl(until, sysdate) >= trunc(sysdate)), 0)) "
                                + "             afterdis "
                                + "  FROM   rt_subscriber a, "
                                + "         payment.subscriber b, "
                                + "         rt_daily_charge_config c "
                                + " WHERE       a.rt_subscriber_id = b.sub_id "
                                + "         AND b.isdn = ? "
                                + "         AND a.price_plan_code = c.product_code";
                        query = cmPosSession.createSQLQuery(sql);
                        query.setParameter(0, data.getIsdn());
                        list = query.list();
                        if (list != null && !list.isEmpty()) {
                            Object[] arr = (Object[]) list.get(0);
                            Double beforeDis = arr[0] != null ? Double.parseDouble(arr[0].toString()) : 0d;
                            Double afterDis = arr[1] != null ? Double.parseDouble(arr[1].toString()) : 0d;
                            if (beforeDis > 0 && afterDis > 0) {
                                Double percent = beforeDis / afterDis;
                                for (Reason r : reasons) {
                                    r.setPayAdvAmount(Double.valueOf(df.format(r.getPayAdvAmount() / percent)));
                                    r.setPromotion(Double.valueOf(df.format(r.getPromotion() / percent)));
                                }
                            }
                        }
                        for (Reason reason : reasons) {
                            if (remain.compareTo(reason.getPayAdvAmount()) >= 0) {
                                remain = remain - reason.getPayAdvAmount();
                                select = reason;
                                break;
                            }
                        }
                    }
                    if (select == null) {
                        amount = amount + remain;
                        String resultOut = new PaymentDAO().paymentForServicePage(paymentSession, imSession, null, staff.getStaffId(),
                                staff.getStaffCode(), staff.getShopId(), contractId,
                                String.valueOf(amount), data.getTime().split(" ")[0], staff.getStaffCode(), shop.getShopCode(), map);
                        if (!"".equals(resultOut) && resultOut != null) {
                            error = true;
                            return new ParamOut(Constants.ERROR_CODE_EMONEY_006, resultOut);
                        }
                        if (list != null && !list.isEmpty() && list.get(0) != null) {
                            List<String> listParam = new ArrayList<String>();
                            listParam.add(data.getIsdn());
                            listParam.add(String.valueOf(amount));
                            sendSMS(cmPreSession, list.get(0).toString(), Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.PARAM_TYPE_PAY_SINGLE_EMONEY, listParam);
                        }
                    } else {
                        amount = amount + remain;
                        result = new PaymentBussiness().payAdvance(paymentSession, imSession, cmPosSession, select.getReasonId(), staff.getStaffId(), contractId, null, data.getIsdn(), tidTemp, select.getPayAdvAmount(), select.getPromotion());
                        paramOut.setErrorCode(result.getErrorCode());
                        paramOut.setErrorDecription(result.getErrorDecription());
                        if (Constants.ERROR_CODE_1.equals(result.getErrorCode())) {
                            error = true;
                            paramOut.setErrorCode(Constants.ERROR_CODE_EMONEY_007);
                            return paramOut;
                        }
                        if (amount > 0) {
                            String resultOut = new PaymentDAO().paymentForServicePage(paymentSession, imSession, null, staff.getStaffId(),
                                    staff.getStaffCode(), staff.getShopId(), contractId,
                                    amount.toString(), data.getTime().split(" ")[0], staff.getStaffCode(), shop.getShopCode(), map);
                            if (!"".equals(resultOut) && resultOut != null) {
                                error = true;
                                return new ParamOut(Constants.ERROR_CODE_EMONEY_006, resultOut);
                            }
                        }

                        if (list != null && !list.isEmpty() && list.get(0) != null) {
                            List<String> listParam = new ArrayList<String>();
                            if (amount > 0) {
                                listParam.add(data.getIsdn());
                                listParam.add(String.valueOf(amount));
                                sendSMS(cmPreSession, list.get(0).toString(), Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.PARAM_TYPE_PAY_SINGLE_EMONEY, listParam);
                            }
                            listParam = new ArrayList<String>();
                            listParam.add(data.getIsdn());
                            listParam.add(select.getPayAdvAmount().toString());
                            listParam.add(select.getName());
                            sendSMS(cmPreSession, list.get(0).toString(), Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.PARAM_TYPE_PAY_ADVANCE_EMONEY, listParam);
                        }
                    }
                }
                return new ParamOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            } else {
                ServicePayment service = new PaymentDAO().searchServicePayment(paymentSession, imSession, cmPosSession, null, null, contractId);
                /*Gach no pay advance*/
                Reason reason = new ReasonDAO().findById(cmPosSession, data.getReasonId());
                if (reason == null) {
                    return new ParamOut(Constants.ERROR_CODE_1, "Can not find reason pay advance");
                }
                if (data.getAmount() < reason.getPayAdvAmount() + (service.getPreviousDebt() == null ? 0d : Double.parseDouble(service.getPreviousDebt()))) {
                    return new ParamOut(Constants.ERROR_CODE_1, "Min amount to pay advance is " + (reason.getPayAdvAmount() + Double.parseDouble(service.getPreviousDebt())));
                }
                
                result = new PaymentBussiness().payAdvance(paymentSession, imSession, cmPosSession, data.getReasonId(), staff.getStaffId(), contractId, null, data.getIsdn(), tidTemp, null, null);
                paramOut.setErrorCode(result.getErrorCode());
                paramOut.setErrorDecription(result.getErrorDecription());
                if (Constants.ERROR_CODE_1.equals(result.getErrorCode())) {
                    error = true;
                    paramOut.setErrorCode(Constants.ERROR_CODE_EMONEY_007);
                    return paramOut;
                } else {
                    /*Gach no don le phan du neu so tien lon hon pay advance*/
                    if (data.getAmount() > reason.getPayAdvAmount()) {
                        Double changeMoney = data.getAmount() - reason.getPayAdvAmount();
                        String resultOut = new PaymentDAO().paymentForServicePage(paymentSession, imSession, null, staff.getStaffId(),
                                staff.getStaffCode(), staff.getShopId(), contractId,
                                changeMoney.toString(), data.getTime().split(" ")[0], staff.getStaffCode(), shop.getShopCode(), map);
                        if (!"".equals(resultOut) && resultOut != null) {
                            error = true;
                            return new ParamOut(Constants.ERROR_CODE_EMONEY_006, resultOut);
                        }
                        if (list != null && !list.isEmpty() && list.get(0) != null) {
                            List<String> listParam = new ArrayList<String>();
                            listParam.add(data.getIsdn());
                            listParam.add(data.getAmount().toString());
                            listParam.add(reason.getName());
                            listParam.add(changeMoney.toString());
                            sendSMS(cmPreSession, list.get(0).toString(), Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.PARAM_TYPE_PAY_EMONEY, listParam);
                        }
                        return new ParamOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
                    } else {
                        if (list != null && !list.isEmpty() && list.get(0) != null) {
                            List<String> listParam = new ArrayList<String>();
                            listParam.add(data.getIsdn());
                            listParam.add(data.getAmount().toString());
                            listParam.add(reason.getName());
                            sendSMS(cmPreSession, list.get(0).toString(), Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.PARAM_TYPE_PAY_ADVANCE_EMONEY, listParam);
                        }
                    }
                }

            }

        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new PayAdvanceBillOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            error = true;
        } finally {
            if (!error) {
                commitTransactions(paymentSession, imSession, cmPosSession, cmPreSession);
            } else {
                rollBackTransactions(paymentSession, imSession, cmPosSession, cmPreSession);
            }
            closeSessions(paymentSession, imSession, cmPosSession, cmPreSession, prodcutSession);
            LogUtils.info(logger, "PaymentController.payAdvanceSub:result=" + LogUtils.toJson(result));
        }
        return paramOut;
    }

    public ParamOut confirmPayAdvance(HibernateHelper hibernateHelper, String staffCode, String tid, String date) {
        Session seessionMerchant = null;
        ParamOut result = new ParamOut();
        result.setErrorCode(Constants.ERROR_CODE_1);
        try {
            seessionMerchant = hibernateHelper.getSession(Constants.SESSION_MERCHANT);

            try {
                //result.setErrorCode(Constants.ERROR_CODE_0);
                String resultTopUp = new TransactionLogDAO().confimTopup(seessionMerchant, tid, date, staffCode);
                if (resultTopUp != null) {
                    result.setErrorCode(resultTopUp);
                }
            } catch (Exception ex) {
                result.setErrorDecription(ex.getMessage());
            }
        } catch (Exception ex) {
            result.setErrorCode("99");
            result.setErrorDecription(ex.getMessage());
            logger.error("confirmPayAdvance: " + ex.getMessage());
        } finally {
            closeSessions(seessionMerchant);
        }
        return result;
    }

    public void sendSMS(Session cmPre, String isdn, String paramType, String paramCode, List<String> listParam) {
        CallableStatement cs = null;
        try {
            if (isdn.startsWith("0")) {
                isdn = isdn.replaceFirst("0", "");
            }
            if (isdn.startsWith("855")) {
                isdn = isdn.replaceFirst("855", "");
            }
            IsdnSendSmsDAO isdnDao = new IsdnSendSmsDAO();
            List<String> listContentSms = isdnDao.genarateSMSContent(cmPre, null, paramType, paramCode, listParam);
            cs = cmPre.connection().prepareCall("{call CM_PRE2.send_sms_alert(?, ?, ?)}");
            for (String content : listContentSms) {

                cs.setString(1, isdn);
                cs.setString(2, "Metfone");
                cs.setString(3, content);
                cs.execute();

            }
        } catch (Exception ex) {
            logger.error("sendSMS: " + ex.getMessage());
        } finally {
            try {
                if (cs != null) {
                    cs.close();
                }
            } catch (Exception ex) {
            }
        }
    }

    public PayAdvanceBillOut printInvoiceBluetooth(HibernateHelper hibernateHelper, String locale, String accBundleTv) {
        PayAdvanceBillOut result = new PayAdvanceBillOut();
        Session cmPosSession = null;
        try {
//            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            SubBundleTv tv = new SubBundleTvDAO().findBundleTvByAccount(cmPosSession, accBundleTv);
            if (tv == null) {
                result = new PayAdvanceBillOut(Constants.ERROR_CODE_1, "Account bundle tv is not exists");
                return result;
            }
            Staff staff = new StaffDAO().findByCode(cmPosSession, tv.getCreateUser());
            result.setStaff(staff);
            SubAdslLeaseline sub = new SubAdslLeaselineDAO().findById(cmPosSession, tv.getSubId());
            com.viettel.bccs.api.Task.BO.Contract contract = new ContractDAO().findById(sub.getContractId(), cmPosSession);
            Customer customer = new CustomerDAO().findById(cmPosSession, contract.getCustId());
            result.setCustomer(customer);
            CustomerPayment invoice = new CustomerPayment();
            invoice.setAccBundleTv(tv.getAccount());
            invoice.setCreateDate(tv.getCreateDate());
            invoice.setProduct(tv.getProductCode());
            Reason r = new ReasonDAO().findById(cmPosSession, tv.getRegReasonId());
            invoice.setReason(r.getName());
            invoice.setTotalAmount(r.getPayAdvAmount() != null ? r.getPayAdvAmount().toString() : "0");
            result.setInvoice(invoice);
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new PayAdvanceBillOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(cmPosSession);
            LogUtils.info(logger, "PaymentController.getCustomerDebit:result=" + LogUtils.toJson(result));
        }
        return result;
    }

    public ParamOut getPaymentReport(HibernateHelper hibernateHelper, String locale, String codeReport, String staffCode, boolean isChiefCenter) {
        ParamOut result = new ParamOut();
        Session session = null;
        Session sessionPayment = null;
        try {
            //Open session
            session = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            sessionPayment = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            //Check staff of reporter is valid of not
            Staff staffDto = new StaffDAO().findByCode(session, staffCode);
            if (staffDto == null) {
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription(LabelUtil.getKey("staff.code.invalid", locale));
                return result;
            }
            Shop shopReporter = new ShopDAO().findById(session, staffDto.getShopId());
            // Check reporter donot input codeReport is level VTC or not
            if (StringUtils.isEmpty(codeReport)) {
                if (!Constants.USER_LEVEL_VTC.equals(shopReporter.getShopType())) {
                    result.setErrorCode(Constants.ERROR_CODE_1);
                    result.setErrorDecription(LabelUtil.getKey("do.not.have.permission.report", locale));
                    return result;
                } else {
                    List<PaymentReport> paymentReportList = new PaymentBussiness().getReportPaymentByUserLevel(sessionPayment, EMPTY_STR, EMPTY_STR, EMPTY_STR);
                    result.setErrorCode(Constants.ERROR_CODE_0);
                    result.setErrorDecription(LabelUtil.getKey("common.success", locale));
                    result.setPaymentReport(paymentReportList);
                    return result;
                }
            } else {
                //Find Shop information from codeReport
                Shop shopReport = new ShopDAO().findByCode(session, codeReport);
                Staff staffReport = new Staff();
                if (shopReport == null) {
                    //Shop not available => Find Staff information from codeReport
                    staffReport = new StaffDAO().findByCode(session, codeReport);
                    if (staffReport == null) {
                        result.setErrorCode(Constants.ERROR_CODE_1);
                        result.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                        return result;
                    } else {
                        shopReport = new ShopDAO().findById(session, staffReport.getShopId());
                    }
                }
                boolean isValidUser = true;
                if (!staffCode.equals(codeReport)) {
                    isValidUser = new PaymentBussiness().validatePermissionReport(shopReporter, codeReport, shopReport, isChiefCenter);
                }
                if (!isValidUser) {
                    result.setErrorCode(Constants.ERROR_CODE_1);
                    result.setErrorDecription(LabelUtil.getKey("do.not.have.permission.report", locale));
                    return result;
                }

                List<String> categorySearch = new PaymentBussiness().getCategorySearchFromShopReport(session, shopReport, staffReport);
                if (categorySearch.size() > 3) {
                    result.setErrorCode(Constants.ERROR_CODE_1);
                    result.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                    return result;
                }

                List<PaymentReport> paymentReportList = new PaymentBussiness().getReportPaymentByUserLevel(sessionPayment, categorySearch.get(0), categorySearch.get(1), categorySearch.get(2));
                result.setErrorCode(Constants.ERROR_CODE_0);
                result.setErrorDecription(LabelUtil.getKey("common.success", locale));
                result.setPaymentReport(paymentReportList);
                return result;
            }
        } catch (Exception e) {
            logger.error("Error occured: ", e);
            result.setErrorCode(Constants.ERROR_CODE_1);
            result.setErrorDecription("The system is busy! Please try again later");
            return result;
        } finally {
            closeSessions(session);
            closeSessions(sessionPayment);
            LogUtils.info(logger, "PaymentController.getPaymentReport:result=" + LogUtils.toJson(result));
        }
    }

    public ParamOut getUnpaidDetail(HibernateHelper hibernateHelper, String locale, String codeReport, String staffCode, boolean isChiefCenter) {
        ParamOut result = new ParamOut();//
        Session session = null;
        Session sessionPayment = null;
        try {
            session = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            //Open session
            sessionPayment = hibernateHelper.getSession(Constants.SESSION_PAYMENT);
            //Check staff of reporter is valid of not
            Staff staffDto = new StaffDAO().findByCode(session, staffCode);
            if (staffDto == null) {
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription(LabelUtil.getKey("staff.code.invalid", locale));
                return result;
            }
            Shop shopReporter = new ShopDAO().findById(session, staffDto.getShopId());
            // Check report donot input coReport is level VTC or not
            if (StringUtils.isEmpty(codeReport)) {
                if (!Constants.USER_LEVEL_VTC.equals(shopReporter.getShopType())) {
                    result.setErrorCode(Constants.ERROR_CODE_1);
                    result.setErrorDecription(LabelUtil.getKey("do.not.have.permission.report", locale));
                    return result;
                } else {
                    List<UnpaidPaymentDetailExpand> paymentReportList = new PaymentBussiness().getListUnpaidDetailByUserLevel(session, sessionPayment, EMPTY_STR, EMPTY_STR, EMPTY_STR);
                    result.setErrorCode(Constants.ERROR_CODE_0);
                    result.setErrorDecription(LabelUtil.getKey("common.success", locale));
                    result.setCustomer(paymentReportList);
                    return result;
                }
            } else {
                //Find Shop information from codeReport
                Shop shopReport = new ShopDAO().findByCode(session, codeReport);
                Staff staffReport = new Staff();
                if (shopReport == null) {
                    //Shop not available => Find Staff information from codeReport
                    staffReport = new StaffDAO().findByCode(session, codeReport);
                    if (staffReport == null) {
                        result.setErrorCode(Constants.ERROR_CODE_1);
                        result.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                        return result;
                    } else {
                        shopReport = new ShopDAO().findById(session, staffReport.getShopId());
                    }
                }
                boolean isValidUser = true;
                if (!staffCode.equals(codeReport)) {
                    isValidUser = new PaymentBussiness().validatePermissionReport(shopReporter, codeReport, shopReport, isChiefCenter);
                }

                if (!isValidUser) {
                    result.setErrorCode(Constants.ERROR_CODE_1);
                    result.setErrorDecription(LabelUtil.getKey("do.not.have.permission.report", locale));
                    return result;
                }

                List<String> categorySearch = new PaymentBussiness().getCategorySearchFromShopReport(session, shopReport, staffReport);
                if (categorySearch.size() > 3) {
                    result.setErrorCode(Constants.ERROR_CODE_1);
                    result.setErrorDecription(LabelUtil.getKey("common.find.nothing", locale));
                    return result;
                }

                List<UnpaidPaymentDetailExpand> unpaidPaymentList = new PaymentBussiness().getListUnpaidDetailByUserLevel(session, sessionPayment, categorySearch.get(0), categorySearch.get(1), categorySearch.get(2));
                result.setErrorCode(Constants.ERROR_CODE_0);
                result.setErrorDecription(LabelUtil.getKey("common.success", locale));
                result.setCustomer(unpaidPaymentList);
                return result;
            }
        } catch (Exception e) {
            logger.error("Error occured: ", e);
            result.setErrorCode(Constants.ERROR_CODE_1);
            result.setErrorDecription("The system is busy! Please try again later");
            return result;
        } finally {
            closeSessions(session);
            closeSessions(sessionPayment);
            LogUtils.info(logger, "PaymentController.getUnpaidDetail:result=" + LogUtils.toJson(result));
        }
    }
}
