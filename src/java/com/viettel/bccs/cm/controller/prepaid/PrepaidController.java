/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.controller.prepaid;

import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.bean.common.MessageError;
import com.viettel.bccs.cm.common.util.pre.Constant;
import com.viettel.bccs.cm.controller.BaseController;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.database.DAO.pre.ApDomainDAO;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.pre.ApParamPre;
import com.viettel.bccs.cm.model.pre.ReasonPre;
import com.viettel.bccs.cm.model.pre.SubMbPre;
import com.viettel.brcd.dao.pre.SubMbPreDAO;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.ws.model.input.ChangeSimInput;
import com.viettel.brcd.ws.model.input.ImageInput;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.brcd.ws.supplier.nims.pre.ChangeSim4GResponse;
import com.viettel.brcd.ws.supplier.nims.pre.CreateNewInfoCusResponse;
import com.viettel.brcd.ws.supplier.nims.pre.PrepaidService;
import com.viettel.brcd.ws.supplier.nims.pre.VerifyChangeSimResponse;
import com.viettel.brcd.ws.supplier.nims.pre.verifyChangeKitResponse;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import com.viettel.im.database.DAO.WebServiceIMDAO;
import com.viettel.im.database.BO.StockSim;
import com.viettel.bccs.cm.supplier.BaseSupplier;
import org.apache.commons.beanutils.BeanUtils;
import java.util.Date;
import com.viettel.brcd.dao.pre.ApParamDAO;
import com.viettel.brcd.dao.pre.ReasonPreDAO;
import java.util.Calendar;
import com.viettel.im.database.BO.StockSimBean;
import org.apache.log4j.Logger;
import com.viettel.bccs.cm.dao.interfaceCommon.InterfaceCommon;
import com.viettel.bccs.cm.supplier.SubFbConfigSupplier;
import com.viettel.bccs.supplier.cm.pre.PreChangeSimSupplier;
import com.viettel.brcd.common.util.FileSaveProcess;
import com.viettel.brcd.common.util.InterfacePr;
import com.viettel.brcd.dao.pre.ActionLog;
import com.viettel.brcd.dao.pre.SubSimMbPreDAO;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.common.OriginalViettelMsg;
import com.viettel.common.ViettelService;
import com.vtc.provisioning.client.Service;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 *
 * @author cuongdm
 */
public class PrepaidController extends BaseController {

    public PrepaidController() {
        logger = Logger.getLogger(PrepaidController.class);
    }

    public ParamOut verifyChangeKit(HibernateHelper hibernateHelper, String locale, String msisdnAgent, String msisdnCust, String newMsisdnCust, String lastSerial) {
        ParamOut reponse = new ParamOut("1", "");
        try {
            verifyChangeKitResponse response = PrepaidService.verifyChangeKit(msisdnAgent, msisdnCust, newMsisdnCust, lastSerial);
            if ((response == null) || (response.getReturn() == null) || (response.getReturn().getErrorCode() == null)) {
                reponse.setErrorDecription("call service gw failed");
            } else {
                if ("00".equals(response.getReturn().getErrorCode())) {
                    reponse.setErrorCode("0");
                }
                reponse.setErrorDecription(response.getReturn().getDescription());
                reponse.setTranId(response.getReturn().getTranId());
            }
        } catch (Exception ex) {
            reponse.setErrorDecription(ex.getMessage());
        }
        return reponse;
    }

    public ParamOut verifyChangeSim(String locale, String msisdnAgent, String msisdnCust, String lastSerial) {
        ParamOut reponse = new ParamOut("1", "");
        try {
            VerifyChangeSimResponse response = PrepaidService.verifyChangeSim(msisdnAgent, msisdnCust, lastSerial);
            if ((response == null) || (response.getReturn() == null) || (response.getReturn().getErrorCode() == null)) {
                reponse.setErrorDecription("call service gw failed");
            } else {
                if ("00".equals(response.getReturn().getErrorCode())) {
                    reponse.setErrorCode("0");
                }
                reponse.setErrorDecription(response.getReturn().getDescription());
                reponse.setTranId(response.getReturn().getTranId());
            }
        } catch (Exception ex) {
            reponse.setErrorDecription(ex.getMessage());
        }
        return reponse;
    }

    public WSRespone changeSim4G(HibernateHelper hibernateHelper, String locale, String pinCode, String transId) {
        WSRespone reponse = new WSRespone("1");
        try {
            ChangeSim4GResponse response = PrepaidService.changeSim4G(pinCode, transId);
            if ((response == null) || (response.getReturn() == null) || (response.getReturn().getErrorCode() == null)) {
                reponse.setErrorDecription("call service gw failed");
            } else {
                if ("00".equals(response.getReturn().getErrorCode())) {
                    reponse.setErrorCode("0");
                }
                reponse.setErrorDecription(response.getReturn().getDescription());
            }
        } catch (Exception ex) {
            reponse.setErrorDecription(ex.getMessage());
        }
        return reponse;
    }

    public WSRespone createNewInfoCus(String token, String serial, String isdn, String dealerIsdn, String idType, String fullName, String idNumber, String dob, List<ImageInput> lstImage, String puk) {
        WSRespone reponse = new WSRespone("1");
        try {
            CreateNewInfoCusResponse response = PrepaidService.createNewInfoCus(token, serial, isdn, dealerIsdn, idType, fullName, idNumber, dob, lstImage, puk);
            if ((response == null) || (response.getReturn() == null) || (response.getReturn().getErrorCode() == null)) {
                reponse.setErrorDecription("call service gw failed");
            } else {
                if ("0".equals(response.getReturn().getErrorCode())) {
                    reponse.setErrorCode("0");
                }
                reponse.setErrorDecription(response.getReturn().getErrorDecription());
            }
        } catch (Exception ex) {
            reponse.setErrorDecription(ex.getMessage());
        }
        return reponse;
    }

    public ParamOut checkChangeSim(HibernateHelper hibernateHelper, String isdn, String idNo) {
        ParamOut reponse = new ParamOut();
        reponse.setErrorCode(Constants.ERROR_CODE_1);
        Session cmPre = null;
        try {
            if (isdn != null) {
                if (isdn.startsWith("0")) {
                    isdn = isdn.replaceFirst("0", "");
                }
                if (isdn.startsWith("855")) {
                    isdn = isdn.replaceFirst("855", "");
                }
            }
            cmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            /*Check thong tin sim, 1 cho doi sim so thuong, 0 la so vip nen khong cho doi*/
            String sql = "SELECT   c.id_no, case when s.vip is null or s.vip != 9 then 1 else 0 end, act_status  "
                    + "  FROM   sub_mb s, customer c "
                    + " WHERE       s.status = 2 "
                    + "         AND s.isdn = ? "
                    + "         AND c.cust_id = s.cust_id";
            Query query = cmPre.createSQLQuery(sql);
            query.setParameter(0, isdn);
            List list = query.list();
            if (list == null || list.isEmpty()) {
                reponse.setErrorDecription("isdn is not normal or not exists");
            } else {
                Object[] arr = (Object[]) list.get(0);
                if (!Constants.STATUS_USE.toString().equals(arr[1] == null ? "" : arr[1].toString())) {
                    reponse.setErrorDecription("This sim is vip, you can not change");
                } else {
                    reponse.setErrorCode(Constants.ERROR_CODE_0);
                    reponse.setErrorDecription("Success");
                    if (idNo != null && idNo.equalsIgnoreCase(arr[0] == null ? "" : arr[0].toString())) {
                        reponse.setIsRightInfor(true);
                    } else {
                        reponse.setIsRightInfor(false);
                    }
                    String actionDo = arr[2] == null ? "" : arr[2].toString();
                    actionDo = actionDo.substring(0, 1);
                    if ("1".equals(actionDo) || "2".equals(actionDo)) {
                        reponse.setIsBlockSim(1l);
                    }
                }
            }

        } catch (Exception ex) {
            reponse.setErrorDecription(ex.getMessage());
            ex.printStackTrace();
        } finally {
            closeSessions(cmPre);
        }
        return reponse;
    }

    public List<ReasonPre> getReasonChangeSim(HibernateHelper hibernateHelper, Long shopId, String isdn) {
        Session cmPre = null;
        Session cmPos = null;
        List<ReasonPre> lstChangeSimReason = new ArrayList();
        try {
            cmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            cmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);

            Shop shop = new ShopDAO().findById(cmPos, shopId);
            if (shop == null) {
            } else {
                String paramType = "";
                if (Constant.SHOP_TYPE_AGENT.equals(shop.getChannelTypeId())) {
                    paramType = Constant.CHANGE_SIM_REASON_CODE_FOR_AGENT;
                } else if (Constant.SHOP_TYPE_AGENT_XNK.equals(shop.getChannelTypeId())) {
                    paramType = Constant.CHANGE_SIM_REASON_CODE_FOR_AGENT_XNK;
                } else if (Constant.SHOP_TYPE_AGENT_DELEGATE.equals(shop.getChannelTypeId())) {
                    paramType = Constant.CHANGE_SIM_REASON_CODE_FOR_AGENT_DELEGATE;
                } else if (Constant.SHOP_TYPE_AGENT_MB.equals(shop.getChannelTypeId())) {
                    paramType = Constant.CHANGE_SIM_REASON_CODE_FOR_AGENT_MB;
                }
                if (isdn != null) {
                    if (isdn.startsWith("0")) {
                        isdn = isdn.replaceFirst("0", "");
                    }
                    if (isdn.startsWith("855")) {
                        isdn = isdn.replaceFirst("855", "");
                    }
                }
                String sql = "select product_code from sub_mb where isdn = ?";
                Query query = cmPre.createSQLQuery(sql);
                query.setParameter(0, isdn);
                List list = query.list();
                if (list != null && !list.isEmpty()) {
                    List<ReasonPre> lstReason = new ArrayList();
                    if (paramType == null || paramType.isEmpty()) /*Theo cua hang*/ {
//                    lstReason = CachePreDB.getReasonByActionCodeForAudit(Constant.ACTION_SUBSCRIBER_CHANGE_SIM);
                        lstReason = getListReasonByActionCodeForAudit(cmPre, Constant.ACTION_SUBSCRIBER_CHANGE_SIM, list.get(0).toString());
                    } else /*Theo loai dai li*/ {
//                    lstReason = CachePreDB.getReasonByActionCodeForAudit(Constant.ACTION_SUBSCRIBER_CHANGE_SIM, paramType);
                        lstReason = getListReasonByActionCodeAndType(cmPre, Constant.ACTION_SUBSCRIBER_CHANGE_SIM, paramType, list.get(0).toString());
                    }

                    lstChangeSimReason = filterReason(lstReason, false);

                }
            }
        } catch (Exception ex) {
            logger.info(ex);
            ex.printStackTrace();
        } finally {
            closeSessions(cmPre, cmPos);
        }
        return lstChangeSimReason;
    }

    public static List<ReasonPre> getListReasonByActionCodeForAudit(Session cmPreSession, String strActionCode, String productCode) {
        List<ReasonPre> lstReason = new ArrayList<ReasonPre>();

        if (!new ApDomainDAO().isMandatoryTrasaction(cmPreSession.connection(), strActionCode)) {
            String strQuery = "SELECT r FROM ReasonPre r, ApDomainPre a WHERE a.id.type = ? "
                    + "AND a.id.code = ? AND a.reasonType = r.type AND a.status = ? AND r.status = ? order by r.name";
            Query query = cmPreSession.createQuery(strQuery);
            query.setParameter(0, Constant.ACTION_FOR_AUDIT);
            query.setParameter(1, strActionCode);
            query.setParameter(2, Constant.STATUS_USE);
            query.setParameter(3, Constant.STATUS_USE);
            lstReason = query.list();
        } else {
            String strQuery = "SELECT r FROM ReasonPre r, MappingPre m WHERE m.actionCode = ? "
                    + "  AND r.reasonId = m.reasonId AND r.status = ? "
                    + "  AND m.status = ? AND (m.productCode = ? or m.productCode is null) order by r.name";

            Query query = cmPreSession.createQuery(strQuery);
            query.setParameter(0, strActionCode);
            query.setParameter(1, Constant.STATUS_USE);
            query.setParameter(2, Constant.STATUS_USE);
            query.setParameter(3, productCode);
            lstReason = query.list();
        }

        return lstReason;
    }

    public static List<ReasonPre> getListReasonByActionCodeAndType(Session cmPreSession, String strActionCode, String type, String productCode) throws Exception {
        List<String> lstReasonCodeForAgent = getListReasonCodeByType(cmPreSession, type);
        StringBuffer strListCode = new StringBuffer();
        if (lstReasonCodeForAgent != null && !lstReasonCodeForAgent.isEmpty()) {
            for (int index = 0; index < lstReasonCodeForAgent.size(); index++) {
                strListCode.append("'" + lstReasonCodeForAgent.get(index) + "'");
                strListCode.append(",");
            }
        }

        List<ReasonPre> lstReason = new ArrayList<ReasonPre>();
        String strQuery = "";
        List lstParam = new ArrayList();

        if (!new ApDomainDAO().isMandatoryTrasaction(cmPreSession.connection(), strActionCode)) {
            strQuery = "SELECT r FROM ReasonPre r, ApDomain a WHERE a.id.type = ? "
                    + " AND a.id.code = ? AND a.reasonType = r.type AND a.status = ? AND r.status = ? ";
            lstParam.add(Constant.ACTION_FOR_AUDIT);
            lstParam.add(strActionCode);
            lstParam.add(Constant.STATUS_USE);
            lstParam.add(Constant.STATUS_USE);
        } else {
            strQuery = "SELECT r FROM ReasonPre r, MappingPre m WHERE m.actionCode = ? "
                    + "  AND r.reasonId = m.reasonId AND r.status = ? "
                    + "  AND m.status = ? AND (m.productCode = ? or m.productCode is null) ";
            lstParam.add(strActionCode);
            lstParam.add(Constant.STATUS_USE);
            lstParam.add(Constant.STATUS_USE);
            lstParam.add(productCode);
        }

        if (strListCode.length() > 0) {
            strListCode.deleteCharAt(strListCode.lastIndexOf(","));

            String strIn = "IN";
            if (Constant.CHANGE_PRODUCT_REASON_CODE_FOR_AGENT_XNK.equals(type)
                    || Constant.CHANGE_SIM_REASON_CODE_FOR_AGENT_XNK.equals(type)) {
                strIn = "NOT IN";
            }

            strQuery += " AND r.code " + strIn + " (" + strListCode.toString() + ") order by r.name";
        }
        strQuery += " order by r.name";

        if (strQuery != null && !strQuery.isEmpty()) {
            Query query = cmPreSession.createQuery(strQuery);
            if (lstParam != null && !lstParam.isEmpty()) {
                for (int i = 0; i < lstParam.size(); i++) {
                    query.setParameter(i, lstParam.get(i));
                }
            }
            lstReason = query.list();
        }
        return lstReason;
    }

    public static List<String> getListReasonCodeByType(Session cmPreSession, String type) throws Exception {
        try {
            String strQuery = "From ApParamPre Where status = ? And paramType = ? And paramCode = ?";
            Query query = cmPreSession.createQuery(strQuery);
            query.setParameter(0, Constant.AP_PARAM_STATUS_USED);
            query.setParameter(1, type);
            query.setParameter(2, type);
            List lstResult = query.list();

            if (lstResult != null && lstResult.size() > 0) {
                List<String> lstReasonCode = new ArrayList<String>();
                ApParamPre apParam = (ApParamPre) lstResult.get(0);
                if (apParam != null) {
                    String strListReasonCode = apParam.getParamValue();
                    if (strListReasonCode != null && !"".equals(strListReasonCode.trim()) && strListReasonCode.indexOf(";") > 0) {
                        String[] arrOfferId = strListReasonCode.split(";");
                        for (int index = 0; index < arrOfferId.length; index++) {
                            lstReasonCode.add(arrOfferId[index]);
                        }
                    } else if (strListReasonCode != null && !"".equals(strListReasonCode.trim())) {
                        lstReasonCode.add(strListReasonCode.trim());
                    }

                    return lstReasonCode;
                }
            }

            return null;

        } catch (Exception ex) {
            //ex.printStackTrace();
            throw ex;
        }

    }
    public static String CHANGE_SIM_SDN_REASON_DESCRIPTION = "$sdn$";

    private List filterReason(List lstReason, boolean isSdn) {
        List result = new ArrayList();
        try {
            if (lstReason == null || lstReason.size() < 1) {
                return lstReason;
            }

            for (int i = 0; i < lstReason.size(); i++) {
                ReasonPre object = (ReasonPre) lstReason.get(i);
                //Neu goi cuoc la SDN hoac co thong tin kenh phan phoi va description cua REASON la $sdn$ thi add
                if (isSdn == CHANGE_SIM_SDN_REASON_DESCRIPTION.equals(object.getDescription())) {
                    result.add(lstReason.get(i));
                }
            }
        } catch (Exception ex) {
        }
        return result;
    }

    public ParamOut changeSimPre(HibernateHelper hibernateHelper, ChangeSimInput changeSimInput) {
        ParamOut response = new ParamOut();
        response.setErrorCode(Constants.ERROR_CODE_1);
        Session cmPre = null;
        Session im = null;
        Session cmPos = null;
        boolean flagFail = false;
        boolean isUsim = true;
        try {
            cmPre = hibernateHelper.getSession(Constants.SESSION_CM_PRE);
            im = hibernateHelper.getSession(Constants.SESSION_IM);
            cmPos = hibernateHelper.getSession(Constants.SESSION_CM_POS);

            Long resultPr = 1L;
            /*validate input*/
            if (changeSimInput == null) {
                response.setErrorDecription("Data input not null");
                return response;
            }
            if (changeSimInput.getStaffId() == null) {
                response.setErrorDecription("StaffId is not null");
                return response;
            }
            if (changeSimInput.getStaffCode() == null || changeSimInput.getStaffCode().trim().isEmpty()) {
                response.setErrorDecription("StaffCode is not null");
                return response;
            }
            if (changeSimInput.getIsdn() == null || changeSimInput.getIsdn().trim().isEmpty()) {
                response.setErrorDecription("ISDN is not null");
                return response;
            }
            if (changeSimInput.getIdNo() == null || changeSimInput.getIdNo().trim().isEmpty()) {
                response.setErrorDecription("ID No is not null");
                return response;
            }
            String isdn = changeSimInput.getIsdn();
            if (isdn.startsWith("0")) {
                isdn = isdn.replaceFirst("0", "");
            }
            if (isdn.startsWith("855")) {
                isdn = isdn.replaceFirst("855", "");
            }
            changeSimInput.setIsdn(isdn);
            //check Usim
            isUsim = this.isCheckUsim(isdn);
            //check so dep - duyetdk
            boolean checkVipNumber = new SubMbPreDAO().getGroupNameMobile(im, cmPos, isdn, changeSimInput.getStaffCode());
            if (checkVipNumber) {
                return new ParamOut(Constants.ERROR_CODE_1, "This number can not change");
            }
            ParamOut checkSim = checkChangeSim(hibernateHelper, isdn, changeSimInput.getIdNo());
            if (Constants.ERROR_CODE_1.equals(checkSim.getErrorCode())) {
                return checkSim;
            }
            /*check fomr sale*/
            if (changeSimInput.getSaleServiceForm() == null || changeSimInput.getSaleServiceForm().trim().isEmpty()) {
                response.setErrorDecription("You must input sale form");
                return response;
            }
            /*check serial*/
            if (changeSimInput.getSerial() == null || changeSimInput.getSerial().trim().isEmpty()) {
                response.setErrorDecription("Serial is not null");
                return response;
            }
            /*check reason*/
            if (changeSimInput.getReasonId() == null) {
                response.setErrorDecription("Reason is not null");
                return response;
            }
            if (checkSim.isIsRightInfor()) {
                //Dung thong tin
            } else {
                //Sai thong tin
                if (changeSimInput.getCommitForm() == null || changeSimInput.getCommitForm().trim().isEmpty()) {
                    response.setErrorDecription("You must input commit form");
                    return response;
                }
            }
            Shop shop = new ShopDAO().findById(cmPos, changeSimInput.getShopId());
            /*Check imsi*/
            WebServiceIMDAO webIMDAO = new WebServiceIMDAO(im);
            StockSim stockSim = webIMDAO.findSimPrePaidByImsiOrSerial(null, changeSimInput.getSerial());
            String newImsi = null;
            if (stockSim != null) {
                newImsi = stockSim.getImsi();
            }
            if (newImsi == null || newImsi.isEmpty()) {
                response.setErrorDecription("No IMSI corresponding to the  Serial " + changeSimInput.getSerial());
                return response;
            }

            String message = null;

            Date issDateTime = new Date();
            BaseSupplier baseSupplier = new BaseSupplier();
            Long actionAuditId = baseSupplier.getSequence(cmPre, "SEQ_ACTION_AUDIT");
            boolean isChange = false;

            SubMbPre mobile = new SubMbPreDAO().findByIsdn(cmPre, changeSimInput.getIsdn());
            SubMbPre oldMobile = (SubMbPre) BeanUtils.cloneBean(mobile);

            String oldImsi = mobile.getImsi();
            String oldSerial = mobile.getSerial();
            String newSerial = changeSimInput.getSerial();

            ReasonPre reason = new ReasonPreDAO().findById(cmPre, changeSimInput.getReasonId());
            if (reason == null) {
                response.setErrorDecription("Can not find reason for " + changeSimInput.getReasonId());
                return response;
            }
            InterfaceCommon interfaceCommon = new InterfaceCommon();
            if (!oldImsi.equals(newImsi)) {
                List<StockSimBean> lstStockSimBean = new ArrayList<StockSimBean>();
                //TuLA4 R1438
                if (changeSimInput.getReasonId().longValue() == Constant.CHANGE_SIM_WARRANTY_PRE_M_REASON_ID) {

                    Date activateDate = mobile.getStaDatetime();
                    List<ApParamPre> listApParam = new ApParamDAO().findByTypeCode(cmPre, Constant.CHANGE_SIM_WARRANTY_PRE_MONTHS, Constant.CHANGE_SIM_WARRANTY_PRE_MONTHS);
                    ApParamPre param = listApParam != null && !listApParam.isEmpty() ? listApParam.get(0) : null;
                    if (param == null) {
                        flagFail = true;
                        message = "Cannot find cm_pre2.ap_param has type " + Constant.CHANGE_SIM_WARRANTY_PRE_MONTHS;
                    } else if (activateDate == null) {
                        flagFail = true;
                        message = "Invalid staDatetime (null) for subId " + mobile.getSubId();
                    } else if (!checkWarrantyDate(activateDate, Integer.parseInt(param.getParamValue()))) {
                        flagFail = true;
                        message = "Cannot " + reason.getName() + " for isdn " + mobile.getIsdn() + " because activate date invalid.";
                    }
                    if (flagFail) {
                        rollBackTransactions(cmPre, im, cmPos);
                        cmPre.beginTransaction();
                        im.beginTransaction();
                        cmPos.beginTransaction();
                        response.setErrorDecription(message);
                        return response;
                    }
                }

                //End TuLA4 R1438
                String saleServiceCode = new com.viettel.bccs.cm.dao.MappingDAO().getSaleServiceCodePre(cmPre, Constant.SERVICE_MOBILE_ID_WEBSERVICE, reason.getReasonId(), mobile.getProductCode(), Constant.ACTION_SUBSCRIBER_CHANGE_SIM);

                if (saleServiceCode == null || "".equals(saleServiceCode)) {
                    rollBackTransactions(cmPre, im, cmPos);
                    cmPre.beginTransaction();
                    im.beginTransaction();
                    cmPos.beginTransaction();
                    response.setErrorDecription("reason does not match with sales service to generate transaction");
                    return response;
                }

                String errorMessage = null;
                //Thuc hien gui giao dich co tru kho
                addToListStockSim(lstStockSimBean, newImsi, newSerial, WebServiceIMDAO.STOCK_SIM_PRE_PAID);

                logger.info(changeSimInput.getStaffCode() + " changeSim, Step 3, START : InterfaceCmInventory.checkImsiIsInStockByOwnerType");
                errorMessage = interfaceCommon.checkValidResourceByOwnerType(im, 2l,
                        changeSimInput.getStaffId(), saleServiceCode, null, lstStockSimBean, null);

                logger.info(changeSimInput.getStaffCode() + " changeSim, Step 4, END : InterfaceCmInventory.checkImsiIsInStockByOwnerType");
                if (errorMessage == null || "".equals(errorMessage)) {
                    // update sub_mb
                    mobile.setImsi(newImsi);
                    mobile.setSerial(newSerial);
                    mobile.setChangeDatetime(issDateTime);
                    cmPre.save(mobile);

                    // update sub_sim_mb
                    SubSimMbPreDAO.changeSubSimMb(mobile.getSubId(), oldImsi, newImsi, newSerial, issDateTime, actionAuditId, cmPre);

                    logger.info(changeSimInput.getStaffCode() + " changeSim, Step 5, START : InterfaceCmInventory.executeSaleTrans");
                    errorMessage = interfaceCommon.executeSaleTrans(cmPre, im, mobile, Constant.TRANS_OF_SUBSCRIBER,
                            null, lstStockSimBean, null, reason.getReasonId(),
                            Constant.ACTION_SUBSCRIBER_CHANGE_SIM, issDateTime, changeSimInput.getStaffId(), changeSimInput.getShopId());

//                                    executeSaleTrans(Session cmSession, Session imSession, Customer customer, Subscriber subscriber, Long objectType, UserToken userToken, List lstStockIsdn, List lstStockSim, List lstStockSerial, List<SubStockModelRel> lstSubStockModelRel, Long reasonId, String actionCode, Date nowDate, HttpServletRequest request)
                    logger.info(changeSimInput.getStaffCode() + " changeSim, Step 6, END : InterfaceCmInventory.executeSaleTrans");
                    String temp = null;
                    temp = LabelUtil.getKey(errorMessage, "en_US");
                    if (temp != null && !temp.isEmpty()) {
                        errorMessage = temp;
                    }
                    if (errorMessage != null && !errorMessage.equals("")) {
                        rollBackTransactions(cmPre, im, cmPos);
                        cmPre.beginTransaction();
                        im.beginTransaction();
                        cmPos.beginTransaction();
                        response.setErrorDecription(errorMessage);
                        return response;
                    }

                    //Ghi Log
                    StringBuilder strDes = new StringBuilder();

                    // Add Action_audit Prefix
                    strDes.append(Constant.ACTION_AUDIT_DB_SW_PREFIX + ": ");

                    if (shop.getShopCode() != null && !"".equals(shop.getShopCode())) {
                        if (changeSimInput.getStaffId() != null) {
                            List lstParam = new ArrayList();
                            lstParam.add(changeSimInput.getStaffCode());
                            lstParam.add(changeSimInput.getStaffCode());
                            lstParam.add(shop.getShopCode());
                            strDes.append(getLogText("[E0104] NV TDN: {0} instead NV perform: {1} in the store: {2}", lstParam));
                        } else {
                            List lstParam = new ArrayList();
                            lstParam.add(changeSimInput.getStaffCode());
                            lstParam.add(shop.getShopCode());
                            strDes.append(getLogText("[E0105] NV TDN: {0} instead of agents perform: {1}", lstParam));
                        }
                        strDes.append(" : ");
                    }

                    // Ghi Action_audit
                    strDes.append("Change sim and update database for Mobile subscriber");

                    strDes.append(" - ");
                    strDes.append("Old imsi: ");
                    strDes.append(oldImsi);
                    strDes.append(" --> ");
                    strDes.append("New imsi: ");
                    strDes.append(newImsi);

                    /*Insert image form*/
                    FileSaveProcess fileSaveProcess = new FileSaveProcess();
                    Long imageId = Common.getSequence("CUST_IMAGE_SEQ", cmPos);
                    SubFbConfigSupplier supplier = new SubFbConfigSupplier();
                    if (changeSimInput.getCommitForm() != null) {
                        String imageName = imageId + "_commit.jpg";
                        fileSaveProcess.saveFileNotFtp(imageName, changeSimInput.getCommitForm(), "", logger, Constants.FTP_DIR_SUB_ACTION);
                        supplier.insertImageDetail(cmPos, imageId, imageName, "CHANGE_ISDN", null, changeSimInput.getStaffCode(), null, null);
                    }
                    if (changeSimInput.getSaleServiceForm() != null) {
                        String imageName = imageId + "_sale.jpg";
                        fileSaveProcess.saveFileNotFtp(imageName, changeSimInput.getSaleServiceForm(), "", logger, Constants.FTP_DIR_SUB_ACTION);
                        supplier.insertImageDetail(cmPos, imageId, imageName, "CHANGE_ISDN", null, changeSimInput.getStaffCode(), null, null);
                    }
                    if (changeSimInput.getIdCardFront() != null) {
                        String imageName = imageId + "_id1.jpg";
                        fileSaveProcess.saveFileNotFtp(imageName, changeSimInput.getIdCardFront(), "", logger, Constants.FTP_DIR_SUB_ACTION);
                        supplier.insertImageDetail(cmPos, imageId, imageName, "CHANGE_ISDN", null, changeSimInput.getStaffCode(), null, null);
                    }
                    if (changeSimInput.getIdCardback() != null) {
                        String imageName = imageId + "_id2.jpg";
                        fileSaveProcess.saveFileNotFtp(imageName, changeSimInput.getIdCardback(), "", logger, Constants.FTP_DIR_SUB_ACTION);
                        supplier.insertImageDetail(cmPos, imageId, imageName, "CHANGE_ISDN", null, changeSimInput.getStaffCode(), null, null);
                    }

                    ActionLog.logAction(cmPre, actionAuditId, mobile.getSubId(), Constant.ACTION_AUDIT_PK_TYPE_SUBSCRIBER,
                            reason.getReasonId(), Constant.ACTION_SUBSCRIBER_CHANGE_SIM, strDes.toString(), issDateTime, changeSimInput.getStaffCode(), shop.getShopCode(), "mBCCS", imageId);

                    ActionLog.logAuditDetail(cmPre, mobile.getSubId(), actionAuditId, "SUB_MB", "IMSI", oldImsi, mobile.getImsi(), issDateTime);
                    ActionLog.logAuditDetail(cmPre, mobile.getSubId(), actionAuditId, "SUB_MB", "SERIAL", oldSerial, mobile.getSerial(), issDateTime);

                    isChange = true;
                    cmPre.flush();
                    logger.info(changeSimInput.getStaffCode() + " changeSim, Step 7");
                    im.flush();
                    logger.info(changeSimInput.getStaffCode() + " changeSim, Step 8");

                } else {
                    message = errorMessage;
                    rollBackTransactions(cmPre, im, cmPos);
                    cmPre.beginTransaction();
                    im.beginTransaction();
                    cmPos.beginTransaction();
                    response.setErrorCode(Constants.ERROR_CODE_1);
                    response.setErrorDecription("change sim failed: " + message);
                    return response;
                }
            } else {
                rollBackTransactions(cmPre, im, cmPos);
                cmPre.beginTransaction();
                im.beginTransaction();
                cmPos.beginTransaction();
                response.setErrorCode(Constants.ERROR_CODE_1);
                response.setErrorDecription("new sim and old sim is duplicate");
                return response;
            }
            boolean checkChangeSim = true;
            InterfacePr interfacePr = new InterfacePr();
            if (isChange) {
                ViettelService changeSimResponse = null;
                logger.info(changeSimInput.getStaffCode() + " changeSim, Step 9, START ProvisioningV2.changeSim");
                changeSimResponse = interfacePr.changeSim(im, oldMobile, newImsi, newSerial);
                logger.info(changeSimInput.getStaffCode() + " changeSim, Step 10, START ProvisioningV2.changeSim");
                if (changeSimResponse != null && changeSimResponse.get("responseCode") != null
                        && "0".equals(changeSimResponse.get("responseCode").toString().trim())) {
                    resultPr = Long.valueOf(changeSimResponse.get("responseCode").toString().trim());
                } else {
                    resultPr = 1L;
                }

                if (!resultPr.equals(0L)) {
                    checkChangeSim = false;
                    rollBackTransactions(cmPre, im, cmPos);
                    cmPre.beginTransaction();
                    im.beginTransaction();
                    cmPos.beginTransaction();
                    message = "Change SIM unsuccessfully because the connection to Exchange fail: " + changeSimResponse.toString();

                    //Update action_log_pr
                    ActionLog.saveActionLogPr(cmPre, changeSimResponse, changeSimInput.getStaffCode(), shop.getShopCode());
                    commitTransactions(cmPre, im, cmPos);
                    cmPre.beginTransaction();
                    im.beginTransaction();
                    cmPos.beginTransaction();
                    logger.info(changeSimInput.getStaffCode() + " changeSim, Step 11");
                    response.setErrorDecription(message);
                    return response;
                } else {
                    //Commit DB, d? ti?p t?c th?c hi?n ch?c nang ch?n m? 1C, 2C
                    /*commitTransactions(cmPre, im, cmPos);
                     cmPre.beginTransaction();
                     im.beginTransaction();
                     cmPos.beginTransaction();*/
                }
            }

            if (isChange && checkChangeSim) {

                String stockModelCode = getStockModelCode(im, newSerial);
                if (stockModelCode != null) {
                    stockModelCode = stockModelCode.trim();
                    if (!stockModelCode.isEmpty() && isActiveSim4G(stockModelCode)) {

                        logger.info(changeSimInput.getStaffCode() + " changeSim, START ProvisioningV2.activeService4G");
                        ViettelService activeService4G = interfacePr.activeService4G(oldMobile.getIsdn());
                        logger.info(changeSimInput.getStaffCode() + " changeSim, End ProvisioningV2.activeService4G");
                        if (activeService4G != null && activeService4G.get("responseCode") != null
                                && "0".equals(activeService4G.get("responseCode").toString().trim())) {
                            resultPr = Long.valueOf(activeService4G.get("responseCode").toString().trim());
                        } else {
                            resultPr = 1L;
                        }
                        if (!resultPr.equals(0L)) {
                            logger.info(changeSimInput.getStaffCode() + " Active 4G fail");
                        } else {
                            if (!isUsim && !"03".equals(mobile.getActStatus())) { // not idle
                                isUsim = this.isCheckUsim(isdn);
                                if (isUsim) {
                                    int i = this.insertChangeSim4G(cmPre, isdn, "change sim from mBCCS");
                                    commitTransactions(cmPre);
                                    cmPre.beginTransaction();
                                    if (i == 1) {
                                        logger.info("insert change_sim_4g table success");
                                    } else {
                                        logger.info(" insert change_sim_4g table fail");
                                    }
                                }
                            }
                            logger.info(changeSimInput.getStaffCode() + " Active 4G success");
                        }
                    }
                }
            }

            boolean updateSuccess = true;
            if (isChange && resultPr.equals(0L)) {
                Long isBlockOpen = changeSimInput.getIsBlockOpen();
                /*if (isBlockOpen != null && isBlockOpen == 1) {
                 String modificationType = null;
                 String strActionCode = "";
                 String strDescriptionLog = "";

                 Long openActionAuditId = baseSupplier.getSequence(cmPre, "SEQ_ACTION_AUDIT");
                 String oldStatus = mobile.getActStatus();
                 String newStatus = "0" + oldStatus.substring(1);
                 String numWay = "";
                 // Thuc hien gui lenh chan mo
                 if (Constant.SUB_ACT_STATUS_BLOCK_OUT_GOING_CALL_BY_CUST.equals(oldStatus) && Constant.SUB_ACT_STATUS_UN_BLOCK_TWO_WAY_BY_CUST.contains(newStatus)) {
                 modificationType = Constant.MODIFICATION_TYPE_UN_BLOCK_OUT_GOING_CALL;
                 strActionCode = Constant.ACTION_SUBCRIBER_ACTIVE_1_WAY; //M? ch?n chieu di
                 strDescriptionLog = "Open 1 way customer request for subscriber =  ";
                 numWay = "1";
                 } else if (Constant.SUB_ACT_STATUS_BLOCK_TWO_WAY_BY_CUST.equals(oldStatus) && Constant.SUB_ACT_STATUS_UN_BLOCK_TWO_WAY_BY_CUST.contains(newStatus)) {
                 strActionCode = Constant.ACTION_SUBCRIBER_ACTIVE_2_WAYS; //M? ch?n 2 chieu
                 modificationType = Constant.MODIFICATION_TYPE_UN_BLOCK_TWO_WAY;
                 strDescriptionLog = "Open 2 ways customer request for subscriber = ";
                 numWay = "2";
                 }

                 if (newStatus.length() == 3) {
                 newStatus = newStatus.substring(0, newStatus.length() - 1);
                 } else if (newStatus.length() == 4) {
                 newStatus = newStatus.substring(0, newStatus.length() - 2);
                 }

                 mobile.setActStatus(newStatus);
                 // Them thoi gian tac dong
                 mobile.setChangeDatetime(issDateTime);
                 cmPre.update(mobile);
                 ActionLog.logActionV2(openActionAuditId, mobile.getSubId(), Constant.ACTION_AUDIT_PK_TYPE_SUBSCRIBER, reason.getReasonId(),
                 strActionCode, strDescriptionLog + mobile.getIsdn(), issDateTime, cmPre, changeSimInput.getStaffCode(), shop.getShopCode());

                 ActionLog.logAuditDetailV2(mobile.getSubId(), openActionAuditId, "SUB_MB", "ACT_STATUS", oldStatus, mobile.getActStatus(), issDateTime, cmPre);
                 cmPre.flush();
                 updateSuccess = true;

                 if (updateSuccess) {

                 logger.info(changeSimInput.getStaffCode() + " blockOpenSub, START ProvisioningV2.blockOpenSubMB");
                 ViettelService blockOpenResponse = interfacePr.blockOpenSub(cmPre, mobile, modificationType, oldStatus, numWay);
                 logger.info(changeSimInput.getStaffCode() + " blockOpenSub, START ProvisioningV2.blockOpenSubMB");
                 if (blockOpenResponse != null && blockOpenResponse.get("responseCode") != null
                 && "0".equals(blockOpenResponse.get("responseCode").toString().trim())) {

                 commitTransactions(cmPre, im, cmPos);
                 cmPre.beginTransaction();
                 im.beginTransaction();
                 cmPos.beginTransaction();
                 } else {
                 rollBackTransactions(cmPre, im, cmPos);
                 cmPre.beginTransaction();
                 im.beginTransaction();
                 cmPos.beginTransaction();

                 ActionLog.saveActionLogPr(cmPre, blockOpenResponse, changeSimInput.getStaffCode(), shop.getShopCode());
                 updateSuccess = false;
                 }

                 } else {
                 message = "Update unsuccessfully because failed to save to database";
                 }
                 }*/
                //tru?ng h?p 
                long reasonIdRestore = Long.parseLong(new ApParamDAO().getValueByTypeCode(cmPre, "CHANGE_SIM_RESTORE", "REASON_ID"));
                if (reason.getReasonId().equals(reasonIdRestore)) {
                    //mo co HLR   
                    cmPre.clear();
                    cmPre.beginTransaction();
                    OriginalViettelMsg resultLock = interfacePr.lockFlagCallHlr(cmPre, mobile.getIsdn(), "FALSE", changeSimInput.getStaffCode(), shop.getShopCode());
                    ActionLog.saveActionLogPr(cmPre, resultLock, changeSimInput.getStaffCode(), shop.getShopCode());
                    actionAuditId = baseSupplier.getSequence(cmPre, "SEQ_ACTION_AUDIT");
                    ActionLog.logActionV2(actionAuditId, mobile.getSubId(), Constant.ACTION_AUDIT_PK_TYPE_SUBSCRIBER,
                            13013l, Constant.ACTION_SUBCRIBER_ACTIVE_1_WAY, "Unblock one way by change sim", new Date(), cmPre, changeSimInput.getStaffCode(), shop.getShopCode());

                    String accountId;
                    String amount;
                    try {
                        accountId = new ApParamDAO().getValueByTypeCode(cmPre, "CHANGE_SIM_RESTORE", "ACCOUNT_ADD");
                        amount = new ApParamDAO().getValueByTypeCode(cmPre, "CHANGE_SIM_RESTORE", "AMOUNT_ADD");
                    } catch (Exception ex) {
                        accountId = "2000";
                        amount = "2";
                    }
                    addAccountLog(cmPre, mobile.getIsdn(), amount, reason.getReasonId(), changeSimInput.getStaffCode(), "add money change sim restore");
                    actionAuditId = baseSupplier.getSequence(cmPre, "SEQ_ACTION_AUDIT");
                    ActionLog.logActionV2(actionAuditId, mobile.getSubId(), Constant.ACTION_AUDIT_PK_TYPE_SUBSCRIBER,
                            13031l, Constant.ACTION_SUBCRIBER_ACTIVE_1_WAY, "Add money 2$ by change sim ", new Date(), cmPre, changeSimInput.getStaffCode(), shop.getShopCode());

                    resultLock = interfacePr.addMoney(cmPre, accountId, mobile.getIsdn(), amount, changeSimInput.getStaffCode(), shop.getShopCode());
                    ActionLog.saveActionLogPr(cmPre, resultLock, changeSimInput.getStaffCode(), shop.getShopCode());

                    cmPre.flush();
                    cmPre.getTransaction().commit();
                    cmPre.beginTransaction();

                }
            } else {
                rollBackTransactions(cmPre, im, cmPos);
                cmPre.beginTransaction();
                im.beginTransaction();
                cmPos.beginTransaction();
                response.setErrorCode(Constants.ERROR_CODE_1);
                response.setErrorDecription("change sim failed: call pro failed " + message);
                return response;
            }
            if (message == null && isChange && resultPr.equals(0L) && updateSuccess) {
                commitTransactions(cmPre, im, cmPos);
                cmPre.beginTransaction();
                im.beginTransaction();
                cmPos.beginTransaction();
                response.setErrorCode(Constants.ERROR_CODE_0);
                response.setErrorDecription("Success");
            } else {
                rollBackTransactions(cmPre, im, cmPos);
                cmPre.beginTransaction();
                im.beginTransaction();
                cmPos.beginTransaction();
                response.setErrorCode(Constants.ERROR_CODE_1);
                response.setErrorDecription(message);
            }
        } catch (Exception ex) {
            logger.info(ex);
            ex.printStackTrace();
            rollBackTransactions(cmPre, im, cmPos);
        } finally {
            closeSessions(cmPre, im, cmPos);
        }
        return response;
    }

    public boolean checkWarrantyDate(Date date, int months) throws Exception {
        if (date == null) {
            return false;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, months);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        /* 23h */

        cal.set(Calendar.MINUTE, 59);/* minute 59th*/

        Calendar current = Calendar.getInstance();
        if (cal.after(current)) {
            return true;
        }
        return false;
    }

    public void addToListStockSim(List lstStockSim, String imsi, String serial, Long stockTypeId) {
        StockSimBean stockSIM = new StockSimBean();

        if (imsi != null) {
            imsi = imsi.trim();
        }
        if (serial != null) {
            serial = serial.trim();
        }
        stockSIM.setImsi(imsi);
        stockSIM.setSerial(serial);
        stockSIM.setStockTypeId(stockTypeId);

        lstStockSim.add(stockSIM);
    }

    public String getLogText(String text, List<String> lstParam) {
        for (int i = 0; i < lstParam.size(); i++) {
            text = text.replace("{" + String.valueOf(i) + "}", lstParam.get(i) != null ? String.valueOf(lstParam.get(i)) : "");
        }
        return text;
    }

    public String getStockModelCode(Session imSession, String serial) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(" select sm.stock_model_code from bccs_im.stock_sim ss ");
            sb.append(" , bccs_im.stock_model sm where ");
            sb.append(" ss.stock_model_id = sm.stock_model_id and ss.serial = to_number(?) ");
            Query query = imSession.createSQLQuery(sb.toString());
            query.setParameter(0, serial);
            List list = query.list();
            if (list != null && list.size() > 0) {
                return list.get(0).toString();
            }
            return null;
        } catch (RuntimeException re) {
            throw re;
        }

    }

    public boolean isActiveSim4G(String stockModelCode) {
        ResourceBundle resource = ResourceBundle.getBundle("config");
        if (resource != null) {
            String str = resource.getString("stockModelCode4g.active.pre");
            if (!"".equals(str)) {
                String[] stockCodes = str.split(";");
                if (stockCodes.length > 0) {
                    for (String stockCode : stockCodes) {
                        if (stockCode.equals(stockModelCode)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public boolean isCheckUsim(String isdn) {
        logger.info("Check usim:");
        try {
            Service service = new Service();
            HashMap<String, ViettelService> lstCheckNew = service.checkUSIM(isdn);
            ViettelService responsePrNew = lstCheckNew.get("RESPONSE");
            String responseCodeNew = (String) responsePrNew.get("responseCode");
            if ((!"0".equals(responseCodeNew))) {
                //trường hợp bị lỗi
                logger.error("Error check Usim");
                return false;
            }
            String content = (String) responsePrNew.get("content");
            if (content.contains("USIM")) {
                logger.info("Check usim: success");
                return true;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    public int insertChangeSim4G(Session cmPre, String isdn, String description) {
        logger.info("Insert to changeSim4G log, isdn :" + isdn);
        String sql = "INSERT INTO CHANGE_SIM_4G (id, isdn, status, change_date, update_date, description, system) "
                + "VALUES(CHANGE_SIM_4G_SEQ.NEXTVAL,?,0,sysdate,sysdate,?,'mBCCS') ";
        try {
            Query query = cmPre.createSQLQuery(sql);

            query.setParameter(0, isdn);
            query.setParameter(1, description);
            query.executeUpdate();
//            sessionCM.flush();
            return 1;
        } catch (Exception ex) {
            logger.error("Error " + ex.getMessage(), ex);
            return -1;
        }
    }

    public int addAccountLog(Session cmPre, String isdn, String amount, Long reasonId, String requester, String description) {
        String sql = " INSERT INTO add_account (insert_date, isdn, basic_account, reason_id, status, requester, document_no) "
                + "VALUES(sysdate,?,?,?,?,?,?) ";
        try {
            Query query = cmPre.createSQLQuery(sql);

            query.setParameter(0, isdn);
            query.setParameter(1, amount);
            query.setParameter(2, reasonId);
            query.setParameter(3, 1);
            query.setParameter(4, requester);
            query.setParameter(5, description);
            query.executeUpdate();
//            sessionCM.flush();
            return 1;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        }
    }
}
