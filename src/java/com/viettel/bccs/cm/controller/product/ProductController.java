package com.viettel.bccs.cm.controller.product;

import com.google.gson.Gson;
import com.viettel.bccs.cm.bussiness.BaseBussiness;
import com.viettel.bccs.cm.bussiness.common.CacheBO;
import com.viettel.bccs.cm.bussiness.product.ProductBussiness;
import com.viettel.bccs.cm.controller.BaseController;
import com.viettel.bccs.cm.dao.SubBundleTvDAO;
import com.viettel.bccs.cm.model.SubBundleTv;
import com.viettel.bccs.cm.supplier.ServiceSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.output.BundleTVInfo;
import com.viettel.brcd.ws.model.output.Group;
import com.viettel.brcd.ws.model.output.GroupOut;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.Product;
import com.viettel.brcd.ws.model.output.ProductOut;
import com.viettel.brcd.ws.model.output.Request;
import com.viettel.brcd.ws.model.output.ServiceResponse;
import com.viettel.brcd.ws.model.output.ServiceResponseList;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.List;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ProductController extends BaseController {

    public ProductController() {
        logger = Logger.getLogger(ProductController.class);
    }

    public GroupOut getProductGroups(HibernateHelper hibernateHelper, Long telecomServiceId, String locale) {
        GroupOut result = null;
        Session productSession = null;
        try {
            productSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            List<Group> groups = new ProductBussiness().getProductGroups(productSession, telecomServiceId);
            result = new GroupOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), groups);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new GroupOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(productSession);
            LogUtils.info(logger, "ProductController.getProductGroups:result=" + LogUtils.toJson(result));
        }
    }

    public ProductOut getProducts(HibernateHelper hibernateHelper, Long telecomServiceId, String groupProduct, String locale) {
        ProductOut result = null;
        Session productSession = null;
        try {
            productSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            List<Product> products = new ProductBussiness().getProducts(productSession, telecomServiceId, groupProduct);
            result = new ProductOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), products);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ProductOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(productSession);
            LogUtils.info(logger, "ProductController.getProducts:result=" + LogUtils.toJson(result));
        }
    }
    
    /**
     * @author duyetdk
     * @since 12/5/2019
     * @des lay danh sach loai thue bao tra truoc
     * @param hibernateHelper
     * @param telecomServiceId
     * @param groupProduct
     * @param locale
     * @return 
     */
    public ProductOut getListProductPre(HibernateHelper hibernateHelper, Long telecomServiceId, String groupProduct, String locale) {
        ProductOut result = null;
        Session productSession = null;
        try {
            productSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            List<Product> products = new ProductBussiness().getListProductPre(productSession, telecomServiceId, groupProduct);
            result = new ProductOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), products);
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ProductOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            return result;
        } finally {
            closeSessions(productSession);
            LogUtils.info(logger, "ProductController.getListProductPre:result=" + LogUtils.toJson(result));
        }
    }

    public ParamOut getProductVasFTTH(HibernateHelper hibernateHelper) {
        ParamOut result = new ParamOut();
        result.setErrorCode(Constants.ERROR_CODE_1);
        result.setErrorDecription(Constants.ERROR_CODE_1);
        Session cmSession = null;
        try {
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            result.setErrorCode(Constants.ERROR_CODE_0);
            result.setErrorDecription("Success");
            result.setListProductVas(CacheBO.getProductVasFTTH(cmSession));
            return result;
        } catch (Throwable ex) {
            result.setErrorDecription(ex.getMessage());
        } finally {
            closeSessions(cmSession);
            LogUtils.info(logger, "ProductController.getProducts:result=" + LogUtils.toJson(result));
        }
        return result;
    }

    public ProductOut getBunldleTvProduct(HibernateHelper hibernateHelper) {
        ProductOut result = null;
        Session productSession = null;
        Session cmSession = null;
        try {
            productSession = hibernateHelper.getSession(Constants.SESSION_PRODUCT);
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            List<Product> products = new ProductBussiness().getProducts(productSession, null, "BundleTV");
            result = new ProductOut(Constants.ERROR_CODE_0, "Success", products);

            Long seqAccount = new BaseBussiness().getSequence(cmSession, "AUTO_ACCOUNT_TV_SEQ");
            StringBuilder account = new StringBuilder("99");
            String formatted = String.format("%06d", seqAccount);
            account.append(formatted);
            result.setAccountBundleTv(account.toString());
            return result;
        } catch (Throwable ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ProductOut(Constants.ERROR_CODE_1, ex.getMessage());
            return result;
        } finally {
            closeSessions(productSession);
            closeSessions(cmSession);
            LogUtils.info(logger, "ProductController.getProducts:result=" + LogUtils.toJson(result));
        }
    }

    public BundleTVInfo getAccBundleTvInfo(HibernateHelper hibernateHelper, String accBundleTv) {
        Session cmSession = null;
        try {
            cmSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            BundleTVInfo info = new BundleTVInfo();
            SubBundleTv tv = new SubBundleTvDAO().findBundleTvByAccount(cmSession, accBundleTv);
            info.setAccount(tv.getAccount());
            info.setActStatus(tv.getActStatus());
            info.setBoxSerial(tv.getBoxSerial());
            info.setCreateDate(tv.getCreateDate());
            info.setCreateUser(tv.getCreateUser());
            info.setDeviceUniqueId(tv.getDeviceUniqueId());
            info.setFirstConnect(tv.getFirstConnect());
            info.setId(tv.getId());
            info.setProductCode(tv.getProductCode());
            info.setRegReasonCode(tv.getRegReasonCode());
            info.setStatus(tv.getStatus());
            Request request = new Request();
            if (tv.getUserUniqueId() != null && tv.getDeviceUniqueId() != null) {
                request.getUserDevicePackageList(tv.getUserUniqueId(), tv.getBoxSerial());
                String data = new Gson().toJson(request);
                ServiceSupplier serviceSup = new ServiceSupplier();
                String response = serviceSup.sendRequest(Constants.SERVICE_METHOD_POST, ResourceUtils.getResource("BUNDLE_TV_SERVICE"), data, null, null);
                if (response != null && !response.isEmpty()) {
                    WSRespone ws = new Gson().fromJson(response, WSRespone.class);
                    ServiceResponseList serviceResponse = ws == null || ws.getErrorDecription() == null ? null : new Gson().fromJson(ws.getErrorDecription(), ServiceResponseList.class);
                    if (serviceResponse != null) {
                        info.setLstPackage(serviceResponse.getContent());
                    }
                }
            }
            return info;
        } catch (Throwable ex) {
            int a = 1;
        } finally {
            closeSessions(cmSession);
        }
        return null;
    }
}
