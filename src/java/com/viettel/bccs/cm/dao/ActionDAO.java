package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.Action;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ActionDAO extends BaseDAO {

    public Action findById(Session session, Long actionId) {
        if (actionId == null || actionId <= 0L) {
            return null;
        }

        String sql = " from Action where actionId = ? ";
        Query query = session.createQuery(sql).setParameter(0, actionId);
        List<Action> actions = query.list();
        if (actions != null && !actions.isEmpty()) {
            return actions.get(0);
        }

        return null;
    }
}
