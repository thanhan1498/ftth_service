package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.ActionLogPr;
import com.viettel.bccs.cm.model.QueryLog;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

public class ActionLogPrDAO extends BaseDAO {

    public ActionLogPr findById(Session cmPosSession, Long id) {
        if (id == null || id <= 0L) {
            return null;
        }

        StringBuilder sql = new StringBuilder().append(" from ActionLogPr where id = ? ");
        Query query = cmPosSession.createQuery(sql.toString()).setParameter(0, id);

        List<ActionLogPr> logs = query.list();
        if (logs != null && !logs.isEmpty()) {
            return logs.get(0);
        }

        return null;
    }

    public ActionLogPr insert(Session sessoin, Date createDate, String shopCode, String userName, String isdn, String request, String response, String responseCode, String exception) {
        Long actionLogPrId = getSequence(sessoin, Constants.ACTION_LOG_PR_SEQ);
        return insert(sessoin, actionLogPrId, createDate, shopCode, userName, isdn, request, response, responseCode, exception);
    }

    public ActionLogPr insert(Session session, Long actionLogPrId, Date createDate, String shopCode, String userName, String isdn, String request, String response, String responseCode, String exception) {
        ActionLogPr actionLogPr = new ActionLogPr();
        actionLogPr.setId(actionLogPrId);
        actionLogPr.setCreateDate(createDate);
        actionLogPr.setUserName(userName);
        actionLogPr.setShopCode(shopCode);
        actionLogPr.setIsdn(isdn);
        actionLogPr.setRequest(request);
        actionLogPr.setResponse(response != null && response.length() >= 4000 ? response.substring(0, 3900) : response);
        actionLogPr.setResponseCode(responseCode);
        actionLogPr.setException(exception);
        session.save(actionLogPr);
        session.flush();
        return actionLogPr;
    }

    /**
     * @author duyetdk
     * @des luu log truy van thong tin KH
     * @since 11/08/2019
     * @param cmPosSession
     * @param function
     * @param paramValue
     * @param nowDate
     * @param userLogin
     */
    public void insertQueryLog(Session cmPosSession, String function, String paramValue,
            Date nowDate, String userLogin) {
        try {
            cmPosSession.beginTransaction();
            QueryLog queryLog = new QueryLog();
            Long queryLogId = getSequence(cmPosSession, Constants.QUERY_LOG_SEQ);
            queryLog.setQueryLogId(queryLogId);
            queryLog.setFunction(function);
            queryLog.setParamValue(paramValue);
            queryLog.setCreateDate(nowDate);
            queryLog.setCreateUser(userLogin);
            queryLog.setSystem("mBCCS");
            queryLog.setIp("mBCCS");
            cmPosSession.save(queryLog);
            cmPosSession.flush();
            cmPosSession.beginTransaction().commit();
            cmPosSession.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
