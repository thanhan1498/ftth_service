package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.bussiness.ApParamBussiness;
import com.viettel.bccs.cm.model.ApDomain;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ApDomainDAO extends BaseDAO {

    public List<ApDomain> findByType(Session session, String type) {
        if (type == null) {
            return null;
        }
        type = type.trim();
        if (type.isEmpty()) {
            return null;
        }
        String sql = " from ApDomain where type = ? and status = ?  order by name ";
        Query query = session.createQuery(sql).setParameter(0, type).setParameter(1, Constants.STATUS_USE);
        return query.list();
    }

    public List<ApDomain> findByTypeCode(Session session, String type, String code) {
        if (type == null || code == null) {
            return null;
        }
        type = type.trim();
        if (type.isEmpty()) {
            return null;
        }
        code = code.trim();
        if (code.isEmpty()) {
            return null;
        }
        String sql = " from ApDomain where type = ? and code = ? and status = ? order by name ";
        Query query = session.createQuery(sql).setParameter(0, type).setParameter(1, code).setParameter(2, Constants.STATUS_USE);
        return query.list();
    }

    public List<ApDomain> findByTypeGroup(Session session, String type, String group) {
        if (type == null || group == null) {
            return null;
        }
        type = type.trim();
        if (type.isEmpty()) {
            return null;
        }
        group = group.trim();
        if (group.isEmpty()) {
            return null;
        }
        String sql = " from ApDomain where type = ? and groupCode = ? and status = ? order by name ";
        Query query = session.createQuery(sql).setParameter(0, type).setParameter(1, group).setParameter(2, Constants.STATUS_USE);
        return query.list();
    }

    public boolean isMandatoryTrasaction(Session cmPosSession, String actionCode) {
        if (actionCode == null) {
            return false;
        }
        actionCode = actionCode.trim();
        if (actionCode.isEmpty()) {
            return false;
        }
        List<ApDomain> domains = getApDomainByTypeAndCode(cmPosSession, Constants.AP_DOMAIN_TYPE_ACTION_DOC, actionCode);
        if (domains != null && !domains.isEmpty()) {
            String value = new ApParamBussiness().getUserManualConfig(cmPosSession, Constants.MANDATORY_TRANSACTION_ACTION);
            if (value != null) {
                for (ApDomain domain : domains) {
                    if (domain != null) {
                        if (value.equals(domain.getGroupCode())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public List<ApDomain> getApDomainByTypeAndCode(Session cmPosSession, String type, String code) {
        if (code == null) {
            return new ApDomainDAO().findByType(cmPosSession, type);
        }
        code = code.trim();
        if (code.isEmpty()) {
            return new ApDomainDAO().findByType(cmPosSession, type);
        }
        return new ApDomainDAO().findByTypeCode(cmPosSession, type, code);
    }
}
