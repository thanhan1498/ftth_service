package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.Area;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class AreaDAO extends BaseDAO {

    public Area findById(Session session, String areaCode) {
        if (areaCode == null) {
            return null;
        }
        areaCode = areaCode.trim();
        if (areaCode.isEmpty()) {
            return null;
        }

        String sql = " from Area where areaCode = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, areaCode).setParameter(1, Constants.STATUS_USE);
        List<Area> areas = query.list();
        if (areas != null && !areas.isEmpty()) {
            return areas.get(0);
        }

        return null;
    }

    public String getCenterCode(Session session, String areaCode) {
        Area area = findById(session, areaCode);
        if (area != null) {
            return area.getCenCode();
        }
        return null;
    }

    public List<Area> findProvinces(Session session) {
        String sql = " from Area where province is not null and district is null and status = ? order by province asc, name asc  ";
        Query query = session.createQuery(sql).setParameter(0, Constants.STATUS_USE);
        List<Area> provinces = query.list();
        return provinces;
    }

    public List<Area> findDistricts(Session session, String province) {
        String sql = " from Area where province = ? and district is not null and precinct is null and status = ? order by province asc, name asc  ";
        Query query = session.createQuery(sql).setParameter(0, province).setParameter(1, Constants.STATUS_USE);
        List<Area> districts = query.list();
        return districts;
    }

    public List<Area> findPrecincts(Session session, String province, String district) {
        String sql = " from Area where province = ? and district = ? and precinct is not null and streetBlock is null and status = ? order by province asc, name asc  ";
        Query query = session.createQuery(sql).setParameter(0, province).setParameter(1, district).setParameter(2, Constants.STATUS_USE);
        List<Area> districts = query.list();
        return districts;
    }

    public List<Area> findByParentCode(Session session, String parentCode) {
        String sql = " from Area where parentCode = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, parentCode).setParameter(1, Constants.STATUS_USE);
        List<Area> areas = query.list();
        return areas;
    }
}
