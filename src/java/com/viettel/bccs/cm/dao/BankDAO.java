package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.Bank;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class BankDAO extends BaseDAO {

    public List<Bank> findByCodeName(Session session, String code, String name) {
        if (code == null || name == null) {
            return null;
        }
        code = code.trim();
        if (code.isEmpty()) {
            return null;
        }
        name = name.trim();
        if (name.isEmpty()) {
            return null;
        }

        StringBuilder sql = new StringBuilder(" from Bank where ").append(createLike(" bankCode "))
                .append(" and ").append(createLike(" name "));
        Query query = session.createQuery(sql.toString())
                .setParameter(0, formatLike(code))
                .setParameter(1, formatLike(name));
        return query.list();
    }

    public List<Bank> findByCodeName(Session session, String code, String name, Long startRow, Long maxRow) {
        if (code == null && name == null) {
            return null;
        }
        code = code.trim();
        name = name.trim();
        if (code.isEmpty() && name.isEmpty()) {
            return null;
        }

        StringBuilder sql = new StringBuilder(" from Bank where ").append(createLike(" bankCode "))
                .append(" and ").append(createLike(" name "));
        Query query = session.createQuery(sql.toString())
                .setParameter(0, formatLike(code))
                .setParameter(1, formatLike(name));
        if (startRow != null) {
            query.setFirstResult(startRow.intValue());
        }
        if (maxRow != null) {
            query.setMaxResults(maxRow.intValue());
        }
        return query.list();
    }
}
