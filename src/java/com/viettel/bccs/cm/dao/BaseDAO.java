package com.viettel.bccs.cm.dao;

import com.google.gson.Gson;
import com.viettel.brcd.common.util.DateTimeUtils;
import com.viettel.bccs.cm.util.Constants;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * @author vanghv1
 */
public class BaseDAO {

    protected Logger _log = Logger.getLogger(BaseDAO.class);
    protected static final Gson gson = new Gson();

    public void info(Logger logger, Object obj) {
        logger.info(obj);
    }

    public void info(Logger logger, Object obj, Throwable ex) {
        logger.info(obj, ex);
    }

    public void error(Logger logger, Object obj) {
        logger.error(obj);
    }

    public void error(Logger logger, Object obj, Throwable ex) {
        logger.error(obj, ex);
    }

    public String toJson(Object obj) {
        return obj == null ? null : gson.toJson(obj);
    }

    public Long getSequence(Session session, String seq) {
        StringBuilder sql = new StringBuilder().append(" select ").append(seq.toLowerCase()).append(".nextval from dual ");
        Query query = session.createSQLQuery(sql.toString());
        return Long.parseLong(query.uniqueResult().toString());
    }

    public Date getSysDateTime(Session session) throws ParseException {
        String strSysDateTime = getStrSysDateTime(session);
        if (strSysDateTime != null && !strSysDateTime.isEmpty()) {
            return DateTimeUtils.toDateddMMyyyyHHmmss(strSysDateTime);
        }
        return null;
    }

    public String getStrSysDateTime(Session session) {
        StringBuilder sql = new StringBuilder().append(" select to_char(sysdate, '").append(DateTimeUtils.DATE_PATTERN_ddMMyyyyHH24miss).append("') from dual ");
        Query query = session.createSQLQuery(sql.toString());
        Object obj = query.uniqueResult();
        if (obj != null) {
            return obj.toString();
        }
        return null;
    }

    public String createLike(String field) {
        return " (lower(" + field + ") like lower(?) escape '\\') ";
    }

    public String createLike(String field, String param) {
        return " (lower(" + field + ") like lower( :" + param + " ) escape '\\') ";
    }

    public String formatLike(String str) {
        return "%" + StringEscapeUtils.escapeSql(str.toLowerCase()).replaceAll("_", "\\\\_").replaceAll("%", "\\\\%") + "%";
    }

    public void buildParameter(Query query, List params) {
        if (query != null && params != null && !params.isEmpty()) {
            int n = params.size();
            for (int i = 0; i < n; i++) {
                query.setParameter(i, params.get(i));
            }
        }
    }

    public void buildParameter(Query query, HashMap params) {
        if (query != null && params != null && !params.isEmpty()) {
            for (Object key : params.keySet()) {
                query.setParameter((String) key, params.get(key));
            }
        }
    }

    public void rollBackTransactions(Session... sessions) {
        if (sessions != null && sessions.length > 0) {
            for (Session session : sessions) {
                if (session != null) {
                    Transaction transaction = session.getTransaction();
                    if (transaction != null) {
                        transaction.rollback();
                    }
                }
            }
        }
    }

    public void commitTransactions(Session... sessions) {
        if (sessions != null && sessions.length > 0) {
            for (Session session : sessions) {
                if (session != null && session.isOpen()) {
                    Transaction transaction = session.getTransaction();
                    if (transaction != null && transaction.isActive()) {
                        transaction.commit();
                    }
                }
            }
        }
    }

    public void flushSessions(Session... sessions) {
        if (sessions != null && sessions.length > 0) {
            for (Session session : sessions) {
                if (session != null) {
                    session.flush();
                }
            }
        }
    }

    public void beginTransactions(Session... sessions) {
        if (sessions != null && sessions.length > 0) {
            for (Session session : sessions) {
                if (session != null) {
                    session.beginTransaction();
                }
            }
        }
    }

    public void clearSessions(Session... sessions) {
        if (sessions != null && sessions.length > 0) {
            for (Session session : sessions) {
                if (session != null) {
                    session.clear();
                }
            }
        }
    }

    public void closeSessions(Session... sessions) {
        if (sessions != null && sessions.length > 0) {
            for (Session session : sessions) {
                if (session != null && session.isOpen()) {
                    session.close();
                }
            }
        }
    }

    public Long getServiceId(String alias) {
        if (alias == null) {
            return null;
        }
        alias = alias.trim().toUpperCase();
        if (Constants.SERVICE_ALIAS_ADSL.equals(alias)) {
            return Constants.SERVICE_ADSL_ID;
        }
        if (Constants.SERVICE_ALIAS_FTTH.equals(alias)) {
            return Constants.SERVICE_FTTH_ID;
        }
        if (Constants.SERVICE_ALIAS_LEASEDLINE.equals(alias)) {
            return Constants.SERVICE_LEASEDLINE_ID;
        }
        return null;
    }

    public Long getWebServiceId(String alias) {
        if (alias == null) {
            return null;
        }
        alias = alias.trim().toUpperCase();
        if (Constants.SERVICE_ALIAS_ADSL.equals(alias)) {
            return Constants.SERVICE_ADSL_ID_WEBSERVICE;
        }
        if (Constants.SERVICE_ALIAS_FTTH.equals(alias)) {
            return Constants.SERVICE_FTTH_ID_WEBSERVICE;
        }
        if (Constants.SERVICE_ALIAS_LEASEDLINE.equals(alias)) {
            return Constants.SERVICE_LEASELINE_ID_WEBSERVICE;
        }
        return null;
    }

    public String getAliasByWebServiceId(Long webServiceId) {
        if (webServiceId == null) {
            return null;
        }

        if (webServiceId.equals(Constants.SERVICE_MOBILE_ID_WEBSERVICE)) {
            return Constants.SERVICE_ALIAS_MOBILE;
        }
        if (webServiceId.equals(Constants.SERVICE_HOMEPHONE_ID_WEBSERVICE)) {
            return Constants.SERVICE_ALIAS_HOMEPHONE;
        }
        if (webServiceId.equals(Constants.SERVICE_PSTN_ID_WEBSERVICE)) {
            return Constants.SERVICE_ALIAS_PSTN;
        }
        if (webServiceId.equals(Constants.SERVICE_ADSL_ID_WEBSERVICE)) {
            return Constants.SERVICE_ALIAS_ADSL;
        }
        if (webServiceId.equals(Constants.SERVICE_FTTH_ID_WEBSERVICE)) {
            return Constants.SERVICE_ALIAS_FTTH;
        }
        if (webServiceId.equals(Constants.SERVICE_LEASELINE_ID_WEBSERVICE)) {
            return Constants.SERVICE_ALIAS_LEASEDLINE;
        }
        if (webServiceId.equals(Constants.SERVICE_OFFICE_WAN_ID_WEBSERVICE)) {
            return Constants.SERVICE_ALIAS_OFFICE_WAN;
        }
        if (webServiceId.equals(Constants.SERVICE_WHITE_LL_ID_WEBSERVICE)) {
            return Constants.SERVICE_ALIAS_WHITE_LL;
        }

        return null;
    }

    /*
     * @author vanghv1
     * @since 01/01/2014
     * return true if a != b
     */
    public boolean isNotEquals(Object a, Object b) {
        if (a == null && b == null) {
            return false;
        }
        if (a != null && b != null) {
            Class cA = a.getClass();
            Class cB = b.getClass();
            if (cA.equals(cB)) {
                if (a.equals(b)) {
                    return false;
                }
                if (cA.equals(String.class)) {
                    String strA = String.valueOf(a);
                    String strB = String.valueOf(b);
                    if (strA.trim().equals(strB.trim())) {
                        return false;
                    }
                }
            }
            if (cA.equals(java.sql.Timestamp.class) && cB.equals(java.util.Date.class)) {
                if (((java.sql.Timestamp) a).getTime() == ((java.util.Date) b).getTime()) {
                    return false;
                }
            }
            if (cA.equals(java.util.Date.class) && cB.equals(java.sql.Timestamp.class)) {
                if (((java.util.Date) a).getTime() == ((java.sql.Timestamp) b).getTime()) {
                    return false;
                }
            }
        }
        if (a != null && b == null && a.getClass().equals(String.class)) {
            if (String.valueOf(a).trim().isEmpty()) {
                return false;
            }
        }
        if (b != null && a == null && b.getClass().equals(String.class)) {
            if (String.valueOf(b).trim().isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public String StringValueOf(Object a) {
        if (a == null) {
            return null;
        }
        Class c = a.getClass();
        if (c.equals(java.util.Date.class) || c.equals(java.sql.Timestamp.class)) {
            return DateTimeUtils.formatddMMyyyyHHmmss(a);
        }
        return String.valueOf(a).trim();
    }
}
