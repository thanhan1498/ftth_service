package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.BusType;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class BusTypeDAO extends BaseDAO {

    public BusType findById(Session session, String busType) {
        if (busType == null) {
            return null;
        }
        busType = busType.trim();
        if (busType.isEmpty()) {
            return null;
        }

        String sql = " from BusType where busType = ?  and status = ? order by name ";
        Query query = session.createQuery(sql).setParameter(0, busType).setParameter(1, Constants.STATUS_USE);
        List<BusType> busTypes = query.list();
        if (busTypes != null && !busTypes.isEmpty()) {
            return busTypes.get(0);
        }

        return null;
    }
    

    public List<BusType> findActives(Session session) {
        String sql = " from BusType where status = ?  order by name ";
        Query query = session.createQuery(sql).setParameter(0, Constants.STATUS_USE);
        List<BusType> busTypes = query.list();
        return busTypes;
    }
}
