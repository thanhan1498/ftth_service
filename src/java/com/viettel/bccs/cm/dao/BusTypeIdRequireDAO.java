package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.BusTypeIdRequire;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class BusTypeIdRequireDAO extends BaseDAO {

    public List<BusTypeIdRequire> findByBusType(Session session, String busType) {
        if (busType == null) {
            return null;
        }
        busType = busType.trim();
        if (busType.isEmpty()) {
            return null;
        }

        String sql = " from BusTypeIdRequire where busType = ?  and status = ?  ";
        Query query = session.createQuery(sql).setParameter(0, busType).setParameter(1, Constants.STATUS_USE);
        List<BusTypeIdRequire> busTypeIdRequire = query.list();
        return busTypeIdRequire;
    }

    public boolean hasIdType(List<BusTypeIdRequire> idRequires, String idType) {
        boolean isExists = false;
        if (idRequires != null && !idRequires.isEmpty()) {
            for (BusTypeIdRequire idRequire : idRequires) {
                if (idRequire != null) {
                    Long type = idRequire.getIdType();
                    if (type != null && type.toString().trim().equals(idType)) {
                        isExists = true;
                        break;
                    }
                }
            }
        }   
        return isExists;
    }
}
