package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.ContractOffer;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.util.Constants;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ContractOfferDAO extends BaseDAO {

    public ContractOffer findById(Session session, Long contractId) {
        if (contractId == null || contractId <= 0L) {
            return null;
        }

        String sql = " from ContractOffer where contractId = ? and status = ?  ";
        Query query = session.createQuery(sql).setParameter(0, contractId).setParameter(1, Constants.STATUS_USE);
        List<ContractOffer> offers = query.list();
        if (offers != null && !offers.isEmpty()) {
            return offers.get(0);
        }

        return null;
    }

    public List<ContractOffer> findByContractId(Session session, Long contractId) {
        if (contractId == null || contractId <= 0L) {
            return null;
        }

        String sql = " from ContractOffer where contractId = ? ";
        Query query = session.createQuery(sql).setParameter(0, contractId);
        List<ContractOffer> offers = query.list();
        if (offers != null && !offers.isEmpty()) {
            return offers;
        }

        return null;
    }

    public static List getContractOfferBySubWithoutStatus(Session session, Long contractId, Long subId) throws Exception {
        if (subId == null) {
            return null;
        }
        if (contractId == null) {
            return null;
        }
        StringBuffer strQuery = new StringBuffer();
        strQuery.append("From ContractOffer Where 1=1 ");
        strQuery.append(" And contractId = ? ");
        strQuery.append(" And contractOfferId In( Select contractOfferId From OfferSub Where subId = ? ) ");
        Query query = session.createQuery(strQuery.toString());
        query.setParameter(0, contractId);
        query.setParameter(1, subId);

        List lstResult = query.list();
        return lstResult;

    }

    public List<ContractOffer> findBySub(Session session, SubAdslLeaseline sub) {
        if (sub == null) {
            return null;
        }
        Long subId = sub.getSubId();
        Long contractId = sub.getContractId();
        if (subId == null || subId <= 0L || contractId == null || contractId <= 0L) {
            return null;
        }

        StringBuilder sql = new StringBuilder()
                .append(" select co from ContractOffer co, OfferSub os where co.contractOfferId = os.contractOfferId ")
                .append(" and co.status = ? and co.contractId = ? and os.status = ? and os.subId = ? ");
        List params = new ArrayList();
        params.add(Constants.CONTRACT_OFFER_STATUS_USE);
        params.add(contractId);
        params.add(Constants.OFFER_SUB_STATUS_USE);
        params.add(subId);
        Query query = session.createQuery(sql.toString());
        buildParameter(query, params);

        List<ContractOffer> offers = query.list();
        return offers;
    }

    public ContractOffer getSingleContractOfferBySubId(Session session, Long subId) {
        if (subId == null || subId <= 0L) {
            return null;
        }

        StringBuilder sql = new StringBuilder()
                .append(" select co from ContractOffer co, OfferSub os where co.contractOfferId = os.contractOfferId ")
                .append(" and co.status = ? and co.groupId is null and os.status = ? and os.subId = ? ");
        List params = new ArrayList();
        params.add(Constants.CONTRACT_OFFER_STATUS_USE);
        params.add(Constants.OFFER_SUB_STATUS_USE);
        params.add(subId);
        Query query = session.createQuery(sql.toString());
        buildParameter(query, params);

        List<ContractOffer> offers = query.list();
        if (offers != null && !offers.isEmpty()) {
            return offers.get(0);
        }

        return null;
    }

    public void update(Session cmPosSession, SubAdslLeaseline sub) {
        List<ContractOffer> offers = new ContractOfferDAO().findBySub(cmPosSession, sub);
        if (offers != null && !offers.isEmpty()) {
            for (ContractOffer offer : offers) {
                if (offer != null) {
                    offer.setStatus(Constants.CONTRACT_OFFER_ACTIVE);

                    cmPosSession.update(offer);
                    cmPosSession.flush();
                }
            }
        }
    }
}
