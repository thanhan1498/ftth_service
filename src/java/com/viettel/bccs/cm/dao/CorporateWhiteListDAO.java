/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.CorporateWhiteList;
import com.viettel.bccs.cm.util.Constants;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author cuongdm
 */
public class CorporateWhiteListDAO {

    public List<CorporateWhiteList> findWhiteListTask(Session cmSession, String isdn) {
        return findWhiteList(cmSession, isdn, null, null, Constants.TYPE_CONF_WHITELIST_TASK);
    }

    public List<CorporateWhiteList> findWhiteListReason(Session cmSession) {
        return findWhiteList(cmSession, null, null, null, Constants.TYPE_CONF_WHITELIST_REASON);
    }

    private List<CorporateWhiteList> findWhiteList(Session cmSession, String isdn, String productCode, String serial, String typeConf) {
        String sql = "select isdn isdn,"
                + "   produc_code productCode,"
                + "   serial serial,"
                + "   type_conf typeConf"
                + " from CORPORATE_WHITE_LIST where status = 1 ";
        List param = new ArrayList();
        if (isdn != null && !isdn.isEmpty()) {
            sql += " and isdn = ? ";
            param.add(isdn);
        }
        if (productCode != null && !productCode.isEmpty()) {
            sql += " and produc_code = ? ";
            param.add(productCode);
        }
        if (serial != null && !serial.isEmpty()) {
            sql += " and serial = ? ";
            param.add(serial);
        }
        if (typeConf != null && !typeConf.isEmpty()) {
            sql += " and type_conf = ? ";
            param.add(typeConf);
        }
        Query query = cmSession.createSQLQuery(sql)
                .addScalar("isdn", Hibernate.STRING)
                .addScalar("productCode", Hibernate.STRING)
                .addScalar("serial", Hibernate.STRING)
                .addScalar("typeConf", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(CorporateWhiteList.class));
        for (int i = 0; i < param.size(); i++) {
            query.setParameter(i, param.get(i));
        }
        return query.list();
    }

    public void disabledWhiteListTask(Session cmSession, String isdn, String userUpdate) {
        String sql = " update CORPORATE_WHITE_LIST "
                + " set status = 0, "
                + "    UPDATE_DATE = sysdate, "
                + "    UPDATE_USER = ? "
                + " where isdn = ? "
                + "    and status = 1 "
                + "    and type_conf = ? ";
        Query query = cmSession.createSQLQuery(sql);
        query.setParameter(0, userUpdate);
        query.setParameter(1, isdn);
        query.setParameter(2, Constants.TYPE_CONF_WHITELIST_TASK);
        query.executeUpdate();
        cmSession.flush();
    }
}
