package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.CurrBillCycle;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class CurrBillCycleDAO extends BaseDAO {

    public CurrBillCycle findById(Session session, Long id) {
        if (id == null || id <= 0L) {
            return null;
        }

        String sql = " from CurrBillCycle where billCycleFrom = ? ";
        Query query = session.createQuery(sql).setParameter(0, id);
        List<CurrBillCycle> cycles = query.list();
        if (cycles != null && !cycles.isEmpty()) {
            return cycles.get(0);
        }

        return null;
    }
}
