/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.dao;

import com.lowagie.text.pdf.codec.Base64;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;
import javax.imageio.ImageIO;
import sun.misc.BASE64Decoder;

/**
 * CustImageAddTextToImageDAO
 *
 * @author phuonghc
 */
public class CustImageAddTextToImageDAO extends BaseDAO {

    public String addTextToImage(String dataImage, List<String> listText) throws Exception {
        try {
            BufferedImage image = null;
            byte[] imageByte;
            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(dataImage);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();

            //add text
            Graphics g = image.getGraphics();
            g.setFont(g.getFont().deriveFont(30f));

            for (int i = 0; i < listText.size(); i++) {
                g.drawString(listText.get(i), 50, i * 150 + 150);
            }
            g.dispose();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", os);

            return Base64.encodeBytes(os.toByteArray());
        } catch (Exception ex) {
            return dataImage;
        }
    }
}
