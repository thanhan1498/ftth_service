/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.dao;

import com.lowagie.text.pdf.codec.Base64;
import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.cm.supplier.SubFbConfigSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.common.util.FileSaveProcess;
import com.viettel.brcd.ws.model.input.ImageInput;
import com.viettel.brcd.ws.model.input.NewImageInput;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import sun.misc.BASE64Decoder;

/**
 *
 * @author cuongdm
 */
public class CustImageDetailDAO extends BaseDAO {

    public List<NewImageInput> findById(Session sessionCm, Long custImageId, String dirFolder, Logger logger, String dir) {

        List<NewImageInput> listImage = new ArrayList<NewImageInput>();
        String sql = " select IMAGE_NAME,IMAGE_TYPE,LATITUDE,LONGITUDE from cust_image_detail where CUST_IMG_ID = ? and status = 1 ";
        Query query = sessionCm.createSQLQuery(sql).setParameter(0, custImageId);
        List list = query.list();
        for (Object obj : list) {
            Object[] arr = (Object[]) obj;
            NewImageInput image = new NewImageInput();
            image.setImageName(arr[0] == null ? "" : arr[0].toString());
            image.setImageType(arr[1] == null ? "" : arr[1].toString());
            image.setLat(arr[2] == null ? "" : arr[2].toString());
            image.setLon(arr[3] == null ? "" : arr[3].toString());
            image.setImage(getDataImage(image.getImageName(), dirFolder, logger, dir));
            listImage.add(image);
        }
        return listImage;
    }

    private String getDataImage(String fileName, String dirFolder, Logger logger, String dir) {
        return new FileSaveProcess().getFileNotFtp(fileName, dirFolder, logger, dir);
    }

    public Long insertCustImage(Session cmPosSession, List<ImageInput> lstImage, String account, String imageType, Long subId, String createUser, Logger logger, String uploadDir, String lat, String lon) throws Exception {
        Long custImageId = Common.getSequence("CUST_IMAGE_SEQ", cmPosSession);
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        DateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        int count = 0;
        String imageDate = dateFormat.format(new Date());
        if (lstImage != null) {
            for (ImageInput imageInput : lstImage) {
                String image = imageInput.getImage();
                if ("".equals(imageInput.getImage())) {
                    continue;
                }
                if (lat != null && lon != null) {
                    try {
                        CustImageAddTextToImageDAO custAddTextToImageDao = new CustImageAddTextToImageDAO();
                        image = custAddTextToImageDao.addTextToImage(image, Arrays.asList("lat: " + lat + ", lon: " + lon, dateTimeFormat.format(new Date())));
                    } catch (Exception e) {
                        _log.error("### An error occured while add text to image" + e.getMessage());
                        image = imageInput.getImage();
                    }
                }
                String imageName = imageDate + "_" + account + "_" + count + "_" + custImageId + ".jpg";
                boolean checkUpload = new FileSaveProcess().saveFileNotFtp(imageName, image, imageType + "_" + account, logger, Constants.FTP_DIR_TASK);
                if (!checkUpload) {
                    logger.info("\n--------------\nUpload image Faile\n--------------\n");
                    continue;
                }

                int checkInsertDB = new SubFbConfigSupplier().insertImageDetail(cmPosSession, custImageId, imageName, imageType, null, createUser, lat, lon);
                if (checkInsertDB == 0) {
                    logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                }
                count++;
            }
            int checkInsertDB = new com.viettel.bccs.api.Task.DAO.CustomerDAO().insertCusImage(cmPosSession, subId, account, imageDate + "_" + account, imageType, createUser, custImageId);
            return checkInsertDB > 0 ? custImageId : null;
        } else {
            return 0l;
        }

    }

    /**
     * @author duyetdk
     * @param cmSession
     * @param custImageId
     * @param dirFolder
     * @param logger
     * @return
     * @throws Exception
     */
    public List<String> getCustImageLocation(Session cmSession, Long custImageId, String dirFolder, Logger logger) throws Exception {
        if (custImageId == null || custImageId == 0L) {
            return null;
        }
        List<String> listImage = new ArrayList<String>();
        String sql = "SELECT image_name as imageName "
                + " FROM cust_image_detail WHERE cust_img_id = ? AND status = ? ORDER BY create_date DESC ";
        Query query = cmSession.createSQLQuery(sql);
        query.setParameter(0, custImageId);
        query.setParameter(1, Constants.STATUS_USE);
        List list = query.list();
        if (!list.isEmpty()) {
            String imageName = list.get(0).toString();
            listImage.add(getDataImage(imageName, dirFolder, logger, Constants.FTP_DIR_TASK));
        }
        return listImage;
    }

    /**
     * @author duyetdk
     * @param cmSession
     * @param account
     * @return
     * @throws Exception
     */
    public List<String> getCustImageIdByAccount(Session cmSession, String account) throws Exception {
        try {
            if (account == null || account.trim().isEmpty()) {
                return null;
            }
            String sql = "SELECT cd.cust_img_id "
                    + "  FROM cust_image cu, cust_image_detail cd "
                    + "  WHERE cu.id = cd.cust_img_id AND cu.account = ? AND cd.image_type = ? AND cu.status = ? ORDER BY cu.create_date DESC ";
            Query query = cmSession.createSQLQuery(sql);
            query.setParameter(0, account.trim());
            query.setParameter(1, Constants.IMAGE_TYPE_LOCATION_CUSTOMER);
            query.setParameter(2, Constants.STATUS_USE);
            List lst = query.list();
            if (lst != null && !lst.isEmpty()) {
                return lst;
            }
        } catch (HibernateException ex) {
            System.out.println(ex);
        }
        return null;
    }

    /**
     * @author duyetdk
     * @param cmPosSession
     * @param lstImage
     * @param account
     * @param imageType
     * @param subId
     * @param createUser
     * @param logger
     * @param uploadDir
     * @param lat
     * @param lon
     * @return
     * @throws Exception
     */
    public Long insertCustImageLocation(Session cmPosSession, List<ImageInput> lstImage, String account, String imageType, Long subId, String createUser, Logger logger, String uploadDir, String lat, String lon) throws Exception {
        Long custImageId = Common.getSequence("CUST_IMAGE_SEQ", cmPosSession);
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        DateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        int count = 0;
        String imageDate = dateFormat.format(new Date());
        if (lstImage != null) {
            for (ImageInput imageInput : lstImage) {
                String image = imageInput.getImage();
                if ("".equals(imageInput.getImage())) {
                    continue;
                }
                /*if (lat != null && lon != null) {
                    try {
                        image = new CustImageAddTextToImageDAO().addTextToImage(image, Arrays.asList("lat: " + lat + "\n lon: " + lon, dateTimeFormat.format(new Date())));
                    } catch (Exception e) {
                        _log.error("### An error occured while add text to image" + e.getMessage());
                        image = imageInput.getImage();
                    }
                }*/
                String imageName = imageDate + "_" + account + "_" + count + "_" + custImageId + ".jpg";
                boolean checkUpload = new FileSaveProcess().saveFileNotFtp(imageName, image, imageType + "_" + account, logger, Constants.FTP_DIR_TASK);
                if (!checkUpload) {
                    logger.info("\n--------------\nUpload image Faile\n--------------\n");
                    continue;
                }

                int checkInsertDB = new SubFbConfigSupplier().insertImageDetail(cmPosSession, custImageId, imageName, imageType, null, createUser, lat, lon);
                if (checkInsertDB == 0) {
                    logger.info("\n--------------\nInsert DB fail image\n--------------\n");
                }
                count++;
            }
            int checkInsertDB = new com.viettel.bccs.api.Task.DAO.CustomerDAO().insertCusImage(cmPosSession, subId, account, imageDate + "_" + account, imageType, createUser, custImageId);
            return checkInsertDB > 0 ? custImageId : null;
        } else {
            return 0l;
        }
    }
//    /**
//     * @author duyetdk
//     * @param dataImage
//     * @param listText
//     * @return
//     * @throws Exception
//     */
//    private String addTextToImageLocation(String dataImage, List<String> listText) throws Exception {
//        try {
//            BufferedImage image = null;
//            byte[] imageByte;
//            BASE64Decoder decoder = new BASE64Decoder();
//            imageByte = decoder.decodeBuffer(dataImage);
//            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
//            image = ImageIO.read(bis);
//            bis.close();
//
//            //add text
//            Graphics g = image.getGraphics();
//            g.setFont(g.getFont().deriveFont(40f));
//
//            for (int i = 0; i < listText.size(); i++) {
//                g.drawString(listText.get(i), 50, i * 150 + 150);
//            }
//            g.dispose();
//            ByteArrayOutputStream os = new ByteArrayOutputStream();
//            ImageIO.write(image, "jpg", os);
//
//            return Base64.encodeBytes(os.toByteArray());
//        } catch (Exception ex) {
//        }
//        return dataImage;
//    }
}
