package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.Customer;
import com.viettel.brcd.util.QueryUtil;
import com.viettel.brcd.util.Validator;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.ws.model.input.CustomerIn;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class CustomerDAO extends BaseDAO {

    public Customer findById(Session session, Long custId) {
        if (custId == null || custId <= 0L) {
            return null;
        }

        String sql = " from Customer where custId = ? ";
        Query query = session.createQuery(sql).setParameter(0, custId);
        List<Customer> customers = query.list();
        if (customers != null && !customers.isEmpty()) {
            return customers.get(0);
        }

        return null;
    }

    public Customer findByIdNo(Session session, String idNo) {
        if (idNo == null) {
            return null;
        }
        idNo = idNo.trim();
        if (idNo.isEmpty()) {
            return null;
        }

        String sql = " from Customer where idNo = ? and correctCus = ? ";
        Query query = session.createQuery(sql).setParameter(0, idNo).setParameter(1, Constants.CORRECT_CUSTOMER);
        List<Customer> customers = query.list();
        if (customers != null && !customers.isEmpty()) {
            return customers.get(0);
        }

        return null;
    }

    public List<CustomerIn> getListCustomer(Session cmPosSession, String idNo, String isdn, String account, Long startRow, Long maxRow) {
        if (!Validator.isNotNull(idNo) && !Validator.isNotNull(isdn) && !Validator.isNotNull(account) && !Validator.isNotNull(startRow) && !Validator.isNotNull(maxRow)) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        sb.append(" select distinct cr.cust_id as custId, cr.bus_type as customType, cr.name ");
        sb.append(" , TO_CHAR(cr.birth_date, 'dd-MM-yyyy') as birthDate ");
        sb.append(" , cr.id_no as idNo, cr.bus_permit_no as busPermitNo, cr.id_issue_place as idIssuePlace ");
        sb.append(" , TO_CHAR(cr.id_issue_date, 'dd-MM-yyyy') as idIssueDate ");
        sb.append(" , cr.address, cr.id_type as idType, cr.sex ");
        sb.append(" , cr.province as provinceCode, cr.district as districtCode ");
        sb.append(" , cr.precinct as precinctCode, cr.street_block_name as streetBlockName ");
        sb.append(" , x_location as lat, y_location as lng ");
        sb.append(" from customer cr, contract c, all_tel_service_sub s where cr.status = 1 ");
        sb.append(" and cr.cust_id = c.cust_id(+) and c.contract_id = s.contract_id(+) ");

        if (Validator.isNotNull(idNo)) {
            sb.append("and (cr.id_no = :idNo or cr.bus_permit_no = :idNo) ");
        }
        if (Validator.isNotNull(isdn) || Validator.isNotNull(account)) {
            sb.append("and (s.isdn like :isdn or s.account like :account) ");
        }

        sb.append("order by cr.name asc ");
        SQLQuery sql = cmPosSession.createSQLQuery(sb.toString());
        if (Validator.isNotNull(idNo)) {
            sql.setParameter("idNo", idNo);
        }
        if (Validator.isNotNull(isdn) && Validator.isNotNull(account)) {
            sql.setParameter("isdn", QueryUtil.getFullStringParam(isdn.toUpperCase()));
            sql.setParameter("account", QueryUtil.getFullStringParam(account.toUpperCase()));
        } else if (Validator.isNotNull(isdn) || Validator.isNotNull(account)) {

            sql.setParameter("isdn", QueryUtil.getFullStringParam(isdn.toUpperCase()));
            sql.setParameter("account", QueryUtil.getFullStringParam(isdn.toUpperCase()));

        }

        sql.addScalar("custId", Hibernate.LONG);
        sql.addScalar("customType", Hibernate.STRING);
        sql.addScalar("name", Hibernate.STRING);
        sql.addScalar("birthDate", Hibernate.STRING);
        sql.addScalar("idNo", Hibernate.STRING);
        sql.addScalar("busPermitNo", Hibernate.STRING);
        sql.addScalar("idIssuePlace", Hibernate.STRING);
        sql.addScalar("idIssueDate", Hibernate.STRING);
        sql.addScalar("address", Hibernate.STRING);
        sql.addScalar("idType", Hibernate.LONG);
        sql.addScalar("sex", Hibernate.STRING);
        sql.addScalar("provinceCode", Hibernate.STRING);
        sql.addScalar("districtCode", Hibernate.STRING);
        sql.addScalar("precinctCode", Hibernate.STRING);
        sql.addScalar("streetBlockName", Hibernate.STRING);
        sql.addScalar("lng", Hibernate.STRING);
        sql.addScalar("lat", Hibernate.STRING);

        sql.setResultTransformer(Transformers.aliasToBean(CustomerIn.class));
        if (startRow != null) {
            sql.setFirstResult(startRow.intValue());
        }
        if (maxRow != null) {
            sql.setMaxResults(maxRow.intValue());
        }

        List<CustomerIn> results = (List<CustomerIn>) sql.list();
        return results;
    }

    public boolean isUsedIdNo(Session session, String idNo) {
        Customer cus = findByIdNo(session, idNo);
        return cus != null;
    }
}
