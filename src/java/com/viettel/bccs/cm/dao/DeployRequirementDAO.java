package com.viettel.bccs.cm.dao;

import com.viettel.bccs.api.Task.BO.SubOwWhiteLl;
import com.viettel.bccs.api.Task.BO.SubPstn;
import com.viettel.bccs.bo.cm.pos.Subscriber;
import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.model.DeployRequirement;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.util.LabelUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class DeployRequirementDAO extends BaseDAO {

    public DeployRequirementDAO() {
        _log = Logger.getLogger(DeployRequirementDAO.class);
    }

    public List<DeployRequirement> findBySubId(Session session, Long subId) {
        if (subId == null || subId <= 0L) {
            return null;
        }
        String sql = " from DeployRequirement where subId = ? ";
        Query query = session.createQuery(sql).setParameter(0, subId);
        List<DeployRequirement> requirements = query.list();
        return requirements;
    }

    public void updateStatusBySubId(Session session, Long subId, Long status) {
        List<DeployRequirement> deployRequirements = new DeployRequirementDAO().findBySubId(session, subId);
        if (deployRequirements != null && !deployRequirements.isEmpty()) {
            for (DeployRequirement deployRequirement : deployRequirements) {
                if (deployRequirement != null) {
                    if (Constants.DEPLOY_REQ_TYPE_NEW_CONTRACT.equals(deployRequirement.getReqType())) {
                        deployRequirement.setStatus(status);

                        info(_log, "DeployRequirementDAO.updateStatusBySubId:start reqId=" + deployRequirement.getReqId());
                        session.update(deployRequirement);
                        session.flush();
                        info(_log, "DeployRequirementDAO.updateStatusBySubId:end reqId=" + deployRequirement.getReqId());
                    }
                }
            }
        }
    }

    public void updateStatusBySubId(Session session, Long subId, Long status, Long actionAuditId, Date issueDateTime) {
        List<DeployRequirement> deployRequirements = new DeployRequirementDAO().findBySubId(session, subId);
        if (deployRequirements != null && !deployRequirements.isEmpty()) {
            ActionDetailBussiness detailBussiness = new ActionDetailBussiness();
            for (DeployRequirement deployRequirement : deployRequirements) {
                if (deployRequirement != null) {
                    if (Constants.DEPLOY_REQ_TYPE_NEW_CONTRACT.equals(deployRequirement.getReqType())) {
                        Object oldValue = deployRequirement.getStatus();
                        deployRequirement.setStatus(status);
                        Object newValue = deployRequirement.getStatus();
                        detailBussiness.insert(session, actionAuditId, ReflectUtils.getTableName(DeployRequirement.class), deployRequirement.getReqId(), ReflectUtils.getColumnName(DeployRequirement.class, "status"), oldValue, newValue, issueDateTime);

                        info(_log, "DeployRequirementDAO.updateStatusBySubId:start reqId=" + deployRequirement.getReqId());
                        session.update(deployRequirement);
                        session.flush();
                        info(_log, "DeployRequirementDAO.updateStatusBySubId:end reqId=" + deployRequirement.getReqId());
                    }
                }
            }
        }
    }

    public void addDeployRequirementForChangeAdd(Session sessionCmPos, Object subscriberSource, DeployRequirement deployRequirement,
            Date nowDate, Long actionAuditId, String locale) {
        try {
            Long depRequirementId = getSequence(sessionCmPos, Constants.DEPLOY_REQUIREMENT_SEQ);
            deployRequirement.setReqId(depRequirementId);
            deployRequirement.setReqDatetime(nowDate);
            deployRequirement.setName("None_Name");
            deployRequirement.setStatus(Constants.DEPLOY_REQ_STATUS_REQUESTED);
            deployRequirement.setIssueDatetime(nowDate);
            deployRequirement.setReqType(Constants.DEPLOY_REQ_TYPE_CHANGE_DEPLOY_AREA);
            deployRequirement.setDescription(LabelUtil.getKey("request.description", locale));
            deployRequirement.setCreateDate(nowDate);

            if (subscriberSource instanceof SubAdslLeaseline) {
                SubAdslLeaseline subscriber = (SubAdslLeaseline) subscriberSource;
                if (StringUtils.isNotEmpty(subscriber.getUserUsing())) {
                    deployRequirement.setName(subscriber.getUserUsing());
                }
                deployRequirement.setSubId(subscriber.getSubId());
                deployRequirement.setProductCode(subscriber.getProductCode());
                deployRequirement.setServiceType(subscriber.getServiceType());
            } else if (subscriberSource instanceof SubPstn) {
                SubPstn subPstn = (SubPstn) subscriberSource;
                if (StringUtils.isNotEmpty(subPstn.getUserUsing())) {
                    deployRequirement.setName(subPstn.getUserUsing());
                }
                deployRequirement.setSubId(subPstn.getSubId());
                deployRequirement.setProductCode(subPstn.getProductCode());
                deployRequirement.setServiceType(subPstn.getServiceType());
            } else if (subscriberSource instanceof SubOwWhiteLl) {
                SubOwWhiteLl subOwWhiteLl = new SubOwWhiteLl();
                if (StringUtils.isNotEmpty(subOwWhiteLl.getUserUsing())) {
                    deployRequirement.setName(subOwWhiteLl.getUserUsing());
                }
                deployRequirement.setSubId(subOwWhiteLl.getSubId());
                deployRequirement.setProductCode(subOwWhiteLl.getProductCode());
                deployRequirement.setServiceType(subOwWhiteLl.getServiceType());
            }

            info(_log, "DeployRequirementDAO.addDeployRequirementForChangeAdd:start reqId=" + deployRequirement.getReqId());
        } catch (Exception e) {
            _log.error("### An error occured while doing addDeployRequirementForChangeAdd: ", e);
        }
    }

    public boolean checkNumberRequestChangeAddrTask(Session sessionCmPos, Long subId, String serviceAlias) {
        boolean isRequestAlready = false;
        try {
            String strQuery = " from DeployRequirement where subId = ? and ( status = ? or status = ? ) and reqType = ? ";
            Query query = sessionCmPos.createQuery(strQuery);
            query.setParameter(0, subId);
            query.setParameter(1, Constants.DEPLOY_REQ_STATUS_REQUESTED);//Yeu cau moi
            query.setParameter(2, Constants.DEPLOY_REQ_STATUS_CHANGE_PORT_SUCCESS);//Doi DCLD sau do doi port thanh cong
            query.setParameter(3, Constants.DEPLOY_REQ_TYPE_CHANGE_DEPLOY_AREA);//Ban ghi thay doi dia chi lap dat
            List listResult = query.list();
            if (listResult != null && listResult.size() > 0) {
                isRequestAlready = true;
            }
        } catch (Exception e) {
            _log.error("### An error occurred while checkNumberRequestChangeAddrTask", e);
        }
        return isRequestAlready;
    }
}
