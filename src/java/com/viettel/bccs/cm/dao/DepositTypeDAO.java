package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.DepositType;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class DepositTypeDAO extends BaseDAO {

    public List<DepositType> findByServiceAndCode(Session session, Long telecomServiceId, String code) {
        if (telecomServiceId == null || telecomServiceId <= 0L || code == null) {
            return null;
        }
        code = code.trim();
        if (code.isEmpty()) {
            return null;
        }
        String sql = " from DepositType where telecomServiceId = ? and code = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, telecomServiceId).setParameter(1, code).setParameter(2, Constants.STATUS_USE.toString());
        return query.list();
    }

    public Long getDepositTypeId(Session session, Long telecomServiceId, String code) {
        List<DepositType> types = findByServiceAndCode(session, telecomServiceId, code);
        if (types != null && !types.isEmpty()) {
            for (DepositType type : types) {
                if (type != null) {
                    Long depositTypeId = type.getDepositTypeId();
                    if (depositTypeId != null) {
                        return depositTypeId;
                    }
                }
            }
        }
        return null;
    }
}
