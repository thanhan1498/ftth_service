package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.Domain;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class DomainDAO extends BaseDAO {

    public List<Domain> findActives(Session session) {
        String sql = " from Domain where status = ? ";
        Query query = session.createQuery(sql).setParameter(0, Constants.STATUS_USE);
        return query.list();
    }
}
