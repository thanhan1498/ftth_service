package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.InvoiceListStaff;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import org.hibernate.Session;

/**
 * @author vietnn6
 */
public class InvoiceListStaffDAO extends BaseDAO {

    public InvoiceListStaff insert(Session session, Long staffId, String staffCode, String currInvoiceNo, String regType, Double payAdvAmount, Date insertDate, Long contractId, Long invoiceListId, Double promotion) {
        InvoiceListStaff invoice = new InvoiceListStaff();

        Long id = getSequence(session, Constants.INVOICE_LIST_STAFF_SEQ);
        invoice.setId(id);
        invoice.setStaffId(staffId);
        invoice.setStaffCode(staffCode);
        invoice.setCurrInvoiceNo(currInvoiceNo);
        invoice.setInsertDate(insertDate);
        invoice.setCurrInvoiceNo(currInvoiceNo);
        invoice.setRegType(regType);
        invoice.setPayAdvAmount(payAdvAmount);
        invoice.setContractId(contractId);
        invoice.setInvoiceListId(invoiceListId);
        invoice.setPromotion(promotion == null ? 0 : promotion);
        invoice.setStatus(Constants.STATUS_USE);

        session.save(invoice);
        session.flush();
        return invoice;
    }
}
