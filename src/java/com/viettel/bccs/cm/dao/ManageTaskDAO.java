/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.dao;

import com.viettel.bccs.api.Task.BO.CableBoxDAO;
import com.viettel.bccs.api.Task.DAO.TaskHistoryDAO;
import com.viettel.bccs.api.Util.EditorUtils;
import com.viettel.bccs.cm.common.util.Common;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubDeployment;
import com.viettel.bccs.cm.model.TaskManagement;
import com.viettel.bccs.cm.model.TaskShopManagement;
import com.viettel.bccs.cm.model.TaskStaffManagement;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.sync.evn.common.util.DateTimeUtils;
import com.viettel.cc.client.bean.InfoComplain;
import com.viettel.cc.database.DAO.CommonActionDatabase;
import com.viettel.bccs.api.Task.BO.CableBox;
import com.viettel.bccs.api.Task.DAO.SendMessageDAO;
import com.viettel.brcd.util.LabelUtil;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;

/**
 * ManageTaskDAO
 *
 * @author phuonghc
 */
public class ManageTaskDAO {

    public String assignTaskForStaffManageConnector(Session sessionCmPos, Session sessionCc, String startDate, String endDate, Long staffId, Long actionAuditId, Date nowDate, Long reasonId,
            Long taskShopMngtId, String telToAssign, Long userCreateId, String shopCode, String loginName, String locale) throws Exception {
        InetAddress inetAddress = InetAddress.getLocalHost();
        String ip = inetAddress.getHostAddress();
        /*Số dt của NV để nhắn tin giao việc*/
        String staffIsdn = (telToAssign != null) ? telToAssign : "";
        TaskShopManagement taskShop = new TaskShopManagementDAO().findByTaskShopId(sessionCmPos, taskShopMngtId);
        if (taskShop == null) {
            /*Giao việc thất bại do công việc không thuộc tổ đội.*/
            return LabelUtil.getKey("a0881.assign.fail.no.shop", locale);
        }
        TaskManagement task = new TaskManagementDAO().findById(sessionCmPos, taskShop.getTaskMngtId());
        if (task == null) {
            /*Giao việc thất bại do không tìm được thông tin công việc.*/
            return "a0871.assign.fail.no.task.info";
        }

        if (Constants.TASK_PROGRESS_END.equals(task.getProgress())) {
            /*Công việc đã được nghiệm thu, bạn không được phép thực hiện giao việc.*/
            return "a0872.progress.2.cannot.assign";
        }

        String strDes = "";
        String actionCode = "";

        if (taskShop.getProgress().equals(Constants.TASK_SHOP_PROGRESS_START)) {
            /*Cập nhật tiến độ cho task_shop_managemet trong trường hợp giao việc mới*/
            String staffNew = "";
            /*Lấy StaffCode mới để ghi lịch sử*/
            Staff staff = new StaffDAO().findById(sessionCmPos, staffId);
            if (staff != null) {
                staffNew = staff.getStaffCode();
            }
            /*Cập nhật triển khai, giao việc cho NV*/
            strDes = "update.task.assign.to.staff" + staffNew;
            actionCode = Constants.ACTION_TASK_ASSIGN_TO_STAFF;
            /*Ghi log việc thay đổi tiến độ công việc*/
            Common.logAuditDetailV2(taskShopMngtId, actionAuditId, "TASK_SHOP_MNGT", "PROGRESS", taskShop.getProgress(),
                    Constants.TASK_SHOP_PROGRESS_ONGOING, nowDate, sessionCmPos);
            taskShop.setProgress(Constants.TASK_SHOP_PROGRESS_ONGOING);
            sessionCmPos.update(taskShop);
        }

        if (Constants.TASK_REQ_TYPE_RECOVER_SUBSCRIBER.equals(task.getReqType())) {
            strDes = "[KP] " + strDes;
        }

        if (Constants.TASK_REQ_TYPE_CHANGE_EQUIPMENT.equals(task.getReqType())) {
            strDes = "[TDTB] " + strDes;
        }

        /*Tạo mới bản ghi TASK_STAFF_MNGT*/
        TaskStaffManagement taskStaff = new TaskStaffManagement();
        taskStaff.setStatus(Constants.STATUS_USE);
        taskStaff.setTaskShopMngtId(taskShopMngtId);
        taskStaff.setStaffId(staffId);
        taskStaff.setProgress(Constants.TASK_STAFF_PROGRESS_START);
        Staff staff = new StaffDAO().findById(sessionCmPos, staffId);
        if (staff != null) {
            /*Giao nhân viên, người thao tác:*/
            taskStaff.setDescription("assign.staff.use" + staff.getName());
        }

        Date endDateTime = DateTimeUtils.convertStringToTime(endDate, "dd/MM/yyyy HH:mm");
        taskStaff.setStaDate(nowDate);
        taskStaff.setFinishDate(endDateTime);

        Date staDateValue = DateTimeUtils.convertStringToDate(startDate);
        Date endDateValue = DateTimeUtils.convertStringToDate(endDate);

        Long ms = endDateValue.getTime() - staDateValue.getTime();
        Long day = ms / (1000 * 60 * 60 * 24);
        taskStaff.setDuration(day);
        taskStaff.setUserId(userCreateId);

        if (task.getSourceType().equals(Constants.TASK_SOURCE_TYPE) || Constants.TASK_REQ_TYPE_KS_OW.equals(task.getReqType()) || Constants.TASK_REQ_TYPE_KS_FLOW_WHITE_LL.equals(task.getReqType())) {
            taskStaff.setStageId(Constants.JOB_STAGE_NEW_CONTRACT);
        } else if (task.getSourceType().equals(Constants.TASK_SOURCE_TYPE_CC)) {
            taskStaff.setStageId(Constants.JOB_STAGE_COMPLAINT);
        } else if (task.getSourceType().equals(Constants.TASK_SOURCE_TYPE_ACTUAL_SURVEY)) {
            taskStaff.setStageId(Constants.JOB_STAGE_ACTUAL_SURVEY);
        }

        sessionCmPos.save(taskStaff);
        /*Ghi log việc tạo mới bản ghi TaskStaffManagement*/
        Common.logAuditDetailV2(taskStaff.getTaskStaffMngtId(), actionAuditId, "TASK_STAFF_MNGT", "ID",
                null, taskStaff.getTaskStaffMngtId(), nowDate, sessionCmPos);
        /*Ghi lịch sử CV*/
        TaskHistoryDAO.saveTaskHistory(sessionCmPos, task.getTaskMngtId(), nowDate, strDes, actionCode, reasonId);
        /*Gửi tin nhắn cho nhân viên được giao*/
        if (StringUtils.isNotEmpty(staffIsdn)) {
            String sendMesResult = this.sendMessage(sessionCmPos, sessionCc, staffIsdn, task, shopCode, loginName, ip, locale);
            if (StringUtils.isNotEmpty(sendMesResult)) {
                /*Gửi tn thất bại (Giao việc vẫn thành công nhưng ko gửi được tn cho NV)*/
                return "1";
            }
        } else {
            /*Không tìm được số đ/t (Giao việc vẫn thành công nhưng ko gửi được tn cho NV)*/
            return "1";
        }
        /*Thành công*/
        return "0";
    }

    private String sendMessage(Session sessionCmPos, Session sessionCc, String tel, TaskManagement task, String shopCode, String loginName, String ip, String locale) {
        String separator = ";";
        StringBuilder content = new StringBuilder();
        /*Nếu đổi địa chỉ lắp đặt: <Mô tả> := Đổi đ/c lắp đặt*/
        if ("1".equals(task.getSourceType())) {
            content.append("XLSC:");
        } else if (Constants.TASK_SOURCE_TYPE_ACTUAL_SURVEY.equals(task.getSourceType())) {
            content.append("ACTUAL SURVEY:");
        } else {
            content.append("[BCCS]").append(separator);
        }
        SubAdslLeaseline sub = new SubAdslLeaselineDAO().findById(sessionCmPos, task.getSubId());
        if (sub != null) {
            if (Constants.SERVICE_ALIAS_LEASEDLINE.equals(sub.getServiceType())) {
                content.append("Leasedline").append(separator);
            }
        }
        content.append(EditorUtils.convertUnicode2Nosign((task.getName() != null) ? task.getName().trim() : " "));
        content.append(separator);
        content.append(EditorUtils.convertUnicode2Nosign((task.getDeployAddress() != null) ? task.getDeployAddress().trim() : " "));
        content.append(separator);
        content.append(EditorUtils.convertUnicode2Nosign((task.getAccount() != null) ? task.getAccount().trim() : " "));
        content.append(separator);
        content.append((task.getTelFax() != null) ? task.getTelFax() : " ");
        content.append(separator);

        if ("1".equals(task.getSourceType())) {
            String priorityName = null;
            InfoComplain infoComplain = CommonActionDatabase.viewLimitCustDate(sessionCc, String.valueOf(task.getCustReqId()));
            priorityName = infoComplain.getPriorityName();
            if (priorityName != null) {
                content.append(EditorUtils.convertUnicode2Nosign(priorityName));
            }
            content.append(separator);
            List<SubDeployment> listSubDeployment = new ArrayList<SubDeployment>();
            if (!"W".equals(task.getServiceType())) {
                listSubDeployment = new SubDeploymentDAO().findBySubId(sessionCmPos, task.getSubId());
            } else {
                listSubDeployment = new SubDeploymentDAO().findBySubIdAndSourceId(sessionCmPos, task.getSubId(), task.getSourceId());
            }
            Long cableBoxId = null;
            Long portNo = 0L;
            if (listSubDeployment != null && !listSubDeployment.isEmpty()) {
                cableBoxId = listSubDeployment.get(0).getCableBoxId();
                portNo = listSubDeployment.get(0).getPortNo();
            }
            String cableBoxCode = null;
            if (cableBoxId != null && cableBoxId > 0) {
                CableBox cableBox = new CableBoxDAO().findById(cableBoxId, sessionCmPos);
                if (cableBox != null) {
                    cableBoxCode = cableBox.getCode();
                }
            }

            if (cableBoxCode != null) {
                content.append(cableBoxCode);
            }
            content.append(separator);

            if (portNo != null) {
                content.append(portNo);
            }
            content.append(separator);

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String strDate = null;
            if (task.getLimitCustDate() != null) {
                strDate = dateFormat.format(task.getLimitCustDate());
            }

            if (strDate != null) {
                content.append(strDate);
            }
        }

        /*2 Cong viec khao satOW,1 Thay doi dia chi lap dat, 0 Dau noi moi*/
        if (Constants.TASK_REQ_TYPE_CHANGE_DEPLOY_ADDRESS.equals(task.getReqType())) {
            content.append("manage.task.ap.18"); // "Doi d/c lap dat"
        }
        content.append(".");
        /*Xử lý tin nhắn, nếu lớn hơn 160 ký tự thì gửi thành nhiều tin*/
        String strContent = content.toString();
        int mesLenght = strContent.length();
        List<String> lstMessage = new ArrayList();
        while (mesLenght > 160) {
            String temp = strContent.substring(0, 160);
            int lastSpace = temp.lastIndexOf(" ");
            lstMessage.add(temp.substring(0, lastSpace - 1));
            strContent = strContent.substring(lastSpace + 1);
            mesLenght = mesLenght - strContent.length();
        }
        lstMessage.add(strContent);
        SendMessageDAO mesDAO = new SendMessageDAO();
        for (String message : lstMessage) {
            if (!mesDAO.sendOneMess(loginName, shopCode, tel, message, ip, sessionCc, sessionCmPos)) {
                return "1";
                /*Gửi tin nhắn thất bại*/

            }
        }
        return null;
    }
}
