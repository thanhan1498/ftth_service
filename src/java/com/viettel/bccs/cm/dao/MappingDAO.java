package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.Mapping;
import com.viettel.bccs.cm.model.pre.MappingPre;
import com.viettel.bccs.cm.util.Constants;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class MappingDAO extends BaseDAO {

    public Mapping findById(Session session, Long id) {
        if (id == null || id <= 0L) {
            return null;
        }

        String sql = " from Mapping where billCycleFrom = ? ";
        Query query = session.createQuery(sql).setParameter(0, id);
        List<Mapping> cycles = query.list();
        if (cycles != null && !cycles.isEmpty()) {
            return cycles.get(0);
        }

        return null;
    }

    public Mapping findMapping(Session session, Long serviceId, Long reasonId, String productCode, String actionCode, String vasCode) {
        if (reasonId == null || actionCode == null) {
            return null;
        }
        actionCode = actionCode.trim();
        if (actionCode.isEmpty()) {
            return null;
        }

        StringBuilder sql = new StringBuilder()
                .append(" select m from Mapping m, Reason r ")
                .append(" where m.reasonId = r.reasonId and m.status = ? and r.status = ? and m.channel is null and m.reasonId = ? and m.actionCode = ? ");
        List params = new ArrayList();
        params.add(Constants.STATUS_USE);
        params.add(Constants.STATUS_USE);
        params.add(reasonId);
        params.add(actionCode);
        if (vasCode != null) {
            sql.append(" and m.vas = ? ");
            params.add(vasCode);
        }
        if (serviceId == null) {
            sql.append(" and m.telServiceId is null ");
        } else {
            sql.append(" and m.telServiceId = ? ");
            params.add(serviceId);
        }

        if (productCode != null) {
            productCode = productCode.trim();
        }
        if (productCode == null || productCode.isEmpty()) {
            sql.append(" and m.productCode is null ");
        } else {
            sql.append(" and m.productCode = ? ");
            params.add(productCode);
        }

        Query query = session.createQuery(sql.toString());
        buildParameter(query, params);
        List<Mapping> mappings = query.list();
        if (mappings != null && !mappings.isEmpty()) {
            for (Mapping mapping : mappings) {
                if (mapping != null) {
                    return mapping;
                }
            }
        }
        return null;
    }

    public String getSaleServiceCode(Session session, Long serviceId, Long reasonId, String productCode, String actionCode, String vasCode) {
        Mapping mapping = findMapping(session, serviceId, reasonId, productCode, actionCode, vasCode);
        if (mapping != null) {
            return mapping.getSaleServiceCode();
        }
        return null;
    }

    public String getSaleServiceCodePre(Session session, Long serviceId, Long reasonId, String productCode, String actionCode) {
        MappingPre mapping = findMappingPre(session, serviceId, reasonId, productCode, actionCode);
        if (mapping != null) {
            return mapping.getSaleServiceCode();
        }
        return null;
    }

    public MappingPre findMappingPre(Session session, Long serviceId, Long reasonId, String productCode, String actionCode) {
        if (reasonId == null || actionCode == null) {
            return null;
        }
        actionCode = actionCode.trim();
        if (actionCode.isEmpty()) {
            return null;
        }

        StringBuilder sql = new StringBuilder()
                .append(" select m from MappingPre m, ReasonPre r ")
                .append(" where m.reasonId = r.reasonId and m.status = ? and r.status = ? and m.channel is null and m.reasonId = ? and m.actionCode = ? ");
        List params = new ArrayList();
        params.add(Constants.STATUS_USE);
        params.add(Constants.STATUS_USE);
        params.add(reasonId);
        params.add(actionCode);

        if (serviceId == null) {
            sql.append(" and m.telServiceId is null ");
        } else {
            sql.append(" and m.telServiceId = ? ");
            params.add(serviceId);
        }

        if (productCode != null) {
            productCode = productCode.trim();
        }
        if (productCode == null || productCode.isEmpty()) {
            sql.append(" and m.productCode is null ");
        } else {
            sql.append(" and m.productCode = ? ");
            params.add(productCode);
        }

        Query query = session.createQuery(sql.toString());
        buildParameter(query, params);
        List<MappingPre> mappings = query.list();
        if (mappings != null && !mappings.isEmpty()) {
            for (MappingPre mapping : mappings) {
                if (mapping != null) {
                    return mapping;
                }
            }
        }
        return null;
    }
}
