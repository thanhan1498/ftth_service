/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.dao;

import com.google.gson.Gson;
import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.cm.model.NotificationLog;
import com.viettel.bccs.cm.model.Token;
import com.viettel.bccs.cm.supplier.TokenSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.ws.model.output.DataNotification;
import com.viettel.brcd.ws.supplier.Notification.SendMulticastMessage;
import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.brcd.ws.common.WebServiceClient;
import com.viettel.brcd.ws.supplier.Notification.PushNotificationResponse;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class NotificationMessageDAO extends BaseDAO {

    private static WebServiceClient wsClient = new WebServiceClient();

    public void generateNotificationData(Session cmSession, Long receiverStaffId, String receiverStaffCode, String createUser, String title, String message,
            String account, Long taskMngtId, Long taskStaffMngtId, String actionType) throws Exception {
        /*get device id of user*/
        List<Token> lstToken = new TokenSupplier().findByStaffId(cmSession, receiverStaffId);
        if (lstToken != null && !lstToken.isEmpty() && lstToken.get(0).getDeviceId() != null) {

            DataNotification data = new DataNotification(lstToken.get(0).getDeviceId(), "MBCCS", title, message, account, taskMngtId, taskStaffMngtId, actionType);
            NotificationLog notification = new NotificationLog(receiverStaffCode, receiverStaffId, lstToken.get(0).getDeviceId(), createUser, message.replaceAll(Constants.NEW_LINE, "\n"), title, account, taskMngtId);
            pushNotification(cmSession, data, notification);
        }
    }

    public void pushNotification(Session cmSession, DataNotification data, NotificationLog notification) throws Exception {
        PushNotificationResponse response;
        try {
            response = pushNotification(data);
        } catch (Exception ex) {
            response = new PushNotificationResponse();
            response.setReturn(ex.getMessage());
        }
        Long status = "success".equalsIgnoreCase(response.getReturn()) ? 1l : 0l;
        String strResponse = response.getReturn();
        String temp = new Gson().toJson(data);
        String StrRequest = temp;
        if (response.getReturn() != null && response.getReturn().length() > 1000) {
            strResponse = response.getReturn().substring(0, 900);
        }
        if (temp != null && temp.length() > 1000) {
            StrRequest = temp.substring(0, 900);
        }
        notification.setStatus(status);
        notification.setResponse(strResponse);
        notification.setRequest(StrRequest);
        notification.setId(Common.getSequence("NOTIFICATION_LOG_SEQ", cmSession));
        notification.setSender("MBCCS");
        notification.setAppName("MBCCS");
        notification.setCreateDate(new Date());
        cmSession.save(notification);
        cmSession.flush();
    }

    private PushNotificationResponse pushNotification(DataNotification data) throws Exception {
        SendMulticastMessage sensMulticastMessage = new SendMulticastMessage();
        sensMulticastMessage.setData(data);
        String xmlText = CommonWebservice.marshal(sensMulticastMessage).replaceAll("sendMulticastMessage", "ser:sendMulticastMessage");
        return (PushNotificationResponse) wsClient.sendRequestViaBccsGW(xmlText, "pushNotification", PushNotificationResponse.class);
    }

    public List<NotificationLog> getNotificationLog(Session cmSession, Long staffId) {
        String sql = " from  NotificationLog where receiverId = ? and createDate >= trunc(sysdate - 7) order by createDate desc";
        Query query = cmSession.createQuery(sql);
        query.setParameter(0, staffId);
        return query.list();
    }
}
