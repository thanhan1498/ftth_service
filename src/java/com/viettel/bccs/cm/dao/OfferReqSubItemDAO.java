package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.OfferReqSubItem;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class OfferReqSubItemDAO extends BaseDAO {

    public List<OfferReqSubItem> findBySubIdAndSubReqId(Session session, Long subId, Long subReqId) {
        if (subId == null || subId <= 0L || subReqId == null || subReqId <= 0L) {
            return null;
        }

        String sql = " from OfferReqSubItem where subId = ? and subReqId = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, subId).setParameter(1, subReqId).setParameter(2, Constants.STATUS_USE);
        List<OfferReqSubItem> items = query.list();
        return items;
    }
}
