package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.model.OfferRequest;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class OfferRequestDAO extends BaseDAO {

    public OfferRequest findById(Session session, Long reqOfferId) {
        if (reqOfferId == null || reqOfferId <= 0L) {
            return null;
        }

        String sql = " from OfferRequest where reqOfferId = ? ";
        Query query = session.createQuery(sql).setParameter(0, reqOfferId);
        List<OfferRequest> requests = query.list();
        if (requests != null && !requests.isEmpty()) {
            return requests.get(0);
        }

        return null;
    }

    public List<OfferRequest> findByCustReqId(Session session, Long custReqId) {
        if (custReqId == null || custReqId <= 0L) {
            return null;
        }
        String sql = " from OfferRequest where custReqId = ? ";
        Query query = session.createQuery(sql).setParameter(0, custReqId);
        List<OfferRequest> requests = query.list();
        return requests;
    }

    public boolean isAllCancel(Session cmPosSession, Long custReqId, Long reqOfferId) {
        List<OfferRequest> reqs = findByCustReqId(cmPosSession, custReqId);
        boolean result = true;
        if (reqs == null || reqs.isEmpty()) {
            return result;
        }
        for (OfferRequest req : reqs) {
            if (req != null) {
                Long status = req.getStatus();
                if (!Constants.SUB_REQ_STATUS_CANCEL.equals(status)) {
                    Long id = req.getReqOfferId();
                    if (reqOfferId == null || !reqOfferId.equals(id)) {
                        result = false;
                        return result;
                    }
                }
            }
        }
        return result;
    }

    public void updateStatus(Session cmPosSession, OfferRequest offerRequest, Long status, Long actionAuditId, Date issueDateTime) {
        Object oldValue = offerRequest.getStatus();
        offerRequest.setStatus(status);
        Object newValue = offerRequest.getStatus();

        new ActionDetailBussiness().insert(cmPosSession, actionAuditId, ReflectUtils.getTableName(OfferRequest.class), offerRequest.getReqOfferId(), ReflectUtils.getColumnName(OfferRequest.class, "status"), oldValue, newValue, issueDateTime);

        cmPosSession.update(offerRequest);
        cmPosSession.flush();
    }
}
