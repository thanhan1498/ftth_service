package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.OfferRequestIsdnDiscounted;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class OfferRequestIsdnDiscountedDAO extends BaseDAO {

    public List<OfferRequestIsdnDiscounted> findBySubReqId(Session session, Long subReqId) {
        if (subReqId == null || subReqId <= 0L) {
            return null;
        }

        String sql = " from OfferRequestIsdnDiscounted where subReqId = ? ";
        Query query = session.createQuery(sql).setParameter(0, subReqId);
        List<OfferRequestIsdnDiscounted> requests = query.list();
        return requests;
    }
}
