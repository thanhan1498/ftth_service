package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.OfferRequestSub;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class OfferRequestSubDAO extends BaseDAO {

    public OfferRequestSub findById(Session session, Long id) {
        if (id == null || id <= 0L) {
            return null;
        }

        String sql = " from OfferRequestSub where offerRequestSubId = ? ";
        Query query = session.createQuery(sql).setParameter(0, id);
        List<OfferRequestSub> requests = query.list();
        if (requests != null && !requests.isEmpty()) {
            return requests.get(0);
        }

        return null;
    }

    public List<OfferRequestSub> findBySubReqId(Session session, Long subReqId) {
        if (subReqId == null || subReqId <= 0L) {
            return null;
        }
        String sql = " from OfferRequestSub where subReqId = ? ";
        Query query = session.createQuery(sql).setParameter(0, subReqId);
        List<OfferRequestSub> requests = query.list();
        return requests;
    }
}
