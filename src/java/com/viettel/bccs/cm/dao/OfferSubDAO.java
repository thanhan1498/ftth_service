package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.model.OfferSub;
import com.viettel.bccs.cm.util.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class OfferSubDAO extends BaseDAO {

    public OfferSub findBySubIdAndReqOfferId(Session session, Long reqOfferId, Long subId) {
        if (subId == null || subId <= 0L || reqOfferId == null || reqOfferId <= 0L) {
            return null;
        }

        StringBuilder sql = new StringBuilder()
                .append(" select os from OfferSub os, ContractOffer co ")
                .append(" where os.contractOfferId = co.contractOfferId and os.subId = ? and os.status = ? and co.reqOfferId = ? and co.status = ? ");
        List params = new ArrayList();
        params.add(subId);
        params.add(Constants.OFFER_SUB_STATUS_USE);
        params.add(reqOfferId);
        params.add(Constants.CONTRACT_OFFER_STATUS_USE);

        Query query = session.createQuery(sql.toString());
        buildParameter(query, params);

        List<OfferSub> offerSubs = query.list();
        if (offerSubs != null && !offerSubs.isEmpty()) {
            return offerSubs.get(0);
        }

        return null;
    }

    public List<OfferSub> finBySubContractOffferId(Session session, Long contractOfferId) {
        if (contractOfferId == null || contractOfferId <= 0L) {
            return null;
        }
        String sql = " from OfferSub where contractOfferId = ?  and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, contractOfferId).setParameter(1, Constants.STATUS_USE);
        List<OfferSub> offSub = query.list();
        if (offSub != null && !offSub.isEmpty()) {
            return offSub;
        }
        return null;


    }

    public void update(Session session, Long subId, Long reqOfferId, Date staDatetime, Long actionAuditId, Date issueDateTime) {
        OfferSub offerSub = new OfferSubDAO().findBySubIdAndReqOfferId(session, subId, reqOfferId);
        if (offerSub != null) {
            offerSub.setStaDatetime(staDatetime);

            session.update(offerSub);
            session.flush();

            //<editor-fold defaultstate="collapsed" desc="luu log action detail">
            new ActionDetailBussiness().insert(session, actionAuditId, ReflectUtils.getTableName(OfferSub.class), offerSub.getOfferSubId(), ReflectUtils.getColumnName(OfferSub.class, "staDatetime"), null, offerSub.getStaDatetime(), issueDateTime);
            //</editor-fold>
        }
    }
}
