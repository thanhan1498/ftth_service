package com.viettel.bccs.cm.dao;

import com.viettel.brcd.ws.model.output.Service;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class ParamDAO extends BaseDAO {

    public List<Service> getListService(Session session) {
        StringBuilder sb = new StringBuilder();
        sb.append(" select param_name serviceName, param_value serviceId, param_code serviceCode ");
        sb.append(" from ap_param where param_type = 'MAPPING_TEL_SERVICE' and status = 1 ");
        sb.append(" and param_value not in (1,2) order by param_name asc ");

        SQLQuery sql = session.createSQLQuery(sb.toString());
        sql.addScalar("serviceId", Hibernate.LONG);
        sql.addScalar("serviceCode", Hibernate.STRING);
        sql.addScalar("serviceName", Hibernate.STRING);
        sql.setResultTransformer(Transformers.aliasToBean(Service.class));

        List<Service> results = (List<Service>) sql.list();
        return results;
    }
    
    public boolean checkWhiteListRegisterSub(Session cmPos, String sender, String productCode, String reasonCode) {
        String sql = "SELECT 1 "
                + "  FROM AP_PARAM "
                + " WHERE PARAM_TYPE = 'WHITE_LIST_REGISTER_SUB' "
                + "   AND PARAM_CODE = ? "
                + "   AND PARAM_NAME = ? "
                + "   AND INSTR(PARAM_VALUE, ?) > 0 ";
        Query query = cmPos.createSQLQuery(sql);
        query.setParameter(0, sender);
        query.setParameter(1, productCode);
        query.setParameter(2, reasonCode);
        List list = query.list();
        if (list == null || list.isEmpty()) {
            return false;
        }
        return true;
    }
}
