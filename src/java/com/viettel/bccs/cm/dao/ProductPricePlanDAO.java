package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.ProductPricePlan;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ProductPricePlanDAO extends BaseDAO {

    public ProductPricePlan findById(Session session, Long id) {
        if (id == null || id <= 0L) {
            return null;
        }

        StringBuilder sb = new StringBuilder()
                .append(" from ProductPricePlan where id = ? ");

        Query sql = session.createQuery(sb.toString()).setParameter(0, id);
        List<ProductPricePlan> pps = sql.list();
        if (pps != null && !pps.isEmpty()) {
            return pps.get(0);
        }

        return null;
    }

    public List<ProductPricePlan> getlistProductPP(Session session, Long offerId) {
        if (offerId == null || offerId <= 0L) {
            return null;
        }

        StringBuilder sb = new StringBuilder()
                .append(" from ProductPricePlan where offerId = ? and status = ? ");

        Query sql = session.createQuery(sb.toString())
                .setParameter(0, offerId)
                .setParameter(1, String.valueOf(Constants.STATUS_USE));

        List<ProductPricePlan> pps = sql.list();
        return pps;
    }
}
