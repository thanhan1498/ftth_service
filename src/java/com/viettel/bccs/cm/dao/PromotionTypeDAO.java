package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.PromotionType;
import com.viettel.bccs.cm.util.Constants;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class PromotionTypeDAO extends BaseDAO {

    public PromotionType findByCode(Session session, String promProgramCode) {
        if (promProgramCode == null) {
            return null;
        }
        promProgramCode = promProgramCode.trim();
        if (promProgramCode.isEmpty()) {
            return null;
        }

        String sql = " from PromotionType where lower(promProgramCode) = lower(?) and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, promProgramCode).setParameter(1, Constants.STATUS_USE);
        List<PromotionType> promotions = query.list();
        if (promotions != null && !promotions.isEmpty()) {
            return promotions.get(0);
        }

        return null;
    }

    public List<PromotionType> getListPromotion(Session session, String productCode) {
        if (productCode == null) {
            return null;
        }
        productCode = productCode.trim();
        if (productCode.isEmpty()) {
            return null;
        }

        StringBuilder sql = new StringBuilder()
                .append(" from PromotionType where staDate <= sysdate and (endDate is null or endDate >= sysdate) ")
                .append(" and ").append(createLike(" telService ")).append(" order by name asc ");
        List params = new ArrayList();
        params.add(formatLike(productCode));
        Query query = session.createQuery(sql.toString());
        buildParameter(query, params);

        List<PromotionType> promotions = query.list();
        return promotions;
    }

    public Date getDayOfMonth(Session session, Long cycle, Long monthAmount, Long dayType, Date staDatetime, Date sysdate) {
        Calendar cal = Calendar.getInstance();
        if (staDatetime != null) {
            cal.setTime(staDatetime);
        }
        if (cycle == null) {
            return cal.getTime();
        }
        if (cycle.equals(0L)) {
            if (dayType != null && Constants.PROMOTION_TYPE_EXPRIRE.equals(dayType) && monthAmount != null) {
                cal.add(Calendar.MONTH, (monthAmount.intValue() - 1));
                cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                cal.set(Calendar.HOUR_OF_DAY, 23);
                cal.set(Calendar.MINUTE, 59);
                cal.set(Calendar.SECOND, 59);
            } else {
                return cal.getTime();
            }
        } else {
            if (dayType != null && Constants.PROMOTION_TYPE_EFFECT.equals(dayType)) {
                cal.add(Calendar.MONTH, cycle.intValue() - 1);
                cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                cal.set(Calendar.HOUR_OF_DAY, 23);
                cal.set(Calendar.MINUTE, 59);
                cal.set(Calendar.SECOND, 58);
            } else {
                cal.add(Calendar.MONTH, (cycle.intValue() + monthAmount.intValue() - 1));
                cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                cal.set(Calendar.HOUR_OF_DAY, 23);
                cal.set(Calendar.MINUTE, 59);
                cal.set(Calendar.SECOND, 59);
            }
        }
        if (dayType != null && Constants.PROMOTION_TYPE_EXPRIRE.equals(dayType) && cal.getTime().before(sysdate)) {
            return null;
        }
        return cal.getTime();
    }
}
