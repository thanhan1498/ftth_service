package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.Quota;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class QuotaDAO extends BaseDAO {

    public Quota findById(Session session, Long quotaId) {
        if (quotaId == null || quotaId <= 0L) {
            return null;
        }

        String sql = " from Quota where quotaId = ? ";
        Query query = session.createQuery(sql).setParameter(0, quotaId);
        List<Quota> quotas = query.list();
        if (quotas != null && !quotas.isEmpty()) {
            return quotas.get(0);
        }

        return null;
    }

    public List<Quota> findByService(Session session, String serviceType) {
        if (serviceType == null) {
            return null;
        }
        serviceType = serviceType.trim();
        if (serviceType.isEmpty()) {
            return null;
        }

        String sql = " from Quota where serviceType = ? and status = ? order by length(quotaValue), quotaValue ";
        Query query = session.createQuery(sql).setParameter(0, serviceType).setParameter(1, Constants.STATUS_USE);
        List<Quota> quotas = query.list();
        return quotas;
    }
}
