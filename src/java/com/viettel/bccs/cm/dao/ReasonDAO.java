package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.brcd.util.QueryUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.util.key.Values;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ReasonDAO extends BaseDAO {

    public Reason findById(Session session, Long reasonId) {
        if (reasonId == null || reasonId <= 0L) {
            return null;
        }

        String sql = " from Reason where reasonId = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, reasonId).setParameter(1, Constants.STATUS_USE);
        List<Reason> reasons = query.list();
        if (reasons != null && !reasons.isEmpty()) {
            return reasons.get(0);
        }

        return null;
    }

    public Reason findByCode(Session session, String code) {
        if (code == null) {
            return null;
        }
        code = code.trim();
        if (code.isEmpty()) {
            return null;
        }

        String sql = " from Reason where code = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, code).setParameter(1, Constants.STATUS_USE);
        List<Reason> reasons = query.list();
        if (reasons != null && !reasons.isEmpty()) {
            return reasons.get(0);

        }

        return null;
    }

    public Reason findByTypeCode(Session session, String type, String code) {
        if (type == null || code == null) {
            return null;
        }
        type = type.trim();
        if (type.isEmpty()) {
            return null;
        }
        code = code.trim();
        if (code.isEmpty()) {
            return null;
        }

        String sql = " from Reason where type = ? and code = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, type).setParameter(1, code).setParameter(2, Constants.STATUS_USE);
        List<Reason> reasons = query.list();
        if (reasons != null && !reasons.isEmpty()) {
            return reasons.get(0);

        }

        return null;
    }

    public Long getLimit(Session session, String code) {
        Reason r = findByCode(session, code);
        if (r != null) {
            Long limit = r.getLimitNumberIsdn();
            if (limit != null && limit > 0L) {
                return limit;
            }
        }
        return 0L;
    }

    public List<Reason> getListRegType(Session session, String productCode, String serviceType) {
        StringBuilder sb = new StringBuilder()
                .append(" From Reason ")
                .append(" where status = :status and type = :type and upper(telService) like :serviceType ")
                .append(" and reasonId in ( select reasonId from Mapping where (productCode = :productCode or productCode is null) and status = :status) ");
        Query sql = session.createQuery(sb.toString())
                .setParameter("type", Constants.REASON_TYPE_REG_TYPE_NEW)
                .setParameter("status", Values.STATUS_ACTIVE)
                .setParameter("serviceType", QueryUtil.getFullStringParam(serviceType.toUpperCase()))
                .setParameter("productCode", productCode);

        List<Reason> reasons = sql.list();
        return reasons;
    }

    public List<Reason> getListRegType(Session session, Session imSession, Long numPort, String productCode, String serviceType, Shop shop) throws Exception {
        if (Constants.SERVICE_ALIAS_ADSL.equals(serviceType)) {
            return getListRegType(session, productCode, serviceType);
        }
        String modelType = Constants.MODEL_TYPE_1;
        if (numPort != null && numPort >= 4L) {
            modelType = Constants.MODEL_TYPE_2;
        }

        if (Constants.SERVICE_ALIAS_FTTH.equals(serviceType)) {
            return getListMappingRegType(session, shop, Constants.SERVICE_FTTH_ID_WEBSERVICE, productCode, modelType);
        }
        return getListMappingRegType(session, shop, Constants.SERVICE_LEASELINE_ID_WEBSERVICE, productCode, modelType);
    }

    public List<Reason> getListMappingRegType(Session session, Shop shop, Long telService, String productCode, String modelType) throws Exception {
        if (modelType == null) {
            return null;
        }
        modelType = modelType.trim();
        if (modelType.isEmpty()) {
            return null;
        }

        StringBuilder sb = new StringBuilder()
                .append(" select rea from MapActiveInfo act, Reason rea, Mapping m ")
                .append(" WHERE (act.regReasonId = rea.reasonId or act.regReasonId = :allValue) ")
                .append(" and m.reasonId = rea.reasonId and m.status = :status ")
                .append(" and (m.productCode = :productCode or m.productCode is null) ")
                .append("  AND act.status = :status ")
                .append("  AND rea.status = :status ")
                .append("  AND act.effectDate <= TRUNC (SYSDATE) ")
                .append("  AND (act.endDate IS NULL ")
                .append("  OR act.endDate >= TRUNC (SYSDATE)) ")
                .append("  AND rea.type = :type ")
                .append("  AND act.telServiceId = :telServiceId ")
                .append("  AND (act.channelTypeId = :channelTypeId or act.channelTypeId = :allValue )")
                .append("  AND (act.provinceCode = :provinceCode OR act.provinceCode = :allValueStr )")
                .append("  AND act.productCode = :productCode ")
                .append("  AND act.modelType = :modelType ");

        Query sql = session.createQuery(sb.toString())
                .setParameter("allValue", Constants.ALL_VALUE)
                .setParameter("allValueStr", Constants.ALL_VALUE.toString())
                .setParameter("status", Constants.STATUS_USE)
                .setParameter("type", Constants.REASON_TYPE_REG_TYPE_NEW)
                .setParameter("telServiceId", telService)
                .setParameter("channelTypeId", shop.getChannelTypeId())
                .setParameter("provinceCode", shop.getProvince())
                .setParameter("productCode", productCode)
                .setParameter("modelType", modelType);

        List<Reason> reasons = sql.list();
        return reasons;
    }

    public List<Reason> findReasons(Session session, String actionCode, String serviceAlias, String productCode) {
        if (actionCode == null) {
            return null;
        }
        actionCode = actionCode.trim();
        if (actionCode.isEmpty()) {
            return null;
        }

        StringBuilder sql = new StringBuilder();
        List params = new ArrayList();
        if (!new ApDomainDAO().isMandatoryTrasaction(session, actionCode)) {
            sql.append(" select r from Reason r, ApDomain a where a.type = ? ");
            sql.append(" and a.reasonType = r.type and a.code = ? and a.status = ? and r.status = ? ");
            params.add(Constants.ACTION_FOR_AUDIT);
            params.add(actionCode);
            params.add(Constants.STATUS_USE);
            params.add(Constants.STATUS_USE);
        } else {
            sql.append(" select r from Reason r, Mapping m where m.actionCode = ? ");
            sql.append(" and r.reasonId = m.reasonId and r.status = ? and m.status = ? and (m.productCode = ? or m.productCode is null) ");
            params.add(actionCode);
            params.add(Constants.STATUS_USE);
            params.add(Constants.STATUS_USE);
            params.add(productCode);
        }
        if (serviceAlias != null) {
            serviceAlias = serviceAlias.trim();
            if (!serviceAlias.isEmpty()) {
                sql.append(" and ").append(createLike(" r.telService "));
                params.add(formatLike(serviceAlias));
            }
        }

        Query query = session.createQuery(sql.toString());
        buildParameter(query, params);

        List<Reason> reasons = query.list();
        return reasons;
    }
    
     /**
     * Lay ra danh sach ly do theo group
     * @param paramType
     * @return danh sach ly do theo group
     */
    public List<Reason> getListReasonByGroup(Session session,String groupValue) {
        List<Reason> listReason = new ArrayList<Reason>();
        if (groupValue != null) {
            groupValue = groupValue.trim();
        }
        if (groupValue == null || groupValue.isEmpty()) {
            return listReason;
        }
        String sql = "from Reason Where reasonGroup = ?";
        Query query = session.createQuery(sql).setParameter(0, groupValue);
        listReason = query.list();
        return listReason;
    }

}
