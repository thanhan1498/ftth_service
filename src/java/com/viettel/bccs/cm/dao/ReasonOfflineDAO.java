package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.ReasonOffline;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ReasonOfflineDAO extends BaseDAO {

    public ReasonOffline findById(Session session, Long id) {
        if (id == null || id <= 0L) {
            return null;
        }

        String sql = " from ReasonOffline where id = ? ";
        Query query = session.createQuery(sql).setParameter(0, id);
        List<ReasonOffline> reasons = query.list();
        if (reasons != null && !reasons.isEmpty()) {
            return reasons.get(0);
        }

        return null;
    }

    public List<ReasonOffline> findByReasonId(Session session, Long reasonId) {
        if (reasonId == null || reasonId <= 0L) {
            return null;
        }
        String sql = " from ReasonOffline where reasonId = ? ";
        Query query = session.createQuery(sql).setParameter(0, reasonId);
        List<ReasonOffline> reasons = query.list();
        return reasons;
    }

    public List<ReasonOffline> findByTypeCode(Session session, String type, String code) {
        if (type == null || code == null) {
            return null;
        }
        type = type.trim();
        if (type.isEmpty()) {
            return null;
        }
        code = code.trim();
        if (code.isEmpty()) {
            return null;
        }
        String sql = " from ReasonOffline where type = ? and code = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, type).setParameter(1, code).setParameter(2, Constants.STATUS_USE);
        List<ReasonOffline> reasons = query.list();
        return reasons;
    }

    public Long getTotalUsedRegType(Session session, String regType) {
        List<ReasonOffline> reasonOffs = findByTypeCode(session, Constants.REASON_TYPE_REG_TYPE_NEW, regType);
        if (reasonOffs != null && !reasonOffs.isEmpty()) {
            for (ReasonOffline r : reasonOffs) {
                if (r != null) {
                    Long n = r.getnNumber();
                    if (n != null && n > 0L) {
                        return n;
                    }
                }
            }
        }
        return 0L;
    }
}
