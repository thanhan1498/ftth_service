/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.RegOnline;
import com.viettel.bccs.cm.model.RegOnlineHis;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class RegOnlineDAO extends BaseDAO {

    public List<RegOnline> getListRequestInDay(Session cmpos, String field, String value) {
        String sql = "from RegOnline where createDate >= trunc(sysdate) and " + field + "= ?";
        Query query = cmpos.createQuery(sql);
        query.setParameter(0, value);
        return query.list();
    }

    public RegOnline getRegOnlineById(Session cmpos, Long requestId) {
        String sql = "from RegOnline where id= ?";
        Query query = cmpos.createQuery(sql);
        query.setParameter(0, requestId);
        List<RegOnline> list = query.list();
        if (list == null | list.isEmpty()) {
            return null;
        } else {
            return list.get(0);
        }
    }
    
    public List<RegOnline> getRegOnlineByIsdn(Session cmpos, String isdn) {
        String sql = "from RegOnline where customerPhone= ? and createDate >= sysdate - 30";
        Query query = cmpos.createQuery(sql);
        query.setParameter(0, isdn);
        List<RegOnline> list = query.list();
        return list;
    }

    public List<RegOnlineHis> getRegOnlineHis(Session cmpos, Long requestId) {
        String sql = "from RegOnlineHis where updateDate >= trunc(sysdate - 30) and requestId = ? order by updateDate desc";
        Query query = cmpos.createQuery(sql);
        query.setParameter(0, requestId);
        return query.list();
    }

    public void saveActionLog(Session cmpos, Long requestId, String col, String oldVal, String newVal, String user, String desc) {
        RegOnlineHis action = new RegOnlineHis();
        action.setColumn(col);
        action.setOldValue(oldVal);
        action.setNewValue(newVal);
        action.setRequestId(requestId);
        action.setUpdateDate(new Date());
        action.setUpdateUser(user);
        action.setDescription(desc);
        cmpos.save(action);
        cmpos.flush();
    }
}
