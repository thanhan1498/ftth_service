package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.Contract;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.SaleTrans;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import org.hibernate.Query;
import org.hibernate.Session;
import java.util.List;

/**
 * @author vanghv1
 */
public class SaleTransDAO extends BaseDAO {

    public SaleTrans getSaleTrans(Customer customer, Contract contract, SubAdslLeaseline subscriber, Long telServiceId, Long shopId, Long staffId, Date nowDate) {
        SaleTrans saleTrans = new SaleTrans();

        if (customer != null) {
            saleTrans.setCustName(customer.getName());
            saleTrans.setAddress(customer.getAddress());
        }
        if (contract != null) {
            saleTrans.setContractNo(contract.getContractNo());
            saleTrans.setPayMethod(contract.getPayMethodCode());
        }
        if (subscriber != null) {
            saleTrans.setSubId(subscriber.getSubId());
            saleTrans.setIsdn(subscriber.getAccount());
            saleTrans.setTelecomServiceId(telServiceId);
            saleTrans.setCreateStaffId(subscriber.getStaffId());
        }

        saleTrans.setShopId(shopId);
        saleTrans.setStaffId(staffId);
        saleTrans.setSaleTransDate(nowDate);
        saleTrans.setStatus(Constants.STATUS_USE);

        return saleTrans;
    }

    public List<SaleTrans> findById(Session session, Long subId) {
        if (subId == null || subId <= 0L) {
            return null;
        }

        String sql = " from SaleTrans where subId = ? and ( actionCode = ? or actionCode = ? ) and status = ? ";
        Query query = session.createQuery(sql);
        query.setParameter(0, subId);
        query.setParameter(1, Constants.ACTION_SUBSCRIBER_ACTIVE_NEW);
        query.setParameter(2, Constants.ACTION_SUBSCRIBER_CHANGE_VAS);
        query.setParameter(3, Constants.STATUS_USE);
        List<SaleTrans> sales = query.list();
        if (sales != null && !sales.isEmpty()) {
            return sales;
        }

        return null;
    }
}
