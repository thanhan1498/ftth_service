package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class ShopDAO extends BaseDAO {

    public Shop findById(Session session, Long shopId) {
        if (shopId == null || shopId <= 0L) {
            return null;
        }

        String sql = " from Shop where shopId = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, shopId).setParameter(1, Constants.STATUS_USE);
        List<Shop> shops = query.list();
        if (shops != null && !shops.isEmpty()) {
            return shops.get(0);
        }

        return null;
    }

    public Shop findByCode(Session session, String code) {

        String sql = " from Shop where shopCode = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, code).setParameter(1, Constants.STATUS_USE);
        List<Shop> shops = query.list();
        if (shops != null && !shops.isEmpty()) {
            return shops.get(0);
        }

        return null;
    }

    /**
     * @author daibq getShopCodeByCenterCode
     * @param session
     * @param centerCode
     * @return
     */
    public String getShopCodeByCenterCode(Session session, String centerCode) {

        String sql = " select shop_code from bccs_im.shop where center_code = ? and status = ?";
        Query query = session.createSQLQuery(sql).setParameter(0, centerCode).setParameter(1, Constants.STATUS_USE);
        List<String> shopCodes = query.list();
        if (shopCodes != null && !shopCodes.isEmpty()) {
            return shopCodes.get(0);
        }

        return null;
    }

    /**
     * @author daibq getShopCodeByCenterCode
     * @param session
     * @param centerCode
     * @return
     */
    public Shop getShopById(Session session, Long id) {

        String sql = " select shop_type shopType, shop_code shopCode, center_code centerCode from bccs_im.shop where shop_id = ? and status = ?";
        Query query = session.createSQLQuery(sql).addScalar("shopType", Hibernate.STRING).
                addScalar("shopCode", Hibernate.STRING).
                addScalar("centerCode", Hibernate.STRING).
                setResultTransformer(Transformers.aliasToBean(Shop.class)).setParameter(0, id).setParameter(1, Constants.STATUS_USE);
        List<Shop> shops = query.list();
        if (shops != null && !shops.isEmpty()) {
            return shops.get(0);
        }

        return null;
    }

    public String getParShopCodeFromParentShopId(Session session, String parentShopId) {
        String sql = "SELECT DISTINCT PAR_SHOP_CODE FROM SHOP WHERE PARENT_SHOP_ID = ? AND STATUS = ? AND PAR_SHOP_CODE is not null";
        Query query = session.createSQLQuery(sql).setParameter(0, parentShopId).setParameter(1, Constants.STATUS_USE);
        List<String> list = query.list();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return "";
    }
}
