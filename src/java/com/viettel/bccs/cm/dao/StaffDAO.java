package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class StaffDAO extends BaseDAO {

    public Staff findById(Session session, Long staffId) {
        if (staffId == null || staffId <= 0L) {
            return null;
        }

        String sql = " from Staff where staffId = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, staffId).setParameter(1, Constants.STATUS_USE);
        List<Staff> staffs = query.list();
        if (staffs != null && !staffs.isEmpty()) {
            return staffs.get(0);
        }

        return null;
    }

    public Staff findByCode(Session session, String staffCode) {
        if (staffCode == null) {
            return null;
        }
        staffCode = staffCode.trim();
        if (staffCode.isEmpty()) {
            return null;
        }

        String sql = " from Staff where upper(staffCode) = upper(?) and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, staffCode).setParameter(1, Constants.STATUS_USE);
        List<Staff> staffs = query.list();
        if (staffs != null && !staffs.isEmpty()) {
            return staffs.get(0);
        }

        return null;
    }

    public List<Staff> findByShop(Session session, Long shopId) {
        if (shopId == null || shopId <= 0L) {
            return null;
        }

        String sql = " from Staff where shopId = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, shopId).setParameter(1, Constants.STATUS_USE);
        List<Staff> staffs = query.list();
        return staffs;
    }

    public List<Staff> findByShopAndChannel(Session session, Long shopId, Long channelTypeId) {
        if (shopId == null || shopId <= 0L) {
            return null;
        }

        String sql = " from Staff where shopId = ? and channelTypeId = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, shopId).setParameter(1, channelTypeId).setParameter(2, Constants.STATUS_USE);
        List<Staff> staffs = query.list();
        return staffs;
    }
}
