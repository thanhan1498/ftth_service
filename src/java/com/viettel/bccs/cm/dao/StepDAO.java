package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.Action;
import com.viettel.bccs.cm.model.Step;
import com.viettel.bccs.cm.model.StepInfo;
import com.viettel.bccs.cm.model.WorkFlow;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class StepDAO extends BaseDAO {

    public Step findById(Session session, Long stepId) {
        if (stepId == null || stepId <= 0L) {
            return null;
        }

        String sql = " from Step where stepId = ? ";
        Query query = session.createQuery(sql).setParameter(0, stepId);
        List<Step> steps = query.list();
        if (steps != null && !steps.isEmpty()) {
            return steps.get(0);
        }

        return null;
    }

    public StepInfo getNextStepInfo(Session cmPosSession, Long telService, Long currentStepId, String locale) {
        if (currentStepId == null) {
            currentStepId = 1L;
        }

        WorkFlowDAO workFlowDAO = new WorkFlowDAO();
        ActionDAO actionDAO = new ActionDAO();
        WorkFlow currentWorkFlow = workFlowDAO.getCurrentStep(cmPosSession, telService, currentStepId);
        Action currentAction = null;
        if (currentWorkFlow != null) {
            currentAction = actionDAO.findById(cmPosSession, currentWorkFlow.getNextActionId());
        }

        WorkFlow workFlow = workFlowDAO.getNextStep(cmPosSession, telService, currentStepId);
        Action action = null;
        if (workFlow != null) {
            action = actionDAO.findById(cmPosSession, workFlow.getNextActionId());
        }

        StepInfo stepInfo = new StepInfo();
        if (action != null) {
            Step step = findById(cmPosSession, workFlow.getNextStepId());
            stepInfo.setStepId(workFlow.getNextStepId());
            stepInfo.setStepName(step.getName());

            stepInfo.setActionId(action.getActionId());
            stepInfo.setActionName(action.getActionName());
            stepInfo.setActionMapping(action.getActionMaping());
            stepInfo.setClassName(action.getClassName());
            stepInfo.setMethodName(action.getMethodName());
            stepInfo.setIsManualOnly(Constants.IS_MANUAL_ONLY.equalsIgnoreCase(workFlow.getIsManualOnly()));
            stepInfo.setActionId(workFlow.getNextActionId());
        }

        if (currentAction != null) {
            stepInfo.setCurrentStepName(currentAction.getActionName());
        } else {
            stepInfo.setCurrentStepName(LabelUtil.getKey("request.accept", locale));
        }

        return stepInfo;
    }
}
