package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.model.SubAccountsAdslLl;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubAccountsAdslLlDAO extends BaseDAO {

    public List<SubAccountsAdslLl> findByAccount(Session session, String account) {
        if (account == null) {
            return null;
        }
        account = account.trim();
        if (account.isEmpty()) {
            return null;
        }

        String sql = " from SubAccountsAdslLl where account = ? ";
        Query query = session.createQuery(sql).setParameter(0, account);
        List<SubAccountsAdslLl> subAccounts = query.list();
        return subAccounts;
    }

    public List<SubAccountsAdslLl> findBySubId(Session session, Long subId) {
        if (subId == null || subId > 0L) {
            return null;
        }

        String sql = " from SubAccountsAdslLl where subId = ?  and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, subId).setParameter(1, Constants.STATUS_USE);
        List<SubAccountsAdslLl> subAccounts = query.list();
        return subAccounts;
    }

    public void insert(Session session, SubAdslLeaseline sub, Date nowDate, Long actionAuditId, Date issueDateTime) {
        SubAccountsAdslLl subAccount = new SubAccountsAdslLl();
        subAccount.setAccount(sub.getAccount());
        subAccount.setEffectFrom(nowDate);
        subAccount.setProductCode(sub.getProductCode());
        subAccount.setSubId(sub.getSubId());
        subAccount.setStatus(Constants.STATUS_USE);

        session.save(subAccount);
        session.flush();

        //<editor-fold defaultstate="collapsed" desc="luu log action detail">
        new ActionDetailBussiness().insert(session, actionAuditId, ReflectUtils.getTableName(SubAccountsAdslLl.class), sub.getSubId(), ReflectUtils.getColumnName(SubAccountsAdslLl.class, "account"), null, subAccount.getAccount(), issueDateTime);
        //</editor-fold>
    }
}
