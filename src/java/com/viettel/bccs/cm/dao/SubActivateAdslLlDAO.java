package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.model.SubActivateAdslLl;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubActivateAdslLlDAO extends BaseDAO {

    public void insert(Session session, SubAdslLeaseline sub, Date effectDate, String actionCode, Long reasonId, Long actionAuditId, Date issueDateTime) {
        SubActivateAdslLl subActivate = new SubActivateAdslLl();
        subActivate.setSubId(sub.getSubId());
        subActivate.setEffectFrom(effectDate);
        subActivate.setActStatus(sub.getActStatus());
        subActivate.setActionCode(actionCode);
        subActivate.setReasonId(reasonId);

        session.save(subActivate);
        session.flush();

        //<editor-fold defaultstate="collapsed" desc="luu log action detail">
        new ActionDetailBussiness().insert(session, actionAuditId, ReflectUtils.getTableName(SubActivateAdslLl.class), sub.getSubId(), ReflectUtils.getColumnName(SubActivateAdslLl.class, "actStatus"), null, subActivate.getActStatus(), issueDateTime);
        //</editor-fold>
    }

    public List<SubActivateAdslLl> findBySubId(Session session, Long subId) {
        if (subId == null || subId > 0L) {
            return null;
        }

        String sql = " from SubActivateAdslLl where subId = ?  and until IS NOT NULL ";
        Query query = session.createQuery(sql).setParameter(0, subId).setParameter(1, Constants.STATUS_USE);
        List<SubActivateAdslLl> subEmails = query.list();
        return subEmails;
    }
      public SubAdslLeaseline findBySubIdByAccount(Session session, String account) {
        SubAdslLeaseline adslLeaseline = null;
        if (account == null || account.isEmpty()) {
            return null;
        }
        String sql = " from SubAdslLeaseline where account = ?  and status =2";
        Query query = session.createQuery(sql).setParameter(0, account);
        List<SubAdslLeaseline> subEmails = query.list();
        if (subEmails != null && !subEmails.isEmpty()) {
            return adslLeaseline = subEmails.get(0);
        }
        return adslLeaseline;
    }
}
