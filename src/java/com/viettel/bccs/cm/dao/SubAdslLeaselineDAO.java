package com.viettel.bccs.cm.dao;

import com.viettel.bccs.api.Task.BO.ApParam;
import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.bussiness.common.CacheBO;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.util.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubAdslLeaselineDAO extends BaseDAO {

    public SubAdslLeaseline findById(Session session, Long subId) {
        if (subId == null || subId <= 0L) {
            return null;
        }

        String sql = " from SubAdslLeaseline where subId = ? ";
        Query query = session.createQuery(sql).setParameter(0, subId);
        List<SubAdslLeaseline> subs = query.list();
        if (subs != null && !subs.isEmpty()) {
            return subs.get(0);
        }

        return null;
    }

    public SubAdslLeaseline findByAccount(Session session, String account) {

        String sql = " from SubAdslLeaseline where account = ? ";
        Query query = session.createQuery(sql).setParameter(0, account);
        List<SubAdslLeaseline> subs = query.list();
        if (subs != null && !subs.isEmpty()) {
            return subs.get(0);
        }

        return null;
    }

    public List<SubAdslLeaseline> findByReqOfferId(Session session, Long reqOfferId) {
        if (reqOfferId == null || reqOfferId <= 0L) {
            return null;
        }

        StringBuilder sql = new StringBuilder()
                .append(" select s from SubAdslLeaseline s, OfferSub os, ContractOffer co, OfferRequest re ")
                .append(" where s.subId = os.subId and os.contractOfferId = co.contractOfferId and co.reqOfferId = re.reqOfferId ")
                .append(" and os.status = ? and co.status = ? and (re.isBundle = ? or s.status <> ?) and re.reqOfferId = ? ");

        List params = new ArrayList();
        params.add(Constants.OFFER_SUB_STATUS_USE);
        params.add(Constants.CONTRACT_OFFER_STATUS_USE);
        params.add(Constants.CONTRACT_OFFER_IS_BUNDLE);
        params.add(Constants.SUB_STATUS_NORMAL_ACTIVED);
        params.add(reqOfferId);
        Query query = session.createQuery(sql.toString());
        buildParameter(query, params);

        List<SubAdslLeaseline> subs = query.list();
        return subs;
    }

    public List<SubAdslLeaseline> findByContractId(Session session, Long contractId) {
        if (contractId == null || contractId <= 0L) {
            return null;
        }

        StringBuilder sql = new StringBuilder()
                .append(" select s from SubAdslLeaseline s, OfferSub os, ContractOffer co  ")
                .append(" where s.subId = os.subId and os.contractOfferId = co.contractOfferId  ")
                .append(" and os.status = ? and co.status = ? and co.contractId = ?  ");

        List params = new ArrayList();
        params.add(Constants.OFFER_SUB_STATUS_USE);
        params.add(Constants.CONTRACT_OFFER_STATUS_USE);
        params.add(contractId);
        Query query = session.createQuery(sql.toString());
        buildParameter(query, params);

        List<SubAdslLeaseline> subs = query.list();
        return subs;
    }

    public void update(Session session, SubAdslLeaseline sub, Date staDatetime, Long actionAuditId, Date issueDateTime) {
        sub.setStaDatetime(staDatetime);

        Object oldValue = sub.getStatus();
        sub.setStatus(Constants.SUB_STATUS_WAIT_ACTIVE);// Chuyen ve trang thai cho kich hoat
        Object newValue = sub.getStatus();
        ActionDetailBussiness detailBussiness = new ActionDetailBussiness();
        detailBussiness.insert(session, actionAuditId, ReflectUtils.getTableName(SubAdslLeaseline.class), sub.getSubId(), ReflectUtils.getColumnName(SubAdslLeaseline.class, "status"), oldValue, newValue, issueDateTime);

        oldValue = sub.getActStatus();
        sub.setActStatus(Constants.SUB_ACT_STATUS_BLOCK_ONE_WAY_BY_CUST_REQ);
        newValue = sub.getActStatus();
        detailBussiness.insert(session, actionAuditId, ReflectUtils.getTableName(SubAdslLeaseline.class), sub.getSubId(), ReflectUtils.getColumnName(SubAdslLeaseline.class, "actStatus"), oldValue, newValue, issueDateTime);

        session.update(sub);
        session.flush();
    }

    public List<SubAdslLeaseline> findByContractIdAll(Session session, Long contractId) {
        if (contractId == null || contractId <= 0L) {
            return null;
        }

        StringBuilder sql = new StringBuilder()
                .append(" select s from SubAdslLeaseline s ")
                .append(" where s.contractId = ?  ");

        List params = new ArrayList();
        params.add(contractId);
        Query query = session.createQuery(sql.toString());
        buildParameter(query, params);

        List<SubAdslLeaseline> subs = query.list();
        return subs;
    }

    public boolean isAccountVip(Session cmSession, String subType, String productCode) {
        if (subType != null && productCode != null) {
            /*Get define sub type is vip*/
            List<ApParam> subTypeVip = CacheBO.getSubTypeVip(cmSession);
            /*Get package vip*/
            List<ApParam> packageVip = CacheBO.getListPackageVip(cmSession);
            /*Kiem tra sub_type co phai vip hay khong*/
            if (subTypeVip != null && !subTypeVip.isEmpty()) {
                for (ApParam ap : subTypeVip) {
                    if (ap.getParamValue().trim().equalsIgnoreCase(subType.trim())) {
                        return true;
                    }
                }
            }
            if (packageVip != null && !packageVip.isEmpty()) {
                for (ApParam param : packageVip) {
                    if (param.getParamCode().trim().equalsIgnoreCase(productCode.trim())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
    
    //phuonghc 15062020 required to change query
    
    public List<Object> findIsdnByContractId(Session session, Long contractId)
    {
        String sql = "select isdn from payment.subscriber where contract_id = ?";
        List params = new ArrayList();
        params.add(contractId);
        Query query = session.createSQLQuery(sql);
        buildParameter(query, params);

        List<Object> subs = query.list();
        return subs;
    }
}
