/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.SubBundleTv;
import org.hibernate.Query;
import org.hibernate.Session;
import java.util.List;

/**
 *
 * @author cuongdm
 */
public class SubBundleTvDAO extends BaseDAO {

    public SubBundleTv findBundleTvByAccount(Session cmSession, String account) {
        String sql = "from SubBundleTv where account = ?";
        Query query = cmSession.createQuery(sql);
        query.setParameter(0, account);
        List<SubBundleTv> list = query.list();
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    public List<SubBundleTv> findBundleTvBySubId(Session cmSession, Long subId) {
        String sql = "from SubBundleTv where subId = ?";
        Query query = cmSession.createQuery(sql);
        query.setParameter(0, subId);
        List<SubBundleTv> list = query.list();
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list;
    }

    public SubBundleTv getBundleTvBySubId(Session sessionCmPos, Long subId) {
        List<SubBundleTv> list = getListBundleTvBySubId(sessionCmPos, subId);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    private List<SubBundleTv> getListBundleTvBySubId(Session sessionCmPos, Long subId) {
        String sql = "from SubBundleTv where subId = ? and status = 1";
        Query query = sessionCmPos.createQuery(sql);
        query.setParameter(0, subId);
        List<SubBundleTv> list = query.list();
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list;
    }
}
