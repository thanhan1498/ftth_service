package com.viettel.bccs.cm.dao;

import com.viettel.bccs.api.Task.BO.SubOwWhiteLl;
import com.viettel.bccs.api.Task.BO.SubPstn;
import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubDeployment;
import com.viettel.bccs.cm.model.SubReqAdslLl;
import com.viettel.bccs.cm.util.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubDeploymentDAO extends BaseDAO {

    public SubDeployment findById(Session session, Long subdeployId) {
        if (subdeployId == null || subdeployId <= 0L) {
            return null;
        }

        String sql = " from SubDeployment where subdeployId = ? ";
        Query query = session.createQuery(sql).setParameter(0, subdeployId);
        List<SubDeployment> deployments = query.list();
        if (deployments != null && !deployments.isEmpty()) {
            return deployments.get(0);
        }

        return null;
    }

    public List<SubDeployment> findBySubId(Session session, Long subId) {
        if (subId == null || subId <= 0L) {
            return null;
        }

        String sql = " from SubDeployment where subId = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, subId).setParameter(1, Constants.STATUS_USE);
        List<SubDeployment> deployments = query.list();
        if (deployments != null && !deployments.isEmpty()) {
            return deployments;
        }

        return null;
    }

    public void insert(Session cmPosSession, SubAdslLeaseline sub, SubReqAdslLl subReq, String actionCode, Date nowDate, Long actionAuditId, Date issueDateTime) {
        SubDeployment subDep = new SubDeployment();
        Long subdeployId = getSequence(cmPosSession, Constants.SUB_DEPLOYMENT_SEQ);
        subDep.setSubdeployId(subdeployId);
        subDep.setSubId(sub.getSubId());
        subDep.setIp(sub.getIpStatic());
        subDep.setDslamId(sub.getDslamId());
        subDep.setCableBoxId(sub.getCableBoxId());
        subDep.setDluId(sub.getDslamId());
        subDep.setShopId(sub.getTeamId());
        subDep.setProductCode(sub.getProductCode());
        subDep.setStationId(subReq.getStationId());
        subDep.setInfraType(sub.getInfraType());
        subDep.setStatus(Constants.STATUS_USE);
        subDep.setStageId(Constants.SUB_DEPLOYMENT_STAGE_ID_WORKING);
        subDep.setActionId(actionCode);
        subDep.setCreateDate(nowDate);

        cmPosSession.save(subDep);
        cmPosSession.flush();

        //<editor-fold defaultstate="collapsed" desc="luu log action detail">
        new ActionDetailBussiness().insert(cmPosSession, actionAuditId, ReflectUtils.getTableName(SubDeployment.class), sub.getSubId(), ReflectUtils.getColumnName(SubDeployment.class, "subdeployId"), null, subDep.getSubdeployId(), issueDateTime);
        //</editor-fold>
    }

    public void deleteSubDeploymentBySub(Session sessionCmPos, Long subId, Long sourceId, Date nowDate, Logger logger) {
        try {
            //tim ra toan bo cac ban ghi theo subId co status = 1
            List<SubDeployment> subDeploymentlist;
            if (sourceId != null) {
                subDeploymentlist = new SubDeploymentDAO().findBySubIdAndSourceId(sessionCmPos, subId, sourceId);
            } else {
                subDeploymentlist = new SubDeploymentDAO().findBySubId(sessionCmPos, subId);
            }

            if (subDeploymentlist != null && !subDeploymentlist.isEmpty()) {
                for (SubDeployment subDeployment : subDeploymentlist) {
                    subDeployment.setStatus(Constants.STATUS_DELETE);
                    sessionCmPos.update(subDeployment);
                }
            }
        } catch (Exception e) {
            logger.error("### An error occured while deleteSubDeploymentBySub with subId=" + subId);
        }
    }

    public void addSubDeploymentBySub(Session sessionCmPos, Object subsciberSource, Date nowDate, Long actionAuditId, String actionCode, Long stationId, Long connectorId, Long stageId) {
        Long subDepId = getSequence(sessionCmPos, "SUB_DEPLOYMENT_SEQ");
        SubDeployment subDep = new SubDeployment();
        subDep.setSubdeployId(subDepId);
        SubDeployment subDepEnd = new SubDeployment();
        boolean isSave = false;
        boolean isSaveEnd = false;

        try {
            if (subsciberSource instanceof SubAdslLeaseline) {
                SubAdslLeaseline subAdslLeaseline = (SubAdslLeaseline) subsciberSource;
                subDep.setSubId(subAdslLeaseline.getSubId());
                subDep.setIp(subAdslLeaseline.getIpStatic());
                subDep.setDslamId(null);
                subDep.setDluId(null);
                subDep.setShopId(subAdslLeaseline.getTeamId());
                subDep.setProductCode(subAdslLeaseline.getProductCode());
                subDep.setInfraType(subAdslLeaseline.getInfraType());
                subDep.setStationId(stationId);
                subDep.setCableBoxId(connectorId);
                isSave = true;
            } else if (subsciberSource instanceof SubPstn) {
                SubPstn subPstn = (SubPstn) subsciberSource;
                subDep.setSubId(subPstn.getSubId());
                subDep.setDslamId(subPstn.getDluId());
                subDep.setDluId(subPstn.getDluId());
                subDep.setShopId(subPstn.getTeamId());
                subDep.setProductCode(subPstn.getProductCode());
                isSave = true;
            } else if (subsciberSource instanceof SubOwWhiteLl) {
                SubOwWhiteLl subOwWhiteLl = new SubOwWhiteLl();
                subDep.setSubId(subOwWhiteLl.getSubId());
                subDep.setIp(subOwWhiteLl.getIpStatic());
                subDep.setDslamId(subOwWhiteLl.getDslamId());
                subDep.setDluId(subOwWhiteLl.getDslamId());
                subDep.setShopId(subOwWhiteLl.getTeamId());
                subDep.setProductCode(subOwWhiteLl.getProductCode());
                subDep.setSourceId(Constants.SOURCE_ID_START);
                isSave = true;
                if (Constants.SERVICE_ALIAS_WHITE_LL.equals(subOwWhiteLl.getServiceType())) {
                    subDepEnd.setSubId(subOwWhiteLl.getSubId());
                    subDepEnd.setIp(subOwWhiteLl.getIpStatic());
                    subDepEnd.setDslamId(subOwWhiteLl.getDslamIdEnd());
                    subDepEnd.setDluId(subOwWhiteLl.getDslamIdEnd());
                    subDepEnd.setShopId(subOwWhiteLl.getTeamIdEnd());
                    subDepEnd.setProductCode(subOwWhiteLl.getProductCode());
                    subDepEnd.setSourceId(Constants.SOURCE_ID_END);
                    isSaveEnd = true;
                }

            }
            if (isSave) {
                subDep.setStatus(Constants.STATUS_USE);
                subDep.setStageId(stageId);
                subDep.setActionId(actionCode);
                subDep.setCreateDate(nowDate);

                sessionCmPos.save(subDep);
                //ghi log detail
                System.out.println("com.viettel.bccs.cm.dao.SubDeploymentDAO.addSubDeploymentBySub()");
            }
            // OW kenhtrang - diem cuoi
            if (isSaveEnd) {
                subDepEnd.setStatus(Constants.STATUS_USE);
                subDepEnd.setStageId(Constants.SUB_DEPLOYMENT_STAGE_ID_WORKING);
                subDepEnd.setActionId(actionCode);
                subDepEnd.setCreateDate(nowDate);

                sessionCmPos.save(subDepEnd);
                //ghi log detail
            }
        } catch (Exception e) {
            System.out.println("com.viettel.bccs.cm.dao.SubDeploymentDAO.addSubDeploymentBySub()");
        }

    }

    public List findBySubIdAndSourceId(Session sessionCmPos, Long subId, Long sourceId) {
        try {
            String queryString = "from SubDeployment as model where model.status = ? AND model.subId = ? AND model.sourceId = ? ";

            Query queryObject = sessionCmPos.createQuery(queryString);
            queryObject.setParameter(0, Constants.STATUS_USE);
            queryObject.setParameter(1, subId);
            queryObject.setParameter(2, sourceId);

            return queryObject.list();
        } catch (RuntimeException re) {
            return new ArrayList<SubDeployment>();
        }
    }
}
