package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.brcd.dao.im.IMDAO;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubDepositAdslLl;
import com.viettel.bccs.cm.supplier.CustomerSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.im.database.BO.Deposit;
import com.viettel.im.database.BO.ReceiptExpense;
import com.viettel.im.database.DAO.WebServiceIMUtil;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubDepositAdslLlDAO extends BaseDAO {

    public SubDepositAdslLl findById(Session session, Long id) {
        if (id == null || id <= 0L) {
            return null;
        }

        String sql = " from SubDepositAdslLl where id = ? ";
        Query query = session.createQuery(sql).setParameter(0, id);
        List<SubDepositAdslLl> deposits = query.list();
        if (deposits != null && !deposits.isEmpty()) {
            return deposits.get(0);
        }

        return null;
    }

    public void insert(Session cmPosSession, Session imSession, SubAdslLeaseline sub, Staff staff, Date nowDate, Long actionAuditId, Customer customer, Date issueDateTime) throws Exception {
        SubDepositAdslLl subDeposit = new SubDepositAdslLl();
        Long telServiceId = Constants.SERVICE_ADSL_ID_WEBSERVICE;
        Long depositTypeId = new DepositTypeDAO().getDepositTypeId(cmPosSession, telServiceId, Constants.SUB_DEPOSIT_TYPE_CODE_ADSL);
        subDeposit.setDepositType(depositTypeId == null ? null : depositTypeId.toString());
        subDeposit.setSubId(sub.getSubId());
        subDeposit.setStatus(Constants.STATUS_USE);
        subDeposit.setUserName(staff.getStaffCode());
        subDeposit.setPc(Constants.WS_IP);
        subDeposit.setReasonType(depositTypeId == null ? null : depositTypeId.toString());
        subDeposit.setRegDatetime(nowDate);
        subDeposit.setDeposit(sub.getDeposit());
        Long id = getSequence(cmPosSession, Constants.SUB_DEPOSIT_ADSL_LL_SEQ);
        subDeposit.setId(id);

        cmPosSession.save(subDeposit);
        cmPosSession.flush();

        //<editor-fold defaultstate="collapsed" desc="luu log action detail">
        new ActionDetailBussiness().insert(cmPosSession, actionAuditId, ReflectUtils.getTableName(SubDepositAdslLl.class), sub.getSubId(), ReflectUtils.getColumnName(SubDepositAdslLl.class, "id"), null, subDeposit.getId(), issueDateTime);
        //</editor-fold>

        // save deposit IM
        Deposit deposit = new Deposit();
        deposit.setIsdn(sub.getAccount());
        deposit.setAmount(sub.getDeposit());
        deposit.setType(Constants.IM_DEPOSIT_TYPE_PAY);
        deposit.setDepositTypeId(depositTypeId);
        deposit.setStaffId(staff.getStaffId());
        deposit.setSubId(sub.getSubId());
        deposit.setCreateDate(nowDate);
        deposit.setCreateBy(staff.getStaffCode());
        deposit.setTelecomServiceId(telServiceId);
        deposit.setCustName(customer.getName());
        deposit.setAddress(customer.getAddress());
        deposit.setTin(customer.getTin());
        deposit.setReasonId(Constants.REASON_TYPE_29_KHYC);
        new IMDAO().saveDeposit(imSession, deposit);
    }

    public Double getTotalCurrentDeposit(Session cmPosSession, Long subId) {
        Double total = 0D;
        String sql = "Select sum(deposit) from SubDepositAdslLl Where subId = ? and status = 1";

        Query query = cmPosSession.createQuery(sql);
        query.setParameter(0, subId);

        List lstSubDeposit = query.list();
        if (lstSubDeposit != null && !lstSubDeposit.isEmpty()) {
            total = (Double) lstSubDeposit.get(0);
        }

        return total != null ? total : 0D;
    }

    public Long insertDepositOfInventory(Session imSession, Session cmPos, Long reasonId, String depositType, Long depositTypeId,
            Double amount, Long telServiceId, Customer cust,
            Long staffId, Long shopId, String loginName, String isdn, Long subId, String payMethod) throws Exception {
        Deposit deposit = new Deposit();

        /*cuongdm: them phieu thu tu dong*/
        ReceiptExpense receiptExpense = new ReceiptExpense();
        receiptExpense.setStaffId(staffId);
        receiptExpense.setShopId(shopId);
        receiptExpense.setUsername(loginName);
        receiptExpense.setType(depositType);
        receiptExpense.setStatus(Constants.STATUS_NONE_APPROVE);
        receiptExpense.setDeliverer(cust.getName());
        receiptExpense.setAddress(cust.getAddress());
        receiptExpense.setPayMethod(payMethod == null ? Constants.RECEIVE_PAY_METHOD : payMethod);
        receiptExpense.setAmount(amount);

        deposit.setIsdn(isdn);
        deposit.setAmount(amount);
        deposit.setType(depositType);
        deposit.setDepositTypeId(depositTypeId);
        deposit.setStaffId(staffId);
        deposit.setSubId(subId);
        deposit.setCreateDate(new Date());
        deposit.setCreateBy(loginName);
        deposit.setTelecomServiceId(telServiceId);
        deposit.setCustName(cust.getName());
        deposit.setAddress(cust.getAddress());
        deposit.setTin(cust.getTin());
        deposit.setReasonId(reasonId);

        //InterfaceCmInventory.saveDeposit(imSession, deposit);
        Long depositSave = saveReceiptExpense(imSession, cmPos, receiptExpense, deposit, subId);
        return receiptExpense.getReceiptId();
    }

    /**
     * @author cuongdm
     * @des Tao phieu thu tu dong
     * @param imSession
     * @param receitptExpense
     * @return
     * @throws Exception
     */
    public Long saveReceiptExpense(Session imSession, Session cmPoSession, ReceiptExpense receitptExpense, Deposit deposit, Long subId) throws Exception {

        try {
            String strMaxReceiptNo = "";
            int maxNoReceiptNo = 3;//STT phieu cua hang 000 trong format VT_PC_ShopCode_yyMM_000

            /*Khoi tao mã phiếu thu*/
            String strQuery = "Select shopCode from Shop where shopId = ?";
            Query query = imSession.createQuery(strQuery);
            query.setParameter(0, receitptExpense.getShopId());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyMM");
            String strFormat = dateFormat.format(new Date());
            String typeRecei;
            String typeReason;
            String reasonCode;
            /*check thue bao dau noi theo CS moi CDBR*/
            boolean isNewConnect = new CustomerSupplier().isCheckNewConnect(cmPoSession, subId);
            if (deposit.getType().equals(Constants.TYPE_RECEIVE_DEPOSIT)) {
                typeRecei = "VT_PT_";
                typeReason = Constants.REASON_TYPE_RECEIVE_DEPOSIT;
                reasonCode = Constants.REASON_CODE_DEPOSIT_POS;
            } else {
                typeRecei = "VT_PC_";
                typeReason = Constants.REASON_TYPE_PAY_DEPOSIT;
                reasonCode = Constants.REASON_CODE_WD_POS_PAYMENT;
            }
            String receiptNoFormat = typeRecei + query.list().get(0) + "_" + strFormat;

            String strQuery1 = "select receipt_No from Receipt_Expense where receipt_No like ? ";
            Query query1 = imSession.createSQLQuery(strQuery1);
            query1.setParameter(0, receiptNoFormat + "%");
            Long maxReceiptNo = 0L;

            List listReceiptNo = query1.list();
            if (listReceiptNo != null && !listReceiptNo.isEmpty()) {
                for (int i = 0; i < listReceiptNo.size(); i++) {
                    String strTemp = listReceiptNo.get(i).toString().replace(receiptNoFormat, "");
                    try {
                        Long longTemp = Long.parseLong(strTemp);
                        if (longTemp > maxReceiptNo) {
                            maxReceiptNo = longTemp;
                        }
                    } catch (Exception ex) {
                    }
                }
                strMaxReceiptNo = String.valueOf(maxReceiptNo + 1L);
            }
            if (strMaxReceiptNo.length() < maxNoReceiptNo) {
                if (strMaxReceiptNo.length() == 0) {
                    strMaxReceiptNo = "0";
                }
                for (int j = 0; j <= maxNoReceiptNo - strMaxReceiptNo.length(); j++) {
                    strMaxReceiptNo = "0" + strMaxReceiptNo;
                }
            }
            /*reason Id*/
            if (isNewConnect) {
                typeReason = Constants.REASON_TYPE_DEPOSIT;
                strQuery = "select reason_Id from  Reason where reason_Type = ? and reason_code = ?";
                query = imSession.createSQLQuery(strQuery);
                query.setParameter(0, typeReason);
                query.setParameter(1, reasonCode);
            } else {
                strQuery = "select reason_Id from  Reason where reason_Type = ?";
                query = imSession.createSQLQuery(strQuery);
                query.setParameter(0, typeReason);
            }
            List resultReason = query.list();
            Date now = new Date();
            receitptExpense.setReceiptNo(receiptNoFormat + strMaxReceiptNo);
            receitptExpense.setReceiptId(WebServiceIMUtil.getSequence(imSession, "Receipt_Expense_SEQ"));
            receitptExpense.setCreateDatetime(now);
            receitptExpense.setReceiptDate(now);
            receitptExpense.setReasonId(Long.parseLong(resultReason.isEmpty() ? "0" : resultReason.get(0).toString()));
            receitptExpense.setReceiptType(Constants.RECEIVE_TYPE_TDCCH);
            imSession.save(receitptExpense);
            //new IMDAO().saveDeposit(imSession, deposit);

            /*add deposit*/
            deposit.setStatus(Constants.STATUS_PAPER);
            deposit.setDepositId(WebServiceIMUtil.getSequence(imSession, "DEPOSIT_SEQ"));
            deposit.setShopId(WebServiceIMUtil.findShopIdByStaffId(imSession, deposit.getStaffId()));
            deposit.setReasonId(Long.parseLong(resultReason.isEmpty() ? "0" : resultReason.get(0).toString()));
            deposit.setReceiptId(receitptExpense.getReceiptId());
            imSession.save(deposit);
            //new IMDAO().saveDeposit(imSession, deposit);

            return 1L;

        } catch (Exception re) {
            re.printStackTrace();
            imSession.clear();
//            log.info("end getListDepositType" + new Date().toString());
            throw re;
        }
    }
}
