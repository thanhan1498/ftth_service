/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.SubDigital;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class SubDigitalDAO extends BaseDAO{
    public SubDigital findById(Session session, Long subId) {
        if (subId == null || subId <= 0L) {
            return null;
        }

        String sql = " from SubDigital where subId = ? ";
        Query query = session.createQuery(sql).setParameter(0, subId);
        List<SubDigital> subs = query.list();
        if (subs != null && !subs.isEmpty()) {
            return subs.get(0);
        }

        return null;
    }

    public SubDigital findByAccount(Session session, String account) {

        String sql = " from SubDigital where account = ? ";
        Query query = session.createQuery(sql).setParameter(0, account);
        List<SubDigital> subs = query.list();
        if (subs != null && !subs.isEmpty()) {
            return subs.get(0);
        }

        return null;
    }
}
