package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubIpAdslLl;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubIpAdslLlDAO extends BaseDAO {

    public void insert(Session session, SubAdslLeaseline sub, Date nowDate, Long actionAuditId, Date issueDateTime) {
        String ipStatic = sub.getIpStatic();
        if (ipStatic != null) {
            ipStatic = ipStatic.trim();
            if (!ipStatic.isEmpty()) {
                SubIpAdslLl subIp = new SubIpAdslLl();
                subIp.setSubId(sub.getSubId());
                subIp.setEffectFrom(nowDate);
                subIp.setIp(sub.getIpStatic());
                subIp.setAutoAssigned(Constants.SUB_IP_AUTO_ASSIGN);
                subIp.setFramedIpNetmask(Constants.SUB_IP_FRAMED_IP_NETMASK);
                subIp.setStatus(Constants.SUB_IP_STATUS_USE);
                subIp.setStatusRating(Constants.SUB_IP_STATUS_RATING);

                session.save(subIp);
                session.flush();

                //<editor-fold defaultstate="collapsed" desc="luu log action detail">
                new ActionDetailBussiness().insert(session, actionAuditId, ReflectUtils.getTableName(SubIpAdslLl.class), sub.getSubId(), ReflectUtils.getColumnName(SubIpAdslLl.class, "ip"), null, subIp.getIp(), issueDateTime);
                //</editor-fold>
            }
        }
    }

    public List<SubIpAdslLl> findBySubId(Session cmPosSession, Long subId) {
        if (subId == null || subId <= 0L) {
            return null;
        }

        String sql = " from SubIpAdslLl where subId = ? and status = ? ";
        Query query = cmPosSession.createQuery(sql).setParameter(0, subId).setParameter(1, Constants.SUB_IP_STATUS_USE);
        List<SubIpAdslLl> ips = query.list();
        return ips;
    }

    public List<SubIpAdslLl> findById(Session cmPosSession, Long subId) {
        if (subId == null || subId <= 0L) {
            return null;
        }
        String sql = " from SubIpAdslLl where subId = ? and ( status  = ? or status = ? ) ";
        Query query = cmPosSession.createQuery(sql).setParameter(0, subId).setParameter(1, Constants.SUB_IP_STATUS_USE).setParameter(2, Constants.SUB_IP_STATUS_PAUSE);
        List<SubIpAdslLl> ips = query.list();
        return ips;
    }

    public boolean isRatingStatus(Session cmPosSession, Long subId, String ip) {
        boolean result = false;
        if (ip == null || subId == null || subId <= 0L) {
            return result;
        }
        List<SubIpAdslLl> ips = findBySubId(cmPosSession, subId);
        if (ips != null && !ips.isEmpty()) {
            for (SubIpAdslLl subIp : ips) {
                if (subIp != null) {
                    if (subIp.getStatusRating() != null && subIp.getStatusRating().equals(1L)) {
                        String ipBlock = subIp.getIpBlock();
                        if (ipBlock != null) {
                            ipBlock = ipBlock.trim();
                        }
                        if (ipBlock != null && ip.equals(ipBlock)) {
                            return true;
                        }
                        if ((ipBlock == null || ipBlock.isEmpty()) && ip.equals(subIp.getIp())) {
                            return true;
                        }
                    }
                }
            }
        }
        return result;
    }
}
