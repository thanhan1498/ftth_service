package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.model.SubProductAdslLl;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubProductAdslLlDAO extends BaseDAO {

    public void insert(Session session, Long subId, Date effectDate, String productCode, Long offerId, Long actionAuditId, Date issueDateTime) {
        SubProductAdslLl subProduct = new SubProductAdslLl();
        Long id = getSequence(session, Constants.SUB_PRODUCT_ADSL_LL_ID_SEQ);
        subProduct.setId(id);
        subProduct.setSubId(subId);
        subProduct.setEffectDate(effectDate);
        subProduct.setProductCode(productCode);
        subProduct.setOfferId(offerId);
        subProduct.setStatus(Constants.SUB_PRODUCT_ADSL_LL_STATUS_USE);

        session.save(subProduct);
        session.flush();

        //<editor-fold defaultstate="collapsed" desc="luu log action detail">
        new ActionDetailBussiness().insert(session, actionAuditId, ReflectUtils.getTableName(SubProductAdslLl.class), subProduct.getId(), ReflectUtils.getColumnName(SubProductAdslLl.class, "id"), null, subProduct.getId(), issueDateTime);
        //</editor-fold>
    }
}
