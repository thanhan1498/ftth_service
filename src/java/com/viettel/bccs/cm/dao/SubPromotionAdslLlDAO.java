package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubPromotionAdslLl;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubPromotionAdslLlDAO extends BaseDAO {

    public List<SubPromotionAdslLl> findBySubId(Session session, Long subId) {
        if (subId == null || subId <= 0L) {
            return null;
        }
        String sql = " from SubPromotionAdslLl where subId = ? and until IS NULL ";
        Query query = session.createQuery(sql).setParameter(0, subId);
        List<SubPromotionAdslLl> promotions = query.list();
        return promotions;
    }

    public void insert(Session cmPosSession, SubAdslLeaseline sub, Date nowDate, Long actionAuditId, Date issueDateTime) {
        String promotionCode = sub.getPromotionCode();
        if (promotionCode != null) {
            promotionCode = promotionCode.trim();
            if (!promotionCode.isEmpty()) {
                SubPromotionAdslLl subPromotion = new SubPromotionAdslLl(sub.getSubId(), promotionCode, nowDate, null);

                cmPosSession.save(subPromotion);
                cmPosSession.flush();

                //<editor-fold defaultstate="collapsed" desc="luu log action detail">
                new ActionDetailBussiness().insert(cmPosSession, actionAuditId, ReflectUtils.getTableName(SubPromotionAdslLl.class), sub.getSubId(), ReflectUtils.getColumnName(SubPromotionAdslLl.class, "subId"), null, subPromotion.getSubId(), issueDateTime);
                //</editor-fold>
            }
        }
    }
}
