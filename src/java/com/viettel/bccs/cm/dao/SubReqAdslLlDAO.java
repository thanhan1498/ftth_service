package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.model.StepInfo;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubReqAdslLl;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubReqAdslLlDAO extends BaseDAO {

    public SubReqAdslLlDAO() {
        _log = Logger.getLogger(SubReqAdslLlDAO.class);
    }

    public SubReqAdslLl findById(Session session, Long id) {
        if (id == null || id <= 0L) {
            return null;
        }

        String sql = " from SubReqAdslLl where id = ? ";
        Query query = session.createQuery(sql);
        query.setParameter(0, id);
        List<SubReqAdslLl> reqs = query.list();
        if (reqs != null && !reqs.isEmpty()) {
            return reqs.get(0);
        }

        return null;
    }

    public SubReqAdslLl findBySubId(Session session, Long subId) {
        if (subId == null || subId <= 0L) {
            return null;
        }

        String sql = " from SubReqAdslLl where subId = ? ";
        Query query = session.createQuery(sql);
        query.setParameter(0, subId);
        List<SubReqAdslLl> reqs = query.list();
        if (reqs != null && !reqs.isEmpty()) {
            return reqs.get(0);
        }

        return null;
    }

    public SubReqAdslLl findByContractId(Session session, Long subReq, Long ContractId) {
        if (ContractId == null || ContractId <= 0L) {
            return null;
        }
        if (subReq == null || subReq <= 0L) {
            return null;
        }

        String sql = "select sr from SubReqAdslLl sr, SubAdslLeaseline s where sr.subId = s.subId and sr.status = ? and sr.isSmart = ? and s.contractId = ? and sr.id = ? ";
        Query query = session.createQuery(sql);
        query.setParameter(0, Constants.SUB_REQ_STATUS_SIGN_CONTRACT);
        query.setParameter(1, Constants.IS_SMART);
        query.setParameter(2, ContractId);
        query.setParameter(3, subReq);
        List<SubReqAdslLl> reqs = query.list();
        if (reqs != null && !reqs.isEmpty()) {
            return reqs.get(0);
        }

        return null;
    }

    public List<SubReqAdslLl> findByCustReqId(Session session, Long custReqId) {
        if (custReqId == null || custReqId <= 0L) {
            return null;
        }

        String sql = " from SubReqAdslLl where custReqId = ? ";
        Query query = session.createQuery(sql).setParameter(0, custReqId);
        List<SubReqAdslLl> reqs = query.list();
        return reqs;
    }

    public boolean isAllCancel(Session session, Long custReqId, Long subReqId) {
        List<SubReqAdslLl> reqs = findByCustReqId(session, custReqId);
        boolean result = true;
        if (reqs == null || reqs.isEmpty()) {
            return result;
        }
        for (SubReqAdslLl req : reqs) {
            if (req != null) {
                Long status = req.getStatus();
                if (!Constants.SUB_REQ_STATUS_CANCEL.equals(status)) {
                    Long id = req.getId();
                    if (subReqId == null || !subReqId.equals(id)) {
                        result = false;
                        return result;
                    }
                }
            }
        }
        return result;
    }

    public void update(Session cmPosSession, Session imSession, Long serviceId, String serviceAlias, SubReqAdslLl subReq, Long actionAuditId, String locale, Date issueDateTime, Long status) {
        Object oldCurrentStepId = subReq.getCurrentStepId();
        subReq.setCurrentStepId(Constants.STEP_ID_REG_ACTIVE);

        ActionDetailBussiness detailBussiness = new ActionDetailBussiness();
        Object oldValue = subReq.getStatus();
        subReq.setStatus(status);
        Object newValue = subReq.getStatus();
        detailBussiness.insert(cmPosSession, actionAuditId, ReflectUtils.getTableName(SubReqAdslLl.class), subReq.getSubId(), ReflectUtils.getColumnName(SubReqAdslLl.class, "status"), oldValue, newValue, issueDateTime);

        oldValue = subReq.getActStatus();
        subReq.setActStatus(Constants.SUB_ACT_STATUS_NORMAL);
        newValue = subReq.getActStatus();
        detailBussiness.insert(cmPosSession, actionAuditId, ReflectUtils.getTableName(SubReqAdslLl.class), subReq.getSubId(), ReflectUtils.getColumnName(SubReqAdslLl.class, "actStatus"), oldValue, newValue, issueDateTime);

        info(_log, "SubReqAdslLlDAO.update:serviceId=" + serviceId + ";currentStepId=" + toJson(subReq.getCurrentStepId()));
        StepInfo stepInfo = new StepDAO().getNextStepInfo(cmPosSession, serviceId, subReq.getCurrentStepId(), locale);
        info(_log, "SubReqAdslLlDAO.update:stepInfo=" + toJson(stepInfo));
        if (stepInfo != null) {
            if (stepInfo.getStepId() != null && !stepInfo.isIsManualOnly()) {//Neu la giao tu dong tu buoc truoc, thuc hien giao viec
                new TaskDAO().assignDeployTask(cmPosSession, imSession, serviceAlias, subReq, subReq.getCustReqId(), stepInfo.getStepId(), actionAuditId, issueDateTime);
                subReq.setCurrentStepId(stepInfo.getStepId());
            }
        }

        newValue = subReq.getCurrentStepId();
        detailBussiness.insert(cmPosSession, actionAuditId, ReflectUtils.getTableName(SubReqAdslLl.class), subReq.getSubId(), ReflectUtils.getColumnName(SubReqAdslLl.class, "currentStepId"), oldCurrentStepId, newValue, issueDateTime);

        cmPosSession.update(subReq);
        cmPosSession.flush();
    }

    public void updateStatus(Session cmPosSession, SubReqAdslLl subReq, Long status, Long actionAuditId, Date issueDateTime) {
        Object oldValue = subReq.getStatus();
        subReq.setStatus(status);
        Object newValue = subReq.getStatus();

        new ActionDetailBussiness().insert(cmPosSession, actionAuditId, ReflectUtils.getTableName(SubReqAdslLl.class), subReq.getId(), ReflectUtils.getColumnName(SubReqAdslLl.class, "status"), oldValue, newValue, issueDateTime);

        cmPosSession.update(subReq);
        cmPosSession.flush();
    }

    public StepInfo getNextStepInfo(Session cmPosSession, SubReqAdslLl subReq) {
        if (subReq == null) {
            return null;
        }
        Boolean cancelSubReq = false;
        Boolean signContract = false;
        Boolean cancelContract = false;
        Boolean activeSubscriber = false;
        Long contractId = null;
        if (Constants.SUB_REQ_STATUS_NEW.equals(subReq.getStatus())) {
            cancelSubReq = true;
            signContract = true;
            return new StepInfo(cancelSubReq, signContract, cancelContract, activeSubscriber, contractId);
        }
        if (Constants.SUB_REQ_STATUS_SIGN_CONTRACT.equals(subReq.getStatus())) {
            cancelContract = true;
            activeSubscriber = true;
            SubAdslLeaseline subAdslll = new SubAdslLeaselineDAO().findById(cmPosSession, subReq.getSubId());
            return new StepInfo(cancelSubReq, signContract, cancelContract, activeSubscriber, subAdslll.getContractId());
        }
        return new StepInfo(cancelSubReq, signContract, cancelContract, activeSubscriber, contractId);
    }

    public Date getLimitDate(Session sessionCmPos, Long subId) {
        if (subId != null) {
            String sql = " FROM SubReqAdslLl WHERE subId = ? AND limitDate IS NOT NULL ORDER BY ID ASC";
            try {
                Query query = sessionCmPos.createQuery(sql);
                query.setParameter(0, subId);
                List<SubReqAdslLl> lst = query.list();
                if (lst != null && !lst.isEmpty()) {
                    SubReqAdslLl subReqAdslLl = lst.get(0);
                    return subReqAdslLl.getLimitDate();
                }
            } catch (HibernateException he) {
                error(_log, "### An error occured while getLimitDate with subId=" + subId + he.getMessage());
            }
        }
        return null;
    }
}
