package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.SubStockModelRel;
import com.viettel.bccs.cm.model.SubStockModelRelReq;
import com.viettel.bccs.cm.util.Constants;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubStockModelRelDAO extends BaseDAO {

    public void insert(Session cmPosSession, List<SubStockModelRelReq> modelReqs, Long saleTransId) throws IllegalAccessException, InvocationTargetException {
        if (modelReqs != null && !modelReqs.isEmpty()) {
            for (SubStockModelRelReq modelReq : modelReqs) {
                if (modelReq != null) {
                    SubStockModelRel subStockModelRel = new SubStockModelRel(modelReq);
                    subStockModelRel.setSaleTransId(saleTransId);

                    cmPosSession.save(subStockModelRel);
                    //cmPosSession.flush();
                }
            }
        }
    }

    public List findBySubId(Session sessionCmPos, Object subId, Logger log) {
        return findByProperty(sessionCmPos, "subId", subId, log);
    }

    public List findByProperty(Session sessionCmPos, String propertyName, Object value, Logger log) {
        log.debug("finding SubStockModelRel instance with property: " + propertyName + ", value: " + value);
        try {
            String queryString = "from SubStockModelRel as model where model.status = ? and model."
                    + propertyName + "= ?";
            Query queryObject = sessionCmPos.createQuery(queryString);
            queryObject.setParameter(0, Constants.STATUS_USE);
            queryObject.setParameter(1, value);
            return queryObject.list();
        } catch (RuntimeException re) {
            log.error("find by property name failed", re);
            throw re;
        }
    }
}
