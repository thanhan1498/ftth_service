package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.SubStockModelRelReq;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubStockModelRelReqDAO extends BaseDAO {

    public SubStockModelRelReq findById(Session session, Long subStockModelRelId) {
        if (subStockModelRelId == null || subStockModelRelId <= 0L) {
            return null;
        }

        String sql = " from SubStockModelRelReq where subStockModelRelId = ? ";
        Query query = session.createQuery(sql).setParameter(0, subStockModelRelId);
        List<SubStockModelRelReq> models = query.list();
        if (models != null && !models.isEmpty()) {
            return models.get(0);
        }

        return null;
    }

    public List<SubStockModelRelReq> findBySubId(Session session, Long subId, String stockTypeName) {
        if (subId == null || subId <= 0L) {
            return null;
        }

        String sql = " from SubStockModelRelReq where subId = ? and status = ? ";
        if (stockTypeName == null) {
            sql += " and stockTypeName is null ";
        } else {
            sql += " and stockTypeName = ? ";
        }
        Query query = session.createQuery(sql).setParameter(0, subId).setParameter(1, Constants.STATUS_USE);
        if(stockTypeName != null)
            query.setParameter(2, stockTypeName);
        List<SubStockModelRelReq> models = query.list();
        return models;
    }
}
