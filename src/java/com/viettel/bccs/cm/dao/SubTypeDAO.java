package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.SubType;
import com.viettel.bccs.cm.util.Constants;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubTypeDAO extends BaseDAO {

    public SubType findById(Session session, String subType) {
        if (subType == null) {
            return null;
        }
        subType = subType.trim();
        if (subType.isEmpty()) {
            return null;
        }

        String sql = " from SubType where subType = ? ";
        Query query = session.createQuery(sql).setParameter(0, subType);
        List<SubType> subTypes = query.list();
        if (subTypes != null && !subTypes.isEmpty()) {
            return subTypes.get(0);
        }

        return null;
    }

    public List<SubType> findByMap(Session session, String serviceType, String productCode) {
        if (serviceType == null || productCode == null) {
            return null;
        }
        serviceType = serviceType.trim();
        if (serviceType.isEmpty()) {
            return null;
        }
        productCode = productCode.trim();
        if (productCode.isEmpty()) {
            return null;
        }

        StringBuilder sql = new StringBuilder()
                .append(" select s from SubType s, MapSubTypeProduct m where s.subType = m.subType and s.status = ? and m.productCode = ?   ")
                .append(" and ").append(createLike(" m.serviceType ")).append(" order by s.name ");

        List params = new ArrayList();
        params.add(Constants.STATUS_USE);
        params.add(productCode);
        params.add(formatLike(serviceType));

        Query query = session.createQuery(sql.toString());
        buildParameter(query, params);

        return query.list();
    }
}
