package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.bussiness.common.CacheBO;
import com.viettel.bccs.cm.model.KpiBonusTask;
import com.viettel.bccs.cm.model.KpiDeadlineTask;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.model.SubReqAdslLl;
import com.viettel.bccs.cm.model.Task;
import com.viettel.bccs.cm.util.Constants;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class TaskDAO extends BaseDAO {

    public void assignDeployTask(Session cmPosSession, Session imSession, String serviceAlias, SubReqAdslLl subReq, Long custReqId, Long nextStepId, Long actionAuditId, Date nowDate) {
        Task task = new Task();

        Long dslamId = subReq.getDslamId();
        Long teamId = (dslamId != null) ? getTeamIdByDslamId(imSession, dslamId) : null;
        if (teamId == null) {
            task.setStatus(1L);// Chua giao cho to
        } else {
            task.setStatus(2L);// Trang thai da giao cho to
            task.setTeamId(teamId);
        }
        task.setTaskTypeId(1L);// viec trien khai
        task.setReqId(custReqId);
        task.setSubId(subReq.getSubId());
        task.setTaskName("Trien khai cho yeu cau: " + custReqId + " dich vu " + serviceAlias);
        task.setStepId(nextStepId);
        task.setProgress(0L);
        task.setStaDate(nowDate);
        task.setCreateDate(nowDate);
        if (serviceAlias != null) {
            task.setDeadline(getDeadLine(serviceAlias, nowDate));
        }
        task.setDeployAddress(subReq.getAddress());
        task.setAreaCode(subReq.getAddressCode());
        task.setDslamId(dslamId);

        Long taskId = getSequence(cmPosSession, Constants.TASK_SEQ);
        task.setTaskId(taskId);

        cmPosSession.save(task);
        cmPosSession.flush();

        new ActionDetailBussiness().insert(cmPosSession, actionAuditId, ReflectUtils.getTableName(Task.class), task.getTaskId(), ReflectUtils.getColumnName(Task.class, "taskId"), null, task.getTaskId(), nowDate);
    }

    public Long getTeamIdByDslamId(Session imSession, Long dslamId) {
        if (dslamId == null) {
            return null;
        }
        Long teamId = null;
//        try {
//            Giao tiep CM-IM: Chua dung, comment lai
//            teamId = CommonSaleTrans.getTeamIdByDslamId(imSession, dslamId);
//        } catch (Exception ex) {
//            return null;
//        }
        return teamId;
    }

    public Date getDeadLine(String telService, Date nowDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(nowDate);

        if (telService != null && Constants.SERVICE_ALIAS_MOBILE.equals(telService)) {
            calendar.add(Calendar.DAY_OF_YEAR, Constants.DEADLINE_MOBILE_INTERVAL);
        }

        if (telService != null && Constants.SERVICE_ALIAS_HOMEPHONE.equals(telService)) {
            calendar.add(Calendar.DAY_OF_YEAR, Constants.DEADLINE_HOMEPHONE_INTERVAL);
        }

        if (telService != null && Constants.SERVICE_ALIAS_PSTN.equals(telService)) {
            calendar.add(Calendar.DAY_OF_YEAR, Constants.DEADLINE_PSTN_INTERVAL);
        }

        if (telService != null && Constants.SERVICE_ALIAS_LEASEDLINE.equals(telService)) {
            calendar.add(Calendar.DAY_OF_YEAR, Constants.DEADLINE_LEASELINE_INTERVAL);
        }

        if (telService != null && Constants.SERVICE_ALIAS_ADSL.equals(telService)) {
            calendar.add(Calendar.DAY_OF_YEAR, Constants.DEADLINE_ADSL_INTERVAL);
        }

        return calendar.getTime();
    }

    public List<KpiDeadlineTask> getListKpiDeadlineTask(Session cmPosSession) {
        String sql = " select JOB_TYPE jobType,"
                + "service_type serviceType,"
                + "nvl(infra_Type,'AON') infraType,"
                + "is_vip isVip,"
                + "OVERTIME overTime, "
                + "WARNING warningTime "
                + " from KPI_DEADLINE_TASK "
                + "where status = 1";
        Query query = cmPosSession.createSQLQuery(sql)
                .addScalar("jobType", Hibernate.STRING)
                .addScalar("serviceType", Hibernate.STRING)
                .addScalar("infraType", Hibernate.STRING)
                .addScalar("isVip", Hibernate.LONG)
                .addScalar("overTime", Hibernate.LONG)
                .addScalar("warningTime", Hibernate.LONG)
                .setResultTransformer(Transformers.aliasToBean(KpiDeadlineTask.class));
        return query.list();
    }

    public List<KpiBonusTask> getListKpiBonusTask(Session cmPosSession) {
        String sql = "select JOB_TYPE     jobType, "
                + "       SERVICE_TYPE serviceType, "
                + "       nvl(INFRA_TYPE,'AON')   infraType, "
                + "       IS_VIP       isVip, "
                + "       KPI          kpi, "
                + "       AMOUNT_BONUS amount,"
                + "       PROVINCE province,"
                + "       LAST_LEVEL lastLevel,"
                + "       PROVINCE || '_' || JOB_TYPE || '_' || SERVICE_TYPE || '_' || nvl(INFRA_TYPE,'AON') || '_' || IS_VIP as keyDic "
                + "  from KPI_BONUS_TASK "
                + " where status = 1 "
                + " order by JOB_TYPE, SERVICE_TYPE, INFRA_TYPE,IS_VIP,kpi ";
        Query query = cmPosSession.createSQLQuery(sql)
                .addScalar("jobType", Hibernate.STRING)
                .addScalar("serviceType", Hibernate.STRING)
                .addScalar("infraType", Hibernate.STRING)
                .addScalar("isVip", Hibernate.INTEGER)
                .addScalar("kpi", Hibernate.DOUBLE)
                .addScalar("amount", Hibernate.DOUBLE)
                .addScalar("keyDic", Hibernate.STRING)
                .addScalar("province", Hibernate.STRING)
                .addScalar("lastLevel", Hibernate.LONG)
                .setResultTransformer(Transformers.aliasToBean(KpiBonusTask.class));
        return query.list();
    }

    public KpiDeadlineTask getKpiDeadLineTask(Session cmPosSession, String jobCode, String infraType, String productCode, String subType, String serviceType) {
        KpiDeadlineTask kpi = null;
        HashMap<String, KpiDeadlineTask> mapKpiDeadline = CacheBO.getListKpiDeadlineTask(cmPosSession);
        if (mapKpiDeadline != null) {
            infraType = infraType == null ? "AON" : infraType;
            boolean isVip = new SubAdslLeaselineDAO().isAccountVip(cmPosSession, subType, productCode);
            kpi = mapKpiDeadline.get((jobCode == null ? Constants.TASK_JOB_CODE_DEFAULT : jobCode) + "_" + serviceType + "_" + infraType + "_" + (isVip ? "1" : "0"));

        }
        return kpi;
    }
}
