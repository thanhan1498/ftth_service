package com.viettel.bccs.cm.dao;

import com.viettel.bccs.api.Task.BO.SubIptv;
import com.viettel.bccs.api.Task.BO.SubPstn;
import com.viettel.bccs.api.Task.DAO.ContractDAO;
import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.bussiness.ContractBussiness;
import static com.viettel.bccs.cm.bussiness.SubscriberBussiness.getAmountTax;
import static com.viettel.bccs.cm.bussiness.SubscriberBussiness.getNameMethodPay;
import static com.viettel.bccs.cm.bussiness.SubscriberBussiness.getPayAdvAmount;
import com.viettel.bccs.cm.database.BO.SubStockModelRel;
import com.viettel.bccs.cm.model.ApDomain;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.Contract;
import com.viettel.brcd.common.util.DateTimeUtils;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.brcd.dao.pre.IsdnSendSmsDAO;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.InvoiceListStaff;
import com.viettel.bccs.cm.model.KpiDeadlineTask;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubBundleTv;
import com.viettel.bccs.cm.model.SubReqAdslLl;
import com.viettel.bccs.cm.model.TaskManagement;
import com.viettel.bccs.cm.model.TechnicalConnector;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.dao.pre.ApParamDAO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import org.hibernate.Query;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class TaskManagementDAO extends BaseDAO {

    protected Logger logger = Logger.getLogger(TaskManagementDAO.class.getName());

    public void insert(Session cmPosSession, Session cmPreSession, Session ccSession, Session nimsSession, String regType, SubReqAdslLl subReq, SubAdslLeaseline sub, Contract contract, Shop shop, Staff staff, Date nowDate, Long actionAuditId, Date issueDateTime, String locale, String delayReason) throws Exception {
        //<editor-fold defaultstate="collapsed" desc="insert TaskManagement">
        TaskManagement taskManagement = new TaskManagement();
        taskManagement.setReqType(regType);
        taskManagement.setServiceType(sub.getServiceType());
        String taskName = LabelUtil.formatKey("task.name.1", locale, StringValueOf(sub.getCustReqId()), sub.getServiceType());
        if (sub.getAccBundleTv() != null) {
            SubBundleTv subTV = new SubBundleTvDAO().findBundleTvByAccount(cmPosSession, sub.getAccBundleTv());
            taskManagement.setPathFileAttach(sub.getAccBundleTv());
            taskManagement.setFileName(subTV.getId().toString());
            taskName += ", have bundle tv";
        }
        taskManagement.setTaskName(taskName);
        Calendar cStart = Calendar.getInstance();
        cStart.setTime(nowDate);
        /*Neu thoi gian tu 17h30 - 7h59 default la 8h hom sau*/
        if (cStart.get(Calendar.HOUR_OF_DAY) > 17 || (Calendar.HOUR_OF_DAY == 17 && cStart.get(Calendar.MINUTE) >= 30)) {
            cStart.add(Calendar.DATE, 1);
            cStart.set(Calendar.HOUR_OF_DAY, 8);
            cStart.set(Calendar.MINUTE, 0);
            cStart.set(Calendar.SECOND, 0);
            cStart.set(Calendar.MILLISECOND, 0);
        } else if (cStart.get(Calendar.HOUR_OF_DAY) < 8) {
            cStart.set(Calendar.HOUR_OF_DAY, 8);
            cStart.set(Calendar.MINUTE, 0);
            cStart.set(Calendar.SECOND, 0);
            cStart.set(Calendar.MILLISECOND, 0);
        }
        nowDate = cStart.getTime();
        taskManagement.setCreateDate(nowDate);

        /*Get limit date from KPI deadline*/
        Long hourDeadline = null;
        KpiDeadlineTask kpi = new TaskDAO().getKpiDeadLineTask(cmPosSession, null, sub.getInfraType(), sub.getProductCode(), sub.getSubType(), sub.getServiceType());
        if (kpi != null) {
            hourDeadline = kpi.getOverTime();
        }

        if (hourDeadline == null) {
            taskManagement.setLimitDate(subReq.getLimitDate());
        } else {
            Calendar cEnd = Calendar.getInstance();
            cEnd.setTime(nowDate);
            cEnd.add(Calendar.HOUR, hourDeadline.intValue());
            taskManagement.setLimitDate(cEnd.getTime());
        }
        taskManagement.setCustReqId(subReq.getCustReqId());

        Long staffId = staff.getStaffId();
        if (staffId == null || staffId <= 0L) {
            staffId = sub.getStaffId();
        }
        taskManagement.setUserId(staffId);

        taskManagement.setProgress(Constants.TASK_PROGRESS_WORKING);
        taskManagement.setStatus(Constants.STATUS_USE);
        taskManagement.setSourceType(Constants.TASK_SOURCE_TYPE);
        taskManagement.setJobCode(Constants.TASK_JOB_CODE_DEFAULT);
        taskManagement.setContractNo(contract.getContractNo());
        taskManagement.setProductCode(sub.getProductCode());
        taskManagement.setContractDate(contract.getSignDate());
        taskManagement.setStarContractDate(contract.getSignDate());
        taskManagement.setName(sub.getUserUsing());

        String tel = contract.getTelFax();
        if (tel != null) {
            tel = tel.trim();
        }
        if (tel == null || tel.isEmpty()) {
            tel = contract.getTelMobile();
        }
        if (tel != null) {
            tel = tel.trim();
        }
        taskManagement.setTelFax(tel);

        taskManagement.setAreaCode(sub.getDeployAreaCode());
        String deployAddress = sub.getDeployAddress();
        taskManagement.setDeployAddress(deployAddress);
        taskManagement.setSubId(sub.getSubId());
        taskManagement.setDslamId(sub.getDslamId());
        taskManagement.setAccount(sub.getAccount());
//        // Bien dung nhan biet cong viec duoc giao tren mobile
//        taskManagement.setMobile(Constants.DEPLOY_ON_SMART_PHONE);

        Long taskMngtId = getSequence(cmPosSession, Constants.TASK_MANAGEMENT_SEQ);
        taskManagement.setTaskMngtId(taskMngtId);
        //duyetdk
        if (delayReason != null && !delayReason.trim().isEmpty()) {
            taskManagement.setDelayReason(delayReason);
        }
        taskManagement.setOriginalCreateDate(issueDateTime);

        cmPosSession.save(taskManagement);
        cmPosSession.flush();
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="luu log action detail">
        new ActionDetailBussiness().insert(cmPosSession, actionAuditId, ReflectUtils.getTableName(TaskManagement.class), taskMngtId, ReflectUtils.getColumnName(TaskManagement.class, "taskMngtId"), null, taskMngtId, issueDateTime);
        //</editor-fold>
        Long connectorId = sub.getCableBoxId();
        List<TechnicalConnector> listTechConnector = new TechnicalConnectorDAO().findStaffManageConnector(cmPosSession, nimsSession, connectorId, sub.getInfraType());
        Long teamOfStaffManageConnector = null;
        Staff staffManage = null;
        StringBuilder taskStaffMngtIdOut = new StringBuilder();
        if (listTechConnector != null && !listTechConnector.isEmpty()) {
            staffManage = new StaffDAO().findById(cmPosSession, listTechConnector.get(0).getStaffId());
            if (staffManage != null) {
                teamOfStaffManageConnector = staffManage.getShopId();
                sub.setStaffAssign(staffManage.getStaffId());
                cmPosSession.save(sub);
                cmPosSession.flush();
                Long taskShopId = new TaskShopManagementDAO().insert(cmPosSession, taskMngtId, staffManage.getShopId(), sub, nowDate, actionAuditId, issueDateTime);
                if (taskShopId != null) {
                    Date dt = new Date();
                    Calendar c = Calendar.getInstance();
                    c.setTime(dt);
                    c.add(Calendar.DATE, 7);
                    dt = c.getTime();
                    if (hourDeadline != null) {
                        dt = taskManagement.getLimitDate();
                    }
                    String result = new TaskStaffManagementDAO().assignTaskForStaff(
                            cmPosSession, ccSession, actionAuditId, nowDate, staffManage.getStaffId(),
                            com.viettel.bccs.api.Util.DateTimeUtils.convertDateTimeToString(nowDate)/*startDate*/, com.viettel.bccs.api.Util.DateTimeUtils.convertDateTimeToString(dt)/*endDate*/,
                            taskShopId, staffManage.getTel(), staff.getStaffCode(), shop.getShopCode(),
                            304518l, sub.getSubId(), taskManagement.getReqType(), staffId, taskStaffMngtIdOut);
                }
            }
        }
        if (teamOfStaffManageConnector == null) {
            new TaskShopManagementDAO().insert(cmPosSession, taskMngtId, sub.getTeamId(), sub, nowDate, actionAuditId, issueDateTime);
        }

        List params = new ArrayList();
        params.add(taskName);
        params.add(contract.getContractNo());
        params.add(sub.getProductCode());
        params.add(deployAddress);
        params.add(DateTimeUtils.formatddMMyyyyHHmmss(nowDate));
        params.add(DateTimeUtils.formatddMMyyyyHHmmss(taskManagement.getLimitDate()));
        String content;
        List<Staff> staffs = new StaffDAO().findByShop(cmPosSession, teamOfStaffManageConnector == null ? sub.getTeamId() : teamOfStaffManageConnector);
        if (teamOfStaffManageConnector == null) {
            Shop shopAssign = new ShopDAO().findById(cmPosSession, sub.getTeamId());
            /*push notification to team*/
            if (staffs != null && !staffs.isEmpty()) {
                List<String> listStr = Arrays.asList(sub.getAccount(), shopAssign.getShopCode(), com.viettel.bccs.api.Util.DateTimeUtils.convertDateTimeToString(taskManagement.getLimitDate()));
                List<String> listContentNotification = new IsdnSendSmsDAO().genarateSMSContent(cmPreSession, null, Constants.AP_PARAM_PARAM_TYPE_NOTIFICATION, "ASSIGN_TASK", listStr);
                for (Staff tempStaff : staffs) {
                    try {
                        if (listContentNotification != null && !listContentNotification.isEmpty()) {
                            String tempNotification = "";
                            for (String temp : listContentNotification) {
                                tempNotification += temp + Constants.NEW_LINE;
                            }
                            if (!tempNotification.isEmpty()) {
                                StringBuilder stringBui = new StringBuilder(tempNotification);
                                stringBui.replace(tempNotification.lastIndexOf(Constants.NEW_LINE), tempNotification.lastIndexOf(Constants.NEW_LINE) + Constants.NEW_LINE.length(), "");
                                tempNotification = stringBui.toString();
                                new NotificationMessageDAO().generateNotificationData(
                                        cmPosSession,
                                        tempStaff.getStaffId(),
                                        tempStaff.getStaffCode(),
                                        staff.getStaffCode(),
                                        LabelUtil.getKey("Notification.AssignTask.title", locale),
                                        tempNotification,/*message*/
                                        sub.getAccount(),/*account*/
                                        taskManagement.getTaskMngtId(),
                                        null,/*taskStaffMngtId*/
                                        Constants.NOTIFICATION_ACTION_TYPE_ASSIGN_TASK_FOR_TEAM/*actionType*/);
                            }
                        }
                    } catch (Exception ex) {
                        logger.error("notification: " + ex.getMessage());
                    }
                }
            }

            List<String> listContentSms = new IsdnSendSmsDAO().genarateSMSContent(cmPreSession, null, Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.SMS_ASSIGN_TASK_APPARAM_CODE, params);
            if (listContentSms != null && !listContentSms.isEmpty()) {
                for (String temp : listContentSms) {
                    new IsdnSendSmsDAO().insertAssignTaskSms(cmPreSession, cmPosSession, temp, staffs, new Date(), shop.getShopCode(), staff.getStaffCode());
                }
            }

        } else if (staffManage != null && staffManage.getStaffCode() != null) {
            /*pushNotification*/
            List<String> listStr = Arrays.asList(sub.getAccount(), staffManage.getName(), com.viettel.bccs.api.Util.DateTimeUtils.convertDateTimeToString(taskManagement.getLimitDate()));
            List<String> listContentNotification = new IsdnSendSmsDAO().genarateSMSContent(cmPreSession, null, Constants.AP_PARAM_PARAM_TYPE_NOTIFICATION, "ASSIGN_TASK", listStr);

            try {
                if (listContentNotification != null && !listContentNotification.isEmpty()) {
                    String tempNotification = "";
                    for (String temp : listContentNotification) {
                        tempNotification += temp + Constants.NEW_LINE;
                    }
                    if (!tempNotification.isEmpty()) {
                        StringBuilder stringBui = new StringBuilder(tempNotification);
                        stringBui.replace(tempNotification.lastIndexOf(Constants.NEW_LINE), tempNotification.lastIndexOf(Constants.NEW_LINE) + Constants.NEW_LINE.length(), "");
                        tempNotification = stringBui.toString();
                        new NotificationMessageDAO().generateNotificationData(
                                cmPosSession,
                                staffManage.getStaffId(),
                                staffManage.getStaffCode(),
                                staff.getStaffCode(),
                                LabelUtil.getKey("Notification.AssignTask.title", locale),
                                tempNotification,/*message*/
                                taskManagement.getAccount(),/*account*/
                                Long.valueOf(taskManagement.getTaskMngtId()),
                                Long.valueOf(taskStaffMngtIdOut.toString()),/*taskStaffMngtId*/
                                Constants.NOTIFICATION_ACTION_TYPE_UPDATE_TASK/*actionType*/);
                    }
                }
            } catch (Exception ex) {
                logger.error("notification: " + ex.getMessage());
            }
            params.add(staffManage.getStaffCode());
            List<String> listContentSms = new IsdnSendSmsDAO().genarateSMSContent(cmPreSession, null, Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.SMS_ASSIGN_TASK_ANNOUNCE_TEAM_SMS, params);
            if (listContentSms != null && !listContentSms.isEmpty()) {
                for (String temp1 : listContentSms) {
                    new IsdnSendSmsDAO().insertAssignTaskSms(cmPreSession, cmPosSession, temp1, staffs, new Date(), shop.getShopCode(), staff.getStaffCode());
                }
            }
        }

    }

    public List<TaskManagement> findByProperty(Session session, String propertyName, Object value) {
        if (value == null) {
            return null;
        }

        String queryString = "from TaskManagement as model where model.status = ? and model."
                + propertyName + "= ?";
        Query queryObject = session.createQuery(queryString);
        queryObject.setParameter(0, Constants.STATUS_USE);
        queryObject.setParameter(1, value);
        List<TaskManagement> reqs = queryObject.list();
        if (reqs != null && !reqs.isEmpty()) {
            return reqs;
        }

        return null;
    }

    public TaskManagement findById(Session cmPosSession, Long taskMngtId) {
        List<TaskManagement> list = findByProperty(cmPosSession, "taskMngtId", taskMngtId);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    public boolean createTaskManagement(Session sessionCmPos, Session sessionIm, Session sessionNims, Session sessionCmPre, Session sessionCc,
            SubAdslLeaseline sub, Date nowDate, Long actionAuditId, final String regType,
            Date limitDate, String telFax, Long userId, String locale, String loginName, String shopCode, String delayReason) throws Exception {
        try {
            /* Service for ADSL or PSTN*/
            List<String> lst = new ArrayList();
            lst.add(String.valueOf(sub.getCustReqId()));
            String taskName = "Survey for request code: {0}, service";
            String deployAddress = "";
            String deployAreaCode = "";
            Long dslamId = null;
            String account = "";
            String serviceType = "";
            String isdn = "";
            Long teamId = null;
            Date limitDateReq = null;
            Double deposit = 0d;
            String custName = "";
            String isdnCust = "";
            String isdnUser = "";
            Double payAmount = 0d;
            Double amountTax = 0d;
            Long connectorId = null;
            String accountBundleTv = null;
            String idBundleTv = null;
            /*----------------------------------------------------------------*/
            taskName += " " + sub.getServiceType();
            deployAddress = sub.getDeployAddress();
            deployAreaCode = sub.getDeployAreaCode();
            dslamId = sub.getDslamId();
            account = sub.getAccount();
            teamId = sub.getTeamId();
            isdn = sub.getAccount();
            deposit = sub.getDeposit();
            connectorId = sub.getCableBoxId();
            serviceType = Constants.SERVICE_ALIAS_FTTH;
            limitDateReq = new SubReqAdslLlDAO().getLimitDate(sessionCmPos, sub.getSubId());
            if (sub.getAccBundleTv() != null) {
                taskName += ", have bunblde tv";
                accountBundleTv = sub.getAccBundleTv();
                SubBundleTv SubBundleTv = new SubBundleTvDAO().getBundleTvBySubId(sessionCmPos, sub.getSubId());
                idBundleTv = SubBundleTv.getId().toString();
            }

            Contract contract = new ContractBussiness().findByIdActive(sessionCmPos, sub.getContractId());
            Customer customer = null;
            if (contract != null) {
                isdnCust = contract.getTelMobile();
                customer = (Customer) sessionCmPos.get(Customer.class.getName(), contract.getCustId());
            }
            if (customer != null) {
                custName = customer.getName();
            }
            StaffDAO staffDao = new StaffDAO();
            Staff staff = staffDao.findById(sessionCmPos, userId);
            isdnUser = staff.getTel();
            //set object
            TaskManagement taskManagement = new TaskManagement();
            taskManagement.setCustReqId(sub.getCustReqId());
            if (userId != null && userId > 0L) {
                taskManagement.setUserId(userId);
            } else {
                taskManagement.setUserId(sub.getStaffId());
            }

            if (Constants.TASK_REQ_TYPE_CHANGE_DEPLOY_ADDRESS.equals(regType)) {
                taskName = LabelUtil.formatKey("task.name.2", locale) + isdn;
                taskManagement.setLimitDate(limitDate);
            } else {
                return false;
            }
            taskManagement.setTaskName(taskName);
            taskManagement.setServiceType(serviceType);
            taskManagement.setProgress(Constants.TASK_PROGRESS_WORKING);
            taskManagement.setStatus(Constants.STATUS_USE);
            taskManagement.setSourceType(Constants.TASK_SOURCE_TYPE);
            taskManagement.setJobCode(Constants.TASK_JOB_CODE_DEFAULT);
            taskManagement.setContractNo(contract.getContractNo());
            taskManagement.setProductCode(sub.getProductCode());
            taskManagement.setContractDate(contract.getSignDate());
            taskManagement.setStarContractDate(contract.getSignDate());
            taskManagement.setName(sub.getUserUsing());

            if (telFax != null && !"".equals(telFax.trim())) {
                taskManagement.setTelFax(telFax);
            } else {
                taskManagement.setTelFax((contract.getTelFax() != null) ? contract.getTelFax() : contract.getTelMobile()); //QuanT chang telFax telMobile
            }

            taskManagement.setAreaCode(deployAreaCode);
            taskManagement.setDeployAddress(deployAddress);
            taskManagement.setReqType(regType);
            taskManagement.setSubId(sub.getSubId());

            taskManagement.setDslamId(dslamId);
            taskManagement.setAccount(account);
            taskManagement.setFileName(idBundleTv);
            taskManagement.setPathFileAttach(accountBundleTv);
            if (idBundleTv != null) {
                List<SubStockModelRel> listStock = new SubStockModelRelDAO().findBySubId(sessionCmPos, taskManagement.getSubId(), logger);
                for (SubStockModelRel temp : listStock) {
                    if (temp.getTaskMngId() != null && temp.getTaskMngId() > 0) {
                        temp.setTaskMngId(taskManagement.getTaskMngtId());
                        sessionCmPos.save(temp);
                    }
                }
            }
            /*Beu thoi gian bat sau 17h30 thi chuyen sanng 8h ngay hom sau, < 8h thi chuyen thanh 8h*/
            Calendar cStart = Calendar.getInstance();
            cStart.setTime(nowDate);
            /*Neu thoi gian tu 17h30 - 7h59 default la 8h hom sau*/
            if (cStart.get(Calendar.HOUR_OF_DAY) > 17 || (Calendar.HOUR_OF_DAY == 17 && cStart.get(Calendar.MINUTE) >= 30)) {
                cStart.add(Calendar.DATE, 1);
                cStart.set(Calendar.HOUR_OF_DAY, 8);
                cStart.set(Calendar.MINUTE, 0);
                cStart.set(Calendar.SECOND, 0);
                cStart.set(Calendar.MILLISECOND, 0);
            } else if (cStart.get(Calendar.HOUR_OF_DAY) < 8) {
                cStart.set(Calendar.HOUR_OF_DAY, 8);
                cStart.set(Calendar.MINUTE, 0);
                cStart.set(Calendar.SECOND, 0);
                cStart.set(Calendar.MILLISECOND, 0);
            }
            nowDate = cStart.getTime();
            /*Get limit date from KPI deadline*/
            Long hourDeadline = null;
            HashMap<String, KpiDeadlineTask> mapKpiDeadline = getMapKpiDeadlineTask(sessionCmPos);
            if (mapKpiDeadline != null) {
                String infraType = sub.getInfraType() == null ? "AON" : sub.getInfraType();
                boolean isVip = new SubAdslLeaselineDAO().isAccountVip(sessionCmPos, sub.getSubType(), sub.getProductCode());
                KpiDeadlineTask hourKpi = mapKpiDeadline.get(Constants.TASK_JOB_CODE_DEFAULT + "_" + sub.getServiceType() + "_" + infraType + "_" + (isVip ? "1" : "0"));
                if (hourKpi != null) {
                    hourDeadline = hourKpi.getOverTime();
                }
            }

            if (hourDeadline != null) {
                Calendar cEnd = Calendar.getInstance();
                cEnd.setTime(nowDate);
                cEnd.add(Calendar.HOUR, hourDeadline.intValue());
                taskManagement.setLimitDate(cEnd.getTime());
            }

            if (StringUtils.isNotEmpty(delayReason)) {
                taskManagement.setDelayReason(delayReason);
            }
            taskManagement.setOriginalCreateDate(new Date());
            taskManagement.setCreateDate(nowDate);
            Long taskMngId = getSequence(sessionCmPos, "TASK_MANAGEMENT_SEQ");
            taskManagement.setTaskMngtId(taskMngId);
            taskManagement.setMobile("9");

            sessionCmPos.save(taskManagement);
            logger.info("### Save taskManagement");

            /*Kiem tra nhan vien quan ly ha tang, de giao giao viec cho nhan vien*/
            Long teamOfStaffManageConnector = null;
            Staff staffManage = null;

            if (connectorId != null) {
                List<TechnicalConnector> listTechConnector = null;
                try {
                    listTechConnector = new TechnicalConnectorDAO().findStaffManageConnector(sessionCmPos, sessionNims, connectorId,
                            sub.getInfraType() == null ? "AON" : sub.getInfraType());
                } catch (Exception e) {
                    logger.info("### Cannot find listTechConnector with connectorId=" + connectorId, e);
                }
                if (listTechConnector != null && !listTechConnector.isEmpty()) {
                    staffManage = new StaffDAO().findById(sessionCmPos, listTechConnector.get(0).getStaffId());
                    if (staffManage != null) {
                        teamOfStaffManageConnector = staffManage.getShopId();
                        sub.setStaffAssign(staffManage.getStaffId());
                        sessionCmPos.save(sub);

                        Long taskShopId = new TaskShopManagementDAO().createTaskShopManagementAssign(sessionCmPos, sub, logger, nowDate, actionAuditId, taskMngId, staffManage.getShopId());
                        if (taskShopId != null) {
                            Date dt = new Date();
                            Calendar c = Calendar.getInstance();
                            c.setTime(dt);
                            c.add(Calendar.DATE, 7);
                            dt = c.getTime();
                            if (hourDeadline != null) {
                                dt = taskManagement.getLimitDate();
                            }
                            String startDate = DateTimeUtils.convertDateTimeToStringV2(taskManagement.getCreateDate(), "dd/MM/yyyy HH:mm:ss");
                            String endDate = DateTimeUtils.convertDateTimeToStringV2(dt, "dd/MM/yyyy HH:mm:ss");
                            Long reason = 304518l;
                            Long staffId = staffManage.getStaffId();
                            String result = new ManageTaskDAO().assignTaskForStaffManageConnector(sessionCmPos, sessionCc, startDate, endDate, staffId, actionAuditId,
                                    nowDate, reason, taskManagement.getTaskMngtId(), staffManage.getTel() == null ? isdnUser : staffManage.getTel(),
                                    staff.getStaffId(), shopCode, loginName, locale);
                        }
                    }
                }
            }
            /*thuc hien giao viec cho to doi*/
            if (teamOfStaffManageConnector == null) { // insert taskShopManagement
                new TaskShopManagementDAO().insert(sessionCmPos, taskMngId, teamId, sub, nowDate, actionAuditId, nowDate);
            }

            // insert sms when assin task to team
            List lstDynamic = new ArrayList();
            lstDynamic.add(taskName);
            lstDynamic.add(contract.getContractNo());
            lstDynamic.add(sub.getProductCode());
            lstDynamic.add(deployAddress);
            lstDynamic.add(DateTimeUtils.convertDateTimeToString(nowDate));
            lstDynamic.add(taskManagement.getLimitDate() != null ? DateTimeUtils.convertDateTimeToString(taskManagement.getLimitDate()) : "");
            IsdnSendSmsDAO isdnSendSmsDAO = new IsdnSendSmsDAO();
            List<String> content = null;
            if (teamOfStaffManageConnector != null && staffManage != null) {
                lstDynamic.add(staffManage.getStaffCode());
                content = isdnSendSmsDAO.genarateSMSContent(sessionCmPre, null, Constants.SMS_ASSIGN_TASK_ANNOUNCE_TEAM_SMS, Constants.SMS_ASSIGN_TASK_ANNOUNCE_TEAM_SMS, lstDynamic);
            } else {
                content = isdnSendSmsDAO.genarateSMSContent(sessionCmPre, null, Constants.SMS_ASSIGN_TASK_APPARAM_CODE, Constants.SMS_ASSIGN_TASK_APPARAM_TYPE, lstDynamic);
            }
            new IsdnSendSmsDAO().insertSMSWhenAssignTaskForTeam(sessionCmPre, sessionCmPos, content.get(0), teamOfStaffManageConnector, nowDate, loginName, shopCode);
            //insert content message to customer, staff
            if (deposit == null) {
                deposit = 0d;
            }
            List lstMethodPay = getListMethodPay(sessionCmPos, Constants.TYPE_PAY_METHOD);
            String namePayMethod = "";
            if (contract != null) {
                payAmount = getPayAdvAmount(sessionCmPos, contract.getContractId());
                amountTax = getAmountTax(sessionIm, contract.getContractNo());
                namePayMethod = getNameMethodPay(lstMethodPay, contract.getPayMethodCode().trim());
            }
            deposit = deposit + payAmount + amountTax;
            if (!isdnCust.isEmpty() && !custName.isEmpty()) {
                setMessageToSend(isdnCust, account, deposit, namePayMethod, custName, sessionCmPre, nowDate, loginName, shopCode);
            }
            /*Gui tin nhan khach hang thong bao khuyen mai pay advance*/
            String[] listIsdnCust = null;
            if (isdnCust.contains("-")) {
                listIsdnCust = isdnCust.split("-");
            } else if (isdnCust.contains("/")) {
                listIsdnCust = isdnCust.split("/");
            } else if (isdnCust.contains(",")) {
                listIsdnCust = isdnCust.split(",");
            } else if (isdnCust.contains(";")) {
                listIsdnCust = isdnCust.split(";");
            } else if (isdnCust.contains(" ")) {
                listIsdnCust = isdnCust.split(" ");
            } else {
                listIsdnCust = new String[]{contract.getTelMobile()};
            }

            ApParamDAO apParamDAO = new ApParamDAO();
            List<ApParam> para = apParamDAO.searchApParam(sessionCmPos, "BRANCH", "GROUP1", 11L, sub.getProvince(), logger);
            List<String> listContentSms;
            List<String> listParam = new ArrayList<String>();
            if (para == null || para.isEmpty()) {
                listContentSms = new IsdnSendSmsDAO().genarateSMSContent(sessionCmPre, "", "GROUP2_PAY_ADVANCE_PROMOTION", "SEND_SMS", listParam);
            } else {
                para = apParamDAO.searchApParam(sessionCmPos, "PACKAGE_VIP_1", sub.getProvince(), 11L, null, logger);
                if (para == null || para.isEmpty()) {
                    listContentSms = new IsdnSendSmsDAO().genarateSMSContent(sessionCmPre, "", "GROUP1_NORMAL_PAY_ADVANCE_PROMOTION", "SEND_SMS", listParam);
                } else {
                    listContentSms = new IsdnSendSmsDAO().genarateSMSContent(sessionCmPre, "", "GROUP1_VIP_PAY_ADVANCE_PROMOTION", "SEND_SMS", listParam);
                }
            }

            if (listContentSms != null && !listContentSms.isEmpty()) {
                for (String isdnTemp : listIsdnCust) {
                    isdnTemp = isdnTemp.trim();
                    if (StringUtils.isNotEmpty(isdnTemp)) {
                        for (String contentStr : listContentSms) {
                            new IsdnSendSmsDAO().insertSMSWhenSignContractSuccessful(sessionCmPre, isdnTemp, contentStr, nowDate, loginName, shopCode);
                        }
                    }
                }
            }

            if (!isdnUser.isEmpty() && !custName.isEmpty()) {
                setMessageToSend(isdnUser, account, deposit, namePayMethod, custName, sessionCmPre, nowDate, loginName, shopCode);
            }
        } catch (Exception e) {
            logger.error("### An error occured while create new task for change deployment address", e);
            return false;
        }
        return true;
    }

    private static HashMap<String, KpiDeadlineTask> getMapKpiDeadlineTask(Session sessionCmPos) {
        HashMap<String, KpiDeadlineTask> listKpiDeadlineTask = null;
        List<KpiDeadlineTask> list = new TaskManagementDAO().getListKpiDeadlineTask(sessionCmPos);
        if (list != null && !list.isEmpty()) {
            listKpiDeadlineTask = new HashMap<String, KpiDeadlineTask>();
            for (KpiDeadlineTask deadline : list) {
                listKpiDeadlineTask.put(
                        deadline.getJobType()
                        + "_" + deadline.getServiceType()
                        + "_" + deadline.getInfraType()
                        + "_" + deadline.getIsVip().toString(),
                        deadline);
            }
        }
        return listKpiDeadlineTask;
    }

    private List<KpiDeadlineTask> getListKpiDeadlineTask(Session cmPosSession) {
        String sql = " select JOB_TYPE jobType,"
                + "service_type serviceType,"
                + "nvl(infra_Type,'AON') infraType,"
                + "is_vip isVip,"
                + "OVERTIME overTime,"
                + "WARNING warningTime "
                + " from KPI_DEADLINE_TASK "
                + "where status = 1";
        Query query = cmPosSession.createSQLQuery(sql)
                .addScalar("jobType", Hibernate.STRING)
                .addScalar("serviceType", Hibernate.STRING)
                .addScalar("infraType", Hibernate.STRING)
                .addScalar("isVip", Hibernate.LONG)
                .addScalar("overTime", Hibernate.LONG)
                .addScalar("warningTime", Hibernate.LONG)
                .setResultTransformer(Transformers.aliasToBean(KpiDeadlineTask.class));
        return query.list();
    }

    public static List getListMethodPay(Session cmPost, String type) {
        String sql = "From ApDomain Where status = 1 and id.type = ?";
        Query query = cmPost.createQuery(sql);
        query.setParameter(0, type);
        List<ApDomain> lst = query.list();
        return lst;
    }

    public static void setMessageToSend(String isdn, String account, Double deposit, String namePayMethod, String customerName, Session cmPreSession, Date nowDate, String staffCode, String shopCode) {
        List lstDynamic = new ArrayList();
        lstDynamic.add(customerName);
        lstDynamic.add(account);
        lstDynamic.add(String.valueOf(deposit));
        lstDynamic.add(String.valueOf(namePayMethod));
        List contentSms = new IsdnSendSmsDAO().genarateSMSContent(cmPreSession, "", Constants.SIGN_CONTRACT_SUCCESSFUL_SMS_CODE, Constants.SIGN_CONTRACT_SUCCESSFUL_SMS_TYPE, lstDynamic);
        new IsdnSendSmsDAO().insertSMSWhenSignContractSuccessful(cmPreSession, isdn, contentSms.get(0).toString(), nowDate, staffCode, shopCode);
    }

    //phuonghc - 07012021 - block reason CH_AF_1ST after change address 1 time
    public List<TaskManagement> findListTaskManagementByAccount(Session cmPosSession, String account) {
        String sql = "From TaskManagement where account = ? and job_code = ?";
        Query query = cmPosSession.createQuery(sql);
        query.setParameter(0, account);
        query.setParameter(1, Constants.TASK_JOB_CODE_DEFAULT);
        List<TaskManagement> list = query.list();
        return list;
    }
}
