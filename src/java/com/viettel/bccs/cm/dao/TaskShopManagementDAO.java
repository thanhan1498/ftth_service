package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.common.util.Common;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.TaskShopManagement;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class TaskShopManagementDAO extends BaseDAO {

    public Long insert(Session session, Long taskMngtId, Long teamId, SubAdslLeaseline sub, Date nowDate, Long actionAuditId, Date issueDateTime) {
        TaskShopManagement taskShopManagement = new TaskShopManagement();
        taskShopManagement.setTaskMngtId(taskMngtId);
        taskShopManagement.setStageId(Constants.JOB_STAGE_NEW_CONTRACT);
        taskShopManagement.setShopId(teamId);

        taskShopManagement.setUserId(sub.getStaffId());
        taskShopManagement.setStatus(Constants.STATUS_USE);

        taskShopManagement.setProgress(Constants.TASK_PROGRESS_START);
        taskShopManagement.setDescription("[BCCS] New task");
        taskShopManagement.setCreateDate(nowDate);

        Long taskShopMngtId = getSequence(session, Constants.TASK_SHOP_MANAGEMENT_SEQ);
        taskShopManagement.setTaskShopMngtId(taskShopMngtId);

        session.save(taskShopManagement);
        session.flush();
        //<editor-fold defaultstate="collapsed" desc="luu log action detail">
        new ActionDetailBussiness().insert(session, actionAuditId, ReflectUtils.getTableName(TaskShopManagement.class), taskShopMngtId, ReflectUtils.getColumnName(TaskShopManagement.class, "taskShopMngtId"), null, taskShopMngtId, issueDateTime);
        //</editor-fold>
        return taskShopMngtId;
    }

    public List<TaskShopManagement> findByProperty(Session session, String propertyName, Object value) {
        if (value == null) {
            return null;
        }

        String queryString = "from TaskShopManagement as model where model.status = ? and model."
                + propertyName + "= ?";
        Query queryObject = session.createQuery(queryString);
        queryObject.setParameter(0, Constants.STATUS_USE);
        queryObject.setParameter(1, value);
        List<TaskShopManagement> reqs = queryObject.list();
        if (reqs != null && !reqs.isEmpty()) {
            return reqs;
        }

        return null;
    }

    public TaskShopManagement findByTaskShopId(Session cmPosSession, Long taskShopMngtId) {
        List<TaskShopManagement> list = findByProperty(cmPosSession, "taskShopMngtId", taskShopMngtId);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
    

    public Long createTaskShopManagementAssign(Session sessionCmPos, SubAdslLeaseline sub, Logger log, Date nowDate, Long actionAuditId, Long taskMngtId, Long teamId) {
        Long id = null;
        try {
            TaskShopManagement obj = new TaskShopManagement();

            obj.setTaskMngtId(taskMngtId);
            obj.setStageId(Constants.JOB_STAGE_NEW_CONTRACT);
            obj.setShopId(teamId);

            obj.setUserId(sub.getStaffId());
            obj.setStatus(Constants.STATUS_USE);

            obj.setProgress(Constants.TASK_PROGRESS_START);
            obj.setDescription("[BCCS] New task");
            obj.setCreateDate(nowDate);

            sessionCmPos.save(obj);
            id = obj.getTaskShopMngtId();
            Common.logAuditDetailV2(id, actionAuditId, "TASK_SHOP_MANAGEMENT", "TASK_SHOP_MNGT_ID", null, id, nowDate, sessionCmPos);

        } catch (Exception e) {
            log.error("Error: ", e);
            return null;
        }
        return id;
    }
}
