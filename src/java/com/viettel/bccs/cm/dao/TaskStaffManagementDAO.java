package com.viettel.bccs.cm.dao;

import com.viettel.bccs.api.Task.DAO.ManagerTaskDAO;
import com.viettel.bccs.api.Task.DAO.TaskHistoryDAO;
import com.viettel.bccs.api.Util.Constant;
import com.viettel.bccs.api.Util.DateTimeUtils;
import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.TaskStaffManagement;
import com.viettel.bccs.cm.model.TaskShopManagement;
import com.viettel.bccs.cm.model.TaskManagement;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ReflectUtils;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class TaskStaffManagementDAO extends BaseDAO {

//    public void insert(Session session, Long taskMngtId, Long teamId, SubAdslLeaseline sub, Date nowDate, Long actionAuditId, Date issueDateTime) {
//        TaskShopManagement taskShopManagement = new TaskShopManagement();
//        taskShopManagement.setTaskMngtId(taskMngtId);
//        taskShopManagement.setStageId(Constants.JOB_STAGE_NEW_CONTRACT);
//        taskShopManagement.setShopId(teamId);
//
//        taskShopManagement.setUserId(sub.getStaffId());
//        taskShopManagement.setStatus(Constants.STATUS_USE);
//
//        taskShopManagement.setProgress(Constants.TASK_PROGRESS_START);
//        taskShopManagement.setDescription("[BCCS] New task");
//        taskShopManagement.setCreateDate(nowDate);
//
//        Long taskShopMngtId = getSequence(session, Constants.TASK_SHOP_MANAGEMENT_SEQ);
//        taskShopManagement.setTaskShopMngtId(taskShopMngtId);
//
//        session.save(taskShopManagement);
//        session.flush();
//        //<editor-fold defaultstate="collapsed" desc="luu log action detail">
//        ActionDetailDAO detailDAO = new ActionDetailDAO();
//        detailDAO.insert(session, actionAuditId, ReflectUtils.getTableName(TaskShopManagement.class), taskShopMngtId, ReflectUtils.getColumnName(TaskShopManagement.class, "taskShopMngtId"), null, taskShopMngtId, issueDateTime);
//        //</editor-fold>
//    }
    public List<TaskStaffManagement> findByProperty(Session session, String propertyName, Object value) {
        if (value == null) {
            return null;
        }

        String queryString = "from TaskStaffManagement as model where model.status = ? and model."
                + propertyName + "= ?";
        Query queryObject = session.createQuery(queryString);
        queryObject.setParameter(0, Constants.STATUS_USE);
        queryObject.setParameter(1, value);
        List<TaskStaffManagement> reqs = queryObject.list();
        if (reqs != null && !reqs.isEmpty()) {
            return reqs;
        }

        return null;
    }

    public String assignTaskForStaff(Session cmSession, Session ccSession,
            Long actionAuditId, Date nowDate, Long staffId, String staDate, String endDate,
            Long taskShopMngtId, String telToAssign, String userLogin, String shopCodeLogin, 
            Long reason, Long subId, String reqType, Long userId, StringBuilder taskStaffIdOut) throws Exception {

        String staffIsdn = (telToAssign != null) ? telToAssign : ""; /*Số dt của NV để nhắn tin giao việc*/

        TaskShopManagement taskShop = new TaskShopManagementDAO().findByTaskShopId(cmSession, taskShopMngtId);
        if (taskShop == null) {
//            Giao việc thất bại do công việc không thuộc tổ đội.
            return "-1";
        }
        TaskManagement task = new TaskManagementDAO().findById(cmSession, taskShop.getTaskMngtId());
        if (task == null) {
//            Giao việc thất bại do không tìm được thông tin công việc.
            return "-2";
        }
        subId = task.getSubId();
        reqType = task.getReqType();
        if (Constant.TASK_PROGRESS_END.equals(task.getProgress())) {
//            Công việc đã được nghiệm thu, bạn không được phép thực hiện giao việc.
            return "-3";
        }

        String strDes = "";
        String actionCode = "";
        Staff staff = new StaffDAO().findById(cmSession, staffId);
        if (taskShop.getProgress().equals(Constant.TASK_SHOP_PROGRESS_START)) {
            /*Cập nhật tiến độ cho task_shop_managemet trong trường hợp giao việc mới*/
            new ActionDetailBussiness().insert(cmSession, actionAuditId, ReflectUtils.getTableName(TaskShopManagement.class), taskShop.getTaskShopMngtId(), ReflectUtils.getColumnName(TaskShopManagement.class, "progress"), Constant.TASK_SHOP_PROGRESS_START, Constant.TASK_SHOP_PROGRESS_ONGOING, nowDate);
            strDes = Constant.UPDATE_TASK_ASSIGN_TO_STAFF + " " + staff.getStaffCode();
            actionCode = Constant.ACTION_TASK_ASSIGN_TO_STAFF;
            taskShop.setProgress(Constant.TASK_SHOP_PROGRESS_ONGOING);
            taskShop.setDescription(Constant.ASSIGN_TASK_USE + ": " + staff.getName());
            cmSession.update(taskShop);
            cmSession.flush();
//            cmSession.update(taskShop);
        }

        /*Tạo mới bản ghi TASK_STAFF_MNGT*/
        TaskStaffManagement taskStaff = new TaskStaffManagement();
        Long taskStaffId = getSequence(cmSession, Constants.TASK_STAFF_MANAGEMENT_SEQ);
        taskStaffIdOut.append(taskStaffId.toString());
        taskStaff.setTaskStaffMngtId(taskStaffId);
        taskStaff.setStatus(Constant.STATUS_USE);
        taskStaff.setTaskShopMngtId(taskShopMngtId);
        taskStaff.setStaffId(staffId);
        taskStaff.setProgress(Constant.TASK_STAFF_PROGRESS_START);
        if (staff != null) {
//            Giao nhân viên, người thao tác:
            taskStaff.setDescription(Constant.ASSIGN_TASK_USE + ": " + staff.getName());
        }

        Date endDateTime = DateTimeUtils.convertStringToTime(endDate, "dd/MM/yyyy HH:mm");
        taskStaff.setStaDate(nowDate);
        taskStaff.setFinishDate(endDateTime);

        Date staDateValue = DateTimeUtils.convertStringToDate(staDate);
        Date endDateValue = DateTimeUtils.convertStringToDate(endDate);
        Long ms = endDateValue.getTime() - staDateValue.getTime();
        Long day = ms / (1000 * 60 * 60 * 24);
        taskStaff.setDuration(day);

        taskStaff.setUserId(userId);
        if (task.getSourceType().equals(Constant.TASK_SOURCE_TYPE)
                || Constant.TASK_REQ_TYPE_KS_OW.equals(task.getReqType())
                || Constant.TASK_REQ_TYPE_KS_FLOW_WHITE_LL.equals(task.getReqType())) {
            taskStaff.setStageId(Constant.JOB_STAGE_NEW_CONTRACT);
        } else if (task.getSourceType().equals(Constant.TASK_SOURCE_TYPE_CC)) {
            taskStaff.setStageId(Constant.JOB_STAGE_COMPLAINT);
        }
        cmSession.save(taskStaff);
        cmSession.flush();
//        cmSession.save(taskStaff);

        /*Ghi log việc tạo mới bản ghi TaskStaffManagement*/
        new ActionDetailBussiness().insert(cmSession, actionAuditId, ReflectUtils.getTableName(TaskStaffManagement.class), taskStaff.getTaskStaffMngtId(), ReflectUtils.getColumnName(TaskStaffManagement.class, "taskStaffMngtId"), null, taskStaff.getTaskStaffMngtId(), nowDate);
        TaskHistoryDAO.saveTaskHistory(cmSession, task.getTaskMngtId(), nowDate, strDes, actionCode, reason);

        /*Gửi tin nhắn cho nhân viên được giao*/
        if (!Common.isNullOrEmpty(staffIsdn)) {
            String ip = "Mobile";
            com.viettel.bccs.api.Task.BO.TaskManagement newTask = new com.viettel.bccs.api.Task.BO.TaskManagement();
            newTask.setSourceType(task.getSourceType());
            newTask.setSubId(task.getSubId());
            newTask.setName(task.getName());
            newTask.setCustReqId(task.getCustReqId());
            newTask.setDeployAddress(task.getDeployAddress());
            newTask.setAccount(task.getAccount());
            newTask.setTelFax(task.getTelFax());
            newTask.setSourceType(task.getSourceType());
            newTask.setServiceType(task.getServiceType());
            newTask.setSourceId(task.getSourceId());
            newTask.setLimitCustDate(task.getLimitCustDate());
            newTask.setReqType(task.getReqType());

            String sendMesResult = new ManagerTaskDAO().sendMessage(userLogin, shopCodeLogin, staffIsdn, newTask, ip, ccSession, cmSession);
            if (!Common.isNullOrEmpty(sendMesResult)) {
                return "1"; /*Gửi tn thất bại (Giao việc vẫn thành công nhưng ko gửi được tn cho NV)*/

            }
        } else {
            return "1"; /*Không tìm được số đ/t (Giao việc vẫn thành công nhưng ko gửi được tn cho NV)*/

        }
        return "0"; /*Thành công*/

    }
}
