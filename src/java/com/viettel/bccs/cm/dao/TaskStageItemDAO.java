package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.TaskShopManagement;
import com.viettel.bccs.cm.model.TaskStageItem;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class TaskStageItemDAO extends BaseDAO {

//    public void insert(Session session, Long taskMngtId, Long teamId, SubAdslLeaseline sub, Date nowDate, Long actionAuditId, Date issueDateTime) {
//        TaskShopManagement taskShopManagement = new TaskShopManagement();
//        taskShopManagement.setTaskMngtId(taskMngtId);
//        taskShopManagement.setStageId(Constants.JOB_STAGE_NEW_CONTRACT);
//        taskShopManagement.setShopId(teamId);
//
//        taskShopManagement.setUserId(sub.getStaffId());
//        taskShopManagement.setStatus(Constants.STATUS_USE);
//
//        taskShopManagement.setProgress(Constants.TASK_PROGRESS_START);
//        taskShopManagement.setDescription("[BCCS] New task");
//        taskShopManagement.setCreateDate(nowDate);
//
//        Long taskShopMngtId = getSequence(session, Constants.TASK_SHOP_MANAGEMENT_SEQ);
//        taskShopManagement.setTaskShopMngtId(taskShopMngtId);
//
//        session.save(taskShopManagement);
//        session.flush();
//        //<editor-fold defaultstate="collapsed" desc="luu log action detail">
//        ActionDetailDAO detailDAO = new ActionDetailDAO();
//        detailDAO.insert(session, actionAuditId, ReflectUtils.getTableName(TaskShopManagement.class), taskShopMngtId, ReflectUtils.getColumnName(TaskShopManagement.class, "taskShopMngtId"), null, taskShopMngtId, issueDateTime);
//        //</editor-fold>
//    }

    public List<TaskStageItem> findByProperty(Session session, String propertyName, Object value) {
        if (value == null) {
            return null;
        }

        String queryString = "from TaskStageItem as model where model.status = ? and model."
                + propertyName + "= ?";
        Query queryObject = session.createQuery(queryString);
        queryObject.setParameter(0, Constants.STATUS_USE);
        queryObject.setParameter(1, value);
        List<TaskStageItem> reqs = queryObject.list();
        if (reqs != null && !reqs.isEmpty()) {
            return reqs;
        }

        return null;
    }
}
