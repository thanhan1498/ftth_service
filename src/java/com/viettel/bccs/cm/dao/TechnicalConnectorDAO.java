/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.dao;

import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.cm.model.TechnicalConnector;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.ws.bccsgw.model.Input;
import com.viettel.brcd.ws.bccsgw.model.Param;
import com.viettel.brcd.ws.common.WebServiceClient;
import com.viettel.brcd.ws.model.output.ConnectorInfo;
import com.viettel.brcd.ws.model.output.StaffManageConnectorInfo;
import com.viettel.brcd.ws.model.output.StaffMapConnector;
import com.viettel.brcd.ws.supplier.nims.getInfoInfras.GetSnFromSplReponse;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author cuongdm
 */
public class TechnicalConnectorDAO extends BaseDAO {

    public List<TechnicalConnector> findByConnectorCode(Session cmPos, String connectorCode) {
        return findByProperty(cmPos, "Upper(connectorCode)", connectorCode.toUpperCase(), null);
    }

    public List<TechnicalConnector> findByConnectorId(Session cmPos, Long connectorId, String infraType) {
        return findByProperty(cmPos, "connectorId", connectorId, infraType);
    }

    public List<TechnicalConnector> findByStaffId(Session cmPos, Long staffId) {
        return findByProperty(cmPos, "staffId", staffId, null);
    }

    public List<TechnicalConnector> findByStaffCode(Session cmPos, String staffCode) {
        return findByProperty(cmPos, "Upper(staffCode)", staffCode.toUpperCase(), null);
    }

    public List<TechnicalConnector> findByStationId(Session cmPos, Long stationId) {
        return findByProperty(cmPos, "stationId", stationId, null);
    }

    public List<TechnicalConnector> findByProperty(Session cmPos, String property, Object value, String infraType) {
        // Query check pair(teamCode, stationCode) existed on DB or not.
        String query = "FROM TechnicalConnector WHERE {0} = ? and status = 1 ";
        if (infraType != null) {
            query += " and infraType = ? ";
        }
        query = query.replace("{0}", property);
        Query queryObj = cmPos.createQuery(query);
        queryObj.setParameter(0, value);
        if (infraType != null) {
            queryObj.setParameter(1, infraType);
        }
        return queryObj.list();
    }

    public List<TechnicalConnector> findBySplitterCode(Session cmPos, String splitterCode, String infraType) throws Exception {
        List<Param> param = new ArrayList<Param>();
        param.add(new Param("splitterCode", splitterCode));
        GetSnFromSplReponse snId = (GetSnFromSplReponse) new WebServiceClient().sendRequestViaBccsGW_Param("getNodeTBBySplitterCode", GetSnFromSplReponse.class, param);
        if (snId == null || snId.getReturn() == null) {
            return null;
        }
        return findByConnectorId(cmPos, snId.getReturn(), infraType);
    }

    public List<TechnicalConnector> findStaffManageConnector(Session cmPos, Session nims, Long connectorId, String infraType) throws Exception {
        List<TechnicalConnector> listTechConnector;
        if (Constants.INFRA_AON.equalsIgnoreCase(infraType) || infraType == null || infraType.isEmpty()) {
            listTechConnector = findByConnectorId(cmPos, connectorId, Constants.INFRA_AON);
        } else {
            String splitterCode = Common.getInraDeviceCode(nims, connectorId, true);
            listTechConnector = findBySplitterCode(cmPos, splitterCode, Constants.INFRA_GPON);
        }
        return listTechConnector;
    }

    public List<StaffManageConnectorInfo> getStaffMapConnector(Session cmPos) {
        String sql = "select *\n"
                + "  from (select a.device_code connector,\n"
                + "               b.station_code stationCode,\n"
                + "               tc.STAFF_CODE staffCode,\n"
                + "               sh.shop_code shopCode,\n"
                + "               to_char(tc.CREATE_DATE, 'dd/mm/yyyy hh24:mi:ss') as dateCreate,\n"
                + "               'GPON' infraType,\n"
                + "               tc.create_user userCreate,\n"
                + "               st.name as staffName,\n"
                + "               sh.PROVINCE_CODE branch,\n"
                + "               sh.name shopName\n"
                + "          from nims_fcn.infra_device a\n"
                + "          join nims_fcn.pon_container d\n"
                + "            on a.device_id = d.id\n"
                + "          join nims_fcn.pon_cat_node_model c\n"
                + "            on d.node_model_id = c.id\n"
                + "           and c.type = 3\n"
                + "          join nims_fcn.infra_stations b\n"
                + "            on b.station_id = a.station_id\n"
                + "          JOIN NIMS_FCN.pon_device nt\n"
                + "            ON nt.parent_id = a.device_id\n"
                + "          JOIN NIMS_FCN.infra_device d\n"
                + "            ON d.device_id = nt.device_id\n"
                + "          JOIN technical_connector tc\n"
                + "            on tc.connector_code = a.device_code\n"
                + "           and tc.status = 1\n"
                + "          join staff st\n"
                + "            on st.staff_id = tc.staff_id\n"
                + "           and st.status = 1\n"
                + "          join shop sh\n"
                + "            on sh.shop_id = st.shop_id\n"
                + "           and sh.status = 1\n"
                + "        union all\n"
                + "        select a.odf_code connector,\n"
                + "               b.station_code stationCode,\n"
                + "               tc.STAFF_CODE staffCode,\n"
                + "               sh.shop_code shopCode,\n"
                + "               to_char(tc.CREATE_DATE, 'dd/mm/yyyy hh24:mi:ss') as dateCreate,\n"
                + "               'AON' infraType,\n"
                + "               tc.create_user userCreate,\n"
                + "               st.name as staffName,\n"
                + "               sh.PROVINCE_CODE branch,\n"
                + "               sh.name shopName\n"
                + "          from nims_cn.odn_odf a\n"
                + "          join nims_fcn.infra_stations b\n"
                + "            on a.root_station_id = b.station_id\n"
                + "          JOIN technical_connector tc\n"
                + "            on tc.connector_code = a.odf_code\n"
                + "           and tc.status = 1\n"
                + "          join staff st\n"
                + "            on st.staff_id = tc.staff_id\n"
                + "           and st.status = 1\n"
                + "          join shop sh\n"
                + "            on sh.shop_id = st.shop_id\n"
                + "           and sh.status = 1\n"
                + "        union all\n"
                + "        select a.connector_code connector,\n"
                + "               b.station_code stationCode,\n"
                + "               tc.STAFF_CODE staffCode,\n"
                + "               sh.shop_code shopCode,\n"
                + "               to_char(tc.CREATE_DATE, 'dd/mm/yyyy hh24:mi:ss') as dateCreate,\n"
                + "               'CCN' infraType,\n"
                + "               tc.create_user userCreate,\n"
                + "               st.name as staffName,\n"
                + "               sh.PROVINCE_CODE branch,\n"
                + "               sh.name shopName\n"
                + "          from nims_cn.ccn_connector a\n"
                + "          join nims_cn.ccn_pdd c\n"
                + "            on a.pdd_id = c.pdd_id\n"
                + "          join nims_fcn.infra_stations b\n"
                + "            on c.station_id = b.station_id\n"
                + "          JOIN technical_connector tc\n"
                + "            on tc.connector_code = a.connector_code\n"
                + "           and tc.status = 1\n"
                + "          join staff st\n"
                + "            on st.staff_id = tc.staff_id\n"
                + "           and st.status = 1\n"
                + "          join shop sh\n"
                + "            on sh.shop_id = st.shop_id\n"
                + "           and sh.status = 1\n"
                + "         where a.connector_type_id in ('HC', 'HCG'))\n"
                + " order by staffCode";
        Query query = cmPos.createSQLQuery(sql)
                .addScalar("connector", Hibernate.STRING)
                .addScalar("staffCode", Hibernate.STRING)
                .addScalar("infraType", Hibernate.STRING)
                .addScalar("userCreate", Hibernate.STRING)
                .addScalar("dateCreate", Hibernate.STRING)
                .addScalar("stationCode", Hibernate.STRING)
                .addScalar("shopName", Hibernate.STRING)
                .addScalar("shopCode", Hibernate.STRING)
                .addScalar("staffName", Hibernate.STRING)
                .addScalar("branch", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(StaffMapConnector.class));
        List<StaffMapConnector> list = query.list();

        List<StaffManageConnectorInfo> listReturn = new ArrayList<StaffManageConnectorInfo>();
        String currentStaffCode = "";
        //StaffManageConnectorInfo currentStaff = null;
        List<ConnectorInfo> listConnector = null;
        for (StaffMapConnector staff : list) {
            if (!currentStaffCode.equals(staff.getStaffCode())) {
                StaffManageConnectorInfo currentStaff = new StaffManageConnectorInfo();
                listReturn.add(currentStaff);
                currentStaff.setBranch(staff.getBranch());
                currentStaff.setCode(staff.getStaffCode());
                currentStaff.setName(staff.getStaffName());
                currentStaff.setTeamCode(staff.getShopCode());
                currentStaff.setTeamName(staff.getShopName());

                listConnector = new ArrayList<ConnectorInfo>();
                currentStaff.setConnectors(listConnector);
                listConnector.add(new ConnectorInfo(staff.getConnector(), staff.getInfraType(), staff.getStationCode()));

                currentStaffCode = staff.getStaffCode();
            } else {
                listConnector.add(new ConnectorInfo(staff.getConnector(), staff.getInfraType(), staff.getStationCode()));
            }
        }
        return listReturn;
    }

    /**
     * @author duyetdk
     * @param splitterCode
     * @return
     * @throws Exception
     */
    public GetSnFromSplReponse findBySplitterCode(String splitterCode) throws Exception {
        if (splitterCode == null || splitterCode.trim().isEmpty()) {
            return null;
        }
        List<Param> param = new ArrayList<Param>();
        param.add(new Param("splitterCode", splitterCode));
        GetSnFromSplReponse snId = (GetSnFromSplReponse) new WebServiceClient().sendRequestViaBccsGW_Param("getNodeTBBySplitterCode", GetSnFromSplReponse.class, param);
        if (snId == null || snId.getReturn() == null) {
            return null;
        }
        return snId;
    }
}
