package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.WorkFlow;
import com.viettel.bccs.cm.util.Constants;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class WorkFlowDAO extends BaseDAO {

    public WorkFlow getNextStep(Session cmPosSession, Long telService, Long currentStepId) {
        if (telService == null || telService <= 0L) {
            return null;
        }

        StringBuilder sql = new StringBuilder()
                .append(" from WorkFlow where status = ? and telService = ? ");
        List params = new ArrayList();
        params.add(Constants.STATUS_USE);
        params.add(telService);
        if (currentStepId != null) {
            sql.append(" and currentStepId = ? ");
            params.add(currentStepId);
        } else {
            sql.append(" and currentStepId is null ");
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<WorkFlow> flows = query.list();
        if (flows != null && !flows.isEmpty()) {
            return flows.get(0);
        }

        return null;
    }

    public WorkFlow getCurrentStep(Session cmPosSession, Long telService, Long nextStepId) {
        if (telService == null || telService <= 0L) {
            return null;
        }

        StringBuilder sql = new StringBuilder()
                .append(" from WorkFlow where status = ? and telService = ? ");
        List params = new ArrayList();
        params.add(Constants.STATUS_USE);
        params.add(telService);
        if (nextStepId != null) {
            sql.append(" and nextStepId = ? ");
            params.add(nextStepId);
        } else {
            sql.append(" and nextStepId is null ");
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<WorkFlow> flows = query.list();
        if (flows != null && !flows.isEmpty()) {
            return flows.get(0);
        }

        return null;
    }
}
