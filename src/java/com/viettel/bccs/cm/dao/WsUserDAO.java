package com.viettel.bccs.cm.dao;

import com.viettel.bccs.cm.model.WsUser;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class WsUserDAO extends BaseDAO {

    public List<WsUser> findAll(Session session) {
        String sql = " from WsUser ";
        Query query = session.createQuery(sql);
        List<WsUser> users = query.list();
        return users;
    }
}
