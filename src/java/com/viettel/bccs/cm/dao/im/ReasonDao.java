/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.dao.im;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import com.viettel.im.database.BO.Reason;

/**
 * ReasonDao for Source IM
 *
 * @author phuonghc
 */
public class ReasonDao {
    private static final String STATUS_USE = "1";
    public Reason findById(Session session, Long reasonId) {
        if (reasonId == null || reasonId <= 0L) {
            return null;
        }

        String sql = " from Reason where reasonId = ? and reasonStatus = ? ";
        Query query = session.createQuery(sql).setParameter(0, reasonId).setParameter(1, STATUS_USE);
        List<Reason> reasons = query.list();
        if (reasons != null && !reasons.isEmpty()) {
            return reasons.get(0);
        }
        return null;
    }
}
