/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.dao.im;

import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.im.database.BO.StockCard;
import org.hibernate.Query;
import org.hibernate.Session;
import java.util.List;

/**
 *
 * @author cuongdm
 */
public class StockCardDAO extends BaseSupplier {

    public StockCard getStockCard(Session imSession, Long stockModelId, Long ownereType, Long ownerId) {
        String sql = "from stockCard where stockModelId = ? and  ownerType = ? and ownerId = ? and status = 1 and stateId = 1 and rownum <=1 for update nowait  ";
        Query query = imSession.createQuery(sql);
        query.setParameter(0, stockModelId);
        query.setParameter(1, ownereType);
        query.setParameter(2, ownerId);
        List<StockCard> list = query.list();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }
}
