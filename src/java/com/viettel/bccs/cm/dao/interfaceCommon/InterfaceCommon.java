/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.dao.interfaceCommon;

import com.viettel.bccs.cm.controller.BaseController;
import com.viettel.im.database.BO.StockIsdnBean;
import com.viettel.im.database.BO.StockSerialBean;
import com.viettel.im.database.BO.StockSimBean;
import com.viettel.im.database.DAO.WebServiceIMDAO;
import java.util.List;
import org.hibernate.Session;
import org.apache.log4j.Logger;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.model.pre.SubMbPre;
import com.viettel.bccs.cm.model.pre.SaleTransPre;
import java.util.ArrayList;
import java.util.Date;
import com.viettel.bccs.cm.util.Constants;
import org.apache.commons.beanutils.BeanUtils;
import com.google.gson.Gson;
import com.viettel.bccs.cm.database.DAO.pre.ApDomainDAO;
import com.viettel.im.database.BO.StockSim;

/**
 *
 * @author cuongdm
 */
public class InterfaceCommon extends BaseController {

    protected static String EXE_SALE_TRANS_ACTION_CODE = "00";            //Action tao giao dich
    protected static String UNLOCK_SIM_ACTION_CODE = "04";                //Action unlock sim
    protected static String CANCEL_SALE_TRANS = "08";                     //Action huy giao dich
    protected static String CHECK_STATUS_SIM = "12";
    protected static String CHECK_VALID_ACTION_CODE = "13";
    protected static String LOCK_RESOURCE_ACTION_CODE = "14";             //Action lock tai nguyen
    protected static String RELEASE_ISDN_ACTION_CODE = "15";
    protected static String RELEASE_ISDN_BY_CONTRACT_TERMINATED_ACTION_CODE = "16";
    protected static String REPAIR_SALE_TRANS = "17";
    protected static String UNLOCK_RESOURCE_ACTION_CODE = "18";
    protected static String UNLOCK_IPHONE_ACTION_CODE = "19";
    protected static String RELEASE_PSTN_AND_RESOURCE_BY_TERMINATE_CONTRACT = "20";               // Action release tai nguyen pstn khi cham dut hop dong
    protected static String UPDATE_ISDN_TO_USING_ACTION_CODE = "20";
    public static long CALL_ID = 0;
    protected static String LOCK_PORT_ACTION_CODE = "30";
    protected static String RELEASE_PORT_ACTION_CODE = "31";

    public InterfaceCommon() {
        logger = Logger.getLogger(InterfaceCommon.class);
    }

    public String checkValidResourceByOwnerType(Session imSession, Long ownerType, Long ownerId, String saleServiceCode,
            List<StockIsdnBean> listStockIsdnBean, List<StockSimBean> listStockSimBean, List<StockSerialBean> listStockSerialBean) throws Exception {
        WebServiceIMDAO webIMDAO = new WebServiceIMDAO(imSession);
        String errorMessage = null;

        if (saleServiceCode == null || "".equals(saleServiceCode)) {
            errorMessage = "Connection reason has not configured with sale service";
            return errorMessage;
        }
        Long validStatus = webIMDAO.checkValidResource(ownerType, ownerId, saleServiceCode,
                listStockIsdnBean, listStockSimBean, listStockSerialBean);

        if (WebServiceIMDAO.CVR_ERR_INVALID_LIST_ISDN.equals(validStatus)) {
            if (listStockIsdnBean != null && listStockIsdnBean.size() > 0) {
                for (StockIsdnBean stockIsdn : listStockIsdnBean) {
                    errorMessage = getReturnMessage(CHECK_VALID_ACTION_CODE, stockIsdn.getMessageCode());

                    if (!"".equals(errorMessage)) {
                        return errorMessage;
                    }
                }
            }
        } else if (WebServiceIMDAO.CVR_ERR_INVALID_LIST_SIM.equals(validStatus)) {
            if (listStockSimBean != null && listStockSimBean.size() > 0) {
                for (StockSimBean stockSim : listStockSimBean) {
                    errorMessage = getReturnMessage(CHECK_VALID_ACTION_CODE, stockSim.getMessageCode());

                    if (!"".equals(errorMessage)) {
                        return errorMessage;
                    }
                }
            }
        } else if (WebServiceIMDAO.CVR_ERR_INVALID_LIST_SERIAL.equals(validStatus)) {
            if (listStockSerialBean != null && listStockSerialBean.size() > 0) {
                for (int i = 0; i < listStockSerialBean.size(); i++) {
                    StockSerialBean stockSerial = listStockSerialBean.get(i);
                    // Them vao doan ma de kiem tra dung loi
                    //( tranh truong hop tra ve null thong bao loi la ko xac dinh)
                    if (stockSerial.getMessageCode() == null && i < listStockSerialBean.size() - 1) {
                        continue;
                    }
                    errorMessage = getReturnMessage(CHECK_VALID_ACTION_CODE, stockSerial.getMessageCode());

                    if (!"".equals(errorMessage)) {
                        return errorMessage;
                    }
                }
            }
        } else {
            errorMessage = getReturnMessage(CHECK_VALID_ACTION_CODE, validStatus);

            if (!"".equals(errorMessage)) {
                return errorMessage;
            }
        }

        return errorMessage;
    }

    public static String getReturnMessage(String actionCode, Long returnCode) {

        if (returnCode != null) {

            //Ma loi cua gui giao dich
            if (actionCode != null && actionCode.equals(EXE_SALE_TRANS_ACTION_CODE)) {

                if (returnCode.equals(WebServiceIMDAO.SST_SUCCESS)) {
                    return "";
                } else {
                    if (returnCode.equals(WebServiceIMDAO.SST_ERR_INVALID_PARAMETERS)) {
                        //return "Tham số truyền vào không đúng";
                        return LabelUtil.getKey("e0112.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_INVALID_LIST_NO_SERIAL)) {
                        //return "Danh sách mặt hàng no-serial lỗi do chưa có > 1 mặt hàng no-serial";
                        return LabelUtil.getKey("e0113.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_INVALID_LIST_NO_SERIAL_MODEL_NOT_FOUND)) {
                        //return "Không tìm thấy mặt hàng no-serial";
                        return LabelUtil.getKey("e0114.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_INVALID_LIST_NO_SERIAL_PRICE_NOT_FOUND)) {
                        //return "Không tìm thấy giá mặt hàng no-serial";
                        return LabelUtil.getKey("e0115.im.response", "en_US");
                    }//Phund lib_IM 0610
                    else if (returnCode.equals(WebServiceIMDAO.SST_ERR_INVALID_LIST_SERIAL)) {
                        //return "Danh sách serial truyền vào không đúng serial hoặc mặt hàng";
                        return LabelUtil.getKey("e0116.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_INVALID_LIST_ISDN)) {
                        //return "Danh sách isdn truyền vào không đúng isdn hoặc mặt hàng";
                        return LabelUtil.getKey("e0117.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_INVALID_LIST_SIM)) {
                        //return "Danh sách SIM không đúng serial hoặc mặt hàng";
                        return LabelUtil.getKey("e0118.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_NOT_ENOUGH_PROPERTIES)) {
                        //return "Tham số cần thiết truyền vào không đủ";
                        return LabelUtil.getKey("e0119.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_PRICE_POLICY_NOT_FOUND)) {
                        //return "Không tìm thấy chính sách giá cho cửa hàng";
                        return LabelUtil.getKey("e0120.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERVICES_PRICE_NOT_FOUND)) {
                        //return "Không tim thấy giá dịch vụ";
                        return LabelUtil.getKey("e0121.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_STAFF_NOT_FOUND)) {
                        //return "Thông tin nhân viên đăng nhập truyền vào không đúng hoặc không tồn tại";
                        return LabelUtil.getKey("e0122.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SHOP_NOT_FOUND)) {
                        //return "Không tìm thấy thông tin cửa hàng ứng với nhân viên đăng nhập";
                        return LabelUtil.getKey("e0123.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SHOP_IS_NOT_AGENT)) {
                        //return "Cửa hàng gửi giao dịch không phải là đại lý";
                        return LabelUtil.getKey("e0124.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SALE_SERVICES_NOT_FOUND)) {
                        //return "Không tìm thấy dịch vụ bán hàng";
                        return LabelUtil.getKey("e0125.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ACCOUNT_BALANCE_NOT_EXIST)) {
                        //return "Đại lý chưa có tài khoản thanh toán";
                        return LabelUtil.getKey("e0126.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ACCOUNT_BALANCE_NOT_ENOUGH)) {
                        //return "Số dư tài khoản của đại lý không đủ để thanh toán";
                        return LabelUtil.getKey("e0127.im.response", "en_US");
                        //SIM
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_STOCK_TYPE_NOT_IN_SERVICES)) {
                        //return "Dịch vụ bán hàng không định nghĩa loại mặt hàng SIM";
                        return LabelUtil.getKey("e0128.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_SERIAL_NOT_FOUND)) {
                        //return "Không tìm thấy số Serial trong hệ thống";
                        return LabelUtil.getKey("e0129.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_STOCK_MODEL_NOT_IN_SERVICES)) {
                        //return "Dịch vụ bán hàng không định nghĩa mặt hàng SIM";
                        return LabelUtil.getKey("e0130.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_STAFF_STOCK_NOT_IN_SERVICES)) {
                        //return "Dịch vụ bán hàng không cho phép lấy SIM từ kho nhân viên";
                        return LabelUtil.getKey("e0131.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_SERIAL_NOT_IN_STAFF_STOCK)) {
                        //return "Số Serial không thuộc kho nhân viên";
                        return LabelUtil.getKey("e0132.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_SHOP_STOCK_NOT_IN_SERVICES)) {
                        //return "Dịch vụ bán hàng không cho phép lấy SIM từ kho cửa hàng (đại lý)";
                        return LabelUtil.getKey("e0133.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_SERIAL_NOT_IN_SHOP_STOCK)) {
                        //return "Số Serial không nằm trong kho cửa hàng (đại lý)";
                        return LabelUtil.getKey("e0134.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_SHOP_IS_AGENT)) {
                        //return "Cửa hàng giữ SIM là đại lý ";
                        return LabelUtil.getKey("e0135.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_DETAIL_ERR_NOT_FOUND)) {
                        //return "(SIM)Không tìm thấy chi tiết lỗi";
                        return LabelUtil.getKey("e0136.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_IN_LIST_EMPTY)) {
                        //return "Danh sách SIM gửi giao dịch sang bên Bán hàng bị rỗng (null)";
                        return LabelUtil.getKey("e0137.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_INVALID_STOCK_TYPE)) {
                        //return "Mặt hàng SIM phải là SIM trắng trả trước (hoặc là SIM trắng trả sau)";
                        return LabelUtil.getKey("e0138.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_NOT_FOUND)) {
                        //return "Không tìm thấy serial SIM trong dịch vụ bán hàng";
                        return LabelUtil.getKey("e0139.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_NOT_IN_STOCK)) {
                        //return LabelUtil.getKey("Serial SIM không ở trong kho";
                        return LabelUtil.getKey("e0140.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_QUANTITY_DIFF_ONE)) {
                        //return "Có nhiều hơn 1 serial SIM trong kho";
                        return LabelUtil.getKey("e0141.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_STOCK_MODEL_NOT_FOUND)) {
                        //return "Không tìm thấy mặt hàng của Serial SIM";
                        return LabelUtil.getKey("e0142.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_MODEL_PRICE_OF_SERIAL_NOT_FOUND)) {
                        //return "Không tìm thấy giá của Serial SIM";
                        return LabelUtil.getKey("e0143.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_UPDATE_LIST_SIM_FAIL)) {
                        //return "Không cập nhật được trạng thái danh sách SIM";
                        return LabelUtil.getKey("e0144.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SIM_INVALID_STATUS)) {
                        //return "Không xác định được trạng thái của serial SIM";
                        return LabelUtil.getKey("e0145.im.response", "en_US");
                        //ISDN
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_STOCK_TYPE_NOT_IN_SERVICES)) {
                        //return "Dịch vụ bán hàng không định nghĩa loại mặt hàng ISDN";
                        return LabelUtil.getKey("e0146.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_SERIAL_NOT_FOUND)) {
                        //return "Không tìm thấy số ISDN trong hệ thống";
                        return LabelUtil.getKey("e0147.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_STOCK_MODEL_NOT_IN_SERVICES)) {
                        //return "Dịch vụ bán hàng không định nghĩa mặt hàng ISDN";
                        return LabelUtil.getKey("e0148.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_STAFF_STOCK_NOT_IN_SERVICES)) {
                        //return "Dịch vụ bán hàng không cho phép lấy ISDN từ kho nhân viên";
                        return LabelUtil.getKey("e0149.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_SERIAL_NOT_IN_STAFF_STOCK)) {
                        //return "Số ISDN không thuộc kho nhân viên";
                        return LabelUtil.getKey("e0150.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_SHOP_STOCK_NOT_IN_SERVICES)) {
                        //return "Dịch vụ bán hàng không cho phép lấy ISDN từ kho cửa hàng (đại lý)";
                        return LabelUtil.getKey("e0151.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_SERIAL_NOT_IN_SHOP_STOCK)) {
                        //return "Số ISDN không nằm trong kho cửa hàng (đại lý)";
                        return LabelUtil.getKey("e0152.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_SHOP_IS_AGENT)) {
                        //return "Cửa hàng giữ ISDN là đại lý ";
                        return LabelUtil.getKey("e0153.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_DETAIL_ERR_NOT_FOUND)) {
                        //return "(ISDN)Không tìm thấy chi tiết lỗi";
                        return LabelUtil.getKey("e0157.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_IN_LIST_EMPTY)) {
                        //return "Danh sách ISDN gửi giao dịch sang bên Bán hàng bị rỗng (null)";
                        return LabelUtil.getKey("e0158.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_INVALID_STOCK_TYPE)) {
                        //return "Mặt hàng ISDN phải là ISDN mobile, homephone, hoặc pstn";
                        return LabelUtil.getKey("e0159.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_NOT_NEW_OR_SUSPEND)) {
                        //return "Số ISDN không ở trạng thái mới hoặc trạng thái ngưng sử dụng";
                        return LabelUtil.getKey("e0160.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_SUSPEND_SPECIAL)) {
                        //return "Số ISDN là số đẹp đang ở trạng thái ngưng sử dụng";
                        return LabelUtil.getKey("e0161.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_TYPE_IS_NULL)) {
                        //return "Không tìm thấy loại số (ISDN type) của số ISDN";
                        return LabelUtil.getKey("e0162.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_UPDATE_INFO_IS_NULL)) {
                        //return "Không có thông tin cập nhật số ISDN";
                        return LabelUtil.getKey("e0163.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_STOCK_MODEL_NOT_FOUND)) {
                        //return "Không tìm thấy mặt hàng của ISDN";
                        return LabelUtil.getKey("e0164.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_PRICE_NOT_FOUND)) {
                        //return "Không tìm thấy giá của ISDN";
                        return LabelUtil.getKey("e0165.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_UPDATE_LIST_ISDN_FAIL)) {
                        //return "Không cập nhật được trạng thái danh sách ISDN";
                        return LabelUtil.getKey("e0166.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_INVALID_STATUS)) {
                        //return "Không xác định được trạng thái của ISDN";
                        return LabelUtil.getKey("e0167.im.response", "en_US");
                        //SERIAL
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_STOCK_TYPE_NOT_IN_SERVICES)) {
                        //return "Dịch vụ bán hàng không định nghĩa loại mặt hàng Serial gắn kèm";
                        return LabelUtil.getKey("e0168.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_SERIAL_NOT_FOUND)) {
                        //return "Không tìm thấy số Serial gắn kèm trong hệ thống";
                        return LabelUtil.getKey("e0169.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_STOCK_MODEL_NOT_IN_SERVICES)) {
                        //return "Dịch vụ bán hàng không định nghĩa mặt hàng Serial gắn kèm";
                        return LabelUtil.getKey("e0170.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_STAFF_STOCK_NOT_IN_SERVICES)) {
                        //return "Dịch vụ bán hàng không cho phép lấy Serial gắn kèm từ kho nhân viên";
                        return LabelUtil.getKey("e0171.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_SERIAL_NOT_IN_STAFF_STOCK)) {
                        //return "Số Serial gắn kèm không thuộc kho nhân viên";
                        return LabelUtil.getKey("e0172.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_SERIAL_NOT_IN_STAFF_STOCK)) {
                        //return "Dịch vụ bán hàng không cho phép lấy Serial gắn kèm từ kho cửa hàng (đại lý)";
                        return LabelUtil.getKey("e0173.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_SERIAL_NOT_IN_SHOP_STOCK)) {
                        //return "Số Serial gắn kèm không nằm trong kho cửa hàng (đại lý)";
                        return LabelUtil.getKey("e0174.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_SHOP_IS_AGENT)) {
                        //return "Cửa hàng giữ Serial gắn kèm là đại lý ";
                        return LabelUtil.getKey("e0175.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_DETAIL_ERR_NOT_FOUND)) {
                        //return "(Serial gắn kèm)Không tìm thấy chi tiết lỗi";
                        return LabelUtil.getKey("e0176.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_IN_LIST_EMPTY)) {
                        //return "Danh sách Serial gắn kèm gửi giao dịch sang bên Bán hàng bị rỗng (null)";
                        return LabelUtil.getKey("e0177.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_INVALID_STOCK_TYPE)) {
                        //return "Mặt hàng Serial gắn kèm không phải là mặt hàng handset, thẻ cào, kit";
                        return LabelUtil.getKey("e0178.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_NOT_FOUND)) {
                        //return "Không tìm thấy Serial gắn kèm trong dịch vụ bán hàng";
                        return LabelUtil.getKey("e0179.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_NOT_IN_STOCK)) {
                        //return "Serial gắn kèm không ở trong kho";
                        return LabelUtil.getKey("e0180.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_QUANTITY_DIFF_ONE)) {
                        //return "Có nhiều hơn 1 serial gắn kèm trong kho";
                        return LabelUtil.getKey("e0181.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_STOCK_MODEL_NOT_FOUND)) {
                        //return "Không tìm thấy mặt hàng của Serial gắn kèm";
                        return LabelUtil.getKey("e0182.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_PRICE_OF_SERIAL_NOT_FOUND)) {
                        //return "Không tìm thấy giá của Serial gắn kèm";
                        return LabelUtil.getKey("e0183.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_INVALID_STOCK_MODEL)) {
                        //return "Serial gắn kèm không khớp với mặt hàng được chọn";
                        return LabelUtil.getKey("e0184.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_UPDATE_LIST_SERIAL_FAIL)) {
                        //return "Không cập nhật được trạng thái danh sách serial gắn kèm";
                        return LabelUtil.getKey("e0185.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERIAL_INVALID_STATUS)) {
                        //return "Không xác định được trạng thái của serial gắn kèm";
                        return LabelUtil.getKey("e0186.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ACCOUNT_ANYPAY_CANNOT_ADD)) {
                        //return "Không cộng được tiền vào tài khoản AnyPay bên phân hệ Bán Hàng";
                        return LabelUtil.getKey("e0187.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERVICES_NOT_ENOUGH_PARAM)) {
                        //return "Tham số truyền vào dịch vụ bán hàng không đủ";
                        return LabelUtil.getKey("e0188.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_MODEL_PRICE_SALED_IS_NOT_FOUND)) {
                        //return "Tham số truyền vào dịch vụ bán hàng không đủ";
                        return LabelUtil.getKey("e0240.im.response", "en_US");
                    } else {
                        //return messageUnknowError + ". Mã lỗi (" + returnCode + ")";
                        return "unknow error : " + returnCode;
                    }
                }
            } //Ma loi cua kiem tra trang thai so SIM
            else if (actionCode != null && actionCode.equals(CHECK_STATUS_SIM)) {

                if (returnCode.equals(WebServiceIMDAO.STATUS_SIM_IN_STOCK)) {
                    return "";//Sim dang trong kho
                } else {
                    if (returnCode.equals(WebServiceIMDAO.STATUS_SIM_HAS_SOLD)) {
                        //return "SIM đã bán";
                        return LabelUtil.getKey("e0189.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.STATUS_SIM_WAITING_CONFIRM)) {
                        //return LabelUtil.getKey("SIM đang chờ xác nhận nhập", "en_US");
                        return LabelUtil.getKey("e0190.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.STATUS_SIM_HAS_INTEGRATED)) {
                        //return LabelUtil.getKey("SIM đã đấu KIT", "en_US");
                        return LabelUtil.getKey("e0191.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.STATUS_SIM_HAS_DESTROYED)) {
                        //return LabelUtil.getKey("SIM đã bị hủy", "en_US");
                        return LabelUtil.getKey("e0192.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.STATUS_SIM_HAS_LOCKED)) {
                        //return LabelUtil.getKey("SIM đang bị khóa", "en_US");
                        return LabelUtil.getKey("e0193.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.STATUS_SIM_NOT_EXIST)) {
                        //return LabelUtil.getKey("SIM không tồn tại", "en_US");
                        return LabelUtil.getKey("e0194.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.FSSBIOS_ERR_INVALID_PARAMETERS)) {
                        //return LabelUtil.getKey("Tham số truyền vào không đúng", "en_US");
                        return LabelUtil.getKey("e0112.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.FSSBIOS_ERR_SALE_SERVICES_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0125.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_SIM_NOT_IN_STOCK)) {
                        //return LabelUtil.getKey("SIM không ở trạng thái trong kho", "en_US");
                        return LabelUtil.getKey("e0195.im.response", "en_US");
                    } else {
                        //return LabelUtil.getKey("Danh sách SIM truyền vào không hợp lệ" + ". Mã lỗi (" + returnCode + ")", "en_US");
                        return "unknow error : " + returnCode;
                    }
                }
            } //Ma loi cua kiem tra trang thai so SIM, ISDN
            else if (actionCode != null && actionCode.equals(CHECK_VALID_ACTION_CODE)) {

                if (returnCode.equals(WebServiceIMDAO.CVR_SUCCESS)) {
                    return "";
                } else {
                    if (returnCode.equals(WebServiceIMDAO.CVR_ERR_INVALID_PARAMETERS)) {
                        //return LabelUtil.getKey("Tham số truyền vào không đúng", "en_US");
                        return LabelUtil.getKey("e0112.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SALE_SERVICES_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tồn tại dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0197.im.response", "en_US");
                        //SIM
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_STOCK_TYPE_NOT_IN_SERVICES)) {
                        //return LabelUtil.getKey("Dịch vụ bán hàng không định nghĩa loại mặt hàng SIM", "en_US");
                        return LabelUtil.getKey("e0128.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_SERIAL_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy SIM trong hệ thống", "en_US");
                        return LabelUtil.getKey("e0198.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_STOCK_MODEL_NOT_IN_SERVICES)) {
                        //return LabelUtil.getKey("Dịch vụ bán hàng không định nghĩa mặt hàng SIM", "en_US");
                        return LabelUtil.getKey("e0130.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_STAFF_STOCK_NOT_IN_SERVICES)) {
                        //return LabelUtil.getKey("Dịch vụ bán hàng không cho phép lấy SIM từ kho nhân viên", "en_US");
                        return LabelUtil.getKey("e0131.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_SERIAL_NOT_IN_STAFF_STOCK)) {
                        //return LabelUtil.getKey("SIM không nằm trong kho nhân viên", "en_US");
                        return LabelUtil.getKey("e0199.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_SHOP_STOCK_NOT_IN_SERVICES)) {
                        //return LabelUtil.getKey("Dịch vụ bán hàng không cho phép lấy SIM từ kho cửa hàng", "en_US");
                        return LabelUtil.getKey("e0133.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_SERIAL_NOT_IN_SHOP_STOCK)) {
                        //return LabelUtil.getKey("SIM không nằm trong kho cửa hàng", "en_US");
                        return LabelUtil.getKey("e0200.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_SHOP_IS_AGENT)) {
                        //return LabelUtil.getKey("Cửa hàng giữ SIM là đại lý ", "en_US");
                        return LabelUtil.getKey("e0135.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_DETAIL_ERR_NOT_FOUND)) {
                        //return LabelUtil.getKey("(SIM)Không tìm thấy chi tiết lỗi", "en_US");
                        return LabelUtil.getKey("e0136.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_IN_LIST_EMPTY)) {
                        //return LabelUtil.getKey("Danh sách SIM gửi giao dịch sang bên Bán hàng bị rỗng (null)", "en_US");
                        return LabelUtil.getKey("e0137.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_INVALID_STOCK_TYPE)) {
                        //return LabelUtil.getKey("Mặt hàng SIM phải là SIM trắng trả trước (hoặc là SIM trắng trả sau)", "en_US");
                        return LabelUtil.getKey("e0138.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_INVALID_STOCK_MODEL)) {
                        //return LabelUtil.getKey("Mặt hàng SIM không chính xác", "en_US");
                        return LabelUtil.getKey("e0201.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy serial SIM trong dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0139.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_NOT_IN_STOCK)) {
                        //return LabelUtil.getKey("Serial SIM không ở trong kho", "en_US");
                        return LabelUtil.getKey("e0140.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_QUANTITY_DIFF_ONE)) {
                        //return LabelUtil.getKey("Có nhiều hơn 1 serial SIM trong kho", "en_US");
                        return LabelUtil.getKey("e0141.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SIM_SERIAL_QUANTITY_NOT_ENOUGH)) {
                        //return LabelUtil.getKey("Số lượng SIM trong kho không đủ", "en_US");
                        return LabelUtil.getKey("e0240.im.response", "en_US");
                        //ISDN
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_ISDN_STOCK_TYPE_NOT_IN_SERVICES)) {
                        //return LabelUtil.getKey("Dịch vụ bán hàng không định nghĩa loại mặt hàng ISDN", "en_US");
                        return LabelUtil.getKey("e0146.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_ISDN_SERIAL_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy số ISDN trong hệ thống", "en_US");
                        return LabelUtil.getKey("e0147.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_ISDN_STOCK_MODEL_NOT_IN_SERVICES)) {
                        //return LabelUtil.getKey("Dịch vụ bán hàng không định nghĩa mặt hàng ISDN", "en_US");
                        return LabelUtil.getKey("e0148.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_ISDN_STAFF_STOCK_NOT_IN_SERVICES)) {
                        //return LabelUtil.getKey("Dịch vụ bán hàng không cho phép lấy ISDN từ kho nhân viên", "en_US");
                        return LabelUtil.getKey("e0149.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_ISDN_SERIAL_NOT_IN_STAFF_STOCK)) {
                        //return LabelUtil.getKey("Số ISDN không thuộc kho nhân viên", "en_US");
                        return LabelUtil.getKey("e0150.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_ISDN_SHOP_STOCK_NOT_IN_SERVICES)) {
                        //return LabelUtil.getKey("Dịch vụ bán hàng không cho phép lấy ISDN từ kho cửa hàng (đại lý)", "en_US");
                        return LabelUtil.getKey("e0151.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_ISDN_SERIAL_NOT_IN_SHOP_STOCK)) {
                        //return LabelUtil.getKey("Số ISDN không nằm trong kho cửa hàng (đại lý)", "en_US");
                        return LabelUtil.getKey("e0152.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_ISDN_SHOP_IS_AGENT)) {
                        //return LabelUtil.getKey("Cửa hàng giữ ISDN là đại lý ", "en_US");
                        return LabelUtil.getKey("e0153.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_ISDN_DETAIL_ERR_NOT_FOUND)) {
                        //return LabelUtil.getKey("(ISDN)Không tìm thấy chi tiết lỗi", "en_US");
                        return LabelUtil.getKey("e0157.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_ISDN_IN_LIST_EMPTY)) {
                        //return LabelUtil.getKey("Danh sách ISDN gửi giao dịch sang bên Bán hàng bị rỗng (null)", "en_US");
                        return LabelUtil.getKey("e0158.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_ISDN_INVALID_STOCK_TYPE)) {
                        //return LabelUtil.getKey("Mặt hàng ISDN phải là ISDN mobile, homephone, hoặc pstn", "en_US");
                        return LabelUtil.getKey("e0159.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_ISDN_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy số ISDN trong dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0202.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_NOT_NEW_OR_SUSPEND)) {
                        //return LabelUtil.getKey("Số ISDN không ở trạng thái mới hoặc trạng thái ngưng sử dụng", "en_US");
                        return LabelUtil.getKey("e0160.im.response", "en_US");
                    } //                    else if (returnCode.equals(WebServiceIMDAO.SST_ERR_MODEL_BASIC_PRICE_OF_SERIAL_NOT_FOUND)) {
                    //                        return LabelUtil.getKey("Không tìm thấy giá giảm trừ hạn mức", "en_US");//Phund
                    //                    }
                    else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ISDN_SUSPEND_SPECIAL)) {
                        //return LabelUtil.getKey("Số ISDN là số đẹp đang ở trạng thái ngưng sử dụng", "en_US");
                        return LabelUtil.getKey("e0161.im.response", "en_US");
                        //SERIAL
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_STOCK_TYPE_NOT_IN_SERVICES)) {
                        //return LabelUtil.getKey("Dịch vụ bán hàng không định nghĩa loại mặt hàng Serial gắn kèm", "en_US");
                        return LabelUtil.getKey("e0168.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_SERIAL_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy số Serial gắn kèm trong hệ thống", "en_US");
                        return LabelUtil.getKey("e0169.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_STOCK_MODEL_NOT_IN_SERVICES)) {
                        //return LabelUtil.getKey("Dịch vụ bán hàng không định nghĩa mặt hàng Serial gắn kèm", "en_US");
                        return LabelUtil.getKey("e0170.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_STAFF_STOCK_NOT_IN_SERVICES)) {
                        //return LabelUtil.getKey("Dịch vụ bán hàng không cho phép lấy Serial gắn kèm từ kho nhân viên", "en_US");
                        return LabelUtil.getKey("e0171.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_SERIAL_NOT_IN_STAFF_STOCK)) {
                        //return LabelUtil.getKey("Số Serial gắn kèm không thuộc kho nhân viên", "en_US");
                        return LabelUtil.getKey("e0172.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_SHOP_STOCK_NOT_IN_SERVICES)) {
                        //return LabelUtil.getKey("Dịch vụ bán hàng không cho phép lấy Serial gắn kèm từ kho cửa hàng (đại lý)", "en_US");
                        return LabelUtil.getKey("e0173.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_SERIAL_NOT_IN_SHOP_STOCK)) {
                        //return LabelUtil.getKey("Số Serial gắn kèm không nằm trong kho cửa hàng (đại lý)", "en_US");
                        return LabelUtil.getKey("e0174.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_SHOP_IS_AGENT)) {
                        //return LabelUtil.getKey("Cửa hàng giữ Serial gắn kèm là đại lý ", "en_US");
                        return LabelUtil.getKey("e0175.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_DETAIL_ERR_NOT_FOUND)) {
                        //return LabelUtil.getKey("(Serial gắn kèm)Không tìm thấy chi tiết lỗi", "en_US");
                        return LabelUtil.getKey("e0176.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_IN_LIST_EMPTY)) {
                        //return LabelUtil.getKey("Danh sách Serial gắn kèm gửi giao dịch sang bên Bán hàng bị rỗng (null)", "en_US");
                        return LabelUtil.getKey("e0177.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_INVALID_STOCK_TYPE)) {
                        //return LabelUtil.getKey("Mặt hàng Serial gắn kèm không phải là mặt hàng handset, thẻ cào, kit", "en_US");
                        return LabelUtil.getKey("e0178.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy Serial gắn kèm trong dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0179.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_NOT_IN_STOCK)) {
                        //return LabelUtil.getKey("Serial gắn kèm không ở trong kho", "en_US");
                        return LabelUtil.getKey("e0180.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_QUANTITY_DIFF_ONE)) {
                        //return LabelUtil.getKey("Có nhiều hơn 1 serial gắn kèm trong kho", "en_US");
                        return LabelUtil.getKey("e0181.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_INVALID_STOCK_MODEL)) {
                        //return LabelUtil.getKey("Serial gắn kèm không khớp với mặt hàng được chọn", "en_US");
                        return LabelUtil.getKey("e0184.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_SERIAL_QUANTITY_NOT_ENOUGH)) {
                        //return LabelUtil.getKey("Số lượng Serial gắn kèm trong kho không đủ", "en_US");
                        return LabelUtil.getKey("e0203.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERVICES_NOT_ENOUGH_PARAM)) {
                        //return LabelUtil.getKey("Tham số truyền vào dịch vụ bán hàng không đủ", "en_US");
                        return LabelUtil.getKey("e0188.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_ISDN_INVALID_STOCK_MODEL)) {
                        return LabelUtil.getKey("e0241.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CVR_ERR_ISDN_IMSI_HLR_INVALID)) {
                        return LabelUtil.getKey("e0242.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_MODEL_PRICE_SALED_IS_NOT_FOUND)) {
                        //return "Tham số truyền vào dịch vụ bán hàng không đủ";
                        return LabelUtil.getKey("e0240.im.response", "en_US");
                    } else {
                        //return messageUnknowError + ". Mã lỗi (" + returnCode + ")", "en_US");
                        return "unknow error : " + returnCode;
                    }
                }
            } else if (actionCode != null && actionCode.equals(LOCK_RESOURCE_ACTION_CODE)) {
                if (returnCode.equals(WebServiceIMDAO.LR_SUCCESS)) {
                    return "";
                } else {
                    if (returnCode.equals(WebServiceIMDAO.LR_ERR_INVALID_PARAMETERS)) {
                        //return LabelUtil.getKey("Tham số truyền vào không đúng", "en_US");
                        return LabelUtil.getKey("e0112.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_ISDN_IN_LIST_EMPTY)) {
                        //return LabelUtil.getKey("Số ISDN bị rỗng", "en_US");
                        return LabelUtil.getKey("e0204.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_ISDN_INVALID_STOCK_TYPE)) {
                        //return LabelUtil.getKey("Loại mặt hàng(số) truyền vào không đúng", "en_US");
                        return LabelUtil.getKey("e0205.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_ISDN_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy số ISDN trong dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0202.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_ISDN_NOT_NEW_OR_SUSPEND)) {
                        //return LabelUtil.getKey("Số ISDN không ở trạng thái là số mới hoặc hủy", "en_US");
                        return LabelUtil.getKey("e0206.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ISDN_SUSPEND_SPECIAL)) {
                        //return LabelUtil.getKey("Số ISDN đang ở trạng thái ngưng sử dụng", "en_US");
                        return LabelUtil.getKey("e0207.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_SIM_IN_LIST_EMPTY)) {
                        //return LabelUtil.getKey("Số Serial bị rỗng", "en_US");
                        return LabelUtil.getKey("e0208.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_SIM_INVALID_STOCK_TYPE)) {
                        //return LabelUtil.getKey("Loại mặt hàng(sim) truyền vào không đúng", "en_US");
                        return LabelUtil.getKey("e0209.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_SIM_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy SIM trong dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0210.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_SIM_NOT_IN_STOCK)) {
                        //return LabelUtil.getKey("SIM không trong kho", "en_US");
                        return LabelUtil.getKey("e0211.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_SERIAL_IN_LIST_EMPTY)) {
                        //return LabelUtil.getKey("Serial gắn kèm mặt hàng bị rỗng", "en_US");
                        return LabelUtil.getKey("e0212.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_SERIAL_INVALID_STOCK_TYPE)) {
                        //return LabelUtil.getKey("Loại mặt hàng gắn kèm không phải là card, handset hoac kit", "en_US");
                        return LabelUtil.getKey("e0213.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_SERIAL_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy serial gắn kèm mặt hàng trong dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0214.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_SERIAL_NOT_IN_STOCK)) {
                        //return LabelUtil.getKey("Serial gắn kèm mặt hàng không ở trong kho", "en_US");
                        return LabelUtil.getKey("e0215.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_SALE_SERVICES_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0125.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_NUMBER_SIM_DIFF_ONE)) {
                        //return LabelUtil.getKey("Tồn tại nhiều hơn 1 serial trong hệ thống giống với số serial nhập vào", "en_US");
                        return LabelUtil.getKey("e0216.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_UPDATE_LIST_ISDN_FAIL)) {
                        //return LabelUtil.getKey("Không cập nhật được trạng thái danh sách ISDN", "en_US");
                        return LabelUtil.getKey("e0166.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_SERIAL_INVALID_STOCK_MODEL)) {
                        //return LabelUtil.getKey("Serial gắn kèm không khớp với mặt hàng được chọn", "en_US");
                        return LabelUtil.getKey("e0184.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_UPDATE_LIST_SERIAL_FAIL)) {
                        //return LabelUtil.getKey("Không cập nhật được trạng thái danh sách serial gắn kèm", "en_US");
                        return LabelUtil.getKey("e0185.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.LR_ERR_UPDATE_LIST_SIM_FAIL)) {
                        //return LabelUtil.getKey("Không cập nhật được trạng thái danh sách SIM", "en_US");
                        return LabelUtil.getKey("e0144.im.response", "en_US");
                    } else {
                        //return messageUnknowError + ". Mã lỗi (" + returnCode + ")", "en_US");
                        return "unknow error : " + returnCode;
                    }
                }
            } else if (actionCode != null && actionCode.equals(UNLOCK_RESOURCE_ACTION_CODE)) {
                if (returnCode.equals(WebServiceIMDAO.UR_SUCCESS)) {
                    return "";
                } else {
                    if (returnCode.equals(WebServiceIMDAO.UR_ERR_INVALID_PARAMETERS)) {
                        //return LabelUtil.getKey("Tham số truyền vào không chính xác", "en_US");
                        return LabelUtil.getKey("e0112.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_ISDN_IN_LIST_EMPTY)) {
                        //return LabelUtil.getKey("Số ISDN truyền vào rỗng", "en_US");
                        return LabelUtil.getKey("e0204.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_ISDN_INVALID_STOCK_TYPE)) {
                        //return LabelUtil.getKey("Loại mặt hàng(số) truyền vào không đúng", "en_US");
                        return LabelUtil.getKey("e0205.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_ISDN_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy số ISDN trong dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0202.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_ISDN_NOT_LOCKED)) {
                        //return LabelUtil.getKey("Số ISDN không ở trạng thái bị khóa", "en_US");
                        return LabelUtil.getKey("e0220.im.response", "en_US");
                        /*                    } else if (returnCode.equals(WebServiceIMDAO.UR_ISDN_SUSPEND_SPECIAL)) {
                         return LabelUtil.getKey("Số ISDN đang ở trạng thái ngưng sử dụng", "en_US");*/
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_SIM_IN_LIST_EMPTY)) {
                        //return LabelUtil.getKey("Số Serial bị rỗng", "en_US");
                        return LabelUtil.getKey("e0208.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_SIM_INVALID_STOCK_TYPE)) {
                        //return LabelUtil.getKey("Loại mặt hàng(sim) truyền vào không đúng", "en_US");
                        return LabelUtil.getKey("e0209.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_SIM_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy SIM trong dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0210.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_SIM_NOT_LOCKED)) {
                        //return LabelUtil.getKey("SIM không ở trạng thái bị khóa", "en_US");
                        return LabelUtil.getKey("e0217.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_SERIAL_IN_LIST_EMPTY)) {
                        //return LabelUtil.getKey("Serial gắn kèm mặt hàng bị rỗng", "en_US");
                        return LabelUtil.getKey("e0212.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_SERIAL_INVALID_STOCK_TYPE)) {
                        //return LabelUtil.getKey("Loại mặt hàng gắn kèm không phải là điện thoại, card, handset hoac kit", "en_US");
                        return LabelUtil.getKey("e0218.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_SERIAL_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy serial gắn kèm mặt hàng trong dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0214.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_SERIAL_NOT_LOCKED)) {
                        //return LabelUtil.getKey("Serial gắn kèm mặt hàng không ở trạng thái khóa", "en_US");
                        return LabelUtil.getKey("e0219.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_SALE_SERVICES_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0125.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_NUMBER_SIM_DIFF_ONE)) {
                        //return LabelUtil.getKey("Tồn tại nhiều hơn 1 serial trong hệ thống giống với số serial nhập vào", "en_US");
                        return LabelUtil.getKey("e0216.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_UPDATE_LIST_ISDN_FAIL)) {
                        //return LabelUtil.getKey("Không cập nhật được trạng thái danh sách ISDN", "en_US");
                        return LabelUtil.getKey("e0166.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_SERIAL_INVALID_STOCK_MODEL)) {
                        //return LabelUtil.getKey("Serial gắn kèm không khớp với mặt hàng được chọn", "en_US");
                        return LabelUtil.getKey("e0184.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_UPDATE_LIST_SERIAL_FAIL)) {
                        //return LabelUtil.getKey("Không cập nhật được trạng thái danh sách serial gắn kèm", "en_US");
                        return LabelUtil.getKey("e0185.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.UR_ERR_UPDATE_LIST_SIM_FAIL)) {
                        //return LabelUtil.getKey("Không cập nhật được trạng thái danh sách SIM", "en_US");
                        return LabelUtil.getKey("e0144.im.response", "en_US");
                    } else {
                        //return messageUnknowError + ". Mã lỗi (" + returnCode + ")", "en_US");
                        return "unknow error : " + returnCode;
                    }
                }

            } else if (actionCode != null && actionCode.equals(CANCEL_SALE_TRANS)) {
                if (returnCode.equals(WebServiceIMDAO.CST_SUCCESS)) {
                    return "";
                } else {
                    if (returnCode.equals(WebServiceIMDAO.CST_ERR_INVALID_PARAMETERS)) {
                        //return LabelUtil.getKey("Tham số truyền vào không chính xác", "en_US");
                        return LabelUtil.getKey("e0112.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CST_ERR_ST_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy giao dịch trong hệ thống quản lý bán hàng", "en_US");
                        return LabelUtil.getKey("e0221.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CST_ERR_ST_NOT_CREATED_BY_CM)) {
                        //return LabelUtil.getKey("Giao dịch không được tạo bởi hệ thống quản lý khách hàng", "en_US");
                        return LabelUtil.getKey("e0222.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CST_ERR_ST_STATUS)) {
                        //return LabelUtil.getKey("Giao dịch không ở trạng thái chưa lập hóa đơn", "en_US");
                        return LabelUtil.getKey("e0223.im.response", "en_US");
                    } //                    else if (returnCode.equals(WebServiceIMDAO.CST_ERR_MODEL_BASIC_PRICE_OF_SERIAL_NOT_FOUND)) {
                    //                        return LabelUtil.getKey("Không tìm thấy giá giảm trừ hạn mức", "en_US");//Phund
                    //                    }
                    else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ACCOUNT_BALANCE_NOT_EXIST)) {
                        //return LabelUtil.getKey("Đại lý chưa có tài khoản thanh toán", "en_US");
                        return LabelUtil.getKey("e0126.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ACCOUNT_BALANCE_NOT_ENOUGH)) {
                        //return LabelUtil.getKey("Số dư tài khoản của đại lý không đủ để thanh toán", "en_US");
                        return LabelUtil.getKey("e0127.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ACCOUNT_ANYPAY_CANNOT_ADD)) {
                        //return LabelUtil.getKey("Không cộng được tiền vào tài khoản AnyPay bên phân hệ Bán Hàng", "en_US");
                        return LabelUtil.getKey("e0187.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_MODEL_PRICE_SALED_IS_NOT_FOUND)) {
                        //return "Tham số truyền vào dịch vụ bán hàng không đủ";
                        return LabelUtil.getKey("e0240.im.response", "en_US");
                    } else {
                        //return messageUnknowError + ". Mã lỗi (" + returnCode + ")", "en_US");
                        return "unknow error : " + returnCode;
                    }
                }
            } else if (actionCode != null && actionCode.equals(RELEASE_ISDN_BY_CONTRACT_TERMINATED_ACTION_CODE)) {
                if (returnCode.equals(WebServiceIMDAO.RIBCT_SUCCESS)) {
                    return "";
                } else {
                    if (returnCode.equals(WebServiceIMDAO.RIBCT_ERR_INVALID_PARAMETERS)) {
                        //return LabelUtil.getKey("Tham số truyền vào không đúng", "en_US");
                        return LabelUtil.getKey("e0112.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.RIBCT_ERR_ISDN_INVALID_STOCK_TYPE)) {
                        //return LabelUtil.getKey("Loại mặt hàng không phải là Mobile, Homephone, hoặc Pstn", "en_US");
                        return LabelUtil.getKey("e0224.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.RIBCT_ERR_INVALID_TERMINATED_TYPE)) {
                        //return LabelUtil.getKey("Tham số xác định loại chấm dứt hợp đồng không chính xác", "en_US");
                        return LabelUtil.getKey("e0225.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.RIBCT_ERR_RUN_UPDATE_FAIL)) {
                        //return LabelUtil.getKey("Thực hiện cập nhật trạng thái ISDN bị lỗi", "en_US");
                        return LabelUtil.getKey("e0226.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.RIBCT_ERR_SHOP_NOT_EXIST)) {
                        //return LabelUtil.getKey("Không tồn tại cửa hàng", "en_US");
                        return LabelUtil.getKey("e0227.im.response", "en_US");
                    } else {
                        //return messageUnknowError + ". Mã lỗi (" + returnCode + ")", "en_US");
                        return "unknow error : " + returnCode;
                    }
                }
            } else if (actionCode != null && actionCode.equals(REPAIR_SALE_TRANS)) {
                if (returnCode != null && returnCode.longValue() > 0) {
                    return "";
                } else {
                    if (returnCode.equals(WebServiceIMDAO.CSTBSS_ERR_INVALID_PARAMETERS)) {
                        //return LabelUtil.getKey("Tham số truyền vào không đúng", "en_US");
                        return LabelUtil.getKey("e0112.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CSTBSS_ERR_ST_NOT_FOUND)) {
                        //return LabelUtil.getKey("Giao dịch không tồn tại", "en_US");
                        return LabelUtil.getKey("e0228.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CSTBSS_ERR_ST_NOT_CREATED_BY_CM)) {
                        //return LabelUtil.getKey("Giao dịch không được tạo bởi phân hệ quản lý khách hàng", "en_US");
                        return LabelUtil.getKey("e0222.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CSTBSS_ERR_ST_STATUS)) {
                        //return LabelUtil.getKey("Giao dịch không ở trạng thái chưa lập hóa đơn", "en_US");
                        return LabelUtil.getKey("e0223.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CSTBSS_ERR_STDL_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy chi tiết giao dịch", "en_US");
                        return LabelUtil.getKey("e0229.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CSTBSS_ERR_NEW_SALE_SERVICES_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0125.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CSTBSS_ERR_SALE_SERVICES_NOT_SIMILAR)) {
                        //return LabelUtil.getKey("Giao dịch cũ và giao dịch mới không tương thích", "en_US");
                        return LabelUtil.getKey("e0230.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CSTBSS_ERR_PRICE_POLICY_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy chính sách giá đối với dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0231.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.CSTBSS_ERR_SERVICES_PRICE_NOT_FOUND)) {
                        //return LabelUtil.getKey("Không tìm thấy giá đối với dịch vụ bán hàng", "en_US");
                        return LabelUtil.getKey("e0232.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ACCOUNT_BALANCE_NOT_EXIST)) {
                        //return LabelUtil.getKey("Đại lý chưa có tài khoản thanh toán", "en_US");
                        return LabelUtil.getKey("e0126.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ACCOUNT_BALANCE_NOT_ENOUGH)) {
                        //return LabelUtil.getKey("Số dư tài khoản của đại lý không đủ để thanh toán", "en_US");
                        return LabelUtil.getKey("e0127.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_ACCOUNT_ANYPAY_CANNOT_ADD)) {
                        //return LabelUtil.getKey("Không cộng được tiền vào tài khoản AnyPay bên phân hệ Bán Hàng", "en_US");
                        return LabelUtil.getKey("e0187.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_SERVICES_NOT_ENOUGH_PARAM)) {
                        //return LabelUtil.getKey("Tham số truyền vào dịch vụ bán hàng không đủ", "en_US");
                        return LabelUtil.getKey("e0188.im.response", "en_US");
                    } else if (returnCode.equals(WebServiceIMDAO.SST_ERR_MODEL_PRICE_SALED_IS_NOT_FOUND)) {
                        //return "Tham số truyền vào dịch vụ bán hàng không đủ";
                        return LabelUtil.getKey("e0240.im.response", "en_US");
                    } else {
                        //return messageUnknowError + ". Mã lỗi (" + returnCode + ")", "en_US");
                        return "unknow error : " + returnCode;
                    }
                }
            } else if (actionCode != null && actionCode.equals(UNLOCK_IPHONE_ACTION_CODE)) {
                if (returnCode.equals(WebServiceIMDAO.CHECK_SERIAL_IP_OK)) {
                    return "";
                } else if (returnCode.equals(WebServiceIMDAO.CHECK_SERIAL_IP_INPUT_NOT_VALID)) {
                    //return "Thông tin đầu vào không hợp lệ", "en_US");
                    return LabelUtil.getKey("e0233.im.response", "en_US");
                } else if (returnCode.equals(WebServiceIMDAO.CHECK_SERIAL_IP_NOT_FOUND)) {
                    //return "Không tìm thấy imei trong kho", "en_US");
                    return LabelUtil.getKey("e0234.im.response", "en_US");
                } else if (returnCode.equals(WebServiceIMDAO.CHECK_SERIAL_IP_MORE_RESULT)) {
                    //return "Có nhiều hơn một imei trong kho", "en_US");
                    return LabelUtil.getKey("e0235.im.response", "en_US");
                } else if (returnCode.equals(WebServiceIMDAO.CHECK_SERIAL_IP_NOT_IN_BH)) {
                    //return "Dịch vụ unlock bảo hành chỉ được lấy imei trong kho bảo hành", "en_US");
                    return LabelUtil.getKey("e0236.im.response", "en_US");
                } else if (returnCode.equals(WebServiceIMDAO.CHECK_SERIAL_IP_NORMAL_IN_BH)) {
                    //return "Dịch vụ unlock thường không cho phép lấy imei trong kho bảo hành", "en_US");
                    return LabelUtil.getKey("e0237.im.response", "en_US");
                } else {
                    //return messageUnknowError + ". Mã lỗi (" + returnCode + ")", "en_US");
                    return "unknow error : " + returnCode;
                }
            } else if (actionCode != null && actionCode.equals(UPDATE_ISDN_TO_USING_ACTION_CODE)) {
                if (returnCode.equals(WebServiceIMDAO.CIS_SUCCESS)) {
                    return "";
                } else if (returnCode.equals(WebServiceIMDAO.CIS_ERR_PARAM_NOT_ENOUGH)) {
                    //return "Thông tin đầu vào không hợp lệ", "en_US");
                    return LabelUtil.getKey("e0233.im.response", "en_US");
                } else if (returnCode.equals(WebServiceIMDAO.CIS_ERR_ISDN_NOT_FOUND)) {
                    //return LabelUtil.getKey("Không tìm thấy số ISDN", "en_US");
                    return LabelUtil.getKey("e0147.im.response", "en_US");
                } else if (returnCode.equals(WebServiceIMDAO.CIS_ERR_ISDN_STATUS_INVALID)) {
                    //return LabelUtil.getKey("Trạng thái số ISDN truyền vào không hợp lệ (trạng thái phải là mới hoặc ngưng sử dụng)", "en_US");
                    return LabelUtil.getKey("e0238.im.response", "en_US");
                } else {
                    //return messageUnknowError + ". Mã lỗi (" + returnCode + ")", "en_US");
                    return "unknow error : " + returnCode;
                }
            }
        } else {
            //return LabelUtil.getKey("Không xác định được trạng thái lỗi.", "en_US");
            return LabelUtil.getKey("e0239.im.response", "en_US");
        }
        return "";
    }

    public String executeSaleTrans(Session cmPreSession, Session imSession,
            SubMbPre subscriber, Long objectType,
            List lstStockIsdn, List lstStockSim, List lstStockSerial,
            Long reasonId, String actionCode, Date nowDate, Long staffId, Long shopId) throws Exception {
        String errorMessage = null;
        String productId = "";
        String saleServiceCode = null;
        Long telServiceId = null;
        SubMbPre transSubscriber = null;
        long current_call_id = 0;
        SaleTransPre saleTrans = null;
        com.viettel.im.database.BO.SaleTrans saleTransIm = null;

        WebServiceIMDAO webIMDAO = new WebServiceIMDAO(imSession);

        //Neu giao dich lien quan den hop dong
        if (Constants.TRANS_OF_SUBSCRIBER.equals(objectType)) {
            Long lCustId = 0L;

            if (subscriber == null) {
                throw new Exception("Chua nhap vao thong tin thue bao");
            }

            //Tao ra 1 bean moi, chua dung thong tin giong thong tin dua vao
            transSubscriber = (SubMbPre) BeanUtils.cloneBean(subscriber);
        } else {
            throw new Exception("Loai giao dich dua vao khong chinh xac");
        }

        //Neu co thue bao, giao dich lien quan den thue bao
        if (transSubscriber != null) {

            productId = transSubscriber.getProductCode();

            telServiceId = Constants.SERVICE_MOBILE_ID_WEBSERVICE;
        }

        saleServiceCode = new com.viettel.bccs.cm.dao.MappingDAO().getSaleServiceCodePre(cmPreSession, telServiceId, reasonId, productId, actionCode);

        if (saleServiceCode != null && !"".equals(saleServiceCode)) {
            saleTrans = getSaleTrans(transSubscriber, saleServiceCode, nowDate, staffId, shopId);
            saleTransIm = new com.viettel.im.database.BO.SaleTrans();

            ///BeanUtils.copyProperties(saleTransIm, saleTrans);
            saleTransIm.setSubId(saleTrans.getSubId());
            saleTransIm.setIsdn(saleTrans.getIsdn());
            saleTransIm.setTelecomServiceId(saleTrans.getTelecomServiceId());
            saleTransIm.setShopId(saleTrans.getShopId());
            saleTransIm.setStaffId(saleTrans.getStaffId());
            saleTransIm.setSaleTransDate(saleTrans.getSaleTransDate());
            saleTransIm.setStatus(saleTrans.getStatus().toString());
            saleTransIm.setListStockIsdnBean(lstStockIsdn);
            saleTransIm.setListStockSimBean(lstStockSim);
            saleTransIm.setListStockSerialBean(lstStockSerial);
            /*Truyền user tạo giao dịch sang IM*/
            saleTransIm.setCreateStaffId(staffId);

            Gson gson = new Gson();
//            if (log.isInfoEnabled()) {
            current_call_id = CALL_ID++;
            StringBuilder paramIn = new StringBuilder();
            paramIn.append("method = [executeSaleTrans] ");
            paramIn.append("SaleTrans = [").append(gson.toJson(saleTransIm)).append("]");
            paramIn.append(",SaleServiceCode = [").append(saleServiceCode).append("]");
            logger.info(current_call_id + " START SaveSaleTrans " + paramIn.toString());
//            }

            Long result = webIMDAO.saveSaleTrans(saleTransIm, saleServiceCode);
//             if (log.isInfoEnabled()) {
            StringBuilder paramOut = new StringBuilder();
            paramOut.append("Method = [executeSaleTrans]");
            paramOut.append(",Result = [").append(result).append("]");
            paramOut.append(",Message = [").append(errorMessage).append("]");
            logger.info(current_call_id + " FINISH " + paramOut.toString() + ", START SaveSaleTrans CM ");
//            }

            if (WebServiceIMDAO.SST_ERR_INVALID_LIST_ISDN.equals(result)) {
                if (lstStockIsdn != null && lstStockIsdn.size() > 0) {
                    for (StockIsdnBean stockIsdn : (List<StockIsdnBean>) lstStockIsdn) {
                        errorMessage = getReturnMessage(EXE_SALE_TRANS_ACTION_CODE, stockIsdn.getMessageCode());

                        if (!"".equals(errorMessage)) {
                            return errorMessage;
                        }
                    }
                }
            } else if (WebServiceIMDAO.SST_ERR_INVALID_LIST_SIM.equals(result)) {
                if (lstStockSim != null && lstStockSim.size() > 0) {
                    for (StockSimBean stockSim : (List<StockSimBean>) lstStockSim) {
                        errorMessage = getReturnMessage(EXE_SALE_TRANS_ACTION_CODE, stockSim.getMessageCode());

                        if (!"".equals(errorMessage)) {
                            return errorMessage;
                        }
                    }
                }
            } else if (WebServiceIMDAO.SST_ERR_INVALID_LIST_SERIAL.equals(result)) {
                if (lstStockSerial != null && lstStockSerial.size() > 0) {
                    for (StockSerialBean stockSerial : (List<StockSerialBean>) lstStockSerial) {
                        errorMessage = getReturnMessage(EXE_SALE_TRANS_ACTION_CODE, stockSerial.getMessageCode());

                        if (!"".equals(errorMessage)) {
                            return errorMessage;
                        }
                    }
                }
            } else {
                errorMessage = getReturnMessage(EXE_SALE_TRANS_ACTION_CODE, result);
            }
            cmPreSession.flush();
            //Thuc hien luu lai ma giao dich, va cac thong tin giao dich gui sang ben Sale
            if (saleTransIm.getSaleTransId() != null && saleTransIm.getSaleTransId() > 0) {
                saleTrans.setSaleTransId(saleTransIm.getSaleTransId());
                saleTrans.setNote(errorMessage);
                saleTrans.setTransResult(result);
                saleTrans.setActionCode(actionCode);
                cmPreSession.save(saleTrans);

                //Luu thong tin serial cua mat hang ban kem
                if (Constants.REASON_TYPE_REG_TYPE_NEW.equals(actionCode)) {
                    List<Long> lstSaleTransId = new ArrayList();
                    lstSaleTransId.add(saleTransIm.getSaleTransId());
                    //Thuc hien save vao bang SubStockModelRel khi giao dich thuc hien xong, tao ra object moi va luu saleTransId vao
                }
            } else {
                cmPreSession.evict(saleTrans);
            }
            cmPreSession.flush();
            return errorMessage;
        } else {
            if (new ApDomainDAO().isMandatoryTrasaction(cmPreSession.connection(), actionCode)) {
                return LabelUtil.getKey("e0763.reason.not.map.ser.sale", "en_US");
            }
        }

        return "";
    }

    private SaleTransPre getSaleTrans(SubMbPre subscriber, String saleServiceCode, Date issDateTime, Long staffId, Long shopId) throws Exception {

        String isdn = null;
        String serial = null;

        SaleTransPre saleTrans = new SaleTransPre();
//        Date issDateTime = Common.getDateTimeFromDB(cmPreSession); // dung datetime truyen vao tu ben ngoai

        //Thong tin lien quan den thue bao
        if (subscriber != null) {
            saleTrans.setSubId(subscriber.getSubId());

            isdn = subscriber.getIsdn();
            serial = subscriber.getSerial();
            saleTrans.setIsdn(isdn);
            saleTrans.setSerial(serial);
            saleTrans.setTelecomServiceId(Constants.SERVICE_MOBILE_ID_WEBSERVICE);

        }
        //Thuc hien lay shop tu bang tra sau

        saleTrans.setShopId(shopId);
        saleTrans.setStaffId(staffId);
        saleTrans.setSaleTransDate(issDateTime);
        saleTrans.setStatus(1L);

        return saleTrans;
    }

    public static StockSim getSimPrePaidByImsiOrSerial(Session imSession, String imsi, String serial) throws Exception {
        WebServiceIMDAO webIMDAO = new WebServiceIMDAO(imSession);
        StockSim stkSim = webIMDAO.findSimPrePaidByImsiOrSerial(imsi, serial);
        return stkSim;
    }
}
