/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.dao.payment;

import com.viettel.bccs.cm.model.payment.SubscriberExtend;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * SubscriberDAO
 *
 * @author phuonghc
 */
public class SubscriberDAO {

    private static Logger logger = Logger.getLogger(SubscriberDAO.class);

    public List<SubscriberExtend> findByContractIdAll(Session sessionPayment, String contractId) {
        List<SubscriberExtend> result = new ArrayList<SubscriberExtend>();
        String sql = "select sub.user_using userUsing, sub.address deployAddress, con.account_number account, "
                + " cus.x_location custLat, cus.y_location custLong "
                + " from subscriber sub inner join contract con on sub.sub_id = con.sub_id "
                + " inner join cm_pos2.customer cus on cus.cust_id = con.cust_id "
                + " where sub.contract_id =:contractId";
        try {
            Query query = sessionPayment.createSQLQuery(sql)
                    .addScalar("userUsing", Hibernate.STRING)
                    .addScalar("deployAddress", Hibernate.STRING)
                    .addScalar("account", Hibernate.STRING)
                    .addScalar("custLat", Hibernate.STRING)
                    .addScalar("custLong", Hibernate.STRING)
                    .setResultTransformer(Transformers.aliasToBean(SubscriberExtend.class));
            query.setParameter("contractId", contractId);
            result = query.list();
            if (result == null || result.isEmpty()) {
                return new ArrayList<SubscriberExtend>();
            }
        } catch (HibernateException he) {
            logger.error("### An error occured while doing findByContractIdAll", he);
        }
        return result;
    }
}
