/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

/**
 *
 * @author partner1
 */
public class AccountFromStaffCode {
    private String msisdn;
    private Long staffType;

    public AccountFromStaffCode() {
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Long getStaffType() {
        return staffType;
    }

    public void setStaffType(Long staffType) {
        this.staffType = staffType;
    }
    
    
}
