/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Transient;

/**
 *
 * @author nhandv
 */
public class AccountInfra implements Serializable {
    @Transient
    private String account;
    @Transient
    private String infraType;
    @Transient
    private String stationCode;
    @Transient
    private String connectorCode;
    @Transient
    private String cLat;
    @Transient
    private String cLong;
    @Transient
    private String cLatStation;
    @Transient
    private String cLongStation;
    @Transient
    private String deviceCode;
    @Transient
    private String port;
    
    public AccountInfra() {
    }
    
    public AccountInfra(String Account, String InfraType, String StationCode, String ConnectorCode) {
        this.account = Account;
        this.infraType = InfraType;
        this.stationCode = StationCode;
        this.connectorCode = ConnectorCode;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getInfraType() {
        return infraType;
    }

    public void setInfraType(String infraType) {
        this.infraType = infraType;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getConnectorCode() {
        return connectorCode;
    }

    public void setConnectorCode(String connectorCode) {
        this.connectorCode = connectorCode;
    }

    public String getcLat() {
        return cLat;
    }

    public void setcLat(String cLat) {
        this.cLat = cLat;
    }

    public String getcLong() {
        return cLong;
    }

    public void setcLong(String cLong) {
        this.cLong = cLong;
    }

    public String getcLatStation() {
        return cLatStation;
    }

    public void setcLatStation(String cLatStation) {
        this.cLatStation = cLatStation;
    }

    public String getcLongStation() {
        return cLongStation;
    }

    public void setcLongStation(String cLongStation) {
        this.cLongStation = cLongStation;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
    
}
