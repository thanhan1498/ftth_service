package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ACTION")
public class Action implements Serializable {

    @Id
    @Column(name = "ACTION_ID", length = 10, precision = 10, scale = 0)
    private Long actionId;
    @Column(name = "ACTION_NAME", length = 180)
    private String actionName;
    @Column(name = "ACTION_MAPING", length = 180)
    private String actionMaping;
    @Column(name = "CLASS_NAME", length = 50)
    private String className;
    @Column(name = "METHOD_NAME", length = 50)
    private String methodName;
    @Column(name = "LAST_UPDATE_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdateTime;
    @Column(name = "LAST_UPDATE_USER", length = 50)
    private String lastUpdateUser;
    @Column(name = "STATUS", length = 2, precision = 2, scale = 0)
    private Long status;
    @Column(name = "ACTION_TYPE", length = 1)
    private String actionType;
    @Column(name = "ACTION_CODE", length = 10)
    private String actionCode;

    public Long getActionId() {
        return actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionMaping() {
        return actionMaping;
    }

    public void setActionMaping(String actionMaping) {
        this.actionMaping = actionMaping;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }
}
