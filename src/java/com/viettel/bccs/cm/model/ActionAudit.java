package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ACTION_AUDIT")
public class ActionAudit implements Serializable {

    @Id
    @Column(name = "ACTION_AUDIT_ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long actionAuditId;
    @Column(name = "ISSUE_DATETIME", length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    private Date issueDatetime;
    @Column(name = "ACTION_CODE", length = 10)
    private String actionCode;
    @Column(name = "REASON_ID", length = 10)
    private Long reasonId;
    @Column(name = "SHOP_CODE", length = 20)
    private String shopCode;
    @Column(name = "USER_NAME", length = 50, nullable = false)
    private String userName;
    @Column(name = "PK_TYPE", length = 1, nullable = false)
    private String pkType;
    @Column(name = "PK_ID", length = 20)
    private Long pkId;
    @Column(name = "IP", length = 20)
    private String ip;
    @Column(name = "DESCRIPTION", length = 500)
    private String description;
    @Column(name = "VALID", length = 1, precision = 1, scale = 0)
    private Long valid;

    public ActionAudit() {
    }

    public ActionAudit(Long actionAuditId, Date issueDatetime, String actionCode, Long reasonId, String shopCode, String userName, String pkType, Long pkId, String ip, String description, Long valid) {
        this.actionAuditId = actionAuditId;
        this.issueDatetime = issueDatetime;
        this.actionCode = actionCode;
        this.reasonId = reasonId;
        this.shopCode = shopCode;
        this.userName = userName;
        this.pkType = pkType;
        this.pkId = pkId;
        this.ip = ip;
        this.description = description;
        this.valid = valid;
    }

    public Long getActionAuditId() {
        return actionAuditId;
    }

    public void setActionAuditId(Long actionAuditId) {
        this.actionAuditId = actionAuditId;
    }

    public Date getIssueDatetime() {
        return issueDatetime;
    }

    public void setIssueDatetime(Date issueDatetime) {
        this.issueDatetime = issueDatetime;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPkType() {
        return pkType;
    }

    public void setPkType(String pkType) {
        this.pkType = pkType;
    }

    public Long getPkId() {
        return pkId;
    }

    public void setPkId(Long pkId) {
        this.pkId = pkId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getValid() {
        return valid;
    }

    public void setValid(Long valid) {
        this.valid = valid;
    }
}
