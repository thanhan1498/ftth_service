package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ACTION_DETAIL")
public class ActionDetail implements Serializable {

    @Id
    @Column(name = "ACTION_DETAIL_ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long actionDetailId;
    @Column(name = "ACTION_AUDIT_ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long actionAuditId;
    @Column(name = "TABLE_NAME", nullable = false, length = 20)
    private String tableName;
    @Column(name = "ROW_ID", length = 10, precision = 10, scale = 0)
    private Long rowId;
    @Column(name = "COL_NAME", nullable = false, length = 20)
    private String colName;
    @Column(name = "OLD_VALUE", length = 200)
    private String oldValue;
    @Column(name = "NEW_VALUE", length = 200)
    private String newValue;
    @Column(name = "ISSUE_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date issueDateTime;

    public ActionDetail() {
    }

    public ActionDetail(Long actionDetailId, Long actionAuditId, String tableName, Long rowId, String colName, String oldValue, String newValue, Date issueDateTime) {
        this.actionDetailId = actionDetailId;
        this.actionAuditId = actionAuditId;
        this.tableName = tableName;
        this.rowId = rowId;
        this.colName = colName;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.issueDateTime = issueDateTime;
    }

    public Long getActionDetailId() {
        return actionDetailId;
    }

    public void setActionDetailId(Long actionDetailId) {
        this.actionDetailId = actionDetailId;
    }

    public Long getActionAuditId() {
        return actionAuditId;
    }

    public void setActionAuditId(Long actionAuditId) {
        this.actionAuditId = actionAuditId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Long getRowId() {
        return rowId;
    }

    public void setRowId(Long rowId) {
        this.rowId = rowId;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public Date getIssueDateTime() {
        return issueDateTime;
    }

    public void setIssueDateTime(Date issueDateTime) {
        this.issueDateTime = issueDateTime;
    }
}
