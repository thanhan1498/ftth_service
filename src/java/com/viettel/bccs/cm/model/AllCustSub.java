/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author vietnn6
 */

@Entity
@Table(name = "ALL_CUST_SUB",schema="CM_PRE2")

public class AllCustSub implements Serializable{

    @Id
    @Column(name = "SUB_ID", length = 11, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "ISDN", length = 10, precision = 10, scale = 0)
    private String isdn;
    @Column(name = "CUST_ID", length = 10, precision = 10, scale = 0)
    private Long custId;
    @Column(name = "OFFER_ID", length = 10, precision = 10, scale = 0)
    private Long offerId;
    @Column(name = "PRODUCT_ID", length = 10, precision = 10, scale = 0)
    private String productId;
    @Column(name = "STATUS", length = 1, precision = 10, scale = 0)
    private String status;
    @Column(name = "ACT_STATUS", length = 10, precision = 10, scale = 0)
    private String actStatus;
    @Column(name = "STATUS_ID", length = 10, precision = 10, scale = 0)
    private Long statusId;
    @Column(name = "SERVICE", length = 10, precision = 10, scale = 0)
    private String service;
    @Column(name = "SERVICE_TYPE", length = 10, precision = 10, scale = 0)
    private String serviceType;
    @Column(name = "LAST_NUMBER", length = 10, precision = 10, scale = 0)
    private String lastNumber;

    // Constructors
    public AllCustSub() {
    }

    public String getLastNumber() {
        return lastNumber;
    }

    public void setLastNumber(String lastNumber) {
        this.lastNumber = lastNumber;
    }

    // Property accessors
    public Long getSubId() {
        return this.subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getIsdn() {
        return this.isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Long getCustId() {
        return this.custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public Long getOfferId() {
        return this.offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getActStatus() {
        return this.actStatus;
    }

    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    public Long getStatusId() {
        return this.statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getService() {
        return this.service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getServiceType() {
        return this.serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

}
