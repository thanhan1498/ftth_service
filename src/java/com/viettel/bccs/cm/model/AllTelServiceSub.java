package com.viettel.bccs.cm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "ALL_TEL_SERVICE_SUB")
public class AllTelServiceSub implements java.io.Serializable {

    @Transient
    private Long stt;
    @Column(name = "CONTRACT_ID")
    private Long contractId;
    @Column(name = "OLD_CONTRACT_ID")
    private Long oldContractId;
    @Id
    @Column(name = "SUB_ID")
    private Long subId;
    @Column(name = "ISDN")
    private String isdn;
    @Column(name = "ACCOUNT")
    private String account;
    @Column(name = "ACT_STATUS")
    private String actStatus;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "STATUS_ID")
    private Long statusId;
    @Column(name = "SERVICE")
    private String service;
    @Column(name = "SERVICE_TYPE")
    private String serviceType;
    @Transient
    private String actStatusBits;
    @Transient
    private Long adslSubId;
    @Transient
    private String productCode;

    public Long getAdslSubId() {
        return adslSubId;
    }

    public void setAdslSubId(Long adslSubId) {
        this.adslSubId = adslSubId;
    }

    public String getActStatusBits() {
        return actStatusBits;
    }

    public void setActStatusBits(String actStatusBits) {
        this.actStatusBits = actStatusBits;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Long getOldContractId() {
        return oldContractId;
    }

    public void setOldContractId(Long oldContractId) {
        this.oldContractId = oldContractId;
    }

    public Long getStt() {
        return stt;
    }

    public void setStt(Long stt) {
        this.stt = stt;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getActStatus() {
        return actStatus;
    }

    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
}
