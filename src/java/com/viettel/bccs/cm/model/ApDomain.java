package com.viettel.bccs.cm.model;

import com.viettel.bccs.cm.model.pk.ApDomainId;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "AP_DOMAIN")
@IdClass(ApDomainId.class)
public class ApDomain implements Serializable {

    @Id
    private String type;
    @Id
    private String code;
    @Column(name = "NAME", length = 100, nullable = false)
    private String name;
    @Column(name = "VALUE", length = 200)
    private String value;
    @Column(name = "STATUS", length = 1)
    private Long status;
    @Column(name = "REASON_TYPE", length = 10)
    private String reasonType;
    @Column(name = "GROUP_CODE", length = 10)
    private String groupCode;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getReasonType() {
        return reasonType;
    }

    public void setReasonType(String reasonType) {
        this.reasonType = reasonType;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }
}
