/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.util.List;

/**
 * ApParamCustom
 *
 * @author phuonghc
 */
public class ApParamCustom {

    private String code;
    private List<String> value;
    private String serviceUsed;

    public ApParamCustom() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }

    public String getServiceUsed() {
        return serviceUsed;
    }

    public void setServiceUsed(String serviceUsed) {
        this.serviceUsed = serviceUsed;
    }
}
