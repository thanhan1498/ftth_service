/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.util.List;

/**
 *
 * @author cuongdm
 */
public class ApParamExtend extends ApParam{
    private String desciption;
    private List<ApParam> paramSub;

    /**
     * @return the desciption
     */
    public String getDesciption() {
        return desciption;
    }

    /**
     * @param desciption the desciption to set
     */
    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    /**
     * @return the paramSub
     */
    public List<ApParam> getParamSub() {
        return paramSub;
    }

    /**
     * @param paramSub the paramSub to set
     */
    public void setParamSub(List<ApParam> paramSub) {
        this.paramSub = paramSub;
    }
}
