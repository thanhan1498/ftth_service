package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "AREA")
public class Area implements Serializable {

    @Id
    @Column(name = "AREA_CODE", length = 15)
    private String areaCode;
    @Column(name = "TYPE", length = 3)
    private String type;
    @Column(name = "PARENT_CODE", length = 15)
    private String parentCode;
    @Column(name = "CEN_CODE", length = 1)
    private String cenCode;
    @Column(name = "PROVINCE", length = 4)
    private String province;
    @Column(name = "DISTRICT", length = 3)
    private String district;
    @Column(name = "PRECINCT", length = 4)
    private String precinct;
    @Column(name = "STREET_BLOCK", length = 5)
    private String streetBlock;
    @Column(name = "STREET", length = 5)
    private String street;
    @Column(name = "NAME", length = 50)
    private String name;
    @Column(name = "FULL_NAME", length = 300)
    private String fullName;
    @Column(name = "ORDER_NO", nullable = false, length = 22, precision = 22, scale = 0)
    private Long orderNo;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "PSTN_CODE", length = 5)
    private String pstnCode;

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getCenCode() {
        return cenCode;
    }

    public void setCenCode(String cenCode) {
        this.cenCode = cenCode;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public String getStreetBlock() {
        return streetBlock;
    }

    public void setStreetBlock(String streetBlock) {
        this.streetBlock = streetBlock;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Long getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Long orderNo) {
        this.orderNo = orderNo;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getPstnCode() {
        return pstnCode;
    }

    public void setPstnCode(String pstnCode) {
        this.pstnCode = pstnCode;
    }
}
