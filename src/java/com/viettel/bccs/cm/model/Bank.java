package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BANK")
public class Bank implements Serializable {

    @Id
    @Column(name = "BANK_CODE", length = 10)
    private String bankCode;
    @Column(name = "NAME", length = 100, nullable = false)
    private String name;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0, nullable = false)
    private Long status;
    @Column(name = "ADDRESS", length = 100, nullable = false)
    private String address;
    @Column(name = "CONTACT_NAME", length = 50)
    private String contactName;
    @Column(name = "CONTACT_TITLE", length = 30)
    private String contactTitle;
    @Column(name = "TEL_NUMBER", length = 20)
    private String telNumber;
    @Column(name = "FAX", length = 20)
    private String fax;
    @Column(name = "EMAIL", length = 30)
    private String email;
    @Column(name = "DESCRIPTION", length = 100)
    private String description;
    @Column(name = "PROVINCE", length = 20)
    private String province;
    @Column(name = "OLD_DISTRICT", length = 2)
    private String oldDistrict;
    @Column(name = "BANK_TYPE", length = 6)
    private String bankType;
    @Column(name = "AMT_BANK_ID", length = 10)
    private Long amtBankId;

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactTitle() {
        return contactTitle;
    }

    public void setContactTitle(String contactTitle) {
        this.contactTitle = contactTitle;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getOldDistrict() {
        return oldDistrict;
    }

    public void setOldDistrict(String oldDistrict) {
        this.oldDistrict = oldDistrict;
    }

    public String getBankType() {
        return bankType;
    }

    public void setBankType(String bankType) {
        this.bankType = bankType;
    }

    public Long getAmtBankId() {
        return amtBankId;
    }

    public void setAmtBankId(Long amtBankId) {
        this.amtBankId = amtBankId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
