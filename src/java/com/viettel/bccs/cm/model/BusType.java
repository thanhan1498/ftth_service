package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BUS_TYPE")
public class BusType implements Serializable {

    @Id
    @Column(name = "BUS_TYPE", length = 3, nullable = false)
    private String busType;
    @Column(name = "NAME", length = 50, nullable = false)
    private String name;
    @Column(name = "CUST_TYPE", length = 1, nullable = false)
    private String custType;
    @Column(name = "TAX", length = 10, precision = 10, scale = 0, nullable = false)
    private Long tax;
    @Column(name = "EXPIRE_REQUIRED", length = 1, nullable = false)
    private String expireRequired;
    @Column(name = "ID_REQUIRED", length = 1, nullable = false)
    private String idRequired;
    @Column(name = "POP_REQUIRED", length = 1, nullable = false)
    private String popRequired;
    @Column(name = "PERMIT_REQUIRED", length = 1, nullable = false)
    private String permitRequired;
    @Column(name = "TIN_REQUIRED", length = 1, nullable = false)
    private String tinRequired;
    @Column(name = "SEX_REQUIRED", length = 1, nullable = false)
    private String sexRequired;
    @Column(name = "NATIONALITY_REQUIRED", length = 1, nullable = false)
    private String nationalityRequired;
    @Column(name = "STATUS", length = 2)
    private Long status;
    @Column(name = "TEL_SERVICE")
    private String telService;

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public Long getTax() {
        return tax;
    }

    public void setTax(Long tax) {
        this.tax = tax;
    }

    public String getExpireRequired() {
        return expireRequired;
    }

    public void setExpireRequired(String expireRequired) {
        this.expireRequired = expireRequired;
    }

    public String getIdRequired() {
        return idRequired;
    }

    public void setIdRequired(String idRequired) {
        this.idRequired = idRequired;
    }

    public String getPopRequired() {
        return popRequired;
    }

    public void setPopRequired(String popRequired) {
        this.popRequired = popRequired;
    }

    public String getPermitRequired() {
        return permitRequired;
    }

    public void setPermitRequired(String permitRequired) {
        this.permitRequired = permitRequired;
    }

    public String getTinRequired() {
        return tinRequired;
    }

    public void setTinRequired(String tinRequired) {
        this.tinRequired = tinRequired;
    }

    public String getSexRequired() {
        return sexRequired;
    }

    public void setSexRequired(String sexRequired) {
        this.sexRequired = sexRequired;
    }

    public String getNationalityRequired() {
        return nationalityRequired;
    }

    public void setNationalityRequired(String nationalityRequired) {
        this.nationalityRequired = nationalityRequired;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getTelService() {
        return telService;
    }

    public void setTelService(String telService) {
        this.telService = telService;
    }
}
