package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BUS_TYPE_ID_REQUIRE")
public class BusTypeIdRequire implements Serializable {

    @Id
    @Column(name = "ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long id;
    @Column(name = "BUS_TYPE", length = 5)
    private String busType;
    @Column(name = "ID_TYPE", length = 10)
    private Long idType;
    @Column(name = "STATUS")
    private Long status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public Long getIdType() {
        return idType;
    }

    public void setIdType(Long idType) {
        this.idType = idType;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
