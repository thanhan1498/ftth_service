/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "CHANGE_SERVICE")
public class ChangeService implements Serializable {

    @Id
    @Column(name = "ID", length = 10, precision = 10, scale = 0)
    private Long id;
    @Column(name = "OLD_ACCOUNT", length = 30)
    private String accountAdsl;
    @Column(name = "NEW_ACCOUNT", length = 30)
    private String accountFtth;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "CREATE_DATE")
    private Date createDate;
    @Column(name = "UPDATE_DATE")
    private Date updateDate;
    @Column(name = "USER_CREATE", length = 50)
    private String userCreate;
    @Column(name = "DESCRIPTION", length = 4000)
    private String description;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the accountAdsl
     */
    public String getAccountAdsl() {
        return accountAdsl;
    }

    /**
     * @param accountAdsl the accountAdsl to set
     */
    public void setAccountAdsl(String accountAdsl) {
        this.accountAdsl = accountAdsl;
    }

    /**
     * @return the accountFtth
     */
    public String getAccountFtth() {
        return accountFtth;
    }

    /**
     * @param accountFtth the accountFtth to set
     */
    public void setAccountFtth(String accountFtth) {
        this.accountFtth = accountFtth;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the userCreate
     */
    public String getUserCreate() {
        return userCreate;
    }

    /**
     * @param userCreate the userCreate to set
     */
    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
