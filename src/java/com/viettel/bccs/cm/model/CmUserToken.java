package com.viettel.bccs.cm.model;

import java.util.List;
import viettel.passport.client.UserToken;

public class CmUserToken extends UserToken {

    public static final String SUCCESS = "1";
    public static final String FAIL = "0";
    private Long userID;
    private String loginName = null;
    private String fullName = null;
    private long groupID;
    private long roleID;
    private long groupLevel;
    private long roleLevel;
    private boolean belongToManyGroup = false;
    private long diaChiID;
    private String diaChi = null;
    private String shopCode;
    private String shopName;
    private String province;
    private Long staffId;
    private Long shopId;
    private Long ownerType;
    private Long ownerId;
    boolean agent;
    private Long shopChannelTypeId;
    private Long saleTransStaffId;
    private boolean presenAgent;
    private Long receiverId;
    private Long receiverType;
    private List<String> menus;
    private List<String> components;
    private String message;
    private String resultCode;
    private String resultMessage;

    public CmUserToken() {
        this.resultCode = SUCCESS;
    }

    public CmUserToken(String resultCode, String resultMessage) {
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
    }

    @Override
    public Long getUserID() {
        return userID;
    }

    @Override
    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @Override
    public String getFullName() {
        return fullName;
    }

    @Override
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public long getGroupID() {
        return groupID;
    }

    public void setGroupID(long groupID) {
        this.groupID = groupID;
    }

    public long getRoleID() {
        return roleID;
    }

    public void setRoleID(long roleID) {
        this.roleID = roleID;
    }

    public long getGroupLevel() {
        return groupLevel;
    }

    public void setGroupLevel(long groupLevel) {
        this.groupLevel = groupLevel;
    }

    public long getRoleLevel() {
        return roleLevel;
    }

    public void setRoleLevel(long roleLevel) {
        this.roleLevel = roleLevel;
    }

    public boolean isBelongToManyGroup() {
        return belongToManyGroup;
    }

    public void setBelongToManyGroup(boolean belongToManyGroup) {
        this.belongToManyGroup = belongToManyGroup;
    }

    public long getDiaChiID() {
        return diaChiID;
    }

    public void setDiaChiID(long diaChiID) {
        this.diaChiID = diaChiID;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(Long ownerType) {
        this.ownerType = ownerType;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isAgent() {
        return agent;
    }

    public void setAgent(boolean agent) {
        this.agent = agent;
    }

    public Long getShopChannelTypeId() {
        return shopChannelTypeId;
    }

    public void setShopChannelTypeId(Long shopChannelTypeId) {
        this.shopChannelTypeId = shopChannelTypeId;
    }

    public Long getSaleTransStaffId() {
        return saleTransStaffId;
    }

    public void setSaleTransStaffId(Long saleTransStaffId) {
        this.saleTransStaffId = saleTransStaffId;
    }

    public boolean isPresenAgent() {
        return presenAgent;
    }

    public void setPresenAgent(boolean presenAgent) {
        this.presenAgent = presenAgent;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long receiverId) {
        this.receiverId = receiverId;
    }

    public Long getReceiverType() {
        return receiverType;
    }

    public void setReceiverType(Long receiverType) {
        this.receiverType = receiverType;
    }

    public List<String> getMenus() {
        return menus;
    }

    public void setMenus(List<String> menus) {
        this.menus = menus;
    }

    public List<String> getComponents() {
        return components;
    }

    public void setComponents(List<String> components) {
        this.components = components;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }
}
