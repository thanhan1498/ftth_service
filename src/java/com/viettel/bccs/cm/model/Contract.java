package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CONTRACT")
public class Contract implements Serializable {

    @Id
    @Column(name = "CONTRACT_ID", length = 10, scale = 0, precision = 10)
    private Long contractId;
    @Column(name = "CONTRACT_NO")
    private String contractNo;
    @Column(name = "OLD_CUST_ID")
    private Long oldCustId;
    @Column(name = "ACCOUNT_NUMBER")
    private String accountNumber;
    @Column(name = "NUM_OF_SUBSCRIBERS")
    private Long numOfSubscribers;
    @Column(name = "SERVICE_TYPES")
    private String serviceTypes;
    @Column(name = "PAY_METHOD_CODE", length = 3)
    private String payMethodCode;
    @Column(name = "CONTRACT_TYPE_CODE")
    private String contractTypeCode;
    @Column(name = "PRINT_METHOD_CODE", length = 4)
    private String printMethodCode;
    @Column(name = "FROM_PRICE")
    private Long fromPrice;
    @Column(name = "TO_PRICE")
    private Long toPrice;
    @Column(name = "REG_TYPE")
    private Long regType;
    @Column(name = "DEPORSIT")
    private Long deporsit;
    @Column(name = "REASON")
    private String reason;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "PAY_AREA_CODE")
    private String payAreaCode;
    @Column(name = "ADDRESS", length = 150)
    private String address;
    @Column(name = "PROVINCE", length = 20)
    private String province;
    @Column(name = "DISTRICT", length = 3)
    private String district;
    @Column(name = "PRECINCT", length = 4)
    private String precinct;
    @Column(name = "BILL_CYCLE_FROM")
    private Long billCycleFrom;
    @Column(name = "SIGN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date signDate;
    @Column(name = "EFFECT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectDate;
    @Column(name = "USER_CREATE")
    private String userCreate;
    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Column(name = "LAST_UPDATE_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdateTime;
    @Column(name = "LAST_UPDATE_USER")
    private String lastUpdateUser;
    @Column(name = "CUST_ID")
    private Long custId;
    @Column(name = "PAYER")
    private String payer;
    @Column(name = "HOME")
    private String home;
    @Column(name = "STREET_NAME")
    private String streetName;
    @Column(name = "STREET_BLOCK_NAME")
    private String streetBlockName;
    @Column(name = "BILL_CYCLE_FROM_CHARGING")
    private String billCycleFromCharging;
    @Column(name = "SUB_ID")
    private Long mainSubId;
    @Column(name = "STREET_BLOCK")
    private String streetBlock;
    @Column(name = "ISDN")
    private String mainIsdn;
    @Column(name = "CONTACT_NAME")
    private String contactName;
    @Column(name = "CONTACT_TITLE")
    private String contactTitle;
    @Column(name = "TEL_FAX", length = 100)
    private String telFax;
    @Column(name = "ID_NO")
    private String idNo;
    @Column(name = "END_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDatetime;
    @Column(name = "CONTRACT_TYPE")
    private String contractType;
    @Column(name = "TEL_MOBILE", length = 11)
    private String telMobile;
    @Column(name = "EMAIL", length = 100)
    private String email;
    @Column(name = "NICK_NAME", length = 100)
    private String nickName;
    @Column(name = "NICK_DOMAIN", length = 5)
    private String nickDomain;
    @Column(name = "NOTICE_CHARGE", length = 4)
    private String noticeCharge;
    @Column(name = "RECEIVE_INVOICE")
    private String receiveInvoice;

    public Contract() {
    }

    public Contract(Long contractId, String contractNo, Long oldCustId, String accountNumber, Long numOfSubscribers, String serviceTypes, String payMethodCode, String contractTypeCode, String printMethodCode, Long fromPrice, Long toPrice, Long regType, Long deporsit, String reason, Long status, String payAreaCode, String address, String province, String district, String precinct, Long billCycleFrom, Date signDate, Date effectDate, String userCreate, Date dateCreate, Date lastUpdateTime, String lastUpdateUser, Long custId, String payer, String home, String streetName, String streetBlockName, String billCycleFromCharging, Long mainSubId, String streetBlock, String mainIsdn, String contactName, String contactTitle, String telFax, String idNo, Date endDatetime, String contractType, String telMobile, String email, String nickName, String nickDomain, String noticeCharge, String receiveInvoice) {
        this.contractId = contractId;
        this.contractNo = contractNo;
        this.oldCustId = oldCustId;
        this.accountNumber = accountNumber;
        this.numOfSubscribers = numOfSubscribers;
        this.serviceTypes = serviceTypes;
        this.payMethodCode = payMethodCode;
        this.contractTypeCode = contractTypeCode;
        this.printMethodCode = printMethodCode;
        this.fromPrice = fromPrice;
        this.toPrice = toPrice;
        this.regType = regType;
        this.deporsit = deporsit;
        this.reason = reason;
        this.status = status;
        this.payAreaCode = payAreaCode;
        this.address = address;
        this.province = province;
        this.district = district;
        this.precinct = precinct;
        this.billCycleFrom = billCycleFrom;
        this.signDate = signDate;
        this.effectDate = effectDate;
        this.userCreate = userCreate;
        this.dateCreate = dateCreate;
        this.lastUpdateTime = lastUpdateTime;
        this.lastUpdateUser = lastUpdateUser;
        this.custId = custId;
        this.payer = payer;
        this.home = home;
        this.streetName = streetName;
        this.streetBlockName = streetBlockName;
        this.billCycleFromCharging = billCycleFromCharging;
        this.mainSubId = mainSubId;
        this.streetBlock = streetBlock;
        this.mainIsdn = mainIsdn;
        this.contactName = contactName;
        this.contactTitle = contactTitle;
        this.telFax = telFax;
        this.idNo = idNo;
        this.endDatetime = endDatetime;
        this.contractType = contractType;
        this.telMobile = telMobile;
        this.email = email;
        this.nickName = nickName;
        this.nickDomain = nickDomain;
        this.noticeCharge = noticeCharge;
        this.receiveInvoice = receiveInvoice;
    }

    public Contract(Contract obj) {
        this.contractId = obj.getContractId();
        this.contractNo = obj.getContractNo();
        this.oldCustId = obj.getOldCustId();
        this.accountNumber = obj.getAccountNumber();
        this.numOfSubscribers = obj.getNumOfSubscribers();
        this.serviceTypes = obj.getServiceTypes();
        this.payMethodCode = obj.getPayMethodCode();
        this.contractTypeCode = obj.getContractTypeCode();
        this.printMethodCode = obj.getPrintMethodCode();
        this.fromPrice = obj.getFromPrice();
        this.toPrice = obj.getToPrice();
        this.regType = obj.getRegType();
        this.deporsit = obj.getDeporsit();
        this.reason = obj.getReason();
        this.status = obj.getStatus();
        this.payAreaCode = obj.getPayAreaCode();
        this.address = obj.getAddress();
        this.province = obj.getProvince();
        this.district = obj.getDistrict();
        this.precinct = obj.getPrecinct();
        this.billCycleFrom = obj.getBillCycleFrom();
        this.signDate = obj.getSignDate();
        this.effectDate = obj.getEffectDate();
        this.userCreate = obj.getUserCreate();
        this.dateCreate = obj.getDateCreate();
        this.lastUpdateTime = obj.getLastUpdateTime();
        this.lastUpdateUser = obj.getLastUpdateUser();
        this.custId = obj.getCustId();
        this.payer = obj.getPayer();
        this.home = obj.getHome();
        this.streetName = obj.getStreetName();
        this.streetBlockName = obj.getStreetBlockName();
        this.billCycleFromCharging = obj.getBillCycleFromCharging();
        this.mainSubId = obj.getMainSubId();
        this.streetBlock = obj.getStreetBlock();
        this.mainIsdn = obj.getMainIsdn();
        this.contactName = obj.getContactName();
        this.contactTitle = obj.getContactTitle();
        this.telFax = obj.getTelFax();
        this.idNo = obj.getIdNo();
        this.endDatetime = obj.getEndDatetime();
        this.contractType = obj.getContractType();
        this.telMobile = obj.getTelMobile();
        this.email = obj.getEmail();
        this.nickName = obj.getNickName();
        this.nickDomain = obj.getNickDomain();
        this.noticeCharge = obj.getNoticeCharge();
        this.receiveInvoice = obj.getReceiveInvoice();
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public Long getOldCustId() {
        return oldCustId;
    }

    public void setOldCustId(Long oldCustId) {
        this.oldCustId = oldCustId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Long getNumOfSubscribers() {
        return numOfSubscribers;
    }

    public void setNumOfSubscribers(Long numOfSubscribers) {
        this.numOfSubscribers = numOfSubscribers;
    }

    public String getServiceTypes() {
        return serviceTypes;
    }

    public void setServiceTypes(String serviceTypes) {
        this.serviceTypes = serviceTypes;
    }

    public String getPayMethodCode() {
        return payMethodCode;
    }

    public void setPayMethodCode(String payMethodCode) {
        this.payMethodCode = payMethodCode;
    }

    public String getContractTypeCode() {
        return contractTypeCode;
    }

    public void setContractTypeCode(String contractTypeCode) {
        this.contractTypeCode = contractTypeCode;
    }

    public String getPrintMethodCode() {
        return printMethodCode;
    }

    public void setPrintMethodCode(String printMethodCode) {
        this.printMethodCode = printMethodCode;
    }

    public Long getFromPrice() {
        return fromPrice;
    }

    public void setFromPrice(Long fromPrice) {
        this.fromPrice = fromPrice;
    }

    public Long getToPrice() {
        return toPrice;
    }

    public void setToPrice(Long toPrice) {
        this.toPrice = toPrice;
    }

    public Long getRegType() {
        return regType;
    }

    public void setRegType(Long regType) {
        this.regType = regType;
    }

    public Long getDeporsit() {
        return deporsit;
    }

    public void setDeporsit(Long deporsit) {
        this.deporsit = deporsit;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getPayAreaCode() {
        return payAreaCode;
    }

    public void setPayAreaCode(String payAreaCode) {
        this.payAreaCode = payAreaCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public Long getBillCycleFrom() {
        return billCycleFrom;
    }

    public void setBillCycleFrom(Long billCycleFrom) {
        this.billCycleFrom = billCycleFrom;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public Date getEffectDate() {
        return effectDate;
    }

    public void setEffectDate(Date effectDate) {
        this.effectDate = effectDate;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public String getPayer() {
        return payer;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetBlockName() {
        return streetBlockName;
    }

    public void setStreetBlockName(String streetBlockName) {
        this.streetBlockName = streetBlockName;
    }

    public String getBillCycleFromCharging() {
        return billCycleFromCharging;
    }

    public void setBillCycleFromCharging(String billCycleFromCharging) {
        this.billCycleFromCharging = billCycleFromCharging;
    }

    public Long getMainSubId() {
        return mainSubId;
    }

    public void setMainSubId(Long mainSubId) {
        this.mainSubId = mainSubId;
    }

    public String getStreetBlock() {
        return streetBlock;
    }

    public void setStreetBlock(String streetBlock) {
        this.streetBlock = streetBlock;
    }

    public String getMainIsdn() {
        return mainIsdn;
    }

    public void setMainIsdn(String mainIsdn) {
        this.mainIsdn = mainIsdn;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactTitle() {
        return contactTitle;
    }

    public void setContactTitle(String contactTitle) {
        this.contactTitle = contactTitle;
    }

    public String getTelFax() {
        return telFax;
    }

    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getTelMobile() {
        return telMobile;
    }

    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickDomain() {
        return nickDomain;
    }

    public void setNickDomain(String nickDomain) {
        this.nickDomain = nickDomain;
    }

    public String getNoticeCharge() {
        return noticeCharge;
    }

    public void setNoticeCharge(String noticeCharge) {
        this.noticeCharge = noticeCharge;
    }

    public String getReceiveInvoice() {
        return receiveInvoice;
    }

    public void setReceiveInvoice(String receiveInvoice) {
        this.receiveInvoice = receiveInvoice;
    }
}
