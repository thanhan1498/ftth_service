package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CONTRACT_BANK")
public class ContractBank implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "CONTRACT_ID")
    private Long contractId;
    @Column(name = "BANK_CODE", length = 10)
    private String bankCode;
    @Column(name = "ACCOUNT_NAME", length = 120)
    private String accountName;
    @Column(name = "ACCOUNT", length = 40)
    private String account;
    @Column(name = "BANK_CONTRACT_NO", length = 10)
    private String bankContractNo;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "BANK_CONTRACT_DATE")
    private Date bankContractDate;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "STATUS")
    private Long status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getBankContractNo() {
        return bankContractNo;
    }

    public void setBankContractNo(String bankContractNo) {
        this.bankContractNo = bankContractNo;
    }

    public Date getBankContractDate() {
        return bankContractDate;
    }

    public void setBankContractDate(Date bankContractDate) {
        this.bankContractDate = bankContractDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
