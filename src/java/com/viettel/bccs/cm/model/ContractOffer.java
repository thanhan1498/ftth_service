package com.viettel.bccs.cm.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "CONTRACT_OFFER")
public class ContractOffer implements java.io.Serializable {

    @Id
    @Column(name = "CONTRACT_OFFER_ID", length = 10, precision = 10, scale = 0)
    private Long contractOfferId;
    @Column(name = "CONTRACT_ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long contractId;
    @Column(name = "REQ_OFFER_ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long reqOfferId;
    @Column(name = "OFFER_ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long offerId;
    @Column(name = "QUALITIES", length = 10, precision = 10, scale = 0)
    private Long qualities;
    @Column(name = "CODE", length = 30)
    private String code;
    @Column(name = "NAME", length = 100)
    private String name;
    @Column(name = "TYPE", length = 10)
    private String type;
    @Column(name = "STATUS", length = 1)
    private Long status;
    @Column(name = "IS_BUNDLE", length = 1, precision = 1, scale = 0)
    private Long isBundle;
    @Transient
    private String offerName;
    @Column(name = "GROUP_ID", length = 20)
    private String groupId;
    @Column(name = "GROUP_IS_ACTIVATE", length = 1, precision = 1, scale = 0)
    private Long groupIsActivate;
    @Transient
    private List offerSubs;

    public ContractOffer() {
    }

    public ContractOffer(Long contractOfferId, Long contractId, Long reqOfferId, Long offerId, Long qualities, String code, String name, String type, Long status, Long isBundle, String offerName, String groupId) {
        this.contractOfferId = contractOfferId;
        this.contractId = contractId;
        this.reqOfferId = reqOfferId;
        this.offerId = offerId;
        this.qualities = qualities;
        this.code = code;
        this.name = name;
        this.type = type;
        this.status = status;
        this.isBundle = isBundle;
        this.offerName = offerName;
        this.groupId = groupId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getContractOfferId() {
        return this.contractOfferId;
    }

    public void setContractOfferId(Long contractOfferId) {
        this.contractOfferId = contractOfferId;
    }

    public Long getContractId() {
        return this.contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Long getReqOfferId() {
        return this.reqOfferId;
    }

    public void setReqOfferId(Long reqOfferId) {
        this.reqOfferId = reqOfferId;
    }

    public Long getOfferId() {
        return this.offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public Long getQualities() {
        return this.qualities;
    }

    public void setQualities(Long qualities) {
        this.qualities = qualities;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }
    @Transient
    private List listSubscriber = null;

    public List getListSubscriber() {
        return listSubscriber;
    }

    public void setListSubscriber(List listSubscriber) {
        this.listSubscriber = listSubscriber;
    }

    public Long getIsBundle() {
        return isBundle;
    }

    public void setIsBundle(Long isBundle) {
        this.isBundle = isBundle;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public List getOfferSubs() {
        return offerSubs;
    }

    public void setOfferSubs(List offerSubs) {
        this.offerSubs = offerSubs;
    }

    public Long getGroupIsActivate() {
        return groupIsActivate;
    }

    public void setGroupIsActivate(Long groupIsActivate) {
        this.groupIsActivate = groupIsActivate;
    }
}