/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
/**
 *
 * @author cuongdm
 */
public class CorporateWhiteList implements Serializable {

    private String isdn;
    private String productCode;
    private Date crateDate;
    private String updateUser;
    private String description;
    private Long status;
    private String statement;
    private String serial;
    private String typeConf;

    /**
     * @return the isdn
     */
    public String getIsdn() {
        return isdn;
    }

    /**
     * @param isdn the isdn to set
     */
    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the crateDate
     */
    public Date getCrateDate() {
        return crateDate;
    }

    /**
     * @param crateDate the crateDate to set
     */
    public void setCrateDate(Date crateDate) {
        this.crateDate = crateDate;
    }

    /**
     * @return the updateUser
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * @param updateUser the updateUser to set
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the statement
     */
    public String getStatement() {
        return statement;
    }

    /**
     * @param statement the statement to set
     */
    public void setStatement(String statement) {
        this.statement = statement;
    }

    /**
     * @return the serial
     */
    public String getSerial() {
        return serial;
    }

    /**
     * @param serial the serial to set
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     * @return the typeConf
     */
    public String getTypeConf() {
        return typeConf;
    }

    /**
     * @param typeConf the typeConf to set
     */
    public void setTypeConf(String typeConf) {
        this.typeConf = typeConf;
    }
}
