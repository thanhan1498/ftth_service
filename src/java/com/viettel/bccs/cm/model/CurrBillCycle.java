package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CURR_BILL_CYCLE")
public class CurrBillCycle implements Serializable {

    @Id
    @Column(name = "BILL_CYCLE_FROM", length = 2, precision = 2, scale = 0)
    private Long billCycleFrom;
    @Column(name = "CURR_BILL_CYCLE", nullable = false, length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    private Date currBillCycle;
    @Column(name = "CHARGING_CODE", length = 10)
    private String chargingCode;
    @Column(name = "DATE_BARRING1", length = 22, precision = 22, scale = 0)
    private Long dateBarring1;
    @Column(name = "DATE_BARRING2", length = 22, precision = 22, scale = 0)
    private Long dateBarring2;
    @Column(name = "DATE_BARRING_ADDITION1", length = 22, precision = 22, scale = 0)
    private Long dateBarringAddition1;
    @Column(name = "DATE_BARRING_ADDITION2", length = 22, precision = 22, scale = 0)
    private Long dateBarringAddition2;

    public Long getBillCycleFrom() {
        return billCycleFrom;
    }

    public void setBillCycleFrom(Long billCycleFrom) {
        this.billCycleFrom = billCycleFrom;
    }

    public Date getCurrBillCycle() {
        return currBillCycle;
    }

    public void setCurrBillCycle(Date currBillCycle) {
        this.currBillCycle = currBillCycle;
    }

    public String getChargingCode() {
        return chargingCode;
    }

    public void setChargingCode(String chargingCode) {
        this.chargingCode = chargingCode;
    }

    public Long getDateBarring1() {
        return dateBarring1;
    }

    public void setDateBarring1(Long dateBarring1) {
        this.dateBarring1 = dateBarring1;
    }

    public Long getDateBarring2() {
        return dateBarring2;
    }

    public void setDateBarring2(Long dateBarring2) {
        this.dateBarring2 = dateBarring2;
    }

    public Long getDateBarringAddition1() {
        return dateBarringAddition1;
    }

    public void setDateBarringAddition1(Long dateBarringAddition1) {
        this.dateBarringAddition1 = dateBarringAddition1;
    }

    public Long getDateBarringAddition2() {
        return dateBarringAddition2;
    }

    public void setDateBarringAddition2(Long dateBarringAddition2) {
        this.dateBarringAddition2 = dateBarringAddition2;
    }
}
