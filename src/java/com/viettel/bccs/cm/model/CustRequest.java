package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author linhlh2
 */
@Entity
@Table(name = "CUST_REQUEST")
public class CustRequest implements Serializable {

    @Id
    @Column(name = "CUST_REQUEST_ID", length = 10, precision = 10, scale = 0)
    private Long custRequestId;
    @Column(name = "CUST_ID", length = 10, precision = 10, scale = 0)
    private Long custId;
    @Column(name = "CONTRACT_ID", length = 10, precision = 10, scale = 0)
    private Long contractId;
    @Column(name = "REQ_DATE", length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    private Date reqDate;
    @Column(name = "FINISH_DATE", length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishDate;
    @Column(name = "ADDED_USER", length = 30)
    private String addedUser;
    @Column(name = "SHOP_CODE", length = 40)
    private String shopCode;
    @Column(name = "STATUS", length = 1)
    private Long status;
    @Column(name = "SERVICE_TYPES", length = 13)
    private String serviceTypes;
    @Column(name = "NUM_OF_SUBSCRIBERS", length = 38)
    private Long numOfSubscribers;
    @Column(name = "REASON", length = 30)
    private String reason;

    public CustRequest() {
    }

    public CustRequest(Long custRequestId) {
        this.custRequestId = custRequestId;
    }

    public Long getCustRequestId() {
        return custRequestId;
    }

    public void setCustRequestId(Long custRequestId) {
        this.custRequestId = custRequestId;
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Date getReqDate() {
        return reqDate;
    }

    public void setReqDate(Date reqDate) {
        this.reqDate = reqDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public String getAddedUser() {
        return addedUser;
    }

    public void setAddedUser(String addedUser) {
        this.addedUser = addedUser;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getServiceTypes() {
        return serviceTypes;
    }

    public void setServiceTypes(String serviceTypes) {
        this.serviceTypes = serviceTypes;
    }

    public Long getNumOfSubscribers() {
        return numOfSubscribers;
    }

    public void setNumOfSubscribers(Long numOfSubscribers) {
        this.numOfSubscribers = numOfSubscribers;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
