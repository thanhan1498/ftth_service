package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author linhlh2
 */
@Entity
@Table(name = "CUSTOMER")
public class Customer implements Serializable {

    @Id
    @Column(name = "CUST_ID", length = 10, precision = 10, scale = 0)
    protected Long custId;
    @Column(name = "BUS_TYPE", nullable = false, length = 10, precision = 10, scale = 0)
    protected String busType;
    @Column(name = "CUST_TYPE_ID", length = 38, precision = 38, scale = 0)
    protected Long custTypeId;
    @Column(name = "ID_TYPE", length = 10, precision = 10, scale = 0)
    protected Long idType;
    @Column(name = "ID_NO", length = 20)
    protected String idNo;
    @Column(name = "ID_ISSUE_PLACE", length = 70)
    protected String idIssuePlace;
    @Column(name = "ID_ISSUE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date idIssueDate;
    @Column(name = "ID_EXPIRE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date idExpireDate;
    @Column(name = "POP_NO", length = 20)
    protected String popNo;
    @Column(name = "BUS_PERMIT_NO", length = 20)
    protected String busPermitNo;
    @Column(name = "POP_ISSUE_PLACE", length = 50)
    protected String popIssuePlace;
    @Column(name = "POP_ISSUE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date popIssueDate;
    @Column(name = "TIN", length = 15)
    protected String tin;
    @Column(name = "CONTACT_NAME", length = 120)
    protected String contactName;
    @Column(name = "CONTACT_TITLE", length = 30)
    protected String contactTitle;
    @Column(name = "NAME", length = 120, nullable = false)
    protected String name;
    @Column(name = "BIRTH_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date birthDate;
    @Column(name = "SEX")
    protected String sex;
    @Column(name = "NATIONALITY")
    protected String nationality;
    @Column(name = "ADDRESS", length = 500)
    protected String address;
    @Column(name = "TEL_FAX", length = 15)
    protected String telFax;
    @Column(name = "EMAIL", length = 50)
    protected String email;
    @Column(name = "AREA_CODE", length = 15)
    protected String areaCode;
    @Column(name = "PROVINCE", length = 20)
    protected String province;
    @Column(name = "DISTRICT", length = 3)
    protected String district;
    @Column(name = "PRECINCT", length = 4)
    protected String precinct;
    @Column(name = "STREET", length = 5)
    protected String street;
    @Column(name = "STREET_NAME", length = 50)
    protected String streetName;
    @Column(name = "STREET_BLOCK", length = 5)
    protected String streetBlock;
    @Column(name = "STREET_BLOCK_NAME", length = 50)
    protected String streetBlockName;
    @Column(name = "HOME", length = 50)
    protected String home;
    @Column(name = "VIP", length = 1)
    protected String vip;
    @Column(name = "STATUS")
    protected Long status;
    @Column(name = "ADDED_USER", length = 50)
    protected String addedUser;
    @Column(name = "ADDED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date addedDate;
    @Column(name = "UPDATED_USER", length = 50)
    protected String updatedUser;
    @Column(name = "UPDATED_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date updatedTime;
    @Column(name = "NOTES", length = 200)
    protected String notes;
    @Column(name = "X_LOCATION", length = 50)
    protected String xLocation;
    @Column(name = "Y_LOCATION", length = 50)
    protected String yLocation;
    @Column(name = "CORRECT_CUS", length = 1)
    protected String correctCus;

    public Customer() {
    }

    public Customer(Long custId) {
        this.custId = custId;
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public Long getCustTypeId() {
        return custTypeId;
    }

    public void setCustTypeId(Long custTypeId) {
        this.custTypeId = custTypeId;
    }

    public Long getIdType() {
        return idType;
    }

    public void setIdType(Long idType) {
        this.idType = idType;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getIdIssuePlace() {
        return idIssuePlace;
    }

    public void setIdIssuePlace(String idIssuePlace) {
        this.idIssuePlace = idIssuePlace;
    }

    public Date getIdIssueDate() {
        return idIssueDate;
    }

    public void setIdIssueDate(Date idIssueDate) {
        this.idIssueDate = idIssueDate;
    }

    public Date getIdExpireDate() {
        return idExpireDate;
    }

    public void setIdExpireDate(Date idExpireDate) {
        this.idExpireDate = idExpireDate;
    }

    public String getPopNo() {
        return popNo;
    }

    public void setPopNo(String popNo) {
        this.popNo = popNo;
    }

    public String getBusPermitNo() {
        return busPermitNo;
    }

    public void setBusPermitNo(String busPermitNo) {
        this.busPermitNo = busPermitNo;
    }

    public String getPopIssuePlace() {
        return popIssuePlace;
    }

    public void setPopIssuePlace(String popIssuePlace) {
        this.popIssuePlace = popIssuePlace;
    }

    public Date getPopIssueDate() {
        return popIssueDate;
    }

    public void setPopIssueDate(Date popIssueDate) {
        this.popIssueDate = popIssueDate;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactTitle() {
        return contactTitle;
    }

    public void setContactTitle(String contactTitle) {
        this.contactTitle = contactTitle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelFax() {
        return telFax;
    }

    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetBlock() {
        return streetBlock;
    }

    public void setStreetBlock(String streetBlock) {
        this.streetBlock = streetBlock;
    }

    public String getStreetBlockName() {
        return streetBlockName;
    }

    public void setStreetBlockName(String streetBlockName) {
        this.streetBlockName = streetBlockName;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getAddedUser() {
        return addedUser;
    }

    public void setAddedUser(String addedUser) {
        this.addedUser = addedUser;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getxLocation() {
        return xLocation;
    }

    public void setxLocation(String xLocation) {
        this.xLocation = xLocation;
    }

    public String getyLocation() {
        return yLocation;
    }

    public void setyLocation(String yLocation) {
        this.yLocation = yLocation;
    }

    public String getCorrectCus() {
        return correctCus;
    }

    public void setCorrectCus(String correctCus) {
        this.correctCus = correctCus;
    }
}
