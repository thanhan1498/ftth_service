/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

/**
 *
 * @author cuongdm
 */
public class CustomerExtend extends Customer {

    private Long taskMngtId;
    private String deployLat;
    private String deployLng;

    /**
     * @return the taskMngtId
     */
    public Long getTaskMngtId() {
        return taskMngtId;
    }

    /**
     * @param taskMngtId the taskMngtId to set
     */
    public void setTaskMngtId(Long taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    /**
     * @return the deployLat
     */
    public String getDeployLat() {
        return deployLat;
    }

    /**
     * @param deployLat the deployLat to set
     */
    public void setDeployLat(String deployLat) {
        this.deployLat = deployLat;
    }

    /**
     * @return the deployLng
     */
    public String getDeployLng() {
        return deployLng;
    }

    /**
     * @param deployLng the deployLng to set
     */
    public void setDeployLng(String deployLng) {
        this.deployLng = deployLng;
    }

}
