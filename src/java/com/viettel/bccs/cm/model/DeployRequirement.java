package com.viettel.bccs.cm.model;

import com.viettel.bccs.cm.model.pk.DeployRequirementId;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "DEPLOY_REQUIREMENT")
@IdClass(DeployRequirementId.class)
public class DeployRequirement implements Serializable {

    @Id
    private Long reqId;
    @Id
    @Temporal(TemporalType.TIMESTAMP)
    private Date reqDatetime;
    @Column(name = "NAME", nullable = false, length = 150)
    private String name;
    @Column(name = "TEL_FAX", length = 150)
    private String telFax;
    @Column(name = "DEPLOY_AREA_CODE", nullable = false, length = 20)
    private String deployAreaCode;
    @Column(name = "DEPLOY_ADDRESS", nullable = false, length = 150)
    private String deployAddress;
    @Column(name = "LINE_TYPE", nullable = false, length = 3)
    private String lineType;
    @Column(name = "LINE_PHONE", length = 10)
    private String linePhone;
    @Column(name = "SHOP_ID", length = 22, precision = 22, scale = 0)
    private Long shopId;
    @Column(name = "STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long staffId;
    @Column(name = "STATUS", nullable = false)
    private Long status;
    @Column(name = "USER_ID", nullable = false, precision = 1, scale = 0)
    private Long userId;
    @Column(name = "DSLAM_ID", length = 10, precision = 10, scale = 0)
    private Long dslamId;
    @Column(name = "EXPIRED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiredDate;
    @Column(name = "ISSUE_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date issueDatetime;
    @Column(name = "REASON_ID", length = 22, precision = 22, scale = 0)
    private Long reasonId;
    @Column(name = "DESCRIPTION", length = 100)
    private String description;
    @Column(name = "CAB_LEN", length = 10, precision = 10, scale = 0)
    private Long cabLen;
    @Column(name = "DEPLOYER_ID", length = 10, precision = 10, scale = 0)
    private Long deployerId;
    @Column(name = "DEPLOY_SHOP_ID", length = 10, precision = 10, scale = 0)
    private Long deployShopId;
    @Column(name = "DEPLOY_STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long deployStaffId;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "REQ_TYPE", length = 1)
    private String reqType;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "PORT_NO", length = 10)
    private String portNo;
    @Column(name = "PRODUCT_CODE", length = 20)
    private String productCode;
    @Column(name = "IB", length = 1)
    private String ib;
    @Column(name = "CALL_SHOP_ID", length = 10, precision = 10, scale = 0)
    private Long callShopId;
    @Column(name = "CALL_STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long callStaffId;
    @Column(name = "BOARD_ID", length = 10, precision = 10, scale = 0)
    private Long boardId;
    @Column(name = "CABLE_BOX_ID", length = 10, precision = 10, scale = 0)
    private Long cableBoxId;
    @Column(name = "ACCEPT_SOURCE", length = 1)
    private String acceptSource;
    @Column(name = "GROUP_REQ_ID", length = 10, precision = 10, scale = 0)
    private Long groupReqId;
    @Column(name = "MOBILE", length = 10)
    private String mobile;
    @Column(name = "SERVICE_TYPE", length = 10)
    private String serviceType;
    @Column(name = "SOURCE_ID", length = 10, precision = 10, scale = 0)
    private Long sourceId;
    @Column(name = "CONTACT", length = 100)
    private String contact;
    @Column(name = "CONTACT_END", length = 100)
    private String contactEnd;

    public Long getReqId() {
        return reqId;
    }

    public void setReqId(Long reqId) {
        this.reqId = reqId;
    }

    public Date getReqDatetime() {
        return reqDatetime;
    }

    public void setReqDatetime(Date reqDatetime) {
        this.reqDatetime = reqDatetime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelFax() {
        return telFax;
    }

    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    public String getDeployAreaCode() {
        return deployAreaCode;
    }

    public void setDeployAreaCode(String deployAreaCode) {
        this.deployAreaCode = deployAreaCode;
    }

    public String getDeployAddress() {
        return deployAddress;
    }

    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getLinePhone() {
        return linePhone;
    }

    public void setLinePhone(String linePhone) {
        this.linePhone = linePhone;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDslamId() {
        return dslamId;
    }

    public void setDslamId(Long dslamId) {
        this.dslamId = dslamId;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Date getIssueDatetime() {
        return issueDatetime;
    }

    public void setIssueDatetime(Date issueDatetime) {
        this.issueDatetime = issueDatetime;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCabLen() {
        return cabLen;
    }

    public void setCabLen(Long cabLen) {
        this.cabLen = cabLen;
    }

    public Long getDeployerId() {
        return deployerId;
    }

    public void setDeployerId(Long deployerId) {
        this.deployerId = deployerId;
    }

    public Long getDeployShopId() {
        return deployShopId;
    }

    public void setDeployShopId(Long deployShopId) {
        this.deployShopId = deployShopId;
    }

    public Long getDeployStaffId() {
        return deployStaffId;
    }

    public void setDeployStaffId(Long deployStaffId) {
        this.deployStaffId = deployStaffId;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getReqType() {
        return reqType;
    }

    public void setReqType(String reqType) {
        this.reqType = reqType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getPortNo() {
        return portNo;
    }

    public void setPortNo(String portNo) {
        this.portNo = portNo;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getIb() {
        return ib;
    }

    public void setIb(String ib) {
        this.ib = ib;
    }

    public Long getCallShopId() {
        return callShopId;
    }

    public void setCallShopId(Long callShopId) {
        this.callShopId = callShopId;
    }

    public Long getCallStaffId() {
        return callStaffId;
    }

    public void setCallStaffId(Long callStaffId) {
        this.callStaffId = callStaffId;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Long getCableBoxId() {
        return cableBoxId;
    }

    public void setCableBoxId(Long cableBoxId) {
        this.cableBoxId = cableBoxId;
    }

    public String getAcceptSource() {
        return acceptSource;
    }

    public void setAcceptSource(String acceptSource) {
        this.acceptSource = acceptSource;
    }

    public Long getGroupReqId() {
        return groupReqId;
    }

    public void setGroupReqId(Long groupReqId) {
        this.groupReqId = groupReqId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactEnd() {
        return contactEnd;
    }

    public void setContactEnd(String contactEnd) {
        this.contactEnd = contactEnd;
    }
}
