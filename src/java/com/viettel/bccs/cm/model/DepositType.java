package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DEPOSIT_TYPE")
public class DepositType implements Serializable {

    @Id
    @Column(name = "DEPOSIT_TYPE_ID", length = 22, precision = 22, scale = 0, nullable = false)
    private Long depositTypeId;
    @Column(name = "CODE", length = 10, nullable = false)
    private String code;
    @Column(name = "NAME", length = 50, nullable = false)
    private String name;
    @Column(name = "DESCRIPTION", length = 100)
    private String description;
    @Column(name = "CHECK_DEBIT", length = 1)
    private String checkDebit;
    @Column(name = "STATUS", length = 1)
    private String status;
    @Column(name = "TELECOM_SERVICE_ID")
    private Long telecomServiceId;

    public Long getDepositTypeId() {
        return depositTypeId;
    }

    public void setDepositTypeId(Long depositTypeId) {
        this.depositTypeId = depositTypeId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCheckDebit() {
        return checkDebit;
    }

    public void setCheckDebit(String checkDebit) {
        this.checkDebit = checkDebit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getTelecomServiceId() {
        return telecomServiceId;
    }

    public void setTelecomServiceId(Long telecomServiceId) {
        this.telecomServiceId = telecomServiceId;
    }
}
