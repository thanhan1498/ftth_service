package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DOMAIN")
public class Domain implements Serializable {

    @Id
    @Column(name = "CODE", length = 5)
    private String code;
    @Column(name = "NAME", length = 50)
    private String name;
    @Column(name = "STATUS", length = 22, precision = 22, scale = 0)
    private Long status;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
