/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import com.viettel.bccs.api.common.Common;
import com.viettel.brcd.ws.vsa.ResourceBundleUtil;
import com.viettel.security.PassTranformer;

/**
 *
 * @author tuantm11
 */
public class FTPBean {
    // define variables

    private String host;
    private String username;
    private String password;
    private String workingDir;

    public FTPBean() throws Exception {
        this.host = ResourceBundleUtil.getResource("ftp_host");
        String user = ResourceBundleUtil.getResource("ftp_username");
        String pass = ResourceBundleUtil.getResource("ftp_password");
//        if (!Common.isNullOrEmpty(user)) {
//            user = PassTranformer.decrypt(user);
//        }
//        if (!Common.isNullOrEmpty(pass)) {
//            pass = PassTranformer.decrypt(pass);
//        }
        this.username = user;
        this.password = pass;
        this.workingDir = ResourceBundleUtil.getResource("ftp_working_dir");
    }

    public FTPBean(String host, String username, String password, String workingDir) {
        this.host = host;
        this.username = username;
        this.password = password;
        this.workingDir = workingDir;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getWorkingDir() {
        return workingDir;
    }

    public void setWorkingDir(String workingDir) {
        this.workingDir = workingDir;
    }
}
