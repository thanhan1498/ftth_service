/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.util.List;

/**
 *
 * @author partner1
 */
public class FormChangeInformation {
    List<ElementImageChangeInformation> element;

    public FormChangeInformation() {
    }

    public List<ElementImageChangeInformation> getElement() {
        return element;
    }

    public void setElement(List<ElementImageChangeInformation> element) {
        this.element = element;
    }
    
    
}
