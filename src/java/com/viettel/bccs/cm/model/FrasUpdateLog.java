/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author nhandv
 */
@Entity
@Table(name = "Fras_update_log")
public class FrasUpdateLog implements Serializable {

    @Id
    @Column(name = "ID", nullable = false, length = 22, precision = 10, scale = 0)
    private Long id;
    @Column(name = "ACCOUNT", length = 20)
    private String account;
    @Column(name = "SUB_ID", length = 22)
    private String subID;
    @Column(name = "FRAS_TYPE")
    private String frasType;
    @Column(name = "STAFF_ID")
    private Long staffID;
    @Column(name = "STAFF_NAME")
    private String staffName;
    @Column(name = "INSERT_DATE")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date insertDate;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "STATION_ID")
    private Long stationID;
    @Column(name = "STATION_CODE")
    private String stationCode;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "ERROR_CODE")
    private String errorCode;
    @Column(name = "DEVICE_CODE")
    private String deviceCode;
    @Column(name = "OLD_VALUE")
    private String oldValue;
    @Column(name = "VALUE_TYPE")
    private String valueType;
    @Column(name = "NEW_VALUE")
    private String newValue;
    public void copyFrasUpdateLog(FrasUpdateLog newFrasUpdateLog) {
            newFrasUpdateLog.setAccount(account);
            newFrasUpdateLog.setInsertDate(new Date());
            newFrasUpdateLog.setSubID(subID);
            newFrasUpdateLog.setStaffID(staffID);
            newFrasUpdateLog.setDescription(description);
            newFrasUpdateLog.setStationCode(stationCode);
            newFrasUpdateLog.setStatus(status);
            newFrasUpdateLog.setDeviceCode(deviceCode);
            newFrasUpdateLog.setFrasType(frasType);
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getSubID() {
        return subID;
    }

    public void setSubID(String subID) {
        this.subID = subID;
    }

    public String getFrasType() {
        return frasType;
    }

    public void setFrasType(String frasType) {
        this.frasType = frasType;
    }

    public Long getStaffID() {
        return staffID;
    }

    public void setStaffID(Long staffID) {
        this.staffID = staffID;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStationID() {
        return stationID;
    }

    public void setStationID(Long stationID) {
        this.stationID = stationID;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }
    
}
