/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

/**
 *
 * @author Nguyen Tran Minh Nhut <tranminhnhutn@metfone.com.kh>
 */
public class InforTaskToUpdate {

    String taskName;
    String taskStaffProgress;
    String staffName;
    String startDateValue;
    String endDateValue;
    String startDateActual;
    Long telServiceId;
    Long taskStaffMngtId;
    Long taskMngtId;
    Long subId;
    Long custReqId;
    Long isLackSerial;
    String reqType;
    Long isChangeEquipment;
    Long isLackPlaceCableBox;
    Long isFTTH;
    Long isComplaint;
    Long isKNSC;
    String note;
    Long reasonId;
    String groupReasonCode;
    String serviceType;
    /*duyetdk: bo sung truong status lay tu SubFbConfig*/
    Long status;
    private int isVip;
    private int checkSOC;
    private String customerName;
    private String deployAddress;
    private String productCode;
    private String tel;
    private String accBundleTv;
    private String jobCode;

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public String getGroupReasonCode() {
        return groupReasonCode;
    }

    public void setGroupReasonCode(String groupReasonCode) {
        this.groupReasonCode = groupReasonCode;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskStaffProgress() {
        return taskStaffProgress;
    }

    public void setTaskStaffProgress(String taskStaffProgress) {
        this.taskStaffProgress = taskStaffProgress;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStartDateValue() {
        return startDateValue;
    }

    public void setStartDateValue(String startDateValue) {
        this.startDateValue = startDateValue;
    }

    public String getEndDateValue() {
        return endDateValue;
    }

    public void setEndDateValue(String endDateValue) {
        this.endDateValue = endDateValue;
    }

    public String getStartDateActual() {
        return startDateActual;
    }

    public void setStartDateActual(String startDateActual) {
        this.startDateActual = startDateActual;
    }

    public Long getTelServiceId() {
        return telServiceId;
    }

    public void setTelServiceId(Long telServiceId) {
        this.telServiceId = telServiceId;
    }

    public Long getTaskStaffMngtId() {
        return taskStaffMngtId;
    }

    public void setTaskStaffMngtId(Long taskStaffMngtId) {
        this.taskStaffMngtId = taskStaffMngtId;
    }

    public Long getTaskMngtId() {
        return taskMngtId;
    }

    public void setTaskMngtId(Long taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Long getCustReqId() {
        return custReqId;
    }

    public void setCustReqId(Long custReqId) {
        this.custReqId = custReqId;
    }

    public Long getIsLackSerial() {
        return isLackSerial;
    }

    public void setIsLackSerial(Long isLackSerial) {
        this.isLackSerial = isLackSerial;
    }

    public String getReqType() {
        return reqType;
    }

    public void setReqType(String reqType) {
        this.reqType = reqType;
    }

    public Long getIsChangeEquipment() {
        return isChangeEquipment;
    }

    public void setIsChangeEquipment(Long isChangeEquipment) {
        this.isChangeEquipment = isChangeEquipment;
    }

    public Long getIsLackPlaceCableBox() {
        return isLackPlaceCableBox;
    }

    public void setIsLackPlaceCableBox(Long isLackPlaceCableBox) {
        this.isLackPlaceCableBox = isLackPlaceCableBox;
    }

    public Long getIsFTTH() {
        return isFTTH;
    }

    public void setIsFTTH(Long isFTTH) {
        this.isFTTH = isFTTH;
    }

    public Long getIsComplaint() {
        return isComplaint;
    }

    public void setIsComplaint(Long isComplaint) {
        this.isComplaint = isComplaint;
    }

    public Long getIsKNSC() {
        return isKNSC;
    }

    public void setIsKNSC(Long isKNSC) {
        this.isKNSC = isKNSC;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the isVip
     */
    public int getIsVip() {
        return isVip;
    }

    /**
     * @param isVip the isVip to set
     */
    public void setIsVip(int isVip) {
        this.isVip = isVip;
    }

    /**
     * @return the checkSOC
     */
    public int getCheckSOC() {
        return checkSOC;
    }

    /**
     * @param checkSOC the checkSOC to set
     */
    public void setCheckSOC(int checkSOC) {
        this.checkSOC = checkSOC;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the deployAddress
     */
    public String getDeployAddress() {
        return deployAddress;
    }

    /**
     * @param deployAddress the deployAddress to set
     */
    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the tel
     */
    public String getTel() {
        return tel;
    }

    /**
     * @param tel the tel to set
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * @return the accBundleTv
     */
    public String getAccBundleTv() {
        return accBundleTv;
    }

    /**
     * @param accBundleTv the accBundleTv to set
     */
    public void setAccBundleTv(String accBundleTv) {
        this.accBundleTv = accBundleTv;
    }

    /**
     * @return the jobCode
     */
    public String getJobCode() {
        return jobCode;
    }

    /**
     * @param jobCode the jobCode to set
     */
    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }
    
}
