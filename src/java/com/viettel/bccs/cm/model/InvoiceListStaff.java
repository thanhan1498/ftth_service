package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "INVOICE_LIST_STAFF")
public class InvoiceListStaff implements Serializable {

    @Id
    @Column(name = "ID", length = 10, precision = 10, scale = 0, nullable = false)
    private Long id;
    @Column(name = "STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long staffId;
    @Column(name = "STAFF_CODE", length = 40)
    private String staffCode;
    @Column(name = "CURR_INVOICE_NO", length = 40)
    private String currInvoiceNo;
    @Column(name = "REG_TYPE", length = 10)
    private String regType;
    @Column(name = "INSERT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;
    @Column(name = "PAY_ADV_AMOUNT", length = 22, precision = 22, scale = 5)
    private Double payAdvAmount;
    @Column(name = "CONTRACT_ID", length = 10, precision = 10, scale = 0)
    private Long contractId;
    @Column(name = "INVOICE_LIST_ID", length = 10, precision = 10, scale = 0)
    private Long invoiceListId;
    @Column(name = "STATUS", length = 10, precision = 10, scale = 0)
    private Long status;
    @Column(name = "PROMOTION", length = 10, precision = 10, scale = 4)
    private Double promotion;

    public Double getPromotion() {
        return promotion;
    }

    public void setPromotion(Double promotion) {
        this.promotion = promotion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public String getCurrInvoiceNo() {
        return currInvoiceNo;
    }

    public void setCurrInvoiceNo(String currInvoiceNo) {
        this.currInvoiceNo = currInvoiceNo;
    }

    public String getRegType() {
        return regType;
    }

    public void setRegType(String regType) {
        this.regType = regType;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Double getPayAdvAmount() {
        return payAdvAmount;
    }

    public void setPayAdvAmount(Double payAdvAmount) {
        this.payAdvAmount = payAdvAmount;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Long getInvoiceListId() {
        return invoiceListId;
    }

    public void setInvoiceListId(Long invoiceListId) {
        this.invoiceListId = invoiceListId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
