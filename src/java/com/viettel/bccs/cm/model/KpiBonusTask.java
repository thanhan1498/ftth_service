/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

/**
 *
 * @author cuongdm
 */
public class KpiBonusTask {

    private String province;
    private String keyDic;
    private String jobType;
    private String serviceType;
    private String infraType;
    private int isVip;
    private Double kpi;
    private Double amount;
    private Long lastLevel;

    /**
     * @return the jobType
     */
    public String getJobType() {
        return jobType;
    }

    /**
     * @param jobType the jobType to set
     */
    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    /**
     * @return the infraType
     */
    public String getInfraType() {
        return infraType;
    }

    /**
     * @param infraType the infraType to set
     */
    public void setInfraType(String infraType) {
        this.infraType = infraType;
    }

    /**
     * @return the isVip
     */
    public int getIsVip() {
        return isVip;
    }

    /**
     * @param isVip the isVip to set
     */
    public void setIsVip(int isVip) {
        this.isVip = isVip;
    }

    /**
     * @return the kpi
     */
    public Double getKpi() {
        return kpi;
    }

    /**
     * @param kpi the kpi to set
     */
    public void setKpi(Double kpi) {
        this.kpi = kpi;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the keyDic
     */
    public String getKeyDic() {
        return keyDic;
    }

    /**
     * @param keyDic the keyDic to set
     */
    public void setKeyDic(String keyDic) {
        this.keyDic = keyDic;
    }

    /**
     * @return the province
     */
    public String getProvince() {
        return province;
    }

    /**
     * @param province the province to set
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     * @return the lastLevel
     */
    public Long getLastLevel() {
        return lastLevel;
    }

    /**
     * @param lastLevel the lastLevel to set
     */
    public void setLastLevel(Long lastLevel) {
        this.lastLevel = lastLevel;
    }
}
