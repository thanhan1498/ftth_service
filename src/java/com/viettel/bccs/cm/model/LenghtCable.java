/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Transient;

/**
 *
 * @author nhandv
 */
public class LenghtCable implements Serializable {

    @Transient
    private String lenghtCable;

    public String getLenghtCable() {
        return lenghtCable;
    }

    public void setLenghtCable(String lenghtCable) {
        this.lenghtCable = lenghtCable;
    }
    
}
