package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author linhlh2
 */
@Entity
@Table(name = "LOCAL_PRODUCT_PP")
public class LocalProductPp implements Serializable {

    @Id
    @Column(name = "ID", scale = 0)
    private Long id;
    @Column(name = "CHARGE_METHOD", length = 1, precision = 1, scale = 0)
    private Long chargeMethod;
    @Column(name = "SPEED", length = 10, precision = 10, scale = 0)
    private Long speed;
    @Column(name = "CODE", length = 30)
    private String code;
    @Column(name = "NAME", length = 50)
    private String name;
    @Column(name = "STATUS", length = 1)
    private String status;
    @Column(name = "SUB_TYPE", length = 3)
    private String subType;
    @Column(name = "OFFER_ID", nullable = false, scale = 0)
    private Long offerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChargeMethod() {
        return chargeMethod;
    }

    public void setChargeMethod(Long chargeMethod) {
        this.chargeMethod = chargeMethod;
    }

    public Long getSpeed() {
        return speed;
    }

    public void setSpeed(Long speed) {
        this.speed = speed;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }
}
