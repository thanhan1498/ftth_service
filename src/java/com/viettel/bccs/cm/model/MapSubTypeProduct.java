package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "MAP_SUB_TYPE_PRODUCT_NEW")
public class MapSubTypeProduct implements Serializable {

    @Id
    @Column(name = "ID", length = 22, precision = 22, scale = 0, nullable = false)
    private Long id;
    @Column(name = "SUB_TYPE", length = 20)
    private String subType;
    @Column(name = "BUS_TYPE", length = 20)
    private String busType;
    @Column(name = "PRODUCT_CODE", length = 30)
    private String productCode;
    @Column(name = "IS_DEFAULT", length = 1, precision = 1, scale = 0, nullable = false)
    private Long isDefault;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0, nullable = false)
    private Long status;
    @Column(name = "SERVICE_TYPE", length = 1, nullable = false)
    private String serviceType;
    @Column(name = "CHANGE_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date changeDatetime;
    @Column(name = "USER_NAME", length = 50)
    private String userName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Long getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Long isDefault) {
        this.isDefault = isDefault;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Date getChangeDatetime() {
        return changeDatetime;
    }

    public void setChangeDatetime(Date changeDatetime) {
        this.changeDatetime = changeDatetime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
