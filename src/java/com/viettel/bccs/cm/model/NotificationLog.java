/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "NOTIFICATION_LOG")
public class NotificationLog implements Serializable {

    @Id
    @Column(name = "ID", length = 22, precision = 22, scale = 0)
    private Long id;
    @Column(name = "SENDER", length = 50)
    private String sender;
    @Column(name = "RECEIVER_CODE", length = 50)
    private String receiverCode;
    @Column(name = "RECEIVER_ID", length = 10, precision = 10, scale = 0)
    private Long receiverId;
    @Column(name = "DEVICE_ID", length = 200)
    private String deviceId;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CREATE_USER", length = 50)
    private String createUser;
    @Column(name = "MESSAGE", length = 500)
    private String message;
    @Column(name = "REQUEST", length = 1000)
    private String request;
    @Column(name = "RESPONSE", length = 500)
    private String response;
    @Column(name = "ALIAS", length = 100)
    private String alias;
    @Column(name = "APP_NAME", length = 50)
    private String appName;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "TASK_MNGT_ID", length = 10, precision = 10, scale = 0)
    private Long taskMngtId;
    @Column(name = "ACCOUNT", length = 50)
    private String account;

    public NotificationLog() {
    }

    public NotificationLog(String receiverCode, Long receiverId, String deviceId, String createUser, String message, String alias, String account, Long taskMngtId) {
        this.receiverCode = receiverCode;
        this.receiverId = receiverId;
        this.deviceId = deviceId;
        this.createUser = createUser;
        this.message = message;
        this.alias = alias;
        this.account = account;
        this.taskMngtId = taskMngtId;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the sender
     */
    public String getSender() {
        return sender;
    }

    /**
     * @param sender the sender to set
     */
    public void setSender(String sender) {
        this.sender = sender;
    }

    /**
     * @return the receiverCode
     */
    public String getReceiverCode() {
        return receiverCode;
    }

    /**
     * @param receiverCode the receiverCode to set
     */
    public void setReceiverCode(String receiverCode) {
        this.receiverCode = receiverCode;
    }

    /**
     * @return the receiverId
     */
    public Long getReceiverId() {
        return receiverId;
    }

    /**
     * @param receiverId the receiverId to set
     */
    public void setReceiverId(Long receiverId) {
        this.receiverId = receiverId;
    }

    /**
     * @return the deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId the deviceId to set
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the request
     */
    public String getRequest() {
        return request;
    }

    /**
     * @param request the request to set
     */
    public void setRequest(String request) {
        this.request = request;
    }

    /**
     * @return the response
     */
    public String getResponse() {
        return response;
    }

    /**
     * @param response the response to set
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * @return the alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * @return the appName
     */
    public String getAppName() {
        return appName;
    }

    /**
     * @param appName the appName to set
     */
    public void setAppName(String appName) {
        this.appName = appName;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the taskMngtId
     */
    public Long getTaskMngtId() {
        return taskMngtId;
    }

    /**
     * @param taskMngtId the taskMngtId to set
     */
    public void setTaskMngtId(Long taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }
}
