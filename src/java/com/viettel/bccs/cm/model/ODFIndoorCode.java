/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Transient;

/**
 *
 * @author nhandv
 */
public class ODFIndoorCode implements Serializable {

    @Transient
    private String code;
    @Transient
    private Long id;

    public ODFIndoorCode() {

    }

    public ODFIndoorCode(String Code, Long id) {
        this.code = Code;
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
