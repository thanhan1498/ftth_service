package com.viettel.bccs.cm.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "OFFER_REQUEST")
public class OfferRequest implements java.io.Serializable {

    @Id
    @Column(name = "REQ_OFFER_ID", length = 10, precision = 10, scale = 0)
    private Long reqOfferId;
    @Column(name = "OFFER_ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long offerId;
    @Transient
    private String offerName;
    @Column(name = "CUST_REQ_ID", nullable = false, length = 10, precision = 10, scale = 0)//TuanPV added on 29/04/2009 - Dùng để hiển thị tên sản phẩm trên màn hình ViewRequestDetail
    private Long custReqId;
    @Column(name = "QUALITIES", nullable = false, length = 10, precision = 10, scale = 0)
    private Long qualities;
    @Column(name = "IS_BUNDLE", nullable = false, length = 1, precision = 1, scale = 0)
    private Long isBundle;
    @Transient
    private List lstReqSub = new ArrayList();
    @Transient
    private Long nextStep;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "TYPE", length = 20)
    private String type;
    @Transient
    String actionString;
    @Transient
    String nextStepAction;
    @Transient
    String actionName;

    public OfferRequest() {
    }

    public OfferRequest(Long reqOfferId, Long offerId, Long custReqId, Long qualities, Long isBundle, Long nextStep, Long status, String actionString, String nextStepAction, String actionName) {
        this.reqOfferId = reqOfferId;
        this.offerId = offerId;
        this.custReqId = custReqId;
        this.qualities = qualities;
        this.isBundle = isBundle;
        this.nextStep = nextStep;
        this.status = status;
        this.actionString = actionString;
        this.nextStepAction = nextStepAction;
        this.actionName = actionName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getReqOfferId() {
        return this.reqOfferId;
    }

    public void setReqOfferId(Long reqOfferId) {
        this.reqOfferId = reqOfferId;
    }

    public Long getOfferId() {
        return this.offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public Long getCustReqId() {
        return this.custReqId;
    }

    public void setCustReqId(Long custReqId) {
        this.custReqId = custReqId;
    }

    public Long getQualities() {
        return this.qualities;
    }

    public void setQualities(Long qualities) {
        this.qualities = qualities;
    }

    public Long getIsBundle() {
        return isBundle;
    }

    public void setIsBundle(Long isBundle) {
        this.isBundle = isBundle;
    }

    public Long getNextStep() {
        return nextStep;
    }

    public void setNextStep(Long nextStep) {
        this.nextStep = nextStep;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionString() {
        return actionString;
    }

    public void setActionString(String actionString) {
        this.actionString = actionString;
    }

    public List getLstReqSub() {
        return lstReqSub;
    }

    public void setLstReqSub(List lstReqSub) {
        this.lstReqSub = lstReqSub;
    }

    public String getNextStepAction() {
        return nextStepAction;
    }

    public void setNextStepAction(String nextStepAction) {
        this.nextStepAction = nextStepAction;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
