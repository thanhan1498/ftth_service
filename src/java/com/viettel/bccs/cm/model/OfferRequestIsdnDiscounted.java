package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "OFFER_REQUEST_ISDN_DISCOUNTED")
public class OfferRequestIsdnDiscounted implements Serializable {

    @Id
    @Column(name = "DISCOUNT_ISDN_ID", length = 10, precision = 10, scale = 0)
    private Long discountIsdnId;
    @Column(name = "REQ_OFFER_ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long reqOfferId;
    @Column(name = "SUB_REQ_ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long subReqId;
    @Column(name = "SUB_ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "DISCOUNT_ISDN", nullable = false, length = 15)
    private String discountIsdn;
    @Column(name = "DISCOUNT_RATE", length = 2, precision = 2, scale = 0)
    private Long discountRate;
    @Column(name = "STA_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date staDatetime;
    @Column(name = "END_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDatetime;
    @Column(name = "STATUS")
    private Long status;

    public Long getDiscountIsdnId() {
        return discountIsdnId;
    }

    public void setDiscountIsdnId(Long discountIsdnId) {
        this.discountIsdnId = discountIsdnId;
    }

    public Long getReqOfferId() {
        return reqOfferId;
    }

    public void setReqOfferId(Long reqOfferId) {
        this.reqOfferId = reqOfferId;
    }

    public Long getSubReqId() {
        return subReqId;
    }

    public void setSubReqId(Long subReqId) {
        this.subReqId = subReqId;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getDiscountIsdn() {
        return discountIsdn;
    }

    public void setDiscountIsdn(String discountIsdn) {
        this.discountIsdn = discountIsdn;
    }

    public Long getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(Long discountRate) {
        this.discountRate = discountRate;
    }

    public Date getStaDatetime() {
        return staDatetime;
    }

    public void setStaDatetime(Date staDatetime) {
        this.staDatetime = staDatetime;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
