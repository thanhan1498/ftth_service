package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "OFFER_SUB")
public class OfferSub implements Serializable {

    @Id
    @Column(name = "OFFER_SUB_ID", length = 10, precision = 10, scale = 0)
    private Long offerSubId;
    @Column(name = "CONTRACT_OFFER_ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long contractOfferId;
    @Column(name = "SUB_ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "MAIN", length = 1)
    private String main;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "PAY_LIMIT", length = 10, precision = 10, scale = 0)
    private Long payLimit;
    @Column(name = "PAY_METHOD", length = 1, precision = 1, scale = 0)
    private Long payMethod;
    @Column(name = "DB_STA_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dbStaDatetime;
    @Column(name = "DB_MODI_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dbModiDatetime;
    @Column(name = "ZTE_LOG_ID", length = 10, precision = 10, scale = 0)
    private Long zteLogId;
    @Column(name = "END_ZTE_LOG_ID", length = 10, precision = 10, scale = 0)
    private Long endZteLogId;
    @Column(name = "SPEED_DIAL_NUMBER", length = 10)
    private String speedDialNumber;
    @Column(name = "IN_MODI_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inModiDatetime;
    @Column(name = "IN_STA_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inStaDatetime;
    @Column(name = "STA_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date staDatetime;
    @Column(name = "END_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDatetime;
    @Column(name = "TEL_SERVICE", length = 1)
    private String telService;
    @Column(name = "IS_NEW_SUB", length = 1, precision = 1, scale = 0)
    private Long isNewSub;
    @Column(name = "PRODUCT_ID", length = 10, precision = 10, scale = 0)
    private Long productId;
    @Column(name = "BAL_SHARE_ID", length = 10, precision = 10, scale = 0)
    private Long balShareId;

    public Long getOfferSubId() {
        return offerSubId;
    }

    public void setOfferSubId(Long offerSubId) {
        this.offerSubId = offerSubId;
    }

    public Long getContractOfferId() {
        return contractOfferId;
    }

    public void setContractOfferId(Long contractOfferId) {
        this.contractOfferId = contractOfferId;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getPayLimit() {
        return payLimit;
    }

    public void setPayLimit(Long payLimit) {
        this.payLimit = payLimit;
    }

    public Long getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(Long payMethod) {
        this.payMethod = payMethod;
    }

    public Date getDbStaDatetime() {
        return dbStaDatetime;
    }

    public void setDbStaDatetime(Date dbStaDatetime) {
        this.dbStaDatetime = dbStaDatetime;
    }

    public Date getDbModiDatetime() {
        return dbModiDatetime;
    }

    public void setDbModiDatetime(Date dbModiDatetime) {
        this.dbModiDatetime = dbModiDatetime;
    }

    public Long getZteLogId() {
        return zteLogId;
    }

    public void setZteLogId(Long zteLogId) {
        this.zteLogId = zteLogId;
    }

    public Long getEndZteLogId() {
        return endZteLogId;
    }

    public void setEndZteLogId(Long endZteLogId) {
        this.endZteLogId = endZteLogId;
    }

    public String getSpeedDialNumber() {
        return speedDialNumber;
    }

    public void setSpeedDialNumber(String speedDialNumber) {
        this.speedDialNumber = speedDialNumber;
    }

    public Date getInModiDatetime() {
        return inModiDatetime;
    }

    public void setInModiDatetime(Date inModiDatetime) {
        this.inModiDatetime = inModiDatetime;
    }

    public Date getInStaDatetime() {
        return inStaDatetime;
    }

    public void setInStaDatetime(Date inStaDatetime) {
        this.inStaDatetime = inStaDatetime;
    }

    public Date getStaDatetime() {
        return staDatetime;
    }

    public void setStaDatetime(Date staDatetime) {
        this.staDatetime = staDatetime;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public String getTelService() {
        return telService;
    }

    public void setTelService(String telService) {
        this.telService = telService;
    }

    public Long getIsNewSub() {
        return isNewSub;
    }

    public void setIsNewSub(Long isNewSub) {
        this.isNewSub = isNewSub;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getBalShareId() {
        return balShareId;
    }

    public void setBalShareId(Long balShareId) {
        this.balShareId = balShareId;
    }
}
