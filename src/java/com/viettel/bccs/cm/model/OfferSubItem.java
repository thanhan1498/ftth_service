package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "OFFER_SUB_ITEM")
public class OfferSubItem implements Serializable {

    @Id
    @Column(name = "OFFER_SUB_ITEM_ID", length = 10, precision = 10, scale = 0)
    private Long offerSubItemId;
    @Column(name = "CONTRACT_OFFER_ID", length = 10, precision = 10, scale = 0)
    private Long contractOfferId;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "SERVICE_ID", nullable = false, length = 30)
    private String serviceId;
    @Column(name = "STA_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date staDatetime;
    @Column(name = "END_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDatetime;
    @Column(name = "STATUS")
    private Long status;

    public Long getOfferSubItemId() {
        return offerSubItemId;
    }

    public void setOfferSubItemId(Long offerSubItemId) {
        this.offerSubItemId = offerSubItemId;
    }

    public Long getContractOfferId() {
        return contractOfferId;
    }

    public void setContractOfferId(Long contractOfferId) {
        this.contractOfferId = contractOfferId;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public Date getStaDatetime() {
        return staDatetime;
    }

    public void setStaDatetime(Date staDatetime) {
        this.staDatetime = staDatetime;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
