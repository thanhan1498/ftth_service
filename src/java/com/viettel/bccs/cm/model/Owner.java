/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.util.Date;

/**
 *
 * @author quangdm
 */
public class Owner {
    private Long regOwnerId;
    private String isdn;
    private Long subId;
    private String product;
    private String insertDate;
    private Date updateDate;
    private String description;
    private String isdn_n3_p3;
    private Integer numberMember;

   
    public Owner() {
    }
    
    public Owner(Long regOwnerId, String isdn, Long subId, String product,
            String insertDate, Date updateDate, String description,String isdn_n3_p3,Integer numberMember) {
        this.regOwnerId = regOwnerId;
        this.isdn = isdn;
        this.subId = subId;
        this.product = product;
        this.insertDate = insertDate;
        this.updateDate = updateDate;
        this.description = description;
        this.isdn_n3_p3=isdn_n3_p3;
        this.numberMember=numberMember;
    }
    
    public Long getRegOwnerId() {
        return regOwnerId;
    }

    public void setRegOwnerId(Long regOwnerId) {
        this.regOwnerId = regOwnerId;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
     public String getIsdn_n3_p3() {
        return isdn_n3_p3;
    }

    public void setIsdn_n3_p3(String isdn_n3_p3) {
        this.isdn_n3_p3 = isdn_n3_p3;
    }
    public Integer getNumberMember() {
        return numberMember;
    }

    public void setNumberMember(Integer numberMember) {
        this.numberMember = numberMember;
    }
}
