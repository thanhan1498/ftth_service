package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author duyetdk
 */
@Entity
@Table(name = "FBB_POTENTIAL_CUSTOMER")
public class PotentialCustomer implements Serializable {

    @Id
    @Column(name = "CUST_ID", length = 10, precision = 10, scale = 0)
    private Long custId;
    @Column(name = "NAME", length = 100)
    private String name;
    @Column(name = "PHONE_NUMBER", length = 70)
    private String phoneNumber;
    @Column(name = "EMAIL", length = 100)
    private String email;
    @Column(name = "ADDRESS", length = 500)
    private String address;
    @Column(name = "PROVINCE", length = 20)
    private String province;
    @Column(name = "OTHER_OPERATOR", length = 100)
    private String otherOperator;
    @Column(name = "FEE", length = 10, precision = 10, scale = 0)
    private Double fee;
    @Column(name = "LATITUDE", length = 255)
    private String latitude;
    @Column(name = "LONGITUDE", length = 255)
    private String longitude;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "CREATE_USER", length = 50)
    private String createUser;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UPDATE_USER", length = 50)
    private String updateUser;
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "NOTES", length = 200)
    private String notes;
    @Column(name = "KIND_OF_CUSTOMER", length = 20)
    private String kindOfCustomer;
    @Column(name = "EXPECTED_SERVICE", length = 20)
    private String expectedService;
    @Column(name = "EXPECTED_SCALE", length = 20)
    private String expectedScale;
    @Column(name = "SERVICE_USED", length = 20)
    private String serviceUsed;

    public PotentialCustomer() {
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getOtherOperator() {
        return otherOperator;
    }

    public void setOtherOperator(String otherOperator) {
        this.otherOperator = otherOperator;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getKindOfCustomer() {
        return kindOfCustomer;
    }

    public void setKindOfCustomer(String kindOfCustomer) {
        this.kindOfCustomer = kindOfCustomer;
    }

    public String getExpectedService() {
        return expectedService;
    }

    public void setExpectedService(String expectedService) {
        this.expectedService = expectedService;
    }

    public String getExpectedScale() {
        return expectedScale;
    }

    public void setExpectedScale(String expectedScale) {
        this.expectedScale = expectedScale;
    }

    public String getServiceUsed() {
        return serviceUsed;
    }

    public void setServiceUsed(String serviceUsed) {
        this.serviceUsed = serviceUsed;
    }
}
