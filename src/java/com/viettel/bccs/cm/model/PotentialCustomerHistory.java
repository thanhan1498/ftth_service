/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import com.viettel.bccs.cm.util.DateTimeUtils;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author phuonghc
 */
@Entity
@Table(name = "FBB_POTENTIAL_CUSTOMER_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PotentialCustomerHistory.findAll", query = "SELECT f FROM PotentialCustomerHistory f")
})
public class PotentialCustomerHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "HIS_ID")
    private Long hisId;
    @Basic(optional = false)
    @Column(name = "CUST_ID")
    private long custId;
    @Column(name = "TYPE_IMPACTED")
    private String typeImpacted;
    @Column(name = "CREATE_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;
    @Column(name = "USER_IMPACTED")
    private String userImpacted;
    @Column(name = "OLD_VALUE")
    private String oldValue;
    @Column(name = "NEW_VALUE")
    private String newValue;
    @Transient
    private String createTimeStr;

    public PotentialCustomerHistory() {
    }

    public PotentialCustomerHistory(Long hisId) {
        this.hisId = hisId;
    }

    public PotentialCustomerHistory(Long hisId, long custId) {
        this.hisId = hisId;
        this.custId = custId;
    }

    public Long getHisId() {
        return hisId;
    }

    public void setHisId(Long hisId) {
        this.hisId = hisId;
    }

    public long getCustId() {
        return custId;
    }

    public void setCustId(long custId) {
        this.custId = custId;
    }

    public String getTypeImpacted() {
        return typeImpacted;
    }

    public void setTypeImpacted(String typeImpacted) {
        this.typeImpacted = typeImpacted;
    }

    public Date getCreateTime() {
        return null;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;

    }

    public String getUserImpacted() {
        return userImpacted;
    }

    public void setUserImpacted(String userImpacted) {
        this.userImpacted = userImpacted;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getCreateTimeStr() {
        if (this.createTime != null) {
            return createTimeStr = DateTimeUtils.formatddMMyyyyHHmmss(this.createTime);
        }
        return createTimeStr;

    }

    public void setCreateTimeStr(String createTimeStr) {
        if (this.createTime != null) {
            this.createTimeStr = DateTimeUtils.formatddMMyyyyHHmmss(this.createTime);
        }
        this.createTimeStr = createTimeStr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (hisId != null ? hisId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PotentialCustomerHistory)) {
            return false;
        }
        PotentialCustomerHistory other = (PotentialCustomerHistory) object;
        if ((this.hisId == null && other.hisId != null) || (this.hisId != null && !this.hisId.equals(other.hisId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.bccs.cm.model.FbbPotentialCustomerHistory[ hisId=" + hisId + " ]";
    }
}
