/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import org.hibernate.tool.hbm2x.StringUtils;

/**
 * PotentialCustomerHistoryDto
 *
 * @author phuonghc
 */
public class PotentialCustomerHistoryDto {

    private Long hisId;
    private Long custId;
    private String column;
    private String createTime;
    private String userImpacted;
    private String oldValue;
    private String newValue;

    public PotentialCustomerHistoryDto() {
    }

    public Long getHisId() {
        return hisId;
    }

    public void setHisId(Long hisId) {
        this.hisId = hisId;
    }

    public long getCustId() {
        return custId;
    }

    public void setCustId(long custId) {
        this.custId = custId;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUserImpacted() {
        return userImpacted;
    }

    public void setUserImpacted(String userImpacted) {
        this.userImpacted = userImpacted;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        if (StringUtils.isEmpty(oldValue)) {
            this.oldValue = " ";
        } else {
            this.oldValue = oldValue;
        }
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }
}
