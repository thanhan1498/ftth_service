package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author linhlh2
 */
@Entity
@Table(name = "PRODUCT_PRICE_PLAN")
public class ProductPricePlan implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "CHARGE_METHOD", length = 1, precision = 1, scale = 0)
    private Long chargeMethod;
    @Column(name = "SPEED", length = 100)
    private String speed;
    @Column(name = "CODE", length = 30)
    private String code;
    @Column(name = "NAME", length = 50)
    private String name;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "OFFER_ID", nullable = false, length = 19, precision = 19)
    private Long offerId;
    @Column(name = "SUB_TYPE", length = 3)
    private String subType;
    @Column(name = "MAX_MONEY", length = 19, precision = 19)
    private Long maxMoney;
    @Column(name = "INPUT_PEAK_RATE", length = 10, precision = 10)
    private Long inputPeakRate;
    @Column(name = "OUTPUT_PEAK_RATE", length = 10, precision = 10)
    private Long outputPeakRate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChargeMethod() {
        return chargeMethod;
    }

    public void setChargeMethod(Long chargeMethod) {
        this.chargeMethod = chargeMethod;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public Long getMaxMoney() {
        return maxMoney;
    }

    public void setMaxMoney(Long maxMoney) {
        this.maxMoney = maxMoney;
    }

    public Long getInputPeakRate() {
        return inputPeakRate;
    }

    public void setInputPeakRate(Long inputPeakRate) {
        this.inputPeakRate = inputPeakRate;
    }

    public Long getOutputPeakRate() {
        return outputPeakRate;
    }

    public void setOutputPeakRate(Long outputPeakRate) {
        this.outputPeakRate = outputPeakRate;
    }
}
