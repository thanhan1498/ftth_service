package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "PROMOTION_TYPE")
public class PromotionType implements Serializable {

    @Id
    @Column(name = "PROM_PROGRAM_CODE", length = 5, nullable = false)
    private String promProgramCode;
    @Column(name = "NAME", length = 100, nullable = false)
    private String name;
    @Column(name = "STA_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date staDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "DESCRIPTION", length = 4000)
    private String description;
    @Column(name = "TYPE", length = 1)
    private String type;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "LOCAL", length = 1, precision = 1, scale = 0)
    private Long local;
    @Column(name = "PROCEDURE_ID", length = 3, precision = 3, scale = 0)
    private Long procedureId;
    @Column(name = "PRODUCT_ID", length = 2, precision = 2, scale = 0)
    private Long productId;
    @Column(name = "PROM_TYPE", length = 1)
    private String promType;
    @Column(name = "TEL_SERVICE", length = 10)
    private String telService;
    @Column(name = "PRICE_PLAN", length = 30)
    private String pricePlan;
    @Column(name = "CYCLE", length = 10)
    private Long cycle;
    @Column(name = "MONTH_AMOUNT", length = 10, precision = 10, scale = 0)
    private Long monthAmount;
    @Column(name = "MONTH_COMMITMENT", length = 10, precision = 10, scale = 0)
    private Long monthCommitment;
    @Column(name = "CONTENT", length = 4000)
    private String content;

    public String getPromProgramCode() {
        return promProgramCode;
    }

    public void setPromProgramCode(String promProgramCode) {
        this.promProgramCode = promProgramCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStaDate() {
        return staDate;
    }

    public void setStaDate(Date staDate) {
        this.staDate = staDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getLocal() {
        return local;
    }

    public void setLocal(Long local) {
        this.local = local;
    }

    public Long getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(Long procedureId) {
        this.procedureId = procedureId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getPromType() {
        return promType;
    }

    public void setPromType(String promType) {
        this.promType = promType;
    }

    public String getTelService() {
        return telService;
    }

    public void setTelService(String telService) {
        this.telService = telService;
    }

    public String getPricePlan() {
        return pricePlan;
    }

    public void setPricePlan(String pricePlan) {
        this.pricePlan = pricePlan;
    }

    public Long getCycle() {
        return cycle;
    }

    public void setCycle(Long cycle) {
        this.cycle = cycle;
    }

    public Long getMonthAmount() {
        return monthAmount;
    }

    public void setMonthAmount(Long monthAmount) {
        this.monthAmount = monthAmount;
    }

    public Long getMonthCommitment() {
        return monthCommitment;
    }

    public void setMonthCommitment(Long monthCommitment) {
        this.monthCommitment = monthCommitment;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
