package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author do khac duyet
 */
@Entity
@Table(name = "QUERY_LOG")
public class QueryLog implements Serializable {

    @Id
    @Column(name = "QUERY_LOG_ID", length = 20, precision = 20, scale = 0)
    private Long queryLogId;
    @Column(name = "FUNCTION", length = 500)
    private String function;
    @Column(name = "PARAM_VALUE", length = 2000)
    private String paramValue;
    @Column(name = "CREATE_USER", length = 50)
    private String createUser;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "SYSTEM", length = 20)
    private String system;
    @Column(name = "IP", length = 50)
    private String ip;

    public QueryLog() {
    }

    public Long getQueryLogId() {
        return queryLogId;
    }

    public void setQueryLogId(Long queryLogId) {
        this.queryLogId = queryLogId;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

}
