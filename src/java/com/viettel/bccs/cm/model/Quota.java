package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "QUOTA")
public class Quota implements Serializable {

    @Id
    @Column(name = "QUOTA_ID", length = 10, precision = 10, scale = 0)
    private Long quotaId;
    @Column(name = "SERVICE_TYPE", length = 1, nullable = false)
    private String serviceType;
    @Column(name = "QUOTA_VALUE", length = 10, precision = 10, scale = 0)
    private Long quotaValue;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;

    public Long getQuotaId() {
        return quotaId;
    }

    public void setQuotaId(Long quotaId) {
        this.quotaId = quotaId;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Long getQuotaValue() {
        return quotaValue;
    }

    public void setQuotaValue(Long quotaValue) {
        this.quotaValue = quotaValue;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
