package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "REASON")
public class Reason implements Serializable {

    @Id
    @Column(name = "REASON_ID", length = 10, precision = 10, scale = 0)
    protected Long reasonId;
    @Column(name = "TYPE", nullable = false, length = 10)
    protected String type;
    @Column(name = "CODE", length = 10)
    protected String code;
    @Column(name = "NAME", nullable = false)
    protected String name;
    @Column(name = "STATUS")
    protected Long status;
    @Column(name = "SER_TRANS_ID", length = 10)
    protected String serTransId;
    @Column(name = "CREATE_TRANS", length = 1)
    protected String createTrans;
    @Column(name = "REG_SPECIAL", length = 1)
    protected String regSpecial;
    @Column(name = "REQ_PROFILE", length = 1)
    protected Long reqProfile;
    @Column(name = "REG_TYPE_REASON", length = 10)
    protected String regTypeReason;
    @Column(name = "TEL_SERVICE", length = 10)
    protected String telService;
    @Column(name = "LIMIT_NUMBER_ISDN", length = 10, precision = 10, scale = 0)
    protected Long limitNumberIsdn;
    @Column(name = "LIMIT_NUMBER_USER", length = 10, precision = 10, scale = 0)
    protected Long limitNumberUser;
    @Column(name = "PAY_ADV_AMOUNT", length = 10, precision = 10, scale = 0)
    protected Double payAdvAmount;
    @Column(name = "DESCRIPTION", length = 22, scale = 5)
    protected String description;
    @Column(name = "REASON_GROUP", length = 10, scale = 0)
    protected String reasonGroup;
    @Column(name = "PROMOTION", length = 10, precision = 10, scale = 4)
    protected Double promotion;

    public Double getPromotion() {
        return promotion;
    }

    public void setPromotion(Double promotion) {
        this.promotion = promotion;
    }

    public String getReasonGroup() {
        return reasonGroup;
    }

    public void setReasonGroup(String reasonGroup) {
        this.reasonGroup = reasonGroup;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSerTransId() {
        return serTransId;
    }

    public void setSerTransId(String serTransId) {
        this.serTransId = serTransId;
    }

    public String getCreateTrans() {
        return createTrans;
    }

    public void setCreateTrans(String createTrans) {
        this.createTrans = createTrans;
    }

    public String getRegSpecial() {
        return regSpecial;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setRegSpecial(String regSpecial) {
        this.regSpecial = regSpecial;
    }

    public Long getReqProfile() {
        return reqProfile;
    }

    public void setReqProfile(Long reqProfile) {
        this.reqProfile = reqProfile;
    }

    public String getRegTypeReason() {
        return regTypeReason;
    }

    public void setRegTypeReason(String regTypeReason) {
        this.regTypeReason = regTypeReason;
    }

    public String getTelService() {
        return telService;
    }

    public void setTelService(String telService) {
        this.telService = telService;
    }

    public Long getLimitNumberIsdn() {
        return limitNumberIsdn;
    }

    public void setLimitNumberIsdn(Long limitNumberIsdn) {
        this.limitNumberIsdn = limitNumberIsdn;
    }

    public Long getLimitNumberUser() {
        return limitNumberUser;
    }

    public void setLimitNumberUser(Long limitNumberUser) {
        this.limitNumberUser = limitNumberUser;
    }

    public Double getPayAdvAmount() {
        return payAdvAmount;
    }

    public void setPayAdvAmount(Double payAdvAmount) {
        this.payAdvAmount = payAdvAmount;
    }

}
