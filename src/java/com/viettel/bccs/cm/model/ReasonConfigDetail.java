/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

/**
 *
 * @author duyetdk
 */
public class ReasonConfigDetail {
    private String reasonCode;
    private String reasonName;

    public ReasonConfigDetail() {
    }

    public ReasonConfigDetail(ApParam ap) {
        this.reasonCode = ap.getParamCode();
        this.reasonName = ap.getParamValue();
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }
    
}
