/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

/**
 *
 * @author duyetdk
 */
public class ReasonDetail extends Reason {
   
    private Double deposit;

    public ReasonDetail() {
    }
    
    public ReasonDetail(Reason re,MapActiveInfo ma) {
        if(re != null){
            this.reasonId = re.getReasonId();
            this.code = re.getCode();
            this.name = re.getName();
            this.status = re.getStatus();
            this.serTransId = re.getSerTransId();
            this.createTrans = re.getCreateTrans();
            this.regSpecial = re.getRegSpecial();
            this.reqProfile = re.getReqProfile();
            this.regTypeReason = re.getRegTypeReason();
            this.telService = re.getTelService();
            this.limitNumberIsdn = re.getLimitNumberIsdn();
            this.limitNumberUser = re.getLimitNumberUser();
            this.payAdvAmount = re.getPayAdvAmount();
            this.description = re.getDescription();
            this.reasonGroup = re.getReasonGroup();
            this.promotion = re.getPromotion();
        }
        if(ma != null){
            this.deposit = ma.getDeposit();
        }
    }

    public Double getDeposit() {
        return deposit;
    }

    public void setDeposit(Double deposit) {
        this.deposit = deposit;
    }
    
}
