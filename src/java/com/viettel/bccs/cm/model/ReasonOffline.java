package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "REASON_OFFLINE")
public class ReasonOffline implements Serializable {

    @Id
    @Column(name = "ID", length = 10)
    private Long id;
    @Column(name = "REASON_ID", length = 30)
    private Long reasonId;
    @Column(name = "TYPE", length = 20)
    private String type;
    @Column(name = "CODE", length = 100)
    private String code;
    @Column(name = "NAME", length = 200)
    private String name;
    @Column(name = "STATUS", length = 3)
    private Long status;
    @Column(name = "N_NUMBER")
    private Long nNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getnNumber() {
        return nNumber;
    }

    public void setnNumber(Long nNumber) {
        this.nNumber = nNumber;
    }
}
