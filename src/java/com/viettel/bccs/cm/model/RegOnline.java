/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "REG_ONLINE")
public class RegOnline implements Serializable {

    @Id
    @Column(name = "ID", length = 10, precision = 10, scale = 0)
    protected Long id;
    @Column(name = "CUSTOMER_NAME", length = 100, nullable = false)
    protected String customerName;
    @Column(name = "CUSTOMER_PHONE", length = 50)
    protected String customerPhone;
    @Column(name = "CUSTOMER_MAIL", length = 100)
    protected String customerMail;
    @Column(name = "PROVINCE", length = 50)
    protected String province;
    @Column(name = "ADDRESS", length = 100)
    protected String address;
    @Column(name = "IP", length = 50)
    protected String ip;
    @Column(name = "SPEED", length = 50)
    protected String speed;
    @Column(name = "PACKAGE_DETAIL", length = 200)
    protected String packageDetail;
    @Column(name = "CODE", length = 100)
    protected String code;
    @Column(name = "ASSIGN_SHOP", length = 50)
    protected String assignShop;
    @Column(name = "ASSIGN_STAFF", length = 50)
    protected String assignStaff;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date createDate;
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date updateDate;
    @Column(name = "UPDATE_USER", length = 50)
    protected String updateUser;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    protected Long status;
    @Column(name = "TYPE", length = 1, precision = 1, scale = 0)
    protected Long type;
    @Column(name = "DESCRIPTION", length = 200)
    protected String description;
    @Column(name = "REASON", length = 200)
    protected String reason;
    @Column(name = "ACCOUNT", length = 200)
    protected String account;
    @Column(name = "SUB_STATUS", length = 1, precision = 1, scale = 0)
    private Long subStatus;
    @Column(name = "APPROVE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date approveDate;
    @Column(name = "APPROVE_USER", length = 50)
    private String approveUser;
    @Column(name = "REQ_CANCEL_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reqCancelDate;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the customerPhone
     */
    public String getCustomerPhone() {
        return customerPhone;
    }

    /**
     * @param customerPhone the customerPhone to set
     */
    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    /**
     * @return the customerMail
     */
    public String getCustomerMail() {
        return customerMail;
    }

    /**
     * @param customerMail the customerMail to set
     */
    public void setCustomerMail(String customerMail) {
        this.customerMail = customerMail;
    }

    /**
     * @return the province
     */
    public String getProvince() {
        return province;
    }

    /**
     * @param province the province to set
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the speed
     */
    public String getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(String speed) {
        this.speed = speed;
    }

    /**
     * @return the packageDetail
     */
    public String getPackageDetail() {
        return packageDetail;
    }

    /**
     * @param packageDetail the packageDetail to set
     */
    public void setPackageDetail(String packageDetail) {
        this.packageDetail = packageDetail;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the assignShop
     */
    public String getAssignShop() {
        return assignShop;
    }

    /**
     * @param assignShop the assignShop to set
     */
    public void setAssignShop(String assignShop) {
        this.assignShop = assignShop;
    }

    /**
     * @return the assignStaff
     */
    public String getAssignStaff() {
        return assignStaff;
    }

    /**
     * @param assignStaff the assignStaff to set
     */
    public void setAssignStaff(String assignStaff) {
        this.assignStaff = assignStaff;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the updateUser
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * @param updateUser the updateUser to set
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public RegOnline() {
    }

    /**
     * @return the type
     */
    public Long getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Long type) {
        this.type = type;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * @return the subStatus
     */
    public Long getSubStatus() {
        return subStatus;
    }

    /**
     * @param subStatus the subStatus to set
     */
    public void setSubStatus(Long subStatus) {
        this.subStatus = subStatus;
    }

    /**
     * @return the approveDate
     */
    public Date getApproveDate() {
        return approveDate;
    }

    /**
     * @param approveDate the approveDate to set
     */
    public void setApproveDate(Date approveDate) {
        this.approveDate = approveDate;
    }

    /**
     * @return the approveUser
     */
    public String getApproveUser() {
        return approveUser;
    }

    /**
     * @param approveUser the approveUser to set
     */
    public void setApproveUser(String approveUser) {
        this.approveUser = approveUser;
    }

    /**
     * @return the reqCancelDate
     */
    public Date getReqCancelDate() {
        return reqCancelDate;
    }

    /**
     * @param reqCancelDate the reqCancelDate to set
     */
    public void setReqCancelDate(Date reqCancelDate) {
        this.reqCancelDate = reqCancelDate;
    }
}
