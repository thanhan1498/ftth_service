package com.viettel.bccs.cm.model;

/**
 * @author vanghv1
 */
public class ReqSubscriber {

    protected Long reqId;

    public Long getReqId() {
        return reqId;
    }

    public void setReqId(Long reqId) {
        this.reqId = reqId;
    }
}
