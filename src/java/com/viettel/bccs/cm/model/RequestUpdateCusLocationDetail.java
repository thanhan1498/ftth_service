/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.util.Date;

/**
 *
 * @author duyetdk
 */
public class RequestUpdateCusLocationDetail {
    private Long rulId;
    private Long cusId;
    private String cusName;
    private String address;
    private Long staffId;
    private String xLocation;
    private String yLocation;
    private String cxLocation;
    private String cyLocation;
    private Long status;
    private String description;
    private String createUser;
    private Date createDate;
    private String lastUpdateUser;
    private Date lastUpdateDate;
    private Long countApproved;

    public RequestUpdateCusLocationDetail() {
    }

    public RequestUpdateCusLocationDetail(RequestUpdateLocation obj) {
        if(obj != null){
        this.rulId = obj.getRulId();
        this.cusId = obj.getCusId();
        this.cusName = obj.getCusName();
        this.address = obj.getAddress();
        this.staffId = obj.getStaffId();
        this.xLocation = obj.getxLocation();
        this.yLocation = obj.getyLocation();
        this.cxLocation = obj.getCxLocation();
        this.cyLocation = obj.getCyLocation();
        this.status = obj.getStatus();
        this.description = obj.getDescription();
        this.createUser = obj.getCreateUser();
        this.createDate = obj.getCreateDate();
        this.lastUpdateUser = obj.getLastUpdateUser();
        this.lastUpdateDate = obj.getLastUpdateDate();
        this.countApproved = obj.getCountApproved();
        }
    }

    public Long getRulId() {
        return rulId;
    }

    public void setRulId(Long rulId) {
        this.rulId = rulId;
    }

    public Long getCusId() {
        return cusId;
    }

    public void setCusId(Long cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getxLocation() {
        return xLocation;
    }

    public void setxLocation(String xLocation) {
        this.xLocation = xLocation;
    }

    public String getyLocation() {
        return yLocation;
    }

    public void setyLocation(String yLocation) {
        this.yLocation = yLocation;
    }

    public String getCxLocation() {
        return cxLocation;
    }

    public void setCxLocation(String cxLocation) {
        this.cxLocation = cxLocation;
    }

    public String getCyLocation() {
        return cyLocation;
    }

    public void setCyLocation(String cyLocation) {
        this.cyLocation = cyLocation;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Long getCountApproved() {
        return countApproved;
    }

    public void setCountApproved(Long countApproved) {
        this.countApproved = countApproved;
    }
        
}
