package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author do khac duyet
 */
@Entity
@Table(name = "REQUEST_UPDATE_LOCATION")
public class RequestUpdateLocation implements Serializable {

    @Id
    @Column(name = "RUL_ID", length = 10, precision = 10, scale = 0)
    private Long rulId;
    @Column(name = "CUS_ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long cusId;
    @Column(name = "CUS_NAME", length = 200)
    private String cusName;
    @Column(name = "ADDRESS", length = 500)
    private String address;
    @Column(name = "STAFF_ID", length = 10)
    private Long staffId;
    @Column(name = "LATITUDE", length = 50)
    private String xLocation;
    @Column(name = "LONGITUDE", length = 50)
    private String yLocation;
    @Column(name = "CURRENT_LATITUDE", length = 50)
    private String cxLocation;
    @Column(name = "CURRENT_LONGITUDE", length = 50)
    private String cyLocation;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "DESCRIPTION", length = 500)
    private String description;
    @Column(name = "CREATE_USER", length = 100)
    private String createUser;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "LAST_UPDATE_USER", length = 100)
    private String lastUpdateUser;
    @Column(name = "LAST_UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdateDate;
    @Column(name = "SHOP_PATH", length = 100)
    private String shopPath;
    @Column(name = "PROVINCE", length = 20)
    private String province;
    @Column(name = "STAFF_OWN_TYPE", length = 1)
    private String staffOwnType;
    @Column(name = "SHOP_ID", length = 10)
    private Long shopId;
    @Column(name = "COUNT_APPROVED", length = 5)
    private Long countApproved;
    @Column(name = "REASON_REQUEST", length = 2000)
    private String reasonRequest;
    @Column(name = "CUST_IMAGE_ID", length = 10, precision = 10, scale = 0)
    private Long custImageId;
    @Column(name = "ACCOUNT", length = 20)
    private String account;

    public RequestUpdateLocation() {
    }

    public RequestUpdateLocation(Long rulId) {
        this.rulId = rulId;
    }

    public Long getRulId() {
        return rulId;
    }

    public void setRulId(Long rulId) {
        this.rulId = rulId;
    }

    public Long getCusId() {
        return cusId;
    }

    public void setCusId(Long cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getShopPath() {
        return shopPath;
    }

    public void setShopPath(String shopPath) {
        this.shopPath = shopPath;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getxLocation() {
        return xLocation;
    }

    public void setxLocation(String xLocation) {
        this.xLocation = xLocation;
    }

    public String getyLocation() {
        return yLocation;
    }

    public void setyLocation(String yLocation) {
        this.yLocation = yLocation;
    }

    public String getCxLocation() {
        return cxLocation;
    }

    public void setCxLocation(String cxLocation) {
        this.cxLocation = cxLocation;
    }

    public String getCyLocation() {
        return cyLocation;
    }

    public void setCyLocation(String cyLocation) {
        this.cyLocation = cyLocation;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getStaffOwnType() {
        return staffOwnType;
    }

    public void setStaffOwnType(String staffOwnType) {
        this.staffOwnType = staffOwnType;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getCountApproved() {
        return countApproved;
    }

    public void setCountApproved(Long countApproved) {
        this.countApproved = countApproved;
    }

    /**
     * @return the reasonRequest
     */
    public String getReasonRequest() {
        return reasonRequest;
    }

    /**
     * @param reasonRequest the reasonRequest to set
     */
    public void setReasonRequest(String reasonRequest) {
        this.reasonRequest = reasonRequest;
    }

    /**
     * @return the custImageId
     */
    public Long getCustImageId() {
        return custImageId;
    }

    /**
     * @param custImageId the custImageId to set
     */
    public void setCustImageId(Long custImageId) {
        this.custImageId = custImageId;
    }

    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }
}
