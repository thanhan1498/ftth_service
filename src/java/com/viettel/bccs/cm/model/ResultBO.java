/*
* Copyright (C) Jun 26, 2012 Viettel Telecom. All rights reserved.
* VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*/
package com.viettel.bccs.cm.model;

/**
*
* @author kdvt_tula4
* @version 1.0
* @ Jun 26, 2012
*/
public class ResultBO {

    private String message;
    private Long resultCode;
    private Object resultObject;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getResultCode() {
        return resultCode;
    }

    public void setResultCode(Long resultCode) {
        this.resultCode = resultCode;
    }

    public Object getResultObject() {
        return resultObject;
    }

    public void setResultObject(Object resultObject) {
        this.resultObject = resultObject;
    }

}
