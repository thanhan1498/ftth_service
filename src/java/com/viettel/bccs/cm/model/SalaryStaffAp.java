/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "Salary_Staff_AP")
public class SalaryStaffAp implements Serializable {

    @Id
    @Column(name = "ID", length = 22, precision = 22, scale = 0)
    private Long id;
    @Column(name = "STAFF_CODE", nullable = false, length = 50)
    private String staffCode;
    @Column(name = "ACC_EMONEY", nullable = false, length = 20)
    private String accEmoney;
    @Column(name = "TYPE", nullable = false, length = 20)
    private String type;
    @Column(name = "HR_CODE", nullable = false, length = 50)
    private String hrCode;
    @Column(name = "STAFF_NAME", nullable = false, length = 50)
    private String staffName;
    @Column(name = "SALARY_MONTH")
    private Date salaryMonth;
    @Column(name = "STATUS", length = 10, precision = 10, scale = 0)
    private Long status;
    @Column(name = "START_WORKING")
    private Date startWorking;
    @Column(name = "DATE_OF_MONTH", length = 10, precision = 10, scale = 0)
    private Long dateOfMonth;
    @Column(name = "DATE_OF_WORK", length = 10, precision = 10, scale = 0)
    private Long dateOfWork;
    @Column(name = "KI", nullable = false, length = 5)
    private String ki;
    @Column(name = "KI_VALUE", length = 22, precision = 22, scale = 5)
    private Double kiValue;
    @Column(name = "TOTAL_CONTRACT", length = 10, precision = 10, scale = 0)
    private Long totalContract;
    @Column(name = "SALARY_LEVEL", length = 22, precision = 22, scale = 5)
    private Double salaryLevel;
    @Column(name = "TOTAL", length = 22, precision = 22, scale = 5)
    private Double total;
    @Column(name = "CREATE_DATE")
    private Date createDate;
    @Column(name = "UPDATE_DATE")
    private Date updateDate;
    @Column(name = "CREATE_USER", nullable = false, length = 50)
    private String createUser;
    @Column(name = "UPDATE_USER", nullable = false, length = 50)
    private String updateUser;
    @Column(name = "PROVINCE", nullable = false, length = 50)
    private String province;
    @Column(name = "MONTH_OF_WORK", length = 10, precision = 10, scale = 0)
    private Long monthOfWork;
    @Column(name = "BASIC_SALARY", length = 22, precision = 22, scale = 5)
    private Double basicSalary;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the staffCode
     */
    public String getStaffCode() {
        return staffCode;
    }

    /**
     * @param staffCode the staffCode to set
     */
    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    /**
     * @return the accEmoney
     */
    public String getAccEmoney() {
        return accEmoney;
    }

    /**
     * @param accEmoney the accEmoney to set
     */
    public void setAccEmoney(String accEmoney) {
        this.accEmoney = accEmoney;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the hrCode
     */
    public String getHrCode() {
        return hrCode;
    }

    /**
     * @param hrCode the hrCode to set
     */
    public void setHrCode(String hrCode) {
        this.hrCode = hrCode;
    }

    /**
     * @return the staffName
     */
    public String getStaffName() {
        return staffName;
    }

    /**
     * @param staffName the staffName to set
     */
    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    /**
     * @return the salaryMonth
     */
    public Date getSalaryMonth() {
        return salaryMonth;
    }

    /**
     * @param salaryMonth the salaryMonth to set
     */
    public void setSalaryMonth(Date salaryMonth) {
        this.salaryMonth = salaryMonth;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the startWorking
     */
    public Date getStartWorking() {
        return startWorking;
    }

    /**
     * @param startWorking the startWorking to set
     */
    public void setStartWorking(Date startWorking) {
        this.startWorking = startWorking;
    }

    /**
     * @return the dateOfMonth
     */
    public Long getDateOfMonth() {
        return dateOfMonth;
    }

    /**
     * @param dateOfMonth the dateOfMonth to set
     */
    public void setDateOfMonth(Long dateOfMonth) {
        this.dateOfMonth = dateOfMonth;
    }

    /**
     * @return the dateOfWork
     */
    public Long getDateOfWork() {
        return dateOfWork;
    }

    /**
     * @param dateOfWork the dateOfWork to set
     */
    public void setDateOfWork(Long dateOfWork) {
        this.dateOfWork = dateOfWork;
    }

    /**
     * @return the ki
     */
    public String getKi() {
        return ki;
    }

    /**
     * @param ki the ki to set
     */
    public void setKi(String ki) {
        this.ki = ki;
    }

    /**
     * @return the kiValue
     */
    public Double getKiValue() {
        return kiValue;
    }

    /**
     * @param kiValue the kiValue to set
     */
    public void setKiValue(Double kiValue) {
        this.kiValue = kiValue;
    }

    /**
     * @return the totalContract
     */
    public Long getTotalContract() {
        return totalContract;
    }

    /**
     * @param totalContract the totalContract to set
     */
    public void setTotalContract(Long totalContract) {
        this.totalContract = totalContract;
    }

    /**
     * @return the salaryLevel
     */
    public Double getSalaryLevel() {
        return salaryLevel;
    }

    /**
     * @param salaryLevel the salaryLevel to set
     */
    public void setSalaryLevel(Double salaryLevel) {
        this.salaryLevel = salaryLevel;
    }

    /**
     * @return the total
     */
    public Double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(Double total) {
        this.total = total;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the updateUser
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * @param updateUser the updateUser to set
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * @return the province
     */
    public String getProvince() {
        return province;
    }

    /**
     * @param province the province to set
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     * @return the monthOfWork
     */
    public Long getMonthOfWork() {
        return monthOfWork;
    }

    /**
     * @param monthOfWork the monthOfWork to set
     */
    public void setMonthOfWork(Long monthOfWork) {
        this.monthOfWork = monthOfWork;
    }

    /**
     * @return the basicSalary
     */
    public Double getBasicSalary() {
        return basicSalary;
    }

    /**
     * @param basicSalary the basicSalary to set
     */
    public void setBasicSalary(Double basicSalary) {
        this.basicSalary = basicSalary;
    }

}
