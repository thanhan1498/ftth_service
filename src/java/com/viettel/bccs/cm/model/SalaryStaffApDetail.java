/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "Salary_Staff_AP_detail")
public class SalaryStaffApDetail implements Serializable {

    @Id
    @Column(name = "ID", length = 22, precision = 22, scale = 0)
    private Long id;
    @Column(name = "SALARY_STAFF_AP_ID", length = 10, precision = 10, scale = 0)
    private Long salaryStaffApId;
    @Column(name = "TYPE", nullable = false, length = 50)
    private String type;
    @Column(name = "VALUE", nullable = false, length = 50)
    private String value;
    @Column(name = "MONEY", length = 22, precision = 22, scale = 5)
    private Double money;
    @Column(name = "CREATE_DATE")
    private Date createDate;
    @Column(name = "UPDATE_DATE")
    private Date updateDate;
    @Column(name = "DESCRIPTION", nullable = false, length = 100)
    private String description;

    public SalaryStaffApDetail() {
    }

    public SalaryStaffApDetail(Date date, String desc, Double money, Long salaryId, String type, String value) {
        createDate = date;
        description = desc;
        this.money = money;
        salaryStaffApId = salaryId;
        this.type = type;
        this.value = value;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the salaryStaffApId
     */
    public Long getSalaryStaffApId() {
        return salaryStaffApId;
    }

    /**
     * @param salaryStaffApId the salaryStaffApId to set
     */
    public void setSalaryStaffApId(Long salaryStaffApId) {
        this.salaryStaffApId = salaryStaffApId;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the money
     */
    public Double getMoney() {
        return money;
    }

    /**
     * @param money the money to set
     */
    public void setMoney(Double money) {
        this.money = money;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
