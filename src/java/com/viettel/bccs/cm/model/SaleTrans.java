package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author linhlh2
 */
@Entity
@Table(name = "SALE_TRANS")
@XmlRootElement
public class SaleTrans implements Serializable {

    @Id
    @Column(name = "SALE_TRANS_ID", length = 10, precision = 10, scale = 0)
    private Long saleTransId;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SALE_TRANS_DATE")
    private Date saleTransDate;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "SHOP_ID", length = 10, precision = 10, scale = 0)
    private Long shopId;
    @Column(name = "STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long staffId;
    @Column(name = "SALE_SERVICE_ID", length = 22, precision = 22, scale = 0)
    private Long saleServiceId;
    @Column(name = "SUB_ID", length = 22, precision = 22, scale = 0)
    private Long subId;
    @Column(name = "ISDN", length = 32)
    private String isdn;
    @Column(name = "ACTION_CODE", length = 30)
    private String actionCode;
    @Column(name = "REASON_ID", length = 10, precision = 10, scale = 0)
    private Long reasonId;
    @Column(name = "SALE_TRANS_TYPE", length = 2)
    private String saleTransType;
    @Column(name = "CHECK_STOCK", length = 2)
    private String checkStock;
    @Column(name = "INVOICE_USED_ID", length = 10, precision = 10, scale = 0)
    private Long invoiceUsedId;
    @Column(name = "PAY_METHOD", length = 2)
    private String payMethod;
    @Column(name = "SALE_SERVICE_PRICE_ID", length = 22, precision = 22, scale = 0)
    private Long saleServicePriceId;
    @Column(name = "AMOUNT_SERVICE", length = 22, precision = 22, scale = 0)
    private Long amountService;
    @Column(name = "AMOUNT_MODEL", length = 22, precision = 22, scale = 0)
    private Long amountModel;
    @Column(name = "DISCOUNT", length = 22, precision = 22, scale = 0)
    private Long discount;
    @Column(name = "PROMOTION", length = 20, precision = 20, scale = 4)
    private Double promotion;
    @Column(name = "AMOUNT_TAX", length = 22, precision = 22, scale = 0)
    private Long amountTax;
    @Column(name = "AMOUNT_NOT_TAX", length = 22, precision = 22, scale = 0)
    private Long amountNotTax;
    @Column(name = "VAT", length = 22, precision = 22, scale = 0)
    private Long vat;
    @Column(name = "TAX", length = 22, precision = 22, scale = 0)
    private Long tax;
    @Column(name = "CUST_NAME", length = 150)
    private String custName;
    @Column(name = "CONTRACT_NO", length = 50)
    private String contractNo;
    @Column(name = "TEL_NUMBER", length = 12)
    private String telNumber;
    @Column(name = "COMPANY", length = 100)
    private String company;
    @Column(name = "ADDRESS", length = 200)
    private String address;
    @Column(name = "TIN", length = 100)
    private String tin;
    @Column(name = "NOTE", length = 200)
    private String note;
    @Column(name = "DESTROY_USER", length = 20)
    private String destroyUser;
    @Column(name = "APPROVER_USER", length = 20)
    private String approverUser;
    @Column(name = "TELECOM_SERVICE_ID", length = 22, precision = 22, scale = 0)
    private Long telecomServiceId;
    @Column(name = "TRANSFER_GOODS", length = 1)
    private String transferGoods;
    @Column(name = "SALE_TRANS_CODE", length = 30)
    private String saleTransCode;
    @Column(name = "STOCK_TRANS_ID", length = 10, precision = 10, scale = 0)
    private Long stockTransId;
    @Column(name = "CREATE_STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long createStaffId;
    @Column(name = "SHOP_CODE", length = 10, precision = 10, scale = 0)
    private String shopCode;
    @Column(name = "TRANS_RESULT", nullable = false, length = 1, precision = 1, scale = 0)
    private Long transResult;
    @Column(name = "SERIAL", length = 30)
    private String serial;

    public Long getSaleTransId() {
        return saleTransId;
    }

    public void setSaleTransId(Long saleTransId) {
        this.saleTransId = saleTransId;
    }

    public Date getSaleTransDate() {
        return saleTransDate;
    }

    public void setSaleTransDate(Date saleTransDate) {
        this.saleTransDate = saleTransDate;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Long getSaleServiceId() {
        return saleServiceId;
    }

    public void setSaleServiceId(Long saleServiceId) {
        this.saleServiceId = saleServiceId;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public String getSaleTransType() {
        return saleTransType;
    }

    public void setSaleTransType(String saleTransType) {
        this.saleTransType = saleTransType;
    }

    public String getCheckStock() {
        return checkStock;
    }

    public void setCheckStock(String checkStock) {
        this.checkStock = checkStock;
    }

    public Long getInvoiceUsedId() {
        return invoiceUsedId;
    }

    public void setInvoiceUsedId(Long invoiceUsedId) {
        this.invoiceUsedId = invoiceUsedId;
    }

    public String getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(String payMethod) {
        this.payMethod = payMethod;
    }

    public Long getSaleServicePriceId() {
        return saleServicePriceId;
    }

    public void setSaleServicePriceId(Long saleServicePriceId) {
        this.saleServicePriceId = saleServicePriceId;
    }

    public Long getAmountService() {
        return amountService;
    }

    public void setAmountService(Long amountService) {
        this.amountService = amountService;
    }

    public Long getAmountModel() {
        return amountModel;
    }

    public void setAmountModel(Long amountModel) {
        this.amountModel = amountModel;
    }

    public Long getDiscount() {
        return discount;
    }

    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    public Double getPromotion() {
        return promotion;
    }

    public void setPromotion(Double promotion) {
        this.promotion = promotion;
    }

    public Long getAmountTax() {
        return amountTax;
    }

    public void setAmountTax(Long amountTax) {
        this.amountTax = amountTax;
    }

    public Long getAmountNotTax() {
        return amountNotTax;
    }

    public void setAmountNotTax(Long amountNotTax) {
        this.amountNotTax = amountNotTax;
    }

    public Long getVat() {
        return vat;
    }

    public void setVat(Long vat) {
        this.vat = vat;
    }

    public Long getTax() {
        return tax;
    }

    public void setTax(Long tax) {
        this.tax = tax;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDestroyUser() {
        return destroyUser;
    }

    public void setDestroyUser(String destroyUser) {
        this.destroyUser = destroyUser;
    }

    public String getApproverUser() {
        return approverUser;
    }

    public void setApproverUser(String approverUser) {
        this.approverUser = approverUser;
    }

    public Long getTelecomServiceId() {
        return telecomServiceId;
    }

    public void setTelecomServiceId(Long telecomServiceId) {
        this.telecomServiceId = telecomServiceId;
    }

    public String getTransferGoods() {
        return transferGoods;
    }

    public void setTransferGoods(String transferGoods) {
        this.transferGoods = transferGoods;
    }

    public String getSaleTransCode() {
        return saleTransCode;
    }

    public void setSaleTransCode(String saleTransCode) {
        this.saleTransCode = saleTransCode;
    }

    public Long getStockTransId() {
        return stockTransId;
    }

    public void setStockTransId(Long stockTransId) {
        this.stockTransId = stockTransId;
    }

    public Long getCreateStaffId() {
        return createStaffId;
    }

    public void setCreateStaffId(Long createStaffId) {
        this.createStaffId = createStaffId;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public Long getTransResult() {
        return transResult;
    }

    public void setTransResult(Long transResult) {
        this.transResult = transResult;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }
}
