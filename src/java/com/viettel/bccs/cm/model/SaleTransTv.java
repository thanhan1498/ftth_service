/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "SALE_TRANS_TV")
@XmlRootElement
public class SaleTransTv implements Serializable {

    @Id
    @Column(name = "SALE_TRANS_ID", length = 10, precision = 10, scale = 0)
    private Long saleTransId;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SALE_TRANS_DATE")
    private Date saleTransDate;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "SHOP_ID", length = 10, precision = 10, scale = 0)
    private Long shopId;
    @Column(name = "STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long staffId;
    @Column(name = "STAFF_CODE", length = 50)
    private String staffCode;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "ISDN", length = 50)
    private String isdn;
    @Column(name = "CONTRACT_NO", length = 50)
    private String contractNo;
    @Column(name = "REASON_ID", length = 10, precision = 10, scale = 0)
    private Long reasonId;
    @Column(name = "sale_service_code", length = 50)
    private String saleServiceCode;
    @Column(name = "action", length = 50)
    private String action;
    @Column(name = "FEE", length = 22, precision = 22, scale = 3)
    private Double fee;
    @Column(name = "deploy_fee", length = 22, precision = 22, scale = 3)
    private Double deployFee;
    @Column(name = "modem_fee", length = 22, precision = 22, scale = 3)
    private Double modemFee;
    @Column(name = "total", length = 22, precision = 22, scale = 3)
    private Double total;

    /**
     * @return the saleTransId
     */
    public Long getSaleTransId() {
        return saleTransId;
    }

    /**
     * @param saleTransId the saleTransId to set
     */
    public void setSaleTransId(Long saleTransId) {
        this.saleTransId = saleTransId;
    }

    /**
     * @return the saleTransDate
     */
    public Date getSaleTransDate() {
        return saleTransDate;
    }

    /**
     * @param saleTransDate the saleTransDate to set
     */
    public void setSaleTransDate(Date saleTransDate) {
        this.saleTransDate = saleTransDate;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the shopId
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * @param shopId the shopId to set
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * @return the staffId
     */
    public Long getStaffId() {
        return staffId;
    }

    /**
     * @param staffId the staffId to set
     */
    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    /**
     * @return the staffCode
     */
    public String getStaffCode() {
        return staffCode;
    }

    /**
     * @param staffCode the staffCode to set
     */
    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    /**
     * @return the subId
     */
    public Long getSubId() {
        return subId;
    }

    /**
     * @param subId the subId to set
     */
    public void setSubId(Long subId) {
        this.subId = subId;
    }

    /**
     * @return the isdn
     */
    public String getIsdn() {
        return isdn;
    }

    /**
     * @param isdn the isdn to set
     */
    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    /**
     * @return the contractNo
     */
    public String getContractNo() {
        return contractNo;
    }

    /**
     * @param contractNo the contractNo to set
     */
    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    /**
     * @return the reasonId
     */
    public Long getReasonId() {
        return reasonId;
    }

    /**
     * @param reasonId the reasonId to set
     */
    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    /**
     * @return the saleServiceCode
     */
    public String getSaleServiceCode() {
        return saleServiceCode;
    }

    /**
     * @param saleServiceCode the saleServiceCode to set
     */
    public void setSaleServiceCode(String saleServiceCode) {
        this.saleServiceCode = saleServiceCode;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * @return the fee
     */
    public Double getFee() {
        return fee;
    }

    /**
     * @param fee the fee to set
     */
    public void setFee(Double fee) {
        this.fee = fee;
    }

    /**
     * @return the deployFee
     */
    public Double getDeployFee() {
        return deployFee;
    }

    /**
     * @param deployFee the deployFee to set
     */
    public void setDeployFee(Double deployFee) {
        this.deployFee = deployFee;
    }

    /**
     * @return the modemFee
     */
    public Double getModemFee() {
        return modemFee;
    }

    /**
     * @param modemFee the modemFee to set
     */
    public void setModemFee(Double modemFee) {
        this.modemFee = modemFee;
    }

    /**
     * @return the total
     */
    public Double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(Double total) {
        this.total = total;
    }

}
