package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author linhlh2
 */
@Entity
@Table(name = "SHOP")
public class Shop implements Serializable {

    @Id
    @Column(name = "SHOP_ID", length = 10, precision = 10, scale = 0)
    private Long shopId;
    @Column(name = "NAME", nullable = false, length = 50)
    private String name;
    @Column(name = "PARENT_SHOP_ID", length = 10, precision = 10, scale = 0)
    private Long parentShopId;
    @Column(name = "ACCOUNT", length = 40)
    private String account;
    @Column(name = "BANK_NAME", length = 150)
    private String bankName;
    @Column(name = "ADDRESS", length = 150)
    private String address;
    @Column(name = "TEL", length = 100)
    private String tel;
    @Column(name = "FAX", length = 100)
    private String fax;
    @Column(name = "SHOP_CODE", length = 40)
    private String shopCode;
    @Column(name = "SHOP_TYPE", length = 1)
    private String shopType;
    @Column(name = "CONTACT_NAME", length = 50)
    private String contactName;
    @Column(name = "CONTACT_TITLE", length = 30)
    private String contactTitle;
    @Column(name = "TEL_NUMBER", length = 20)
    private String telNumber;
    @Column(name = "EMAIL", length = 30)
    private String email;
    @Column(name = "DESCRIPTION", length = 100)
    private String description;
    @Column(name = "PROVINCE", length = 4)
    private String province;
    @Column(name = "PAR_SHOP_CODE", length = 10)
    private String parShopCode;
    @Column(name = "CENTER_CODE", length = 1)
    private String centerCode;
    @Column(name = "OLD_SHOP_CODE", length = 40)
    private String oldShopCode;
    @Column(name = "COMPANY", length = 4000)
    private String company;
    @Column(name = "TIN", length = 50)
    private String tin;
//    @Column(name = "SHOP", length = 20)
//    private String shop;
    @Column(name = "PROVINCE_CODE", length = 20)
    private String provinceCode;
    @Column(name = "PAY_COMM", length = 1)
    private String payComm;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CHANNEL_TYPE_ID", length = 22, precision = 22, scale = 0, nullable = false)
    private Long channelTypeId;
    @Column(name = "DISCOUNT_POLICY", length = 20)
    private String discountPolicy;
    @Column(name = "PRICE_POLICY", length = 20)
    private String pricePolicy;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "SHOP_PATH", length = 500)
    private String shopPath;

    public Shop() {
    }

    public Shop(Long shopId) {
        this.shopId = shopId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentShopId() {
        return parentShopId;
    }

    public void setParentShopId(Long parentShopId) {
        this.parentShopId = parentShopId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactTitle() {
        return contactTitle;
    }

    public void setContactTitle(String contactTitle) {
        this.contactTitle = contactTitle;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getParShopCode() {
        return parShopCode;
    }

    public void setParShopCode(String parShopCode) {
        this.parShopCode = parShopCode;
    }

    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public String getOldShopCode() {
        return oldShopCode;
    }

    public void setOldShopCode(String oldShopCode) {
        this.oldShopCode = oldShopCode;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

//    public String getShop() {
//        return shop;
//    }
//
//    public void setShop(String shop) {
//        this.shop = shop;
//    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getPayComm() {
        return payComm;
    }

    public void setPayComm(String payComm) {
        this.payComm = payComm;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getChannelTypeId() {
        return channelTypeId;
    }

    public void setChannelTypeId(Long channelTypeId) {
        this.channelTypeId = channelTypeId;
    }

    public String getDiscountPolicy() {
        return discountPolicy;
    }

    public void setDiscountPolicy(String discountPolicy) {
        this.discountPolicy = discountPolicy;
    }

    public String getPricePolicy() {
        return pricePolicy;
    }

    public void setPricePolicy(String pricePolicy) {
        this.pricePolicy = pricePolicy;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getShopPath() {
        return shopPath;
    }

    public void setShopPath(String shopPath) {
        this.shopPath = shopPath;
    }
}
