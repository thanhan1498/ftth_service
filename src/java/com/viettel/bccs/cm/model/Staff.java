package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author vietnn6
 */
@Entity
@Table(name = "STAFF")
public class Staff implements Serializable {

    @Id
    @Column(name = "STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long staffId;
    @Column(name = "SHOP_ID", length = 10, precision = 10, scale = 0, nullable = false)
    private Long shopId;
    @Column(name = "STAFF_CODE", length = 40, nullable = false)
    private String staffCode;
    @Column(name = "NAME", length = 50)
    private String name;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "BIRTHDAY")
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthday;
    @Column(name = "ID_NO", length = 20)
    private String idNo;
    @Column(name = "ID_ISSUE_PLACE", length = 100)
    private String idIssuePlace;
    @Column(name = "ID_ISSUE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date idIssueDate;
    @Column(name = "TEL", length = 15)
    private String tel;
    @Column(name = "TYPE", length = 1, precision = 1, scale = 0)
    private Long type;
    @Column(name = "SERIAL", length = 20)
    private String serial;
    @Column(name = "ISDN", length = 9)
    private String isdn;
    @Column(name = "PIN", length = 10)
    private String pin;
    @Column(name = "ADDRESS", length = 100)
    private String address;
    @Column(name = "PROVINCE", length = 5)
    private String province;
    @Column(name = "STAFF_OWN_TYPE", length = 1)
    private String staffOwnType;
    @Column(name = "STAFF_OWNER_ID", length = 10, precision = 10, scale = 0)
    private Long staffOwnerId;
    @Column(name = "CHANNEL_TYPE_ID", length = 22, precision = 22, scale = 0, nullable = false)
    private Long channelTypeId;
    @Column(name = "PRICE_POLICY", length = 20)
    private String pricePolicy;
    @Column(name = "DISCOUNT_POLICY", length = 20)
    private String discountPolicy;

    public Staff() {
    }

    public Staff(Long staffId) {
        this.staffId = staffId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getIdIssuePlace() {
        return idIssuePlace;
    }

    public void setIdIssuePlace(String idIssuePlace) {
        this.idIssuePlace = idIssuePlace;
    }

    public Date getIdIssueDate() {
        return idIssueDate;
    }

    public void setIdIssueDate(Date idIssueDate) {
        this.idIssueDate = idIssueDate;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getStaffOwnType() {
        return staffOwnType;
    }

    public void setStaffOwnType(String staffOwnType) {
        this.staffOwnType = staffOwnType;
    }

    public Long getStaffOwnerId() {
        return staffOwnerId;
    }

    public void setStaffOwnerId(Long staffOwnerId) {
        this.staffOwnerId = staffOwnerId;
    }

    public Long getChannelTypeId() {
        return channelTypeId;
    }

    public void setChannelTypeId(Long channelTypeId) {
        this.channelTypeId = channelTypeId;
    }

    public String getPricePolicy() {
        return pricePolicy;
    }

    public void setPricePolicy(String pricePolicy) {
        this.pricePolicy = pricePolicy;
    }

    public String getDiscountPolicy() {
        return discountPolicy;
    }

    public void setDiscountPolicy(String discountPolicy) {
        this.discountPolicy = discountPolicy;
    }
}
