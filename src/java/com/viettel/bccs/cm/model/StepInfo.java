package com.viettel.bccs.cm.model;

public class StepInfo {

    protected Long stepId;
    protected String currentStepName;
    protected String stepName;
    protected Long actionId;
    protected String actionName;
    protected String actionMapping;
    protected String className;
    protected String methodName;
    protected boolean isManualOnly;
    protected String message;
    protected Boolean cancelSubReq;
    protected Boolean signContract;
    protected Boolean cancelContract;
    protected Boolean activeSubscriber;
    protected Long contractId;

    public StepInfo() {
    }

    public StepInfo(String message) {
        this.message = message;
    }

    public StepInfo(Boolean cancelSubReq, Boolean signContract, Boolean cancelContract, Boolean activeSubscriber, Long contractId) {
        this.cancelSubReq = cancelSubReq;
        this.signContract = signContract;
        this.cancelContract = cancelContract;
        this.activeSubscriber = activeSubscriber;
        this.contractId = contractId;
    }

    public StepInfo(String message, Boolean cancelSubReq, Boolean signContract, Boolean cancelContract, Boolean activeSubscriber) {
        this.message = message;
        this.cancelSubReq = cancelSubReq;
        this.signContract = signContract;
        this.cancelContract = cancelContract;
        this.activeSubscriber = activeSubscriber;
    }

    public Long getStepId() {
        return stepId;
    }

    public void setStepId(Long stepId) {
        this.stepId = stepId;
    }

    public String getCurrentStepName() {
        return currentStepName;
    }

    public void setCurrentStepName(String currentStepName) {
        this.currentStepName = currentStepName;
    }

    public String getStepName() {
        return stepName;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    public Long getActionId() {
        return actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionMapping() {
        return actionMapping;
    }

    public void setActionMapping(String actionMapping) {
        this.actionMapping = actionMapping;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public boolean isIsManualOnly() {
        return isManualOnly;
    }

    public void setIsManualOnly(boolean isManualOnly) {
        this.isManualOnly = isManualOnly;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getCancelSubReq() {
        return cancelSubReq;
    }

    public void setCancelSubReq(Boolean cancelSubReq) {
        this.cancelSubReq = cancelSubReq;
    }

    public Boolean getSignContract() {
        return signContract;
    }

    public void setSignContract(Boolean signContract) {
        this.signContract = signContract;
    }

    public Boolean getCancelContract() {
        return cancelContract;
    }

    public void setCancelContract(Boolean cancelContract) {
        this.cancelContract = cancelContract;
    }

    public Boolean getActiveSubscriber() {
        return activeSubscriber;
    }

    public void setActiveSubscriber(Boolean activeSubscriber) {
        this.activeSubscriber = activeSubscriber;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }
}
