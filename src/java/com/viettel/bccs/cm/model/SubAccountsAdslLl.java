package com.viettel.bccs.cm.model;

import com.viettel.bccs.cm.model.pk.SubAccountsAdslLlId;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SUB_ACCOUNTS_ADSL_LL")
@IdClass(SubAccountsAdslLlId.class)
public class SubAccountsAdslLl implements Serializable {

    @Id
    private String account;
    @Id
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectFrom;
    @Id
    private String productCode;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0, nullable = false)
    private Long subId;
    @Column(name = "UNTIL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date until;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Date getUntil() {
        return until;
    }

    public void setUntil(Date until) {
        this.until = until;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
