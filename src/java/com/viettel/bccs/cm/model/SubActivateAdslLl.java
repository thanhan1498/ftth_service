package com.viettel.bccs.cm.model;

import com.viettel.bccs.cm.model.pk.SubActivateAdslLlId;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SUB_ACTIVATE_ADSL_LL")
@IdClass(SubActivateAdslLlId.class)
public class SubActivateAdslLl implements Serializable {

    @Id
    private Long subId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectFrom;
    @Column(name = "ACTION_CODE", length = 4, nullable = false)
    private String actionCode;
    @Column(name = "ACT_STATUS", length = 3, nullable = false)
    private String actStatus;
    @Column(name = "REASON_ID", length = 10, precision = 10, scale = 0)
    private Long reasonId;
    @Column(name = "UNTIL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date until;

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getActStatus() {
        return actStatus;
    }

    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public Date getUntil() {
        return until;
    }

    public void setUntil(Date until) {
        this.until = until;
    }
}
