package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SUB_ADSL_LL")
public class SubAdslLeaseline implements Serializable {

    @Id
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "CUST_REQ_ID", length = 10, precision = 10, scale = 0)
    private Long custReqId;
    @Column(name = "CONTRACT_ID", length = 10, precision = 10, scale = 0)
    private Long contractId;
    @Column(name = "IB", length = 1)
    private String ib;
    @Column(name = "CABLE_BOX_ID", length = 10, precision = 10, scale = 0)
    private Long cableBoxId;
    @Column(name = "BOARD_ID", length = 10, precision = 10, scale = 0)
    private Long boardId;
    @Column(name = "CAB_LEN", length = 10)
    private String cabLen;
    @Column(name = "PORT_NO", length = 10)
    private String portNo;
    @Column(name = "DSLAM_ID", length = 10, precision = 10, scale = 0)
    private Long dslamId;
    @Column(name = "TEAM_ID", length = 10, precision = 10, scale = 0)
    private Long teamId;
    @Column(name = "STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long staffId;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FIRST_CONNECT")
    private Date firstConnect;
    @Column(name = "DEPLOY_ADDRESS", length = 500)
    private String deployAddress;
    @Column(name = "DEPLOY_AREA_CODE", length = 50, nullable = false)
    private String deployAreaCode;
    @Column(name = "LINE_PHONE", length = 10)
    private String linePhone;
    @Column(name = "LINE_TYPE", length = 10, nullable = false)
    private String lineType;
    @Column(name = "SPEED", length = 10, precision = 10, scale = 0)
    private Long speed;
    @Column(name = "PASSWORD", length = 100)
    private String password;
    @Column(name = "ACCOUNT", length = 100)
    private String account;
    @Column(name = "ISDN", length = 20)
    private String isdn;
    @Column(name = "TASK_ID", length = 10, precision = 10, scale = 0)
    private Long taskId;
    @Column(name = "ADDRESS_CODE", length = 50, nullable = false)
    private String addressCode;
    @Column(name = "ADDRESS", length = 500)
    private String address;
    @Column(name = "DEPOSIT", length = 16, precision = 16, scale = 4)
    private Double deposit;
    @Column(name = "STATUS", nullable = false)
    private Long status;
    @Column(name = "USER_CREATED", length = 30, nullable = false)
    private String userCreated;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "VIP")
    private String vip;
    @Column(name = "ACT_STATUS", length = 3)
    private String actStatus;
    @Column(name = "STA_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date staDatetime;
    @Column(name = "END_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDatetime;
    @Column(name = "SHOP_CODE", length = 50)
    private String shopCode;
    @Column(name = "FINISH_REASON_ID", length = 10, precision = 10, scale = 0)
    private Long finishReasonId;
    @Column(name = "REG_REASON_ID", length = 10, precision = 10, scale = 0)
    private Long regReasonId;
    @Column(name = "PROVINCE", length = 10)
    private String province;
    @Column(name = "DISTRICT", length = 10)
    private String district;
    @Column(name = "PRECINCT", length = 10)
    private String precinct;
    @Column(name = "SUB_TYPE", length = 10, nullable = false)
    private String subType;
    @Column(name = "QUOTA", length = 22, precision = 22, scale = 0)
    private Long quota;
    @Column(name = "PROMOTION_CODE", length = 4)
    private String promotionCode;
    @Column(name = "REG_TYPE", length = 10)
    private String regType;
    @Column(name = "REASON_DEPOSIT_ID", length = 22, precision = 22, scale = 0)
    private Long reasonDepositId;
    @Column(name = "IP_STATIC", length = 15)
    private String ipStatic;
    @Column(name = "USER_USING", length = 200)
    private String userUsing;
    @Column(name = "USER_TITLE", length = 50)
    private String userTitle;
    @Column(name = "CHARSIC", length = 100)
    private String charsic;
    @Column(name = "SLOT_CARD", length = 100)
    private String slotCard;
    @Column(name = "PRODUCT_CODE", length = 10)
    private String productCode;
    @Column(name = "SERVICE_TYPE", length = 10)
    private String serviceType;
    @Column(name = "STREET_BLOCK", length = 5)
    private String streetBlock;
    @Column(name = "STREET_BLOCK_NAME", length = 50)
    private String streetBlockName;
    @Column(name = "STREET_NAME", length = 50)
    private String streetName;
    @Column(name = "HOME", length = 20)
    private String home;
    @Column(name = "CHANGE_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date changeDatetime;
    @Column(name = "PRICE_PLAN", length = 19, precision = 19, scale = 0)
    private Long pricePlan;
    @Column(name = "LOCAL_PRICE_PLAN", length = 19, precision = 19, scale = 0)
    private Long localPricePlan;
    @Column(name = "TEL_FAX", length = 15)
    private String telFax;
    @Column(name = "EMAIL", length = 100)
    private String email;
    @Column(name = "NICK_NAME", length = 50)
    private String nickName;
    @Column(name = "NICK_DOMAIN", length = 5)
    private String nickDomain;
    @Column(name = "TEL_MOBILE", length = 15)
    private String telMobile;
    @Column(name = "PROJECT", length = 10, precision = 10, scale = 0)
    private Long project;
    @Column(name = "IS_INFO_COMPLETED", length = 1, precision = 1, scale = 0)
    private Long isInfoCompleted;
    @Column(name = "STATION_ID", length = 10, precision = 10, scale = 0)
    private Long stationId;
    @Column(name = "AMOUNT_BOX", length = 25, precision = 10, scale = 15)
    private Double amountBox;
    @Column(name = "AMOUNT_CORE", length = 25, precision = 10, scale = 15)
    private Double amountCore;
    @Column(name = "AMOUNT_PRODUCT", length = 25, precision = 10, scale = 15)
    private Double amountProduct;
    @Column(name = "INFRA_TYPE", length = 10)
    private String infraType;
    @Column(name = "CUST_LAT")
    private Double custLat;
    @Column(name = "CUST_LONG")
    private Double custLong;
    @Column(name = "INFRA_LAT")
    private Double infraLat;
    @Column(name = "INFRA_LONG")
    private Double infraLong;
    @Column(name = "STAFF_ASSIGN", length = 10, precision = 10, scale = 0)
    private Long staffAssign;
    @Column(name = "ACC_BUNDLE_TV", length = 50)
    private String accBundleTv;
    @Column(name = "req_online_id", length = 10, precision = 10, scale = 0)
    private Long reqOnlineId;

    public String getInfraType() {
        return infraType;
    }

    public void setInfraType(String infraType) {
        this.infraType = infraType;
    }

    public SubAdslLeaseline() {
    }

    public SubAdslLeaseline(Long subId, Long custReqId, Long contractId, String ib, Long cableBoxId, Long boardId, String cabLen, String portNo, Long dslamId, Long teamId, Long staffId, Date firstConnect, String deployAddress, String deployAreaCode, String linePhone, String lineType, Long speed, String password, String account, String isdn, Long taskId, String addressCode, String address, Double deposit, Long status, String userCreated, Date createDate, String vip, String actStatus, Date staDatetime, Date endDatetime, String shopCode, Long finishReasonId, Long regReasonId, String province, String district, String precinct, String subType, Long quota, String promotionCode, String regType, Long reasonDepositId, String ipStatic, String userUsing, String userTitle, String charsic, String slotCard, String productCode, String serviceType, String streetBlock, String streetBlockName, String streetName, String home, Date changeDatetime, Long pricePlan, Long localPricePlan, String telFax, String email, String nickName, String nickDomain, String telMobile, Long project, Long isInfoCompleted, Long stationId) {
        this.subId = subId;
        this.custReqId = custReqId;
        this.contractId = contractId;
        this.ib = ib;
        this.cableBoxId = cableBoxId;
        this.boardId = boardId;
        this.cabLen = cabLen;
        this.portNo = portNo;
        this.dslamId = dslamId;
        this.teamId = teamId;
        this.staffId = staffId;
        this.firstConnect = firstConnect;
        this.deployAddress = deployAddress;
        this.deployAreaCode = deployAreaCode;
        this.linePhone = linePhone;
        this.lineType = lineType;
        this.speed = speed;
        this.password = password;
        this.account = account;
        this.isdn = isdn;
        this.taskId = taskId;
        this.addressCode = addressCode;
        this.address = address;
        this.deposit = deposit;
        this.status = status;
        this.userCreated = userCreated;
        this.createDate = createDate;
        this.vip = vip;
        this.actStatus = actStatus;
        this.staDatetime = staDatetime;
        this.endDatetime = endDatetime;
        this.shopCode = shopCode;
        this.finishReasonId = finishReasonId;
        this.regReasonId = regReasonId;
        this.province = province;
        this.district = district;
        this.precinct = precinct;
        this.subType = subType;
        this.quota = quota;
        this.promotionCode = promotionCode;
        this.regType = regType;
        this.reasonDepositId = reasonDepositId;
        this.ipStatic = ipStatic;
        this.userUsing = userUsing;
        this.userTitle = userTitle;
        this.charsic = charsic;
        this.slotCard = slotCard;
        this.productCode = productCode;
        this.serviceType = serviceType;
        this.streetBlock = streetBlock;
        this.streetBlockName = streetBlockName;
        this.streetName = streetName;
        this.home = home;
        this.changeDatetime = changeDatetime;
        this.pricePlan = pricePlan;
        this.localPricePlan = localPricePlan;
        this.telFax = telFax;
        this.email = email;
        this.nickName = nickName;
        this.nickDomain = nickDomain;
        this.telMobile = telMobile;
        this.project = project;
        this.isInfoCompleted = isInfoCompleted;
        this.stationId = stationId;
    }

    public SubAdslLeaseline(SubAdslLeaseline obj) {
        this.subId = obj.subId;
        this.custReqId = obj.custReqId;
        this.contractId = obj.contractId;
        this.ib = obj.ib;
        this.cableBoxId = obj.cableBoxId;
        this.boardId = obj.boardId;
        this.cabLen = obj.cabLen;
        this.portNo = obj.portNo;
        this.dslamId = obj.dslamId;
        this.teamId = obj.teamId;
        this.staffId = obj.staffId;
        this.firstConnect = obj.firstConnect;
        this.deployAddress = obj.deployAddress;
        this.deployAreaCode = obj.deployAreaCode;
        this.linePhone = obj.linePhone;
        this.lineType = obj.lineType;
        this.speed = obj.speed;
        this.password = obj.password;
        this.account = obj.account;
        this.isdn = obj.isdn;
        this.taskId = obj.taskId;
        this.addressCode = obj.addressCode;
        this.address = obj.address;
        this.deposit = obj.deposit;
        this.status = obj.status;
        this.userCreated = obj.userCreated;
        this.createDate = obj.createDate;
        this.vip = obj.vip;
        this.actStatus = obj.actStatus;
        this.staDatetime = obj.staDatetime;
        this.endDatetime = obj.endDatetime;
        this.shopCode = obj.shopCode;
        this.finishReasonId = obj.finishReasonId;
        this.regReasonId = obj.regReasonId;
        this.province = obj.province;
        this.district = obj.district;
        this.precinct = obj.precinct;
        this.subType = obj.subType;
        this.quota = obj.quota;
        this.promotionCode = obj.promotionCode;
        this.regType = obj.regType;
        this.reasonDepositId = obj.reasonDepositId;
        this.ipStatic = obj.ipStatic;
        this.userUsing = obj.userUsing;
        this.userTitle = obj.userTitle;
        this.charsic = obj.charsic;
        this.slotCard = obj.slotCard;
        this.productCode = obj.productCode;
        this.serviceType = obj.serviceType;
        this.streetBlock = obj.streetBlock;
        this.streetBlockName = obj.streetBlockName;
        this.streetName = obj.streetName;
        this.home = obj.home;
        this.changeDatetime = obj.changeDatetime;
        this.pricePlan = obj.pricePlan;
        this.localPricePlan = obj.localPricePlan;
        this.telFax = obj.telFax;
        this.email = obj.email;
        this.nickName = obj.nickName;
        this.nickDomain = obj.nickDomain;
        this.telMobile = obj.telMobile;
        this.project = obj.project;
        this.isInfoCompleted = obj.isInfoCompleted;
        this.stationId = obj.stationId;
    }

    public SubAdslLeaseline(SubReqAdslLl subReq) {
        if (subReq != null) {
            this.subId = subReq.getSubId();
            this.custReqId = subReq.getCustReqId();
            this.ib = subReq.getIb();
            this.cableBoxId = subReq.getCableBoxId();
            this.boardId = subReq.getBoardId();
            this.cabLen = subReq.getCabLen();
            this.portNo = subReq.getPortNo();
            this.dslamId = subReq.getDslamId();
            this.teamId = subReq.getTeamId();
            this.staffId = subReq.getStaffId();
            this.firstConnect = subReq.getFirstConnect();
            this.deployAddress = subReq.getDeployAddress();
            this.deployAreaCode = subReq.getDeployAreaCode();
            this.linePhone = subReq.getLinePhone();
            this.lineType = subReq.getLineType();
            this.speed = subReq.getSpeed();
            this.password = subReq.getPassword();
            this.account = subReq.getAccount();
            this.infraType = subReq.getInfraType();
            this.isdn = subReq.getIsdn();
            this.taskId = subReq.getTaskId();
            this.addressCode = subReq.getAddressCode();
            this.address = subReq.getAddress();
            this.deposit = subReq.getDeposit();
            this.status = subReq.getStatus();
            this.userCreated = subReq.getUserCreated();
            this.createDate = subReq.getCreateDate();
            this.vip = subReq.getVip();
            this.actStatus = subReq.getActStatus();
            this.staDatetime = subReq.getStaDatetime();
            this.endDatetime = subReq.getEndDatetime();
            this.shopCode = subReq.getShopCode();
            this.finishReasonId = subReq.getFinishReasonId();
            this.regReasonId = subReq.getRegReasonId();
            this.province = subReq.getProvince();
            this.district = subReq.getDistrict();
            this.precinct = subReq.getPrecinct();
            this.subType = subReq.getSubType();
            this.quota = subReq.getQuota();
            this.promotionCode = subReq.getPromotionCode();
            this.regType = subReq.getRegType();
            this.reasonDepositId = subReq.getReasonDepositId();
            this.ipStatic = subReq.getIpStatic();
            this.userUsing = subReq.getUserUsing();
            this.userTitle = subReq.getUserTitle();
            this.charsic = subReq.getCharsic();
            this.slotCard = subReq.getSlotCard();
            this.productCode = subReq.getProductCode();
            this.serviceType = subReq.getServiceType();
            this.streetBlock = subReq.getStreetBlock();
            this.streetBlockName = subReq.getStreetBlockName();
            this.streetName = subReq.getStreetName();
            this.home = subReq.getHome();
            this.pricePlan = subReq.getPricePlan();
            this.localPricePlan = subReq.getLocalPricePlan();
            this.telFax = subReq.getTelFax();
            this.email = subReq.getEmail();
            this.nickName = subReq.getNickName();
            this.nickDomain = subReq.getNickDomain();
            this.telMobile = subReq.getTelMobile();
            this.project = subReq.getProject();
            this.stationId = subReq.getStationId();
            this.custLat = subReq.getLat();
            this.custLong = subReq.getLng();
            this.infraLat = subReq.getInfraLat();
            this.infraLong = subReq.getInfraLong();
            this.accBundleTv = subReq.getAccBundleTv();
            this.reqOnlineId = subReq.getReqOnlineId();
        }
    }

    public Double getAmountBox() {
        return amountBox;
    }

    public void setAmountBox(Double amountBox) {
        this.amountBox = amountBox;
    }

    public Double getAmountCore() {
        return amountCore;
    }

    public void setAmountCore(Double amountCore) {
        this.amountCore = amountCore;
    }

    public Double getAmountProduct() {
        return amountProduct;
    }

    public void setAmountProduct(Double amountProduct) {
        this.amountProduct = amountProduct;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Long getCustReqId() {
        return custReqId;
    }

    public void setCustReqId(Long custReqId) {
        this.custReqId = custReqId;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public String getIb() {
        return ib;
    }

    public void setIb(String ib) {
        this.ib = ib;
    }

    public Long getCableBoxId() {
        return cableBoxId;
    }

    public void setCableBoxId(Long cableBoxId) {
        this.cableBoxId = cableBoxId;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getCabLen() {
        return cabLen;
    }

    public void setCabLen(String cabLen) {
        this.cabLen = cabLen;
    }

    public String getPortNo() {
        return portNo;
    }

    public void setPortNo(String portNo) {
        this.portNo = portNo;
    }

    public Long getDslamId() {
        return dslamId;
    }

    public void setDslamId(Long dslamId) {
        this.dslamId = dslamId;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {

        this.staffId = staffId;
    }

    public Date getFirstConnect() {
        return firstConnect;
    }

    public void setFirstConnect(Date firstConnect) {
        this.firstConnect = firstConnect;
    }

    public String getDeployAddress() {
        return deployAddress;
    }

    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    public String getDeployAreaCode() {
        return deployAreaCode;
    }

    public void setDeployAreaCode(String deployAreaCode) {
        this.deployAreaCode = deployAreaCode;
    }

    public String getLinePhone() {
        return linePhone;
    }

    public void setLinePhone(String linePhone) {
        this.linePhone = linePhone;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public Long getSpeed() {
        return speed;
    }

    public void setSpeed(Long speed) {
        this.speed = speed;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getAddressCode() {
        return addressCode;
    }

    public void setAddressCode(String addressCode) {
        this.addressCode = addressCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getDeposit() {
        return deposit;
    }

    public void setDeposit(Double deposit) {
        this.deposit = deposit;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public String getActStatus() {
        return actStatus;
    }

    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    public Date getStaDatetime() {
        return staDatetime;
    }

    public void setStaDatetime(Date staDatetime) {
        this.staDatetime = staDatetime;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public Long getFinishReasonId() {
        return finishReasonId;
    }

    public void setFinishReasonId(Long finishReasonId) {
        this.finishReasonId = finishReasonId;
    }

    public Long getRegReasonId() {
        return regReasonId;
    }

    public void setRegReasonId(Long regReasonId) {
        this.regReasonId = regReasonId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public Long getQuota() {
        return quota;
    }

    public void setQuota(Long quota) {
        this.quota = quota;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    public String getRegType() {
        return regType;
    }

    public void setRegType(String regType) {
        this.regType = regType;
    }

    public Long getReasonDepositId() {
        return reasonDepositId;
    }

    public void setReasonDepositId(Long reasonDepositId) {
        this.reasonDepositId = reasonDepositId;
    }

    public String getIpStatic() {
        return ipStatic;
    }

    public void setIpStatic(String ipStatic) {
        this.ipStatic = ipStatic;
    }

    public String getUserUsing() {
        return userUsing;
    }

    public void setUserUsing(String userUsing) {
        this.userUsing = userUsing;
    }

    public String getUserTitle() {
        return userTitle;
    }

    public void setUserTitle(String userTitle) {
        this.userTitle = userTitle;
    }

    public String getCharsic() {
        return charsic;
    }

    public void setCharsic(String charsic) {
        this.charsic = charsic;
    }

    public String getSlotCard() {
        return slotCard;
    }

    public void setSlotCard(String slotCard) {
        this.slotCard = slotCard;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getStreetBlock() {
        return streetBlock;
    }

    public void setStreetBlock(String streetBlock) {
        this.streetBlock = streetBlock;
    }

    public String getStreetBlockName() {
        return streetBlockName;
    }

    public void setStreetBlockName(String streetBlockName) {
        this.streetBlockName = streetBlockName;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public Date getChangeDatetime() {
        return changeDatetime;
    }

    public void setChangeDatetime(Date changeDatetime) {
        this.changeDatetime = changeDatetime;
    }

    public Long getPricePlan() {
        return pricePlan;
    }

    public void setPricePlan(Long pricePlan) {
        this.pricePlan = pricePlan;
    }

    public Long getLocalPricePlan() {
        return localPricePlan;
    }

    public void setLocalPricePlan(Long localPricePlan) {
        this.localPricePlan = localPricePlan;
    }

    public String getTelFax() {
        return telFax;
    }

    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickDomain() {
        return nickDomain;
    }

    public void setNickDomain(String nickDomain) {
        this.nickDomain = nickDomain;
    }

    public String getTelMobile() {
        return telMobile;
    }

    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }

    public Long getProject() {
        return project;
    }

    public void setProject(Long project) {
        this.project = project;
    }

    public Long getIsInfoCompleted() {
        return isInfoCompleted;
    }

    public void setIsInfoCompleted(Long isInfoCompleted) {
        this.isInfoCompleted = isInfoCompleted;
    }

    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    public Double getCustLat() {
        return custLat;
    }

    public void setCustLat(Double custLat) {
        this.custLat = custLat;
    }

    public Double getCustLong() {
        return custLong;
    }

    public void setCustLong(Double custLong) {
        this.custLong = custLong;
    }

    /**
     * @return the infraLat
     */
    public Double getInfraLat() {
        return infraLat;
    }

    /**
     * @param infraLat the infraLat to set
     */
    public void setInfraLat(Double infraLat) {
        this.infraLat = infraLat;
    }

    /**
     * @return the infraLong
     */
    public Double getInfraLong() {
        return infraLong;
    }

    /**
     * @param infraLong the infraLong to set
     */
    public void setInfraLong(Double infraLong) {
        this.infraLong = infraLong;
    }

    /**
     * @return the staffAssign
     */
    public Long getStaffAssign() {
        return staffAssign;
    }

    /**
     * @param staffAssign the staffAssign to set
     */
    public void setStaffAssign(Long staffAssign) {
        this.staffAssign = staffAssign;
    }

    /**
     * @return the accBundleTv
     */
    public String getAccBundleTv() {
        return accBundleTv;
    }

    /**
     * @param accBundleTv the accBundleTv to set
     */
    public void setAccBundleTv(String accBundleTv) {
        this.accBundleTv = accBundleTv;
    }

    /**
     * @return the reqOnlineId
     */
    public Long getReqOnlineId() {
        return reqOnlineId;
    }

    /**
     * @param reqOnlineId the reqOnlineId to set
     */
    public void setReqOnlineId(Long reqOnlineId) {
        this.reqOnlineId = reqOnlineId;
    }
}
