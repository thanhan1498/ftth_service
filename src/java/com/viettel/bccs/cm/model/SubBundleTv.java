/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "SUB_BUNDLE_TV")
public class SubBundleTv implements Serializable {

    @Id
    @Column(name = "ID", length = 10, precision = 10, scale = 0)
    @GeneratedValue(generator = "InvSeq")
    @SequenceGenerator(name = "InvSeq", sequenceName = "SUB_BUNDLE_TV_SEQ", allocationSize = 1)
    protected Long id;
    @Column(name = "ACCOUNT", length = 50)
    protected String account;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    protected Long subId;
    @Column(name = "STATUS", length = 2, precision = 2, scale = 0)
    protected Long status;
    @Column(name = "REG_REASON_ID", length = 10, precision = 10, scale = 0)
    protected Long regReasonId;
    @Column(name = "REG_REASON_CODE", length = 50)
    protected String regReasonCode;
    @Column(name = "PRODUCT_CODE", length = 100)
    protected String productCode;
    @Column(name = "BOX_SERIAL", length = 100)
    protected String boxSerial;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE")
    protected Date createDate;
    @Column(name = "CREATE_USER", length = 100)
    protected String createUser;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MODIFY_DATE")
    protected Date modifyDate;
    @Column(name = "MODIFY_USER", length = 100)
    protected String modifyUser;
    @Column(name = "DESCRIPTION", length = 100)
    protected String description;
    @Column(name = "user_unique_id", length = 100)
    protected String userUniqueId;
    @Column(name = "device_unique_id", length = 100)
    protected String deviceUniqueId;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FIRST_CONNECT")
    protected Date firstConnect;
    @Column(name = "ACT_STATUS", length = 5)
    protected String actStatus;
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * @return the subId
     */
    public Long getSubId() {
        return subId;
    }

    /**
     * @param subId the subId to set
     */
    public void setSubId(Long subId) {
        this.subId = subId;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the regReasonId
     */
    public Long getRegReasonId() {
        return regReasonId;
    }

    /**
     * @param regReasonId the regReasonId to set
     */
    public void setRegReasonId(Long regReasonId) {
        this.regReasonId = regReasonId;
    }

    /**
     * @return the regReasonCode
     */
    public String getRegReasonCode() {
        return regReasonCode;
    }

    /**
     * @param regReasonCode the regReasonCode to set
     */
    public void setRegReasonCode(String regReasonCode) {
        this.regReasonCode = regReasonCode;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the boxSerial
     */
    public String getBoxSerial() {
        return boxSerial;
    }

    /**
     * @param boxSerial the boxSerial to set
     */
    public void setBoxSerial(String boxSerial) {
        this.boxSerial = boxSerial;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the modifyDate
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * @param modifyDate the modifyDate to set
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * @return the modifyUser
     */
    public String getModifyUser() {
        return modifyUser;
    }

    /**
     * @param modifyUser the modifyUser to set
     */
    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the userUniqueId
     */
    public String getUserUniqueId() {
        return userUniqueId;
    }

    /**
     * @param userUniqueId the userUniqueId to set
     */
    public void setUserUniqueId(String userUniqueId) {
        this.userUniqueId = userUniqueId;
    }

    /**
     * @return the deviceUniqueId
     */
    public String getDeviceUniqueId() {
        return deviceUniqueId;
    }

    /**
     * @param deviceUniqueId the deviceUniqueId to set
     */
    public void setDeviceUniqueId(String deviceUniqueId) {
        this.deviceUniqueId = deviceUniqueId;
    }

    /**
     * @return the firstConnect
     */
    public Date getFirstConnect() {
        return firstConnect;
    }

    /**
     * @param firstConnect the firstConnect to set
     */
    public void setFirstConnect(Date firstConnect) {
        this.firstConnect = firstConnect;
    }

    /**
     * @return the actStatus
     */
    public String getActStatus() {
        return actStatus;
    }

    /**
     * @param actStatus the actStatus to set
     */
    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }
}
