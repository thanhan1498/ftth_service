package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "SUB_DEPLOYMENT")
public class SubDeployment implements Serializable {

    @Id
    @Column(name = "SUBDEPLOY_ID", length = 10, precision = 10, scale = 0)
    private Long subdeployId;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "ACTION_ID", length = 3)
    private String actionId;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "STATUS", length = 10, precision = 10, scale = 0)
    private Long status;
    @Column(name = "ISSUE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date issueDate;
    @Column(name = "FEE", length = 1)
    private String fee;
    @Column(name = "PLATE_A", length = 10, precision = 10, scale = 0)
    private Long plateA;
    @Column(name = "PLATE_B", length = 10, precision = 10, scale = 0)
    private Long plateB;
    @Column(name = "LINE_NO", length = 10, precision = 10, scale = 0)
    private Long lineNo;
    @Column(name = "PORT_NO", length = 10, precision = 10, scale = 0)
    private Long portNo;
    @Column(name = "CABLE_BOX_ID", length = 10, precision = 10, scale = 0)
    private Long cableBoxId;
    @Column(name = "STAGE_ID", length = 10, precision = 10, scale = 0)
    private Long stageId;
    @Column(name = "CAB_LEN", length = 10, precision = 10, scale = 0)
    private Long cabLen;
    @Column(name = "BOARD_ID", length = 10, precision = 10, scale = 0)
    private Long boardId;
    @Column(name = "DSLAM_ID", length = 10, precision = 10, scale = 0)
    private Long dslamId;
    @Column(name = "ERROR", length = 1)
    private String error;
    @Column(name = "FEE_AMOUNT", length = 10, precision = 10, scale = 0)
    private Long feeAmount;
    @Column(name = "ACCEPTED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date acceptedDate;
    @Column(name = "SHOP_ID", length = 10, precision = 10, scale = 0)
    private Long shopId;
    @Column(name = "STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long staffId;
    @Column(name = "UPDATER_USER_ID", length = 10, precision = 10, scale = 0)
    private Long updaterUserId;
    @Column(name = "UPDATER_SHOP_ID", length = 10, precision = 10, scale = 0)
    private Long updaterShopId;
    @Column(name = "SOURCE_ID", length = 10, precision = 10, scale = 0)
    private Long sourceId;
    @Column(name = "PRODUCT_CODE", length = 20)
    private String productCode;
    @Column(name = "REQ_ID", length = 10, precision = 10, scale = 0)
    private Long reqId;
    @Column(name = "ROOT_CAB_LEN", length = 10, precision = 10, scale = 0)
    private Long rootCabLen;
    @Column(name = "HANDOVER_STATUS", length = 1)
    private String handoverStatus;
    @Column(name = "EXPORT_STATUS", length = 1)
    private String exportStatus;
    @Column(name = "ELCOM_RESULT", length = 300)
    private String elcomResult;
    @Column(name = "CHANNEL_SPEED", length = 15)
    private String channelSpeed;
    @Column(name = "PORT_SPEED", length = 15)
    private String portSpeed;
    @Column(name = "IP", length = 20)
    private String ip;
    @Column(name = "SUBNET_MASK", length = 20)
    private String subnetMask;
    @Column(name = "DLU_ID", length = 10, precision = 10, scale = 0)
    private Long dluId;
    @Column(name = "PLATE_CABLE_BOX_A", length = 10, precision = 10, scale = 0)
    private Long plateCableBoxA;
    @Column(name = "PLATE_CABLE_BOX_B", length = 10, precision = 10, scale = 0)
    private Long plateCableBoxB;
    @Column(name = "REP_TYPE", length = 10, precision = 10, scale = 0)
    private Long repType;
    @Column(name = "REP_NO", length = 10, precision = 10, scale = 0)
    private Long repNo;
    @Column(name = "STATION_ID", length = 10, precision = 10, scale = 0)
    private Long stationId;
    @Column(name = "INFRA_TYPE", length = 10)
    private String infraType;
    @Column(name = "SPITTER_ID", length = 10, precision = 10, scale = 0)
    private Long splitterId;
    @Column(name = "IS_SPLITTER", length = 1)
    private String isSplitter;
    @Column(name = "PORT_LOGIC", length = 2, precision = 2, scale = 0)
    private Long portLogic;
    @Column(name = "COUPLER_NO", length = 2, precision = 2, scale = 0)
    private Long couplerNo;
//    @Column(name = "NETWORK_CLASS", length = 100)
    @Transient
    private String networkClass;
//    @Column(name = "SUB_REQ_ID", length = 10, precision = 10, scale = 0)
    @Transient
    private Long subReqId;
    @Transient
//    @Column(name = "RADIUS_CUST", length = 10, precision = 10, scale = 0)
    private Long radiusCust;
    @Transient
//    @Column(name = "TECHNOLOGY", length = 500)
    private String technology;
    @Transient
//    @Column(name = "TEAM_CODE", length = 200)
    private String teamCode;
//    @Column(name = "CABLE_BOX_CODE", length = 100)
    @Transient
    private String cableBoxCode;
//    @Column(name = "STATION_CODE", length = 100)
    @Transient
    private String stationCode;
//    @Column(name = "SOURCE_TYPE", length = 2, precision = 2, scale = 0)
    @Transient
    private Long sourceType;
//    @Column(name = "CABLE_BOX_TYPE", length = 100)
    @Transient
    private String cableBoxType;
//    @Column(name = "ODF_INDOR", length = 100)
    @Transient
    private String odfIndor;
//    @Column(name = "ODF_OUTDOR", length = 100)
    @Transient
    private String odfOutdor;
//    @Column(name = "X_LOCATION", length = 50)
    @Transient
    private String xLocation;
//    @Column(name = "Y_LOCATION", length = 50)
    @Transient
    private String yLocation;
//    @Column(name = "ADDRESS", length = 500)
    @Transient
    private String address;
//    @Column(name = "PROGRESS", length = 2)
    @Transient
    private String progress;
//    @Column(name = "DSLAM_CODE", length = 100)
    @Transient
    private String dslamCode;

    public SubDeployment() {
    }

    public void copySubDeployment(SubDeployment newSubDep) {

        newSubDep.setAcceptedDate(this.acceptedDate);
        newSubDep.setActionId(this.actionId);
        newSubDep.setAddress(this.address);
        newSubDep.setBoardId(this.boardId);
        newSubDep.setCabLen(this.cabLen);
        newSubDep.setCableBoxCode(this.cableBoxCode);
        newSubDep.setCableBoxId(this.cableBoxId);
        newSubDep.setCableBoxType(this.cableBoxType);
        newSubDep.setChannelSpeed(channelSpeed);
        newSubDep.setCreateDate(createDate);
        newSubDep.setDluId(dluId);
        newSubDep.setDslamCode(dslamCode);
        newSubDep.setDslamId(dslamId);
        newSubDep.setElcomResult(elcomResult);
        newSubDep.setError(error);
        newSubDep.setExportStatus(exportStatus);
        newSubDep.setFee(fee);
        newSubDep.setFeeAmount(feeAmount);
        newSubDep.setHandoverStatus(handoverStatus);
        newSubDep.setInfraType(infraType);
        newSubDep.setIp(ip);
        newSubDep.setIsSplitter(isSplitter);
        newSubDep.setIssueDate(issueDate);
        newSubDep.setLineNo(lineNo);
        newSubDep.setNetworkClass(networkClass);
        newSubDep.setOdfIndor(odfIndor);
        newSubDep.setOdfOutdor(odfOutdor);
        newSubDep.setPlateA(plateA);
        newSubDep.setPlateB(plateB);
        newSubDep.setPlateCableBoxA(plateCableBoxA);
        newSubDep.setPlateCableBoxB(plateCableBoxB);
        newSubDep.setPortLogic(portLogic);
        newSubDep.setPortNo(portNo);
        newSubDep.setPortSpeed(portSpeed);
        newSubDep.setProductCode(productCode);
        newSubDep.setProgress(progress);
        newSubDep.setRadiusCust(radiusCust);
        newSubDep.setRepNo(repNo);
        newSubDep.setRepType(repType);
        newSubDep.setReqId(reqId);
        newSubDep.setRootCabLen(rootCabLen);
        newSubDep.setShopId(shopId);
        newSubDep.setSourceId(sourceId);
        newSubDep.setSourceType(sourceType);
        newSubDep.setSplitterId(splitterId);
        newSubDep.setStaffId(staffId);
        newSubDep.setStageId(stageId);
        newSubDep.setStationCode(stationCode);
        newSubDep.setStationId(stationId);
        newSubDep.setStatus(status);
        newSubDep.setSubId(subId);
        newSubDep.setSubReqId(subReqId);
        newSubDep.setSubdeployId(subdeployId);
        newSubDep.setSubnetMask(subnetMask);
        newSubDep.setTeamCode(teamCode);
        newSubDep.setTechnology(technology);
        newSubDep.setUpdaterShopId(updaterShopId);
        newSubDep.setUpdaterUserId(updaterUserId);
        newSubDep.setxLocation(xLocation);
        newSubDep.setyLocation(yLocation);
    }

    public String getInfraType() {
        return infraType;
    }

    public void setInfraType(String infraType) {
        this.infraType = infraType;
    }

    public Long getSubdeployId() {
        return subdeployId;
    }

    public void setSubdeployId(Long subdeployId) {
        this.subdeployId = subdeployId;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public Long getPlateA() {
        return plateA;
    }

    public void setPlateA(Long plateA) {
        this.plateA = plateA;
    }

    public Long getPlateB() {
        return plateB;
    }

    public void setPlateB(Long plateB) {
        this.plateB = plateB;
    }

    public Long getLineNo() {
        return lineNo;
    }

    public void setLineNo(Long lineNo) {
        this.lineNo = lineNo;
    }

    public Long getPortNo() {
        return portNo;
    }

    public void setPortNo(Long portNo) {
        this.portNo = portNo;
    }

    public Long getCableBoxId() {
        return cableBoxId;
    }

    public void setCableBoxId(Long cableBoxId) {
        this.cableBoxId = cableBoxId;
    }

    public Long getStageId() {
        return stageId;
    }

    public void setStageId(Long stageId) {
        this.stageId = stageId;
    }

    public Long getCabLen() {
        return cabLen;
    }

    public void setCabLen(Long cabLen) {
        this.cabLen = cabLen;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Long getDslamId() {
        return dslamId;
    }

    public void setDslamId(Long dslamId) {
        this.dslamId = dslamId;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Long getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(Long feeAmount) {
        this.feeAmount = feeAmount;
    }

    public Date getAcceptedDate() {
        return acceptedDate;
    }

    public void setAcceptedDate(Date acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Long getUpdaterUserId() {
        return updaterUserId;
    }

    public void setUpdaterUserId(Long updaterUserId) {
        this.updaterUserId = updaterUserId;
    }

    public Long getUpdaterShopId() {
        return updaterShopId;
    }

    public void setUpdaterShopId(Long updaterShopId) {
        this.updaterShopId = updaterShopId;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Long getReqId() {
        return reqId;
    }

    public void setReqId(Long reqId) {
        this.reqId = reqId;
    }

    public Long getRootCabLen() {
        return rootCabLen;
    }

    public void setRootCabLen(Long rootCabLen) {
        this.rootCabLen = rootCabLen;
    }

    public String getHandoverStatus() {
        return handoverStatus;
    }

    public void setHandoverStatus(String handoverStatus) {
        this.handoverStatus = handoverStatus;
    }

    public String getExportStatus() {
        return exportStatus;
    }

    public void setExportStatus(String exportStatus) {
        this.exportStatus = exportStatus;
    }

    public String getElcomResult() {
        return elcomResult;
    }

    public void setElcomResult(String elcomResult) {
        this.elcomResult = elcomResult;
    }

    public String getChannelSpeed() {
        return channelSpeed;
    }

    public void setChannelSpeed(String channelSpeed) {
        this.channelSpeed = channelSpeed;
    }

    public String getPortSpeed() {
        return portSpeed;
    }

    public void setPortSpeed(String portSpeed) {
        this.portSpeed = portSpeed;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getSubnetMask() {
        return subnetMask;
    }

    public void setSubnetMask(String subnetMask) {
        this.subnetMask = subnetMask;
    }

    public Long getDluId() {
        return dluId;
    }

    public void setDluId(Long dluId) {
        this.dluId = dluId;
    }

    public Long getPlateCableBoxA() {
        return plateCableBoxA;
    }

    public void setPlateCableBoxA(Long plateCableBoxA) {
        this.plateCableBoxA = plateCableBoxA;
    }

    public Long getPlateCableBoxB() {
        return plateCableBoxB;
    }

    public void setPlateCableBoxB(Long plateCableBoxB) {
        this.plateCableBoxB = plateCableBoxB;
    }

    public Long getRepType() {
        return repType;
    }

    public void setRepType(Long repType) {
        this.repType = repType;
    }

    public Long getRepNo() {
        return repNo;
    }

    public void setRepNo(Long repNo) {
        this.repNo = repNo;
    }

    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    public String getNetworkClass() {
        return networkClass;
    }

    public void setNetworkClass(String networkClass) {
        this.networkClass = networkClass;
    }

    public Long getSubReqId() {
        return subReqId;
    }

    public void setSubReqId(Long subReqId) {
        this.subReqId = subReqId;
    }

    public Long getRadiusCust() {
        return radiusCust;
    }

    public void setRadiusCust(Long radiusCust) {
        this.radiusCust = radiusCust;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getTeamCode() {
        return teamCode;
    }

    public void setTeamCode(String teamCode) {
        this.teamCode = teamCode;
    }

    public String getCableBoxCode() {
        return cableBoxCode;
    }

    public void setCableBoxCode(String cableBoxCode) {
        this.cableBoxCode = cableBoxCode;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public Long getSourceType() {
        return sourceType;
    }

    public void setSourceType(Long sourceType) {
        this.sourceType = sourceType;
    }

    public String getCableBoxType() {
        return cableBoxType;
    }

    public void setCableBoxType(String cableBoxType) {
        this.cableBoxType = cableBoxType;
    }

    public String getOdfIndor() {
        return odfIndor;
    }

    public void setOdfIndor(String odfIndor) {
        this.odfIndor = odfIndor;
    }

    public String getOdfOutdor() {
        return odfOutdor;
    }

    public void setOdfOutdor(String odfOutdor) {
        this.odfOutdor = odfOutdor;
    }

    public String getxLocation() {
        return xLocation;
    }

    public void setxLocation(String xLocation) {
        this.xLocation = xLocation;
    }

    public String getyLocation() {
        return yLocation;
    }

    public void setyLocation(String yLocation) {
        this.yLocation = yLocation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getDslamCode() {
        return dslamCode;
    }

    public void setDslamCode(String dslamCode) {
        this.dslamCode = dslamCode;
    }

    public Long getSplitterId() {
        return splitterId;
    }

    public void setSplitterId(Long splitterId) {
        this.splitterId = splitterId;
    }

    /**
     * @return the isSplitter
     */
    public String getIsSplitter() {
        return isSplitter;
    }

    /**
     * @param isSplitter the isSplitter to set
     */
    public void setIsSplitter(String isSplitter) {
        this.isSplitter = isSplitter;
    }

    /**
     * @return the portLogic
     */
    public Long getPortLogic() {
        return portLogic;
    }

    /**
     * @param portLogic the portLogic to set
     */
    public void setPortLogic(Long portLogic) {
        this.portLogic = portLogic;
    }

    /**
     * @return the couplerNo
     */
    public Long getCouplerNo() {
        return couplerNo;
    }

    /**
     * @param couplerNo the couplerNo to set
     */
    public void setCouplerNo(Long couplerNo) {
        this.couplerNo = couplerNo;
    }
}
