package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SUB_DEPOSIT_ADSL_LL")
public class SubDepositAdslLl implements Serializable {

    @Id
    @Column(name = "ID", length = 22, precision = 22, scale = 0, nullable = false)
    private Long id;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "REG_DATETIME", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date regDatetime;
    @Column(name = "USER_NAME", length = 50, nullable = false)
    private String userName;
    @Column(name = "PC", length = 30, nullable = false)
    private String pc;
    @Column(name = "DEPOSIT", length = 16, precision = 16, scale = 0)
    private Double deposit;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "REASON_TYPE", length = 100)
    private String reasonType;
    @Column(name = "DEPOSIT_TYPE", length = 1)
    private String depositType;
    @Column(name = "ACTION_TYPE", length = 10)
    private String actionType;
    @Column(name = "Receipt_Expense_Id", length = 10)
    private Long receiptEnxenseId;
    @Column(name = "INVOICE_LIST_ID", length = 10)
    private Long invoiceListId;
    @Column(name = "INVOICE_NO", length = 10)
    private Long invoiceNo;

    public SubDepositAdslLl() {
    }

    public SubDepositAdslLl(Long subId, Long status, String userName, String pc, String reasonType, 
            Date regDatetime, String depositType, Double deposit, Long invoiceListId, Long invoiceNo) {
        this.subId = subId;
        this.status = status;
        this.userName = userName;
        this.pc = pc;
        this.reasonType = reasonType;
        this.regDatetime = regDatetime;
        this.depositType = depositType;
        this.deposit = deposit;
        this.invoiceListId = invoiceListId;
        this.invoiceNo = invoiceNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Date getRegDatetime() {
        return regDatetime;
    }

    public void setRegDatetime(Date regDatetime) {
        this.regDatetime = regDatetime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPc() {
        return pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public Double getDeposit() {
        return deposit;
    }

    public void setDeposit(Double deposit) {
        this.deposit = deposit;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getReasonType() {
        return reasonType;
    }

    public void setReasonType(String reasonType) {
        this.reasonType = reasonType;
    }

    public String getDepositType() {
        return depositType;
    }

    public void setDepositType(String depositType) {
        this.depositType = depositType;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    /**
     * @return the receiptEnxenseId
     */
    public Long getReceiptEnxenseId() {
        return receiptEnxenseId;
    }

    /**
     * @param receiptEnxenseId the receiptEnxenseId to set
     */
    public void setReceiptEnxenseId(Long receiptEnxenseId) {
        this.receiptEnxenseId = receiptEnxenseId;
    }

    /**
     * @return the invoiceListId
     */
    public Long getInvoiceListId() {
        return invoiceListId;
    }

    /**
     * @param invoiceListId the invoiceListId to set
     */
    public void setInvoiceListId(Long invoiceListId) {
        this.invoiceListId = invoiceListId;
    }

    /**
     * @return the invoiceNo
     */
    public Long getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * @param invoiceNo the invoiceNo to set
     */
    public void setInvoiceNo(Long invoiceNo) {
        this.invoiceNo = invoiceNo;
    }
}
