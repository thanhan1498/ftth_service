/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "SUB_DIGITAL")
public class SubDigital implements Serializable {

    @Id
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "sale_trans_id", length = 10, precision = 10, scale = 0)
    private Long saleTransId;
    @Column(name = "ACCOUNT", length = 100)
    private String account;
    @Column(name = "ISDN", length = 100)
    private String isdn;
    @Column(name = "team_id", length = 10, precision = 10, scale = 0)
    private Long teamId;
    @Column(name = "first_connect")
    @Temporal(TemporalType.TIMESTAMP)
    private Date firstConnect;
    @Column(name = "deploy_address", length = 100)
    private String deployAddress;
    @Column(name = "address_code", length = 100)
    private String addressCode;
    @Column(name = "address", length = 100)
    private String address;
    @Column(name = "province", length = 100)
    private String province;
    @Column(name = "district", length = 100)
    private String district;
    @Column(name = "precinct", length = 100)
    private String precintct;
    @Column(name = "street", length = 100)
    private String street;
    @Column(name = "street_name", length = 100)
    private String streetName;
    @Column(name = "street_block", length = 100)
    private String streetBlock;
    @Column(name = "street_block_name", length = 100)
    private String streetBlockName;
    @Column(name = "home", length = 100)
    private String home;
    @Column(name = "sta_datetime", length = 100)
    @Temporal(TemporalType.TIMESTAMP)
    private Date staDateTime;
    @Column(name = "end_datetime", length = 100)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDateTime;
    @Column(name = "user_created", length = 100)
    private String userCreated;
    @Column(name = "create_date", length = 100)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "status", length = 10, precision = 10, scale = 0)
    private Long status;
    @Column(name = "shop_code", length = 100)
    private String shopCode;
    @Column(name = "staff_id", length = 10, precision = 10, scale = 0)
    private Long staffId;
    @Column(name = "contract_id", length = 10, precision = 10, scale = 0)
    private Long contractId;
    @Column(name = "service_type", length = 100)
    private String serviceType;
    @Column(name = "change_datetime", length = 100)
    @Temporal(TemporalType.TIMESTAMP)
    private Date changeDateTime;
    @Column(name = "cust_lat", length = 100)
    private String custLat;
    @Column(name = "cust_long", length = 100)
    private String custLong;
    @Column(name = "warranty_date", length = 100)
    @Temporal(TemporalType.TIMESTAMP)
    private Date warrantyDate;
    @Column(name = "user_using", length = 100)
    private String userUsing;
    @Column(name = "tel_mobile", length = 100)
    private String telMobile;

    /**
     * @return the subId
     */
    public Long getSubId() {
        return subId;
    }

    /**
     * @param subId the subId to set
     */
    public void setSubId(Long subId) {
        this.subId = subId;
    }

    /**
     * @return the saleTransId
     */
    public Long getSaleTransId() {
        return saleTransId;
    }

    /**
     * @param saleTransId the saleTransId to set
     */
    public void setSaleTransId(Long saleTransId) {
        this.saleTransId = saleTransId;
    }

    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * @return the isdn
     */
    public String getIsdn() {
        return isdn;
    }

    /**
     * @param isdn the isdn to set
     */
    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    /**
     * @return the teamId
     */
    public Long getTeamId() {
        return teamId;
    }

    /**
     * @param teamId the teamId to set
     */
    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    /**
     * @return the firstConnect
     */
    public Date getFirstConnect() {
        return firstConnect;
    }

    /**
     * @param firstConnect the firstConnect to set
     */
    public void setFirstConnect(Date firstConnect) {
        this.firstConnect = firstConnect;
    }

    /**
     * @return the deployAddress
     */
    public String getDeployAddress() {
        return deployAddress;
    }

    /**
     * @param deployAddress the deployAddress to set
     */
    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    /**
     * @return the addressCode
     */
    public String getAddressCode() {
        return addressCode;
    }

    /**
     * @param addressCode the addressCode to set
     */
    public void setAddressCode(String addressCode) {
        this.addressCode = addressCode;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the province
     */
    public String getProvince() {
        return province;
    }

    /**
     * @param province the province to set
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     * @return the district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * @param district the district to set
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     * @return the precintct
     */
    public String getPrecintct() {
        return precintct;
    }

    /**
     * @param precintct the precintct to set
     */
    public void setPrecintct(String precintct) {
        this.precintct = precintct;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the streetName
     */
    public String getStreetName() {
        return streetName;
    }

    /**
     * @param streetName the streetName to set
     */
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    /**
     * @return the streetBlock
     */
    public String getStreetBlock() {
        return streetBlock;
    }

    /**
     * @param streetBlock the streetBlock to set
     */
    public void setStreetBlock(String streetBlock) {
        this.streetBlock = streetBlock;
    }

    /**
     * @return the streetBlockName
     */
    public String getStreetBlockName() {
        return streetBlockName;
    }

    /**
     * @param streetBlockName the streetBlockName to set
     */
    public void setStreetBlockName(String streetBlockName) {
        this.streetBlockName = streetBlockName;
    }

    /**
     * @return the home
     */
    public String getHome() {
        return home;
    }

    /**
     * @param home the home to set
     */
    public void setHome(String home) {
        this.home = home;
    }

    /**
     * @return the staDateTime
     */
    public Date getStaDateTime() {
        return staDateTime;
    }

    /**
     * @param staDateTime the staDateTime to set
     */
    public void setStaDateTime(Date staDateTime) {
        this.staDateTime = staDateTime;
    }

    /**
     * @return the endDateTime
     */
    public Date getEndDateTime() {
        return endDateTime;
    }

    /**
     * @param endDateTime the endDateTime to set
     */
    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    /**
     * @return the userCreated
     */
    public String getUserCreated() {
        return userCreated;
    }

    /**
     * @param userCreated the userCreated to set
     */
    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the shopCode
     */
    public String getShopCode() {
        return shopCode;
    }

    /**
     * @param shopCode the shopCode to set
     */
    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    /**
     * @return the staffId
     */
    public Long getStaffId() {
        return staffId;
    }

    /**
     * @param staffId the staffId to set
     */
    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    /**
     * @return the contractId
     */
    public Long getContractId() {
        return contractId;
    }

    /**
     * @param contractId the contractId to set
     */
    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    /**
     * @return the changeDateTime
     */
    public Date getChangeDateTime() {
        return changeDateTime;
    }

    /**
     * @param changeDateTime the changeDateTime to set
     */
    public void setChangeDateTime(Date changeDateTime) {
        this.changeDateTime = changeDateTime;
    }

    /**
     * @return the custLat
     */
    public String getCustLat() {
        return custLat;
    }

    /**
     * @param custLat the custLat to set
     */
    public void setCustLat(String custLat) {
        this.custLat = custLat;
    }

    /**
     * @return the custLong
     */
    public String getCustLong() {
        return custLong;
    }

    /**
     * @param custLong the custLong to set
     */
    public void setCustLong(String custLong) {
        this.custLong = custLong;
    }

    /**
     * @return the warrantyDate
     */
    public Date getWarrantyDate() {
        return warrantyDate;
    }

    /**
     * @param warrantyDate the warrantyDate to set
     */
    public void setWarrantyDate(Date warrantyDate) {
        this.warrantyDate = warrantyDate;
    }

    /**
     * @return the userUsing
     */
    public String getUserUsing() {
        return userUsing;
    }

    /**
     * @param userUsing the userUsing to set
     */
    public void setUserUsing(String userUsing) {
        this.userUsing = userUsing;
    }

    /**
     * @return the telMobile
     */
    public String getTelMobile() {
        return telMobile;
    }

    /**
     * @param telMobile the telMobile to set
     */
    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }
}
