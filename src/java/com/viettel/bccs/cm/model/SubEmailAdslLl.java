package com.viettel.bccs.cm.model;

import com.viettel.bccs.cm.model.pk.SubEmailAdslLlId;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SUB_EMAIL_ADSL_LL")
@IdClass(SubEmailAdslLlId.class)
public class SubEmailAdslLl implements Serializable {

    @Id
    private String email;
    @Id
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectFrom;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0, nullable = false)
    private Long subId;
    @Column(name = "UNTIL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date until;

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Date getUntil() {
        return until;
    }

    public void setUntil(Date until) {
        this.until = until;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
