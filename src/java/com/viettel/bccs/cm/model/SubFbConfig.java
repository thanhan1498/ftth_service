/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author duyetdk
 */

@Entity
@Table(name = "SUB_FB_CONF")
public class SubFbConfig implements Serializable {
    
    @Id
    @Column(name = "ID", length = 10, precision = 10, scale = 0)
    private Long Id;
    @Column(name = "TASK_MANAGEMENT_ID", length = 10, precision = 10, scale = 0)
    private Long taskManagementId;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "STATUS_CONF")
    private Long statusConf;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "ACCOUNT", length = 20)
    private String account;
    @Column(name = "INFRA_IMG_ID", length = 10, precision = 10, scale = 0)
    private Long ifraImgId;
    @Column(name = "INFRA_LAT", length = 20)
    private String infraLat;
    @Column(name = "INFRA_LONG", length = 20)
    private String infraLong;
    @Column(name = "CUST_IMG_ID", length = 10, precision = 10, scale = 0)
    private Long custImgId;
    @Column(name = "CUST_LAT", length = 20)
    private String custLat;
    @Column(name = "CUST_LONG", length = 20)
    private String custLong;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "LAST_UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdateDate;
    @Column(name = "CREATE_USER", length = 50)
    private String createUser;
    @Column(name = "LAST_UPDATE_USER", length = 50)
    private String lastUpdateUser;
    @Column(name = "REASON_CODE", length = 50)
    private String reasonCode;
    @Column(name = "DESCRIPTION", length = 200)
    private String description;
    @Column(name = "PROVINCE", length = 50)
    private String province;
    @Column(name = "TYPE_CONFIG", length = 50)
    private String typeConf;
    
    public SubFbConfig() {
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Long getTaskManagementId() {
        return taskManagementId;
    }

    public void setTaskManagementId(Long taskManagementId) {
        this.taskManagementId = taskManagementId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getStatusConf() {
        return statusConf;
    }

    public void setStatusConf(Long statusConf) {
        this.statusConf = statusConf;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Long getIfraImgId() {
        return ifraImgId;
    }

    public void setIfraImgId(Long ifraImgId) {
        this.ifraImgId = ifraImgId;
    }

    public String getInfraLat() {
        return infraLat;
    }

    public void setInfraLat(String infraLat) {
        this.infraLat = infraLat;
    }

    public String getInfraLong() {
        return infraLong;
    }

    public void setInfraLong(String infraLong) {
        this.infraLong = infraLong;
    }

    public Long getCustImgId() {
        return custImgId;
    }

    public void setCustImgId(Long custImgId) {
        this.custImgId = custImgId;
    }

    public String getCustLat() {
        return custLat;
    }

    public void setCustLat(String custLat) {
        this.custLat = custLat;
    }

    public String getCustLong() {
        return custLong;
    }

    public void setCustLong(String custLong) {
        this.custLong = custLong;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    /**
     * @return the typeConfig
     */
    public String getTypeConfig() {
        return typeConf;
    }

    /**
     * @param typeConfig the typeConfig to set
     */
    public void setTypeConfig(String typeConfig) {
        this.typeConf = typeConfig;
    }

}
