/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.util.Date;

/**
 *
 * @author duyetdk
 */
public class SubFbConfigDetail {

    private Long Id;
    private Long taskManagementId;
    private Long status;
    private Long statusConf;
    private Long subId;
    private String account;
    private Long ifraImgId;
    private String infraLat;
    private String infraLong;
    private Long custImgId;
    private String custLat;
    private String custLong;
    private Date createDate;
    private Date lastUpdateDate;
    private String createUser;
    private String lastUpdateUser;
    private String reasonCode;
    private String description;
    private String connectorName;
    private String taskName;

    public SubFbConfigDetail() {
    }

    public SubFbConfigDetail(SubFbConfigDetail sub, String connectorName) {
        if (sub != null) {
            this.Id = sub.getId();
            this.taskManagementId = sub.getTaskManagementId();
            this.status = sub.getStatus();
            this.statusConf = sub.getStatusConf();
            this.subId = sub.getSubId();
            this.account = sub.getAccount();
            this.ifraImgId = sub.getIfraImgId();
            this.infraLat = sub.getInfraLat();
            this.infraLong = sub.getInfraLong();
            this.custImgId = sub.getCustImgId();
            this.custLat = sub.getCustLat();
            this.custLong = sub.getCustLong();
            this.createDate = sub.getCreateDate();
            this.lastUpdateDate = sub.getLastUpdateDate();
            this.createUser = sub.getCreateUser();
            this.lastUpdateUser = sub.getLastUpdateUser();
            this.reasonCode = sub.getReasonCode();
            this.description = sub.getDescription();
            this.taskName = sub.getTaskName();
            if (connectorName != null && !connectorName.trim().isEmpty()) {
                this.connectorName = connectorName;
            }
        }
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Long getTaskManagementId() {
        return taskManagementId;
    }

    public void setTaskManagementId(Long taskManagementId) {
        this.taskManagementId = taskManagementId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getStatusConf() {
        return statusConf;
    }

    public void setStatusConf(Long statusConf) {
        this.statusConf = statusConf;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Long getIfraImgId() {
        return ifraImgId;
    }

    public void setIfraImgId(Long ifraImgId) {
        this.ifraImgId = ifraImgId;
    }

    public String getInfraLat() {
        return infraLat;
    }

    public void setInfraLat(String infraLat) {
        this.infraLat = infraLat;
    }

    public String getInfraLong() {
        return infraLong;
    }

    public void setInfraLong(String infraLong) {
        this.infraLong = infraLong;
    }

    public Long getCustImgId() {
        return custImgId;
    }

    public void setCustImgId(Long custImgId) {
        this.custImgId = custImgId;
    }

    public String getCustLat() {
        return custLat;
    }

    public void setCustLat(String custLat) {
        this.custLat = custLat;
    }

    public String getCustLong() {
        return custLong;
    }

    public void setCustLong(String custLong) {
        this.custLong = custLong;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getConnectorName() {
        return connectorName;
    }

    public void setConnectorName(String connectorName) {
        this.connectorName = connectorName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }
}
