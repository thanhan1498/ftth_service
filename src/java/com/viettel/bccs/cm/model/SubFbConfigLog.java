/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author duyetdk
 */
@Entity
@Table(name = "SUB_FB_CONF_LOG")
public class SubFbConfigLog implements Serializable {
    @Id
    @Column(name = "ID", length = 10, precision = 10, scale = 0)
    private Long Id;
    @Column(name = "SUB_FB_CONF_ID", length = 10, precision = 10, scale = 0)
    private Long subFbConfigId;
    @Column(name = "STATUS_CONF")
    private Long statusConfig;
    @Column(name = "ERROR_CODE", length = 10)
    private String errorCode;
    @Column(name = "REQUEST", length = 400)
    private String request;
    @Column(name = "RESPONSE", length = 200)
    private String response;
    @Column(name = "ACTION", length = 50)
    private String action;
    @Column(name = "CREATE_USER", length = 50)
    private String createUser;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "DESCRIPTION", length = 200)
    private String description;

    public SubFbConfigLog() {
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Long getSubFbConfigId() {
        return subFbConfigId;
    }

    public void setSubFbConfigId(Long subFbConfigId) {
        this.subFbConfigId = subFbConfigId;
    }

    public Long getStatusConfig() {
        return statusConfig;
    }

    public void setStatusConfig(Long statusConfig) {
        this.statusConfig = statusConfig;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
