package com.viettel.bccs.cm.model;

import com.viettel.bccs.cm.model.pk.SubIpAdslLlId;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SUB_IP_ADSL_LL")
@IdClass(SubIpAdslLlId.class)
public class SubIpAdslLl implements Serializable {

    @Id
    private Long subId;
    @Id
    private String ip;
    @Id
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectFrom;
    @Column(name = "UNTIL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date until;
    @Column(name = "AUTO_ASSIGNED", length = 1, nullable = false)
    private String autoAssigned;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0, nullable = false)
    private Long status;
    @Column(name = "STATUS_RATING", length = 1, precision = 1, scale = 0)
    private Long statusRating;
    @Column(name = "IP_BLOCK", length = 50)
    private String ipBlock;
    @Column(name = "FRAMED_IP_NETMASK", length = 50)
    private String framedIpNetmask;
    @Column(name = "IP_BLOCK_PROCESS", length = 50)
    private String ipBlockProcess;

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public Date getUntil() {
        return until;
    }

    public void setUntil(Date until) {
        this.until = until;
    }

    public String getAutoAssigned() {
        return autoAssigned;
    }

    public void setAutoAssigned(String autoAssigned) {
        this.autoAssigned = autoAssigned;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getStatusRating() {
        return statusRating;
    }

    public void setStatusRating(Long statusRating) {
        this.statusRating = statusRating;
    }

    public String getIpBlock() {
        return ipBlock;
    }

    public void setIpBlock(String ipBlock) {
        this.ipBlock = ipBlock;
    }

    public String getFramedIpNetmask() {
        return framedIpNetmask;
    }

    public void setFramedIpNetmask(String framedIpNetmask) {
        this.framedIpNetmask = framedIpNetmask;
    }

    public String getIpBlockProcess() {
        return ipBlockProcess;
    }

    public void setIpBlockProcess(String ipBlockProcess) {
        this.ipBlockProcess = ipBlockProcess;
    }
}
