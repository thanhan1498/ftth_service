/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.util.Date;

/**
 *
 * @author partner1
 */
public class SubMbOut {
    
    private String actStatus;
    private Long custId;
    private String customerName;
    private String groupName;
    private String idNo;
    private String birthDate;
    private String isdn;
    private Long offerId;
    private String offerName;
    private Long status;
    private String statusName;
    private Long subId;

    public SubMbOut() {
    }
    
    

    public SubMbOut(String actStatus, Long custId, String customerName, String groupName, String idNo, String birthDate, String isdn, Long offerId, String offerName, Long status, String statusName, Long subId) {
        this.actStatus = actStatus;
        this.custId = custId;
        this.customerName = customerName;
        this.groupName = groupName;
        this.idNo = idNo;
        this.birthDate = birthDate;
        this.isdn = isdn;
        this.offerId = offerId;
        this.offerName = offerName;
        this.status = status;
        this.statusName = statusName;
        this.subId = subId;
    }

    public String getActStatus() {
        return actStatus;
    }

    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }
    
    
    
}
