/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "SUB_MB")
public class SubMbPos implements Serializable {

    @Id
    @Column(name = "sub_id", length = 11, precision = 11, scale = 0)
    private Long subId;
    @Column(name = "CUST_REQ_ID", length = 11, precision = 11, scale = 0)
    private Long custReqId;
    @Column(name = "CONTRACT_ID", length = 11, precision = 11, scale = 0)
    private Long contractId;
    @Column(name = "SUB_TYPE", length = 10)
    private String subType;
    @Column(name = "REG_REASON_ID", length = 11, precision = 11, scale = 0)
    private Long regReasonId;
    @Column(name = "FINISH_REASON_ID", length = 11, precision = 11, scale = 0)
    private Long finishReasonId;
    @Column(name = "shop_code", length = 20)
    private String shopCode;
    @Column(name = "end_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDatetime;
    @Column(name = "sta_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date staDatetime;
    @Column(name = "act_status", length = 2)
    private String actStatus;
    @Column(name = "vip", length = 1)
    private String vip;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "user_created", length = 50)
    private String userCreated;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "isdn", length = 10)
    private String isdn;
    @Column(name = "product_code", length = 30)
    private String productCode;

    /**
     * @return the subId
     */
    public Long getSubId() {
        return subId;
    }

    /**
     * @param subId the subId to set
     */
    public void setSubId(Long subId) {
        this.subId = subId;
    }

    /**
     * @return the custReqId
     */
    public Long getCustReqId() {
        return custReqId;
    }

    /**
     * @param custReqId the custReqId to set
     */
    public void setCustReqId(Long custReqId) {
        this.custReqId = custReqId;
    }

    /**
     * @return the contractId
     */
    public Long getContractId() {
        return contractId;
    }

    /**
     * @param contractId the contractId to set
     */
    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    /**
     * @return the subType
     */
    public String getSubType() {
        return subType;
    }

    /**
     * @param subType the subType to set
     */
    public void setSubType(String subType) {
        this.subType = subType;
    }

    /**
     * @return the regReasonId
     */
    public Long getRegReasonId() {
        return regReasonId;
    }

    /**
     * @param regReasonId the regReasonId to set
     */
    public void setRegReasonId(Long regReasonId) {
        this.regReasonId = regReasonId;
    }

    /**
     * @return the finishReasonId
     */
    public Long getFinishReasonId() {
        return finishReasonId;
    }

    /**
     * @param finishReasonId the finishReasonId to set
     */
    public void setFinishReasonId(Long finishReasonId) {
        this.finishReasonId = finishReasonId;
    }

    /**
     * @return the shopCode
     */
    public String getShopCode() {
        return shopCode;
    }

    /**
     * @param shopCode the shopCode to set
     */
    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    /**
     * @return the endDatetime
     */
    public Date getEndDatetime() {
        return endDatetime;
    }

    /**
     * @param endDatetime the endDatetime to set
     */
    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    /**
     * @return the staDatetime
     */
    public Date getStaDatetime() {
        return staDatetime;
    }

    /**
     * @param staDatetime the staDatetime to set
     */
    public void setStaDatetime(Date staDatetime) {
        this.staDatetime = staDatetime;
    }

    /**
     * @return the actStatus
     */
    public String getActStatus() {
        return actStatus;
    }

    /**
     * @param actStatus the actStatus to set
     */
    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    /**
     * @return the vip
     */
    public String getVip() {
        return vip;
    }

    /**
     * @param vip the vip to set
     */
    public void setVip(String vip) {
        this.vip = vip;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the userCreated
     */
    public String getUserCreated() {
        return userCreated;
    }

    /**
     * @param userCreated the userCreated to set
     */
    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the isdn
     */
    public String getIsdn() {
        return isdn;
    }

    /**
     * @param isdn the isdn to set
     */
    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

}
