package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SUB_PRODUCT_ADSL_LL")
public class SubProductAdslLl implements Serializable {

    @Id
    @Column(name = "ID", length = 22, precision = 22, scale = 0)
    private Long id;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0, nullable = false)
    private Long subId;
    @Column(name = "EFFECT_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectDate;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "OFFER_ID", length = 10, precision = 10, scale = 0)
    private Long offerId;
    @Column(name = "PRODUCT_CODE", length = 10)
    private String productCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Date getEffectDate() {
        return effectDate;
    }

    public void setEffectDate(Date effectDate) {
        this.effectDate = effectDate;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
}
