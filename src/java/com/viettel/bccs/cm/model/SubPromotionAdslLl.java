package com.viettel.bccs.cm.model;

import com.viettel.bccs.cm.model.pk.SubPromotionAdslLlId;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SUB_PROMOTION_ADSL_LL")
@IdClass(SubPromotionAdslLlId.class)
public class SubPromotionAdslLl implements Serializable {

    @Id
    private Long subId;
    @Id
    private String promotionCode;
    @Id
    @Temporal(TemporalType.TIMESTAMP)
    private Date effect;
    @Column(name = "UNTIL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date until;

    public SubPromotionAdslLl() {
    }

    public SubPromotionAdslLl(Long subId, String promotionCode, Date effect, Date until) {
        this.subId = subId;
        this.promotionCode = promotionCode;
        this.effect = effect;
        this.until = until;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    public Date getEffect() {
        return effect;
    }

    public void setEffect(Date effect) {
        this.effect = effect;
    }

    public Date getUntil() {
        return until;
    }

    public void setUntil(Date until) {
        this.until = until;
    }
}
