package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SUB_REL_PRODUCT_ADSL_LL")
public class SubRelProductAdslLl implements Serializable {

    @Id
    @Column(name = "ID")
    private Long Id;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "STA_DATETIME")
    private Date staDatetime;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "END_DATETIME")
    private Date endDatetime;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "REG_DATE")
    private Date regDate;
    @Column(name = "REL_PRODUCT_VALUE", length = 256)
    private String relProductValue;
    @Column(name = "IS_CONNECTED", length = 1)
    private Long isConnected;
    @Column(name = "MAIN_PRODUCT_CODE", length = 30)
    private String mainProductCode;
    @Column(name = "REL_PRODUCT_CODE", length = 30)
    private String relProductCode;
    @Column(name = "STATUS", length = 1)
    private Long status;

    public SubRelProductAdslLl() {
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Date getStaDatetime() {
        return staDatetime;
    }

    public void setStaDatetime(Date staDatetime) {
        this.staDatetime = staDatetime;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public String getRelProductValue() {
        return relProductValue;
    }

    public void setRelProductValue(String relProductValue) {
        this.relProductValue = relProductValue;
    }

    public Long getIsConnected() {
        return isConnected;
    }

    public void setIsConnected(Long isConnected) {
        this.isConnected = isConnected;
    }

    public String getMainProductCode() {
        return mainProductCode;
    }

    public void setMainProductCode(String mainProductCode) {
        this.mainProductCode = mainProductCode;
    }

    public String getRelProductCode() {
        return relProductCode;
    }

    public void setRelProductCode(String relProductCode) {
        this.relProductCode = relProductCode;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
