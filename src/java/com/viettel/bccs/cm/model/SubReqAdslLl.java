package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author linhlh2
 */
@Entity
@Table(name = "SUB_REQ_ADSL_LL")
public class SubReqAdslLl extends ReqSubscriber implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "TASK_ID", length = 10, precision = 10, scale = 0)
    private Long taskId;
    @Column(name = "CUST_REQ_ID", length = 10, precision = 10, scale = 0, nullable = false)
    private Long custReqId;
    @Column(name = "IB", length = 1)
    private String ib;
    @Column(name = "CABLE_BOX_ID", length = 10, precision = 10, scale = 0)
    private Long cableBoxId;
    @Column(name = "BOARD_ID", length = 10, precision = 10, scale = 0)
    private Long boardId;
    @Column(name = "CAB_LEN", length = 10)
    private String cabLen;
    @Column(name = "PORT_NO", length = 10)
    private String portNo;
    @Column(name = "DSLAM_ID", length = 10, precision = 10, scale = 0)
    private Long dslamId;
    @Column(name = "TEAM_ID", length = 10, precision = 10, scale = 0)
    private Long teamId;
    @Column(name = "STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long staffId;
    @Column(name = "FIRST_CONNECT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date firstConnect;
    @Column(name = "DEPLOY_ADDRESS", length = 500)
    private String deployAddress;
    @Column(name = "DEPLOY_AREA_CODE", length = 50, nullable = false)
    private String deployAreaCode;
    @Column(name = "LINE_PHONE", length = 10)
    private String linePhone;
    @Column(name = "LINE_TYPE", length = 10)
    private String lineType;
    @Column(name = "SPEED", length = 10, precision = 10, scale = 0, nullable = false)
    private Long speed;
    @Column(name = "PASSWORD", length = 100)
    private String password;
    @Column(name = "ACCOUNT", length = 100)
    private String account;
    @Column(name = "ISDN", length = 20)
    private String isdn;
    @Column(name = "ADDRESS_CODE", length = 50, nullable = false)
    private String addressCode;
    @Column(name = "ADDRESS", length = 500)
    private String address;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "DEPOSIT", length = 16, precision = 16, scale = 4)
    private Double deposit;
    @Column(name = "STATUS", length = 22, precision = 22, scale = 0)
    private Long status;
    @Column(name = "USER_CREATED", length = 30, nullable = false)
    private String userCreated;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "VIP", nullable = false)
    private String vip;
    @Column(name = "ACT_STATUS", nullable = false, length = 3)
    private String actStatus;
    @Column(name = "STA_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date staDatetime;
    @Column(name = "END_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDatetime;
    @Column(name = "SHOP_CODE", length = 50, nullable = false)
    private String shopCode;
    @Column(name = "FINISH_REASON_ID", length = 10, precision = 10, scale = 0)
    private Long finishReasonId;
    @Column(name = "REG_REASON_ID", length = 10, precision = 10, scale = 0, nullable = false)
    private Long regReasonId;
    @Column(name = "PROVINCE", length = 10)
    private String province;
    @Column(name = "DISTRICT", length = 10)
    private String district;
    @Column(name = "PRECINCT", length = 10)
    private String precinct;
    @Column(name = "SUB_TYPE", length = 10, nullable = false)
    private String subType;
    @Column(name = "CURRENT_STEP_ID", length = 10, precision = 10, scale = 0)
    private Long currentStepId;
    @Column(name = "QUOTA", length = 22, precision = 22, scale = 0)
    private Long quota;
    @Column(name = "PROMOTION_CODE", length = 10)
    private String promotionCode;
    @Column(name = "REG_TYPE", length = 10)
    private String regType;
    @Column(name = "REASON_DEPOSIT_ID", length = 22, precision = 22, scale = 0)
    private Long reasonDepositId;
    @Column(name = "REQ_OFFER_ID", length = 10, precision = 10, scale = 0)
    private Long reqOfferId;
    @Id
    @Column(name = "ID", length = 10, precision = 10, scale = 0)
    private Long id;
    @Column(name = "IS_NEW_SUB")
    private Long isNewSub;
    @Column(name = "IP_STATIC", length = 15)
    private String ipStatic;
    @Column(name = "IS_SATISFY")
    private Long isSatisfy;
    @Column(name = "USER_USING", length = 200)
    private String userUsing;
    @Column(name = "USER_TITLE", length = 50)
    private String userTitle;
    @Column(name = "CHARSIC", length = 100)
    private String charsic;
    @Column(name = "SLOT_CARD", length = 100)
    private String slotCard;
    @Column(name = "PRODUCT_CODE", length = 10)
    private String productCode;
    @Column(name = "SERVICE_TYPE", length = 10)
    private String serviceType;
    @Column(name = "STREET")
    private String street;
    @Column(name = "STREET_NAME", length = 50)
    private String streetName;
    @Column(name = "STREET_BLOCK", length = 5)
    private String streetBlock;
    @Column(name = "STREET_BLOCK_NAME", length = 50)
    private String streetBlockName;
    @Column(name = "HOME", length = 20)
    private String home;
    @Column(name = "LIMIT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date limitDate;
    @Column(name = "PRICE_PLAN", length = 19, precision = 19, scale = 0)
    private Long pricePlan;
    @Column(name = "LOCAL_PRICE_PLAN", length = 19, precision = 19, scale = 0)
    private Long localPricePlan;
    @Column(name = "EMAIL", length = 100)
    private String email;
    @Column(name = "TEL_FAX", length = 15)
    private String telFax;
    @Column(name = "TEL_MOBILE", length = 15)
    private String telMobile;
    @Column(name = "NICK_NAME", length = 50)
    private String nickName;
    @Column(name = "NICK_DOMAIN", length = 5)
    private String nickDomain;
    @Column(name = "PROJECT", length = 10, precision = 10, scale = 0)
    private Long project;
    @Column(name = "STATION_ID", length = 10, precision = 10, scale = 0)
    private Long stationId;
    @Column(name = "SOURCE_TYPE")
    private String sourceType;
    @Column(name = "REQ_TYPE")
    private String reqType;
    @Column(name = "LAT")
    private Double lat;
    @Column(name = "LNG")
    private Double lng;
    @Column(name = "IS_SMART")
    private Long isSmart;
    @Column(name = "AMOUNT_BOX", length = 25, precision = 10, scale = 15)
    private Double amountBox;
    @Column(name = "AMOUNT_CORE", length = 25, precision = 10, scale = 15)
    private Double amountCore;
    @Column(name = "AMOUNT_PRODUCT", length = 25, precision = 10, scale = 15)
    private Double amountProduct;
    @Column(name = "INFRA_TYPE", length = 10)
    private String infraType;
    @Column(name = "INFRA_LAT")
    private Double infraLat;
    @Column(name = "INFRA_LONG")
    private Double infraLong;
    @Column(name = "ACC_BUNDLE_TV", length = 50)
    private String accBundleTv;
    @Column(name = "req_online_id", length = 10, precision = 10, scale = 0)
    private Long reqOnlineId;

    public String getInfraType() {
        return infraType;
    }

    public void setInfraType(String infraType) {
        this.infraType = infraType;
    }

    public SubReqAdslLl() {
    }

    public Double getAmountBox() {
        return amountBox;
    }

    public void setAmountBox(Double amountBox) {
        this.amountBox = amountBox;
    }

    public Double getAmountCore() {
        return amountCore;
    }

    public void setAmountCore(Double amountCore) {
        this.amountCore = amountCore;
    }

    public Double getAmountProduct() {
        return amountProduct;
    }

    public void setAmountProduct(Double amountProduct) {
        this.amountProduct = amountProduct;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Long getIsSmart() {
        return isSmart;
    }

    public void setIsSmart(Long isSmart) {
        this.isSmart = isSmart;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getCustReqId() {
        return custReqId;
    }

    public void setCustReqId(Long custReqId) {
        this.custReqId = custReqId;
    }

    public String getIb() {
        return ib;
    }

    public void setIb(String ib) {
        this.ib = ib;
    }

    public Long getCableBoxId() {
        return cableBoxId;
    }

    public void setCableBoxId(Long cableBoxId) {
        this.cableBoxId = cableBoxId;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getCabLen() {
        return cabLen;
    }

    public void setCabLen(String cabLen) {
        this.cabLen = cabLen;
    }

    public String getPortNo() {
        return portNo;
    }

    public void setPortNo(String portNo) {
        this.portNo = portNo;
    }

    public Long getDslamId() {
        return dslamId;
    }

    public void setDslamId(Long dslamId) {
        this.dslamId = dslamId;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Date getFirstConnect() {
        return firstConnect;
    }

    public void setFirstConnect(Date firstConnect) {
        this.firstConnect = firstConnect;
    }

    public String getDeployAddress() {
        return deployAddress;
    }

    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    public String getDeployAreaCode() {
        return deployAreaCode;
    }

    public void setDeployAreaCode(String deployAreaCode) {
        this.deployAreaCode = deployAreaCode;
    }

    public String getLinePhone() {
        return linePhone;
    }

    public void setLinePhone(String linePhone) {
        this.linePhone = linePhone;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public Long getSpeed() {
        return speed;
    }

    public void setSpeed(Long speed) {
        this.speed = speed;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getAddressCode() {
        return addressCode;
    }

    public void setAddressCode(String addressCode) {
        this.addressCode = addressCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getDeposit() {
        return deposit;
    }

    public void setDeposit(Double deposit) {
        this.deposit = deposit;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public String getActStatus() {
        return actStatus;
    }

    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    public Date getStaDatetime() {
        return staDatetime;
    }

    public void setStaDatetime(Date staDatetime) {
        this.staDatetime = staDatetime;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public Long getFinishReasonId() {
        return finishReasonId;
    }

    public void setFinishReasonId(Long finishReasonId) {
        this.finishReasonId = finishReasonId;
    }

    public Long getRegReasonId() {
        return regReasonId;
    }

    public void setRegReasonId(Long regReasonId) {
        this.regReasonId = regReasonId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public Long getCurrentStepId() {
        return currentStepId;
    }

    public void setCurrentStepId(Long currentStepId) {
        this.currentStepId = currentStepId;
    }

    public Long getQuota() {
        return quota;
    }

    public void setQuota(Long quota) {
        this.quota = quota;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    public String getRegType() {
        return regType;
    }

    public void setRegType(String regType) {
        this.regType = regType;
    }

    public Long getReasonDepositId() {
        return reasonDepositId;
    }

    public void setReasonDepositId(Long reasonDepositId) {
        this.reasonDepositId = reasonDepositId;
    }

    public Long getReqOfferId() {
        return reqOfferId;
    }

    public void setReqOfferId(Long reqOfferId) {
        this.reqOfferId = reqOfferId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIsNewSub() {
        return isNewSub;
    }

    public void setIsNewSub(Long isNewSub) {
        this.isNewSub = isNewSub;
    }

    public String getIpStatic() {
        return ipStatic;
    }

    public void setIpStatic(String ipStatic) {
        this.ipStatic = ipStatic;
    }

    public Long getIsSatisfy() {
        return isSatisfy;
    }

    public void setIsSatisfy(Long isSatisfy) {
        this.isSatisfy = isSatisfy;
    }

    public String getUserUsing() {
        return userUsing;
    }

    public void setUserUsing(String userUsing) {
        this.userUsing = userUsing;
    }

    public String getUserTitle() {
        return userTitle;
    }

    public void setUserTitle(String userTitle) {
        this.userTitle = userTitle;
    }

    public String getCharsic() {
        return charsic;
    }

    public void setCharsic(String charsic) {
        this.charsic = charsic;
    }

    public String getSlotCard() {
        return slotCard;
    }

    public void setSlotCard(String slotCard) {
        this.slotCard = slotCard;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetBlock() {
        return streetBlock;
    }

    public void setStreetBlock(String streetBlock) {
        this.streetBlock = streetBlock;
    }

    public String getStreetBlockName() {
        return streetBlockName;
    }

    public void setStreetBlockName(String streetBlockName) {
        this.streetBlockName = streetBlockName;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public Date getLimitDate() {
        return limitDate;
    }

    public void setLimitDate(Date limitDate) {
        this.limitDate = limitDate;
    }

    public Long getPricePlan() {
        return pricePlan;
    }

    public void setPricePlan(Long pricePlan) {
        this.pricePlan = pricePlan;
    }

    public Long getLocalPricePlan() {
        return localPricePlan;
    }

    public void setLocalPricePlan(Long localPricePlan) {
        this.localPricePlan = localPricePlan;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelFax() {
        return telFax;
    }

    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    public String getTelMobile() {
        return telMobile;
    }

    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickDomain() {
        return nickDomain;
    }

    public void setNickDomain(String nickDomain) {
        this.nickDomain = nickDomain;
    }

    public Long getProject() {
        return project;
    }

    public void setProject(Long project) {
        this.project = project;
    }

    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getReqType() {
        return reqType;
    }

    public void setReqType(String reqType) {
        this.reqType = reqType;
    }

    /**
     * @return the infraLat
     */
    public Double getInfraLat() {
        return infraLat;
    }

    /**
     * @param infraLat the infraLat to set
     */
    public void setInfraLat(Double infraLat) {
        this.infraLat = infraLat;
    }

    /**
     * @return the infraLong
     */
    public Double getInfraLong() {
        return infraLong;
    }

    /**
     * @param infraLong the infraLong to set
     */
    public void setInfraLong(Double infraLong) {
        this.infraLong = infraLong;
    }

    /**
     * @return the accBundleTv
     */
    public String getAccBundleTv() {
        return accBundleTv;
    }

    /**
     * @param accBundleTv the accBundleTv to set
     */
    public void setAccBundleTv(String accBundleTv) {
        this.accBundleTv = accBundleTv;
    }

    /**
     * @return the reqOnlineId
     */
    public Long getReqOnlineId() {
        return reqOnlineId;
    }

    /**
     * @param reqOnlineId the reqOnlineId to set
     */
    public void setReqOnlineId(Long reqOnlineId) {
        this.reqOnlineId = reqOnlineId;
    }
}
