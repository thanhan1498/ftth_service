package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;

public class SubReqDeployment implements Serializable {

    private String province;
    private String district;
    private String precinct;
    private String streetBlock;
    private String streetName;
    private String home;
    private String address;
    private String areaCode;
    private Long subReqDepId;
    private String odfOutdor;
    private Long RadiusCust;
    private Long subId;
    private String actionId;
    private Date createDate;
    private Long status;
    private Date issueDate;
    private Long LineNo;
    private Long portNo;
    private Long cableBoxId;
    private String cableBoxCode;
    private String cableBoxType;
    private String stationCode;
    private Long stageId;
    private Long cabLen;
    private Long boardId;
    private String error;
    private Long feeAmount;
    private Date acceptedDate;
    private Long posId;
    private Long staffId;
    private Long updaterUserId;
    private Long updaterPosId;
    private Long sourceId;
    private Long productId;
    private Long reqId;
    private String channelSpeed;
    private Long odfIndor;
    private String portSpeed;
    private String ip;
    private String subnetMask;
    private Long taskId;
    private Date reqDatetime;
    private Long subReqId;
    private String deployAddressCode;
    private String deployAddress;
    private String teamCode;
    private Long sourceType;
    private String interfaceDevice;
    private String userUsing;
    private String telFax;
    private String telMobile;
    private String email;
    private String typeCrc;// loai crc
    private String typeSignal;// loai bao hieu
    private String trunkId;
    private String description;
    private SubReqDeployment originalSubReqDeployment;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SubReqDeployment() {
    }

    public String getTrunkId() {
        return trunkId;
    }

    public void setTrunkId(String trunkId) {
        this.trunkId = trunkId;
    }

    public Long getSubReqDepId() {
        return subReqDepId;
    }

    public void setSubReqDepId(Long subReqDepId) {
        this.subReqDepId = subReqDepId;
    }

    public String getOdfOutdor() {
        return odfOutdor;
    }

    public void setOdfOutdor(String odfOutdor) {
        this.odfOutdor = odfOutdor;
    }

    public Long getRadiusCust() {
        return RadiusCust;
    }

    public void setRadiusCust(Long RadiusCust) {
        this.RadiusCust = RadiusCust;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Long getLineNo() {
        return LineNo;
    }

    public void setLineNo(Long LineNo) {
        this.LineNo = LineNo;
    }

    public Long getPortNo() {
        return portNo;
    }

    public void setPortNo(Long portNo) {
        this.portNo = portNo;
    }

    public Long getCableBoxId() {
        return cableBoxId;
    }

    public void setCableBoxId(Long cableBoxId) {
        this.cableBoxId = cableBoxId;
    }

    public String getCableBoxCode() {
        return cableBoxCode;
    }

    public void setCableBoxCode(String cableBoxCode) {
        this.cableBoxCode = cableBoxCode;
    }

    public String getCableBoxType() {
        return cableBoxType;
    }

    public void setCableBoxType(String cableBoxType) {
        this.cableBoxType = cableBoxType;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public Long getStageId() {
        return stageId;
    }

    public void setStageId(Long stageId) {
        this.stageId = stageId;
    }

    public Long getCabLen() {
        return cabLen;
    }

    public void setCabLen(Long cabLen) {
        this.cabLen = cabLen;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Long getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(Long feeAmount) {
        this.feeAmount = feeAmount;
    }

    public Date getAcceptedDate() {
        return acceptedDate;
    }

    public void setAcceptedDate(Date acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    public Long getPosId() {
        return posId;
    }

    public void setPosId(Long posId) {
        this.posId = posId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Long getUpdaterUserId() {
        return updaterUserId;
    }

    public void setUpdaterUserId(Long updaterUserId) {
        this.updaterUserId = updaterUserId;
    }

    public Long getUpdaterPosId() {
        return updaterPosId;
    }

    public void setUpdaterPosId(Long updaterPosId) {
        this.updaterPosId = updaterPosId;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getReqId() {
        return reqId;
    }

    public void setReqId(Long reqId) {
        this.reqId = reqId;
    }

    public String getChannelSpeed() {
        return channelSpeed;
    }

    public void setChannelSpeed(String channelSpeed) {
        this.channelSpeed = channelSpeed;
    }

    public Long getOdfIndor() {
        return odfIndor;
    }

    public void setOdfIndor(Long odfIndor) {
        this.odfIndor = odfIndor;
    }

    public String getPortSpeed() {
        return portSpeed;
    }

    public void setPortSpeed(String portSpeed) {
        this.portSpeed = portSpeed;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getSubnetMask() {
        return subnetMask;
    }

    public void setSubnetMask(String subnetMask) {
        this.subnetMask = subnetMask;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Date getReqDatetime() {
        return reqDatetime;
    }

    public void setReqDatetime(Date reqDatetime) {
        this.reqDatetime = reqDatetime;
    }

    public Long getSubReqId() {
        return subReqId;
    }

    public void setSubReqId(Long subReqId) {
        this.subReqId = subReqId;
    }

    public String getDeployAddress() {
        return deployAddress;
    }

    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    public String getDeployAddressCode() {
        return deployAddressCode;
    }

    public void setDeployAddressCode(String deployAddressCode) {
        this.deployAddressCode = deployAddressCode;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getStreetBlock() {
        return streetBlock;
    }

    public void setStreetBlock(String streetBlock) {
        this.streetBlock = streetBlock;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getTeamCode() {
        return teamCode;
    }

    public void setTeamCode(String teamCode) {
        this.teamCode = teamCode;
    }

    public Long getSourceType() {
        return sourceType;
    }

    public void setSourceType(Long sourceType) {
        this.sourceType = sourceType;
    }

    public SubReqDeployment getOriginalSubReqDeployment() {
        return originalSubReqDeployment;
    }

    public void setOriginalSubReqDeployment(SubReqDeployment originalSubReqDeployment) {
        this.originalSubReqDeployment = originalSubReqDeployment;
    }

    public String getInterfaceDevice() {
        return interfaceDevice;
    }

    public void setInterfaceDevice(String interfaceDevice) {
        this.interfaceDevice = interfaceDevice;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelFax() {
        return telFax;
    }

    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    public String getTelMobile() {
        return telMobile;
    }

    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }

    public String getUserUsing() {
        return userUsing;
    }

    public void setUserUsing(String userUsing) {
        this.userUsing = userUsing;
    }

    public String getTypeCrc() {
        return typeCrc;
    }

    public void setTypeCrc(String typeCrc) {
        this.typeCrc = typeCrc;
    }

    public String getTypeSignal() {
        return typeSignal;
    }

    public void setTypeSignal(String typeSignal) {
        this.typeSignal = typeSignal;
    }
}
