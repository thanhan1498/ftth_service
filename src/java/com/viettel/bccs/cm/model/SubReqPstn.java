package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author linhlh2
 */
@Entity
@Table(name = "SUB_REQ_PSTN")
public class SubReqPstn extends ReqSubscriber implements Serializable {

    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "CUST_REQ_ID", length = 10, precision = 10, scale = 0, nullable = false)
    private Long custReqId;
    @Column(name = "SUB_TYPE", length = 10, nullable = false)
    private String subType;
    @Column(name = "DEPLOY_ADDRESS", length = 500)
    private String deployAddress;
    @Column(name = "LINE_TYPE", length = 10, nullable = false)
    private String lineType;
    @Column(name = "ACCOUNT", length = 100)
    private String account;
    @Column(name = "DEPLOY_AREA_CODE", length = 50)
    private String deployAreaCode;
    @Column(name = "ISDN", length = 20, nullable = false)
    private String isdn;
    @Column(name = "TASK_ID", length = 10, precision = 10, scale = 0)
    private Long taskId;
    @Column(name = "ADDRESS_CODE", length = 50, nullable = false)
    private String addressCode;
    @Column(name = "ADDRESS", length = 500, nullable = false)
    private String address;
    @Column(name = "DEPOSIT", length = 16, precision = 16, scale = 4)
    private Double deposit;
    @Column(name = "STATUS", nullable = false)
    private Long status;
    @Column(name = "USER_CREATED", length = 30, nullable = false)
    private String userCreated;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "VIP", length = 1, nullable = false)
    private String vip;
    @Column(name = "ACT_STATUS", length = 3, nullable = false)
    private String actStatus;
    @Column(name = "STA_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date staDatetime;
    @Column(name = "END_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDatetime;
    @Column(name = "SHOP_CODE", length = 50, nullable = false)
    private String shopCode;
    @Column(name = "FINISH_REASON_ID", length = 10, precision = 10, scale = 0)
    private Long finishReasonId;
    @Column(name = "REG_REASON_ID", length = 10, precision = 10, scale = 0, nullable = false)
    private Long regReasonId;
    @Column(name = "PROVINCE", length = 10)
    private String province;
    @Column(name = "DISTRICT", length = 10)
    private String district;
    @Column(name = "PRECINCT", length = 10)
    private String precinct;
    @Column(name = "CURRENT_STEP_ID", length = 10, precision = 10, scale = 0)
    private Long currentStepId;
    @Column(name = "QUOTA", length = 22, precision = 22, scale = 0)
    private Long quota;
    @Column(name = "PROMOTION_CODE", length = 4)
    private String promotionCode;
    @Column(name = "REG_TYPE", length = 10)
    private String regType;
    @Column(name = "REASON_DEPOSIT_ID", length = 22, precision = 22, scale = 0)
    private Long reasonDepositId;
    @Column(name = "REQ_OFFER_ID", length = 10, precision = 10, scale = 0)
    private Long reqOfferId;
    @Id
    @Column(name = "ID", length = 10, precision = 10, scale = 0)
    private Long id;
    @Column(name = "IS_NEW_SUB")
    private Long isNewSub;
    @Column(name = "DLU_ID")
    private Long dluId;
    @Column(name = "IS_SATISFY")
    private Long isSatisfy;
    @Column(name = "USER_USING", length = 200)
    private String userUsing;
    @Column(name = "USER_TITLE", length = 50)
    private String userTitle;
    @Column(name = "BOARD_ID", length = 10, precision = 10, scale = 0)
    private Long boardId;
    @Column(name = "CABLE_BOX_ID", length = 10, precision = 10, scale = 0)
    private Long cableBoxId;
    @Column(name = "PORT_NO", length = 10, precision = 10, scale = 0)
    private Long portNo;
    @Column(name = "STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long staffId;
    @Column(name = "PASSWORD", length = 20)
    private String password;
    @Column(name = "PRODUCT_CODE", length = 10)
    private String productCode;
    @Column(name = "TEAM_ID", length = 10, precision = 10, scale = 0)
    private Long teamId;
    @Column(name = "STREET", length = 5)
    private String street;
    @Column(name = "STREET_NAME", length = 50)
    private String streetName;
    @Column(name = "STREET_BLOCK", length = 5)
    private String streetBlock;
    @Column(name = "STREET_BLOCK_NAME", length = 10)
    private String streetBlockName;
    @Column(name = "HOME", length = 20)
    private String home;
    @Column(name = "LIMIT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date limitDate;
    @Column(name = "TEL_FAX", length = 15)
    private String telFax;
    @Column(name = "TEL_MOBILE", length = 15)
    private String telMobile;
    @Column(name = "EMAIL", length = 100)
    private String email;
    @Column(name = "NICK_NAME", length = 50)
    private String nickName;
    @Column(name = "NICK_DOMAIN", length = 5)
    private String nickDomain;
    @Column(name = "PROJECT", length = 10, precision = 10, scale = 0)
    private Long project;

    public SubReqPstn() {
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Long getCustReqId() {
        return custReqId;
    }

    public void setCustReqId(Long custReqId) {
        this.custReqId = custReqId;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getDeployAddress() {
        return deployAddress;
    }

    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getDeployAreaCode() {
        return deployAreaCode;
    }

    public void setDeployAreaCode(String deployAreaCode) {
        this.deployAreaCode = deployAreaCode;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getAddressCode() {
        return addressCode;
    }

    public void setAddressCode(String addressCode) {
        this.addressCode = addressCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getDeposit() {
        return deposit;
    }

    public void setDeposit(Double deposit) {
        this.deposit = deposit;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public String getActStatus() {
        return actStatus;
    }

    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    public Date getStaDatetime() {
        return staDatetime;
    }

    public void setStaDatetime(Date staDatetime) {
        this.staDatetime = staDatetime;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public Long getFinishReasonId() {
        return finishReasonId;
    }

    public void setFinishReasonId(Long finishReasonId) {
        this.finishReasonId = finishReasonId;
    }

    public Long getRegReasonId() {
        return regReasonId;
    }

    public void setRegReasonId(Long regReasonId) {
        this.regReasonId = regReasonId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public Long getCurrentStepId() {
        return currentStepId;
    }

    public void setCurrentStepId(Long currentStepId) {
        this.currentStepId = currentStepId;
    }

    public Long getQuota() {
        return quota;
    }

    public void setQuota(Long quota) {
        this.quota = quota;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    public String getRegType() {
        return regType;
    }

    public void setRegType(String regType) {
        this.regType = regType;
    }

    public Long getReasonDepositId() {
        return reasonDepositId;
    }

    public void setReasonDepositId(Long reasonDepositId) {
        this.reasonDepositId = reasonDepositId;
    }

    public Long getReqOfferId() {
        return reqOfferId;
    }

    public void setReqOfferId(Long reqOfferId) {
        this.reqOfferId = reqOfferId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIsNewSub() {
        return isNewSub;
    }

    public void setIsNewSub(Long isNewSub) {
        this.isNewSub = isNewSub;
    }

    public Long getDluId() {
        return dluId;
    }

    public void setDluId(Long dluId) {
        this.dluId = dluId;
    }

    public Long getIsSatisfy() {
        return isSatisfy;
    }

    public void setIsSatisfy(Long isSatisfy) {
        this.isSatisfy = isSatisfy;
    }

    public String getUserUsing() {
        return userUsing;
    }

    public void setUserUsing(String userUsing) {
        this.userUsing = userUsing;
    }

    public String getUserTitle() {
        return userTitle;
    }

    public void setUserTitle(String userTitle) {
        this.userTitle = userTitle;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Long getCableBoxId() {
        return cableBoxId;
    }

    public void setCableBoxId(Long cableBoxId) {
        this.cableBoxId = cableBoxId;
    }

    public Long getPortNo() {
        return portNo;
    }

    public void setPortNo(Long portNo) {
        this.portNo = portNo;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetBlock() {
        return streetBlock;
    }

    public void setStreetBlock(String streetBlock) {
        this.streetBlock = streetBlock;
    }

    public String getStreetBlockName() {
        return streetBlockName;
    }

    public void setStreetBlockName(String streetBlockName) {
        this.streetBlockName = streetBlockName;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public Date getLimitDate() {
        return limitDate;
    }

    public void setLimitDate(Date limitDate) {
        this.limitDate = limitDate;
    }

    public String getTelFax() {
        return telFax;
    }

    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    public String getTelMobile() {
        return telMobile;
    }

    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickDomain() {
        return nickDomain;
    }

    public void setNickDomain(String nickDomain) {
        this.nickDomain = nickDomain;
    }

    public Long getProject() {
        return project;
    }

    public void setProject(Long project) {
        this.project = project;
    }
}
