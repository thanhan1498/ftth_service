package com.viettel.bccs.cm.model;

import com.viettel.im.database.BO.APStockModelBean;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SubRequest implements java.io.Serializable {

    private Long subReqId;
    private Long subId;
    private Long taskId;
    private Long staffId;
    private Date firstConnect;
    private String deployAddress;
    private String deployAreaCode;
    private String linePhone;
    private String lineType;
    private Long speed;
    private Long localSpeed;//toc do trong nuoc
    private String password;
    private String account;
    private String isdn;
    private Long productId;
    private Long deposit;
    private Long status;
    private Long statusTmp;
    private String userCreated;
    private Date createDate;
    private String vip;
    private Date staDatetime;
    private Date endDatetime;
    private String shopCode;
    private Long finishReasonId;
    private Long regReasonId;
    private String subType;
    private Long currentStepId;
    private Long quota;
    private String promotionCode;
    private String regType;
    private Long reasonDepositeId;
    private String ipStatic;
    private Long isSatisfy;
    private String userUsing;
    private String userTitle;
    private Long sourceCode;
    private String serviceType;
    private Long contractId;
    private String productCode;
    private Long mainSubId;
    private Long offerId;
    private Long custId;
    private String progress;
    private String technology;
    private String telFax;
    private String telMobile;
    private String email;
    private Long subGroup;
    private Date limitDate;
    private Long parentSubId;
    private String parentAccount;
    private Long groupReqId;
    private Long groupAccountId;
    private Long gponGroupAccountId;
    private Customer customer;
    private Contract contract;
    private Subscriber subscriber;
    private Subscriber subscriberPPPOE;
    private SubRequest subRequest;
    private SubRequest subRequestPPPOE;
    private Long objectFileId;
//    private ObjectFile objectFile;
    private Long upLink;
    private Long pricePlan;
    private Long localPricePlan;
    private String hasStockModel;
    //dia chi ip
    private String ipLan;
    private String ipWan;
    private String ipGateway;
    private String ipRouter;
    private Long imRequest;
    private String vlan;
    private Long numOfComputer;
    private Long pricePlanId;
    private String toIsdn;
    private String fromIsdn;
    private String fileName;// ten file cho file attach 
    private String pathFileAttach;// duong dan file
    private String parentServiceType;// dịch vụ nền
    private String contractNo;
    private Long statusSdn;
    private Long statusSdnTmp;
    private Long actionAuditId;
    private Long notificationLevel;
    private String type;
    private String gponRelationType;
    private boolean allowEditRequest;
    private List<SubRequest> lstSubRequestByGroup;
    private List<SubRequest> lstChildSubId;
    private String[] itemServicesId;
//    private List<MpServiceFee> mpServiceFeeList;
    private List<APStockModelBean> apStockModelBeans;
    private List<SubStockModelRelReq> stockModelRelReqs = new ArrayList<SubStockModelRelReq>();
//    private List<SubReqDeployment> subReqDeployments = new ArrayList<SubReqDeployment>();
//    private List<SubConnection> connectionPoints = new ArrayList<SubConnection>();
//    private AllTelServiceSub allTelServiceSub;
//    private List<ActionDetail> actionDetails = new ArrayList<ActionDetail>();
//    private List<SubRelation> subRelations = new ArrayList<SubRelation>();
//    private GroupAccount gponGroupAccount;
//    private SubRelation gponsubRelation;
    private String vasCodeTK;
    private boolean needRetake = false;
    private String retakeAccount;

    public String getVasCodeTK() {
        return vasCodeTK;
    }

    public void setVasCodeTK(String vasCodeTK) {
        this.vasCodeTK = vasCodeTK;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGponRelationType() {
        return gponRelationType;
    }

    public void setGponRelationType(String gponRelationType) {
        this.gponRelationType = gponRelationType;
    }

//    public SubRelation getGponsubRelation() {
//        return gponsubRelation;
//    }
//
//    public void setGponsubRelation(SubRelation gponsubRelation) {
//        this.gponsubRelation = gponsubRelation;
//    }
//
//    public GroupAccount getGponGroupAccount() {
//        return gponGroupAccount;
//    }
//
//    public void setGponGroupAccount(GroupAccount gponGroupAccount) {
//        this.gponGroupAccount = gponGroupAccount;
//    }
    public Long getGponGroupAccountId() {
        return gponGroupAccountId;
    }

    public void setGponGroupAccountId(Long gponGroupAccountId) {
        this.gponGroupAccountId = gponGroupAccountId;
    }

    public Long getNotificationLevel() {
        return notificationLevel;
    }

    public void setNotificationLevel(Long notificationLevel) {
        this.notificationLevel = notificationLevel;
    }

    public Long getStatusTmp() {
        return statusTmp;
    }

    public void setStatusTmp(Long statusTmp) {
        this.statusTmp = statusTmp;
    }

    public Long getStatusSdnTmp() {
        return statusSdnTmp;
    }

    public void setStatusSdnTmp(Long statusSdnTmp) {
        this.statusSdnTmp = statusSdnTmp;
    }

    public String getParentServiceType() {
        return parentServiceType;
    }

    public void setParentServiceType(String parentServiceType) {
        this.parentServiceType = parentServiceType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPathFileAttach() {
        return pathFileAttach;
    }

    public void setPathFileAttach(String pathFileAttach) {
        this.pathFileAttach = pathFileAttach;
    }

    public List<SubStockModelRelReq> getStockModelRelReqs() {
        return stockModelRelReqs;
    }

    public void setStockModelRelReqs(List<SubStockModelRelReq> stockModelRelReqs) {
        this.stockModelRelReqs = stockModelRelReqs;
    }

    public List<SubRequest> getLstChildSubId() {
        return lstChildSubId;
    }

    public void setLstChildSubId(List<SubRequest> lstChildSubId) {
        this.lstChildSubId = lstChildSubId;
    }

    public List<SubRequest> getLstSubRequestByGroup() {
        return lstSubRequestByGroup;
    }

    public void setLstSubRequestByGroup(List<SubRequest> lstSubRequestByGroup) {
        this.lstSubRequestByGroup = lstSubRequestByGroup;
    }

    public Long getImRequest() {
        return imRequest;
    }

    public void setImRequest(Long imRequest) {
        this.imRequest = imRequest;
    }

    public String getAccount() {
        return account;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCurrentStepId() {
        return currentStepId;
    }

    public void setCurrentStepId(Long currentStepId) {
        this.currentStepId = currentStepId;
    }

    public String getDeployAddress() {
        return deployAddress;
    }

    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    public String getDeployAreaCode() {
        return deployAreaCode;
    }

    public void setDeployAreaCode(String deployAreaCode) {
        this.deployAreaCode = deployAreaCode;
    }

    public Long getDeposit() {
        return deposit;
    }

    public void setDeposit(Long deposit) {
        this.deposit = deposit;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public Long getFinishReasonId() {
        return finishReasonId;
    }

    public void setFinishReasonId(Long finishReasonId) {
        this.finishReasonId = finishReasonId;
    }

    public Date getFirstConnect() {
        return firstConnect;
    }

    public void setFirstConnect(Date firstConnect) {
        this.firstConnect = firstConnect;
    }

    public String getIpStatic() {
        return ipStatic;
    }

    public void setIpStatic(String ipStatic) {
        this.ipStatic = ipStatic;
    }

    public Long getIsSatisfy() {
        return isSatisfy;
    }

    public void setIsSatisfy(Long isSatisfy) {
        this.isSatisfy = isSatisfy;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getLinePhone() {
        return linePhone;
    }

    public void setLinePhone(String linePhone) {
        this.linePhone = linePhone;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    public Long getQuota() {
        return quota;
    }

    public void setQuota(Long quota) {
        this.quota = quota;
    }

    public Long getReasonDepositeId() {
        return reasonDepositeId;
    }

    public void setReasonDepositeId(Long reasonDepositeId) {
        this.reasonDepositeId = reasonDepositeId;
    }

    public Long getRegReasonId() {
        return regReasonId;
    }

    public void setRegReasonId(Long regReasonId) {
        this.regReasonId = regReasonId;
    }

    public String getRegType() {
        return regType;
    }

    public void setRegType(String regType) {
        this.regType = regType;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public Long getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(Long sourceCode) {
        this.sourceCode = sourceCode;
    }

    public Long getSpeed() {
        return speed;
    }

    public void setSpeed(Long speed) {
        this.speed = speed;
    }

    public Long getLocalSpeed() {
        return localSpeed;
    }

    public void setLocalSpeed(Long localSpeed) {
        this.localSpeed = localSpeed;
    }

    public Date getStaDatetime() {
        return staDatetime;
    }

    public void setStaDatetime(Date staDatetime) {
        this.staDatetime = staDatetime;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Long getSubReqId() {
        return subReqId;
    }

    public void setSubReqId(Long subReqId) {
        this.subReqId = subReqId;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserTitle() {
        return userTitle;
    }

    public void setUserTitle(String userTitle) {
        this.userTitle = userTitle;
    }

    public String getUserUsing() {
        return userUsing;
    }

    public void setUserUsing(String userUsing) {
        this.userUsing = userUsing;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Long getMainSubId() {
        return mainSubId;
    }

    public void setMainSubId(Long mainSubId) {
        this.mainSubId = mainSubId;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Subscriber subscriber) {
        this.subscriber = subscriber;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getTelFax() {
        return telFax;
    }

    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    public String getTelMobile() {
        return telMobile;
    }

    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getSubGroup() {
        return subGroup;
    }

    public void setSubGroup(Long subGroup) {
        this.subGroup = subGroup;
    }

//    public SubReqDeployment getSubReqDeployment() {
//        if (this.subReqDeployments == null || this.subReqDeployments.isEmpty()) {
//            return null;
//        } else {
//            return this.subReqDeployments.get(0);
//        }
//    }
//
//    public void setSubReqDeployment(SubReqDeployment subReqDeployment) {
//        this.subReqDeployments = new ArrayList<SubReqDeployment>();
//        if (subReqDeployment != null) {
//            this.subReqDeployments.add(subReqDeployment);
//        }
//    }
    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public Date getLimitDate() {
        return limitDate;
    }

    public void setLimitDate(Date limitDate) {
        this.limitDate = limitDate;
    }

//    public String getStatusName() {
//        return SubRequestSupplier.getReqStatusName(this.status);
//    }
//
//    public String getProgressName() {
//        return SubRequestSupplier.getProgressName(this.progress);
//    }
//    public String getServiceTypeName() {
//        TelecomService telecomService = TelecomServiceBusiness.getTelecomServiceByServiceAlias(this.serviceType);
//        return (telecomService == null) ? this.serviceType : telecomService.getTelServiceName();
//    }
//
//    public List<SubReqDeployment> getSubReqDeployments() {
//        return subReqDeployments;
//    }
//    public void setSubReqDeployments(List<SubReqDeployment> subReqDeployments) {
//        this.subReqDeployments = subReqDeployments;
//    }
    public Long getParentSubId() {
        return parentSubId;
    }

    public void setParentSubId(Long parentSubId) {
        this.parentSubId = parentSubId;
    }

    public String getParentAccount() {
        return parentAccount;
    }

    public void setParentAccount(String parentAccount) {
        this.parentAccount = parentAccount;
    }

    public Long getGroupAccountId() {
        return groupAccountId;
    }

    public void setGroupAccountId(Long groupAccountId) {
        this.groupAccountId = groupAccountId;
    }

    public Long getGroupReqId() {
        return groupReqId;
    }

    public void setGroupReqId(Long groupReqId) {
        this.groupReqId = groupReqId;
    }

    public SubRequest getSubRequest() {
        return subRequest;
    }

    public void setSubRequest(SubRequest subRequest) {
        this.subRequest = subRequest;
    }

    public Long getObjectFileId() {
        return objectFileId;
    }

    public void setObjectFileId(Long objectFileId) {
        this.objectFileId = objectFileId;
    }

//    public ObjectFile getObjectFile() {
//        return objectFile;
//    }
//
//    public void setObjectFile(ObjectFile objectFile) {
//        this.objectFile = objectFile;
//    }
    public Long getUpLink() {
        return upLink;
    }

    public void setUpLink(Long upLink) {
        if (upLink == null) {
            upLink = 0L;
        }
        this.upLink = upLink;
    }

    public Long getLocalPricePlan() {
        return localPricePlan;
    }

    public void setLocalPricePlan(Long localPricePlan) {
        this.localPricePlan = localPricePlan;
    }

    public Long getPricePlan() {
        return pricePlan;
    }

    public void setPricePlan(Long pricePlan) {
        this.pricePlan = pricePlan;
    }

    public String getHasStockModel() {
        return hasStockModel;
    }

    public void setHasStockModel(String hasStockModel) {
        this.hasStockModel = hasStockModel;
    }

//    public List<SubConnection> getConnectionPoints() {
//        if (this.connectionPoints == null) {
//            this.connectionPoints = new ArrayList<SubConnection>();
//        }
//        return this.connectionPoints;
//
//    }
//
//    public SubConnection getConnectionPointByIndex(int index) {
//        if (this.connectionPoints == null || this.connectionPoints.isEmpty()) {
//            return null;
//        } else {
//            return this.connectionPoints.get(index);
//        }
//    }
//
//    public void addConnectionPoint(SubConnection subConnection) {
//        if (subConnection == null) {
//            this.connectionPoints = new ArrayList<SubConnection>();
//        }
//        this.connectionPoints.add(subConnection);
//    }
    public String getIpLan() {
        return ipLan;
    }

    public void setIpLan(String ipLan) {
        this.ipLan = ipLan;
    }

    public String getIpWan() {
        return ipWan;
    }

    public void setIpWan(String ipWan) {
        this.ipWan = ipWan;
    }

    public String getIpGateway() {
        return ipGateway;
    }

    public void setIpGateway(String ipGateway) {
        this.ipGateway = ipGateway;
    }

    public String getIpRouter() {
        return ipRouter;
    }

    public void setIpRouter(String ipRouter) {
        this.ipRouter = ipRouter;
    }

    public String getVlan() {
        return vlan;
    }

    public void setVlan(String vlan) {
        this.vlan = vlan;
    }

    public Long getNumOfComputer() {
        return numOfComputer;
    }

    public void setNumOfComputer(Long numOfComputer) {
        this.numOfComputer = numOfComputer;
    }

    public String getToIsdn() {
        return toIsdn;
    }

    public void setToIsdn(String toIsdn) {
        this.toIsdn = toIsdn;
    }

    public String getFromIsdn() {
        return fromIsdn;
    }

    public void setFromIsdn(String fromIsdn) {
        this.fromIsdn = fromIsdn;
    }

    public String[] getItemServicesId() {
        return itemServicesId;
    }

    public void setItemServicesId(String[] itemServicesId) {
        this.itemServicesId = itemServicesId;
    }

    public Long getPricePlanId() {
        return pricePlanId;
    }

    public void setPricePlanId(Long pricePlanId) {
        this.pricePlanId = pricePlanId;
    }

//    public List<MpServiceFee> getMpServiceFeeList() {
//        return mpServiceFeeList;
//    }
//
//    public void setMpServiceFeeList(List<MpServiceFee> mpServiceFeeList) {
//        this.mpServiceFeeList = mpServiceFeeList;
//    }
    public List<APStockModelBean> getApStockModelBeans() {
        return apStockModelBeans;
    }

    public void setApStockModelBeans(List<APStockModelBean> apStockModelBeans) {
        this.apStockModelBeans = apStockModelBeans;
    }

//    public AllTelServiceSub getAllTelServiceSub() {
//        return allTelServiceSub;
//    }
//
//    public List<ActionDetail> getActionDetails() {
//        return actionDetails;
//    }
//
//    public void setActionDetails(List<ActionDetail> actionDetails) {
//        this.actionDetails = actionDetails;
//    }
//    public void setAllTelServiceSub(AllTelServiceSub allTelServiceSub) {
//        this.allTelServiceSub = allTelServiceSub;
//    }
//
//    public List<SubRelation> getSubRelations() {
//        return subRelations;
//    }
//
//    public void setSubRelations(List<SubRelation> subRelations) {
//        this.subRelations = subRelations;
//    }
//
//    public void addSubRelation(SubRelation subRelation) {
//        if (subRelation != null) {
//            if (subRelations == null) {
//                subRelations = new ArrayList<SubRelation>();
//            }
//            subRelations.add(subRelation);
//        }
//    }
    public Long getActionAuditId() {
        return actionAuditId;
    }

    public void setActionAuditId(Long actionAuditId) {
        this.actionAuditId = actionAuditId;
    }

    public boolean isAllowEditRequest() {
        return allowEditRequest;
    }

    public void setAllowEditRequest(boolean allowEditRequest) {
        this.allowEditRequest = allowEditRequest;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public Long getStatusSdn() {
        return statusSdn;
    }

    public void setStatusSdn(Long statusSdn) {
        this.statusSdn = statusSdn;
    }

    public Subscriber getSubscriberPPPOE() {
        return subscriberPPPOE;
    }

    public void setSubscriberPPPOE(Subscriber subscriberPPPOE) {
        this.subscriberPPPOE = subscriberPPPOE;
    }

    public SubRequest getSubRequestPPPOE() {
        return subRequestPPPOE;
    }

    public void setSubRequestPPPOE(SubRequest subRequestPPPOE) {
        this.subRequestPPPOE = subRequestPPPOE;
    }

    public boolean isNeedRetake() {
        return needRetake;
    }

    public void setNeedRetake(boolean needRetake) {
        this.needRetake = needRetake;
    }

    public String getRetakeAccount() {
        return retakeAccount;
    }

    public void setRetakeAccount(String retakeAccount) {
        this.retakeAccount = retakeAccount;
    }

    /**
     * copy properties tu thang doi tuong khac;
     *
     * @param subRequest
     */
    public SubRequest copyProperties(SubRequest subRequest) throws Exception {
        this.subReqId = subRequest.getSubReqId();
        this.subId = subRequest.getSubId();
        this.taskId = subRequest.getTaskId();
        this.staffId = subRequest.getStaffId();
        this.firstConnect = subRequest.getFirstConnect();
        this.deployAddress = subRequest.getDeployAddress();
        this.deployAreaCode = subRequest.getDeployAreaCode();
        this.linePhone = subRequest.getLinePhone();
        this.lineType = subRequest.getLineType();
        this.speed = subRequest.getSpeed();
        this.password = subRequest.getPassword();
        this.account = subRequest.getAccount();
        this.isdn = subRequest.getIsdn();
        this.productId = subRequest.getProductId();
        this.deposit = subRequest.getDeposit();
        this.status = subRequest.getStatus();
        this.userCreated = subRequest.getUserCreated();
        this.createDate = subRequest.getCreateDate();
        this.vip = subRequest.getVip();
        this.staDatetime = subRequest.getStaDatetime();
        this.endDatetime = subRequest.getEndDatetime();
        this.shopCode = subRequest.getShopCode();
        this.finishReasonId = subRequest.getFinishReasonId();
        this.regReasonId = subRequest.getRegReasonId();
        this.subType = subRequest.getSubType();
        this.currentStepId = subRequest.getCurrentStepId();
        this.quota = subRequest.getQuota();
        this.promotionCode = subRequest.getPromotionCode();
        this.regType = subRequest.getRegType();
        this.reasonDepositeId = subRequest.getReasonDepositeId();
        this.ipStatic = subRequest.getIpStatic();
        this.isSatisfy = subRequest.getIsSatisfy();
        this.userUsing = subRequest.getUserUsing();
        this.userTitle = subRequest.getUserTitle();
        this.sourceCode = subRequest.getSourceCode();
        this.serviceType = subRequest.getServiceType();
        this.contractId = subRequest.getContractId();
        this.productCode = subRequest.getProductCode();
        this.mainSubId = subRequest.getMainSubId();
        this.offerId = subRequest.getOfferId();
        this.custId = subRequest.getCustId();
        this.progress = subRequest.getProgress();
        this.technology = subRequest.getTechnology();
        this.telFax = subRequest.getTelFax();
        this.telMobile = subRequest.getTelMobile();
        this.email = subRequest.getEmail();
        this.subGroup = subRequest.getSubGroup();
        this.limitDate = subRequest.getLimitDate();
        this.parentSubId = subRequest.getParentSubId();
        this.parentAccount = subRequest.getParentAccount();
        this.groupReqId = subRequest.getGroupReqId();
        this.groupAccountId = subRequest.getGroupAccountId();
        this.objectFileId = subRequest.getObjectFileId();
        this.upLink = subRequest.getUpLink();
        this.ipLan = subRequest.getIpLan();
        this.ipWan = subRequest.getIpWan();
        this.ipGateway = subRequest.getIpGateway();
        this.ipRouter = subRequest.getIpRouter();
        this.imRequest = subRequest.getImRequest();
        this.numOfComputer = subRequest.getNumOfComputer();
        this.vlan = subRequest.getVlan();
        this.pricePlanId = subRequest.getPricePlanId();
        this.fromIsdn = subRequest.getFromIsdn();
        this.toIsdn = subRequest.getToIsdn();
        this.statusSdn = subRequest.getStatusSdn();

        return this;
    }

    public List<SubRequest> getListSubRequets() {
        List<SubRequest> subRequests = new ArrayList<SubRequest>();
        SubRequest subRequestTemp = this;
        while (subRequestTemp != null) {
            subRequests.add(subRequestTemp);
            subRequestTemp = subRequestTemp.getSubRequest();
        }

        return subRequests;
    }
}
