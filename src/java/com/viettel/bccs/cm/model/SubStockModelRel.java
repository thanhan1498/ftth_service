package com.viettel.bccs.cm.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "SUB_STOCK_MODEL_REL")
public class SubStockModelRel implements java.io.Serializable {

    @Id
    @Column(name = "SUB_STOCK_MODEL_REL_ID", length = 10, precision = 10, scale = 0)
    private Long subStockModelRelId;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "SALE_SERVICE_ID", length = 10, precision = 10, scale = 0)
    private Long saleServiceId;
    @Transient
    private String saleServiceCode;
    @Column(name = "STOCK_MODEL_ID", length = 10, precision = 10, scale = 0)
    private Long stockModelId;
    @Column(name = "SERIAL", length = 30)
    private String serial;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "SALE_TRANS_ID", length = 10, precision = 10, scale = 0)
    private Long saleTransId;
    @Transient
    private String stockModelName;
    @Transient
    private String showCommitment;
    @Column(name = "PARTNER_ID", length = 10, precision = 10, scale = 0)
    private Long partnerId;
    @Column(name = "PARTNER_NAME", length = 100)
    private String partnerName;
    @Column(name = "STOCK_TYPE_ID", length = 10, precision = 10, scale = 0)
    private Long stockTypeId;
    @Column(name = "STOCK_TYPE_NAME", length = 250)
    private String stockTypeName;
    @Transient
    private Boolean needSerial;
    @Column(name = "QUANTITY", length = 5, precision = 5, scale = 0)
    private Long quantity;
    @Column(name = "SOURCE_ID", length = 10, precision = 10, scale = 0)
    private Long sourceId;
    @Column(name = "NEW_SERIAL", length = 30)
    private String newSerial;
    @Column(name = "TASK_MNG_ID", length = 10, precision = 10, scale = 0)
    private Long taskMngId;

    public SubStockModelRel() {
    }

    public SubStockModelRel(Long subStockModelRelId) {
        this.subStockModelRelId = subStockModelRelId;
    }

    public SubStockModelRel(Long subStockModelRelId, Long subId,
            Long saleServiceId, Long stockModelId, String serial,
            Date createdDate, Long status, Long saleTransId) {
        this.subStockModelRelId = subStockModelRelId;
        this.subId = subId;
        this.saleServiceId = saleServiceId;
        this.stockModelId = stockModelId;
        this.serial = serial;
        this.createdDate = createdDate;
        this.status = status;
        this.saleTransId = saleTransId;
    }

    public SubStockModelRel(Long subStockModelRelId, Long subId, Long saleServiceId, String saleServiceCode, Long stockModelId, String serial, Date createdDate, Long status, Long saleTransId, String stockModelName, String showCommitment, Long partnerId, String partnerName, Long stockTypeId, String stockTypeName, Boolean needSerial, Long quantity, Long sourceId, String newSerial, Long taskMngId) {
        this.subStockModelRelId = subStockModelRelId;
        this.subId = subId;
        this.saleServiceId = saleServiceId;
        this.saleServiceCode = saleServiceCode;
        this.stockModelId = stockModelId;
        this.serial = serial;
        this.createdDate = createdDate;
        this.status = status;
        this.saleTransId = saleTransId;
        this.stockModelName = stockModelName;
        this.showCommitment = showCommitment;
        this.partnerId = partnerId;
        this.partnerName = partnerName;
        this.stockTypeId = stockTypeId;
        this.stockTypeName = stockTypeName;
        this.needSerial = needSerial;
        this.quantity = quantity;
        this.sourceId = sourceId;
        this.newSerial = newSerial;
        this.taskMngId = taskMngId;
    }

    public SubStockModelRel(SubStockModelRelReq obj) {
        this.subStockModelRelId = obj.getSubStockModelRelId();
        this.subId = obj.getSubId();
        this.saleServiceId = obj.getSaleServiceId();
        this.stockModelId = obj.getStockModelId();
        this.serial = obj.getSerial();
        this.createdDate = obj.getCreatedDate();
        this.status = obj.getStatus();
        this.saleTransId = obj.getSaleTransId();
        this.stockModelName = obj.getStockModelName();
        this.showCommitment = obj.getShowCommitment();
        this.partnerId = obj.getPartnerId();
        this.partnerName = obj.getPartnerName();
        this.stockTypeId = obj.getStockTypeId();
        this.stockTypeName = obj.getStockTypeName();
        this.needSerial = obj.getNeedSerial();
        this.sourceId = obj.getSourceId();
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Boolean getNeedSerial() {
        return needSerial;
    }

    public void setNeedSerial(Boolean needSerial) {
        this.needSerial = needSerial;
    }

    public String getShowCommitment() {
        return showCommitment;
    }

    public void setShowCommitment(String showCommitment) {
        this.showCommitment = showCommitment;
    }

    // Property accessors
    public Long getSubStockModelRelId() {
        return this.subStockModelRelId;
    }

    public void setSubStockModelRelId(Long subStockModelRelId) {
        this.subStockModelRelId = subStockModelRelId;
    }

    public Long getSubId() {
        return this.subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Long getSaleServiceId() {
        return this.saleServiceId;
    }

    public void setSaleServiceId(Long saleServiceId) {
        this.saleServiceId = saleServiceId;
    }

    public Long getStockModelId() {
        return this.stockModelId;
    }

    public void setStockModelId(Long stockModelId) {
        this.stockModelId = stockModelId;
    }

    public String getSerial() {
        return this.serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getStatus() {
        return this.status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getSaleTransId() {
        return this.saleTransId;
    }

    public void setSaleTransId(Long saleTransId) {
        this.saleTransId = saleTransId;
    }

    public String getStockModelName() {
        return stockModelName;
    }

    public void setStockModelName(String stockModelName) {
        this.stockModelName = stockModelName;
    }

    public String getSaleServiceCode() {
        return saleServiceCode;
    }

    public void setSaleServiceCode(String saleServiceCode) {
        this.saleServiceCode = saleServiceCode;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public Long getStockTypeId() {
        return stockTypeId;
    }

    public void setStockTypeId(Long stockTypeId) {
        this.stockTypeId = stockTypeId;
    }

    public String getStockTypeName() {
        return stockTypeName;
    }

    public void setStockTypeName(String stockTypeName) {
        this.stockTypeName = stockTypeName;
    }

    public String getNewSerial() {
        return newSerial;
    }

    public void setNewSerial(String newSerial) {
        this.newSerial = newSerial;
    }

    public Long getTaskMngId() {
        return taskMngId;
    }

    public void setTaskMngId(Long taskMngId) {
        this.taskMngId = taskMngId;
    }
}
