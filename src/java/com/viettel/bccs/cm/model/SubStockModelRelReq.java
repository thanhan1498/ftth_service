package com.viettel.bccs.cm.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "SUB_STOCK_MODEL_REL_REQ")
public class SubStockModelRelReq implements java.io.Serializable {

    @Id
    @Column(name = "SUB_STOCK_MODEL_REL_ID", length = 10, precision = 10, scale = 0)
    private Long subStockModelRelId;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "SALE_SERVICE_ID", length = 10, precision = 10, scale = 0)
    private Long saleServiceId;
    @Column(name = "STOCK_MODEL_ID", length = 10, precision = 10, scale = 0)
    private Long stockModelId;
    @Column(name = "SERIAL", length = 30)
    private String serial;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "SALE_TRANS_ID", length = 10, precision = 10, scale = 0)
    private Long saleTransId;
    @Column(name = "PARTNER_ID", length = 10, precision = 10, scale = 0)
    private Long partnerId;
    @Column(name = "PARTNER_NAME", length = 100)
    private String partnerName;
    @Column(name = "STOCK_TYPE_ID", length = 10, precision = 10, scale = 0)
    private Long stockTypeId;
    @Column(name = "SOURCE_ID", length = 10, precision = 10, scale = 0)
    private Long sourceId;
    @Column(name = "STOCK_TYPE_NAME", length = 250)
    private String stockTypeName;
    @Transient
    private String stockModelName;
    @Transient
    private String showCommitment;
    @Transient
    private Boolean needSerial;

    public SubStockModelRelReq() {
    }

    public SubStockModelRelReq(Long subStockModelRelId) {
        this.subStockModelRelId = subStockModelRelId;
    }

    public SubStockModelRelReq(Long subStockModelRelId, Long subId,
            Long saleServiceId, Long stockModelId, String serial,
            Date createdDate, Long status, Long saleTransId) {
        this.subStockModelRelId = subStockModelRelId;
        this.subId = subId;
        this.saleServiceId = saleServiceId;
        this.stockModelId = stockModelId;
        this.serial = serial;
        this.createdDate = createdDate;
        this.status = status;
        this.saleTransId = saleTransId;
    }

    public Long getSubStockModelRelId() {
        return this.subStockModelRelId;
    }

    public void setSubStockModelRelId(Long subStockModelRelId) {
        this.subStockModelRelId = subStockModelRelId;
    }

    public Long getSubId() {
        return this.subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Long getSaleServiceId() {
        return this.saleServiceId;
    }

    public void setSaleServiceId(Long saleServiceId) {
        this.saleServiceId = saleServiceId;
    }

    public Long getStockModelId() {
        return this.stockModelId;
    }

    public void setStockModelId(Long stockModelId) {
        this.stockModelId = stockModelId;
    }

    public String getSerial() {
        return this.serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getStatus() {
        return this.status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getSaleTransId() {
        return this.saleTransId;
    }

    public void setSaleTransId(Long saleTransId) {
        this.saleTransId = saleTransId;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public Long getStockTypeId() {
        return stockTypeId;
    }

    public void setStockTypeId(Long stockTypeId) {
        this.stockTypeId = stockTypeId;
    }

    public String getStockTypeName() {
        return stockTypeName;
    }

    public void setStockTypeName(String stockTypeName) {
        this.stockTypeName = stockTypeName;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public Boolean getNeedSerial() {
        return needSerial;
    }

    public void setNeedSerial(Boolean needSerial) {
        this.needSerial = needSerial;
    }

    public String getShowCommitment() {
        return showCommitment;
    }

    public void setShowCommitment(String showCommitment) {
        this.showCommitment = showCommitment;
    }

    public String getStockModelName() {
        return stockModelName;
    }

    public void setStockModelName(String stockModelName) {
        this.stockModelName = stockModelName;
    }
}
