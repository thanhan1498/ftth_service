package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SUB_TYPE")
public class SubType implements Serializable {

    @Id
    @Column(name = "SUB_TYPE", length = 3, nullable = false)
    private String subType;
    @Column(name = "NAME", length = 50, nullable = false)
    private String name;
    @Column(name = "DIS_AMOUNT", length = 12, precision = 12, scale = 0)
    private Double disAmount;
    @Column(name = "MON_FEE1", length = 12, precision = 12, scale = 0)
    private Double monFee1;
    @Column(name = "MON_FEE2", length = 12, precision = 12, scale = 0)
    private Double monFee2;
    @Column(name = "MON_FEE3", length = 12, precision = 12, scale = 0)
    private Double monFee3;
    @Column(name = "DAY_FEE1", length = 12, precision = 12, scale = 0)
    private Double dayFee1;
    @Column(name = "DAY_FEE2", length = 12, precision = 12, scale = 0)
    private Double dayFee2;
    @Column(name = "DAY_FEE3", length = 12, precision = 12, scale = 0)
    private Double dayFee3;
    @Column(name = "CHARGE_REPORT", length = 1)
    private String chargeReport;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "AIR_FREE", length = 1)
    private String airFree;
    @Column(name = "INT_FREE", length = 1)
    private String intFree;
    @Column(name = "RM_FREE", length = 1)
    private String rmFree;
    @Column(name = "TEL_SERVICE", length = 10)
    private String telService;
    @Column(name = "CHECK_DEPOSIT", length = 1)
    private String checkDeposit;

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getDisAmount() {
        return disAmount;
    }

    public void setDisAmount(Double disAmount) {
        this.disAmount = disAmount;
    }

    public Double getMonFee1() {
        return monFee1;
    }

    public void setMonFee1(Double monFee1) {
        this.monFee1 = monFee1;
    }

    public Double getMonFee2() {
        return monFee2;
    }

    public void setMonFee2(Double monFee2) {
        this.monFee2 = monFee2;
    }

    public Double getMonFee3() {
        return monFee3;
    }

    public void setMonFee3(Double monFee3) {
        this.monFee3 = monFee3;
    }

    public Double getDayFee1() {
        return dayFee1;
    }

    public void setDayFee1(Double dayFee1) {
        this.dayFee1 = dayFee1;
    }

    public Double getDayFee2() {
        return dayFee2;
    }

    public void setDayFee2(Double dayFee2) {
        this.dayFee2 = dayFee2;
    }

    public Double getDayFee3() {
        return dayFee3;
    }

    public void setDayFee3(Double dayFee3) {
        this.dayFee3 = dayFee3;
    }

    public String getChargeReport() {
        return chargeReport;
    }

    public void setChargeReport(String chargeReport) {
        this.chargeReport = chargeReport;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getAirFree() {
        return airFree;
    }

    public void setAirFree(String airFree) {
        this.airFree = airFree;
    }

    public String getIntFree() {
        return intFree;
    }

    public void setIntFree(String intFree) {
        this.intFree = intFree;
    }

    public String getRmFree() {
        return rmFree;
    }

    public void setRmFree(String rmFree) {
        this.rmFree = rmFree;
    }

    public String getTelService() {
        return telService;
    }

    public void setTelService(String telService) {
        this.telService = telService;
    }

    public String getCheckDeposit() {
        return checkDeposit;
    }

    public void setCheckDeposit(String checkDeposit) {
        this.checkDeposit = checkDeposit;
    }
}
