package com.viettel.bccs.cm.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author BinhBM
 */
public class Subscriber {

    protected Long subId;
    protected Long custReqId;
    protected Long isSatisfy;
    protected Long contractId;
    protected String productCode;
    protected String actStatus;
    protected Date staDatetime;
    protected Date endDatetime;
    protected Long status;
    protected Long finishReasonId;
    protected Long regReasonId;
    protected Date createDate;
    protected String userCreated;
    protected String shopCode;
    protected String shopName;
    protected String subType;
    protected Long quota;
    protected String promotionCode;
    protected String regType;
    protected Long reasonDepositId;
    protected Double deposit;
    protected String ipStatic;
    protected Double quotaOver;
    protected Long isNewSub;
    protected String userUsing;
    protected String userTitle;
    Map resourceMap = new HashMap();
    protected Map attributeMap = new HashMap();
    protected Map vasMap = new HashMap();
    protected Map vasCheckBoxMap = new HashMap();
    protected String message;
    protected Long staffId;
    protected String staffName;
    protected String staffCode;
    protected String password;
    protected Date changeDatetime;

    /*Dùng cho đấu nối sửa sai*/
    protected String offerName;
    protected Long custId;
    protected String custName;
    protected String limitDate;
    protected String province;
    protected String district;
    protected String precinct;
    private String streetBlock;
    protected Long teamId;
    private String vip;
    private String debit;
    private String statusOpenBarring;
    private String deployAreaCode;
    protected String imsi;
    protected String serviceType;
    protected String account;
    //Them thuoc tin serial va isInfoCompleted
    protected String serial;
    protected Long isInfoCompleted;
    //Them thong tin address
    private String deployAddress;//Dia chi lap dat
    private String address; // Dia chi khach hang
    private String telMobile;//So dien thoai

    public String getDeployAddress() {
        return deployAddress;
    }

    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelMobile() {
        return telMobile;
    }

    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
    // tunk truong ngay sinh thue bao
    private Date subBirth;

    public Date getSubBirth() {
        return subBirth;
    }

    public void setSubBirth(Date subBirth) {
        this.subBirth = subBirth;
    }

    public String getStreetBlock() {
        return streetBlock;
    }

    public void setStreetBlock(String streetBlock) {
        this.streetBlock = streetBlock;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getDeployAreaCode() {
        return deployAreaCode;
    }

    public void setDeployAreaCode(String deployAreaCode) {
        this.deployAreaCode = deployAreaCode;
    }

    public String getStatusOpenBarring() {
        return statusOpenBarring;
    }

    public void setStatusOpenBarring(String statusOpenBarring) {
        this.statusOpenBarring = statusOpenBarring;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }
    /*TuanPV - 17/09/2009 - Dùng cho hiển thị sau khi đấu nối tại bước tiếp nhận yêu cầu*/
    //protected List<Product> lstVasActiveSuccess = new ArrayList<Product>();
    //protected List<Product> lstVasActiverFail = new ArrayList<Product>();
    //protected List<Product> lstVasNotActive = new ArrayList<Product>();
//    protected List<SubRelProduct> lstVasActiveSuccess = new ArrayList<SubRelProduct>();
//    protected List<SubRelProduct> lstVasActiverFail = new ArrayList<SubRelProduct>();
//    protected List<SubRelProduct> lstVasNotActive = new ArrayList<SubRelProduct>();
    /*TuanPV - 30/09/2009 - Biến lưu lại Id của action_log_pr ứng với lệnh đấu nối thuê bao*/
    protected Long actionLogPrId;
    protected List<SubStockModelRel> lstStockModel = new ArrayList<SubStockModelRel>();
    // add by NgocLD - 14/01/2010
    protected Long numResetZone;
    protected String isdn;
    private Long dslamId;
    protected Long[] arrStockTypeId;

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Long getDslamId() {
        return dslamId;
    }

    public void setDslamId(Long dslamId) {
        this.dslamId = dslamId;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Map getVasCheckBoxMap() {
        return vasCheckBoxMap;
    }

    public void setVasCheckBoxMap(Map vasCheckBoxMap) {
        this.vasCheckBoxMap = vasCheckBoxMap;
    }

    public Double getQuotaOver() {
        return quotaOver;
    }

    public void setQuotaOver(Double quotaOver) {
        this.quotaOver = quotaOver;
    }

    public Map getVasMap() {
        return vasMap;
    }

    public void setVasMap(Map vasMap) {
        this.vasMap = vasMap;
    }

    public Map getAttributeMap() {
        return attributeMap;
    }

    public void setAttributeMap(Map attributeMap) {
        this.attributeMap = attributeMap;
    }

    public String getIpStatic() {
        return ipStatic;
    }

    public void setIpStatic(String ipStatic) {
        this.ipStatic = ipStatic;
    }

    public Long getIsSatisfy() {
        return isSatisfy;
    }

    public void setIsSatisfy(Long isSatisfy) {
        this.isSatisfy = isSatisfy;
    }

    public Map getResourceMap() {
        return resourceMap;
    }

    public void setResourceMap(Map resourceMap) {
        this.resourceMap = resourceMap;
    }

    public Double getDeposit() {
        return deposit;
    }

    public void setDeposit(Double deposit) {
        this.deposit = deposit;
    }

    public Long getReasonDepositId() {
        return reasonDepositId;
    }

    public void setReasonDepositId(Long reasonDepositId) {
        this.reasonDepositId = reasonDepositId;
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getActStatus() {
        return actStatus;
    }

    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCustReqId() {
        return custReqId;
    }

    public void setCustReqId(Long custReqId) {
        this.custReqId = custReqId;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public Long getFinishReasonId() {
        return finishReasonId;
    }

    public void setFinishReasonId(Long finishReasonId) {
        this.finishReasonId = finishReasonId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Long getRegReasonId() {
        return regReasonId;
    }

    public void setRegReasonId(Long regReasonId) {
        this.regReasonId = regReasonId;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public Date getStaDatetime() {
        return staDatetime;
    }

    public void setStaDatetime(Date staDatetime) {
        this.staDatetime = staDatetime;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    public Long getQuota() {
        return quota;
    }

    public void setQuota(Long quota) {
        this.quota = quota;
    }

    public String getRegType() {
        return regType;
    }

    public void setRegType(String regType) {
        this.regType = regType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getIsNewSub() {
        return isNewSub;
    }

    public void setIsNewSub(Long isNewSub) {
        this.isNewSub = isNewSub;
    }

    public String getUserUsing() {
        return userUsing;
    }

    public void setUserUsing(String userUsing) {
        this.userUsing = userUsing;
    }

    public String getUserTitle() {
        return userTitle;
    }

    public void setUserTitle(String userTitle) {
        this.userTitle = userTitle;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

//    public List<SubRelProduct> getLstVasActiveSuccess()
//    {
//        return lstVasActiveSuccess;
//    }
//
//    public void setLstVasActiveSuccess(List<SubRelProduct> lstVasActiveSuccess)
//    {
//        this.lstVasActiveSuccess = lstVasActiveSuccess;
//    }
//
//    public List<SubRelProduct> getLstVasActiverFail()
//    {
//        return lstVasActiverFail;
//    }
//
//    public void setLstVasActiverFail(List<SubRelProduct> lstVasActiverFail)
//    {
//        this.lstVasActiverFail = lstVasActiverFail;
//    }
//
//    public List<SubRelProduct> getLstVasNotActive()
//    {
//        return lstVasNotActive;
//    }
//
//    public void setLstVasNotActive(List<SubRelProduct> lstVasNotActive)
//    {
//        this.lstVasNotActive = lstVasNotActive;
//    }
    public List<SubStockModelRel> getLstStockModel() {
        return lstStockModel;
    }

    public void setLstStockModel(List<SubStockModelRel> lstStockModel) {
        this.lstStockModel = lstStockModel;
    }

//    public SubReqCommitment getSubReqCommitment() {
//        return subReqCommitment;
//    }
//
//    public void setSubReqCommitment(SubReqCommitment subReqCommitment) {
//        this.subReqCommitment = subReqCommitment;
//    }
    public Long getActionLogPrId() {
        return actionLogPrId;
    }

    public void setActionLogPrId(Long actionLogPrId) {
        this.actionLogPrId = actionLogPrId;
    }

    public Date getChangeDatetime() {
        return changeDatetime;
    }

    public void setChangeDatetime(Date changeDatetime) {
        this.changeDatetime = changeDatetime;
    }

    public Long getNumResetZone() {
        return numResetZone;
    }

    public void setNumResetZone(Long numResetZone) {
        this.numResetZone = numResetZone;
    }

    public Long[] getArrStockTypeId() {
        return arrStockTypeId;
    }

    public void setArrStockTypeId(Long[] arrStockTypeId) {
        this.arrStockTypeId = arrStockTypeId;
    }

    public String getLimitDate() {
        return limitDate;
    }

    public void setLimitDate(String limitDate) {
        this.limitDate = limitDate;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public Long getIsInfoCompleted() {
        return isInfoCompleted;
    }

    public void setIsInfoCompleted(Long isInfoCompleted) {
        this.isInfoCompleted = isInfoCompleted;
    }
    private List lstReason = new ArrayList();

    public List getLstReason() {
        return lstReason;
    }

    public void setLstReason(List lstReason) {
        this.lstReason = lstReason;
    }
}
