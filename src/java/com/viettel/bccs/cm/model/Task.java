package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TASK")
public class Task implements Serializable {

    @Id
    @Column(name = "TASK_ID", length = 10, precision = 10, scale = 0)
    private Long taskId;
    @Column(name = "STEP_ID", length = 10, precision = 10, scale = 0)
    private Long stepId;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "TASK_TYPE_ID", length = 10, precision = 10, scale = 0)
    private Long taskTypeId;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "AREA_CODE", length = 15)
    private String areaCode;
    @Column(name = "REQ_ID", length = 10, precision = 10, scale = 0)
    private Long reqId;
    @Column(name = "TEAM_ID", length = 10, precision = 10, scale = 0)
    private Long teamId;
    @Column(name = "STA_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date staDate;
    @Column(name = "FINISH_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishDate;
    @Column(name = "OLD_GROUP_ID", length = 10, precision = 10, scale = 0)
    private Long oldGroupId;
    @Column(name = "STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long staffId;
    @Column(name = "TASK_NAME", length = 100)
    private String taskName;
    @Column(name = "PROGRESS", length = 1)
    private Long progress;
    @Column(name = "STATUS", length = 1)
    private Long status;
    @Column(name = "REQ_USER_ID", length = 10, precision = 10, scale = 0)
    private Long reqUserId;
    @Column(name = "CONTRACT_ID", length = 50)
    private String contractId;
    @Column(name = "CUST_NAME")
    private String custName;
    @Column(name = "TEL_FAX", length = 100)
    private String telFax;
    @Column(name = "DEPLOY_ADDRESS", length = 150)
    private String deployAddress;
    @Column(name = "DSLAM_ID", length = 10, precision = 10, scale = 0)
    private Long dslamId;
    @Column(name = "BOARD_ID", length = 10, precision = 10, scale = 0)
    private Long boardId;
    @Column(name = "CABLE_BOX_ID", length = 10, precision = 10, scale = 0)
    private Long cableBoxId;
    @Column(name = "ACCOUNT", length = 30)
    private String account;
    @Column(name = "VIP", length = 1)
    private String vip;
    @Column(name = "IS_NEED_ALARM", length = 1)
    private String isNeedAlarm;
    @Column(name = "DEADLINE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deadline;
    @Column(name = "HISTORY", length = 500)
    private String history;
    @Column(name = "DLU_ID", length = 10, precision = 10, scale = 0)
    private Long dluId;

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getStepId() {
        return stepId;
    }

    public void setStepId(Long stepId) {
        this.stepId = stepId;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Long getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(Long taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public Long getReqId() {
        return reqId;
    }

    public void setReqId(Long reqId) {
        this.reqId = reqId;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Date getStaDate() {
        return staDate;
    }

    public void setStaDate(Date staDate) {
        this.staDate = staDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Long getOldGroupId() {
        return oldGroupId;
    }

    public void setOldGroupId(Long oldGroupId) {
        this.oldGroupId = oldGroupId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Long getProgress() {
        return progress;
    }

    public void setProgress(Long progress) {
        this.progress = progress;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getReqUserId() {
        return reqUserId;
    }

    public void setReqUserId(Long reqUserId) {
        this.reqUserId = reqUserId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getTelFax() {
        return telFax;
    }

    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    public String getDeployAddress() {
        return deployAddress;
    }

    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    public Long getDslamId() {
        return dslamId;
    }

    public void setDslamId(Long dslamId) {
        this.dslamId = dslamId;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Long getCableBoxId() {
        return cableBoxId;
    }

    public void setCableBoxId(Long cableBoxId) {
        this.cableBoxId = cableBoxId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public String getIsNeedAlarm() {
        return isNeedAlarm;
    }

    public void setIsNeedAlarm(String isNeedAlarm) {
        this.isNeedAlarm = isNeedAlarm;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public Long getDluId() {
        return dluId;
    }

    public void setDluId(Long dluId) {
        this.dluId = dluId;
    }
}
