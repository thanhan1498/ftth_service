/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

/**
 *
 * @author User-PC
 */
public class TaskInfoDetail {

    Long taskMngtId;
    Long telServiceId;
    Long subId;
    Long shopId;
    String shopName;
    Long staffId;
    String endDateValue;
    String taskName;
    String dsLamName;
    String productName;
    Long custReqId;
    String contractNo;
    String progress;
    String sourceType;
    Long taskShopMngtId;
    String deployAddress;
    String startDateValue;
    String limitDate;
    String custName;
    String custTelFax;
    String reqType;
    String serviceType;
    String pricePlanName;
    String localPricePlanName;
    String pricePlanSpeed;
    String compContent;
    private String stationCode;
    private String connector;
    private String accBundleTv;

    public Long getTaskMngtId() {
        return taskMngtId;
    }

    public void setTaskMngtId(Long taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    public Long getTelServiceId() {
        return telServiceId;
    }

    public void setTelServiceId(Long telServiceId) {
        this.telServiceId = telServiceId;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getEndDateValue() {
        return endDateValue;
    }

    public void setEndDateValue(String endDateValue) {
        this.endDateValue = endDateValue;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDsLamName() {
        return dsLamName;
    }

    public void setDsLamName(String dsLamName) {
        this.dsLamName = dsLamName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getCustReqId() {
        return custReqId;
    }

    public void setCustReqId(Long custReqId) {
        this.custReqId = custReqId;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public Long getTaskShopMngtId() {
        return taskShopMngtId;
    }

    public void setTaskShopMngtId(Long taskShopMngtId) {
        this.taskShopMngtId = taskShopMngtId;
    }

    public String getDeployAddress() {
        return deployAddress;
    }

    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    public String getStartDateValue() {
        return startDateValue;
    }

    public void setStartDateValue(String startDateValue) {
        this.startDateValue = startDateValue;
    }

    public String getLimitDate() {
        return limitDate;
    }

    public void setLimitDate(String limitDate) {
        this.limitDate = limitDate;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustTelFax() {
        return custTelFax;
    }

    public void setCustTelFax(String custTelFax) {
        this.custTelFax = custTelFax;
    }

    public String getReqType() {
        return reqType;
    }

    public void setReqType(String reqType) {
        this.reqType = reqType;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getPricePlanName() {
        return pricePlanName;
    }

    public void setPricePlanName(String pricePlanName) {
        this.pricePlanName = pricePlanName;
    }

    public String getLocalPricePlanName() {
        return localPricePlanName;
    }

    public void setLocalPricePlanName(String localPricePlanName) {
        this.localPricePlanName = localPricePlanName;
    }

    public String getPricePlanSpeed() {
        return pricePlanSpeed;
    }

    public void setPricePlanSpeed(String pricePlanSpeed) {
        this.pricePlanSpeed = pricePlanSpeed;
    }

    public String getCompContent() {
        return compContent;
    }

    public void setCompContent(String compContent) {
        this.compContent = compContent;
    }

    /**
     * @return the stationCode
     */
    public String getStationCode() {
        return stationCode;
    }

    /**
     * @param stationCode the stationCode to set
     */
    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    /**
     * @return the connector
     */
    public String getConnector() {
        return connector;
    }

    /**
     * @param connector the connector to set
     */
    public void setConnector(String connector) {
        this.connector = connector;
    }

    /**
     * @return the accBundleTv
     */
    public String getAccBundleTv() {
        return accBundleTv;
    }

    /**
     * @param accBundleTv the accBundleTv to set
     */
    public void setAccBundleTv(String accBundleTv) {
        this.accBundleTv = accBundleTv;
    }
}
