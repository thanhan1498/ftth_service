package com.viettel.bccs.cm.model;

import com.viettel.bccs.cm.model.pk.TaskManagementId;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TASK_MANAGEMENT")
@IdClass(TaskManagementId.class)
public class TaskManagement implements Serializable {

    @Id
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Id
    private Long taskMngtId;
    @Column(name = "JOB_ID", length = 10, precision = 10, scale = 0)
    private Long jobId;
    @Column(name = "CUST_REQ_ID", length = 10, precision = 10, scale = 0)
    private Long custReqId;
    @Column(name = "USER_ID", length = 10, precision = 10, scale = 0)
    private Long userId;
    @Column(name = "TASK_NAME", length = 100)
    private String taskName;
    @Column(name = "PROGRESS", length = 2)
    private String progress;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "REQ_USER_ID", length = 10, precision = 10, scale = 0)
    private Long reqUserId;
    @Column(name = "SOURCE_TYPE", length = 1)
    private String sourceType;
    @Column(name = "JOB_CODE", length = 10)
    private String jobCode;
    @Column(name = "CONTRACT_NO", length = 50)
    private String contractNo;
    @Column(name = "PRODUCT_CODE", length = 10)
    private String productCode;
    @Column(name = "CONTRACT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date contractDate;
    @Column(name = "STAR_CONTRACT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date starContractDate;
    @Column(name = "NAME", length = 150)
    private String name;
    @Column(name = "TEL_FAX", length = 100)
    private String telFax;
    @Column(name = "AREA_CODE", length = 25)
    private String areaCode;
    @Column(name = "DEPLOY_ADDRESS", length = 150)
    private String deployAddress;
    @Column(name = "REQ_TYPE", length = 1)
    private String reqType;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "LIMIT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date limitDate;
    @Column(name = "DSLAM_ID", length = 10, precision = 10, scale = 0)
    private Long dslamId;
    @Column(name = "ACCOUNT", length = 30)
    private String account;
    @Column(name = "VIP", length = 1)
    private String vip;
    @Column(name = "SERVICE_TYPE", length = 10)
    private String serviceType;
    @Column(name = "DEPLOY_TRANS_ID", length = 10, precision = 10, scale = 0)
    private Long deployTransId;
    @Column(name = "SOURCE_ID", length = 10, precision = 10, scale = 0)
    private Long sourceId;
    @Column(name = "IS_SYSTEM_ERROR")
    private Long isSystemError;
    @Column(name = "LIMIT_CUST_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date limitCustDate;
    @Column(name = "MOBILE", length = 10)
    private String mobile;
    @Column(name = "FILE_NAME", length = 50)
    private String fileName;
    @Column(name = "PATH_FILE_ATTACH", length = 50)
    private String pathFileAttach;
    //duyetdk
    @Column(name = "ORIGINAL_CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date originalCreateDate;
    @Column(name = "DELAY_REASON", length = 500)
    private String delayReason;

    public Date getOriginalCreateDate() {
        return originalCreateDate;
    }

    public void setOriginalCreateDate(Date originalCreateDate) {
        this.originalCreateDate = originalCreateDate;
    }

    public String getDelayReason() {
        return delayReason;
    }

    public void setDelayReason(String delayReason) {
        this.delayReason = delayReason;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getTaskMngtId() {
        return taskMngtId;
    }

    public void setTaskMngtId(Long taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public Long getCustReqId() {
        return custReqId;
    }

    public void setCustReqId(Long custReqId) {
        this.custReqId = custReqId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getReqUserId() {
        return reqUserId;
    }

    public void setReqUserId(Long reqUserId) {
        this.reqUserId = reqUserId;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Date getContractDate() {
        return contractDate;
    }

    public void setContractDate(Date contractDate) {
        this.contractDate = contractDate;
    }

    public Date getStarContractDate() {
        return starContractDate;
    }

    public void setStarContractDate(Date starContractDate) {
        this.starContractDate = starContractDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelFax() {
        return telFax;
    }

    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getDeployAddress() {
        return deployAddress;
    }

    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    public String getReqType() {
        return reqType;
    }

    public void setReqType(String reqType) {
        this.reqType = reqType;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Date getLimitDate() {
        return limitDate;
    }

    public void setLimitDate(Date limitDate) {
        this.limitDate = limitDate;
    }

    public Long getDslamId() {
        return dslamId;
    }

    public void setDslamId(Long dslamId) {
        this.dslamId = dslamId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Long getDeployTransId() {
        return deployTransId;
    }

    public void setDeployTransId(Long deployTransId) {
        this.deployTransId = deployTransId;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public Long getIsSystemError() {
        return isSystemError;
    }

    public void setIsSystemError(Long isSystemError) {
        this.isSystemError = isSystemError;
    }

    public Date getLimitCustDate() {
        return limitCustDate;
    }

    public void setLimitCustDate(Date limitCustDate) {
        this.limitCustDate = limitCustDate;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the pathFileAttach
     */
    public String getPathFileAttach() {
        return pathFileAttach;
    }

    /**
     * @param pathFileAttach the pathFileAttach to set
     */
    public void setPathFileAttach(String pathFileAttach) {
        this.pathFileAttach = pathFileAttach;
    }
}
