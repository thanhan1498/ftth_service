package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TASK_STAFF_MANAGEMENT")
public class TaskStaffManagement implements Serializable {

    @Id
    @Column(name = "TASK_STAFF_MNGT_ID", length = 10, precision = 10, scale = 0)
    private Long taskStaffMngtId;
    @Column(name = "TASK_SHOP_MNGT_ID", length = 10, precision = 10, scale = 0)
    private Long taskShopMngtId;
    @Column(name = "STAGE_ID", length = 10, precision = 10, scale = 0)
    private Long stageId;
    @Column(name = "PARENT_ID", length = 10, precision = 10, scale = 0)
    private Long parentId;
    @Column(name = "STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long staffId;
    @Column(name = "STA_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date staDate;
    @Column(name = "FINISH_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishDate;
    @Column(name = "ACTUAL_FINISH_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actualFinishDate;
    @Column(name = "DURATION", length = 5, precision = 5, scale = 0)
    private Long duration;
    @Column(name = "ACTUAL_DURATION", length = 5, precision = 5, scale = 0)
    private Long actualDuration;
    @Column(name = "USER_ID", length = 10, precision = 10, scale = 0)
    private Long userId;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "PROGRESS", length = 2)
    private String progress;
    @Column(name = "ITEM_STATUS", length = 1)
    private String itemStatus;
    @Column(name = "REASON_ID", length = 10, precision = 10, scale = 0)
    private Long reasonId;
    @Column(name = "DESCRIPTION", length = 300)
    private String description;
    @Column(name = "ACTUAL_STA_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actualStaDate;
    @Column(name = "NUM_SMS_SENT", length = 10, precision = 10, scale = 0)
    private Long numSmsSent;
    @Column(name = "POS_DEPEND_ID", length = 10, precision = 10, scale = 0)
    private Long posDependId;

    public TaskStaffManagement() {
    }

    public Long getTaskStaffMngtId() {
        return taskStaffMngtId;
    }

    public void setTaskStaffMngtId(Long taskStaffMngtId) {
        this.taskStaffMngtId = taskStaffMngtId;
    }

    public Long getTaskShopMngtId() {
        return taskShopMngtId;
    }

    public void setTaskShopMngtId(Long taskShopMngtId) {
        this.taskShopMngtId = taskShopMngtId;
    }

    public Long getStageId() {
        return stageId;
    }

    public void setStageId(Long stageId) {
        this.stageId = stageId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Date getStaDate() {
        return staDate;
    }

    public void setStaDate(Date staDate) {
        this.staDate = staDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Date getActualFinishDate() {
        return actualFinishDate;
    }

    public void setActualFinishDate(Date actualFinishDate) {
        this.actualFinishDate = actualFinishDate;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getActualDuration() {
        return actualDuration;
    }

    public void setActualDuration(Long actualDuration) {
        this.actualDuration = actualDuration;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getActualStaDate() {
        return actualStaDate;
    }

    public void setActualStaDate(Date actualStaDate) {
        this.actualStaDate = actualStaDate;
    }

    public Long getNumSmsSent() {
        return numSmsSent;
    }

    public void setNumSmsSent(Long numSmsSent) {
        this.numSmsSent = numSmsSent;
    }

    public Long getPosDependId() {
        return posDependId;
    }

    public void setPosDependId(Long posDependId) {
        this.posDependId = posDependId;
    }
}
