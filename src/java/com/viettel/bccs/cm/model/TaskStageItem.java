package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TASK_STAGE_ITEM")
public class TaskStageItem implements Serializable {

    @Id
    @Column(name = "STAK_STG_ITEM_ID", length = 10, precision = 10, scale = 0)
    private Long stakStgItemId;
    @Column(name = "TASK_MNGT_ID", length = 10, precision = 10, scale = 0)
    private Long taskMngtId;
    @Column(name = "TASK_STAFF_MNGT_ID", length = 10, precision = 10, scale = 0)
    private Long taskStaffMngtId;
    @Column(name = "JSG_ID", length = 10, precision = 10, scale = 0)
    private Long jsgId;
    @Column(name = "QUANTITY", length = 10, precision = 10, scale = 0)
    private Long quantity;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "EXPORT_STATUS", length = 1)
    private String exportStatus;

    public TaskStageItem() {
    }

    public Long getStakStgItemId() {
        return stakStgItemId;
    }

    public void setStakStgItemId(Long stakStgItemId) {
        this.stakStgItemId = stakStgItemId;
    }

    public Long getTaskMngtId() {
        return taskMngtId;
    }

    public void setTaskMngtId(Long taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    public Long getTaskStaffMngtId() {
        return taskStaffMngtId;
    }

    public void setTaskStaffMngtId(Long taskStaffMngtId) {
        this.taskStaffMngtId = taskStaffMngtId;
    }

    public Long getJsgId() {
        return jsgId;
    }

    public void setJsgId(Long jsgId) {
        this.jsgId = jsgId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getExportStatus() {
        return exportStatus;
    }

    public void setExportStatus(String exportStatus) {
        this.exportStatus = exportStatus;
    }
}
