/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "TECHNICAL_CONNECTOR")
public class TechnicalConnector implements Serializable {

    @Id
    @Column(name = "ID", length = 10, precision = 10, scale = 0)
    private Long id;
    @Column(name = "CONNECTOR_ID", length = 10, precision = 10, scale = 0)
    private Long connectorId;
    @Column(name = "STATUS", length = 10, precision = 10, scale = 0)
    private Long status;
    @Column(name = "CONNECTOR_CODE", length = 50)
    private String connectorCode;
    @Column(name = "INFRA_TYPE", length = 50)
    private String infraType;
    @Column(name = "STATION_ID", length = 10, precision = 10, scale = 0)
    private Long stationId;
    @Column(name = "STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long staffId;
    @Column(name = "STAFF_CODE", length = 50)
    private String staffCode;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CREATE_USER", length = 50)
    private String createUser;

    /**
     * @return the connectorId
     */
    public Long getConnectorId() {
        return connectorId;
    }

    /**
     * @param connectorId the connectorId to set
     */
    public void setConnectorId(Long connectorId) {
        this.connectorId = connectorId;
    }

    /**
     * @return the connectorCode
     */
    public String getConnectorCode() {
        return connectorCode;
    }

    /**
     * @param connectorCode the connectorCode to set
     */
    public void setConnectorCode(String connectorCode) {
        this.connectorCode = connectorCode;
    }

    /**
     * @return the infraType
     */
    public String getInfraType() {
        return infraType;
    }

    /**
     * @param infraType the infraType to set
     */
    public void setInfraType(String infraType) {
        this.infraType = infraType;
    }

    /**
     * @return the stationId
     */
    public Long getStationId() {
        return stationId;
    }

    /**
     * @param stationId the stationId to set
     */
    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    /**
     * @return the staffId
     */
    public Long getStaffId() {
        return staffId;
    }

    /**
     * @param staffId the staffId to set
     */
    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    /**
     * @return the staffCode
     */
    public String getStaffCode() {
        return staffCode;
    }

    /**
     * @param staffCode the staffCode to set
     */
    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }
}
