package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * @author linhlh2
 */
@Entity
@Table(name = "TOKEN")
public class Token implements Serializable {

    @Id
    @Column(name = "TOKEN_ID", length = 22)
    private Long tokenId;
    @Column(name = "USER_NAME", length = 50)
    private String userName;
    @Column(name = "SERIAL", length = 25)
    private String serial;
    @Column(name = "TOKEN", length = 50)
    private String token;
    @Column(name = "CREATE_TIME", length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;
    @Column(name = "LAST_REQUEST", length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastRequest;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "STAFF_ID", length = 20)
    private Long staffId;
    @Column(name = "PERMISSION", length = 1000)
    private String permission;
    @Column(name = "STAFF_NAME", length = 100)
    private String staffName;
    @Column(name = "DEVICE_ID", length = 100)
    private String deviceId;
    @Transient
    private String resultCode;
    @Transient
    private String resultMessage;

    public Token() {
    }

    public Token(String resultCode, String resultMessage) {
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
    }

    public Token(Long tokenId, String userName, String serial, String token, Date createTime, Date lastRequest, Long staffId, String staffName, Long status, String deviceId) {
        this.tokenId = tokenId;
        this.userName = userName;
        this.serial = serial;
        this.token = token;
        this.createTime = createTime;
        this.lastRequest = lastRequest;
        this.staffId = staffId;
        this.staffName = staffName;
        this.status = status;
        this.deviceId = deviceId == null || "?".equals(deviceId) || deviceId.isEmpty() ? null : deviceId;
    }

    public Long getTokenId() {
        return tokenId;
    }

    public void setTokenId(Long tokenId) {
        this.tokenId = tokenId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastRequest() {
        return lastRequest;
    }

    public void setLastRequest(Date lastRequest) {
        this.lastRequest = lastRequest;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    /**
     * @return the deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId the deviceId to set
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
