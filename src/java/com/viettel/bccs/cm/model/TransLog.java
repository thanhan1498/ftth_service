package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author linhlh2
 */
@Entity
@Table(name = "TRANS_LOG")
public class TransLog implements Serializable {

    @Id
    @Column(name = "TRANS_ID", length = 22)
    private Long transId;
    @Column(name = "TOKEN_ID", length = 22)
    private Long tokenId;
    @Column(name = "TOKEN", length = 50)
    private String token;
    @Column(name = "ISSUE_DATE", length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    private Date issueDate;
    @Column(name = "CLASS_NAME", length = 150)
    private String className;
    @Column(name = "METHOD", length = 100)
    private String method;
    @Column(name = "PARAMS", length = 4000)
    private String params;
    @Column(name = "EXCUTE", length = 10)
    private Long excute;
    @Column(name = "RESPONSE", length = 2000)
    private String response;

    public TransLog() {
    }

    public TransLog(Long transId, Long tokenId, String token, Date issueDate, String className, String method, String params, Long excute, String response) {
        this.transId = transId;
        this.tokenId = tokenId;
        this.token = token;
        this.issueDate = issueDate;
        this.className = className;
        this.method = method;
        this.params = params != null && params.length() > 4000 ? params.substring(0, 3999) : params;
        this.excute = excute;
        this.response = response != null && response.length() > 2000 ? response.substring(0, 1999) : response;
    }

    public Long getTransId() {
        return transId;
    }

    public void setTransId(Long transId) {
        this.transId = transId;
    }

    public Long getTokenId() {
        return tokenId;
    }

    public void setTokenId(Long tokenId) {
        this.tokenId = tokenId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public Long getExcute() {
        return excute;
    }

    public void setExcute(Long excute) {
        this.excute = excute;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
