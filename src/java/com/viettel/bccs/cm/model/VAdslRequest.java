package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "V_ADSL_REQUEST")
public class VAdslRequest implements Serializable {

    @Id
    @Column(name = "CUST_REQ_ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long custReqId;
    @Column(name = "ADDED_USER", length = 30)
    private String addUser;
    @Column(name = "REQ_DATE", length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    private Date reqDate;
    @Column(name = "CUS_REQ_STATUS", length = 1, precision = 1)
    private Long cusReqStatus;
    @Column(name = "CUST_ID", length = 10, precision = 10, scale = 0)
    private Long custId;
    @Column(name = "CUST_NAME", length = 150)
    private String custName;
    @Column(name = "BUS_TYPE", length = 10)
    private String busType;
    @Column(name = "BIRTH_DATE", length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthDate;
    @Column(name = "SEX", length = 1)
    private String sex;
    @Column(name = "ID_TYPE", length = 2)
    private Long idType;
    @Column(name = "ID_NO", length = 50)
    private String idNo;
    @Column(name = "ID_ISSUE_PLACE", length = 70)
    private String idIssuePlace;
    @Column(name = "ID_ISSUE_DATE", length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    private Date idIssueDate;
    @Column(name = "BUS_PERMIT_NO", length = 50)
    private String busPermitNo;
    @Column(name = "POP_NO", length = 20)
    private String popNo;
    @Column(name = "TIN", length = 30)
    private String tin;
    @Column(name = "AREA_CODE", length = 27)
    private String areaCode;
    @Column(name = "PROVINCE", length = 20)
    private String province;
    @Column(name = "DISTRICT", length = 10)
    private String district;
    @Column(name = "PRECINCT", length = 15)
    private String precinct;
    @Column(name = "ADDRESS", length = 500)
    private String address;
    @Column(name = "CUST_STATUS", length = 1, precision = 1)
    private Long custStatus;
    @Column(name = "SUB_REQ_ID", length = 10, precision = 10, scale = 0)
    private Long subReqId;
    @Column(name = "SERVICE_ALIAS", length = 10)
    private String serviceAlias;
    @Column(name = "ACCOUNT", length = 100)
    private String account;
    @Column(name = "IS_SMART", length = 1, precision = 1)
    private Long isSmart;
    @Column(name = "SUB_REQ_STATUS")
    private Long subReqStatus;
    @Column(name = "REQ_OFFER_ID", length = 10, precision = 10, scale = 0)
    private Long reqOfferId;
    @Column(name = "OFFER_ID", length = 10, precision = 10, scale = 0)
    private Long offerId;
    @Column(name = "REQ_OFFER_STATUS", length = 1)
    private Long reqOfferStatus;
    @Column(name = "ACC_BUNDLE_TV", length = 100)
    private String accBundleTv;

    public Long getCustReqId() {
        return custReqId;
    }

    public void setCustReqId(Long custReqId) {
        this.custReqId = custReqId;
    }

    public String getAddUser() {
        return addUser;
    }

    public void setAddUser(String addUser) {
        this.addUser = addUser;
    }

    public Date getReqDate() {
        return reqDate;
    }

    public void setReqDate(Date reqDate) {
        this.reqDate = reqDate;
    }

    public Long getCusReqStatus() {
        return cusReqStatus;
    }

    public void setCusReqStatus(Long cusReqStatus) {
        this.cusReqStatus = cusReqStatus;
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Long getIdType() {
        return idType;
    }

    public void setIdType(Long idType) {
        this.idType = idType;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getIdIssuePlace() {
        return idIssuePlace;
    }

    public void setIdIssuePlace(String idIssuePlace) {
        this.idIssuePlace = idIssuePlace;
    }

    public Date getIdIssueDate() {
        return idIssueDate;
    }

    public void setIdIssueDate(Date idIssueDate) {
        this.idIssueDate = idIssueDate;
    }

    public String getBusPermitNo() {
        return busPermitNo;
    }

    public void setBusPermitNo(String busPermitNo) {
        this.busPermitNo = busPermitNo;
    }

    public String getPopNo() {
        return popNo;
    }

    public void setPopNo(String popNo) {
        this.popNo = popNo;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getCustStatus() {
        return custStatus;
    }

    public void setCustStatus(Long custStatus) {
        this.custStatus = custStatus;
    }

    public Long getSubReqId() {
        return subReqId;
    }

    public void setSubReqId(Long subReqId) {
        this.subReqId = subReqId;
    }

    public String getServiceAlias() {
        return serviceAlias;
    }

    public void setServiceAlias(String serviceAlias) {
        this.serviceAlias = serviceAlias;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Long getIsSmart() {
        return isSmart;
    }

    public void setIsSmart(Long isSmart) {
        this.isSmart = isSmart;
    }

    public Long getSubReqStatus() {
        return subReqStatus;
    }

    public void setSubReqStatus(Long subReqStatus) {
        this.subReqStatus = subReqStatus;
    }

    public Long getReqOfferId() {
        return reqOfferId;
    }

    public void setReqOfferId(Long reqOfferId) {
        this.reqOfferId = reqOfferId;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public Long getReqOfferStatus() {
        return reqOfferStatus;
    }

    public void setReqOfferStatus(Long reqOfferStatus) {
        this.reqOfferStatus = reqOfferStatus;
    }

    /**
     * @return the accBundleTv
     */
    public String getAccBundleTv() {
        return accBundleTv;
    }

    /**
     * @param accBundleTv the accBundleTv to set
     */
    public void setAccBundleTv(String accBundleTv) {
        this.accBundleTv = accBundleTv;
    }
}
