package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author linhlh2
 */
@Entity
@Table(name = "VIEW_ALL_CUST_REQUEST")
public class ViewAllCustRequest implements Serializable {

    @Id
    @Column(name = "SUB_REQ_ID", length = 10, precision = 10, scale = 0)
    private Long subReqId;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "CUST_REQ_ID", length = 10, precision = 10, scale = 0)
    private Long custReqId;
    @Column(name = "OFFER_REQ_ID", length = 10, precision = 10, scale = 0)
    private Long offerReqId;
    @Column(name = "ISDN", length = 10)
    private String isdn;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "CUST_ID", length = 15, precision = 15, scale = 0)
    private Long custId;
    @Column(name = "IS_NEW_SUB", length = 1, precision = 1, scale = 0)
    private Long isNewSub;
    @Column(name = "ACCOUNT", length = 50)
    private String account;
    @Column(name = "TELESERVICE", length = 50)
    private String teleservice;
    @Column(name = "SERVICE_TYPE", length = 50)
    private String serviceType;
    @Column(name = "NUM_OF_SUBSCRIBERS", length = 38, precision = 38, scale = 0)
    private Long numOfSubscribers;
    @Column(name = "SHOP_CODE")
    private String shopCode;
    @Column(name = "ADDED_USER")
    private String addedUser;
    @Column(name = "REQ_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reqDate;

    public ViewAllCustRequest() {
    }

    public Long getSubReqId() {
        return subReqId;
    }

    public void setSubReqId(Long subReqId) {
        this.subReqId = subReqId;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Long getCustReqId() {
        return custReqId;
    }

    public void setCustReqId(Long custReqId) {
        this.custReqId = custReqId;
    }

    public Long getOfferReqId() {
        return offerReqId;
    }

    public void setOfferReqId(Long offerReqId) {
        this.offerReqId = offerReqId;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public Long getIsNewSub() {
        return isNewSub;
    }

    public void setIsNewSub(Long isNewSub) {
        this.isNewSub = isNewSub;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getTeleservice() {
        return teleservice;
    }

    public void setTeleservice(String teleservice) {
        this.teleservice = teleservice;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Long getNumOfSubscribers() {
        return numOfSubscribers;
    }

    public void setNumOfSubscribers(Long numOfSubscribers) {
        this.numOfSubscribers = numOfSubscribers;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getAddedUser() {
        return addedUser;
    }

    public void setAddedUser(String addedUser) {
        this.addedUser = addedUser;
    }

    public Date getReqDate() {
        return reqDate;
    }

    public void setReqDate(Date reqDate) {
        this.reqDate = reqDate;
    }
}
