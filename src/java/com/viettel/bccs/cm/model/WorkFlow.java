package com.viettel.bccs.cm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "WORK_FLOW")
public class WorkFlow implements Serializable {

    @Id
    @Column(name = "WORK_FLOW_ID", length = 10, precision = 10, scale = 0)
    private Long workFlowId;
    @Column(name = "TEL_SERVICE", length = 10, precision = 10, scale = 0)
    private Long telService;
    @Column(name = "NEXT_STEP_ID", length = 10, precision = 10, scale = 0)
    private Long nextStepId;
    @Column(name = "LAST_UPDATE_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdateTime;
    @Column(name = "LAST_UPDATE_USER", length = 30)
    private String lastUpdateUser;
    @Column(name = "CURRENT_STEP_ID", length = 22, precision = 22, scale = 0)
    private Long currentStepId;
    @Column(name = "NEXT_ACTION_ID", length = 22, precision = 22, scale = 0)
    private Long nextActionId;
    @Column(name = "STATUS", length = 2, precision = 2, scale = 0)
    private Long status;
    @Column(name = "IS_MANUAL_ONLY", length = 1)
    private String isManualOnly;

    public Long getWorkFlowId() {
        return workFlowId;
    }

    public void setWorkFlowId(Long workFlowId) {
        this.workFlowId = workFlowId;
    }

    public Long getTelService() {
        return telService;
    }

    public void setTelService(Long telService) {
        this.telService = telService;
    }

    public Long getNextStepId() {
        return nextStepId;
    }

    public void setNextStepId(Long nextStepId) {
        this.nextStepId = nextStepId;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public Long getCurrentStepId() {
        return currentStepId;
    }

    public void setCurrentStepId(Long currentStepId) {
        this.currentStepId = currentStepId;
    }

    public Long getNextActionId() {
        return nextActionId;
    }

    public void setNextActionId(Long nextActionId) {
        this.nextActionId = nextActionId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getIsManualOnly() {
        return isManualOnly;
    }

    public void setIsManualOnly(String isManualOnly) {
        this.isManualOnly = isManualOnly;
    }
}
