package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "WS_USER")
public class WsUser implements Serializable {

    @Id
    @Column(name = "WS_USER_ID")
    private Long wsUserId;
    @Column(name = "USERNAME")
    private String username;
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "IP_ADDRESS")
    private String ipAddress;
    @Column(name = "CHANNEL")
    private String channel;

    public Long getWsUserId() {
        return wsUserId;
    }

    public void setWsUserId(Long wsUserId) {
        this.wsUserId = wsUserId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
