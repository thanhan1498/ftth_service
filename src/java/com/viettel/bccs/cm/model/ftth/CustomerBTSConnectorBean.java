/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.ftth;

/**
 *
 * @author partner1
 */
public class CustomerBTSConnectorBean {
    Long BtsId;
    String BtsCode;
    Long connectorId;
    String connectorCode;
    String customerType;
    Long count;

    public CustomerBTSConnectorBean() {
    }
   
    public String getBtsCode() {
        return BtsCode;
    }

    public void setBtsCode(String BtsCode) {
        this.BtsCode = BtsCode;
    }

    public Long getBtsId() {
        return BtsId;
    }

    public void setBtsId(Long BtsId) {
        this.BtsId = BtsId;
    }

    public String getConnectorCode() {
        return connectorCode;
    }

    public void setConnectorCode(String connectorCode) {
        this.connectorCode = connectorCode;
    }

    public Long getConnectorId() {
        return connectorId;
    }

    public void setConnectorId(Long connectorId) {
        this.connectorId = connectorId;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }
    
}
