/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.ftth;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author partner1
 */
@Entity
@Table(name = "INTERRUPTION_EXTEND")
public class InterruptExtend implements Serializable {
    @Id
    @Column(name = "ID", nullable = false, length = 10, precision = 10, scale = 0)
    Long id;
    @Column(name = "INTERRUPTION_ID", length = 10, precision = 10, scale = 0)
    Long interruptionId;
    @Column(name = "INFLUENCE_TYPE", length = 10, precision = 10, scale = 0)
    Long influenceType;
    @Column(name = "STATION_ID", length = 10, precision = 10, scale = 0)
    Long stationId;
    @Column(name = "CONNECTOR_ID", length = 10, precision = 10, scale = 0)
    Long connectorId;
    @Column(name = "CODE")
    String code;
    @Column(name = "NAME")
    String name;

    public InterruptExtend() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getConnectorId() {
        return connectorId;
    }

    public void setConnectorId(Long connectorId) {
        this.connectorId = connectorId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInfluenceType() {
        return influenceType;
    }

    public void setInfluenceType(Long influenceType) {
        this.influenceType = influenceType;
    }

    public Long getInterruptionId() {
        return interruptionId;
    }

    public void setInterruptionId(Long interruptionId) {
        this.interruptionId = interruptionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

}
