/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.ftth;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author partner1
 */
@Entity
@Table(name = "INTERRUPT_REASON_TYPE")
public class InterruptReasonType implements Serializable {
    @Id
    @Column(name = "ID", nullable = false, length = 10, precision = 10, scale = 0)
    Long id;
    @Column(name = "CODE")
    String code;
    @Column(name = "NAME")
    String name;
    @Column(name = "STATUS", length = 2, precision = 2, scale = 0)
    Long status;

    public InterruptReasonType() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
    
}
