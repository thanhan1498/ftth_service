/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.ftth;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author partner1
 */
@Entity
@Table(name = "INTERRUPTION_SMS")
public class InterruptSms implements Serializable {
    @Id
    @Column(name = "ID", nullable = false, length = 10, precision = 10, scale = 0)
    Long id;
    @Column(name = "INTERRUPTION_ID", length = 10, precision = 10, scale = 0)
    Long interruptionId;
    @Column(name = "CREATE_USER")
    String createUser;
    @Column(name = "CREATE_DATE", length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    Date createDate;
    @Column(name = "SMS_CONTENT")
    String smsContent;
    @Column(name = "SERVICE_TYPE")
    String serviceType;
    @Column(name = "INFRA_TYPE")
    String infraType;

    @Transient
    Long numSendSms;
    
    public InterruptSms() {
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInfraType() {
        return infraType;
    }

    public void setInfraType(String infraType) {
        this.infraType = infraType;
    }

    public Long getInterruptionId() {
        return interruptionId;
    }

    public void setInterruptionId(Long interruptionId) {
        this.interruptionId = interruptionId;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getSmsContent() {
        return smsContent;
    }

    public void setSmsContent(String smsContent) {
        this.smsContent = smsContent;
    }

    public Long getNumSendSms() {
        return numSendSms;
    }

    public void setNumSendSms(Long numSendSms) {
        this.numSendSms = numSendSms;
    }

}
