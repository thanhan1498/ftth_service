/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.ftth;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author partner1
 */
@Entity
@Table(name = "INTERRUPTION")
public class Interruption implements Serializable {
    @Id
    @Column(name = "ID", nullable = false, length = 10, precision = 10, scale = 0)
    Long id;
    @Column(name = "STATUS", length = 2, precision = 2, scale = 0)
    Long status;
    @Column(name = "REASON_TYPE", length = 10, precision = 10, scale = 0)
    Long reasonType;
    @Column(name = "REASON_DETAIL", length = 10, precision = 10, scale = 0)
    Long reasonDetail;
    @Column(name = "AFFECT_FROM_DATE", length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    Date affectFromDate;
    @Column(name = "AFFECT_TO_DATE", length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    Date affectToDate;
    @Column(name = "AFFECT_CUSTOMER_TYPE")
    String affectCustomerType;
    @Column(name = "INFLUENCE_SCOPE", length = 2, precision = 2, scale = 0)
    Long influenceScope;
    @Column(name = "INFLUENCE_LEVEL", length = 2, precision = 2, scale = 0)
    Long influenceLevel;
    @Column(name = "SERVICE_INTERRUPTED")
    String serviceInterrupted;
    @Column(name = "DESCRIPTION")
    String description;
    @Column(name = "DOCUMENT_NAME")
    String documentName;
    @Lob
    @Column(name = "DOCUMENT_CONTENT", columnDefinition="BLOB")
    byte[] documentContent;
    @Column(name = "CREATE_USER")
    String createUser;
    @Column(name = "CREATE_DATE", length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    Date createDate;
    @Column(name = "UPDATE_USER")
    String updateUser;
    @Column(name = "UPDATE_DATE", length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    Date updateDate;
    @Column(name = "SHOP_ID", length = 10, precision = 10, scale = 0)
    Long shopId;
    
    @Transient
    List<InterruptExtend> lstStation;
    @Transient
    List<InterruptExtend> lstConnector;
    @Transient
    List<InterruptSms> lstInterruptSms;
    @Transient
    String reasonTypeName;
    @Transient
    String reasonDetailName;
    @Transient
    String smsStatusName;
    @Transient
    String shopBranchName;
            
    public Interruption() {
    }

    public String getAffectCustomerType() {
        return affectCustomerType;
    }

    public void setAffectCustomerType(String affectCustomerType) {
        this.affectCustomerType = affectCustomerType;
    }

    public Date getAffectFromDate() {
        return affectFromDate;
    }

    public void setAffectFromDate(Date affectFromDate) {
        this.affectFromDate = affectFromDate;
    }

    public Date getAffectToDate() {
        return affectToDate;
    }

    public void setAffectToDate(Date affectToDate) {
        this.affectToDate = affectToDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getDocumentContent() {
        return documentContent;
    }

    public void setDocumentContent(byte[] documentContent) {
        this.documentContent = documentContent;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInfluenceLevel() {
        return influenceLevel;
    }

    public void setInfluenceLevel(Long influenceLevel) {
        this.influenceLevel = influenceLevel;
    }

    public Long getInfluenceScope() {
        return influenceScope;
    }

    public void setInfluenceScope(Long influenceScope) {
        this.influenceScope = influenceScope;
    }

    public Long getReasonDetail() {
        return reasonDetail;
    }

    public void setReasonDetail(Long reasonDetail) {
        this.reasonDetail = reasonDetail;
    }

    public Long getReasonType() {
        return reasonType;
    }

    public void setReasonType(Long reasonType) {
        this.reasonType = reasonType;
    }

    public String getServiceInterrupted() {
        return serviceInterrupted;
    }

    public void setServiceInterrupted(String serviceInterrupted) {
        this.serviceInterrupted = serviceInterrupted;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public List<InterruptExtend> getLstConnector() {
        return lstConnector;
    }

    public void setLstConnector(List<InterruptExtend> lstConnector) {
        this.lstConnector = lstConnector;
    }

    public List<InterruptExtend> getLstStation() {
        return lstStation;
    }

    public void setLstStation(List<InterruptExtend> lstStation) {
        this.lstStation = lstStation;
    }

    public List<InterruptSms> getLstInterruptSms() {
        return lstInterruptSms;
    }

    public void setLstInterruptSms(List<InterruptSms> lstInterruptSms) {
        this.lstInterruptSms = lstInterruptSms;
    }

    public String getReasonDetailName() {
        return reasonDetailName;
    }

    public void setReasonDetailName(String reasonDetailName) {
        this.reasonDetailName = reasonDetailName;
    }

    public String getReasonTypeName() {
        return reasonTypeName;
    }

    public void setReasonTypeName(String reasonTypeName) {
        this.reasonTypeName = reasonTypeName;
    }

    public String getSmsStatusName() {
        return smsStatusName;
    }

    public void setSmsStatusName(String smsStatusName) {
        this.smsStatusName = smsStatusName;
    }

    public String getShopBranchName() {
        return shopBranchName;
    }

    public void setShopBranchName(String shopBranchName) {
        this.shopBranchName = shopBranchName;
    }

}