/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.ftth;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author partner1
 */
@Entity
@Table(name = "SUBSCRIBER_INTERRUPTION_SMS")
public class SubscriberInterruptSms implements Serializable {
    @Id
    @Column(name = "ID", nullable = false, length = 10, precision = 10, scale = 0)
    Long id;
    @Column(name = "INTERRUPTION_ID", length = 10, precision = 10, scale = 0)
    Long interruptionId;
    @Column(name = "CREATE_USER")
    String createUser;
    @Column(name = "CREATE_DATE", length = 7)
    @Temporal(TemporalType.TIMESTAMP)
    Date createDate;
    @Column(name = "SEND_SMS_ID")
    String sendSmsId;
    @Column(name = "ERROR_DESCRIPTION")
    String errorDescription;
    @Column(name = "ISDN_ACCOUNT")
    String isdnAccount;
    @Column(name = "CUSTOMER_NAME")
    String customerName;
    @Column(name = "PRODUCT_CODE")
    String productCode;
    @Column(name = "SERVICE_TYPE")
    String serviceType;
    @Column(name = "CONTRACT_NUMBER")
    String contractNumber;
    @Column(name = "DEPLOYMENT_ADDRESS")
    String deploymentAddress;
    @Column(name = "BTS_CODE")
    String btsCode;
    @Column(name = "BTS_ID", length = 10, precision = 10, scale = 0)
    Long btsId;
    @Column(name = "CONNECTOR_CODE")
    String connectorCode;
    @Column(name = "CONNECTOR_ID", length = 10, precision = 10, scale = 0)
    Long connectorId;
    @Column(name = "INFRA_TYPE")
    String infraType;
    @Column(name = "TEL_MOBILE")
    String telMobile;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    Long subId;

    public SubscriberInterruptSms() {
    }

    public String getBtsCode() {
        return btsCode;
    }

    public void setBtsCode(String btsCode) {
        this.btsCode = btsCode;
    }

    public Long getBtsId() {
        return btsId;
    }

    public void setBtsId(Long btsId) {
        this.btsId = btsId;
    }

    public String getConnectorCode() {
        return connectorCode;
    }

    public void setConnectorCode(String connectorCode) {
        this.connectorCode = connectorCode;
    }

    public Long getConnectorId() {
        return connectorId;
    }

    public void setConnectorId(Long connectorId) {
        this.connectorId = connectorId;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDeploymentAddress() {
        return deploymentAddress;
    }

    public void setDeploymentAddress(String deploymentAddress) {
        this.deploymentAddress = deploymentAddress;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInfraType() {
        return infraType;
    }

    public void setInfraType(String infraType) {
        this.infraType = infraType;
    }

    public Long getInterruptionId() {
        return interruptionId;
    }

    public void setInterruptionId(Long interruptionId) {
        this.interruptionId = interruptionId;
    }

    public String getIsdnAccount() {
        return isdnAccount;
    }

    public void setIsdnAccount(String isdnAccount) {
        this.isdnAccount = isdnAccount;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getSendSmsId() {
        return sendSmsId;
    }

    public void setSendSmsId(String sendSmsId) {
        this.sendSmsId = sendSmsId;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getTelMobile() {
        return telMobile;
    }

    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }

}
