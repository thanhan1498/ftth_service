/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.im;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "isdn_restore")
public class IsdnRestore implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "author_generator")
    @SequenceGenerator(name = "author_generator", sequenceName = "isdn_restore_seq")
    @Column(name = "ID", length = 10, precision = 10, scale = 0)
    private Long id;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "cust_id", length = 10, precision = 10, scale = 0)
    private Long custId;
    @Column(name = "isdn", length = 15)
    private String isdn;
    @Column(name = "old_imsi", length = 30)
    private String oldImsi;
    @Column(name = "new_imsi", length = 30)
    private String newImsi;
    @Column(name = "old_serial", length = 30)
    private String oldSerial;
    @Column(name = "new_serial", length = 30)
    private String newSerial;
    @Column(name = "removed_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date removedDate;
    @Column(name = "restored_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date restoreDate;
    @Column(name = "ERROR_CODE", length = 2)
    private String errorCode;
    @Column(name = "error_des", length = 200)
    private String errorDes;
    @Column(name = "res_staff_id", length = 10, precision = 10, scale = 0)
    private Long resStaffId;
    @Column(name = "res_shop_id", length = 10, precision = 10, scale = 0)
    private Long resShopId;
    @Column(name = "isdn_owner_id", length = 20)
    private String isdnOwnerId;
    @Column(name = "isdn_owner_type", length = 2)
    private String isdnOwnerType;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the subId
     */
    public Long getSubId() {
        return subId;
    }

    /**
     * @param subId the subId to set
     */
    public void setSubId(Long subId) {
        this.subId = subId;
    }

    /**
     * @return the custId
     */
    public Long getCustId() {
        return custId;
    }

    /**
     * @param custId the custId to set
     */
    public void setCustId(Long custId) {
        this.custId = custId;
    }

    /**
     * @return the isdn
     */
    public String getIsdn() {
        return isdn;
    }

    /**
     * @param isdn the isdn to set
     */
    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    /**
     * @return the oldImsi
     */
    public String getOldImsi() {
        return oldImsi;
    }

    /**
     * @param oldImsi the oldImsi to set
     */
    public void setOldImsi(String oldImsi) {
        this.oldImsi = oldImsi;
    }

    /**
     * @return the newImsi
     */
    public String getNewImsi() {
        return newImsi;
    }

    /**
     * @param newImsi the newImsi to set
     */
    public void setNewImsi(String newImsi) {
        this.newImsi = newImsi;
    }

    /**
     * @return the newSerial
     */
    public String getNewSerial() {
        return newSerial;
    }

    /**
     * @param newSerial the newSerial to set
     */
    public void setNewSerial(String newSerial) {
        this.newSerial = newSerial;
    }

    /**
     * @return the removedDate
     */
    public Date getRemovedDate() {
        return removedDate;
    }

    /**
     * @param removedDate the removedDate to set
     */
    public void setRemovedDate(Date removedDate) {
        this.removedDate = removedDate;
    }

    /**
     * @return the restoreDate
     */
    public Date getRestoreDate() {
        return restoreDate;
    }

    /**
     * @param restoreDate the restoreDate to set
     */
    public void setRestoreDate(Date restoreDate) {
        this.restoreDate = restoreDate;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * @return the errorDes
     */
    public String getErrorDes() {
        return errorDes;
    }

    /**
     * @param errorDes the errorDes to set
     */
    public void setErrorDes(String errorDes) {
        this.errorDes = errorDes;
    }

    /**
     * @return the resStaffId
     */
    public Long getResStaffId() {
        return resStaffId;
    }

    /**
     * @param resStaffId the resStaffId to set
     */
    public void setResStaffId(Long resStaffId) {
        this.resStaffId = resStaffId;
    }

    /**
     * @return the resShopId
     */
    public Long getResShopId() {
        return resShopId;
    }

    /**
     * @param resShopId the resShopId to set
     */
    public void setResShopId(Long resShopId) {
        this.resShopId = resShopId;
    }

    /**
     * @return the isdnOwnerId
     */
    public String getIsdnOwnerId() {
        return isdnOwnerId;
    }

    /**
     * @param isdnOwnerId the isdnOwnerId to set
     */
    public void setIsdnOwnerId(String isdnOwnerId) {
        this.isdnOwnerId = isdnOwnerId;
    }

    /**
     * @return the isdnOwnerType
     */
    public String getIsdnOwnerType() {
        return isdnOwnerType;
    }

    /**
     * @param isdnOwnerType the isdnOwnerType to set
     */
    public void setIsdnOwnerType(String isdnOwnerType) {
        this.isdnOwnerType = isdnOwnerType;
    }

    /**
     * @return the oldSerial
     */
    public String getOldSerial() {
        return oldSerial;
    }

    /**
     * @param oldSerial the oldSerial to set
     */
    public void setOldSerial(String oldSerial) {
        this.oldSerial = oldSerial;
    }

}
