/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.im;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author phuonghc
 */
@Entity
@Table(name = "SALE_ANYPAY_FILE")
public class SaleAnyPayFile implements Serializable {

    @Id
    @Column(name = "ID", length = 10, precision = 10, scale = 0)
    protected Long id;
    @Column(name = "CODE", length = 200)
    protected String code;
    @Column(name = "SUB_ID", length = 11)
    protected Long subId;
    @Column(name = "ISDN", length = 20)
    protected String isdn;
    @Column(name = "SERIAL", length = 30)
    protected String serial;
    @Column(name = "AMOUNT", length = 10)
    protected Long amount;
    @Column(name = "STATUS", length = 1)
    protected Long status;
    @Column(name = "CREATE_USER", length = 50)
    protected String createUser;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date createDate;
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date updateDate;
    @Column(name = "DESCRIPTION", length = 200)
    protected String description;
    @Column(name = "CHANNEL_CODE", length = 50)
    protected String channelCode;
    @Column(name = "CHANNEL_TYPE_ID", length = 5)
    protected Long channelTypeId;
    @Column(name = "SALE_TRANS_ID", length = 10)
    protected Long saleTransId;
    @Column(name = "TOPUP_BASIC", length = 10)
    protected String topupBasic;
    @Column(name = "TOPUP_VALID_SUB", length = 10)
    protected String topupValidSub;
    @Column(name = "TOPUP_PROMOTION", length = 10)
    protected String topupPromotion;
    @Column(name = "PROMOTION_VALID", length = 10)
    protected String promotionValid;
    @Column(name = "TOPUP_DATA", length = 10)
    protected String topupData;
    @Column(name = "DATA_VALID", length = 10)
    protected String dataValid;
    @Column(name = "SMS_CONTENT", length = 3000)
    protected String smsContent;
    @Column(name = "PROGRAM", length = 100)
    protected String program;
    @Column(name = "REASON_ID", length = 20)
    protected Long reasonId;
    @Column(name = "REQUESTER", length = 200)
    protected String requester;
    @Column(name = "DOCUMENT_NO", length = 1000)
    protected String documentNo;
    @Column(name = "TRANS_TYPE", length = 20)
    protected String transType;
    @Column(name = "PROCESS_STATUS", length = 1)
    protected Long processStatus;
    @Column(name = "IS_CORPORATE", length = 1)
    protected Long isCorporate;
    @Column(name = "REASON_NAME", length = 500)
    protected String reasonName;
    @Column(name = "IP", length = 50)
    protected String ip;
    @Column(name = "REASON_CM_ID", length = 20)
    protected Long reasonCmId;
    @Column(name = "TOPUP_BASIC_VALID", length = 5)
    protected String topupBasicValid;

    public SaleAnyPayFile() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public Long getChannelTypeId() {
        return channelTypeId;
    }

    public void setChannelTypeId(Long channelTypeId) {
        this.channelTypeId = channelTypeId;
    }

    public Long getSaleTransId() {
        return saleTransId;
    }

    public void setSaleTransId(Long saleTransId) {
        this.saleTransId = saleTransId;
    }

    public String getTopupBasic() {
        return topupBasic;
    }

    public void setTopupBasic(String topupBasic) {
        this.topupBasic = topupBasic;
    }

    public String getTopupValidSub() {
        return topupValidSub;
    }

    public void setTopupValidSub(String topupValidSub) {
        this.topupValidSub = topupValidSub;
    }

    public String getTopupPromotion() {
        return topupPromotion;
    }

    public void setTopupPromotion(String topupPromotion) {
        this.topupPromotion = topupPromotion;
    }

    public String getPromotionValid() {
        return promotionValid;
    }

    public void setPromotionValid(String promotionValid) {
        this.promotionValid = promotionValid;
    }

    public String getTopupData() {
        return topupData;
    }

    public void setTopupData(String topupData) {
        this.topupData = topupData;
    }

    public String getDataValid() {
        return dataValid;
    }

    public void setDataValid(String dataValid) {
        this.dataValid = dataValid;
    }

    public String getSmsContent() {
        return smsContent;
    }

    public void setSmsContent(String smsContent) {
        this.smsContent = smsContent;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public Long getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(Long processStatus) {
        this.processStatus = processStatus;
    }

    public Long getIsCorporate() {
        return isCorporate;
    }

    public void setIsCorporate(Long isCorporate) {
        this.isCorporate = isCorporate;
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Long getReasonCmId() {
        return reasonCmId;
    }

    public void setReasonCmId(Long reasonCmId) {
        this.reasonCmId = reasonCmId;
    }

    public String getTopupBasicValid() {
        return topupBasicValid;
    }

    public void setTopupBasicValid(String topupBasicValid) {
        this.topupBasicValid = topupBasicValid;
    }

}
