package com.viettel.bccs.cm.model.pk;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ApDomainId implements java.io.Serializable {

    @Column(name = "TYPE", length = 30)
    private String type;
    @Column(name = "CODE", length = 10)
    private String code;

    public ApDomainId() {
    }

    public ApDomainId(String type, String code) {
        this.type = type;
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object other) {
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof ApDomainId)) {
            return false;
        }
        ApDomainId castOther = (ApDomainId) other;
        boolean isEquals = this.type == null ? castOther.type == null : ((castOther.type != null) && this.type.equals(castOther.type));
        isEquals = isEquals && (this.code == null ? castOther.code == null : ((castOther.code != null) && this.code.equals(castOther.code)));

        return isEquals;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 37 * result + (this.type == null ? 0 : this.type.hashCode());
        result = 37 * result + (this.code == null ? 0 : this.code.hashCode());
        return result;
    }
}
