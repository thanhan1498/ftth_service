package com.viettel.bccs.cm.model.pk;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class DeployRequirementId implements java.io.Serializable {

    @Column(name = "REQ_ID")
    private Long reqId;
    @Column(name = "REQ_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reqDatetime;

    public DeployRequirementId() {
    }

    public DeployRequirementId(Long reqId, Date reqDatetime) {
        this.reqId = reqId;
        this.reqDatetime = reqDatetime;
    }

    public Long getReqId() {
        return reqId;
    }

    public void setReqId(Long reqId) {
        this.reqId = reqId;
    }

    public Date getReqDatetime() {
        return reqDatetime;
    }

    public void setReqDatetime(Date reqDatetime) {
        this.reqDatetime = reqDatetime;
    }

    @Override
    public boolean equals(Object other) {
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof DeployRequirementId)) {
            return false;
        }
        DeployRequirementId castOther = (DeployRequirementId) other;
        boolean isEquals = this.reqId == null ? castOther.reqId == null : ((castOther.reqId != null) && this.reqId.equals(castOther.reqId));
        isEquals = isEquals && (this.reqDatetime == null ? castOther.reqDatetime == null : ((castOther.reqDatetime != null) && this.reqDatetime.equals(castOther.reqDatetime)));

        return isEquals;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 37 * result + (this.reqId == null ? 0 : this.reqId.hashCode());
        result = 37 * result + (this.reqDatetime == null ? 0 : this.reqDatetime.hashCode());
        return result;
    }
}
