package com.viettel.bccs.cm.model.pk;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class SubAccountsAdslLlId implements java.io.Serializable {

    @Column(name = "ACCOUNT", length = 50)
    private String account;
    @Column(name = "EFFECT_FROM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectFrom;
    @Column(name = "PRODUCT_CODE")
    private String productCode;

    public SubAccountsAdslLlId() {
    }

    public SubAccountsAdslLlId(String account, Date effectFrom, String productCode) {
        this.account = account;
        this.effectFrom = effectFrom;
        this.productCode = productCode;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    @Override
    public boolean equals(Object other) {
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof SubAccountsAdslLlId)) {
            return false;
        }
        SubAccountsAdslLlId castOther = (SubAccountsAdslLlId) other;
        boolean isEquals = this.account == null ? castOther.account == null : ((castOther.account != null) && this.account.equals(castOther.account));
        isEquals = isEquals && (this.effectFrom == null ? castOther.effectFrom == null : ((castOther.effectFrom != null) && this.effectFrom.equals(castOther.effectFrom)));
        isEquals = isEquals && (this.productCode == null ? castOther.productCode == null : ((castOther.productCode != null) && this.productCode.equals(castOther.productCode)));

        return isEquals;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 37 * result + (this.account == null ? 0 : this.account.hashCode());
        result = 37 * result + (this.effectFrom == null ? 0 : this.effectFrom.hashCode());
        result = 37 * result + (this.productCode == null ? 0 : this.productCode.hashCode());
        return result;
    }
}
