package com.viettel.bccs.cm.model.pk;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class SubEmailAdslLlId implements java.io.Serializable {

    @Column(name = "EMAIL", length = 50)
    private String email;
    @Column(name = "EFFECT_FROM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectFrom;

    public SubEmailAdslLlId() {
    }

    public SubEmailAdslLlId(String email, Date effectFrom, String productCode) {
        this.email = email;
        this.effectFrom = effectFrom;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object other) {
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof SubEmailAdslLlId)) {
            return false;
        }
        SubEmailAdslLlId castOther = (SubEmailAdslLlId) other;
        boolean isEquals = this.email == null ? castOther.email == null : ((castOther.email != null) && this.email.equals(castOther.email));
        isEquals = isEquals && (this.effectFrom == null ? castOther.effectFrom == null : ((castOther.effectFrom != null) && this.effectFrom.equals(castOther.effectFrom)));

        return isEquals;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 37 * result + (this.email == null ? 0 : this.email.hashCode());
        result = 37 * result + (this.effectFrom == null ? 0 : this.effectFrom.hashCode());
        return result;
    }
}
