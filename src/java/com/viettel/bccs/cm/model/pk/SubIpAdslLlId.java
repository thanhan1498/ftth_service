package com.viettel.bccs.cm.model.pk;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class SubIpAdslLlId implements java.io.Serializable {

    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "IP", length = 64)
    private String ip;
    @Column(name = "EFFECT_FROM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectFrom;

    public SubIpAdslLlId() {
    }

    public SubIpAdslLlId(Long subId, String ip, Date effectFrom) {
        this.subId = subId;
        this.ip = ip;
        this.effectFrom = effectFrom;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    @Override
    public boolean equals(Object other) {
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof SubIpAdslLlId)) {
            return false;
        }
        SubIpAdslLlId castOther = (SubIpAdslLlId) other;
        boolean isEquals = this.subId == null ? castOther.subId == null : ((castOther.subId != null) && this.subId.equals(castOther.subId));
        isEquals = isEquals && (this.ip == null ? castOther.ip == null : ((castOther.ip != null) && this.ip.equals(castOther.ip)));
        isEquals = isEquals && (this.effectFrom == null ? castOther.effectFrom == null : ((castOther.effectFrom != null) && this.effectFrom.equals(castOther.effectFrom)));

        return isEquals;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 37 * result + (this.subId == null ? 0 : this.subId.hashCode());
        result = 37 * result + (this.ip == null ? 0 : this.ip.hashCode());
        result = 37 * result + (this.effectFrom == null ? 0 : this.effectFrom.hashCode());
        return result;
    }
}
