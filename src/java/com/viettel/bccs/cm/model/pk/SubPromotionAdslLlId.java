package com.viettel.bccs.cm.model.pk;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class SubPromotionAdslLlId implements java.io.Serializable {

    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "PROMOTION_CODE", length = 4)
    private String promotionCode;
    @Column(name = "EFFECT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date effect;

    public SubPromotionAdslLlId() {
    }

    public SubPromotionAdslLlId(Long subId, String promotionCode, Date effect) {
        this.subId = subId;
        this.promotionCode = promotionCode;
        this.effect = effect;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    public Date getEffect() {
        return effect;
    }

    public void setEffect(Date effect) {
        this.effect = effect;
    }

    @Override
    public boolean equals(Object other) {
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof SubPromotionAdslLlId)) {
            return false;
        }
        SubPromotionAdslLlId castOther = (SubPromotionAdslLlId) other;
        boolean isEquals = this.subId == null ? castOther.subId == null : ((castOther.subId != null) && this.subId.equals(castOther.subId));
        isEquals = isEquals && (this.promotionCode == null ? castOther.promotionCode == null : ((castOther.promotionCode != null) && this.promotionCode.equals(castOther.promotionCode)));
        isEquals = isEquals && (this.effect == null ? castOther.effect == null : ((castOther.effect != null) && this.effect.equals(castOther.effect)));

        return isEquals;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 37 * result + (this.subId == null ? 0 : this.subId.hashCode());
        result = 37 * result + (this.promotionCode == null ? 0 : this.promotionCode.hashCode());
        result = 37 * result + (this.effect == null ? 0 : this.effect.hashCode());
        return result;
    }
}
