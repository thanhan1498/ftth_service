package com.viettel.bccs.cm.model.pk;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class TaskManagementId implements Serializable {

    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "TASK_MNGT_ID", length = 10, precision = 10, scale = 0)
    private Long taskMngtId;

    public TaskManagementId() {
    }

    public TaskManagementId(Date createDate, Long taskMngtId) {
        this.createDate = createDate;
        this.taskMngtId = taskMngtId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getTaskMngtId() {
        return taskMngtId;
    }

    public void setTaskMngtId(Long taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    @Override
    public boolean equals(Object other) {
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof TaskManagementId)) {
            return false;
        }
        TaskManagementId castOther = (TaskManagementId) other;
        boolean isEquals = this.createDate == null ? castOther.createDate == null : ((castOther.createDate != null) && this.createDate.equals(castOther.createDate));
        isEquals = isEquals && (this.taskMngtId == null ? castOther.taskMngtId == null : ((castOther.taskMngtId != null) && this.taskMngtId.equals(castOther.taskMngtId)));

        return isEquals;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 37 * result + (this.createDate == null ? 0 : this.createDate.hashCode());
        result = 37 * result + (this.taskMngtId == null ? 0 : this.taskMngtId.hashCode());
        return result;
    }
}
