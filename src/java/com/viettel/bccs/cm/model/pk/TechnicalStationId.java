package com.viettel.bccs.cm.model.pk;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TechnicalStationId implements java.io.Serializable {

    @Column(name = "TEAM_ID", length = 10, precision = 10, scale = 0)
    private Long teamId;
    @Column(name = "STATION_ID", length = 10, precision = 10, scale = 0)
    private Long stationId;

    public TechnicalStationId() {
    }

    public TechnicalStationId(Long teamId, Long stationId) {
        this.teamId = teamId;
        this.stationId = stationId;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    @Override
    public boolean equals(Object other) {
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof TechnicalStationId)) {
            return false;
        }
        TechnicalStationId castOther = (TechnicalStationId) other;
        boolean isEquals = this.teamId == null ? castOther.teamId == null : ((castOther.teamId != null) && this.teamId.equals(castOther.teamId));
        isEquals = isEquals && (this.stationId == null ? castOther.stationId == null : ((castOther.stationId != null) && this.stationId.equals(castOther.stationId)));

        return isEquals;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 37 * result + (this.teamId == null ? 0 : this.teamId.hashCode());
        result = 37 * result + (this.stationId == null ? 0 : this.stationId.hashCode());
        return result;
    }
}
