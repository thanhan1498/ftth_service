/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model;

import java.io.Serializable;
import javax.persistence.Transient;

/**
 *
 * @author nhandv
 */
public class portOnBox implements Serializable {

    @Transient
    private String port;
    @Transient
    private String cabinetCode;

    public portOnBox() {
    }

    public String getCabinetCode() {
        return cabinetCode;
    }

    public void setCabinetCode(String cabinetCode) {
        this.cabinetCode = cabinetCode;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

}
