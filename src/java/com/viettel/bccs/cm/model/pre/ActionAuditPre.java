/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.pre;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "ACTION_AUDIT")
public class ActionAuditPre implements java.io.Serializable {

    @Id
    @Column(name = "ACTION_AUDIT_ID", length = 20, precision = 20, scale = 0)
    private Long actionAuditId;
    @Column(name = "ISSUE_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date issueDatetime;
    @Column(name = "ACTION_CODE", length = 10)
    private String actionCode;
    @Column(name = "REASON_ID", length = 10, precision = 20, scale = 0)
    private Long reasonId;
    @Column(name = "SHOP_CODE", length = 20)
    private String shopCode;
    @Column(name = "USER_NAME", length = 50)
    private String userName;
    @Column(name = "PK_TYPE", length = 1)
    private String pkType;
    @Column(name = "PK_ID", length = 11, precision = 11, scale = 0)
    private Long pkId;
    @Column(name = "IP", length = 20)
    private String ip;
    @Column(name = "DESCRIPTION", length = 500)
    private String description;
    @Column(name = "VALID", length = 1, precision = 1, scale = 0)
    private Long valid;
    @Column(name = "FILE_ID", length = 11, precision = 11, scale = 0)
    private Long fileId;

    // Constructors
    /**
     * default constructor
     */
    public ActionAuditPre() {
    }

    /**
     * minimal constructor
     */
    public ActionAuditPre(Long actionAuditId, Date issueDatetime, String userName,
            String pkType, Long pkId) {
        this.actionAuditId = actionAuditId;
        this.issueDatetime = issueDatetime;
        this.userName = userName;
        this.pkType = pkType;
        this.pkId = pkId;
    }

    /**
     * full constructor
     */
    public ActionAuditPre(Long actionAuditId, Date issueDatetime,
            String actionCode, Long reasonId, String shopCode, String userName,
            String pkType, Long pkId, String ip, String description, Long valid, String actionName, String reasonName) {
        this.actionAuditId = actionAuditId;
        this.issueDatetime = issueDatetime;
        this.actionCode = actionCode;
        this.reasonId = reasonId;
        this.shopCode = shopCode;
        this.userName = userName;
        this.pkType = pkType;
        this.pkId = pkId;
        this.ip = ip;
        this.description = description;
        this.valid = valid;
    }

    // Property accessors
    public Long getActionAuditId() {
        return this.actionAuditId;
    }

    public void setActionAuditId(Long actionAuditId) {
        this.actionAuditId = actionAuditId;
    }

    public Date getIssueDatetime() {
        return this.issueDatetime;
    }

    public void setIssueDatetime(Date issueDatetime) {
        this.issueDatetime = issueDatetime;
    }

    public String getActionCode() {
        return this.actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public Long getReasonId() {
        return this.reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public String getShopCode() {
        return this.shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPkType() {
        return this.pkType;
    }

    public void setPkType(String pkType) {
        this.pkType = pkType;
    }

    public Long getPkId() {
        return this.pkId;
    }

    public void setPkId(Long pkId) {
        this.pkId = pkId;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getValid() {
        return this.valid;
    }

    public void setValid(Long valid) {
        this.valid = valid;
    }

    /**
     * @return the fileId
     */
    public Long getFileId() {
        return fileId;
    }

    /**
     * @param fileId the fileId to set
     */
    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }
}
