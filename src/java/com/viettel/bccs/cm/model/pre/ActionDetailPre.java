/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.pre;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "ACTION_DETAIL")
public class ActionDetailPre implements java.io.Serializable {

    @Id
    @Column(name = "ACTION_DETAIL_ID", length = 10, precision = 10, scale = 0)
    private Long actionDetailId;
    @Column(name = "ACTION_AUDIT_ID", length = 10, precision = 10, scale = 0)
    private Long actionAuditId;
    @Column(name = "ROW_ID", length = 15, precision = 15, scale = 0)
    private Long rowId;
    @Column(name = "TABLE_NAME", length = 20)
    private String tableName;
    @Column(name = "COL_NAME", length = 20)
    private String colName;
    @Column(name = "OLD_VALUE", length = 200)
    private String oldValue;
    @Column(name = "NEW_VALUE", length = 200)
    private String newValue;

    public Date getIssueDateTime() {
        return issueDateTime;
    }

    public void setIssueDateTime(Date issueDateTime) {
        this.issueDateTime = issueDateTime;
    }
    @Column(name = "ISSUE_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date issueDateTime;

    // Constructors
    /**
     * default constructor
     */
    public ActionDetailPre() {
    }

    /**
     * minimal constructor
     */
    public ActionDetailPre(Long actionDetailId, Long rowId, String tableName,
            String colName) {
        this.actionDetailId = actionDetailId;
        this.rowId = rowId;
        this.tableName = tableName;
        this.colName = colName;
    }

    /**
     * full constructor
     */
    public ActionDetailPre(Long actionDetailId, Long actionAuditId, Long rowId,
            String tableName, String colName, String oldValue, String newValue) {
        this.actionDetailId = actionDetailId;
        this.actionAuditId = actionAuditId;
        this.rowId = rowId;
        this.tableName = tableName;
        this.colName = colName;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    // Property accessors
    public Long getActionDetailId() {
        return this.actionDetailId;
    }

    public void setActionDetailId(Long actionDetailId) {
        this.actionDetailId = actionDetailId;
    }

    public Long getActionAuditId() {
        return this.actionAuditId;
    }

    public void setActionAuditId(Long actionAuditId) {
        this.actionAuditId = actionAuditId;
    }

    public Long getRowId() {
        return this.rowId;
    }

    public void setRowId(Long rowId) {
        this.rowId = rowId;
    }

    public String getTableName() {
        return this.tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColName() {
        return this.colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getOldValue() {
        return this.oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return this.newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }
}
