package com.viettel.bccs.cm.model.pre;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ACTION_LOG_PR")
public class ActionLogPrPre implements java.io.Serializable {

    @Id
    @Column(name = "ID", length = 22, precision = 22, scale = 0)
    private Long id;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "ISDN", length = 32)
    private String isdn;
    @Column(name = "SERIAL", length = 50)
    private String serial;
    @Column(name = "IMSI", length = 20)
    private String imsi;
    @Column(name = "ACTION_TYPE_ID", length = 10, precision = 10, scale = 0)
    private Long actionTypeId;
    @Column(name = "REQUEST", length = 4000)
    private String request;
    @Column(name = "RESPONSE", length = 4000)
    private String response;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "SHOP_CODE", length = 10)
    private String shopCode;
    @Column(name = "USER_NAME", length = 40)
    private String userName;
    @Column(name = "RESPONSE_CODE", length = 100)
    private String responseCode;
    @Column(name = "EXCEPTION", length = 4000)
    private String exception;

    public ActionLogPrPre() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public Long getActionTypeId() {
        return actionTypeId;
    }

    public void setActionTypeId(Long actionTypeId) {
        this.actionTypeId = actionTypeId;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }
}
