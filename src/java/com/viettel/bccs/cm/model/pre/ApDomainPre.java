/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.pre;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "AP_DOMAIN")
public class ApDomainPre implements Serializable {

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the reasonType
     */
    public String getReasonType() {
        return reasonType;
    }

    /**
     * @param reasonType the reasonType to set
     */
    public void setReasonType(String reasonType) {
        this.reasonType = reasonType;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the groupCode
     */
    public String getGroupCode() {
        return groupCode;
    }

    /**
     * @param groupCode the groupCode to set
     */
    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    @Id
    @Column(name = "TYPE", length = 30, nullable = false)
    private String type;
    @Column(name = "CODE", length = 30, nullable = false)
    private String code;
    @Column(name = "NAME", length = 30, nullable = false)
    private String name;
    @Column(name = "VALUE", length = 30, nullable = false)
    private String value;
    @Column(name = "REASON_TYPE", length = 30, nullable = false)
    private String reasonType;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "GROUP_CODE", length = 30, nullable = false)
    private String groupCode;
}
