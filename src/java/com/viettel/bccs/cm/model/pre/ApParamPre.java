package com.viettel.bccs.cm.model.pre;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author linhlh2
 */
@Entity
@Table(name = "AP_PARAM")
public class ApParamPre implements Serializable {

    @Id
    @Column(name = "PARAM_ID", length = 10, precision = 10, scale = 0)
    private Long paramId;
    @Column(name = "PARAM_TYPE", length = 30, nullable = false)
    private String paramType;
    @Column(name = "PARAM_CODE", length = 30, nullable = false)
    private String paramCode;
    @Column(name = "PARAM_NAME", length = 100, nullable = false)
    private String paramName;
    @Column(name = "PARAM_VALUE", length = 200)
    private String paramValue;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;

    public Long getParamId() {
        return paramId;
    }

    public void setParamId(Long paramId) {
        this.paramId = paramId;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getParamCode() {
        return paramCode;
    }

    public void setParamCode(String paramCode) {
        this.paramCode = paramCode;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
