package com.viettel.bccs.cm.model.pre;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author vietnn
 */
@Entity
@Table(name = "CUSTOMER")
public class CustomerPre implements Serializable {

    @Id
    @Column(name = "CUST_ID", length = 11, precision = 10, scale = 0)
    private Long custId;
    @Column(name = "BUS_TYPE", nullable = false, length = 10, precision = 10, scale = 0)
    private String busType;
    @Column(name = "NAME", length = 120, nullable = false)
    private String name;
    @Column(name = "BIRTH_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthDate;
    @Column(name = "SEX")
    private String sex;
    @Column(name = "CUST_TYPE_ID", length = 38, precision = 38, scale = 0)
    private Long custTypeId;
    @Column(name = "ID_TYPE", length = 10, precision = 10, scale = 0)
    private Long idType;
    @Column(name = "ID_NO", length = 20)
    private String idNo;
    @Column(name = "ID_ISSUE_PLACE", length = 70)
    private String idIssuePlace;
    @Column(name = "ID_ISSUE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date idIssueDate;
    @Column(name = "ID_EXPIRE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date idExpireDate;
    @Column(name = "POP_NO", length = 20)
    private String popNo;
    @Column(name = "BUS_PERMIT_NO", length = 20)
    private String busPermitNo;
    @Column(name = "POP_ISSUE_PLACE", length = 50)
    private String popIssuePlace;
    @Column(name = "POP_ISSUE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date popIssueDate;
    @Column(name = "TIN", length = 15)
    private String tin;
    @Column(name = "NATIONALITY")
    private String nationality;
    @Column(name = "ADDRESS", length = 500)
    private String address;
    @Column(name = "TEL_FAX", length = 15)
    private String telFax;
    @Column(name = "EMAIL", length = 50)
    private String email;
    @Column(name = "AREA_CODE", length = 15)
    private String areaCode;
    @Column(name = "PROVINCE", length = 20)
    private String province;
    @Column(name = "DISTRICT", length = 3)
    private String district;
    @Column(name = "PRECINCT", length = 4)
    private String precinct;
    @Column(name = "STREET_NAME", length = 50)
    private String streetName;
    @Column(name = "STREET_BLOCK_NAME", length = 50)
    private String streetBlockName;
    @Column(name = "HOME", length = 50)
    private String home;
    @Column(name = "VIP", length = 1)
    private String vip;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "ADDED_USER", length = 50)
    private String addedUser;
    @Column(name = "ADDED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedDate;
    @Column(name = "UPDATED_USER", length = 50)
    private String updatedUser;
    @Column(name = "UPDATED_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTime;
    @Column(name = "NOTES", length = 200)
    private String notes;
    @Column(name = "CORRECT_CUS", length = 1)
    private String correctCus;

    public CustomerPre() {
    }

    public CustomerPre(Long custId) {
        this.custId = custId;
    }

    public String getCorrectCus() {
        return correctCus;
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public Long getCustTypeId() {
        return custTypeId;
    }

    public void setCustTypeId(Long custTypeId) {
        this.custTypeId = custTypeId;
    }

    public Long getIdType() {
        return idType;
    }

    public void setIdType(Long idType) {
        this.idType = idType;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getIdIssuePlace() {
        return idIssuePlace;
    }

    public void setIdIssuePlace(String idIssuePlace) {
        this.idIssuePlace = idIssuePlace;
    }

    public Date getIdIssueDate() {
        return idIssueDate;
    }

    public void setIdIssueDate(Date idIssueDate) {
        this.idIssueDate = idIssueDate;
    }

    public Date getIdExpireDate() {
        return idExpireDate;
    }

    public void setIdExpireDate(Date idExpireDate) {
        this.idExpireDate = idExpireDate;
    }

    public String getPopNo() {
        return popNo;
    }

    public void setPopNo(String popNo) {
        this.popNo = popNo;
    }

    public String getBusPermitNo() {
        return busPermitNo;
    }

    public void setBusPermitNo(String busPermitNo) {
        this.busPermitNo = busPermitNo;
    }

    public String getPopIssuePlace() {
        return popIssuePlace;
    }

    public void setPopIssuePlace(String popIssuePlace) {
        this.popIssuePlace = popIssuePlace;
    }

    public Date getPopIssueDate() {
        return popIssueDate;
    }

    public void setPopIssueDate(Date popIssueDate) {
        this.popIssueDate = popIssueDate;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelFax() {
        return telFax;
    }

    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }


    public String getStreetBlockName() {
        return streetBlockName;
    }

    public void setStreetBlockName(String streetBlockName) {
        this.streetBlockName = streetBlockName;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getAddedUser() {
        return addedUser;
    }

    public void setAddedUser(String addedUser) {
        this.addedUser = addedUser;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setCorrectCus(String correctCus) {
        this.correctCus = correctCus;
    }
}
