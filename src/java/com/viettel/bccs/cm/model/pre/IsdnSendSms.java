package com.viettel.bccs.cm.model.pre;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ISDN_SEND_SMS")
public class IsdnSendSms implements Serializable {

    @Id
    @Column(name = "ID", length = 12, precision = 12, scale = 0)
    private Long id;
    @Column(name = "INSERT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;
    @Column(name = "ISDN", length = 20)
    private String isdn;
    @Column(name = "SMS_CONTENT", length = 4000)
    private String smsContent;
    @Column(name = "PROCESS_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date processDate;
    @Column(name = "PROCESS_STATUS", length = 2)
    private String processStatus;
    @Column(name = "USER_NAME", length = 50)
    private String userName;
    @Column(name = "SHOP_CODE", length = 50)
    private String shopCode;
    @Column(name = "SEND_TIMES", length = 2, precision = 2, scale = 0)
    private Long sendTimes;
    @Column(name = "MAX_SEND_TIME", length = 2, precision = 2, scale = 0)
    private Long maxSendTime;
    @Column(name = "APP_CODE", length = 50)
    private String appCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getSmsContent() {
        return smsContent;
    }

    public void setSmsContent(String smsContent) {
        this.smsContent = smsContent;
    }

    public Date getProcessDate() {
        return processDate;
    }

    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    public String getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public Long getSendTimes() {
        return sendTimes;
    }

    public void setSendTimes(Long sendTimes) {
        this.sendTimes = sendTimes;
    }

    public Long getMaxSendTime() {
        return maxSendTime;
    }

    public void setMaxSendTime(Long maxSendTime) {
        this.maxSendTime = maxSendTime;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }
}
