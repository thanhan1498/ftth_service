/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.pre;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author User-PC
 */
@Entity
@Table(name = "MAP_ACTIVE_INFO")
public class MapActiveInfoPre implements Serializable {

    @Id
    @Column(name = "ID", length = 22, precision = 22, scale = 0)
    protected Long id;
    @Column(name = "TEL_SERVICE_ID", nullable = false, length = 10, precision = 10, scale = 0)
    protected Long telServiceId;
    @Column(name = "PRODUCT_CODE", nullable = false, length = 10, precision = 10, scale = 0)
    protected String productCode;
    @Column(name = "Offer_id", length = 1, precision = 1, scale = 0)
    protected Long offerId;
    @Column(name = "Offer_Name", length = 100)
    protected String offerName;
    @Column(name = "REG_REASON_ID", length = 10, precision = 10, scale = 0, nullable = false)
    protected Long regReasonId;
    @Column(name = "REASON_NAME", length = 100)
    protected String reasonName;
    @Column(name = "PROM_CODE", length = 5)
    protected String promCode;
    @Column(name = "PROM_NAME", length = 150)
    protected String promName;
    @Column(name = "CHANNEL_TYPE_ID", nullable = false, length = 1, precision = 1, scale = 0)
    protected Long channelTypeId;
    @Column(name = "CHANNEL_NAME", length = 50)
    protected String channelName;
    @Column(name = "PROVINCE_CODE", nullable = false, length = 4)
    protected String provinceCode;
    @Column(name = "DISTRICT_CODE", nullable = false, length = 3)
    protected String districtCode;
    @Column(name = "EFFECT_DATE", nullable = false, length = 7)
    protected Date effectDate;
    @Column(name = "END_DATE", nullable = false, length = 7)
    protected Date endDate;
    @Column(name = "PROVINCE_NAME", nullable = false, length = 50)
    protected String provinceName;
    @Column(name = "DISTRICT_NAME", nullable = false, length = 50)
    protected String districtName;
    @Column(name = "status")
    protected Long status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTelServiceId() {
        return telServiceId;
    }

    public void setTelServiceId(Long telServiceId) {
        this.telServiceId = telServiceId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public Long getRegReasonId() {
        return regReasonId;
    }

    public void setRegReasonId(Long regReasonId) {
        this.regReasonId = regReasonId;
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    public String getPromCode() {
        return promCode;
    }

    public void setPromCode(String promCode) {
        this.promCode = promCode;
    }

    public String getPromName() {
        return promName;
    }

    public void setPromName(String promName) {
        this.promName = promName;
    }

    public Long getChannelTypeId() {
        return channelTypeId;
    }

    public void setChannelTypeId(Long channelTypeId) {
        this.channelTypeId = channelTypeId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public Date getEffectDate() {
        return effectDate;
    }

    public void setEffectDate(Date effectDate) {
        this.effectDate = effectDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}