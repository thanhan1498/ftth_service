/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.pre;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Mapping")
public class MappingPre implements Serializable {

    @Id
    @Column(name = "ID", length = 22, precision = 22, scale = 0)
    private Long id;
    @Column(name = "PRODUCT_CODE", length = 10)
    private String productCode;
    @Column(name = "ACTION_CODE", nullable = false, length = 30)
    private String actionCode;
    @Column(name = "SALE_SERVICE_ID", length = 10, precision = 10, scale = 0)
    private Long saleServiceId;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "VAS", length = 10)
    private String vas;
    @Column(name = "PRODUCT_NAME", length = 100)
    private String productName;
    @Column(name = "VAS_NAME", length = 100)
    private String vasName;
    @Column(name = "SALE_SERVICE_NAME", length = 100)
    private String saleServiceName;
    @Column(name = "REASON_NAME", length = 100)
    private String reasonName;
    @Column(name = "ACTION_NAME", length = 100)
    private String actionName;
    @Column(name = "TEL_SERVICE_ID", length = 10, precision = 10, scale = 0)
    private Long telServiceId;
    @Column(name = "REASON_ID", nullable = false, length = 10, precision = 10, scale = 0)
    private Long reasonId;
    @Column(name = "CHANNEL", length = 1, precision = 1, scale = 0)
    private Long channel;
    @Column(name = "SALE_SERVICE_CODE", length = 20)
    private String saleServiceCode;
    @Column(name = "USER_CREATE", length = 30)
    private String userCreate;
    @Column(name = "USER_UPDATE", length = 30)
    private String userUpdate;
    @Column(name = "CREATE_DATETIME")
    private Date createDatetime;
    @Column(name = "CHANGE_DATETIME")
    private Date changeDatetime;
    @Column(name = "IP", length = 50)
    private String ip;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public Long getSaleServiceId() {
        return saleServiceId;
    }

    public void setSaleServiceId(Long saleServiceId) {
        this.saleServiceId = saleServiceId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getVas() {
        return vas;
    }

    public void setVas(String vas) {
        this.vas = vas;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getVasName() {
        return vasName;
    }

    public void setVasName(String vasName) {
        this.vasName = vasName;
    }

    public String getSaleServiceName() {
        return saleServiceName;
    }

    public void setSaleServiceName(String saleServiceName) {
        this.saleServiceName = saleServiceName;
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public Long getTelServiceId() {
        return telServiceId;
    }

    public void setTelServiceId(Long telServiceId) {
        this.telServiceId = telServiceId;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public Long getChannel() {
        return channel;
    }

    public void setChannel(Long channel) {
        this.channel = channel;
    }

    public String getSaleServiceCode() {
        return saleServiceCode;
    }

    public void setSaleServiceCode(String saleServiceCode) {
        this.saleServiceCode = saleServiceCode;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Date getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }

    public Date getChangeDatetime() {
        return changeDatetime;
    }

    public void setChangeDatetime(Date changeDatetime) {
        this.changeDatetime = changeDatetime;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
