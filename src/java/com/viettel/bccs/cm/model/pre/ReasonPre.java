/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.pre;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author User-PC
 */
@Entity
@Table(name = "REASON")
public class ReasonPre {

    @Id
    @Column(name = "REASON_ID", length = 10, precision = 10, scale = 0)
    private Long reasonId;
    @Column(name = "TYPE", nullable = false, length = 10)
    private String type;
    @Column(name = "CODE", length = 10)
    private String code;
    @Column(name = "NAME", nullable = false)
    private String name;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "SER_TRANS_ID", length = 10)
    private String serTransId;
    @Column(name = "REQ_PROFILE", length = 1)
    private String reqProfile;
    @Column(name = "DESCRIPTION", length = 22, scale = 5)
    private String description;
    @Column(name = "LIMIT_NUMBER_ISDN", length = 10, precision = 10, scale = 0)
    private Long limitNumberIsdn;
    @Column(name = "LIMIT_NUMBER_USER", length = 10, precision = 10, scale = 0)
    private Long limitNumberUser;

    // Constructors
    /**
     * default constructor
     */
    public ReasonPre() {
    }

    /**
     * minimal constructor
     */
    public ReasonPre(Long reasonId, String type, String name) {
        this.reasonId = reasonId;
        this.type = type;
        this.name = name;
    }

    /**
     * full constructor
     */
    public ReasonPre(Long reasonId, String code, String type, String name,
            String description, Long status, String serTransId) {
        this.reasonId = reasonId;
        this.code = code;
        this.type = type;
        this.name = name;
        this.description = description;
        this.status = status;
        this.serTransId = serTransId;
    }

    public Long getLimitNumberIsdn() {
        return limitNumberIsdn;
    }

    public void setLimitNumberIsdn(Long limitNumberIsdn) {
        this.limitNumberIsdn = limitNumberIsdn;
    }

    public Long getLimitNumberUser() {
        return limitNumberUser;
    }

    public void setLimitNumberUser(Long limitNumberUser) {
        this.limitNumberUser = limitNumberUser;
    }

    // Property accessors
    public String getReqProfile() {
        return reqProfile;
    }

    public void setReqProfile(String reqProfile) {
        this.reqProfile = reqProfile;
    }

    public Long getReasonId() {
        return this.reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStatus() {
        return this.status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getSerTransId() {
        return this.serTransId;
    }

    public void setSerTransId(String serTransId) {
        this.serTransId = serTransId;
    }
}
