/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.pre;

import java.util.Date;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "SALE_TRANS")
public class SaleTransPre implements java.io.Serializable {
    
    @Id
    @Column(name = "SALE_TRANS_ID", length = 10, precision = 10, scale = 0)
    private Long saleTransId;
    @Column(name = "SALE_TRANS_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date saleTransDate;
    @Column(name = "SALE_TRANS_TYPE", length = 2)
    private String saleTransType;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "CHECK_STOCK", length = 2)
    private String checkStock;
    @Column(name = "INVOICE_USED_ID", length = 10, precision = 10, scale = 0)
    private Long invoiceUsedId;
    @Column(name = "INVOICE_CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date invoiceCreateDate;
    @Column(name = "SHOP_ID", length = 10, precision = 10, scale = 0)
    private Long shopId;
    @Column(name = "STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long staffId;
    @Column(name = "PAY_METHOD", length = 2)
    private String payMethod;
    @Column(name = "SALE_SERVICE_ID", length = 22, precision = 22, scale = 0)
    private Long saleServiceId;
    @Column(name = "SALE_SERVICE_PRICE_ID", length = 22, precision = 22, scale = 0)
    private Long saleServicePriceId;
    @Column(name = "AMOUNT_SERVICE", length = 22, precision = 22, scale = 0)
    private Long amountService;
    @Column(name = "AMOUNT_MODEL", length = 22, precision = 22, scale = 0)
    private Long amountModel;
    @Column(name = "DISCOUNT", length = 22, precision = 22, scale = 0)
    private Long discount;
    @Column(name = "PROMOTION", length = 20, precision = 20, scale = 4)
    private Double promotion;
    @Column(name = "AMOUNT_TAX", length = 22, precision = 22, scale = 0)
    private Long amountTax;
    @Column(name = "AMOUNT_NOT_TAX", length = 22, precision = 22, scale = 0)
    private Long amountNotTax;
    @Column(name = "VAT", length = 22, precision = 22, scale = 0)
    private Long vat;
    @Column(name = "TAX", length = 22, precision = 22, scale = 0)
    private Long tax;
    @Column(name = "SUB_ID", length = 22, precision = 22, scale = 0)
    private Long subId;
    @Column(name = "ISDN", length = 20)
    private String isdn;
    @Column(name = "CUST_NAME", length = 50)
    private String custName;
    @Column(name = "CONTRACT_NO", length = 50)
    private String contractNo;
    @Column(name = "TEL_NUMBER", length = 12)
    private String telNumber;
    @Column(name = "COMPANY", length = 100)
    private String company;
    @Column(name = "ADDRESS", length = 100)
    private String address;
    @Column(name = "TIN", length = 100)
    private String tin;
    @Column(name = "NOTE", length = 200)
    private String note;
    @Column(name = "DESTROY_USER", length = 20)
    private String destroyUser;
    @Column(name = "DESTROY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date destroyDate;
    @Column(name = "APPROVER_USER", length = 20)
    private String approverUser;
    @Column(name = "APPROVER_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date approverDate;
    @Column(name = "REASON_ID", length = 10, precision = 10, scale = 0)
    private Long reasonId;
    @Column(name = "TELECOM_SERVICE_ID", length = 22, precision = 22, scale = 0)
    private Long telecomServiceId;
    @Column(name = "TRANSFER_GOODS", length = 1)
    private String transferGoods;
    @Column(name = "SALE_TRANS_CODE", length = 30)
    private String saleTransCode;
    @Column(name = "STOCK_TRANS_ID", length = 10, precision = 10, scale = 0)
    private Long stockTransId;
    @Column(name = "CREATE_STAFF_ID", length = 10, precision = 10, scale = 0)
    private Long createStaffId;
    @Column(name = "SHOP_CODE", length = 10)
    private String shopCode;
    @Column(name = "TRANS_RESULT", length = 1, precision = 1, scale = 0)
    private Long transResult;
    @Column(name = "SERIAL", length = 30)
    private String serial;
    @Lob
    @Column(name = "LST_STOCK_ISDN", length = 4000)
    private Blob lstStockIsdn;
    @Lob
    @Column(name = "LST_STOCK_SIM", length = 4000)
    private Blob lstStockSim;

    @Column(name = "ACTION_CODE", length = 30)
    private String actionCode;

    // Constructors
    /**
     * default constructor
     */
    public SaleTransPre() {
    }

    /**
     * minimal constructor
     */
    public SaleTransPre(Long saleTransId, Date saleTransDate, Long status) {
        this.saleTransId = saleTransId;
        this.saleTransDate = saleTransDate;
        this.status = status;
    }

    /**
     * full constructor
     */
    public SaleTransPre(Long saleTransId, Date saleTransDate,
            String saleTransType, Long status, String checkStock,
            Long invoiceUsedId, Date invoiceCreateDate, Long shopId,
            Long staffId, String payMethod, Long saleServiceId,
            Long saleServicePriceId, Long amountService, Long amountModel,
            Long discount, Double promotion, Long amountTax, Long amountNotTax,
            Long vat, Long tax, Long subId, String isdn, String custName,
            String contractNo, String telNumber, String company,
            String address, String tin, String note, String destroyUser,
            Date destroyDate, String approverUser, Date approverDate,
            Long reasonId, Long telecomServiceId, String transferGoods,
            String saleTransCode, Long stockTransId, Long createStaffId) {
        this.saleTransId = saleTransId;
        this.saleTransDate = saleTransDate;
        this.saleTransType = saleTransType;
        this.status = status;
        this.checkStock = checkStock;
        this.invoiceUsedId = invoiceUsedId;
        this.invoiceCreateDate = invoiceCreateDate;
        this.shopId = shopId;
        this.staffId = staffId;
        this.payMethod = payMethod;
        this.saleServiceId = saleServiceId;
        this.saleServicePriceId = saleServicePriceId;
        this.amountService = amountService;
        this.amountModel = amountModel;
        this.discount = discount;
        this.promotion = promotion;
        this.amountTax = amountTax;
        this.amountNotTax = amountNotTax;
        this.vat = vat;
        this.tax = tax;
        this.subId = subId;
        this.isdn = isdn;
        this.custName = custName;
        this.contractNo = contractNo;
        this.telNumber = telNumber;
        this.company = company;
        this.address = address;
        this.tin = tin;
        this.note = note;
        this.destroyUser = destroyUser;
        this.destroyDate = destroyDate;
        this.approverUser = approverUser;
        this.approverDate = approverDate;
        this.reasonId = reasonId;
        this.telecomServiceId = telecomServiceId;
        this.transferGoods = transferGoods;
        this.saleTransCode = saleTransCode;
        this.stockTransId = stockTransId;
        this.createStaffId = createStaffId;
    }

    // Property accessors
    public Long getSaleTransId() {
        return this.saleTransId;
    }

    public void setSaleTransId(Long saleTransId) {
        this.saleTransId = saleTransId;
    }

    public Date getSaleTransDate() {
        return this.saleTransDate;
    }

    public void setSaleTransDate(Date saleTransDate) {
        this.saleTransDate = saleTransDate;
    }

    public String getSaleTransType() {
        return this.saleTransType;
    }

    public void setSaleTransType(String saleTransType) {
        this.saleTransType = saleTransType;
    }

    public Long getStatus() {
        return this.status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getCheckStock() {
        return this.checkStock;
    }

    public void setCheckStock(String checkStock) {
        this.checkStock = checkStock;
    }

    public Long getInvoiceUsedId() {
        return this.invoiceUsedId;
    }

    public void setInvoiceUsedId(Long invoiceUsedId) {
        this.invoiceUsedId = invoiceUsedId;
    }

    public Date getInvoiceCreateDate() {
        return this.invoiceCreateDate;
    }

    public void setInvoiceCreateDate(Date invoiceCreateDate) {
        this.invoiceCreateDate = invoiceCreateDate;
    }

    public Long getShopId() {
        return this.shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getStaffId() {
        return this.staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getPayMethod() {
        return this.payMethod;
    }

    public void setPayMethod(String payMethod) {
        this.payMethod = payMethod;
    }

    public Long getSaleServiceId() {
        return this.saleServiceId;
    }

    public void setSaleServiceId(Long saleServiceId) {
        this.saleServiceId = saleServiceId;
    }

    public Long getSaleServicePriceId() {
        return this.saleServicePriceId;
    }

    public void setSaleServicePriceId(Long saleServicePriceId) {
        this.saleServicePriceId = saleServicePriceId;
    }

    public Long getAmountService() {
        return this.amountService;
    }

    public void setAmountService(Long amountService) {
        this.amountService = amountService;
    }

    public Long getAmountModel() {
        return this.amountModel;
    }

    public void setAmountModel(Long amountModel) {
        this.amountModel = amountModel;
    }

    public Long getDiscount() {
        return this.discount;
    }

    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    public Double getPromotion() {
        return this.promotion;
    }

    public void setPromotion(Double promotion) {
        this.promotion = promotion;
    }

    public Long getAmountTax() {
        return this.amountTax;
    }

    public void setAmountTax(Long amountTax) {
        this.amountTax = amountTax;
    }

    public Long getAmountNotTax() {
        return this.amountNotTax;
    }

    public void setAmountNotTax(Long amountNotTax) {
        this.amountNotTax = amountNotTax;
    }

    public Long getVat() {
        return this.vat;
    }

    public void setVat(Long vat) {
        this.vat = vat;
    }

    public Long getTax() {
        return this.tax;
    }

    public void setTax(Long tax) {
        this.tax = tax;
    }

    public Long getSubId() {
        return this.subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getIsdn() {
        return this.isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getCustName() {
        return this.custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getContractNo() {
        return this.contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getTelNumber() {
        return this.telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public String getCompany() {
        return this.company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTin() {
        return this.tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDestroyUser() {
        return this.destroyUser;
    }

    public void setDestroyUser(String destroyUser) {
        this.destroyUser = destroyUser;
    }

    public Date getDestroyDate() {
        return this.destroyDate;
    }

    public void setDestroyDate(Date destroyDate) {
        this.destroyDate = destroyDate;
    }

    public String getApproverUser() {
        return this.approverUser;
    }

    public void setApproverUser(String approverUser) {
        this.approverUser = approverUser;
    }

    public Date getApproverDate() {
        return this.approverDate;
    }

    public void setApproverDate(Date approverDate) {
        this.approverDate = approverDate;
    }

    public Long getReasonId() {
        return this.reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public Long getTelecomServiceId() {
        return this.telecomServiceId;
    }

    public void setTelecomServiceId(Long telecomServiceId) {
        this.telecomServiceId = telecomServiceId;
    }

    public String getTransferGoods() {
        return this.transferGoods;
    }

    public void setTransferGoods(String transferGoods) {
        this.transferGoods = transferGoods;
    }

    public String getSaleTransCode() {
        return this.saleTransCode;
    }

    public void setSaleTransCode(String saleTransCode) {
        this.saleTransCode = saleTransCode;
    }

    public Long getStockTransId() {
        return this.stockTransId;
    }

    public void setStockTransId(Long stockTransId) {
        this.stockTransId = stockTransId;
    }

    public Long getCreateStaffId() {
        return this.createStaffId;
    }

    public void setCreateStaffId(Long createStaffId) {
        this.createStaffId = createStaffId;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public void setTransResult(Long transResult) {
        this.transResult = transResult;
    }

    public Long getTransResult() {
        return transResult;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public Blob getLstStockIsdn() {
        return lstStockIsdn;
    }

    public void setLstStockIsdn(Blob lstStockIsdn) {
        this.lstStockIsdn = lstStockIsdn;
    }

    public Blob getLstStockSim() {
        return lstStockSim;
    }

    public void setLstStockSim(Blob lstStockSim) {
        this.lstStockSim = lstStockSim;
    }


    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

}
