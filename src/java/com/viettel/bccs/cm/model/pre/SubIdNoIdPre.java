/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.pre;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Embeddable
public class SubIdNoIdPre implements Serializable{

    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "STA_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date staDatetime;

    /**
     * @return the subId
     */
    public Long getSubId() {
        return subId;
    }

    /**
     * @param subId the subId to set
     */
    public void setSubId(Long subId) {
        this.subId = subId;
    }

    /**
     * @return the staDatetime
     */
    public Date getStaDatetime() {
        return staDatetime;
    }

    /**
     * @param staDatetime the staDatetime to set
     */
    public void setStaDatetime(Date staDatetime) {
        this.staDatetime = staDatetime;
    }

}
