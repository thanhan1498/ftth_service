/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.pre;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "SUB_MB")
public class SubMbPre implements Serializable {

    @Id
    @Column(name = "sub_id", length = 11, precision = 11, scale = 0)
    private Long subId;
    @Column(name = "cust_id", length = 11, precision = 11, scale = 0)
    private Long custId;
    @Column(name = "isdn", length = 10)
    private String isdn;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "act_status", length = 2)
    private String actStatus;
    @Column(name = "sta_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date staDatetime;
    @Column(name = "end_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDatetime;
    @Column(name = "finish_reason_id", length = 10, precision = 10, scale = 0)
    private Long finishReasonId;
    @Column(name = "reg_type", length = 30)
    private String regType;
    @Column(name = "vip", length = 1)
    private String vip;
    @Column(name = "user_created", length = 50)
    private String userCreated;
    @Column(name = "shop_code", length = 50)
    private String shopCode;
    @Column(name = "offer_id", length = 10, precision = 1, scale = 0)
    private Long offerId;
    @Column(name = "sub_name", length = 200)
    private String subName;
    @Column(name = "birth_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthDate;
    @Column(name = "gender", length = 1)
    private String gender;
    @Column(name = "province", length = 10)
    private String province;
    @Column(name = "district", length = 10)
    private String district;
    @Column(name = "precinct", length = 10)
    private String precinct;
    @Column(name = "address", length = 255)
    private String address;
    @Column(name = "subcos_huawei", length = 10)
    private String subcosHuawei;
    @Column(name = "subcos_zte", length = 10)
    private String subcosZte;
    @Column(name = "start_money", length = 10)
    private String startMoney;
    @Column(name = "imsi", length = 15)
    private String imsi;
    @Column(name = "serial", length = 20)
    private String serial;
    @Column(name = "password", length = 32)
    private String password;
    @Column(name = "valid_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validDatetime;
    @Column(name = "change_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date changeDatetime;
    @Column(name = "promotion_code", length = 3)
    private String promotionCode;
    @Column(name = "first_exp_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date firstExpDate;
    @Column(name = "second_exp_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date secondExpDate;
    @Column(name = "notes", length = 200)
    private String notes;
    @Column(name = "first_shop_code", length = 50)
    private String firstShopCode;
    @Column(name = "prom_valid_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date promValidDatetime;
    @Column(name = "product_code", length = 30)
    private String productCode;
    @Column(name = "org_product_code", length = 30)
    private String orgProductCode;
    @Column(name = "last_number", length = 1)
    private String lastNumber;
    @Column(name = "num_reset_zone", length = 10, precision = 10, scale = 0)
    private Long numResetZone;
    @Column(name = "sub_birth")
    @Temporal(TemporalType.TIMESTAMP)
    private Date subBirth;
    @Column(name = "email", length = 100)
    private String email;
    @Column(name = "tel_fax", length = 15)
    private String telFax;
    @Column(name = "tel_mobile", length = 15)
    private String telMobile;
    @Column(name = "nick_name", length = 50)
    private String nickName;
    @Column(name = "nick_domain", length = 5)
    private String nickDomain;
    @Column(name = "dev_shop_code", length = 40)
    private String devShopCode;
    @Column(name = "dev_staff_code", length = 40)
    private String devStaffCode;
    @Column(name = "staff_id", length = 10, precision = 10, scale = 0)
    private Long staaffId;

    /**
     * @return the subId
     */
    public Long getSubId() {
        return subId;
    }

    /**
     * @param subId the subId to set
     */
    public void setSubId(Long subId) {
        this.subId = subId;
    }

    /**
     * @return the custId
     */
    public Long getCustId() {
        return custId;
    }

    /**
     * @param custId the custId to set
     */
    public void setCustId(Long custId) {
        this.custId = custId;
    }

    /**
     * @return the isdn
     */
    public String getIsdn() {
        return isdn;
    }

    /**
     * @param isdn the isdn to set
     */
    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the actStatus
     */
    public String getActStatus() {
        return actStatus;
    }

    /**
     * @param actStatus the actStatus to set
     */
    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    /**
     * @return the staDatetime
     */
    public Date getStaDatetime() {
        return staDatetime;
    }

    /**
     * @param staDatetime the staDatetime to set
     */
    public void setStaDatetime(Date staDatetime) {
        this.staDatetime = staDatetime;
    }

    /**
     * @return the endDatetime
     */
    public Date getEndDatetime() {
        return endDatetime;
    }

    /**
     * @param endDatetime the endDatetime to set
     */
    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    /**
     * @return the finishReasonId
     */
    public Long getFinishReasonId() {
        return finishReasonId;
    }

    /**
     * @param finishReasonId the finishReasonId to set
     */
    public void setFinishReasonId(Long finishReasonId) {
        this.finishReasonId = finishReasonId;
    }

    /**
     * @return the regType
     */
    public String getRegType() {
        return regType;
    }

    /**
     * @param regType the regType to set
     */
    public void setRegType(String regType) {
        this.regType = regType;
    }

    /**
     * @return the vip
     */
    public String getVip() {
        return vip;
    }

    /**
     * @param vip the vip to set
     */
    public void setVip(String vip) {
        this.vip = vip;
    }

    /**
     * @return the userCreated
     */
    public String getUserCreated() {
        return userCreated;
    }

    /**
     * @param userCreated the userCreated to set
     */
    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    /**
     * @return the shopCode
     */
    public String getShopCode() {
        return shopCode;
    }

    /**
     * @param shopCode the shopCode to set
     */
    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    /**
     * @return the offerId
     */
    public Long getOfferId() {
        return offerId;
    }

    /**
     * @param offerId the offerId to set
     */
    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    /**
     * @return the subName
     */
    public String getSubName() {
        return subName;
    }

    /**
     * @param subName the subName to set
     */
    public void setSubName(String subName) {
        this.subName = subName;
    }

    /**
     * @return the birthDate
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * @param birthDate the birthDate to set
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the province
     */
    public String getProvince() {
        return province;
    }

    /**
     * @param province the province to set
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     * @return the district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * @param district the district to set
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     * @return the precinct
     */
    public String getPrecinct() {
        return precinct;
    }

    /**
     * @param precinct the precinct to set
     */
    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the subcosHuawei
     */
    public String getSubcosHuawei() {
        return subcosHuawei;
    }

    /**
     * @param subcosHuawei the subcosHuawei to set
     */
    public void setSubcosHuawei(String subcosHuawei) {
        this.subcosHuawei = subcosHuawei;
    }

    /**
     * @return the subcosZte
     */
    public String getSubcosZte() {
        return subcosZte;
    }

    /**
     * @param subcosZte the subcosZte to set
     */
    public void setSubcosZte(String subcosZte) {
        this.subcosZte = subcosZte;
    }

    /**
     * @return the startMoney
     */
    public String getStartMoney() {
        return startMoney;
    }

    /**
     * @param startMoney the startMoney to set
     */
    public void setStartMoney(String startMoney) {
        this.startMoney = startMoney;
    }

    /**
     * @return the imsi
     */
    public String getImsi() {
        return imsi;
    }

    /**
     * @param imsi the imsi to set
     */
    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    /**
     * @return the serial
     */
    public String getSerial() {
        return serial;
    }

    /**
     * @param serial the serial to set
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the validDatetime
     */
    public Date getValidDatetime() {
        return validDatetime;
    }

    /**
     * @param validDatetime the validDatetime to set
     */
    public void setValidDatetime(Date validDatetime) {
        this.validDatetime = validDatetime;
    }

    /**
     * @return the changeDatetime
     */
    public Date getChangeDatetime() {
        return changeDatetime;
    }

    /**
     * @param changeDatetime the changeDatetime to set
     */
    public void setChangeDatetime(Date changeDatetime) {
        this.changeDatetime = changeDatetime;
    }

    /**
     * @return the promotionCode
     */
    public String getPromotionCode() {
        return promotionCode;
    }

    /**
     * @param promotionCode the promotionCode to set
     */
    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    /**
     * @return the firstExpDate
     */
    public Date getFirstExpDate() {
        return firstExpDate;
    }

    /**
     * @param firstExpDate the firstExpDate to set
     */
    public void setFirstExpDate(Date firstExpDate) {
        this.firstExpDate = firstExpDate;
    }

    /**
     * @return the secondExpDate
     */
    public Date getSecondExpDate() {
        return secondExpDate;
    }

    /**
     * @param secondExpDate the secondExpDate to set
     */
    public void setSecondExpDate(Date secondExpDate) {
        this.secondExpDate = secondExpDate;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the firstShopCode
     */
    public String getFirstShopCode() {
        return firstShopCode;
    }

    /**
     * @param firstShopCode the firstShopCode to set
     */
    public void setFirstShopCode(String firstShopCode) {
        this.firstShopCode = firstShopCode;
    }

    /**
     * @return the promValidDatetime
     */
    public Date getPromValidDatetime() {
        return promValidDatetime;
    }

    /**
     * @param promValidDatetime the promValidDatetime to set
     */
    public void setPromValidDatetime(Date promValidDatetime) {
        this.promValidDatetime = promValidDatetime;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the orgProductCode
     */
    public String getOrgProductCode() {
        return orgProductCode;
    }

    /**
     * @param orgProductCode the orgProductCode to set
     */
    public void setOrgProductCode(String orgProductCode) {
        this.orgProductCode = orgProductCode;
    }

    /**
     * @return the lastNumber
     */
    public String getLastNumber() {
        return lastNumber;
    }

    /**
     * @param lastNumber the lastNumber to set
     */
    public void setLastNumber(String lastNumber) {
        this.lastNumber = lastNumber;
    }

    /**
     * @return the numResetZone
     */
    public Long getNumResetZone() {
        return numResetZone;
    }

    /**
     * @param numResetZone the numResetZone to set
     */
    public void setNumResetZone(Long numResetZone) {
        this.numResetZone = numResetZone;
    }

    /**
     * @return the subBirth
     */
    public Date getSubBirth() {
        return subBirth;
    }

    /**
     * @param subBirth the subBirth to set
     */
    public void setSubBirth(Date subBirth) {
        this.subBirth = subBirth;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the telFax
     */
    public String getTelFax() {
        return telFax;
    }

    /**
     * @param telFax the telFax to set
     */
    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    /**
     * @return the telMobile
     */
    public String getTelMobile() {
        return telMobile;
    }

    /**
     * @param telMobile the telMobile to set
     */
    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }

    /**
     * @return the nickName
     */
    public String getNickName() {
        return nickName;
    }

    /**
     * @param nickName the nickName to set
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    /**
     * @return the nickDomain
     */
    public String getNickDomain() {
        return nickDomain;
    }

    /**
     * @param nickDomain the nickDomain to set
     */
    public void setNickDomain(String nickDomain) {
        this.nickDomain = nickDomain;
    }

    /**
     * @return the devShopCode
     */
    public String getDevShopCode() {
        return devShopCode;
    }

    /**
     * @param devShopCode the devShopCode to set
     */
    public void setDevShopCode(String devShopCode) {
        this.devShopCode = devShopCode;
    }

    /**
     * @return the devStaffCode
     */
    public String getDevStaffCode() {
        return devStaffCode;
    }

    /**
     * @param devStaffCode the devStaffCode to set
     */
    public void setDevStaffCode(String devStaffCode) {
        this.devStaffCode = devStaffCode;
    }

    /**
     * @return the staaffId
     */
    public Long getStaaffId() {
        return staaffId;
    }

    /**
     * @param staaffId the staaffId to set
     */
    public void setStaaffId(Long staaffId) {
        this.staaffId = staaffId;
    }

}
