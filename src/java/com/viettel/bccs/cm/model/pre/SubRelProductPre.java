/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.pre;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "Sub_Rel_Product")
public class SubRelProductPre implements Serializable{

    @Id
    @Column(name = "Sub_Rel_Product_ID", length = 10, precision = 10, scale = 0)
    private Long subRelProductId;
    @Column(name = "sub_id", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "MAIN_PRODUCT_CODE", length = 50)
    private String mainProductCode;
    @Column(name = "REL_PRODUCT_CODE", length = 50)
    private String relProductCode;
    @Column(name = "STA_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date staDatetime;
    @Column(name = "END_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDatetime;
    @Column(name = "REG_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date regDate;
    @Column(name = "REL_PRODUCT_VALUE", length = 50)
    private String relProductValue;
    @Column(name = "status", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "is_connected", length = 1, precision = 1, scale = 0)
    private Long isConnected;

    /**
     * default constructor
     */
    public SubRelProductPre() {
    }

    /**
     * minimal constructor
     */
    public SubRelProductPre(Long subRelProductId, Date staDatetime, Date regDate) {
        this.subRelProductId = subRelProductId;
        this.staDatetime = staDatetime;
        this.regDate = regDate;
    }

    /**
     * full constructor
     */
    public SubRelProductPre(Long subRelProductId, Long subId, String mainProductCode,
            String relProductCode, Date staDatetime, Date endDatetime,
            Date regDate, String relProductValue) {
        this.subRelProductId = subRelProductId;
        this.subId = subId;
        this.mainProductCode = mainProductCode;
        this.relProductCode = relProductCode;
        this.staDatetime = staDatetime;
        this.endDatetime = endDatetime;
        this.regDate = regDate;
        this.relProductValue = relProductValue;
    }

    // Property accessors
    public Long getSubRelProductId() {
        return this.subRelProductId;
    }

    public void setSubRelProductId(Long subRelProductId) {
        this.subRelProductId = subRelProductId;
    }

    public Long getSubId() {
        return this.subId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getMainProductCode() {
        return mainProductCode;
    }

    public void setMainProductCode(String mainProductCode) {
        this.mainProductCode = mainProductCode;
    }

    public String getRelProductCode() {
        return relProductCode;
    }

    public void setRelProductCode(String relProductCode) {
        this.relProductCode = relProductCode;
    }

    public Date getStaDatetime() {
        return this.staDatetime;
    }

    public void setStaDatetime(Date staDatetime) {
        this.staDatetime = staDatetime;
    }

    public Date getEndDatetime() {
        return this.endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public Date getRegDate() {
        return this.regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public String getRelProductValue() {
        return this.relProductValue;
    }

    public void setRelProductValue(String relProductValue) {
        this.relProductValue = relProductValue;
    }

    /**
     * @return the isConnected
     */
    public Long getIsConnected() {
        return isConnected;
    }

    /**
     * @param isConnected the isConnected to set
     */
    public void setIsConnected(Long isConnected) {
        this.isConnected = isConnected;
    }
}
