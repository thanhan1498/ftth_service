/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.model.pre;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "SUB_SIM_MB")
public class SubSimMbPre implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "author_generator")
    @SequenceGenerator(name = "author_generator", sequenceName = "SEQ_SUB_SIM_MB")
    @Column(name = "id", length = 22, precision = 22, scale = 0)
    private Long id;
    @Column(name = "SUB_ID", length = 10, precision = 10, scale = 0)
    private Long subId;
    @Column(name = "imsi", length = 15)
    private String imsi;
    @Column(name = "STA_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date staDatetime;
    @Column(name = "END_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDatetime;
    @Column(name = "STATUS", length = 1, precision = 1, scale = 0)
    private Long status;
    @Column(name = "serial", length = 20)
    private String serial;
    // Constructors

    /**
     * default constructor
     */
    public SubSimMbPre() {
    }

    /**
     * minimal constructor
     */
    public SubSimMbPre(Long id, Long subId, String imsi, Date staDatetime) {
        this.id = id;
        this.subId = subId;
        this.imsi = imsi;
        this.staDatetime = staDatetime;
    }

    /**
     * full constructor
     */
    public SubSimMbPre(Long id, Long subId, String imsi, Date staDatetime,
            Date endDatetime, Long status) {
        this.id = id;
        this.subId = subId;
        this.imsi = imsi;
        this.staDatetime = staDatetime;
        this.endDatetime = endDatetime;
        this.status = status;
    }

    // Property accessors
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSubId() {
        return this.subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getImsi() {
        return this.imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public Date getStaDatetime() {
        return this.staDatetime;
    }

    public void setStaDatetime(Date staDatetime) {
        this.staDatetime = staDatetime;
    }

    public Date getEndDatetime() {
        return this.endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public Long getStatus() {
        return this.status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }
}
