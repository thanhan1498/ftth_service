/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.supplier;

import com.google.gson.Gson;
import com.viettel.bccs.cm.model.AccountInfra;
import com.viettel.bccs.cm.model.BoxCode;
import com.viettel.bccs.cm.model.FrasUpdateLog;
import com.viettel.bccs.cm.model.LenghtCable;
import com.viettel.bccs.cm.model.LocationODF;
import com.viettel.bccs.cm.model.LocationSN;
import com.viettel.bccs.cm.model.TechInfo;
import com.viettel.bccs.cm.model.portOnBox;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.brcd.ws.model.output.CustomerInfraResponse;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.brcd.ws.model.output.customerInfrasDetail;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author nhandv
 */
public class AccountInfrasSupplier extends BaseSupplier {

    public AccountInfrasSupplier() {
        logger = Logger.getLogger(SaBaySupplier.class);
    }

    public List<AccountInfra> getLstAccountInfra(Session cmPosSession,
            String staffID, String account, String infrasType,
            String infrasStatus, String btsCode, String staffCode, Integer start, Integer max,
            String connectorCode, String deviceCode, String shopPath) {
        List<AccountInfra> lst = new ArrayList<AccountInfra>();
        StringBuilder sql = new StringBuilder()
                .append(" SELECT DISTINCT g.account_isdn account,g.infra_type infraType,"
                        + "  st.station_code stationCode, g.connector_code connectorCode"
                        + " ,sub.cust_lat cLat, sub.cust_long cLong "
                        + " ,g.staff_id staffId, g.device_code deviceCode, g.port "
                        + " FROM   (SELECT   a.account_isdn,\n"
                        + "                   b.staff_id,\n"
                        + "                   b.connector_code,\n"
                        + "                   b.station_id,\n"
                        //+ "                   c.cust_lat,\n"
                        //+ "                   c.cust_long,\n"
                        + "                   b.infra_type,\n"
                        + "                   NULL port,\n"
                        + " (SELECT   device_code\n" 
                        + "  FROM    nims_fcn.infra_device gpon\n" 
                        + "  WHERE   gpon.device_id = a.device_id) device_code\n"
                        + "           FROM   nims_cn.ccn_subscription a,\n"
                        + "                   technical_connector b\n"
                        //+ "                   sub_adsl_ll c\n"
                        + "           WHERE       a.box_id = b.connector_id\n"
                        //+ "                   AND c.account = a.account_isdn\n"
                        //+ "                   AND c.status = 2\n"
                        + "                   AND b.status = 1\n"
                        + "                   AND b.infra_type = 'CCN'\n"
                        + "          UNION ALL\n"
                        + "          SELECT   a.account_isdn,\n"
                        + "                   b.staff_id,\n"
                        + "                   b.connector_code,\n"
                        + "                   b.station_id,\n"
                        //+ "                   c.cust_lat,\n"
                        //+ "                   c.cust_long,\n"
                        + "                   b.infra_type,\n"
                        + "                   p.port,\n"
                        + " (SELECT  device_code\n" 
                        + "  FROM   nims_fcn.infra_device gpon\n" 
                        + "  WHERE   gpon.device_id = a.device_id) device_code\n"
                        + "           FROM    nims_cn.ccn_subscription a,\n"
                        + "                   technical_connector b,\n"
                        + "                   nims_fcn.infra_device dd,\n"
                        + "                   nims_fcn.infra_port p\n"
                        //+ "                   sub_adsl_ll c\n"
                        + "           WHERE       a.odf_id = b.connector_id\n"
                        + "                   AND a.port_id = p.port_id\n"
                        + "                   AND p.device_id = dd.device_id\n"
                        + "                   AND b.status = 1\n"
                        + "                   AND a.odf_id IS NOT NULL\n"
                        + "                   AND b.infra_type = 'AON'\n"
                        + "          UNION ALL\n"
                        + "          SELECT   s.account_isdn,\n"
                        + "                   tc.staff_id,\n"
                        + "                   tc.connector_code,\n"
                        + "                   tc.station_id,\n"
                        //+ "                   y.cust_lat,\n"
                        //+ "                   y.cust_long,\n"
                        + "                   tc.infra_type\n,"
                        + "                   p.port,\n"
                        + " (SELECT   device_code\n" 
                        + "  FROM   nims_fcn.infra_device gpon\n" 
                        + "  WHERE   gpon.device_id = s.device_id) device_code\n"
                        + "           FROM   nims_fcn.infra_device a,\n"
                        + "                   nims_fcn.pon_device nt,\n"
                        + "                   nims_fcn.infra_device dd,\n"
                        + "                   nims_fcn.infra_port p,\n"
                        + "                   nims_cn.ccn_subscription s,\n"
                        + "                   technical_connector tc\n"
                        //+ "                   sub_adsl_ll y\n"
                        + "           WHERE       nt.parent_id = a.device_id\n"
                        + "                   AND dd.device_id = nt.device_id\n"
                        + "                   AND p.device_id = dd.device_id\n"
                        + "                   AND s.port_spliter_id = p.port_id\n"
                        + "                   AND tc.connector_code = a.device_code\n"
                        //+ "                   AND y.account = s.account_isdn\n"
                        + "                   AND tc.status = 1\n"
                        + "                   AND tc.infra_type = 'GPON'\n"
                        + "                   AND s.port_spliter_id IS NOT NULL\n"
                        + "                   AND s.department_id IS NOT NULL\n"
                        + "                   ) g INNER JOIN NIMS_FCN.infra_stations st ON g.station_id=st.station_id, sub_adsl_ll sub ")//staff sta, shop sh
                .append(" WHERE g.staff_id = :staffId ")
                //.append(" AND sta.shop_id = sh.shop_id ")
                .append(" AND g.account_isdn = sub.account ")
                //.append(" AND sub.sub_id = dep.sub_id ")
                .append(" AND sub.status = 2 ");
                //.append(" AND sh.shop_path LIKE :shopPath ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        //params.put("shopPath", "%" + shopPath + "%");
        params.put("staffId", staffID);
        //  params.put("staffId", "20441104"); // gán cứng để test 20441104
        //  staffCode = "VTC_PNP_TEAM5_LENGSOKTHY"; // gán cứng để test
        String url = "";
        if (account != null && !account.isEmpty() && !account.equals("?")) {
            sql.append(" AND g.account_isdn = :account");
            params.put("account", account);

        }
        if (infrasType != null && !infrasType.isEmpty() && !infrasType.equals("?")) {
            sql.append(" AND g.infra_type = :infra_type");
            params.put("infra_type", infrasType);
        }
        if (btsCode != null && !btsCode.isEmpty() && !btsCode.equals("?")) {
            sql.append(" AND st.station_code = :station_code");
            params.put("station_code", btsCode);
        }
        if (deviceCode != null && !deviceCode.isEmpty() && !deviceCode.equals("?")) {
            sql.append(" AND g.device_code = :deviceCode");
            params.put("deviceCode", deviceCode);
        }
        if (connectorCode != null && !connectorCode.isEmpty() && !connectorCode.equals("?")) {
            sql.append(" AND g.connector_code = :connectorCode");
            params.put("connectorCode", connectorCode);
        }
        //sql.append(" AND ROWNUM <= 50");
        if (infrasStatus != null && !infrasStatus.isEmpty() && !infrasStatus.equals("?")) {
            url = Constants.URL_NIM_ACCOUNT_INFRAS + "action=search&staff_code=" + staffCode + "&status=" + infrasStatus;
        }
        Query query = cmPosSession.createSQLQuery(sql.toString())
                .addScalar("account", Hibernate.STRING)
                .addScalar("infraType", Hibernate.STRING)
                .addScalar("stationCode", Hibernate.STRING)
                .addScalar("connectorCode", Hibernate.STRING)
                .addScalar("cLat", Hibernate.STRING)
                .addScalar("cLong", Hibernate.STRING)
                .addScalar("deviceCode", Hibernate.STRING)
                .addScalar("port", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(AccountInfra.class));
        buildParameter(query, params);

        List<AccountInfra> result = query.list();
        logger.error("getNismsListAccount:result=" + url + " &start=" + start + " max= " + max);
        if (infrasStatus != null && !infrasStatus.isEmpty() && !infrasStatus.equals("?")) {
            String response = new ServiceSupplier().sendRequest(Constants.SERVICE_METHOD_GET, url, null, new WSRespone(), null);
            CustomerInfraResponse serviceResponse = new Gson().fromJson(response, CustomerInfraResponse.class);
            logger.error("getNismsListAccount:result=" + response);
            if (serviceResponse != null) {
                List<customerInfrasDetail> lstData = serviceResponse.getData();
                if (!lstData.isEmpty()) {
                    for (AccountInfra accountInfra : result) {
                        boolean isHave = false;
                        for (customerInfrasDetail customer : lstData) {
                            if (customer.getAccount().equals(accountInfra.getAccount())
                                    && customer.getStatus().equals(infrasStatus)) {
                                isHave = true;
                                break;
                            }
                        }
                        if (isHave) {
                            lst.add(accountInfra);
                        }
                    }
                }
            }
        } else {
            lst = result;
        }
        List<AccountInfra> rs = new ArrayList<AccountInfra>();
        if (start >= lst.size()) {
            start = lst.size() - 1;
        }
        if ((max + start) >= lst.size()) {
            max = lst.size();
        }

        for (int c = start; c < (max + start); c++) {
            rs.add(lst.get(c));
        }
//        if (start != null) {
//            lst.(start);
//        }
//        if (max != null) {
//            query.setMaxResults(max);
//        }
        return rs;
    }

    public Integer getCountAccount(Session cmPosSession,
            String staffID, String account, String infrasType,
            String infrasStatus, String btsCode, String staffCode,
            String connectorCode, String deviceCode, String shopPath) {
        List<AccountInfra> lst = new ArrayList<AccountInfra>();
        StringBuilder sql = new StringBuilder()
                .append(" SELECT DISTINCT g.account_isdn account,g.infra_type infraType,"
                        + "  st.station_code stationCode, g.connector_code connectorCode"
                        + " ,sub.cust_lat cLat, sub.cust_long cLong "
                        + " ,g.staff_id staffId, g.device_code deviceCode, g.port "
                        + " FROM   (SELECT   a.account_isdn,\n"
                        + "                   b.staff_id,\n"
                        + "                   b.connector_code,\n"
                        + "                   b.station_id,\n"
                        //+ "                   c.cust_lat,\n"
                        //+ "                   c.cust_long,\n"
                        + "                   b.infra_type,\n"
                        + "                   NULL port,\n"
                        + " (SELECT   device_code\n" 
                        + "  FROM    nims_fcn.infra_device gpon\n" 
                        + "  WHERE   gpon.device_id = a.device_id) device_code\n"
                        + "           FROM   nims_cn.ccn_subscription a,\n"
                        + "                   technical_connector b\n"
                        //+ "                   sub_adsl_ll c\n"
                        + "           WHERE       a.box_id = b.connector_id\n"
                        //+ "                   AND c.account = a.account_isdn\n"
                        //+ "                   AND c.status = 2\n"
                        + "                   AND b.status = 1\n"
                        + "                   AND b.infra_type = 'CCN'\n"
                        + "          UNION ALL\n"
                        + "          SELECT   a.account_isdn,\n"
                        + "                   b.staff_id,\n"
                        + "                   b.connector_code,\n"
                        + "                   b.station_id,\n"
                        //+ "                   c.cust_lat,\n"
                        //+ "                   c.cust_long,\n"
                        + "                   b.infra_type,\n"
                        + "                   p.port,\n"
                        + " (SELECT  device_code\n" 
                        + "  FROM   nims_fcn.infra_device gpon\n" 
                        + "  WHERE   gpon.device_id = a.device_id) device_code\n"
                        + "           FROM    nims_cn.ccn_subscription a,\n"
                        + "                   technical_connector b,\n"
                        + "                   nims_fcn.infra_device dd,\n"
                        + "                   nims_fcn.infra_port p\n"
                        //+ "                   sub_adsl_ll c\n"
                        + "           WHERE       a.odf_id = b.connector_id\n"
                        + "                   AND a.port_id = p.port_id\n"
                        + "                   AND p.device_id = dd.device_id\n"
                        + "                   AND b.status = 1\n"
                        + "                   AND a.odf_id IS NOT NULL\n"
                        + "                   AND b.infra_type = 'AON'\n"
                        + "          UNION ALL\n"
                        + "          SELECT   s.account_isdn,\n"
                        + "                   tc.staff_id,\n"
                        + "                   tc.connector_code,\n"
                        + "                   tc.station_id,\n"
                        //+ "                   y.cust_lat,\n"
                        //+ "                   y.cust_long,\n"
                        + "                   tc.infra_type\n,"
                        + "                   p.port,\n"
                        + " (SELECT   device_code\n" 
                        + "  FROM   nims_fcn.infra_device gpon\n" 
                        + "  WHERE   gpon.device_id = s.device_id) device_code\n"
                        + "           FROM   nims_fcn.infra_device a,\n"
                        + "                   nims_fcn.pon_device nt,\n"
                        + "                   nims_fcn.infra_device dd,\n"
                        + "                   nims_fcn.infra_port p,\n"
                        + "                   nims_cn.ccn_subscription s,\n"
                        + "                   technical_connector tc\n"
                        //+ "                   sub_adsl_ll y\n"
                        + "           WHERE       nt.parent_id = a.device_id\n"
                        + "                   AND dd.device_id = nt.device_id\n"
                        + "                   AND p.device_id = dd.device_id\n"
                        + "                   AND s.port_spliter_id = p.port_id\n"
                        + "                   AND tc.connector_code = a.device_code\n"
                        //+ "                   AND y.account = s.account_isdn\n"
                        + "                   AND tc.status = 1\n"
                        + "                   AND tc.infra_type = 'GPON'\n"
                        + "                   AND s.port_spliter_id IS NOT NULL\n"
                        + "                   AND s.department_id IS NOT NULL\n"
                        + "                   ) g INNER JOIN NIMS_FCN.infra_stations st ON g.station_id=st.station_id, sub_adsl_ll sub ")//staff sta, shop sh
                .append(" WHERE g.staff_id = :staffId ")
                //.append(" AND sta.shop_id = sh.shop_id ")
                .append(" AND g.account_isdn = sub.account ")
                //.append(" AND sub.sub_id = dep.sub_id ")
                .append(" AND sub.status = 2 ");
                //.append(" AND sh.shop_path LIKE :shopPath ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        //params.put("shopPath", "%" + shopPath + "%");
        params.put("staffId", staffID);
        //  params.put("staffId", "20441104"); // gán cứng để test 20441104
        //  staffCode = "VTC_PNP_TEAM5_LENGSOKTHY"; // gán cứng để test
        String url = "";
        if (account != null && !account.isEmpty() && !account.equals("?")) {
            sql.append(" AND g.account_isdn = :account");
            params.put("account", account);

        }
        if (infrasType != null && !infrasType.isEmpty() && !infrasType.equals("?")) {
            sql.append(" AND g.infra_type = :infra_type");
            params.put("infra_type", infrasType);
        }
        if (btsCode != null && !btsCode.isEmpty() && !btsCode.equals("?")) {
            sql.append(" AND st.station_code = :station_code");
            params.put("station_code", btsCode);
        }
        if (deviceCode != null && !deviceCode.isEmpty() && !deviceCode.equals("?")) {
            sql.append(" AND g.device_code = :deviceCode");
            params.put("deviceCode", deviceCode);
        }
        if (connectorCode != null && !connectorCode.isEmpty() && !connectorCode.equals("?")) {
            sql.append(" AND g.connector_code = :connectorCode");
            params.put("connectorCode", connectorCode);
        }
        //sql.append(" AND ROWNUM <= 50");
        if (infrasStatus != null && !infrasStatus.isEmpty() && !infrasStatus.equals("?")) {
            url = Constants.URL_NIM_ACCOUNT_INFRAS + "action=search&staff_code=" + staffCode + "&status=" + infrasStatus;
        }
        Query query = cmPosSession.createSQLQuery(sql.toString())
                .addScalar("account", Hibernate.STRING)
                .addScalar("infraType", Hibernate.STRING)
                .addScalar("stationCode", Hibernate.STRING)
                .addScalar("connectorCode", Hibernate.STRING)
                .addScalar("cLat", Hibernate.STRING)
                .addScalar("cLong", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(AccountInfra.class));
        buildParameter(query, params);

        List<AccountInfra> result = query.list();
        if (infrasStatus != null && !infrasStatus.isEmpty() && !infrasStatus.equals("?")) {
            String response = new ServiceSupplier().sendRequest(Constants.SERVICE_METHOD_GET, url, null, new WSRespone(), null);
            CustomerInfraResponse serviceResponse = new Gson().fromJson(response, CustomerInfraResponse.class);
            logger.error("getNismsListAccount:result=" + response);
            if (serviceResponse != null) {
                List<customerInfrasDetail> lstData = serviceResponse.getData();
                if (!lstData.isEmpty()) {
                    for (AccountInfra accountInfra : result) {
                        boolean isHave = false;
                        for (customerInfrasDetail customer : lstData) {
                            if (customer.getAccount().equals(accountInfra.getAccount())
                                    && customer.getStatus().equals(infrasStatus)) {
                                isHave = true;
                                break;
                            }
                        }
                        if (isHave) {
                            lst.add(accountInfra);
                        }
                    }
                }
            }
        } else {
            lst = result;
        }
        return lst.size();
    }

    public boolean checkAccountbyStaff(Session cmPosSession,
            String account, String staffId) {
        StringBuilder sql = new StringBuilder()
                .append("select * from v_get_list_accout where account_isdn=:account_isdn and staff_id=:staff_id");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("account_isdn", account);
        params.put("staff_id", staffId);
        Query query = cmPosSession.createSQLQuery(sql.toString());
        buildParameter(query, params);
        List<Object> result = query.list();
        return !result.isEmpty();
    }

    public List<BoxCode> getLstCabinet(Session cmPosSession,
            String stationCode) {
        Long stationID = getStationID(cmPosSession, stationCode);
        StringBuilder sql = new StringBuilder()
                .append("SELECT   cabinet.connector_code cabinetCode \n"
                        + "    FROM   nims_cn.ccn_subscription s,\n"
                        + "           nims_cn.ccn_connector cabinet,\n"
                        + "           nims_cn.ccn_connector box,\n"
                        + "           infra_device d,\n"
                        + "           nims_fcn.infra_port p,\n"
                        + "           infra_stations st\n"
                        + "   WHERE       box_id IS NOT NULL\n"
                        + "           AND s.cabinet_id = cabinet.connector_id(+)\n"
                        + "           AND s.box_id = box.connector_id\n"
                        + "           AND s.device_id = d.device_id(+)\n"
                        + "           AND s.port_id = p.port_id(+)\n"
                        + "           AND s.parent_subscription_id IS NULL\n"
                        + "           AND s.station_id = st.station_id(+)\n"
                        + "           AND s.station_id = :stationID \n"
                        + "           GROUP BY   cabinet.connector_code");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("stationID", stationID);
        Query query = cmPosSession.createSQLQuery(sql.toString())
                .addScalar("cabinetCode", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(BoxCode.class));
        buildParameter(query, params);
        List<BoxCode> result = query.list();
        return result;
    }

    public List<portOnBox> getLstCabinetPort(Session cmPosSession,
            String stationCode) {
        Long stationID = getStationID(cmPosSession, stationCode);
        StringBuilder sql = new StringBuilder()
                .append("SELECT   p.port port ,cabinet.connector_code cabinetCode \n"
                        + "    FROM   nims_cn.ccn_subscription s,\n"
                        + "           nims_cn.ccn_connector cabinet,\n"
                        + "           nims_cn.ccn_connector box,\n"
                        + "           infra_device d,\n"
                        + "           nims_fcn.infra_port p,\n"
                        + "           infra_stations st\n"
                        + "   WHERE  box_id IS NOT NULL\n"
                        + "           AND s.cabinet_id = cabinet.connector_id(+)\n"
                        + "           AND s.box_id = box.connector_id\n"
                        + "           AND s.device_id = d.device_id(+)\n"
                        + "           AND s.port_id = p.port_id(+)\n"
                        + "           AND s.parent_subscription_id IS NULL\n"
                        + "           AND s.station_id = st.station_id(+)\n"
                        + "           AND s.station_id = :stationID "
                        + "           AND cabinet.connector_code  IS NOT NULL ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("stationID", stationID);
        Query query = cmPosSession.createSQLQuery(sql.toString())
                .addScalar("port", Hibernate.STRING)
                .addScalar("cabinetCode", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(portOnBox.class));
        buildParameter(query, params);
        List<portOnBox> result = query.list();
        return result;
    }

    public Long getStationID(Session cmPosSession,
            String stationCode) {
        Long stationID = 0L;
        StringBuilder sql = new StringBuilder()
                .append(" select station_id cabinetCode from nims_fcn. infra_stations where STATION_CODE = :STATION_CODE");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("STATION_CODE", stationCode);
        Query query = cmPosSession.createSQLQuery(sql.toString())
                .addScalar("cabinetCode", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(BoxCode.class));
        buildParameter(query, params);
        List<BoxCode> result = query.list();
        if (!result.isEmpty()) {
            stationID = Long.parseLong(result.get(0).getCabinetCode());
        }
        return stationID;
    }

    public List<Object> getInfraStructureOfAccount(Session nims, boolean isGpon, String stationCode) {
        String sql;
        if (isGpon) {
            sql = "select b.station_code, "
                    + "       b.station_id, "
                    + "       a.device_id dnId, "
                    + "       a.device_code dnCode, "
                    + "       d.device_code splCode, "
                    + "       d.device_id splId, "
                    + "       p.port as spliPort, "
                    + "       account_isdn,p.port_id as port_id "
                    + "  from nims_fcn.infra_device a "
                    + "  join nims_fcn.pon_container d "
                    + "    on a.device_id = d.id "
                    + "  join nims_fcn.pon_cat_node_model c "
                    + "    on d.node_model_id = c.id "
                    + "   and c.type = 3 "
                    + "  join nims_fcn.infra_stations b "
                    + "    on b.station_id = a.station_id "
                    + "  JOIN NIMS_FCN.pon_device nt "
                    + "    ON nt.parent_id = a.device_id "
                    + "  JOIN NIMS_FCN.infra_device d "
                    + "    ON d.device_id = nt.device_id "
                    + "  LEFT JOIN NIMS_FCN.infra_port p "
                    + "    ON p.device_id = d.device_id "
                    + "  LEFT JOIN nims_cn.ccn_subscription s "
                    + "    ON s.port_spliter_id = p.port_id "
                    + " WHERE s.port_spliter_id IS NOT NULL "
                    + " and station_code = ?";
        } else {
            sql = "select s.account_isdn, "
                    + "       d.device_code, "
                    + "       p.port, "
                    + "       s.mdf_a, "
                    + "       s.mdf_b, "
                    + "       s.plate_b, "
                    + "       null cabinet_code, "
                    + "       null connector_code, "
                    + "       decode(s.box_id, null, odf.odf_code, '') odf_code, "
                    + "       s.coupler_no,"
                    + "       st.station_code "
                    + "  from nims_cn.ccn_subscription s, "
                    + "       NIMS_CN.odn_odf          odf, "
                    + "       infra_device             d, "
                    + "       infra_port               p,"
                    + "       infra_stations st "
                    + " where s.odf_id is not null "
                    + "   and s.odf_id = odf.odf_id "
                    + "   and s.device_id = d.device_id(+) "
                    + "   and s.port_id = p.port_id(+) "
                    + "   and s.parent_subscription_id is null "
                    + "   and s.station_id = st.station_id (+) "
                    + "   and account_isdn = ? "
                    + "union all "
                    + "select s.account_isdn, "
                    + "       d.device_code, "
                    + "       p.port, "
                    + "       s.mdf_a, "
                    + "       s.mdf_b, "
                    + "       s.plate_b, "
                    + "       cabinet.connector_code cabinet_code, "
                    + "       box.connector_code     connector_code, "
                    + "       null                   odf_code, "
                    + "       s.coupler_no, "
                    + "       st.station_code "
                    + "  from nims_cn.ccn_subscription s, "
                    + "       NIMS_CN.ccn_connector    cabinet, "
                    + "       NIMS_CN.ccn_connector    box, "
                    + "       infra_device             d, "
                    + "       infra_port               p,"
                    + "       infra_stations st "
                    + " where box_id is not null "
                    + "   and s.cabinet_id = cabinet.connector_id(+) "
                    + "   and s.box_id = box.connector_id "
                    + "   and s.device_id = d.device_id(+) "
                    + "   and s.port_id = p.port_id(+) "
                    + "   and s.parent_subscription_id is null "
                    + "   and s.station_id = st.station_id (+) "
                    + "   and account_isdn = ? and station_code = ?";
        }
        Query query = nims.createSQLQuery(sql);
        query.setParameter(0, stationCode);
        List data = query.list();
        return data;
    }

    public List<LenghtCable> getLenghCap(Session cmPosSession,
            String account) {
        StringBuilder sql = new StringBuilder()
                .append(" select s.SUBSCRITION_CABLE_LENGTH lenghtCable from NIMS_CN.CCN_SUBSCRIPTION s "
                        + " where s.ACCOUNT_ISDN=:ACCOUNT_ISDN");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("ACCOUNT_ISDN", account);
        Query query = cmPosSession.createSQLQuery(sql.toString())
                .addScalar("lenghtCable", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(LenghtCable.class));
        buildParameter(query, params);
        List<LenghtCable> result = query.list();
        return result;
    }

    public Long getOdfID(Session session, String OdfCode) {
        StringBuilder sql = new StringBuilder().append("SELECT  odf_id \n"
                + "  FROM   nims_cn.odn_odf\n"
                + " WHERE   odf_code = :odf_code");
        Query query = session.createSQLQuery(sql.toString());
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("odf_code", OdfCode);
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public LocationODF getXYODF_AON(Session cmPosSession,
            String odfCode, boolean isAON, String connectorCode, String accountIsdn) {
        LocationODF locationODF = null;
        StringBuilder sql = new StringBuilder();
        HashMap<String, Object> params = new HashMap<String, Object>();
        if (isAON) {
            sql.append("SELECT   a.geometry.sdo_point.x AS x,\n"
                    + "          a.geometry.sdo_point.y AS y\n"
                    + "  FROM   nims_cn.odn_odf a where odf_code=:odfCode");

            params.put("odfCode", odfCode);
        } else {
            sql.append("SELECT   c.cust_lat x,\n"
                    + "         c.cust_long y\n"
                    + "  FROM   nims_cn.ccn_subscription a, technical_connector b, sub_adsl_ll c\n"
                    + " WHERE       a.box_id = b.connector_id\n"
                    + "         AND c.account = a.account_isdn\n"
                    + "         AND c.status = 2\n"
                    + "         AND b.status = 1\n"
                    + "         AND b.infra_type = 'CCN'\n"
                    + "         AND b.connector_code = :connector_code \n"
                    + "         AND a.account_isdn= :account_isdn\n");
            params.put("connector_code", connectorCode);
            params.put("account_isdn", accountIsdn);
        }
        Query query = cmPosSession.createSQLQuery(sql.toString())
                .addScalar("x", Hibernate.DOUBLE)
                .addScalar("y", Hibernate.DOUBLE)
                .setResultTransformer(Transformers.aliasToBean(LocationODF.class));
        buildParameter(query, params);
        List<LocationODF> result = query.list();
        if (result != null && result.size() > 0) {
            locationODF = result.get(0);
        }
        return locationODF;
    }

    public LocationODF getLocationAccount(Session cmPosSession, String accountIsdn) {
        LocationODF locationODF = null;
        StringBuilder sql = new StringBuilder();
        HashMap<String, Object> params = new HashMap<String, Object>();
        sql.append("select cust_lat x,cust_long y from sub_adsl_ll where account=:account and status=2");

        params.put("account", accountIsdn);
        Query query = cmPosSession.createSQLQuery(sql.toString())
                .addScalar("x", Hibernate.DOUBLE)
                .addScalar("y", Hibernate.DOUBLE)
                .setResultTransformer(Transformers.aliasToBean(LocationODF.class));
        buildParameter(query, params);
        List<LocationODF> result = query.list();
        if (result != null && result.size() > 0) {
            locationODF = result.get(0);
        }
        return locationODF;
    }

    public LocationSN getLocationSNCode(Session cmNimsSession, String deviceCode) {
        LocationSN locationODF = null;
        StringBuilder sql = new StringBuilder();
        HashMap<String, Object> params = new HashMap<String, Object>();
        sql.append("SELECT   d.geometry.sdo_point.x AS x,\n"
                + "         d.geometry.sdo_point.y AS y "
                + " FROM   nims_fcn.pon_container a,\n"
                + "         nims_fcn.pon_cat_node_model b,\n"
                + "         nims_fcn.pon_device c,\n"
                + "         nims_fcn.pon_device_geo d,\n"
                + "         nims_fcn.infra_device idv\n"
                + " WHERE       a.id = c.device_id\n"
                + "         AND c.device_id = d.id\n"
                + "         AND c.device_id = idv.device_id\n"
                + "         AND a.node_model_id = b.id\n"
                + "         AND b.TYPE = 3\n"
                + "         AND device_code = :device_code");
        logger.error("getLocationSNCode: " + sql.toString() + " device_code:" + deviceCode);
        params.put("device_code", deviceCode);
        Query query = cmNimsSession.createSQLQuery(sql.toString())
                .addScalar("x", Hibernate.DOUBLE)
                .addScalar("y", Hibernate.DOUBLE)
                .setResultTransformer(Transformers.aliasToBean(LocationSN.class));
        buildParameter(query, params);
        List<LocationSN> result = query.list();
        if (result != null && result.size() > 0) {
            locationODF = result.get(0);
            logger.error("getLocationSNCode: X:" + locationODF.getX());
        }
        return locationODF;
    }

    public String getPortSpliiter(Session cmNimsSession, String accoutIsdn) {
        String portSplitter = null;
        StringBuilder sql = new StringBuilder();
        HashMap<String, Object> params = new HashMap<String, Object>();
        sql.append("SELECT    p.port AS spliport"
                + "  FROM                               nims_fcn.infra_device a\n"
                + "                                 JOIN\n"
                + "                                     nims_fcn.pon_container d\n"
                + "                                 ON a.device_id = d.id\n"
                + "                             JOIN\n"
                + "                                 nims_fcn.pon_cat_node_model c\n"
                + "                             ON d.node_model_id = c.id AND c.TYPE = 3\n"
                + "                         JOIN\n"
                + "                             nims_fcn.infra_stations b\n"
                + "                         ON b.station_id = a.station_id\n"
                + "                     JOIN\n"
                + "                         nims_fcn.pon_device nt\n"
                + "                     ON nt.parent_id = a.device_id\n"
                + "                 JOIN\n"
                + "                     nims_fcn.infra_device d\n"
                + "                 ON d.device_id = nt.device_id\n"
                + "             LEFT JOIN\n"
                + "                 nims_fcn.infra_port p\n"
                + "             ON p.device_id = d.device_id\n"
                + "         LEFT JOIN\n"
                + "             nims_cn.ccn_subscription s\n"
                + "         ON s.port_spliter_id = p.port_id\n"
                + " WHERE   s.port_spliter_id IS NOT NULL AND s.account_isdn = :account_isdn");
        logger.error("getPortSpliiter: " + sql.toString() + " account_isdn:" + accoutIsdn);
        params.put("account_isdn", accoutIsdn);
        Query query = cmNimsSession.createSQLQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        if (obj != null) {
            portSplitter = ConvertUtils.toStringValue(obj);
        }
        return portSplitter;
    }
    public String getPortSpliiterID(Session cmNimsSession, String accoutIsdn) {
        String portSplitter = null;
        StringBuilder sql = new StringBuilder();
        HashMap<String, Object> params = new HashMap<String, Object>();
        sql.append("SELECT    p.port_id AS spliportID"
                + "  FROM                               nims_fcn.infra_device a\n"
                + "                                 JOIN\n"
                + "                                     nims_fcn.pon_container d\n"
                + "                                 ON a.device_id = d.id\n"
                + "                             JOIN\n"
                + "                                 nims_fcn.pon_cat_node_model c\n"
                + "                             ON d.node_model_id = c.id AND c.TYPE = 3\n"
                + "                         JOIN\n"
                + "                             nims_fcn.infra_stations b\n"
                + "                         ON b.station_id = a.station_id\n"
                + "                     JOIN\n"
                + "                         nims_fcn.pon_device nt\n"
                + "                     ON nt.parent_id = a.device_id\n"
                + "                 JOIN\n"
                + "                     nims_fcn.infra_device d\n"
                + "                 ON d.device_id = nt.device_id\n"
                + "             LEFT JOIN\n"
                + "                 nims_fcn.infra_port p\n"
                + "             ON p.device_id = d.device_id\n"
                + "         LEFT JOIN\n"
                + "             nims_cn.ccn_subscription s\n"
                + "         ON s.port_spliter_id = p.port_id\n"
                + " WHERE   s.port_spliter_id IS NOT NULL AND s.account_isdn = :account_isdn");
        logger.error("getPortSpliiter: " + sql.toString() + " account_isdn:" + accoutIsdn);
        params.put("account_isdn", accoutIsdn);
        Query query = cmNimsSession.createSQLQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        if (obj != null) {
            portSplitter = ConvertUtils.toStringValue(obj);
        }
        return portSplitter;
    }
    
    public void updateSub_ADSL_ll(Session cmPosSession, Double x, Double y, String account) {
        try {
            cmPosSession.beginTransaction();
            StringBuilder sql = new StringBuilder(" UPDATE  sub_adsl_ll SET cust_lat = ?, cust_long = ? WHERE   account = ? and status=2");
            Query query = cmPosSession.createSQLQuery(sql.toString());
            query.setParameter(0, x);
            query.setParameter(1, y);
            query.setParameter(2, account);
            query.executeUpdate();
            cmPosSession.beginTransaction().commit();
        } catch (Exception e) {
        } finally {

        }

    }

    public void insertFrasUpdateLog(Session cmPosSession, FrasUpdateLog frasUpdateLog) {
        try {
            cmPosSession.beginTransaction();
            Long frasUpdateSEQ = getSequence(cmPosSession, "fras_update_seq");
            StringBuilder sql = new StringBuilder("insert INTO fras_update_log (id,account,sub_id,fras_type,"
                    + " staff_id,\n"
                    + " staff_name,insert_date,description,\n"
                    + " station_id,\n"
                    + " station_code,status,error_code,device_code,old_value,value_type,\n"
                    + " new_value) values (?,?,?,?,?,?,SYSDATE,?,?,?,?,?,?,?,?,?)");
            Query query = cmPosSession.createSQLQuery(sql.toString());
            query.setParameter(0, frasUpdateSEQ);
            query.setParameter(1, frasUpdateLog.getAccount());
            query.setParameter(2, frasUpdateLog.getSubID());
            query.setParameter(3, frasUpdateLog.getFrasType());
            query.setParameter(4, frasUpdateLog.getStaffID());
            query.setParameter(5, frasUpdateLog.getStaffName());
            query.setParameter(6, frasUpdateLog.getDescription());
            query.setParameter(7, frasUpdateLog.getStationID());
            query.setParameter(8, frasUpdateLog.getStationCode());
            query.setParameter(9, frasUpdateLog.getStatus());
            query.setParameter(10, frasUpdateLog.getErrorCode());
            query.setParameter(11, frasUpdateLog.getDeviceCode());
            query.setParameter(12, frasUpdateLog.getOldValue());
            query.setParameter(13, frasUpdateLog.getValueType());
            query.setParameter(14, frasUpdateLog.getNewValue());
            query.executeUpdate();
            cmPosSession.beginTransaction().commit();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }
    
    /**
     * @author duyetdk
     * @des lay ds connector gan theo tram
     * @param nims
     * @param stationId
     * @param stationCode
     * @return 
     */
    public List<TechInfo> getAllConnectorOfStation(Session nims, Long stationId, String stationCode) {
        String sql = "SELECT a.device_id id, a.device_code code,'GPON' as infraType "
                + "  FROM nims_fcn.infra_device       a, "
                + "       nims_fcn.pon_container      b, "
                + "       nims_fcn.pon_cat_node_model c, "
                + "       nims_fcn.infra_stations     d "
                + " WHERE a.device_id = b.id "
                + "   AND b.node_model_id = c.id "
                + "   AND c.type = 3 "
                + "   AND d.station_id = a.station_id "
                + "   AND d.station_code = ? "
                + " UNION ALL "
                + " SELECT a.connector_id id, a.connector_code code,  'CCN' as infraType "
                + "  FROM nims_cn.ccn_connector   a, "
                + "       nims_fcn.infra_stations b, "
                + "       nims_cn.ccn_pdd         c "
                + " WHERE a.connector_type_id in ('HC', 'HCG') "
                + "   AND a.pdd_id = c.pdd_id "
                + "   AND c.station_id = b.station_id "
                + "   AND b.station_code = ? "
                + " UNION ALL "
                + " SELECT a.odf_id  id, a.odf_code code,  'AON' as infraType "
                + "  FROM nims_cn.odn_odf a, nims_fcn.infra_stations b "
                + " WHERE a.root_station_id = b.station_id "
                + "   AND b.station_code = ? ";
        Query query = nims.createSQLQuery(sql)
                .addScalar("id", Hibernate.LONG)
                .addScalar("code", Hibernate.STRING)
                .addScalar("infraType", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(TechInfo.class));
        query.setParameter(0, stationCode);
        query.setParameter(1, stationCode);
        query.setParameter(2, stationCode);
        return query.list();
    }
}
