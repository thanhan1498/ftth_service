package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.ActionAudit;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ActionAuditSupplier extends BaseSupplier {

    public ActionAuditSupplier() {
        logger = Logger.getLogger(ActionAuditSupplier.class);
    }

    public ActionAudit insert(Session cmPosSession, Date issueDatetime, String actionCode, Long reasonId, String shopCode, String userName, String pkType, Long pkId, String ip, String description) {
        Long actionAuditId = getSequence(cmPosSession, Constants.ACTION_AUDIT_SEQ);
        ActionAudit audit = new ActionAudit(actionAuditId, issueDatetime, actionCode, reasonId, shopCode, userName, pkType, pkId, ip, description, null);
        cmPosSession.save(audit);
        //cmPosSession.flush();
        return audit;
    }
}
