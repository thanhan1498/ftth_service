package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.ActionDetail;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ActionDetailSupplier extends BaseSupplier {

    public ActionDetailSupplier() {
        logger = Logger.getLogger(ActionDetailSupplier.class);
    }

    public ActionDetail insert(Session cmPosSession, Long actionAuditId, String tableName, Long rowId, String colName, String oldValue, String newValue, Date issueDateTime) {
        Long actionDetailId = getSequence(cmPosSession, Constants.SEQ_ACTION_DETAIL);
        ActionDetail detail = new ActionDetail(actionDetailId, actionAuditId, tableName, rowId, colName, oldValue, newValue, issueDateTime);
        cmPosSession.save(detail);
        //cmPosSession.flush();
        return detail;
    }
}
