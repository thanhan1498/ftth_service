package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.ApDomain;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ApDomainSupplier extends BaseSupplier {

    public ApDomainSupplier() {
        logger = Logger.getLogger(ApDomainSupplier.class);
    }

    public List<ApDomain> findByType(Session cmPosSession, String type, Long status) {
        StringBuilder sql = new StringBuilder().append(" from ApDomain where type = :type ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("type", type);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<ApDomain> result = query.list();
        return result;
    }

    public List<ApDomain> findByTypeCode(Session cmPosSession, String type, String code, Long status) {
        StringBuilder sql = new StringBuilder()
                .append(" from ApDomain where type = :type and code = :code ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("type", type);
        params.put("code", code);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<ApDomain> result = query.list();
        return result;
    }

    public List<ApDomain> findByTypeGroup(Session cmPosSession, String type, String groupCode, Long status) {
        StringBuilder sql = new StringBuilder()
                .append(" from ApDomain where type = :type and groupCode = :groupCode ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("type", type);
        params.put("groupCode", groupCode);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<ApDomain> result = query.list();
        return result;
    }
}
