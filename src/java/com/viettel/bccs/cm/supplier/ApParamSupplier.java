package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.util.Constants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class ApParamSupplier extends BaseSupplier {

    public ApParamSupplier() {
        logger = Logger.getLogger(ApParamSupplier.class);
    }

    public List<ApParam> findByType(Session cmPosSession, String paramType, Long status) {
        StringBuilder sql = new StringBuilder().append(" from ApParam where paramType = :paramType ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("paramType", paramType);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
//        sql.append(" order by paramName ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<ApParam> result = query.list();
        return result;
    }

    public List<ApParam> findByTypeCode(Session cmPosSession, String paramType, String paramCode, Long status) {
        StringBuilder sql = new StringBuilder()
                .append(" from ApParam where paramType = :paramType and paramCode = :paramCode ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("paramType", paramType);
        params.put("paramCode", paramCode);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by paramName ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<ApParam> result = query.list();
        return result;
    }

    /**
     * @author : duyetdk
     * @des: find param value
     * @since 15-03-2018
     */
    public List<ApParam> find(Session cmPosSession, String paramCode, boolean suggest) {
        StringBuilder sql = new StringBuilder()
                .append(" from ApParam where 1 = 1 and paramType = :reason  ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        if (paramCode != null && !paramCode.trim().isEmpty()) {
            sql.append(" and paramCode = :paramCode ");
            params.put("paramCode", paramCode);
        }
        params.put("reason", Constants.REASON_CONFIG_IMT);
        if (suggest) {
            sql.append(" or paramType = :suggest ");
            params.put("suggest", Constants.SUGGEST_CONFIG_IMT);
        }
        sql.append(" and status = :status ");
        params.put("status", Constants.STATUS_USE);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<ApParam> result = query.list();
        return result;
    }

    public ApParam findByTypeValue(Session cmPosSession, String paramType, String paramValue) {
        StringBuilder sql = new StringBuilder()
                .append(" from ApParam where paramType = :paramType and paramValue = :paramValue and status = :status");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("paramType", paramType);
        params.put("paramValue", paramValue);
        params.put("status", Constants.STATUS_USE);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<ApParam> result = query.list();
        if (result != null && !result.isEmpty()) {
            return result.get(0);
        }
        return null;
    }

    public List<ApParam> findByTypeMerchant(Session merchantSession, String paramType, String paramCode, String paramName) {
        StringBuilder sql = new StringBuilder().append("select param_value paramValue, param_type paramType, param_code paramCode, param_name paramName from Ap_Param where  status = 1");
        HashMap<String, Object> params = new HashMap<String, Object>();
        if (paramType != null) {
            sql.append(" and param_Type = :paramType ");
            params.put("paramType", paramType);
        }
        if (paramCode != null) {
            sql.append(" and param_Code = :paramCode ");
            params.put("paramCode", paramCode);
        }
        if (paramName != null) {
            sql.append(" and param_name = :paramName ");
            params.put("paramName", paramName);
        }
//        sql.append(" order by paramName ");
        Query query = merchantSession.createSQLQuery(sql.toString())
                .addScalar("paramValue", Hibernate.STRING)
                .addScalar("paramType", Hibernate.STRING)
                .addScalar("paramCode", Hibernate.STRING)
                .addScalar("paramName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ApParam.class));
        buildParameter(query, params);
        List<ApParam> result = query.list();
        return result;
    }

    public void updateTimeSendMessageFinance(Session merchantSession, String paramType, String dateTime) {
        String sql = "update Ap_Param set param_value = ? where param_type = ? and status = 1";
        Query query = merchantSession.createSQLQuery(sql);
        query.setParameter(0, dateTime);
        query.setParameter(1, paramType);
        query.executeUpdate();
        merchantSession.flush();
    }

    public List<ApParam> findByEachProperty(Session cmPosSession, String paramType, String paramCode, String paramName, String paramValue) {
        StringBuilder sql = new StringBuilder()
                .append(" from ApParam where status = 1  ");

        HashMap<String, Object> params = new HashMap<String, Object>();
        if (paramType != null) {
            sql.append(" and paramType = :paramType  ");
            params.put("paramType", paramType);
        }
        if (paramCode != null) {
            sql.append(" and paramCode = :paramCode  ");
            params.put("paramCode", paramCode);
        }
        if (paramName != null) {
            sql.append(" and paramName = :paramName  ");
            params.put("paramName", paramName);
        }
        if (paramValue != null) {
            sql.append(" and paramValue like :paramValue  ");
            params.put("paramValue", "%" + paramValue + "%");
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        return query.list();
    }

    public List<ApParam> findParam(Session session, String paramType, String paramCode, String paramName) {
        StringBuilder sql = new StringBuilder().append("select param_code paramCode, param_name paramName from Ap_Param where status = 1");
        HashMap<String, Object> params = new HashMap<String, Object>();
        if (paramType != null) {
            sql.append(" and param_Type = :paramType ");
            params.put("paramType", paramType);
        }
        if (paramCode != null) {
            sql.append(" and param_Code = :paramCode ");
            params.put("paramCode", paramCode);
        }
        if (paramName != null) {
            sql.append(" and param_name = :paramName ");
            params.put("paramName", paramName);
        }
        sql.append(" order by paramName ");
        Query query = session.createSQLQuery(sql.toString())
                .addScalar("paramName", Hibernate.STRING)
                .addScalar("paramCode", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ApParam.class));
        buildParameter(query, params);
        List<ApParam> result = query.list();
        return result;
    }

    //phuonghc 06092020
    public Map<String, String> getUserManualConfig(Session sessionCmPos) {
        Map<String, String> userManualMap = new HashMap<String, String>();
        try {
            String sql = " From ApParam Where status = ? and paramCode = ?";
            Query query = sessionCmPos.createQuery(sql);
            query.setParameter(0, Constants.STATUS_USE);
            query.setParameter(1, Constants.USER_MANUAL_CONFIG);
            List lstResult = query.list();
            if (lstResult != null && lstResult.size() > 0) {
                for (int i = 0; i < lstResult.size(); i++) {
                    ApParam apParam = (ApParam) lstResult.get(i);
                    if (apParam != null && apParam.getParamType() != null && !"".equals(apParam.getParamType().trim())) {
                        userManualMap.put(apParam.getParamType(), apParam.getParamValue());
                    }
                }
            }
        } catch (Exception e) {
            logger.error("### An error occurred while get appram with USER_MANUAL_CONFIG" + e.getMessage());
        } finally {
            return userManualMap;
        }
    }
}
