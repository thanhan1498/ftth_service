package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.Area;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class AreaSupplier extends BaseSupplier {

    public AreaSupplier() {
        logger = Logger.getLogger(AreaSupplier.class);
    }

    public List<Area> findById(Session cmPosSession, String areaCode, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Area where areaCode = :areaCode ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("areaCode", areaCode);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Area> result = query.list();
        return result;
    }

    public List<Area> findProvinces(Session cmPosSession, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Area where province is not null and district is null ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by province, name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Area> result = query.list();
        return result;
    }

    public List<Area> findDistricts(Session cmPosSession, String province, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Area where province = :province and district is not null and precinct is null ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("province", province);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by district, name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Area> result = query.list();
        return result;
    }

    public List<Area> findPrecincts(Session cmPosSession, String province, String district, Long status) {
        StringBuilder sql = new StringBuilder()
                .append(" from Area where province = :province and district = :district and precinct is not null and streetBlock is null ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("province", province);
        params.put("district", district);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by precinct, name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Area> result = query.list();
        return result;
    }
}
