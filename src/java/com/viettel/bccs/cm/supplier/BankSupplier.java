package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.Bank;
import com.viettel.bccs.cm.util.ConvertUtils;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class BankSupplier extends BaseSupplier {

    public BankSupplier() {
        logger = Logger.getLogger(BankSupplier.class);
    }

    public void buildQuery(StringBuilder sql, HashMap params, String bankCode, String name, Long status) {
        if (sql == null || params == null) {
            return;
        }
        if (bankCode != null) {
            bankCode = bankCode.trim();
            if (!bankCode.isEmpty()) {
                sql.append(" and ").append(createLike(" bankCode ", "bankCode"));
                params.put("bankCode", formatLike(bankCode));
            }
        }
        if (name != null) {
            name = name.trim();
            if (!name.isEmpty()) {
                sql.append(" and ").append(createLike(" name ", "name"));
                params.put("name", formatLike(name));
            }
        }
        sql.append(" and status = :status ");
        params.put("status", status);
    }

    public Long count(Session cmPosSession, String bankCode, String name, Long status) {
        StringBuilder sql = new StringBuilder().append(" select count(*) from Bank where 1 = 1 ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        buildQuery(sql, params, bankCode, name, status);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public List<Bank> find(Session cmPosSession, String bankCode, String name, Long status, Integer start, Integer max) {
        StringBuilder sql = new StringBuilder().append(" from Bank where 1 = 1 ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        buildQuery(sql, params, bankCode, name, status);
        sql.append(" order by name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        if (start != null) {
            query.setFirstResult(start);
        }
        if (max != null) {
            query.setMaxResults(max);
        }
        List<Bank> result = query.list();
        return result;
    }
}
