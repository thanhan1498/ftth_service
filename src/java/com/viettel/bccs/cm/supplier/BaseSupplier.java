package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.bccs.cm.util.DateTimeUtils;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * @author vanghv1
 */
public class BaseSupplier {

    protected Logger logger = Logger.getLogger(BaseSupplier.class);

    public Long getSequence(Session session, String seq) {
        StringBuilder sql = new StringBuilder().append(" select ").append(seq.toLowerCase()).append(".nextval from dual ");
        Query query = session.createSQLQuery(sql.toString());
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public Date getSysDateTime(Session session) throws ParseException {
        String strSysDateTime = getStrSysDateTime(session);
        if (strSysDateTime != null && !strSysDateTime.isEmpty()) {
            return DateTimeUtils.toDateddMMyyyyHHmmss(strSysDateTime);
        }
        return null;
    }

    public String getStrSysDateTime(Session session) {
        StringBuilder sql = new StringBuilder().append(" select to_char(sysdate, '").append(DateTimeUtils.DATE_PATTERN_ddMMyyyyHH24miss).append("') from dual ");
        Query query = session.createSQLQuery(sql.toString());
        Object obj = query.uniqueResult();
        return ConvertUtils.toStringValue(obj);
    }

    public String createLike(String field) {
        return " (lower(" + field + ") like lower(?) escape '\\') ";
    }

    public String createLike(String field, String paramName) {
        return " (lower(" + field + ") like lower( :" + paramName + " ) escape '\\') ";
    }

    public String formatLike(String str) {
        return "%" + StringEscapeUtils.escapeSql(str.toLowerCase()).replaceAll("_", "\\\\_").replaceAll("%", "\\\\%") + "%";
    }

    public void buildParameter(Query query, List params) {
        if (query != null && params != null && !params.isEmpty()) {
            int n = params.size();
            for (int i = 0; i < n; i++) {
                query.setParameter(i, params.get(i));
            }
        }
    }

    public void buildParameter(Query query, HashMap<String, Object> params) {
        if (query != null && params != null && !params.isEmpty()) {
            for (String key : params.keySet()) {
                query.setParameter(key, params.get(key));
            }
        }
    }

    public void rollBackTransactions(Session... sessions) {
        if (sessions != null && sessions.length > 0) {
            for (Session session : sessions) {
                if (session != null) {
                    Transaction transaction = session.getTransaction();
                    if (transaction != null) {
                        transaction.rollback();
                    }
                }
            }
        }
    }

    public void commitTransactions(Session... sessions) {
        if (sessions != null && sessions.length > 0) {
            for (Session session : sessions) {
                if (session != null && session.isOpen()) {
                    Transaction transaction = session.getTransaction();
                    if (transaction != null && transaction.isActive()) {
                        transaction.commit();
                    }
                }
            }
        }
    }

    public void flushSessions(Session... sessions) {
        if (sessions != null && sessions.length > 0) {
            for (Session session : sessions) {
                if (session != null) {
                    session.flush();
                }
            }
        }
    }

    public void beginTransactions(Session... sessions) {
        if (sessions != null && sessions.length > 0) {
            for (Session session : sessions) {
                if (session != null) {
                    session.beginTransaction();
                }
            }
        }
    }

    public void clearSessions(Session... sessions) {
        if (sessions != null && sessions.length > 0) {
            for (Session session : sessions) {
                if (session != null) {
                    session.clear();
                }
            }
        }
    }

    public void closeSessions(Session... sessions) {
        if (sessions != null && sessions.length > 0) {
            for (Session session : sessions) {
                if (session != null && session.isOpen()) {
                    session.close();
                }
            }
        }
    }

    /*
     * @author vanghv1
     * @since 01/01/2014
     * return true if a != b
     */
    public boolean isNotEquals(Object a, Object b) {
        if (a == null && b == null) {
            return false;
        }
        if (a != null && b != null) {
            Class cA = a.getClass();
            Class cB = b.getClass();
            if (cA.equals(cB)) {
                if (a.equals(b)) {
                    return false;
                }
                if (cA.equals(String.class)) {
                    String strA = String.valueOf(a);
                    String strB = String.valueOf(b);
                    if (strA.trim().equals(strB.trim())) {
                        return false;
                    }
                }
            }
            if (cA.equals(java.sql.Timestamp.class) && cB.equals(java.util.Date.class)) {
                if (((java.sql.Timestamp) a).getTime() == ((java.util.Date) b).getTime()) {
                    return false;
                }
            }
            if (cA.equals(java.util.Date.class) && cB.equals(java.sql.Timestamp.class)) {
                if (((java.util.Date) a).getTime() == ((java.sql.Timestamp) b).getTime()) {
                    return false;
                }
            }
        }
        if (a != null && b == null && a.getClass().equals(String.class)) {
            if (String.valueOf(a).trim().isEmpty()) {
                return false;
            }
        }
        if (b != null && a == null && b.getClass().equals(String.class)) {
            if (String.valueOf(b).trim().isEmpty()) {
                return false;
            }
        }
        return true;
    }

    /**
     * @author cuongdm
     * @since 08/05/2018
     * @des get sysdate from db
     * @param session
     * @return
     * @throws ParseException 
     */
    public static Date getSysDateDb(Session session) throws ParseException {
        String strQuery = "SELECT sysdate as system_datetime FROM Dual";
        SQLQuery queryObject = session.createSQLQuery(strQuery);
        queryObject.addScalar("system_datetime", Hibernate.TIMESTAMP);
        Date sysdate = (Date) queryObject.uniqueResult();
        return sysdate;
    }
}
