package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.BusTypeIdRequire;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class BusTypeIdRequireSupplier extends BaseSupplier {

    public BusTypeIdRequireSupplier() {
        logger = Logger.getLogger(BusTypeIdRequireSupplier.class);
    }

    public List<BusTypeIdRequire> findByBusType(Session cmPosSession, String busType, Long status) {
        StringBuilder sql = new StringBuilder().append(" from BusTypeIdRequire where busType = :busType ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("busType", busType);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<BusTypeIdRequire> result = query.list();
        return result;
    }
}
