package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.BusType;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class BusTypeSupplier extends BaseSupplier {

    public BusTypeSupplier() {
        logger = Logger.getLogger(BusTypeSupplier.class);
    }

    public List<BusType> findById(Session cmPosSession, String busType, Long status) {
        StringBuilder sql = new StringBuilder().append(" from BusType where busType = :busType ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("busType", busType);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<BusType> result = query.list();
        return result;
    }

    public List<BusType> findByStatus(Session cmPosSession, Long status) {
        StringBuilder sql = new StringBuilder().append(" from BusType where status = :status order by name ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("status", status);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<BusType> result = query.list();
        return result;
    }

    public List<BusType> findByIdPre(Session cmPreSession, String busType, Long status) {
        StringBuilder sql = new StringBuilder().append("select name from Bus_Type where bus_Type = :busType ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("busType", busType);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by name ");
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("name", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(BusType.class));
        buildParameter(query, params);
        List<BusType> result = query.list();
        return result;
    }
    public List<BusType> findByStatusPre(Session cmPreSession, Long status) {
        StringBuilder sql = new StringBuilder().append("select bus_type busType, name from Bus_Type where status = :status order by name ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("status", status);
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("busType", Hibernate.STRING)
                .addScalar("name", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(BusType.class));
        buildParameter(query, params);
        List<BusType> result = query.list();
        return result;
    }
}
