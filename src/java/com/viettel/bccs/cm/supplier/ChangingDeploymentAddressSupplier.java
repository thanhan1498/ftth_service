/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.bussiness.ApParamBussiness;
import com.viettel.bccs.cm.bussiness.ContractBussiness;
import com.viettel.bccs.cm.bussiness.CustomerBussiness;
import com.viettel.bccs.cm.dao.DeployRequirementDAO;
import com.viettel.bccs.cm.dao.MappingDAO;
import com.viettel.bccs.cm.dao.ReasonDAO;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.dao.SubDeploymentDAO;
import com.viettel.bccs.cm.dao.TaskManagementDAO;
import com.viettel.bccs.cm.dao.TechnicalConnectorDAO;
import com.viettel.bccs.cm.model.ApDomain;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.Contract;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.DeployRequirement;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.TaskManagement;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.dao.im.IMDAO;
import com.viettel.brcd.dao.util.ContractDAOUtil;
import com.viettel.brcd.dao.util.RequestDAOUtil;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.ChangeDeploymentAddressIn;
import com.viettel.brcd.ws.model.input.ChangeDeploymentAddressLocation;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.supplier.nims.getInfoInfras.GetSnFromSplReponse;
import com.viettel.brcd.ws.supplier.nims.lockinfras.LockAndUnlockInfrasResponse;
import com.viettel.brcd.ws.supplier.nims.lockinfras.NimsWsLockUnlockInfrasBusiness;
import com.viettel.brcd.ws.supplier.nims.lockinfras.ResultForm;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.tool.hbm2x.StringUtils;
import org.hibernate.transform.Transformers;

/**
 * ChangingDeploymentAddressSupplier
 *
 * @author phuonghc
 */
public class ChangingDeploymentAddressSupplier {

    private static final Logger LOGGER = Logger.getLogger(ChangingDeploymentAddressSupplier.class);
    private static final String TASK_CHANGE_DEPLOYMENT_ADDRESS = "changing deployment address";

    public List<SubAdslLeaseline> getListAccountForSubscriber(Session sessionCmPos, String custId) {
        LOGGER.info("### Start get list account from custId: " + custId);
        List<SubAdslLeaseline> result = new ArrayList<SubAdslLeaseline>();
        String sql = "SELECT s.account account, s.sub_id subId, sub.infra_type infraType, sub.service_type serviceType, sub.product_code productCode "
                + "FROM customer cs "
                + "INNER JOIN contract c ON cs.cust_id = c.cust_id "
                + "INNER JOIN all_tel_service_sub s ON c.contract_id = s.contract_id "
                + "INNER JOIN sub_adsl_ll sub ON sub.sub_id = s.sub_id "
                + "AND cs.cust_id = ?";
        try {
            Query query = sessionCmPos.createSQLQuery(sql)
                    .addScalar("account", Hibernate.STRING)
                    .addScalar("subId", Hibernate.LONG)
                    .addScalar("infraType", Hibernate.STRING)
                    .addScalar("serviceType", Hibernate.STRING)
                    .addScalar("productCode", Hibernate.STRING)
                    .setResultTransformer(Transformers.aliasToBean(SubAdslLeaseline.class));
            query.setParameter(0, custId);
            result = query.list();
            if (result == null || result.isEmpty()) {
                return new ArrayList<SubAdslLeaseline>();
            }
        } catch (HibernateException he) {
            LOGGER.error("### An error occured when get account from custId" + he.getMessage());
        } finally {
            LOGGER.info("### End get list account from custId: " + custId);
            return result;
        }
    }

    public List<Reason> getListReasonByType(Session sessionCmPos, String custId, String account) {
        LOGGER.info("### Start get list reason");
        List<SubAdslLeaseline> subAdslLlList = new CustomerBussiness().findObjectByCustIdAndAccount(sessionCmPos, Long.valueOf(custId), account);
        if (subAdslLlList != null && !subAdslLlList.isEmpty()) {
            SubAdslLeaseline subAdslLl = subAdslLlList.get(0);
            try {
                List<Reason> reasonList = getListReasonByActionCodeAndTelServiceForAudit(sessionCmPos, Constants.ACTION_SUB_CHANGE_INS_ADDR, subAdslLl.getServiceType(), subAdslLl.getProductCode());

                //phuonghc 07012020 - remove CH_AF_1ST if changeaddress seconds time above
                List<TaskManagement> taskManagementList = new TaskManagementDAO().findListTaskManagementByAccount(sessionCmPos, account);
                boolean hasTaskChangeAddress = false;
                for (TaskManagement ele : taskManagementList) {
                    if (StringUtils.isNotEmpty(ele.getTaskName()) && ele.getTaskName().contains(TASK_CHANGE_DEPLOYMENT_ADDRESS)) {
                        hasTaskChangeAddress = true;
                    }
                }
                if (hasTaskChangeAddress) {
                    Iterator itr = reasonList.iterator();
                    while (itr.hasNext()) {
                        Reason reasonSpecial = (Reason) itr.next();
                        if ("CH_AF_1ST".equalsIgnoreCase(reasonSpecial.getCode())) {
                            itr.remove();
                            break;
                        }
                    }
                }

                if (reasonList.isEmpty()) {
                    LOGGER.info("### Cannot find reason with subAdslLl.getProductCode()=" + subAdslLl.getProductCode());
                    return new ArrayList<Reason>();
                }
                return reasonList;
            } catch (Exception e) {
                LOGGER.error("### An error occured while find reason custId=" + custId, e);
                return new ArrayList<Reason>();
            }
        } else {
            LOGGER.error("### Cannot find SubAdslLeaseline with custId=" + custId);
            return new ArrayList<Reason>();
        }
    }

    public ParamOut changeDeploymentAddress(Session sessionCmPos, Session sessionCmPre, Session sessionCc, Session sessionNims, Session sessionIm,
            ChangeDeploymentAddressIn input, String locale) throws Exception {
        LOGGER.info("### Start process change deployment address for ADSL, FTTH");
        ParamOut out = new ParamOut();

        /*cannot find subscriber ADSL | FTTH*/
        List<SubAdslLeaseline> subAdslList = new SubAdslLeaselineSupplier().findById(sessionCmPos, Long.valueOf(input.getSubId()));
        if (subAdslList == null || subAdslList.isEmpty() || subAdslList.get(0) == null) {
            out.setErrorCode(Constants.ERROR_CODE_1);
            out.setErrorDecription(LabelUtil.getKey("sub.not.available", locale));
            return out;
        }
        SubAdslLeaseline subAdsl = subAdslList.get(0);
        String oldProvinceModel = subAdsl.getProvince();
        String oldDistrictModel = subAdsl.getDistrict();
        String oldPrecinctModel = subAdsl.getPrecinct();
        String oldStreetNameModel = subAdsl.getStreetName();
        String oldHomeModel = subAdsl.getStreetName();
        String oldDeployAddressModel = subAdsl.getDeployAddress();
        String oldLineTypeModel = subAdsl.getLineType();
        String oldLinePhoneModel = subAdsl.getLinePhone();
        Long oldTeamIdModel = subAdsl.getTeamId();

        String infraType = (subAdsl.getInfraType() != null) ? subAdsl.getInfraType() : "AON";
        String telService = (subAdsl.getLineType() != null) ? subAdsl.getLineType() : "";
        /* if this connector is locked already => cannot to change deployment address*/
        try {
            if (this.findLockedService(sessionCmPos, sessionNims, infraType, Long.valueOf(input.getConnectorId())).toUpperCase().trim().equals(telService.toUpperCase().trim())) {
                out.setErrorCode(Constants.ERROR_CODE_1);
                out.setErrorDecription(LabelUtil.getKey("connector.is.locked", locale));
                return out;
            }
        } catch (Exception e) {
            LOGGER.info("### Cannot find lock service for connectorId=" + input.getConnectorId() + "||" + e.getMessage());
        }
        boolean isChange = false;
        /*From provinceCode, districtCode, precinctCode get province, district, precinct*/

        ChangeDeploymentAddressLocation locationName = this.getLocationName(sessionCmPos, input.getProvinceCode() + input.getDistrictCode() + input.getPrecinctCode());
        String teamId = this.getTeamIdFromTeamCode(sessionCmPos, input.getTeamCode());
        if (StringUtils.isEmpty(teamId)) {
            out.setErrorCode(Constants.ERROR_CODE_1);
            out.setErrorDecription(LabelUtil.getKey("teamCode.is.invalid", locale));
            return out;
        }

        String province = (locationName.getProvince() == null) ? null : locationName.getProvince().trim().toUpperCase();
        String newDistrict = (locationName.getDistrict() == null) ? null : locationName.getDistrict().trim().toUpperCase();
        String newPrecinct = (locationName.getPrecinct() == null) ? null : locationName.getPrecinct().trim().toUpperCase();
        String newStreetName = (input.getStreetName() == null) ? null : input.getStreetName();
        String newHome = (input.getHome() == null) ? null : input.getHome().trim();

        String newDeployAddress = input.getHome() + "," + input.getStreetName() + "," + locationName.getPrecinct() + "," + locationName.getDistrict() + "," + locationName.getProvince();
        String newLineType = (input.getLineType() == null) ? null : input.getLineType().trim();
        String newLinePhone = (input.getIsdn() == null) ? null : input.getIsdn().trim();
        Long newTeamId = Long.valueOf(teamId);
        String oldProvince = (oldProvinceModel == null) ? "" : oldProvinceModel.trim().toUpperCase();

        if (newTeamId == null || newTeamId == 0L) {
            out.setErrorCode(Constants.ERROR_CODE_1);
            out.setErrorDecription(LabelUtil.getKey("teamId.is.not.correct", locale));
            return out;
        }

        if (StringUtils.isEmpty(province)) {
            out.setErrorCode(Constants.ERROR_CODE_1);
            out.setErrorDecription(LabelUtil.getKey("province.is.not.correct", locale));
            return out;
        }

        if (StringUtils.isEmpty(newDistrict)) {
            out.setErrorCode(Constants.ERROR_CODE_1);
            out.setErrorDecription(LabelUtil.getKey("newDistrict.is.not.correct", locale));
            return out;
        }

        if (StringUtils.isEmpty(newPrecinct)) {
            out.setErrorCode(Constants.ERROR_CODE_1);
            out.setErrorDecription(LabelUtil.getKey("newPrecinct.is.not.correct", locale));
            return out;
        }

        String newDeployAreaCode = input.getProvinceCode().concat(input.getDistrictCode()).concat(input.getPrecinctCode());

        if (!ContractDAOUtil.validatePayAreaCode(sessionCmPos, newDeployAreaCode)) {
            out.setErrorCode(Constants.ERROR_CODE_1);
            out.setErrorDecription(LabelUtil.getKey("newDeployAreaCode.is.not.correct", locale));
            return out;
        }

        boolean isSameProvince = oldProvince.equalsIgnoreCase(input.getProvinceCode());
        /*Bo sung doan pass cho cac tinh PAI thuoc BAT, KEB thuoc KAM*/
        if (!isSameProvince) {
            for (String text : Constants.ALLOW_CHANGE_ADDRESS_ADSL) {
                if (text.contains(oldProvince) && text.contains(province)) {
                    isSameProvince = true;
                    break;
                }
            }
        }

        if (!isSameProvince) {
            out.setErrorCode(Constants.ERROR_CODE_1);
            out.setErrorDecription(LabelUtil.getKey("error.not.same.province", locale) + oldProvince);
            return out;
        }

        boolean isManyRequest = new DeployRequirementDAO().checkNumberRequestChangeAddrTask(sessionCmPos, Long.valueOf(input.getSubId()), Constants.SERVICE_ALIAS_ADSL);
        if (isManyRequest) {
            out.setErrorCode(Constants.ERROR_CODE_1);
            out.setErrorDecription(LabelUtil.getKey("too.many.task.change", locale));
            return out;
        }

        String oldDistrict = (oldDistrictModel == null) ? "" : oldDistrictModel.trim().toUpperCase();
        String oldPrecinct = (oldPrecinctModel == null) ? "" : oldPrecinctModel.trim().toUpperCase();
        String oldStreetName = (oldStreetNameModel == null) ? "" : oldStreetNameModel.trim();
        String oldHome = (oldHomeModel == null) ? "" : oldHomeModel.trim();
        String oldDeployAddress = (oldDeployAddressModel == null) ? "" : oldDeployAddressModel.trim();
        String oldLineType = (oldLineTypeModel == null) ? "" : oldLineTypeModel.trim();
        String oldLinePhone = (oldLinePhoneModel == null) ? "" : oldLinePhoneModel.trim();
        Long oldTeamId = oldTeamIdModel;
        Long actionAuditId = getSequence(sessionCmPos, "ACTION_AUDIT_SEQ");

        if (locationName.getDistrictCode() != null && !newDistrict.equals(oldDistrict)) {
            subAdsl.setDistrict(locationName.getDistrictCode());
            isChange = true;
        }

        if (locationName.getPrecinctCode() != null && !newPrecinct.equals(oldPrecinct)) {
            subAdsl.setPrecinct(locationName.getPrecinctCode());
            isChange = true;
        }

        if (newStreetName != null && !newStreetName.equals(oldStreetName)) {
            subAdsl.setStreetName(newStreetName);
            isChange = true;
        }
        if (newHome != null && !newHome.equals(oldHome)) {
            subAdsl.setHome(newHome);
            isChange = true;
        }
        if (newDeployAddress != null && !newDeployAddress.equals(oldDeployAddress)) {
            subAdsl.setDeployAddress(newDeployAddress);
            subAdsl.setDeployAreaCode(newDeployAreaCode);
            isChange = true;
        }
        if (newTeamId != null && newTeamId.longValue() != 0 && (oldTeamId == null || oldTeamId.longValue() == 0 || !newTeamId.equals(oldTeamId))) {
            subAdsl.setTeamId(newTeamId);
            isChange = true;
        }

        if (newLineType != null && !newLineType.equals(oldLineType)) {
            subAdsl.setLineType(newLineType);
            isChange = true;
        }

        if ((newLinePhone != null && !newLinePhone.equals(oldLinePhone)) || (newLinePhone == null && oldLinePhone != null)) {
            subAdsl.setLinePhone(newLinePhone);
            isChange = true;
        }

        if (!isChange) {
            out.setErrorCode(Constants.ERROR_CODE_1);
            out.setErrorDecription(LabelUtil.getKey("nothing.deployment.address.change", locale));
            return out;
        }

        /*impact time*/
        subAdsl.setChangeDatetime(new Date());
        subAdsl.setProject(null);
        /*update new value for cable_box_id, dslam_id, station_id, broad_id, port_no*/
        subAdsl.setCableBoxId(Long.valueOf(input.getConnectorId()));
        subAdsl.setStationId(Long.valueOf(input.getStationId()));

        /*Create new record for deploy_requiment*/
        DeployRequirement deployRequirement = new DeployRequirement();
        new DeployRequirementDAO().addDeployRequirementForChangeAdd(sessionCmPos, subAdsl, deployRequirement, new Date(), actionAuditId, locale);
        /*Bonus information for deploymentRequirement*/
        deployRequirement.setShopId(subAdsl.getTeamId());
        deployRequirement.setDeployAreaCode(subAdsl.getDeployAreaCode());
        deployRequirement.setDeployAddress(subAdsl.getDeployAddress());
        deployRequirement.setLineType(subAdsl.getLineType());
        deployRequirement.setUserId(Long.valueOf(input.getStaffId()));
        deployRequirement.setTelFax(subAdsl.getTelFax());
        deployRequirement.setDslamId(subAdsl.getDslamId());
        deployRequirement.setReasonId(Long.valueOf(input.getReasonId()));
        deployRequirement.setMobile("9"); //change deployment address from mobile, not from web

        if (input.getLimitDate() != null) {
            deployRequirement.setExpiredDate(input.getLimitDate());
        }

        sessionCmPos.save(deployRequirement);
        /*update for old subdeployment*/
        new SubDeploymentDAO().deleteSubDeploymentBySub(sessionCmPos, Long.valueOf(input.getSubId()), null, new Date(), LOGGER);
        /*insert a new subdeployment*/
        new SubDeploymentDAO().addSubDeploymentBySub(sessionCmPos, subAdsl, new Date(), actionAuditId,
                Constants.ACTION_SUB_CHANGE_INS_ADDR, Long.valueOf(input.getStationId()), Long.valueOf(input.getConnectorId()), Constants.SUB_DEPLOYMENT_STAGE_ID_WORKING);

        /*assign task automatically*/
        Date limitDate = null;
        if (input.getLimitDate() != null) {
            limitDate = input.getLimitDate();
        }

        String sdateTimeDeploy = input.getDateTimeDeploy();
        String delayReason = "";
        Date dtDeploy = new Date();

        if (sdateTimeDeploy != null && !sdateTimeDeploy.trim().isEmpty()) {
            delayReason = input.getDelayReason();
            dtDeploy = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(sdateTimeDeploy);
            if (dtDeploy.compareTo(new Date()) < 0) {
                out.setErrorCode(Constants.ERROR_CODE_1);
                out.setErrorDecription(LabelUtil.getKey("must.over.current.time", locale));
                return out;
            }
            if (dtDeploy.getTime() - new Date().getTime() > 7 * 24 * 60 * 60 * 1000) {
                out.setErrorCode(Constants.ERROR_CODE_1);
                out.setErrorDecription(LabelUtil.getKey("must.not.over.7.days", locale));
                return out;
            }
        } else {
            dtDeploy = new Date();
        }

        boolean isCreateTaskSuccess = new TaskManagementDAO().createTaskManagement(sessionCmPos, sessionIm, sessionNims, sessionCmPre, sessionCc, subAdsl,
                dtDeploy, actionAuditId, Constants.TASK_REQ_TYPE_CHANGE_DEPLOY_ADDRESS,
                limitDate, subAdsl.getTelFax(), Long.valueOf(input.getStaffId()), locale, input.getStaffCode(), input.getShopCode(), delayReason);
        if (!isCreateTaskSuccess) {
            out.setErrorCode(Constants.ERROR_CODE_1);
            out.setErrorDecription(LabelUtil.getKey("cannot.create.task", locale));
            return out;
        }
        /* Update invoice for IM*/
        Long serviceId = null;
        /*Get saleTransCode from productCode*/
        if (Constants.SERVICE_ALIAS_ADSL.equals(subAdsl.getServiceType())) {
            serviceId = Constants.SERVICE_ADSL_ID_WEBSERVICE;
        } else if (Constants.SERVICE_ALIAS_FTTH.equals(subAdsl.getServiceType())) {
            serviceId = Constants.SERVICE_FTTH_ID_WEBSERVICE;
        }
        String saleTransCode = new MappingDAO().getSaleServiceCode(sessionCmPos, serviceId, Long.valueOf(input.getReasonId()), subAdsl.getProductCode(), Constants.ACTION_SUB_CHANGE_INS_ADDR, null);
        Contract contract = new ContractBussiness().findByIdActive(sessionCmPos, subAdsl.getContractId());
        Customer customer = (Customer) sessionCmPos.get(Customer.class.getName(), contract.getCustId());
        Shop shop = new ShopDAO().findByCode(sessionCmPos, input.getShopCode());
        Staff staff = new StaffDAO().findByCode(sessionCmPos, input.getStaffCode());
        Reason reason = new ReasonDAO().findById(sessionCmPos, Long.valueOf(input.getReasonId()));

        String messageIm = new IMDAO().executeSaleTrans(sessionCmPos, sessionIm, saleTransCode,
                Constants.TRANS_OF_SUBSCRIBER, contract, customer, subAdsl, shop, staff, reason, Constants.ACTION_SUB_CHANGE_INS_ADDR, new Date(), locale);
        sessionCmPos.evict(subAdsl);

        if ("true".equals(LabelUtil.getKey("allow.to.lock.port", locale))) {
            LockAndUnlockInfrasResponse infrasResponse = NimsWsLockUnlockInfrasBusiness.lockUnlockInfras(NimsWsLockUnlockInfrasBusiness.INFRAS_ACTION_LOCK,
                    subAdsl.getAccount(), null, Long.valueOf(input.getConnectorId()), null, subAdsl.getServiceType(), subAdsl.getInfraType(), null, null, locale);
            ResultForm resultForm = infrasResponse.getReturn();
            if (resultForm != null && !resultForm.getResult().equals(NimsWsLockUnlockInfrasBusiness.INFRAS_LOCL_UNLOCK_OK)) {
                out.setErrorCode(Constants.ERROR_CODE_1);
                out.setErrorDecription(LabelUtil.getKey("lock.port.unsuccessfully", locale));
                return out;
            }
        }

        if (!"".equals(messageIm)) {
            out.setErrorCode(Constants.ERROR_CODE_1);
            out.setErrorDecription(messageIm);
            return out;
        }

        out.setErrorCode(Constants.ERROR_CODE_0);
        out.setErrorDecription(LabelUtil.getKey("change.deployment.address.successfully", locale));
        return out;
    }

    public ChangeDeploymentAddressLocation getInformationNewDeploymentAddressFromLatLng(Session sessionCmPos, String lat, String lng) {
        String sql = "select province_name province, province provinceCode, district_name district, district districtCode, precinct_name precinct, precinct precinctCode "
                + " from area_extended where gg_location_lat = ? and gg_location_lng = ?";
        Query query = sessionCmPos.createSQLQuery(sql)
                .addScalar("province", Hibernate.STRING)
                .addScalar("district", Hibernate.STRING)
                .addScalar("precinct", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ChangeDeploymentAddressLocation.class));
        List<ChangeDeploymentAddressLocation> result = query.list();

        if (result == null && result.isEmpty()) {
            return new ChangeDeploymentAddressLocation();
        }
        return result.get(0);
    }

    //-------------------//
    //-------------------//
    //-------------------//
    private List<Reason> getListReasonByActionCodeAndTelServiceForAudit(Session sessionCmPos, String strActionCode, String telService, String productCode) {
        List<Reason> lstReason = new ArrayList<Reason>();
        String strQuery = "";
        List lstParam = new ArrayList();
        if (isMandatoryTransaction(sessionCmPos, strActionCode)) {
            strQuery = "SELECT r FROM Reason r, ApDomain a WHERE a.id.type = ? AND a.id.code = ? AND a.reasonType = r.type AND a.status = ? AND r.status = ?";
            lstParam.add(Constants.ACTION_FOR_AUDIT);
            lstParam.add(strActionCode);
            lstParam.add(Constants.STATUS_USE);
            lstParam.add(Constants.STATUS_USE);
        } else {
            strQuery = "SELECT r FROM Reason r, Mapping m WHERE m.actionCode = ? AND r.reasonId = m.reasonId AND r.status = ? AND m.status = ? AND (m.productCode = ? or m.productCode is null)";
            lstParam.add(strActionCode);
            lstParam.add(Constants.STATUS_USE);
            lstParam.add(Constants.STATUS_USE);
            lstParam.add(productCode);
        }
        if (StringUtils.isNotEmpty(telService)) {
            strQuery += " AND upper(r.telService) like ? ";
            lstParam.add("%" + telService.toUpperCase() + "%");
        }
        strQuery += " order by r.code";
        Query query = sessionCmPos.createQuery(strQuery);
        if (lstParam != null && !lstParam.isEmpty()) {
            for (int i = 0; i < lstParam.size(); i++) {
                query.setParameter(i, lstParam.get(i));
            }
        }
        lstReason = query.list();
        if (lstReason == null || lstReason.isEmpty()) {
            return new ArrayList<Reason>();
        }
        return lstReason;
    }

    private boolean isMandatoryTransaction(Session sesssionCmPos, String strActionCode) {
        boolean isMandatory = false;
        try {
            List<ApDomain> apDomainList = getApDomainByTypeAndCode(sesssionCmPos, Constants.AP_DOMAIN_TYPE_ACTION_DOC, strActionCode.trim());
            if (apDomainList.isEmpty()) {
                isMandatory = false;
            } else {
                ApDomain apDomain = apDomainList.get(0);
                if (StringUtils.isNotEmpty(apDomain.getGroupCode())
                        && apDomain.getGroupCode().trim().equals(new ApParamBussiness().getUserManualConfig(sesssionCmPos).get(Constants.MANDATORY_TRANSACTION_ACTION))) {
                    return true;
                }
            }
        } catch (Exception e) {
            LOGGER.error("### An error occured while doing check mandatory transaction: " + e.getMessage());
            isMandatory = false;
        } finally {
            return isMandatory;
        }
    }

    private List<ApDomain> getApDomainByTypeAndCode(Session sessionCmPos, String type, String code) {
        List<ApDomain> apDomainList = new ArrayList<ApDomain>();
        try {
            if (StringUtils.isNotEmpty(type)) {
                StringBuilder sql = new StringBuilder("From ApDomain Where id.type = ? and status = ? ");
                List lstParam = new ArrayList();
                lstParam.add(type.trim());
                lstParam.add(Constants.STATUS_USE);

                if (StringUtils.isNotEmpty(code)) {
                    sql.append(" and id.code = ? ");
                    lstParam.add(code.trim());
                }
                Query query = sessionCmPos.createQuery(sql.toString());
                if (!lstParam.isEmpty()) {
                    for (int i = 0; i < lstParam.size(); i++) {
                        query.setParameter(i, lstParam.get(i));
                    }
                }
                apDomainList = query.list();
            }
        } catch (Exception e) {
            LOGGER.error("### An error occured while doing getApDomainByTypeAndCode " + e.getMessage());
        } finally {
            return apDomainList;
        }
    }

    private String findLockedService(Session sessionCmPos, Session sessionNims, String infraType, Long connectorId) throws Exception {
        String lockedService = "";
        List<ApParam> apList = new ApParamBussiness().findByTypeCode(sessionCmPos, "LOCK_CONNECTOR", "LOCK_CONNECTOR");
        if (apList != null && !apList.isEmpty() && apList.get(0) != null) {
            String connectorCode = "";
            //List<String> lstA = req.getConnectorCodeById(nimsSession, input.getInfrastruct().getCableBoxId(), input.getInfrastruct().getTechnology());
            List<String> listA = getConnectorCodeById(sessionNims, connectorId, infraType);
            if (Constants.MAP_LIMIT_MATERIAL_INFRA_TYPE_GPON.equals(infraType)) {
                String splCode = String.valueOf((listA != null && !listA.isEmpty()) ? (String) listA.get(0) : "");
                GetSnFromSplReponse snId = new TechnicalConnectorDAO().findBySplitterCode(splCode);
                List<String> lstB = getConnectorCodeById(sessionNims, Long.valueOf((snId != null) ? snId.getReturn().longValue() : 0L), infraType);
                connectorCode = String.valueOf((lstB != null && !lstB.isEmpty()) ? (String) lstB.get(0) : "");
            } else {
                connectorCode = String.valueOf((listA != null && !listA.isEmpty()) ? (String) listA.get(0) : "");
            }

            if (StringUtils.isNotEmpty(connectorCode)) {
                List<String> list = new RequestDAOUtil().getLockServiceByConnectorCode(sessionCmPos, connectorCode, infraType);
                if (list != null && !list.isEmpty()) {
                    lockedService = String.valueOf(list.get(0));
                }
            }
        }
        return lockedService;
    }

    private List<String> getConnectorCodeById(Session sessionNims, Long connectorId, String infraType) {
        try {
            if (connectorId == null || connectorId == 0L) {
                return new ArrayList<String>();
            }
            String sql = "";
            if (Constants.MAP_LIMIT_MATERIAL_INFRA_TYPE_GPON.equals(infraType)) {
                sql = "SELECT a.device_code FROM nims_fcn.infra_device a WHERE a.device_id = ? ";
            }
            if (Constants.MAP_LIMIT_MATERIAL_INFRA_TYPE_AON.equals(infraType)) {
                sql = "SELECT b.odf_code FROM nims_cn.odn_odf b WHERE b.odf_id = ? ";
            }
            Query query = sessionNims.createSQLQuery(sql);
            query.setParameter(0, connectorId);
            List result = query.list();
            if (result != null && !result.isEmpty()) {
                return result;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return new ArrayList<String>();
    }

    private long getSequence(Session sessionCmPos, String sequenceName) throws Exception {
        String strQuery = "SELECT " + sequenceName + " .NextVal FROM Dual";
        Query queryObject = sessionCmPos.createSQLQuery(strQuery);
        BigDecimal bigDecimal = (BigDecimal) queryObject.uniqueResult();
        return bigDecimal.longValue();
    }

    private ChangeDeploymentAddressLocation getLocationName(Session sessionCmPos, String areaCode) {

        String sql = "select province_name province, district_name district, precinct_name precinct from area_extended where area_code=:areaCode";
        Query query = sessionCmPos.createSQLQuery(sql)
                .addScalar("province", Hibernate.STRING)
                .addScalar("district", Hibernate.STRING)
                .addScalar("precinct", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ChangeDeploymentAddressLocation.class));
        query.setParameter("areaCode", areaCode);
        List<ChangeDeploymentAddressLocation> result = query.list();

        if (result == null || result.isEmpty()) {
            return new ChangeDeploymentAddressLocation();
        }
        return result.get(0);
    }

    private String getTeamIdFromTeamCode(Session sessionCmPos, String teamCode) {
        String sql = "select DISTINCT team_id from technical_station where team_code=:teamCode";
        Query query = sessionCmPos.createSQLQuery(sql);
        query.setParameter("teamCode", teamCode);

        List result = query.list();
        if (result == null || result.isEmpty()) {
            return "";
        }
        return String.valueOf(result.get(0));
    }

    public static void main(String[] args) throws Exception {
        Date a = new Date();
        Date b = new Date();

        try {
            BeanUtils.copyProperties(b, a);
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println(b);
    }
}
