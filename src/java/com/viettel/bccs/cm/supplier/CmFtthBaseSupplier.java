/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.supplier;

import com.opensymphony.xwork2.util.TextUtils;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.TechnicalStation;
import com.viettel.bccs.cm.model.ftth.CustomerBTSConnectorBean;
import com.viettel.bccs.cm.model.ftth.InterruptExtend;
import com.viettel.bccs.cm.model.ftth.InterruptReasonDetail;
import com.viettel.bccs.cm.model.ftth.InterruptReasonType;
import com.viettel.bccs.cm.model.ftth.InterruptSms;
import com.viettel.bccs.cm.model.ftth.Interruption;
import com.viettel.bccs.cm.model.ftth.SubscriberFtth;
import com.viettel.bccs.cm.model.ftth.SubscriberInterruptSms;
import com.viettel.bccs.cm.util.DateTimeUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author partner1
 */
public class CmFtthBaseSupplier extends BaseSupplier {

    public CmFtthBaseSupplier() {
        logger = Logger.getLogger(CmFtthBaseSupplier.class);
    }

    public List<InterruptReasonType> getReasonType(Session cmPosSession, Long reasonTypeId, Long status) {
        StringBuilder sql = new StringBuilder().append(" from InterruptReasonType where 1 = 1 ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        if (reasonTypeId != null) {
            sql.append(" and id = :id ");
            params.put("id", reasonTypeId);
        }
        sql.append(" order by code asc ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<InterruptReasonType> result = query.list();
        return result;
    }

    public List<InterruptReasonDetail> getReasonDetail(Session cmPosSession, Long reasonTypeId, Long status, Long reasonDetailId) {
        StringBuilder sql = new StringBuilder().append(" from InterruptReasonDetail where 1 = 1 ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        if (reasonDetailId != null) {
            sql.append(" and id = :id ");
            params.put("id", reasonDetailId);
        }
        if (reasonTypeId != null) {
            sql.append(" and reasonTypeId = :reasonTypeId ");
            params.put("reasonTypeId", reasonTypeId);
        }
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by code asc ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<InterruptReasonDetail> result = query.list();
        return result;
    }

    public List<Shop> getBranchList(Session cmPosSession) {
        StringBuilder sql = new StringBuilder().append(" from Shop where (shopId = 61 or (parentShopId = 61 and channelTypeId = 1 and shopType = 2)) and status = 1");
        sql.append(" order by shopCode asc ");
        Query query = cmPosSession.createQuery(sql.toString());
        List<Shop> result = query.list();
        return result;
    }

    public Shop getCurBranchUser(Session cmPosSession, Long staffId) {
        StringBuilder sql = new StringBuilder().append("select a from Shop a, Staff b where a.shopId = b.shopId and a.shopId = nvl(TO_NUMBER(SUBSTR(a.shopPath || '_', INSTR(a.shopPath || '_', '_', 2) + 1, INSTR (a.shopPath || '_', '_', INSTR(a.shopPath || '_', '_', 2) + 1)- INSTR(a.shopPath || '_', '_', 2) - 1)), 61) and b.staffId = :staffId");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("staffId", staffId);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Shop> result = query.list();
        return (result != null && !result.isEmpty()) ? result.get(0) : null;
    }

    public List<Staff> getStaffOfBranch(Session cmPosSession, Long branchShopId) {
        StringBuilder sql = new StringBuilder().append("select a from Staff a, Shop b where a.shopId = b.shopId and a.status = 1 and b.status = 1 and a.channelTypeId = 14 and b.shopId in (select c.shopId from Shop c where c.shopId = :shopId or c.shopPath || '_' like :shopPath) ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("shopId", branchShopId);
        params.put("shopPath", "%_" + branchShopId + "_%");
        sql.append(" order by a.staffCode asc ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Staff> result = query.list();
        return result;
    }

    public List<TechnicalStation> getBtsByBranch(Session cmPosSession, Long branchShopId) {
        StringBuilder sql = new StringBuilder().append("select a from TechnicalStation a, Shop b where a.teamId = b.shopId and a.status = 1 and b.status = 1 and b.shopId in (select c.shopId from Shop c where c.shopId = :shopId or c.shopPath || '_' like :shopPath) ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("shopId", branchShopId);
        params.put("shopPath", "%_" + branchShopId + "_%");
        sql.append(" order by a.stationCode asc ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<TechnicalStation> result = query.list();
        return result;
    }

    public List<Interruption> getInterruptFtth(Session cmPosSession, Long interruptionId, String staffCode, Long reasonTypeId, Long reasonDetailId, String influenceScope, String fromDate, String toDate, Long shopId) throws Exception {
        HashMap<String, Object> params = new HashMap<String, Object>();
        StringBuilder sql = new StringBuilder().append("FROM Interruption Where 1 = 1 ");
        if (interruptionId != null && interruptionId > 0) {
            sql.append(" and id = :interruptionId ");
            params.put("interruptionId", interruptionId);
        }
        if (shopId != null && shopId > 0) {
            sql.append(" and shopId in (select c.shopId from Shop c where c.shopId = :shopId or c.shopPath || '_' like :shopPath) ");
            params.put("shopId", shopId);
            params.put("shopPath", "%_" + shopId + "_%");
        }
        if (staffCode != null && !"".equals(staffCode)) {
            sql.append(" and lower(createUser) = :createUser");
            params.put("createUser", staffCode.toLowerCase());
        }
        if (reasonTypeId != null && reasonTypeId > 0) {
            sql.append(" and reasonType = :reasonType");
            params.put("reasonType", reasonTypeId);
        }
        if (reasonDetailId != null && reasonDetailId > 0) {
            sql.append(" and reasonDetail = :reasonDetail");
            params.put("reasonDetail", reasonDetailId);
        }
        if (influenceScope != null && !"".equals(influenceScope)) {
            sql.append(" and influenceScope = :influenceScope");
            params.put("influenceScope", Long.parseLong(influenceScope));
        }
        if (fromDate != null && !"".equals(fromDate)) {
            sql.append(" and createDate >= :fromDate");
            params.put("fromDate", DateTimeUtils.toDateddMMyyyy(fromDate));
        }
        if (toDate != null && !"".equals(toDate)) {
            sql.append(" and createDate <= :toDate");
            params.put("toDate", DateTimeUtils.toDateddMMyyyy(toDate));
        }
        sql.append(" order by createDate desc ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Interruption> result = query.list();
        return result;
    }

    public List<InterruptExtend> getInterruptExtend(Session cmPosSession, Long interruptionId, Long influenceType) throws Exception {
        HashMap<String, Object> params = new HashMap<String, Object>();
        StringBuilder sql = new StringBuilder().append("FROM InterruptExtend Where 1 = 1 ");
        if (interruptionId != null) {
            sql.append(" and interruptionId = :interruptionId ");
            params.put("interruptionId", interruptionId);
        }
        if (influenceType != null) {
            sql.append(" and influenceType = :influenceType");
            params.put("influenceType", influenceType);
        }
        sql.append(" order by code asc ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<InterruptExtend> result = query.list();
        return result;
    }

    public List<InterruptSms> getInterruptSms(Session cmPosSession, Long interruptId) throws Exception {
        HashMap<String, Object> params = new HashMap<String, Object>();
        StringBuilder sql = new StringBuilder().append("FROM InterruptSms Where 1 = 1 ");
        if (interruptId != null) {
            sql.append(" and interruptionId = :interruptId ");
            params.put("interruptId", interruptId);
        }
        sql.append(" order by createDate asc ");

        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<InterruptSms> result = query.list();
        return result;
    }

    public List<SubscriberFtth> getListSubAdslLeaseLine(Session cmPosSession, String serviceType, Long influenceScope, Long stationId, Long connectorId) {
        List params = new ArrayList();
        String sql = "select a.sub_id subId, a.tel_mobile telMobile, nvl(a.infra_type,'AON') infraType, "
                + " a.account isdnAccount, c.name customerName, a.product_code productCode,"
                + " a.service_type servicesType, b.contract_no contractNumber, a.DEPLOY_ADDRESS deploymentAddress "
                + " From sub_adsl_ll a, contract b, customer c "
                + " Where a.contract_id = b.contract_id "
                + " and b.cust_id = c.cust_id "
                + " and a.status = 2 "
                + " and a.service_type = ? ";
        params.add(serviceType);

        if ("F".equals(serviceType)) {
            sql += " and (a.infra_type = 'AON' or a.infra_type is null) ";
        } else if ("G".equals(serviceType)) {
            sql += " and a.infra_type = 'GPON' ";
        }

        if (influenceScope.equals(0L)) {
            //su co toan mang
        } else if (influenceScope.equals(1L)) {
            //su co bts
            sql += " and a.station_id = ? ";
            params.add(stationId);
        } else {
            //su co connector
            sql += " and a.CABLE_BOX_ID = ? ";
            params.add(connectorId);
        }

        Query query = cmPosSession.createSQLQuery(sql).addScalar("subId", Hibernate.LONG).addScalar("telMobile", Hibernate.STRING).addScalar("infraType", Hibernate.STRING).addScalar("isdnAccount", Hibernate.STRING).addScalar("customerName", Hibernate.STRING).addScalar("productCode", Hibernate.STRING).addScalar("servicesType", Hibernate.STRING).addScalar("contractNumber", Hibernate.STRING).addScalar("deploymentAddress", Hibernate.STRING).setResultTransformer(Transformers.aliasToBean(SubscriberFtth.class));
        for (int i = 0; i < params.size(); i++) {
            query.setParameter(i, params.get(i));
        }
        return query.list();
    }

    public List<SubscriberFtth> getListSubPstn(Session cmPosSession, Long influenceScope, Long stationId, Long connectorId) {
        List params = new ArrayList();
        String sql = "select a.sub_id subId, a.tel_mobile telMobile, '' infraType, "
                + " a.isdn isdnAccount, c.name customerName, a.product_code productCode,"
                + " 'P' servicesType, b.contract_no contractNumber, a.DEPLOY_ADDRESS deploymentAddress "
                + " From sub_pstn a, contract b, customer c "
                + " Where a.contract_id = b.contract_id "
                + " and b.cust_id = c.cust_id "
                + " and a.status = 2 ";

        if (influenceScope.equals(0L)) {
            //su co toan mang
        } else if (influenceScope.equals(1L)) {
            //su co bts
            sql += " and a.BOARD_ID = ? ";
            params.add(stationId);
        } else {
            //su co connector
            sql += " and a.CABLE_BOX_ID = ? ";
            params.add(connectorId);
        }

        Query query = cmPosSession.createSQLQuery(sql).addScalar("subId", Hibernate.LONG).addScalar("telMobile", Hibernate.STRING).addScalar("infraType", Hibernate.STRING).addScalar("isdnAccount", Hibernate.STRING).addScalar("customerName", Hibernate.STRING).addScalar("productCode", Hibernate.STRING).addScalar("servicesType", Hibernate.STRING).addScalar("contractNumber", Hibernate.STRING).addScalar("deploymentAddress", Hibernate.STRING).setResultTransformer(Transformers.aliasToBean(SubscriberFtth.class));
        for (int i = 0; i < params.size(); i++) {
            query.setParameter(i, params.get(i));
        }
        return query.list();
    }

    public Long getNumSendSmsByService(Session cmPosSession, Long interruptId, String serviceType) throws Exception {
        HashMap<String, Object> params = new HashMap<String, Object>();
        StringBuilder sql = new StringBuilder().append("FROM InterruptSms Where 1 = 1 ");
        if (interruptId != null) {
            sql.append(" and interruptionId = :interruptId ");
            params.put("interruptId", interruptId);
        }
        if (serviceType != null && !"".equals(serviceType)) {
            sql.append(" and serviceType = :serviceType ");
            params.put("serviceType", serviceType);
        }

        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<InterruptSms> result = query.list();
        return Long.valueOf(result.size());
    }

    public List<SubscriberInterruptSms> getSubscriberInterruptSms(Session cmPosSession, Long interruptionId, String serviceType) throws Exception {
        HashMap<String, Object> params = new HashMap<String, Object>();
        StringBuilder sql = new StringBuilder().append("FROM SubscriberInterruptSms Where 1 = 1 ");
        if (interruptionId != null) {
            sql.append(" and interruptionId = :interruptionId ");
            params.put("interruptionId", interruptionId);
        }
        if (serviceType != null && !"".equals(serviceType)) {
            sql.append(" and serviceType = :serviceType ");
            params.put("serviceType", serviceType);
        }

        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<SubscriberInterruptSms> result = query.list();
        return result;
    }

    public void insertInterruption(Session cmPosSession, Long interruptionId, Long reasonTypeId, Long reasonDetailId,
            Date affectFromDate, Date affectToDate, List<String> lstCustomerType,
            Long influenceScrope, Long influenceLevel, List<String> lstServiceInterrupted,
            String moreDescription, String documentName, byte[] documentData, String staffCode,
            Date nowDate, Long shopId) {
        Interruption interruption = new Interruption();
        interruption.setId(interruptionId);
        interruption.setStatus(0L);
        interruption.setReasonType(reasonTypeId);
        interruption.setReasonDetail(reasonDetailId);
        interruption.setAffectFromDate(affectFromDate);
        interruption.setAffectToDate(affectToDate);
        String customerTypes = TextUtils.join(",", lstCustomerType);
        interruption.setAffectCustomerType(customerTypes);
        interruption.setInfluenceScope(influenceScrope);
        interruption.setInfluenceLevel(influenceLevel);
        String serviceInterrupted = TextUtils.join(",", lstServiceInterrupted);;
        interruption.setServiceInterrupted(serviceInterrupted);
        interruption.setDescription(moreDescription);
        interruption.setDocumentName(documentName);
        interruption.setDocumentContent(documentData);
        interruption.setCreateUser(staffCode);
        interruption.setCreateDate(nowDate);
        interruption.setShopId(shopId);
        cmPosSession.save(interruption);
    }

    public void insertInterruptionExtend(Session cmPosSession, Long id, Long interruptionId, Long influenceType, Long stationId, Long connectorId, String code, String name) {
        InterruptExtend interruptExtend = new InterruptExtend();
        interruptExtend.setId(id);
        interruptExtend.setInterruptionId(interruptionId);
        interruptExtend.setInfluenceType(influenceType);
        interruptExtend.setStationId(stationId);
        interruptExtend.setConnectorId(connectorId);
        interruptExtend.setCode(code);
        interruptExtend.setName(name);
        cmPosSession.save(interruptExtend);
    }

    public void insertSubscriberInterruptSms(Session cmPosSession, Long id, Long interruptionId, String createUser, Date createDate, String isdnAccount, String customerName,
            String productCode, String serviceType, String contractNumber, String deploymentAddress, String btsCode, Long btsId, String connectorCode, Long connectorId,
            Long subId, String telMobile, String infraType) {
        SubscriberInterruptSms subscriberInterruptSms = new SubscriberInterruptSms();
        subscriberInterruptSms.setId(id);
        subscriberInterruptSms.setInterruptionId(interruptionId);
        subscriberInterruptSms.setCreateUser(createUser);
        subscriberInterruptSms.setCreateDate(createDate);
        subscriberInterruptSms.setIsdnAccount(isdnAccount);
        subscriberInterruptSms.setCustomerName(customerName);
        subscriberInterruptSms.setProductCode(productCode);
        subscriberInterruptSms.setServiceType(serviceType);
        subscriberInterruptSms.setContractNumber(contractNumber);
        subscriberInterruptSms.setDeploymentAddress(deploymentAddress);
        subscriberInterruptSms.setBtsCode(btsCode);
        subscriberInterruptSms.setBtsId(btsId);
        subscriberInterruptSms.setConnectorCode(connectorCode);
        subscriberInterruptSms.setConnectorId(connectorId);
        subscriberInterruptSms.setSubId(subId);
        subscriberInterruptSms.setTelMobile(telMobile);
        subscriberInterruptSms.setInfraType(infraType);
        cmPosSession.save(subscriberInterruptSms);
    }

    public void insertInterruptSms(Session cmPosSession, Long id, Long interruptionId, String createUser, Date createDate, String smsContent, String serviceType, String infraType) {
        InterruptSms interruptSms = new InterruptSms();
        interruptSms.setId(id);
        interruptSms.setInterruptionId(interruptionId);
        interruptSms.setCreateUser(createUser);
        interruptSms.setCreateDate(createDate);
        interruptSms.setSmsContent(smsContent);
        interruptSms.setServiceType(serviceType);
        interruptSms.setInfraType(infraType);
        cmPosSession.save(interruptSms);
    }

    public List getServiceSmsNotYet(Session cmPosSession, Long interruptionId, Long numSendSms, String affectCustomerType) {
        List customerTypeNotYet = new ArrayList();
        String[] lstCustomerType = affectCustomerType.split(",");
        HashMap<String, Object> params = new HashMap<String, Object>();
        String sql = "Select count(*) numSendSms, service_type serviceType from INTERRUPTION_SMS where 1 = 1 ";
        if (interruptionId != null) {
            sql += " and INTERRUPTION_ID = :interruptionId ";
            params.put("interruptionId", interruptionId);
        }
        sql += " group by service_type having count(*) >= :numSendSms";
        params.put("numSendSms", numSendSms);
        Query query = cmPosSession.createSQLQuery(sql).addScalar("numSendSms", Hibernate.LONG).addScalar("serviceType", Hibernate.STRING).setResultTransformer(Transformers.aliasToBean(InterruptSms.class));
        buildParameter(query, params);
        List<InterruptSms> lstInterruptSmses = query.list();
        boolean isNotYet = true;
        for (int i = 0; i < lstCustomerType.length; i++) {
            isNotYet = true;
            for (InterruptSms interruptSms : lstInterruptSmses) {
                if (interruptSms.getServiceType().equals(lstCustomerType[i])) {
                    isNotYet = false;
                }
            }
            if (isNotYet) {
                customerTypeNotYet.add(lstCustomerType[i]);
            }
        }
        return customerTypeNotYet;
    }

    public List<CustomerBTSConnectorBean> countCustomerBTSConnector(Session cmPosSession, Long interruptId) {
        HashMap<String, Object> params = new HashMap<String, Object>();
        String sql = "Select bts_id btsId, bts_code btsCode, connector_id connectorId, connector_code connectorCode, count(*) count "
                + " from SUBSCRIBER_INTERRUPTION_SMS where INTERRUPTION_ID = :interruptId "
                + " group by bts_id, bts_code, connector_id, connector_code ";
        params.put("interruptId", interruptId);
        Query query = cmPosSession.createSQLQuery(sql).addScalar("btsId", Hibernate.LONG).addScalar("btsCode", Hibernate.STRING).addScalar("connectorId", Hibernate.LONG).addScalar("connectorCode", Hibernate.STRING).addScalar("count", Hibernate.LONG).setResultTransformer(Transformers.aliasToBean(CustomerBTSConnectorBean.class));
        buildParameter(query, params);
        return query.list();
    }

    public List<CustomerBTSConnectorBean> countCustomerServiceBtsConnector(Session cmPosSession, Long interruptId, Long btsId, Long connectorId) {
        HashMap<String, Object> params = new HashMap<String, Object>();
        String sql = "Select service_type customerType, count(*) count "
                + " from SUBSCRIBER_INTERRUPTION_SMS where INTERRUPTION_ID = :interruptId ";
        if (btsId != null && btsId > 0) {
            sql += " And bts_id = :btsId ";
            params.put("btsId", btsId);
        }
        if (connectorId != null && connectorId > 0) {
            sql += " And connector_id = :connectorId ";
            params.put("connectorId", connectorId);
        }
        sql += " group by service_type ";
        params.put("interruptId", interruptId);
        Query query = cmPosSession.createSQLQuery(sql).addScalar("customerType", Hibernate.STRING).addScalar("count", Hibernate.LONG).setResultTransformer(Transformers.aliasToBean(CustomerBTSConnectorBean.class));
        buildParameter(query, params);
        return query.list();
    }
}
