package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.common.util.PerformanceLogger;
import com.viettel.passport.PassportWS;
import com.viettel.passport.PassportWSService;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import viettel.passport.client.UserToken;

public class CmUserTokenSupplier extends BaseSupplier {

    public CmUserTokenSupplier() {
        logger = Logger.getLogger(PerformanceLogger.class);
    }

    public UserToken getUserToken(String userName, String password, String locale) throws MalformedURLException, ParserConfigurationException, SAXException, IOException {
        UserToken result = null;

        String passportUrl = ResourceUtils.getResource("passportUrl", "vsa_config");
        String domainCode = ResourceUtils.getResource("domainCode", "vsa_config");
        Date sTime = new Date();
        LogUtils.info(logger, "CmUserTokenSupplier.getUserToken: \n passportUrl= " + passportUrl + "\n domainCode= " + domainCode + " start time= " + sTime);
        PassportWSService pws = new PassportWSService(new URL(passportUrl));
        PassportWS ppWS = pws.getPassportWSPort();
        String response = ppWS.validate(userName, password, domainCode);
        sTime = new Date();
        LogUtils.info(logger, "CmUserTokenSupplier.getUserToken: end time= " + sTime);
        if (response == null || Constants.NO.equals(response)) {
            return result;
        }

        result = UserToken.parseXMLResponse(response);
        return result;
    }
}
