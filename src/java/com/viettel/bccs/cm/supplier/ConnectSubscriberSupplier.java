/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.supplier;

import com.viettel.brcd.ws.common.WebServiceClient;
import com.viettel.brcd.ws.model.output.ConnectSubscriberResponse;

/**
 *
 * @author duyetdk
 */
public class ConnectSubscriberSupplier extends WebServiceClient {
     public ConnectSubscriberResponse processResponse(String isdn, String productCode, String reasonCode, String serial, 
            Long isactive,Long staffId,String type,String cusName,String idNo,String birthDay) throws Exception {

        String xmlText ="<ws:connectSubscriber>"
                    + "<wsRequest>"
                    + "<isdn>" + isdn + "</isdn>"
                    + "<productCode>" + productCode + "</productCode>"
                    + "<serial>" + serial + "</serial>"
                    + "<isActive>" + isactive + "</isActive>"
                    + "<reasonCode>" + reasonCode + "</reasonCode>"
                    + "<staffId>" + staffId + "</staffId>"
                    + "<cusName>" + cusName + "</cusName>"
                    + "<idNumber>" + idNo +"</idNumber>"
                    + "<birthDay>" + birthDay + "</birthDay>"
                    + "<type>" + type + "</type>"
                    + "</wsRequest>"
                    + "</ws:connectSubscriber>";
        return (ConnectSubscriberResponse) sendRequestViaBccsGW(xmlText, "connectSubscriber", ConnectSubscriberResponse.class);
     }
}
