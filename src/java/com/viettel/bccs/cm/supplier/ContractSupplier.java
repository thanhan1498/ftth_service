package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.Contract;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ContractSupplier extends BaseSupplier {

    
     public ContractSupplier() {
        logger = Logger.getLogger(ContractSupplier.class);
    }

    public List<Contract> findById(Session cmPosSession, Long contractId, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Contract where contractId = :contractId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("contractId", contractId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Contract> result = query.list();
        return result;
    }

    public List<Contract> findByCustId(Session cmPosSession, Long customerId, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Contract where custId = :custId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("custId", customerId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Contract> result = query.list();
        return result;
    }

    public String findIsdnByContractId(Session cmPosSession, Long contractId) {
        StringBuilder sql = new StringBuilder().append(" from Contract where contractId = :contractId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("contractId", contractId);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Contract> result = query.list();
        return result.get(0).getTelMobile();
    }
}
