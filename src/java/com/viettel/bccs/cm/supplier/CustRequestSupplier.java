package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.CustRequest;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class CustRequestSupplier extends BaseSupplier {

    public CustRequestSupplier() {
        logger = Logger.getLogger(CustRequestSupplier.class);
    }

    public List<CustRequest> findById(Session cmPosSession, Long custRequestId) {
        StringBuilder sql = new StringBuilder().append(" from CustRequest where custRequestId = :custRequestId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("custRequestId", custRequestId);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<CustRequest> result = query.list();
        return result;
    }

    public CustRequest update(Session cmPosSession, CustRequest custRequest) {
        cmPosSession.save(custRequest);
//        cmPosSession.flush();
        return custRequest;
    }
}
