package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.bussiness.ApParamBussiness;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.Contract;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.CustomerExtend;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.pre.CustomerPre;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.brcd.ws.model.output.CcActionInformation;
import com.viettel.brcd.ws.model.output.VBtsAccount;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class CustomerSupplier extends BaseSupplier {

    public CustomerSupplier() {
        logger = Logger.getLogger(CustomerSupplier.class);
    }

    public List<Customer> findById(Session cmPosSession, Long custId, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Customer where custId = :custId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("custId", custId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Customer> result = query.list();
        return result;
    }

    public List<CustomerPre> findByIdPre(Session cmPreSession, Long custId, Long status) {
        StringBuilder sql = new StringBuilder().append(" from CustomerPre where custId = :custId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("custId", custId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPreSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<CustomerPre> result = query.list();
        return result;
    }

    public void buildQuery(StringBuilder sql, HashMap params, String idNo, String isdn, String account) {
        if (sql == null || params == null) {
            return;
        }
        if (idNo != null) {
            idNo = idNo.trim();
            if (!idNo.isEmpty()) {
                sql.append(" and (upper(cr.id_no) = upper( :idNo ) or upper(cr.bus_permit_no) = upper( :idNo )) ");
                params.put("idNo", idNo);
            }
        }
        isdn = isdn == null ? "" : isdn.trim();
        account = account == null ? "" : account.trim();
        if (!isdn.isEmpty() && !account.isEmpty()) {
            sql.append(" and ").append("(").append(createLike(" s.isdn ", "isdn")).append(" or ").append(createLike(" s.account ", "account")).append(")");
            params.put("isdn", isdn);
            params.put("account", account);
        } else if (!isdn.isEmpty()) {
            sql.append(" and ").append("(").append(createLike(" s.isdn ", "isdn")).append(" or ").append(createLike(" s.account ", "isdn")).append(")");
            params.put("isdn", isdn);
        } else if (!account.isEmpty()) {
            sql.append(" and ").append("(").append(createLike(" s.isdn ", "account")).append(" or ").append(createLike(" s.account ", "account")).append(")");
            params.put("account", account);
        }
    }

    public Long count(Session cmPosSession, String idNo, String isdn, String account) {
        StringBuilder sql = new StringBuilder()
                .append(" select count(*) ")
                .append(" from customer cr, contract c, all_tel_service_sub s ")
                .append(" where cr.cust_id = c.cust_id(+) and c.contract_id = s.contract_id(+) and cr.status = :status ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("status", Constants.STATUS_USE);
        buildQuery(sql, params, idNo, isdn, account);
        Query query = cmPosSession.createSQLQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public List<CustomerExtend> find(Session cmPosSession, String idNo, String isdn, String account, Integer start, Integer max, String customerName, String contact) {
        StringBuilder sql = new StringBuilder()
                .append(" select distinct cr.cust_id custId, cr.bus_type busType, cr.name, cr.birth_date birthDate, cr.id_no idNo, cr.bus_permit_no busPermitNo ")
                .append(" , cr.id_issue_place idIssuePlace, cr.id_issue_date idIssueDate, cr.address, cr.id_type idType, cr.sex, cr.province, cr.district ")
                .append(" , cr.precinct, cr.street_block_name streetBlockName, cr.y_location yLocation, cr.x_location xLocation, NVL( cr.tel_fax , c.tel_mobile ) telFax"
                        + ", sub.task_management_id taskMngtId,sub.cust_lat deployLat,sub.cust_long deployLng ")
                .append(" from customer cr, contract c, all_tel_service_sub s, sub_fb_conf sub ")
                .append(" where cr.cust_id = c.cust_id(+) and c.contract_id = s.contract_id(+) and cr.status = :status and s.account = sub.account(+) "
                        + " and nvl(TYPE_CONFIG,'IMT') = 'IMT' ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("status", Constants.STATUS_USE);
        buildQuery(sql, params, idNo, isdn, account);
        if (customerName != null && !customerName.isEmpty()) {
            sql.append(" and upper(cr.name) like :name ");
            params.put("name", "%" + customerName.toUpperCase() + "%");
        }
        if (contact != null && !contact.isEmpty()) {
            sql.append(" and (c.tel_fax like :telFax or c.tel_mobile like :tetlMobile)   ");
            params.put("telFax", "%" + contact.toUpperCase() + "%");
            params.put("tetlMobile", "%" + contact.toUpperCase() + "%");
        }

        sql.append(" order by cr.name ");

        Query query = cmPosSession.createSQLQuery(sql.toString())
                .addScalar("custId", Hibernate.LONG)
                .addScalar("busType", Hibernate.STRING)
                .addScalar("name", Hibernate.STRING)
                .addScalar("birthDate", Hibernate.TIMESTAMP)
                .addScalar("idNo", Hibernate.STRING)
                .addScalar("busPermitNo", Hibernate.STRING)
                .addScalar("idIssuePlace", Hibernate.STRING)
                .addScalar("idIssueDate", Hibernate.TIMESTAMP)
                .addScalar("address", Hibernate.STRING)
                .addScalar("idType", Hibernate.LONG)
                .addScalar("sex", Hibernate.STRING)
                .addScalar("province", Hibernate.STRING)
                .addScalar("district", Hibernate.STRING)
                .addScalar("precinct", Hibernate.STRING)
                .addScalar("yLocation", Hibernate.STRING)
                .addScalar("xLocation", Hibernate.STRING)
                .addScalar("streetBlockName", Hibernate.STRING)
                .addScalar("telFax", Hibernate.STRING)
                .addScalar("deployLat", Hibernate.STRING)
                .addScalar("deployLng", Hibernate.STRING)
                .addScalar("taskMngtId", Hibernate.LONG)
                .setResultTransformer(Transformers.aliasToBean(CustomerExtend.class));
        buildParameter(query, params);
        if (start != null) {
            query.setFirstResult(start);
        }
        if (max != null) {
            query.setMaxResults(max);
        }
        List<CustomerExtend> result = query.list();
        return result;
    }

    public Long count(Session cmPosSession, String idNo, Long status, String correctCus) {
        StringBuilder sql = new StringBuilder().append(" select count(*) from Customer where upper(idNo) = upper( :idNo ) ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("idNo", idNo);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        if (correctCus != null) {
            sql.append(" and correctCus = :correctCus ");
            params.put("correctCus", correctCus);
        }
        sql.append(" order by name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public List<Customer> find(Session cmPosSession, String idNo, Long status, String correctCus) {
        StringBuilder sql = new StringBuilder().append(" from Customer where upper(idNo) = upper( :idNo )  ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("idNo", idNo);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        if (correctCus != null) {
            sql.append(" and correctCus = :correctCus ");
            params.put("correctCus", correctCus);
        }
        sql.append(" order by name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Customer> result = query.list();
        return result;
    }

    public Customer insert(Session cmPosSession, String busType, String name, Long idType, String custType, String idNo, Date issDate, String idIssuePlace, String province, String district, String precinct, String areaCode, String address, String streetBlock, String streetBlockName, String addedUser, Date nowDate, Date bDate, String sex, String xLocation, String yLocation, String telMobile) {
        Customer cus = new Customer();
        cus.setBusType(busType);
        cus.setName(name);
        cus.setIdType(idType);
        if (!Constants.BUS_TYPE_BUSINESS.equals(custType)) {// Neu la KHDN
            cus.setIdNo(idNo);
            cus.setIdIssueDate(issDate);
            cus.setIdIssuePlace(idIssuePlace);
        } else {
            cus.setBusPermitNo(idNo);
        }
        cus.setProvince(province);
        cus.setDistrict(district);
        cus.setPrecinct(precinct);
        cus.setAreaCode(areaCode);
        address = address == null ? streetBlockName : (streetBlockName == null ? address.trim() : (streetBlockName.trim() + ", " + address));
        cus.setAddress(address);
        cus.setStreetBlock(streetBlock);
        cus.setStreetBlockName(streetBlockName);
        if (addedUser != null) {
            cus.setAddedUser(addedUser.trim().toUpperCase());
        }
        cus.setAddedDate(nowDate);
        cus.setBirthDate(bDate);
        cus.setContactName(name);
        cus.setSex(Constants.MALE.equals(sex) ? "M" : "F");
        cus.setxLocation(xLocation);
        cus.setyLocation(yLocation);
        cus.setTelFax(telMobile);
        cus.setStatus(Constants.STATUS_USE);
        cus.setCorrectCus(Constants.IS_NOT_CORRECT_CUSTOMER);
        Long cusId = getSequence(cmPosSession, Constants.ADD_CUST_SEQ);
        cus.setCustId(cusId);
        cmPosSession.save(cus);
        cmPosSession.flush();
        return cus;
    }

    public Customer update(Session cmPosSession, Customer cus, Long cusId, String busType, String name, Long idType, String custType, String idNo, Date issDate, String idIssuePlace, String province, String district, String precinct, String areaCode, String address, String streetBlock, String streetBlockName, String addedUser, Date nowDate, Date bDate, String sex, String xLocation, String yLocation, String telMobile) {
        cus.setBusType(busType);
        cus.setName(name);
        cus.setIdType(idType);
        if (!Constants.BUS_TYPE_BUSINESS.equals(custType)) {// Neu la KHDN
            cus.setIdNo(idNo);
            cus.setIdIssueDate(issDate);
            cus.setIdIssuePlace(idIssuePlace);
        } else {
            cus.setBusPermitNo(idNo);
        }
        cus.setProvince(province);
        cus.setDistrict(district);
        cus.setPrecinct(precinct);
        cus.setAreaCode(areaCode);
        address = address == null ? streetBlockName : (streetBlockName == null ? address.trim() : (streetBlockName.trim() + ", " + address));
        cus.setAddress(address);
        cus.setStreetBlock(streetBlock);
        cus.setStreetBlockName(streetBlockName);
        if (addedUser != null) {
            cus.setAddedUser(addedUser.trim().toUpperCase());
        }
        cus.setAddedDate(nowDate);
        cus.setBirthDate(bDate);
        cus.setContactName(name);
        cus.setSex(Constants.MALE.equals(sex) ? "M" : "F");
        List<ApParam> listApParam = new ApParamBussiness().findByType(cmPosSession, Constants.FLAG_NEW_FLOW_UPDATE_LOCATION);
        if (listApParam != null && !listApParam.isEmpty() && listApParam.get(0).getParamValue().equals(Constants.STATUS_USE.toString())) {
            if (cus.getxLocation() == null || cus.getyLocation() == null
                    || cus.getxLocation().isEmpty() || cus.getyLocation().isEmpty()) {
                cus.setxLocation(xLocation);
                cus.setyLocation(yLocation);
            }
        } else {
            cus.setxLocation(xLocation);
            cus.setyLocation(yLocation);
        }

        cus.setTelFax(telMobile);
        cus.setStatus(Constants.STATUS_USE);
        cus.setCorrectCus(Constants.CORRECT_CUSTOMER);
        cus.setCustId(cusId);
        cmPosSession.update(cus);
        cmPosSession.flush();
        return cus;
    }

    public Customer update(Session cmPosSession, Customer cus, String xLocation, String yLocation, String userUpdate) {
        cus.setxLocation(xLocation);
        cus.setyLocation(yLocation);
        cus.setUpdatedTime(new Date());
        cus.setUpdatedUser(userUpdate.trim().toUpperCase());
        cmPosSession.update(cus);
        cmPosSession.flush();
        return cus;
    }

    public List<Customer> find(Session cmPreSession, String idNo, String isdn, String service) throws Exception {
        String queryString = null;
        List<Customer> lstCustInfo = new ArrayList();
        List colParameter = new ArrayList();
        if (isdn == null || "".equals(isdn)) {
            queryString = " select distinct cr.cust_id custId, cr.bus_type busType, cr.name, cr.birth_date birthDate, cr.id_no idNo, cr.bus_permit_no busPermitNo "
                    + " , cr.id_issue_place idIssuePlace, cr.id_issue_date idIssueDate, cr.address, cr.id_type idType, cr.sex, cr.province, cr.district "
                    + " , cr.precinct, cr.street_block_name streetBlockName "
                    + " from customer cr "
                    + " where cr.status = " + Constants.STATUS_USE;
            if (idNo != null && !idNo.trim().equals("")) {
                queryString = queryString + " and lower(id_No)= ?";
                colParameter.add(idNo.trim().toLowerCase());
            }
            queryString += " and (correct_Cus = ? or rownum <= ?)";
            colParameter.add(Constants.CORRECT_CUSTOMER);
            colParameter.add(Long.valueOf(1000));
            Query query = cmPreSession.createSQLQuery(queryString.toString())
                    .addScalar("custId", Hibernate.LONG)
                    .addScalar("busType", Hibernate.STRING)
                    .addScalar("name", Hibernate.STRING)
                    .addScalar("birthDate", Hibernate.TIMESTAMP)
                    .addScalar("idNo", Hibernate.STRING)
                    .addScalar("busPermitNo", Hibernate.STRING)
                    .addScalar("idIssuePlace", Hibernate.STRING)
                    .addScalar("idIssueDate", Hibernate.TIMESTAMP)
                    .addScalar("address", Hibernate.STRING)
                    .addScalar("idType", Hibernate.LONG)
                    .addScalar("sex", Hibernate.STRING)
                    .addScalar("province", Hibernate.STRING)
                    .addScalar("district", Hibernate.STRING)
                    .addScalar("precinct", Hibernate.STRING)
                    .addScalar("streetBlockName", Hibernate.STRING)
                    .setResultTransformer(Transformers.aliasToBean(Customer.class));
            for (int i = 0; i < colParameter.size(); i++) {
                query.setParameter(i, colParameter.get(i));
            }
            lstCustInfo = query.list();
        } else {
            queryString = " select distinct cr.cust_id custId, cr.bus_type busType, cr.name, cr.birth_date birthDate, cr.id_no idNo, cr.bus_permit_no busPermitNo "
                    + " , cr.id_issue_place idIssuePlace, cr.id_issue_date idIssueDate, cr.address, cr.id_type idType, cr.sex, cr.province, cr.district "
                    + " , cr.precinct, cr.street_block_name streetBlockName "
                    + " from customer cr, All_Cust_Sub a "
                    + " where cr.status = " + Constants.STATUS_USE;
            queryString += " and cr.cust_Id = a.cust_Id ";
            if (idNo != null && !idNo.trim().equals("")) {
                queryString = queryString + " and lower(cr.id_No)= ?";
                colParameter.add(idNo.trim().toLowerCase());
            }
            if (service != null && !service.isEmpty()) {
                queryString = queryString + " AND a.service_Type = ? ";
                colParameter.add(getServiceAliasByServiceId(Long.parseLong(service)));

                // Xu ly TH nhap so 0 o dau so ISDN
                if (isdn.trim().charAt(0) == '0') {
                    isdn = isdn.trim().substring(1);
                }
                queryString = queryString + " AND a.isdn = ? ";
                colParameter.add(isdn.trim());
                // sua chi tim kiem nhung thue bao dang hoat dong
                queryString = queryString + " AND a.status_Id = ? ";
                colParameter.add(Constants.SUB_STATUS_NORMAL_ACTIVED);
                if (Constants.SERVICE_MOBILE_ID_WEBSERVICE.toString().equals(service)) {
                    queryString = queryString + " AND a.last_Number = ? ";
                    colParameter.add(isdn.trim().substring(isdn.trim().length() - 1));
                }
            }
            queryString += " and (cr.correct_Cus = ? or rownum <= ?)";
            colParameter.add(Constants.CORRECT_CUSTOMER);
            colParameter.add(Long.valueOf(1000));
            Query query = cmPreSession.createSQLQuery(queryString.toString())
                    .addScalar("custId", Hibernate.LONG)
                    .addScalar("busType", Hibernate.STRING)
                    .addScalar("name", Hibernate.STRING)
                    .addScalar("birthDate", Hibernate.TIMESTAMP)
                    .addScalar("idNo", Hibernate.STRING)
                    .addScalar("busPermitNo", Hibernate.STRING)
                    .addScalar("idIssuePlace", Hibernate.STRING)
                    .addScalar("idIssueDate", Hibernate.TIMESTAMP)
                    .addScalar("address", Hibernate.STRING)
                    .addScalar("idType", Hibernate.LONG)
                    .addScalar("sex", Hibernate.STRING)
                    .addScalar("province", Hibernate.STRING)
                    .addScalar("district", Hibernate.STRING)
                    .addScalar("precinct", Hibernate.STRING)
                    .addScalar("streetBlockName", Hibernate.STRING)
                    .setResultTransformer(Transformers.aliasToBean(Customer.class));
            for (int i = 0; i < colParameter.size(); i++) {
                query.setParameter(i, colParameter.get(i));
            }
            lstCustInfo = query.list();
        }
        return lstCustInfo;
    }

    public Long countPre(Session cmPreSession, String idNo, Long status, String correctCus) {
        StringBuilder sql = new StringBuilder().append(" select count(*) from cm_pre2.Customer where upper(id_No) = upper( :idNo ) ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("idNo", idNo);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        if (correctCus != null) {
            sql.append(" and correct_Cus = :correctCus ");
            params.put("correctCus", correctCus);
        }
        sql.append(" order by name ");
        Query query = cmPreSession.createSQLQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public static String getServiceAliasByServiceId(Long teleServiceId) throws Exception {
        String serviceType = "";
        if (teleServiceId == null) {
            return serviceType;
        }

        //if(teleServiceId.equals(Constant.SERVICE_MOBILE_ID))
        if (teleServiceId.equals(1L)) //fix tạm để chạy, vì trong ApDomain bây h trường code của Mobile đang là 1
        {
            serviceType = Constants.SERVICE_ALIAS_MOBILE;
        } //else if(teleServiceId.equals(Constant.SERVICE_HOMEPHONE_ID))
        else if (teleServiceId.equals(5L)) //fix tạm để chạy, vì trong ApDomain bây h trường code của Homephone đang là 5
        {
            serviceType = Constants.SERVICE_ALIAS_HOMEPHONE;
        }
        return serviceType;
    }

    public CustomerPre insertPre(Session cmPreSession, String busType, String name, Long idType, String custType, String idNo, Date issDate, String idIssuePlace, String province, String district, String precinct, String areaCode, String address, String streetBlock, String streetBlockName, String addedUser, Date nowDate, Date bDate, String sex, String telMobile) {
        CustomerPre cus = new CustomerPre();
        cus.setBusType(busType);
        cus.setName(name);
        cus.setIdType(idType);
        if (!Constants.BUS_TYPE_BUSINESS.equals(custType)) {// Neu la KHDN
            cus.setIdNo(idNo);
            cus.setIdIssueDate(issDate);
            cus.setIdIssuePlace(idIssuePlace);
        } else {
            cus.setBusPermitNo(idNo);
        }
        cus.setProvince(province);
        cus.setDistrict(district);
        cus.setPrecinct(precinct);
        cus.setAreaCode(areaCode);
        address = address == null ? streetBlockName : (streetBlockName == null ? address.trim() : (streetBlockName.trim() + ", " + address));
        cus.setAddress(address);
        cus.setStreetName(streetBlock);
        cus.setStreetBlockName(streetBlockName);
        cus.setTelFax(telMobile);
        if (addedUser != null) {
            cus.setAddedUser(addedUser.trim().toUpperCase());
        }
        cus.setAddedDate(nowDate);
        cus.setBirthDate(bDate);
        cus.setSex(Constants.MALE.equals(sex) ? "M" : "F");
        cus.setStatus(Constants.STATUS_USE);
        cus.setCorrectCus(Constants.IS_NOT_CORRECT_CUSTOMER);
        Long cusId = getSequence(cmPreSession, Constants.SEQ_CUSTOMER);
        cus.setCustId(cusId);
        cmPreSession.save(cus);
        cmPreSession.flush();
        return cus;
    }

    public CustomerPre updatePre(Session cmPreSession, CustomerPre cus, Long cusId, String busType, String name, Long idType, String custType, String idNo, Date issDate, String idIssuePlace, String province, String district, String precinct, String areaCode, String address, String streetBlock, String streetBlockName, String addedUser, Date nowDate, Date bDate, String sex, String telMobile) {
        cus.setBusType(busType);
        cus.setName(name);
        cus.setIdType(idType);
        if (!Constants.BUS_TYPE_BUSINESS.equals(custType)) {// Neu la KHDN
            cus.setIdNo(idNo);
            cus.setIdIssueDate(issDate);
            cus.setIdIssuePlace(idIssuePlace);
        } else {
            cus.setBusPermitNo(idNo);
        }
        cus.setProvince(province);
        cus.setDistrict(district);
        cus.setPrecinct(precinct);
        cus.setAreaCode(areaCode);
        address = address == null ? streetBlockName : (streetBlockName == null ? address.trim() : (streetBlockName.trim() + ", " + address));
        cus.setAddress(address);
        cus.setStreetName(streetBlock);
        cus.setTelFax(telMobile);
        cus.setStreetBlockName(streetBlockName);
        if (addedUser != null) {
            cus.setAddedUser(addedUser.trim().toUpperCase());
        }
        cus.setAddedDate(nowDate);
        cus.setBirthDate(bDate);
        cus.setSex(Constants.MALE.equals(sex) ? "M" : "F");
        cus.setStatus(Constants.STATUS_USE);
        cus.setCorrectCus(Constants.IS_NOT_CORRECT_CUSTOMER);
        cus.setCustId(cusId);
        cmPreSession.save(cus);
        cmPreSession.flush();
        return cus;
    }

    /*
     * @Author:DuyetDK
     * @Since: 23/01/2018
     * @Desc: find account in sub_adsl_ll by custId
     */
    public List<SubAdslLeaseline> findByCustId(Session cmPosSession, Long custId) {
        StringBuilder sql = new StringBuilder().append(" select s.account from Customer cu, Contract co, SubAdslLeaseline s  "
                + " where cu.custId = co.custId and co.contractId = s.contractId and cu.custId = :custId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("custId", custId);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<SubAdslLeaseline> result = query.list();
        return result;
    }

    /*
     * @Author:DuyetDK
     * @Since: 23/01/2018
     * @Desc: find isdn in Contract by custId
     */
    public List<Contract> findContractByCustId(Session cmPosSession, Long custId) {
        StringBuilder sql = new StringBuilder().append(" select co.mainIsdn from Customer cu, Contract co  "
                + " where cu.custId = co.custId and cu.custId = :custId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("custId", custId);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Contract> result = query.list();
        return result;
    }

    /*
     * @Author:DuyetDK
     * @Since: 09/02/2018
     * @Desc: find lat long in bts by custId
     */
    public List<VBtsAccount> findCoorByCustId(Session cmPosSession, Long custId) {
        StringBuilder sql = new StringBuilder().append(" select v.cust_Id custId,v.contract_Id contractId,v.account account,v.latth latth,v.longth longth from v_bts_account v where v.cust_Id = :custId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("custId", custId);
        Query query = cmPosSession.createSQLQuery(sql.toString())
                .addScalar("custId", Hibernate.LONG)
                .addScalar("contractId", Hibernate.LONG)
                .addScalar("account", Hibernate.STRING)
                .addScalar("latth", Hibernate.STRING)
                .addScalar("longth", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(VBtsAccount.class));
        buildParameter(query, params);
        List<VBtsAccount> result = query.list();
        return result;
    }

    /**
     * @since 182303
     * @author cuongdm
     * @des: kiem tra xem thue bao co dau noi theo chinh sach moi CDBR
     * @param session
     * @param contractId
     * @return
     */
    public boolean isCheckNewConnect(Session session, Long contractId) {
        return false;
        /*
        String sql = "SELECT 1 "
                + " FROM sub_adsl_ll s "
                + " JOIN reason r "
                + " ON s.reg_type = r.code "
                + " JOIN map_active_info m "
                + " ON r.reason_id = m.reg_reason_id "
                + " AND (m.province_code = s.province or m.province_code ='-1') "
                + " AND m.product_code = s.product_code "
                + " WHERE s.contract_id=?"
                + " AND m.status = 1"
                + " AND m.deposit is not null ";
        Query query = session.createSQLQuery(sql);
        query.setParameter(0, contractId);
        List<String> list = query.list();
        return list.size() > 0;*/

    }

    public List<CcActionInformation> getComplainHistory(Session sessionCC, String isdn) {
        String sql = "Select to_char(ACCEPT_DATE, 'dd/mm/yyyy HH24:MI:SS') as dates,  "
                + "       COMP_CONTENT as content,  "
                + "       ct.name as compType,  "
                + "       ACCEPT_USER as acceptUser,  "
                + "       RESULT_CONTENT as resultContent,  "
                + "       complain_id as complainId,  "
                + "       decode(status,  "
                + "              0,  "
                + "              'New',  "
                + "              1,  "
                + "              'Processing',  "
                + "              2,  "
                + "              'Finish',  "
                + "              3,  "
                + "              'Close',  "
                + "              'error') as status,  "
                + "       status as statusCode,  "
                + "       note as note,  "
                + "       end_user as endUser,  "
                + "       To_char(cust_limit_date, 'dd/mm/yyyy HH24:MI:SS') as custLimitDate,  "
                + "       To_char(res_limit_date, 'dd/mm/yyyy HH24:MI:SS') as resLimitDate,  "
                + "       cl.name as compLevel,  "
                + "       isdn as subNo,  "
                + "       to_char(cp.comp_type_id) compTypeId,  "
                + "       to_char(cp.priority_id) priorityId,  "
                + "       to_char(limit_cust_date, 'dd/mm/yyyy HH24:MI:SS') as limitCustDate,  "
                + "       (select name  "
                + "          from comp_cause cce  "
                + "         where cce.comp_cause_id = cp.comp_cause_id) as compCauseName,  "
                + "      to_char(ACCEPT_DATE, 'dd/mm/yyyy hh24:mi:ss') acceptDate,  "
                + "       '0' typeAgent  "
                + "  from complain cp, comp_level cl, COMP_TYPE ct  "
                + " where ct.COMP_TYPE_ID = cp.COMP_TYPE_ID  "
                + "   and cp.comp_level_id = cl.comp_level_id  "
                + "   and ACCEPT_DATE >= trunc(sysdate - 90)  "
                + "   and status <> 4  "
                + "   and isdn = ?  "
                + "union all  "
                + "Select to_char(ACCEPT_DATE, 'dd/mm/yyyy HH24:MI:SS') as dates,  "
                + "       COMP_CONTENT as content,  "
                + "       ct.name as compType,  "
                + "       ACCEPT_USER as acceptUser,  "
                + "       RESULT_CONTENT as resultContent,  "
                + "       complain_id as complainId,  "
                + "       decode(status,  "
                + "              0,  "
                + "              'New',  "
                + "              1,  "
                + "              'Processing',  "
                + "              2,  "
                + "              'Finish',  "
                + "              3,  "
                + "              'Close',  "
                + "              'error') as status,  "
                + "       status as statusCode,  "
                + "       note as note,  "
                + "       end_user as endUser,  "
                + "       To_char(cust_limit_date, 'dd/mm/yyyy HH24:MI:SS') as custLimitDate,  "
                + "       To_char(res_limit_date, 'dd/mm/yyyy HH24:MI:SS') as resLimitDate,  "
                + "       cl.name as compLevel,  "
                + "       isdn as subNo,  "
                + "       to_char(cp.comp_type_id) compTypeId,  "
                + "       to_char(cp.priority_id) priorityId,  "
                + "       to_char(limit_cust_date, 'dd/mm/yyyy HH24:MI:SS') as limitCustDate,  "
                + "       (select name  "
                + "          from comp_cause cce  "
                + "         where cce.comp_cause_id = cp.comp_cause_id) as compCauseName,  "
                + "       to_char(ACCEPT_DATE, 'dd/mm/yyyy hh24:mi:ss') acceptDate,  "
                + "       '1' typeAgent  "
                + "  from complain_agent cp, comp_level cl, COMP_TYPE ct  "
                + " where ct.COMP_TYPE_ID = cp.COMP_TYPE_ID  "
                + "   and cp.comp_level_id = cl.comp_level_id  "
                + "   and ACCEPT_DATE >= trunc(sysdate - 90)  "
                + "   and status <> 4  "
                + "   and isdn = ?  "
                + "   order by dates desc ";
        Query query = sessionCC.createSQLQuery(sql)
                .addScalar("dates", Hibernate.STRING)
                .addScalar("content", Hibernate.STRING)
                .addScalar("compType", Hibernate.STRING)
                .addScalar("acceptUser", Hibernate.STRING)
                .addScalar("resultContent", Hibernate.STRING)
                .addScalar("complainId", Hibernate.LONG)
                .addScalar("status", Hibernate.STRING)
                .addScalar("statusCode", Hibernate.STRING)
                .addScalar("note", Hibernate.STRING)
                .addScalar("endUser", Hibernate.STRING)
                .addScalar("custLimitDate", Hibernate.STRING)
                .addScalar("resLimitDate", Hibernate.STRING)
                .addScalar("compLevel", Hibernate.STRING)
                .addScalar("subNo", Hibernate.STRING)
                .addScalar("compTypeId", Hibernate.STRING)
                .addScalar("priorityId", Hibernate.STRING)
                .addScalar("limitCustDate", Hibernate.STRING)
                .addScalar("compCauseName", Hibernate.STRING)
                .addScalar("acceptDate", Hibernate.STRING)
                .addScalar("typeAgent", Hibernate.STRING)
                //.addScalar("numberComplain", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(CcActionInformation.class));
        query.setParameter(0, isdn);
        query.setParameter(1, isdn);
        //query.setParameter(2, isdn);
        //query.setParameter(3, isdn);
        return query.list();
    }

    public List<CcActionInformation> getBillingCollection(Session payment, Long contractId) {
        String sql = "SELECT to_char(md.receipt_date, 'dd/mm/yyyy hh24:mi:ss') as appliedCycle, "
                + "       (SELECT name FROM payment_type WHERE code = md.payment_type) collectionStaffTel, "
                + "       (SELECT name "
                + "          FROM collection_staff "
                + "         WHERE collection_staff_id = md.collection_staff_id) as collectionStaffName, "
                + "       (SELECT COLLECTION_STAFF_CODE "
                + "          FROM collection_staff "
                + "         WHERE collection_staff_id = md.collection_staff_id) as collectionStaffCode, "
                + "       (SELECT name "
                + "          FROM collection_group "
                + "         WHERE collection_group_id = md.collection_group_id) as collectionGroupName "
                + "  FROM payment_contract md "
                + "  LEFT JOIN contract_remain_audit cra "
                + "    ON md.payment_id = cra.payment_id "
                + "   AND md.create_date = cra.create_date "
                + "   AND cra.type <> 6 "
                + " WHERE md.contract_id = ? "
                + " ORDER BY md.applied_cycle DESC, md.receipt_date DESC, md.create_date DESC";
        Query query = payment.createSQLQuery(sql)
                .addScalar("appliedCycle", Hibernate.STRING)
                .addScalar("collectionGroupName", Hibernate.STRING)
                .addScalar("collectionStaffName", Hibernate.STRING)
                .addScalar("collectionStaffTel", Hibernate.STRING)
                .addScalar("collectionStaffCode", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(CcActionInformation.class));
        query.setLong(0, contractId);
        return query.list();
    }

    //phuonghc 18092020 - select object sub_adsl_ll
    public List<SubAdslLeaseline> findObjectByCustId(Session cmPosSession, Long custId, String account) {
        String sql = " select s from Customer cu, Contract co, SubAdslLeaseline s"
                + " where cu.custId = co.custId and co.contractId = s.contractId and cu.custId=:custId "
                + " and s.account=:account";
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("custId", custId);
        params.put("account", account);
        Query query = cmPosSession.createQuery(sql);
        buildParameter(query, params);
        List<SubAdslLeaseline> result = query.list();
        return result;
    }

}
