package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.Domain;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class DomainSupplier extends BaseSupplier {

    public DomainSupplier() {
        logger = Logger.getLogger(DomainSupplier.class);
    }

    public List<Domain> findByStatus(Session cmPosSession, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Domain where status = :status ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("status", status);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Domain> result = query.list();
        return result;
    }
}
