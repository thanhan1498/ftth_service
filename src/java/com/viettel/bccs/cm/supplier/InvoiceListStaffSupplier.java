/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.supplier;

import com.viettel.database.config.BaseHibernateDAOMDB;
import com.viettel.bccs.cm.common.util.Constant;
import java.util.Date;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author bongcm
 */
public class InvoiceListStaffSupplier extends BaseHibernateDAOMDB {

    public static void insertInvoiceListStaffSupplier(Session cmPosSession, Long staffId, String staffCode, String currInvoiceNo, String regType, Double payAdvAmount, Date insertDate, Long contractId, Long invoiceListId) throws Exception {

        String sql = "INSERT INTO INVOICE_LIST_STAFF_LOG ("
                + "ID"
                + ",INSERT_DATE"
                + ",CURR_INVOICE_NO"
                + ",REG_TYPE"
                + ",STAFF_CODE"
                + ",STAFF_ID"
                + ",PAY_ADV_AMOUNT"
                + ",CONTRACT_ID"
                + ",INVOICE_LIST_ID"
                + ",STATUS) "
                + "VALUES("
                + "INVOICE_LIST_STAFF_SEQ.nextval"
                + ",sysdate"
                + ",?"
                + ",?"
                + ",?"
                + ",?"
                + ",?"
                + ",?"
                + ",?"
                + ",?)";
        Query query = cmPosSession.createSQLQuery(sql);
        query.setParameter(0,currInvoiceNo);
        query.setParameter(1,regType);
        query.setParameter(2,staffCode);
        query.setParameter(3,staffId);
        query.setParameter(4,payAdvAmount);
        query.setParameter(5,contractId);
        query.setParameter(6,invoiceListId);
        query.setParameter(7,Constant.STATUS_USE);

    }

    /**
     * minhtramv. 151222. Insert invoice list staff to payment.
     *
     * @param paymentSession
     * @param staffId
     * @param staffCode
     * @param currInvoiceNo
     * @param regType
     * @param payAdvAmount
     * @param insertDate
     * @param contractId
     * @param invoiceListId
     * @throws Exception
     */
    public static void insertInvoiceListStaffPayment(Session paymentSession,
            Long staffId, String staffCode, String currInvoiceNo, String regType,
            Double payAdvAmount, Date insertDate, Long contractId, Long invoiceListId, Double promotion) throws Exception {

        String sql = "INSERT INTO invoice_list_staff (ID,STAFF_ID,STAFF_CODE,CURR_INVOICE_NO,"
                + " REG_TYPE,INSERT_DATE,CONTRACT_ID,STATUS,INVOICE_LIST_ID,PAY_ADV_AMOUNT,PROMOTION,COLLECTION_STAFF_CODE) "
                + "VALUES(INVOICE_LIST_STAFF_SEQ.nextval,?,?,?,?,sysdate,?,?,?,?,?,?)";

        Query query = paymentSession.createSQLQuery(sql);
        query.setParameter(0, staffId);
        query.setParameter(1, staffCode);
        query.setParameter(2, currInvoiceNo);
        query.setParameter(3, regType);
        query.setParameter(4, contractId);
        query.setParameter(5, Constant.STATUS_USE);
        query.setParameter(6, invoiceListId);
        query.setParameter(7, payAdvAmount);
        query.setParameter(8, promotion == null ? "" : promotion);
        query.setParameter(9, null);
        System.out.println(">>========>promotion submit: " + (promotion == null ? "null" : promotion));
        query.executeUpdate();
    }

    /**
     * Insert invoice list staff to payment.
     *
     * @param paymentSession
     * @param staffId
     * @param staffCode
     * @param currInvoiceNo
     * @param regType
     * @param payAdvAmount
     * @param insertDate
     * @param contractId
     * @param invoiceListId
     * @param collectionStaffCode
     * @throws Exception
     */
    public static void insertInvoiceListStaff(Session paymentSession,
            Long staffId, String staffCode, String currInvoiceNo, String regType,
            Double payAdvAmount, Date insertDate, Long contractId, Long invoiceListId, Double promotion, String collectionStaffCode, String tid) throws Exception {

        String sql = "INSERT INTO invoice_list_staff (ID,STAFF_ID,STAFF_CODE,CURR_INVOICE_NO,"
                + " REG_TYPE,INSERT_DATE,CONTRACT_ID,STATUS,INVOICE_LIST_ID,PAY_ADV_AMOUNT,PROMOTION,COLLECTION_STAFF_CODE, TRANSACTION_ID) "
                + "VALUES(INVOICE_LIST_STAFF_SEQ.nextval,?,?,?,?,sysdate,?,?,?,?,?,?, ?)";
        Query query = paymentSession.createSQLQuery(sql);
        query.setParameter(0, staffId);
        query.setParameter(1, staffCode);
        query.setParameter(2, currInvoiceNo);
        query.setParameter(3, regType);
        query.setParameter(4, contractId);
        query.setParameter(5, Constant.STATUS_USE);
        query.setParameter(6, invoiceListId);
        query.setParameter(7, payAdvAmount);
        query.setParameter(8, promotion == null ? "" : promotion);
        query.setParameter(9, collectionStaffCode);
        query.setParameter(10, tid);
        System.out.println(">>========>promotion submit: " + (promotion == null ? "null" : promotion));
        query.executeUpdate();
    }

    /**
     * Insert invoice list staff log.
     *
     * @param cmPosSession
     * @param staffId
     * @param staffCode
     * @param currInvoiceNo
     * @param regType
     * @param payAdvAmount
     * @param insertDate
     * @param contractId
     * @param invoiceListId
     * @param collectionStaffCode
     * @throws Exception
     */
    public static void insertInvoiceListStaffLog(Session cmPosSession, Long staffId, String staffCode, String currInvoiceNo, String regType, Double payAdvAmount, Date insertDate, Long contractId, Long invoiceListId, String collectionStaffCode) throws Exception {
        String sql = "INSERT INTO INVOICE_LIST_STAFF_LOG ("
                + "ID"
                + ",INSERT_DATE"
                + ",CURR_INVOICE_NO"
                + ",REG_TYPE"
                + ",STAFF_CODE"
                + ",STAFF_ID"
                + ",PAY_ADV_AMOUNT"
                + ",CONTRACT_ID"
                + ",INVOICE_LIST_ID"
                + ",STATUS"
                + ",COLLECTION_STAFF_CODE) "
                + "VALUES("
                + "INVOICE_LIST_STAFF_SEQ.nextval"
                + ",sysdate"
                + ",?"
                + ",?"
                + ",?"
                + ",?"
                + ",?"
                + ",?"
                + ",?"
                + ",?"
                + ",?)";
        Query query = cmPosSession.createSQLQuery(sql);
        query.setParameter(0,currInvoiceNo);
        query.setParameter(1,regType);
        query.setParameter(2,staffCode);
        query.setParameter(3,staffId);
        query.setParameter(4,payAdvAmount);
        query.setParameter(5,contractId);
        query.setParameter(6,invoiceListId);
        query.setParameter(7,Constant.STATUS_USE);
        query.setParameter(8,collectionStaffCode);
        
        query.executeUpdate();
    }
}
