package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.ProductPricePlan;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ProductPricePlanSupplier extends BaseSupplier {

    public ProductPricePlanSupplier() {
        logger = Logger.getLogger(ProductPricePlanSupplier.class);
    }

    public List<ProductPricePlan> findById(Session cmPosSession, Long id, String status) {
        StringBuilder sql = new StringBuilder().append(" from ProductPricePlan where id = :id ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("id", id);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<ProductPricePlan> result = query.list();
        return result;
    }

    public List<ProductPricePlan> findByOfferId(Session cmPosSession, Long offerId, String status) {
        StringBuilder sql = new StringBuilder().append(" from ProductPricePlan where offerId = :offerId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("offerId", offerId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<ProductPricePlan> result = query.list();
        return result;
    }
}
