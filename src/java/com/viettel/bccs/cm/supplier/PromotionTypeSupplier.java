package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.PromotionType;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class PromotionTypeSupplier extends BaseSupplier {

    public PromotionTypeSupplier() {
        logger = Logger.getLogger(PromotionTypeSupplier.class);
    }

    public List<PromotionType> findByService(Session cmPosSession, String telService) {
        StringBuilder sql = new StringBuilder()
                .append(" from PromotionType ")
                .append(" where staDate <= sysdate and (endDate is null or endDate >= sysdate) and ").append(createLike(" telService ", "telService"));
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("telService", formatLike(telService));
        sql.append(" order by name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<PromotionType> result = query.list();
        return result;
    }
}
