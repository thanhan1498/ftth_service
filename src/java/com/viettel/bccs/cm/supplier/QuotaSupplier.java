package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.Quota;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class QuotaSupplier extends BaseSupplier {

    public QuotaSupplier() {
        logger = Logger.getLogger(QuotaSupplier.class);
    }

    public List<Quota> findById(Session cmPosSession, Long quotaId, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Quota where quotaId = :quotaId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("quotaId", quotaId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Quota> result = query.list();
        return result;
    }

    public List<Quota> findByService(Session cmPosSession, String serviceType, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Quota where serviceType = :serviceType ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("serviceType", serviceType);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by length(quotaValue), quotaValue ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Quota> result = query.list();
        return result;
    }
}
