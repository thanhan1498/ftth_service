package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.dao.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.dao.SubDepositAdslLlDAO;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.MapActiveInfo;
import com.viettel.bccs.cm.model.Mapping;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.pre.ReasonPre;
import com.viettel.bccs.cm.supplier.payment.PaymentSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.sync.api.Constant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class ReasonSupplier extends BaseSupplier {

    public ReasonSupplier() {
        logger = Logger.getLogger(ReasonSupplier.class);
    }

    public List<Reason> findById(Session cmPosSession, Long reasonId, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Reason where reasonId = :reasonId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("reasonId", reasonId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Reason> result = query.list();
        return result;
    }

    public List<Reason> findByCode(Session cmPosSession, String code, Long status) {
        StringBuilder sql = new StringBuilder()
                .append(" from Reason where code = :code ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("code", code);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Reason> result = query.list();
        return result;
    }

    public List<Reason> findByTypeCode(Session cmPosSession, String type, String code, Long status) {
        StringBuilder sql = new StringBuilder()
                .append(" from Reason where type = :type and code = :code ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("type", type);
        params.put("code", code);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Reason> result = query.list();
        return result;
    }

    public List<Reason> getRegTypes(Session cmPosSession, String productCode, String telService) {
        StringBuilder sql = new StringBuilder()
                .append(" select r from Reason r, Mapping m ")
                .append(" where r.reasonId = m.reasonId and r.status = :status and r.type = :type and ").append(createLike(" r.telService ", "telService"))
                .append(" and m.status = :status and (m.productCode = :productCode or m.productCode is null) ")
                .append(" order by r.name ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("status", Constants.STATUS_USE);
        params.put("type", Constants.REASON_TYPE_REG_TYPE_NEW);
        params.put("telService", formatLike(telService));
        params.put("productCode", productCode);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Reason> result = query.list();
        return result;
    }

    public List<Reason> getRegTypes(Session cmPosSession, Long channelTypeId, String provinceCode, String productCode, Long telServiceId, String modelType) {
        StringBuilder sql = new StringBuilder()
                .append(" select r from Reason r, MapActiveInfo ma, Mapping m ")
                .append(" where (r.reasonId = ma.regReasonId or ma.regReasonId = :allValue) ")
                .append(" and r.reasonId = m.reasonId and m.status = :status ")
                .append(" and (m.productCode = :productCode or m.productCode is null) ")
                .append(" and r.status = :status and r.type = :type and ma.status = :status and ma.telServiceId = :telServiceId ")
                .append(" and ma.productCode = :productCode and ma.modelType = :modelType ")
                .append(" and ma.effectDate <= trunc(sysdate) and (ma.endDate is null or ma.endDate >= trunc(sysdate)) ")
                .append(" and (ma.channelTypeId = :channelTypeId or ma.channelTypeId = :allValue) ")
                .append(" and (ma.provinceCode = :provinceCode or ma.provinceCode = :allValueStr) ")
                .append(" order by r.name ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("allValue", Constants.ALL_VALUE);
        params.put("status", Constants.STATUS_USE);
        params.put("productCode", productCode);
        params.put("type", Constants.REASON_TYPE_REG_TYPE_NEW);
        params.put("telServiceId", telServiceId);
        params.put("modelType", modelType);
        params.put("channelTypeId", channelTypeId);
        params.put("provinceCode", provinceCode);
        params.put("allValueStr", Constants.ALL_VALUE.toString());
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Reason> result = query.list();
        return result;
    }

    public List<Reason> find(Session cmPosSession, String actionCode, String telService, String productCode) {
        StringBuilder sql = new StringBuilder()
                .append(" select r from Reason r, Mapping m ")
                .append(" where r.reasonId = m.reasonId and r.status = :status and m.status = :status and m.actionCode = :actionCode ")
                .append(" and (m.productCode = :productCode or m.productCode is null) ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("status", Constants.STATUS_USE);
        params.put("actionCode", actionCode);
        params.put("productCode", productCode);
        if (telService != null) {
            telService = telService.trim();
            if (!telService.isEmpty()) {
                sql.append(" and ").append(createLike(" r.telService ", "telService"));
                params.put("telService", formatLike(telService));
            }
        }
        sql.append(" order by r.name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Reason> result = query.list();
        return result;
    }

    public List<Reason> find(Session cmPosSession, String actionCode, String telService) {
        StringBuilder sql = new StringBuilder()
                .append(" select r from Reason r, ApDomain a ")
                .append(" where a.type = :actionType and a.reasonType = r.type and a.code = :actionCode and a.status = :status and r.status = :status ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("actionType", Constants.ACTION_FOR_AUDIT);
        params.put("actionCode", actionCode);
        params.put("status", Constants.STATUS_USE);
        if (telService != null) {
            telService = telService.trim();
            if (!telService.isEmpty()) {
                sql.append(" and ").append(createLike(" r.telService ", "telService"));
                params.put("telService", formatLike(telService));
            }
        }
        sql.append(" order by r.name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Reason> result = query.list();
        return result;
    }

    public List<ReasonPre> getAllRegTypeListByUser(Session cmSession, String userName, String provinceCode, String districtCode, String productCode, Long chanelTypeId) {
        String queryString = null;
        Query queryObject = null;
        queryString = "from ReasonPre where status = 1 and type = ? AND (reasonId IN (Select map.regReasonId From MapActiveInfoPre map Where "
                + " (map.provinceCode = ? OR map.provinceCode='-1' OR map.provinceCode is null) AND map.status = 1 "
                + " AND (map.districtCode = ? OR map.districtCode = '-1' OR map.districtCode is null)"
                + " AND (map.channelTypeId = ? OR map.channelTypeId = '-1' OR map.channelTypeId is null)"
                + " AND (map.effectDate <= trunc(sysdate) AND (trunc(sysdate)<=map.endDate OR map.endDate is null))"
                + " AND (map.productCode = ? OR map.productCode='-1' OR map.productCode is null)) "
                + " OR '-1' IN (Select map.regReasonId From MapActiveInfo map Where "
                + " (map.provinceCode = ? OR map.provinceCode='-1' OR map.provinceCode is null) AND map.status = 1 "
                + " AND (map.districtCode = ? OR map.districtCode = '-1' OR map.districtCode is null)"
                + " AND (map.channelTypeId = ? OR map.channelTypeId = '-1' OR map.channelTypeId is null)"
                + " AND (map.effectDate <= trunc(sysdate) AND (trunc(sysdate)<=map.endDate OR map.endDate is null))"
                + " AND (map.productCode = ? OR map.productCode='-1' OR map.productCode is null))) ";
        queryString += "order by NLSSORT(code,'NLS_SORT=vietnamese') ";
        queryObject = cmSession.createQuery(queryString);
        queryObject.setParameter(0, Constant.REASON_TYPE_REG_TYPE_NEW);
        queryObject.setParameter(1, provinceCode);
        queryObject.setParameter(2, districtCode);
        queryObject.setParameter(3, chanelTypeId);
        queryObject.setParameter(4, productCode);
        queryObject.setParameter(5, provinceCode);
        queryObject.setParameter(6, districtCode);
        queryObject.setParameter(7, chanelTypeId);
        queryObject.setParameter(8, productCode);

        List<ReasonPre> lstRegType = queryObject.list();
        if (lstRegType != null && lstRegType.size() > 0) {
            for (int i = 0; i < lstRegType.size(); i++) {
                ReasonPre reason = (ReasonPre) lstRegType.get(i);
                reason.setName(reason.getCode() + " -- " + reason.getName());
            }
        } else {
            lstRegType = new ArrayList<ReasonPre>();
            /*Nếu không có dữ liệu, trả về 1 list rỗng*/
        }
        return lstRegType;

    }

    public List<ReasonPre> getListRegTypeForAgent(Session cmSession, String type) {
        List<String> lstReasonCodeForAgent = getListReasonCodeByType(cmSession, type);
        List<ReasonPre> lstRegType = new ArrayList<ReasonPre>();
        if (lstReasonCodeForAgent != null && lstReasonCodeForAgent.size() > 0) {
            StringBuffer strListCode = new StringBuffer();
            for (int index = 0; index < lstReasonCodeForAgent.size(); index++) {
                strListCode.append("'" + lstReasonCodeForAgent.get(index) + "'");
                strListCode.append(",");
            }
            if (strListCode.length() > 0) {
                strListCode.deleteCharAt(strListCode.lastIndexOf(","));
            }

            /*Fix code đối với đại lý XNK thì sẽ ko lấy các Code trong ApParam*/
            String strIn = "IN";
            if (Constants.REASON_CODE_FOR_AGENT_XNK.equals(type)) {
                strIn = "NOT IN";
            }
            String queryString = "From ReasonPre Where status = 1 And type = ? And code " + strIn + " (" + strListCode.toString() + ") Order By NLSSORT(code,'NLS_SORT=vietnamese')";
            Query queryObject = cmSession.createQuery(queryString);
            queryObject.setParameter(0, Constant.REASON_TYPE_REG_TYPE_NEW);
            lstRegType = queryObject.list();

            if (lstRegType != null && lstRegType.size() > 0) {
                for (int i = 0; i < lstRegType.size(); i++) {
                    ReasonPre reason = (ReasonPre) lstRegType.get(i);
                    reason.setName(reason.getCode() + " -- " + reason.getName());
                }
            }
        }
        return lstRegType;
    }

    public static List<String> getListReasonCodeByType(Session cmSession, String type) {
        String strQuery = "From ApParamPre Where status = ? And paramType = ? And paramCode = ?";
        Query query = cmSession.createQuery(strQuery);
        query.setParameter(0, Constants.AP_PARAM_STATUS_USED);
        query.setParameter(1, type);
        query.setParameter(2, type);
        List lstResult = query.list();

        if (lstResult != null && lstResult.size() > 0) {
            List<String> lstReasonCode = new ArrayList<String>();
            ApParam apParam = (ApParam) lstResult.get(0);
            if (apParam != null) {
                String strListReasonCode = apParam.getParamValue();
                if (strListReasonCode != null && !"".equals(strListReasonCode.trim()) && strListReasonCode.indexOf(";") > 0) {
                    String[] arrOfferId = strListReasonCode.split(";");
                    for (int index = 0; index < arrOfferId.length; index++) {
                        lstReasonCode.add(arrOfferId[index]);
                    }
                } else if (strListReasonCode != null && !"".equals(strListReasonCode.trim())) {
                    lstReasonCode.add(strListReasonCode.trim());
                }

                return lstReasonCode;
            }
        }

        return null;

    }

    public List<ReasonPre> getAllRegTypeListByUser(Session cmSession, String productCode, String userName) {
        String lstReasonCode = getListReasonPermitByUser(cmSession, userName, Constant.ACTION_SUBSCRIBER_ACTIVE_NEW);
        String lstReasonCodeNotAllow = getListReasonCodeNotAllowView(cmSession, Constant.ACTION_SUBSCRIBER_ACTIVE_NEW);
        List<ReasonPre> lstRegType = new ArrayList<ReasonPre>();
        if (lstReasonCode == null && !"".equals(lstReasonCode)) {
            userName = "All";/*Gán một giá trị bất kỳ để tạo key cho mapRegTypeForAgent*/
        }
        String queryString = null;
        List lstParam = new ArrayList();
        if (lstReasonCode != null && !"".equals(lstReasonCode)) {
            queryString = "from ReasonPre where status = 1 and type = ? ";
            lstParam.add(Constant.REASON_TYPE_REG_TYPE_NEW);
            if (lstReasonCodeNotAllow != null && !"".equals(lstReasonCodeNotAllow)) {
                queryString += "and (code not in ( " + lstReasonCodeNotAllow + " ) or code in ( " + lstReasonCode + " ) ) ";
            }
            queryString += " and reasonId in ( select reasonId from MappingPre where (productCode = ? or productCode is null) and status = 1) ";
            lstParam.add(productCode);
            queryString += " order by NLSSORT(code,'NLS_SORT=vietnamese') ";
        } else {
            /*Lấy HTHM ko cần quan tâm HP or Mobile*/
            queryString = "from ReasonPre where status = 1 and type = ? ";
            lstParam.add(Constant.REASON_TYPE_REG_TYPE_NEW);
            if (lstReasonCodeNotAllow != null && !"".equals(lstReasonCodeNotAllow)) {
                queryString += "and code not in ( " + lstReasonCodeNotAllow + " ) ";
            }
            queryString += " and reasonId in ( select reasonId from MappingPre where (productCode = ? or productCode is null) and status = 1) ";
            lstParam.add(productCode);
            queryString += "order by NLSSORT(code,'NLS_SORT=vietnamese') ";
        }
        Query queryObject = cmSession.createQuery(queryString);
        if (lstParam != null && !lstParam.isEmpty()) {
            for (int i = 0; i < lstParam.size(); i++) {
                queryObject.setParameter(i, lstParam.get(i));
            }
        }
        lstRegType = queryObject.list();
        if (lstRegType != null && lstRegType.size() > 0) {
            for (int i = 0; i < lstRegType.size(); i++) {
                ReasonPre reason = (ReasonPre) lstRegType.get(i);
                reason.setName(reason.getCode() + " -- " + reason.getName());
            }
        } else {
            lstRegType = new ArrayList<ReasonPre>();
            /*Nếu không có dữ liệu, trả về 1 list rỗng*/
        }
        return lstRegType;
    }

    public String getListReasonPermitByUser(Session cmSession, String userName, String actionCode) {
        StringBuffer lstReasonCode = new StringBuffer();
        String queryString = null;
        Query queryObject = null;
        queryString = "from ApParamPre where status = ? and paramType = ? ";
        queryObject = cmSession.createQuery(queryString);
        queryObject.setParameter(0, Constants.STATUS_USE);
        queryObject.setParameter(1, Constants.APPARAM_REASON_PERMIT_BY_USER);
        List<ApParam> userRoles = queryObject.list();

        if (userRoles != null && userRoles.size() > 0) {
            String op = "";
            for (ApParam ap : userRoles) {
                String paramVal = ap.getParamValue();
                String[] arrParamVal = paramVal.split(":");
                if (arrParamVal != null && arrParamVal.length > 0) {
                    if (arrParamVal[0] != null && arrParamVal[0].contains(userName)
                            && arrParamVal[1] != null && arrParamVal[1].contains(actionCode)) {
                        if (arrParamVal[2] != null && !"".equals(arrParamVal[2])) {
                            String[] reasonCode = arrParamVal[2].split(",");
                            for (int i = 0; i < reasonCode.length; i++) {
                                lstReasonCode.append(op).append("'").append(reasonCode[i]).append("'");
                                op = ",";
                            }
                        }
                    }
                }
            }
        }
        return lstReasonCode.toString();
    }

    public String getListReasonCodeNotAllowView(Session cmSession, String actionCode) {
        List<ApParam> lstApParam = new ArrayList<ApParam>();
        StringBuffer lstReasonCode = new StringBuffer();
        String queryString = null;
        Query queryObject = null;
        queryString = "from ApParamPre where status = ? and paramType = ? ";
        queryObject = cmSession.createQuery(queryString);
        queryObject.setParameter(0, Constants.STATUS_USE);
        queryObject.setParameter(1, Constants.APPARAM_REASON_NOT_ALLOW_VIEW);
        lstApParam = queryObject.list();

        if (lstApParam != null && lstApParam.size() > 0) {
            String op = "";
            for (ApParam ap : lstApParam) {
                String paramVal = ap.getParamValue();
                String[] arrParamVal = paramVal.split(":");
                if (arrParamVal != null && arrParamVal.length > 0) {
                    if (arrParamVal[0] != null && arrParamVal[0].contains(actionCode)) {
                        if (arrParamVal[1] != null && !"".equals(arrParamVal[1])) {
                            String[] reasonCode = arrParamVal[1].split(",");
                            for (int i = 0; i < reasonCode.length; i++) {
                                lstReasonCode.append(op).append("'").append(reasonCode[i]).append("'");
                                op = ",";
                            }
                        }
                    }
                }
            }
        }

        return lstReasonCode.toString();
    }

    public List<Reason> getReasonPayAdvance(Session cmSession, Session paymentSession, Session product, Long contractId, String isdn) throws Exception {

        SubAdslLeaseline sub = new SubAdslLeaselineDAO().findByAccount(cmSession, isdn);
        if (sub == null) {
            return null;
        }
        String sql = "SELECT   product_code "
                + "  FROM   product p, product_offer o "
                + " WHERE   p.product_id = o.product_id "
                + " and offer_code = ? ";
        Query query = product.createSQLQuery(sql);
        query.setParameter(0, sub.getProductCode());
        List list = query.list();

//        sql = "SELECT R.REASON_ID as reasonId, R.NAME as name, R.PAY_ADV_AMOUNT as payAdvAmount "
//                + "  FROM MAPPING MAP  "
//                + "  JOIN REASON R  "
//                + "    ON MAP.REASON_ID = R.REASON_ID  "
//                + " WHERE MAP.SALE_SERVICE_CODE = 'Pay_Adv'  "
//                + "   AND R.STATUS = 1  "
//                + "   AND R.PAY_ADV_AMOUNT > 0  "
//                + "   AND MAP.PRODUCT_CODE = ? "
//                + " ORDER BY PAY_ADV_AMOUNT ";
        //duyetdk: map theo province
        sql = "SELECT r.reason_id as reasonId, r.name as name, r.pay_adv_amount as payAdvAmount "
                + "  FROM mapping map  "
                + "  JOIN reason r  "
                + "    ON map.reason_id = r.reason_id  "
                + "  JOIN map_active_info m "
                + "    ON m.product_code = map.product_code "
                + "   AND m.reg_reason_id = r.reason_id "
                + "   AND m.status = 1 "
                + "   AND (m.province_code = '-1' OR m.province_code = ?) "
                + " WHERE map.sale_service_code = 'Pay_Adv'  "
                + "   AND (map.product_code = ? or map.product_code = ?)"
                + "   AND r.status = 1  "
                + "   AND r.pay_adv_amount > 0  "
                + " ORDER BY r.pay_adv_amount ";

        query = cmSession.createSQLQuery(sql)
                .addScalar("reasonId", Hibernate.LONG)
                .addScalar("name", Hibernate.STRING)
                .addScalar("payAdvAmount", Hibernate.DOUBLE)
                .setResultTransformer(Transformers.aliasToBean(Reason.class));
        query.setParameter(0, sub.getProvince());
        query.setParameter(1, list == null || list.isEmpty() ? "" : list.get(0).toString());
        query.setParameter(2, sub.getProductCode());
        List<Reason> listReason = query.list();
        Double disCount = 0d;
        /*Kiem tra CDBR co thuoc chinh sach moi*/
        boolean flagIsNewPlanCDBR = new CustomerSupplier().isCheckNewConnect(cmSession, contractId);
        if (flagIsNewPlanCDBR) {
            SubAdslLeaseline subAdslLl = new SubAdslLeaselineDAO().findByAccount(cmSession, isdn);
            if (subAdslLl != null) {
                double deposit = new SubDepositAdslLlDAO().getTotalCurrentDeposit(cmSession, subAdslLl.getSubId());
                /*Kiem tra tien coc*/
                if (deposit > 0d) {
                    /*Kiem tra no cuoc*/
                    PaymentSupplier paymentSup = new PaymentSupplier();
                    boolean isDebit = paymentSup.isAllDebitSub(paymentSession, subAdslLl.getSubId());
                    if (!isDebit) {
                        /*Kiem tra so lan Pay advance*/
                        Long numOfPayAdvance = paymentSup.NumberOfPayAdvance(paymentSession, subAdslLl.getContractId());
                        if (numOfPayAdvance > 0) {
                            disCount = deposit;
                        }
                    }
                }
            }
        }

        if (disCount > 0d && listReason != null) {
            for (Reason r : listReason) {
                r.setPayAdvAmount(r.getPayAdvAmount() - disCount);
                r.setDescription("Discount " + disCount.toString() + " from deposit");
            }
        }
        return listReason;
    }

    /**
     * @author : duyetdk
     * @des: find Deposit from MapActiveInfo by regReasonId
     * @since 15-03-2018
     */
    public List<MapActiveInfo> find(Session cmPosSession, Long id, Long telServiceId, Long status, String productCode, Long regReasonId) {
        StringBuilder sql = new StringBuilder().append(" from MapActiveInfo where 1=1 ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        if (id != null) {
            sql.append(" and id = :id ");
            params.put("id", id);
        }
        if (telServiceId != null) {
            sql.append(" and telServiceId = :telServiceId ");
            params.put("telServiceId", telServiceId);
        }
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        if (productCode != null && !productCode.trim().isEmpty()) {
            sql.append(" and productCode = :productCode ");
            params.put("productCode", productCode);
        }
        if (regReasonId != null) {
            sql.append(" and regReasonId = :regReasonId ");
            params.put("regReasonId", regReasonId);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<MapActiveInfo> result = query.list();
        return result;
    }

    public List<Reason> getReasonPayAdvanceEmoney(Session cmSession, Session product, String isdn) throws Exception {

        SubAdslLeaseline sub = new SubAdslLeaselineDAO().findByAccount(cmSession, isdn);
        if (sub == null) {
            return null;
        }
        String sql = "SELECT   product_code "
                + "  FROM   product p, product_offer o "
                + " WHERE   p.product_id = o.product_id "
                + " and offer_code = ? ";
        Query query = product.createSQLQuery(sql);
        query.setParameter(0, sub.getProductCode());
        List list = query.list();

        sql = "SELECT R.REASON_ID as reasonId, R.NAME as name, R.PAY_ADV_AMOUNT as payAdvAmount , R.promotion"
                + "  FROM MAPPING MAP  "
                + "  JOIN REASON R  "
                + "    ON MAP.REASON_ID = R.REASON_ID  "
                + "   join map_active_info m "
                + "    on m.product_code = map.product_code "
                + "   and m.reg_reason_id = r.reason_id "
                + "   and m.status = 1 "
                + "   and (m.province_code = '-1' or m.province_code = ?)"
                + " WHERE MAP.SALE_SERVICE_CODE = 'Pay_Adv_Emoney'  "
                + "   AND (map.product_code = ? or map.product_code = ? ) "
                + "   AND R.STATUS = 1  "
                + "   AND R.PAY_ADV_AMOUNT > 0  "
                + " ORDER BY PAY_ADV_AMOUNT desc";

        query = cmSession.createSQLQuery(sql)
                .addScalar("reasonId", Hibernate.LONG)
                .addScalar("name", Hibernate.STRING)
                .addScalar("payAdvAmount", Hibernate.DOUBLE)
                .addScalar("promotion", Hibernate.DOUBLE)
                .setResultTransformer(Transformers.aliasToBean(Reason.class));
        query.setParameter(0, sub.getProvince());
        query.setParameter(1, list == null || list.isEmpty() ? "" : list.get(0).toString());
        query.setParameter(2, sub.getProductCode());
        List<Reason> listReason = query.list();
        return listReason;
    }

    public List<Reason> getMappingByOfferIdVas(Session cmPosSession, String productCode, String vas) {
        String sql = " select r from Mapping m, Reason r where m.productCode = ? and m.vas = ? and m.reasonId = r.reasonId and r.status = 1 and m.status = 1";
        Query qeury = cmPosSession.createQuery(sql);
        qeury.setParameter(0, productCode);
        qeury.setParameter(1, vas);
        return qeury.list();
    }

    public List<Mapping> getMappingByOfferIdVas(Session cmPosSession, String productCode, String vas, Long reasonId) {
        String sql = " select m from Mapping m where m.productCode = ? and m.vas = ? and m.reasonId = ? and m.status = 1 ";
        Query qeury = cmPosSession.createQuery(sql);
        qeury.setParameter(0, productCode);
        qeury.setParameter(1, vas);
        qeury.setParameter(2, reasonId);
        return qeury.list();
    }
}
