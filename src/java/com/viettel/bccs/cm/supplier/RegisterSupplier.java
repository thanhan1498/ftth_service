/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.pre.CustomerPre;
import com.viettel.bccs.cm.model.pre.SubMbPre;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author duyetdk
 */
public class RegisterSupplier extends BaseSupplier {

    public List<SubMbPre> checkSubActive(Session session, String isdn, Long status, String actStatus) throws Exception {
        String sqlStr = " FROM SubMbPre WHERE isdn = ? AND status = ? AND actStatus = ? ";
        Query query = session.createQuery(sqlStr);
        query.setParameter(0, isdn);
        query.setParameter(1, status);
        query.setParameter(2, actStatus);
        List ls = query.list();
        return ls;
    }

    public List<CustomerPre> getCustomerInfoByIdNo(Session session, String idNo) throws Exception {
        logger.info("\n--------------\n" + "Start getCustomerInfoByIdNo" + "\n--------------\n");
        try {
            String sqlStr = "FROM CustomerPre "
                    + " WHERE LOWER (id_no) = LOWER (?) "
                    + " AND ADDED_DATE >= to_date('01/04/2017','dd/mm/yyyy') "
                    + " AND STATUS = 1 "
                    + " ORDER BY cust_id DESC ";
            Query query = session.createQuery(sqlStr);
            logger.info("\n--------------\n" + sqlStr + "\n--------------\n");
            query.setParameter(0, idNo);
            List ls = query.list();

            logger.info("\n--------------\n" + "Stop getCustomerInfoByIdNo" + "\n--------------\n");
            return ls;
        } catch (HibernateException e) {
            logger.error("Error getCustomerInfoByIdNo " + e.toString());
            return null;
        }
    }

    public void insertCustomer(Session cmPreSession, Long custId, Long idType, String name, Date dob,
            String idNumber, String addUser, Date nowDate, String gender, String contact, String province) throws Exception {
        CustomerPre cus = new CustomerPre();
        cus.setCustId(custId);
        cus.setBusType(Constants.BUS_TYPE);
        cus.setName(name);
        cus.setBirthDate(dob);
        cus.setIdNo(idNumber);
        cus.setIdType(idType);
        cus.setAddedUser(addUser);
        cus.setAddedDate(nowDate);
        cus.setNotes(Constants.NOTE_UPDATE_BY_MBCCS);
        cus.setStatus(Constants.STATUS_USE);
        cus.setCorrectCus(Constants.CORRECT_CUS);
        cus.setSex(gender);
        cus.setProvince(province);
        cus.setTelFax(contact);
        cmPreSession.save(cus);
        cmPreSession.flush();
    }

    public int insertCustImageDetail(Session cmSession, Long custImageDetailId, Long cusImgId, String imageName, String imageType, String objType)
            throws Exception {
        String sql = "INSERT INTO cust_image_detail(id,cust_img_id,create_date,image_name,image_type,obj_type,status) "
                + " VALUES(?,?,sysdate,?,?,?,?)";

        Query query = cmSession.createSQLQuery(sql);
        query.setParameter(0, custImageDetailId);
        query.setParameter(1, cusImgId);
        query.setParameter(2, imageName);
        query.setParameter(3, imageType);
        query.setParameter(4, objType);
        query.setParameter(5, 1L);
        return query.executeUpdate();
    }

    public int insertCustImage(Session cmSession, Long custImageId, Long cusId, String account, String idNo, String name,
            String staffCodeLogin, String serial, String isdn, Long staffType) throws Exception {

        String sql = "INSERT INTO cust_image(id,cust_id,account,id_card,name,staff_code,serial,isdn,create_date,staff_type,status,is_correct) "
                + " VALUES(?,?,?,?,?,?,?,?,sysdate,?,?,?)";

        Query query = cmSession.createSQLQuery(sql);
        query.setParameter(0, custImageId);
        query.setParameter(1, cusId);
        query.setParameter(2, account);
        query.setParameter(3, idNo);
        query.setParameter(4, name);
        query.setParameter(5, staffCodeLogin);
        query.setParameter(6, serial);
        query.setParameter(7, isdn);
        query.setParameter(8, staffType == null ? 0L : staffType);
        query.setParameter(9, Constants.STATUS_USE);
        query.setParameter(10, 1L);
        return query.executeUpdate();
    }
}
