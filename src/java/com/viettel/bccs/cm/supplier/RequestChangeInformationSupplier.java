/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.api.Task.BO.Reason;
import com.viettel.bccs.api.Util.Constant;
import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.cm.database.BO.pre.SubIdNo;
import com.viettel.bccs.cm.model.AccountFromStaffCode;
import com.viettel.bccs.cm.model.CustImagePre;
import com.viettel.bccs.cm.model.OTPObject;
import com.viettel.bccs.cm.model.pre.ReasonPre;
import com.viettel.bccs.cm.model.SubIdNoPre;
import com.viettel.bccs.cm.model.SubMb;
import com.viettel.bccs.cm.model.pre.CustomerPre;
import com.viettel.bccs.cm.model.pre.SubMbPre;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.bccs.cm.util.DateTimeUtils;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.cm.util.ReflectUtils;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.bccs.cm.util.ValidateUtils;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.vsa.ResourceBundleUtil;
import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import com.viettel.brcd.ws.bccsgw.model.Param;
import com.viettel.brcd.ws.common.WebServiceClient;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class RequestChangeInformationSupplier extends BaseSupplier {

    public RequestChangeInformationSupplier() {
        logger = Logger.getLogger(RequestChangeInformationSupplier.class);
    }

    public List<SubMb> getListSubMbActiveByCustId(Session cmPreSession, Long custId) {
        StringBuilder sql = new StringBuilder().append("SELECT  t1.cust_id custId, "
                + "t1.isdn isdn,t1.sub_id subId, t1.serial serial "
                + "FROM sub_mb t1 "
                + "WHERE 1 = 1 AND t1.status = :statusSubId AND t1.cust_id = :custIdParam ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("statusSubId", Constants.SUB_STATUS_NORMAL_ACTIVED);
        params.put("custIdParam", custId);
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("custId", Hibernate.LONG)
                .addScalar("isdn", Hibernate.STRING)
                .addScalar("subId", Hibernate.LONG)
                .addScalar("serial", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(SubMb.class));
        buildParameter(query, params);
        List<SubMb> result = query.list();
        return result;
    }
    
    public List<SubMb> getListSubMbChangeInfoActiveByCustId(Session cmPreSession, Long custId) {
        StringBuilder sql = new StringBuilder().append("SELECT  t1.cust_id custId, "
                + "t1.isdn isdn,t1.sub_id subId, t1.serial serial "
                + "FROM sub_mb t1 "
                + "WHERE 1 = 1 AND t1.status = :statusSubId AND t1.cust_id = :custIdParam and rownum <= 3");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("statusSubId", Constants.SUB_STATUS_NORMAL_ACTIVED);
        params.put("custIdParam", custId);
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("custId", Hibernate.LONG)
                .addScalar("isdn", Hibernate.STRING)
                .addScalar("subId", Hibernate.LONG)
                .addScalar("serial", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(SubMb.class));
        buildParameter(query, params);
        List<SubMb> result = query.list();
        return result;
    }

    public List<SubMb> getListSubMbWithSerial(Session cmPreSession, Long custId, String isdn) {
        StringBuilder sql = new StringBuilder().append("SELECT  t1.cust_id custId, "
                + "t1.isdn isdn,t1.sub_id subId, t1.serial serial "
                + "FROM sub_mb t1 "
                + "WHERE 1 = 1 AND t1.status <> 0 AND t1.isdn = :isdn "
                + "AND t1.last_number = :last_number AND t1.cust_id = :custIdParam");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("isdn", isdn);
        params.put("last_number", isdn.substring(isdn.length() - 1));
        params.put("custIdParam", custId);
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("custId", Hibernate.LONG)
                .addScalar("isdn", Hibernate.STRING)
                .addScalar("subId", Hibernate.LONG)
                .addScalar("serial", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(SubMb.class));
        buildParameter(query, params);
        List<SubMb> result = query.list();
        return result;
    }

    public List<SubMb> getSubMbChangeInfo(Session cmPreSession, Long subId) {
        StringBuilder sql = new StringBuilder().append("SELECT  t1.cust_id custId, t1.isdn isdn, "
                + "t1.sub_id subId,t1.serial serial, "
                + "t1.PRODUCT_CODE productCode, t1.SUB_NAME subName "
                + "FROM sub_mb t1 WHERE t1.sub_id = :subIdParam ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("subIdParam", subId);
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("custId", Hibernate.LONG)
                .addScalar("isdn", Hibernate.STRING)
                .addScalar("subId", Hibernate.LONG)
                .addScalar("serial", Hibernate.STRING)
                .addScalar("productCode", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(SubMb.class));
        buildParameter(query, params);
        List<SubMb> result = query.list();
        return result;
    }

    public List<SubMb> getSubMbFromISDN(Session cmPreSession, String isdn) {
        StringBuilder sql = new StringBuilder().append("SELECT  t1.cust_id custId, '' offerName, '' statusName, "
                + "t1.sub_id subId,t1.isdn isdn, t1.status status, t1.act_status actStatus, "
                + "t2.id_no idNo, t2.name customerName, t1.offer_id offerId, t2.birth_date "
                + "birthDate FROM sub_mb t1 left JOIN customer t2 ON t1.cust_id = t2.cust_id "
                + "and t2.status = '1' WHERE 1 = 1 AND t1.status = 2 AND t1.isdn = :isdn "
                + "AND t1.last_number = :last_number");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("isdn", isdn);
        params.put("last_number", isdn.substring(isdn.length() - 1));
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("custId", Hibernate.LONG)
                .addScalar("offerName", Hibernate.STRING)
                .addScalar("statusName", Hibernate.STRING)
                .addScalar("subId", Hibernate.LONG)
                .addScalar("isdn", Hibernate.STRING)
                .addScalar("status", Hibernate.LONG)
                .addScalar("actStatus", Hibernate.STRING)
                .addScalar("idNo", Hibernate.STRING)
                .addScalar("customerName", Hibernate.STRING)
                .addScalar("offerId", Hibernate.LONG)
                .addScalar("birthDate", Hibernate.DATE)
                .setResultTransformer(Transformers.aliasToBean(SubMb.class));
        buildParameter(query, params);
        List<SubMb> result = query.list();
        return result;
    }

    public String getOfferNameFromOfferId(Session cmProductSession, Long offerId) {
        StringBuilder sql = new StringBuilder().append("select OFFER_NAME offerName "
                + "from product_offer where offer_id = :offerId");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("offerId", offerId);
        Query query = cmProductSession.createSQLQuery(sql.toString())
                .addScalar("offerName", Hibernate.STRING);
        buildParameter(query, params);
        List<String> result = query.list();
        return (result != null && !result.isEmpty()) ? result.get(0) : "";
    }

    public String getGroupNameByISDN(Session cmIMSession, String isdn) {
        StringBuilder sql = new StringBuilder().append("SELECT l.name groupName FROM stock_isdn_mobile a, "
                + "stock_model c, isdn_filter_rules i, filter_type l WHERE "
                + "a.stock_model_id = c.stock_model_id AND a.rules_id = i.rules_id "
                + "AND i.filter_type_id = l.filter_type_id AND a.isdn = :isdn");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("isdn", isdn);
        Query query = cmIMSession.createSQLQuery(sql.toString())
                .addScalar("groupName", Hibernate.STRING);
        buildParameter(query, params);
        List<String> result = query.list();
        return (result != null && !result.isEmpty()) ? result.get(0) : ""; 
    }

    public void logActionV2(Long actionAuditId, Long pkId, String pkType,
            Long reasonId, String actionCode, String logDescription, String loginName,
            String shopCodeLogin, Date nowDate, Session cmPosSession, String ip) throws Exception {
        try {
            String sql = "insert into action_audit(action_Audit_Id,action_Code,Issue_Datetime,reason_id,User_Name,Shop_Code,Pk_Type,Pk_Id,Description, ip) "
                    + "values(?,?,?,?,?,?,?,?,?,?)";
            Query query = cmPosSession.createSQLQuery(sql);
            query.setParameter(0, actionAuditId);
            query.setParameter(1, actionCode);
            query.setParameter(2, nowDate);
            query.setParameter(3, reasonId == null ? 0L : reasonId);
            query.setParameter(4, loginName);
            query.setParameter(5, shopCodeLogin);
            query.setParameter(6, pkType);
            query.setParameter(7, pkId);
            query.setParameter(8, logDescription);
            query.setParameter(9, ip);
            query.executeUpdate();
            cmPosSession.flush();
//            cmPosSession.getTransaction().commit();
        } catch (Exception e) {
            throw e;
        }

    }

    public Long createCustImageHaveIsdn(Session cmPreSession, Long custId, String account,
            String idNo, String staffCode, String name, String serial,
            String isdn, Long staffType) throws Exception {
        try {
            String sql = "insert into CUST_IMAGE(id,CUST_ID,ACCOUNT,ID_CARD,NAME,"
                    + "STAFF_CODE,SERIAL,ISDN,CREATE_DATE,STATUS,"
                    + "IS_CORRECT,STAFF_TYPE) "
                    + "values(?,?,?,?,?,?,?,?,sysdate,?,?,?)";
            Query query = cmPreSession.createSQLQuery(sql);
            Long custImageId = getSequence(cmPreSession, Constants.CUST_IMAGE_SEQ);
            query.setParameter(0, custImageId);
            query.setParameter(1, custId);
            query.setParameter(2, account);
            query.setParameter(3, idNo.toUpperCase());
            query.setParameter(4, name);
            query.setParameter(5, staffCode);
            query.setParameter(6, serial);
            query.setParameter(7, isdn);
            query.setParameter(8, 1L);
            query.setParameter(9, 1L);
            query.setParameter(10, staffType);
            query.executeUpdate();
            cmPreSession.flush();
            return custImageId;
        } catch (Exception ex) {
            logger.error("Create cust_image fail", ex);
            return null;
        }
    }

    public void createCustImage(Session cmPreSession, Long custId, String account,
            String staffCode, Long staffType) throws Exception {
        try {
            String sql = "insert into CUST_IMAGE(id,CUST_ID,ACCOUNT,"
                    + "STAFF_CODE,CREATE_DATE,STATUS,LAST_UPDATE,"
                    + "IS_CORRECT,STAFF_TYPE) "
                    + "values(?,?,?,?,sysdate,?,sysdate,?,?)";
            Query query = cmPreSession.createSQLQuery(sql);
            Long custImageId = getSequence(cmPreSession, Constants.CUST_IMAGE_SEQ);
            query.setParameter(0, custImageId);
            query.setParameter(1, custId);
            query.setParameter(2, account);
            query.setParameter(3, staffCode);
            query.setParameter(4, 1L);
            query.setParameter(5, 1L);
            query.setParameter(6, staffType);
            query.executeUpdate();
            cmPreSession.flush();
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void logAuditDetailV2(Long rowId, Long actionAuditId, String tableName, String colName,
            Object oldValue, Object newValue, Date issueDateTime, Session cmPreSession) throws Exception {
        try {
            String sql = "insert into action_detail(action_detail_id,Table_Name,Row_Id,"
                    + "Col_Name,issue_datetime,Old_Value,New_Value,Action_Audit_Id) "
                    + "values(?,?,?,?,?,?,?,?)";
            Query query = cmPreSession.createSQLQuery(sql);
            Long actionDetailId = getSequence(cmPreSession, Constants.SEQ_ACTION_DETAIL);
            query.setParameter(0, actionDetailId);
            query.setParameter(1, tableName);
            query.setParameter(2, rowId);
            query.setParameter(3, colName);
            query.setParameter(4, issueDateTime);
            query.setParameter(5, (oldValue == null) ? "" : String.valueOf(oldValue));
            query.setParameter(6, (newValue == null) ? "" : String.valueOf(newValue));
            query.setParameter(7, actionAuditId);
            query.executeUpdate();
            cmPreSession.flush();
        } catch (Exception ex) {
            throw ex;
        }
    }

    public boolean logCustomerNew(CustomerPre oldCust, CustomerPre newCust, Long actionAuditId, Date issDateTime, Session cmPreSession) throws Exception {
        Date sysDate = new Date();
        boolean status = true;

        if (newCust.getCustId() == null) {
            newCust.setCustId(0L);
        }

        logAuditDetailV2(0L, actionAuditId, "Customer", "CUST_ID", null, newCust.getCustId(), issDateTime, cmPreSession);
        status = false;

        return status;
    }

    public boolean logCustomer(CustomerPre oldCust, CustomerPre newCust, Long actionAuditId, Date issDateTime, Session cmPreSession) throws Exception {
        Date sysDate = new Date();
        boolean status = true;
        if (oldCust.getAddress() == null) {
            oldCust.setAddress("");
        }
        if (oldCust.getAddedDate() == null) {
            oldCust.setAddedDate(sysDate);
        }
        if (oldCust.getBirthDate() == null) {
            oldCust.setBirthDate(sysDate);
        }

        String oldIdNo = oldCust.getIdNo();
        if (oldCust.getIdNo() == null) {
            oldCust.setIdNo("");
        }
        Long oldIdType = oldCust.getIdType();
        if (oldCust.getIdType() == null) {
            oldCust.setIdType(0L);
        }

        if (oldCust.getSex() == null) {
            oldCust.setSex("");
        }

        if (oldCust.getName() == null) {
            oldCust.setName("");
        }

        if (oldCust.getCustId() == null) {
            oldCust.setCustId(0L);
        }

        System.out.println("Debug log customer " + newCust.getAddress());

        if (!newCust.getAddress().equalsIgnoreCase(oldCust.getAddress())) {
            logAuditDetailV2(oldCust.getCustId(), actionAuditId, "Customer", "ADDRESS", oldCust.getAddress().toString(), newCust.getAddress().toString(), issDateTime, cmPreSession);
            status = false;
        } else if (newCust.getAddress() == null && oldCust.getAddress() != null) {
            logAuditDetailV2(oldCust.getCustId(), actionAuditId, "Customer", "ADDRESS", oldCust.getAddress().toString(), null, issDateTime, cmPreSession);
            status = false;
        }

        if (!(DateTimeUtils.formatDate(newCust.getBirthDate(), "yyyy/MM/dd")).equals(DateTimeUtils.formatDate(oldCust.getBirthDate(), "yyyy/MM/dd"))) {
            logAuditDetailV2(oldCust.getCustId(), actionAuditId, "Customer", "BIRTH_DATE", oldCust.getBirthDate().toString(), newCust.getBirthDate().toString(), issDateTime, cmPreSession);
            status = false;
        }

        if (newCust.getIdNo() != null && oldCust.getIdNo() != null && !newCust.getIdNo().equalsIgnoreCase(oldCust.getIdNo())) {
            logAuditDetailV2(oldCust.getCustId(), actionAuditId, "Customer", "ID_NO", oldCust.getIdNo().toString(), newCust.getIdNo().toString(), issDateTime, cmPreSession);
            status = false;
        } else if (newCust.getIdNo() == null && oldIdNo != null) {
            logAuditDetailV2(oldCust.getCustId(), actionAuditId, "Customer", "ID_NO", oldCust.getIdNo().toString(), null, issDateTime, cmPreSession);
            status = false;
        }

        if (newCust.getIdType() != null && oldCust.getIdType() != null && !newCust.getIdType().equals(oldCust.getIdType())) {
            logAuditDetailV2(oldCust.getCustId(), actionAuditId, "Customer", "ID_TYPE", oldCust.getIdType().toString(), newCust.getIdType().toString(), issDateTime, cmPreSession);
            status = false;
        } else if (newCust.getIdType() == null && oldIdType != null) {
            logAuditDetailV2(oldCust.getCustId(), actionAuditId, "Customer", "ID_TYPE", oldCust.getIdType().toString(), null, issDateTime, cmPreSession);
            status = false;
        }

        if (newCust.getSex() != null && !newCust.getSex().equalsIgnoreCase(oldCust.getSex())) {
            logAuditDetailV2(oldCust.getCustId(), actionAuditId, "Customer", "SEX", oldCust.getSex().toString(), newCust.getSex().toString(), issDateTime, cmPreSession);
            status = false;
        }

        if (!newCust.getName().equalsIgnoreCase(oldCust.getName())) {
            logAuditDetailV2(oldCust.getCustId(), actionAuditId, "Customer", "NAME", oldCust.getName().toString(), newCust.getName().toString(), issDateTime, cmPreSession);
            status = false;
        }

        return status;
    }

    public List<AccountFromStaffCode> getAccountFromStaffCode(Session cmImSession, String staffCode) {
        StringBuilder sql = new StringBuilder().append("select msisdn, OWNER_CODE staffType from account_agent "
                + "where owner_code = :userLogin and status =  '1'");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("userLogin", staffCode);
        Query query = cmImSession.createSQLQuery(sql.toString())
                .addScalar("msisdn", Hibernate.STRING)
                .addScalar("staffType", Hibernate.LONG)
                .setResultTransformer(Transformers.aliasToBean(AccountFromStaffCode.class));
        buildParameter(query, params);
        List<AccountFromStaffCode> result = query.list();
        return result;
    }

    public List<CustomerPre> checkCustExistByCustId(Session cmPreSession, Long custId) {
        StringBuilder sql = new StringBuilder().append("select CUST_ID custId, NAME name, BIRTH_DATE birthDate, "
                + "SEX sex, ID_NO idNo, ID_TYPE idType, PROVINCE province, ADDRESS address, "
                + "STATUS status, ADDED_USER addedUser, ADDED_DATE addedDate, UPDATED_USER updatedUser, "
                + "UPDATED_TIME updatedTime, TEL_FAX telFax from Customer where CUST_ID = :custIdParam");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("custIdParam", custId);
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("custId", Hibernate.LONG)
                .addScalar("name", Hibernate.STRING)
                .addScalar("birthDate", Hibernate.DATE)
                .addScalar("sex", Hibernate.STRING)
                .addScalar("idNo", Hibernate.STRING)
                .addScalar("idType", Hibernate.LONG)
                .addScalar("province", Hibernate.STRING)
                .addScalar("address", Hibernate.STRING)
                .addScalar("status", Hibernate.LONG)
                .addScalar("addedUser", Hibernate.STRING)
                .addScalar("addedDate", Hibernate.DATE)
                .addScalar("updatedUser", Hibernate.STRING)
                .addScalar("updatedTime", Hibernate.DATE)
                .addScalar("telFax", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(CustomerPre.class));
        buildParameter(query, params);
        List<CustomerPre> result = query.list();
        return result;
    }

    public List<CustomerPre> checkCustExist(Session cmPreSession, String idNo) {
        String idNoUpper = idNo.toUpperCase();
        StringBuilder sql = new StringBuilder().append("select CUST_ID custId, NAME name, BIRTH_DATE birthDate, "
                + "SEX sex, ID_NO idNo, ID_TYPE idType, PROVINCE province, ADDRESS address, "
                + "STATUS status, ADDED_USER addedUser, ADDED_DATE addedDate, UPDATED_USER updatedUser, "
                + "UPDATED_TIME updatedTime, TEL_FAX telFax from Customer where id_no = :idNoUpper and status = 1");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("idNoUpper", idNoUpper);
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("custId", Hibernate.LONG)
                .addScalar("name", Hibernate.STRING)
                .addScalar("birthDate", Hibernate.DATE)
                .addScalar("sex", Hibernate.STRING)
                .addScalar("idNo", Hibernate.STRING)
                .addScalar("idType", Hibernate.LONG)
                .addScalar("province", Hibernate.STRING)
                .addScalar("address", Hibernate.STRING)
                .addScalar("status", Hibernate.LONG)
                .addScalar("addedUser", Hibernate.STRING)
                .addScalar("addedDate", Hibernate.DATE)
                .addScalar("updatedUser", Hibernate.STRING)
                .addScalar("updatedTime", Hibernate.DATE)
                .addScalar("telFax", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(CustomerPre.class));
        buildParameter(query, params);
        List<CustomerPre> result = query.list();
        return result;
    }

    public String updateSubIdNo(Session cmPreSession, Long subId, Date staDateTime) {
        String mess;
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String strDate = dateFormat.format(staDateTime);
            String sql = "update SUB_ID_NO set END_DATETIME = sysdate,STATUS = ? "
                    + "where SUB_ID = ? and TO_CHAR(STA_DATETIME, 'DD-MM-YYYY') = ?";
            Query query = cmPreSession.createSQLQuery(sql);
            query.setParameter(0, Constants.STATUS_NOT_USE);
            query.setParameter(1, subId);
            query.setParameter(2, strDate);
            query.executeUpdate();
            cmPreSession.flush();
            mess = ConvertUtils.toStringValue(1L);
        } catch (Exception ex) {
            mess = ConvertUtils.toStringValue(0L);
        }
        LogUtils.info(logger, "RequestChangeInformationSupplier.update:SUB_ID_NO.sub_id="
                + LogUtils.toJson(subId) + " and staDateTime=" + LogUtils.toJson(staDateTime));
        return mess;
    }

    public List<CustImagePre> getCustImageFromSerialAndIsdn(Session cmPreSession, String serial,
            String isdn) {
        StringBuilder sql = new StringBuilder().append("select ID id, CUST_ID custId, ACCOUNT account "
                + "FROM CUST_IMAGE "
                + "WHERE  status = :status_active  AND "
                + "serial = :serialParam AND "
                + "ISDN = :isdnParam");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("status_active", 1L);
        params.put("serialParam", serial);
        params.put("isdnParam", isdn);
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("custId", Hibernate.LONG)
                .addScalar("account", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(CustImagePre.class));
        buildParameter(query, params);
        List<CustImagePre> result = query.list();
        return result;
    }
    
    public void updateInfoCustomerImage(Session cmSession, String staffCode, Long id) {
        try {
            String sql = "update cust_image set last_update = sysdate, account_update = ?, "
                    + "status = ? where ID = ? ";
            Query query = cmSession.createSQLQuery(sql);
            query.setParameter(0, staffCode);
            query.setParameter(1, "0");
            query.setParameter(2, id);
            query.executeUpdate();
            cmSession.flush();
        } catch (Exception ex) {
        }
    }

    public List<CustImagePre> getCustImageActiveFromIsdn(Session cmPreSession, String isdn) {
        StringBuilder sql = new StringBuilder().append("select ID id, CUST_ID custId, ACCOUNT account "
                + "FROM CUST_IMAGE "
                + "WHERE  status = :status_active "
                + "AND ISDN = :isdnParam");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("status_active", 1L);
        params.put("isdnParam", isdn);
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("custId", Hibernate.LONG)
                .addScalar("account", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(CustImagePre.class));
        buildParameter(query, params);
        List<CustImagePre> result = query.list();
        return result;
    }

    public List<CustImagePre> getCustImageFromCustIdAndIsdn(Session cmPreSession, Long custId, String isdn) {
        StringBuilder sql = new StringBuilder().append("select ID id, CUST_ID custId, ACCOUNT account "
                + "FROM CUST_IMAGE "
                + "WHERE  status = :status_active  AND "
                + "CUST_ID = :custIdParam AND "
                + "ISDN = :isdnParam");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("status_active", 1L);
        params.put("custIdParam", custId);
        params.put("isdnParam", isdn);
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("custId", Hibernate.LONG)
                .addScalar("account", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(CustImagePre.class));
        buildParameter(query, params);
        List<CustImagePre> result = query.list();
        return result;
    }

    public List<CustImagePre> getCustImageFromCustId(Session cmPreSession, Long custId) {
        StringBuilder sql = new StringBuilder().append("select ID id, CUST_ID custId, ACCOUNT account "
                + "FROM CUST_IMAGE "
                + "WHERE  status = :status_active  AND "
                + "CUST_ID = :custIdParam");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("status_active", 1L);
        params.put("custIdParam", custId);
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("custId", Hibernate.LONG)
                .addScalar("account", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(CustImagePre.class));
        buildParameter(query, params);
        List<CustImagePre> result = query.list();
        return result;
    }

    public List<SubIdNoPre> selectActiveSubIdNoBySubId(Session cmPreSession, Long oldCustId) {
        StringBuilder sql = new StringBuilder().append("select SUB_ID subId, ISDN isdn, "
                + "ID_NO idNo, APP app, STA_DATETIME staDateTime, END_DATETIME endDateTime,"
                + "STATUS status FROM SUB_ID_NO WHERE  status = :statusSubIdNo  AND "
                + "SUB_ID IN (SELECT SUB_ID FROM SUB_MB WHERE status = :statusSubId  AND CUST_ID = :oldCustId)");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("statusSubIdNo", Constants.STATUS_ACTIVE_SUB_ID_NO);
        params.put("statusSubId", Constants.SUB_STATUS_NORMAL_ACTIVED);
        params.put("oldCustId", oldCustId);
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("subId", Hibernate.LONG)
                .addScalar("isdn", Hibernate.STRING)
                .addScalar("idNo", Hibernate.STRING)
                .addScalar("app", Hibernate.STRING)
                .addScalar("staDateTime", Hibernate.DATE)
                .addScalar("endDateTime", Hibernate.DATE)
                .addScalar("status", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(SubIdNoPre.class));
        buildParameter(query, params);
        List<SubIdNoPre> result = query.list();
        return result;
    }

    public List<CustomerPre> findById(Session cmPreSession, Long custId) {
        StringBuilder sql = new StringBuilder().append("select CUST_ID custId, NAME name, BIRTH_DATE birthDate, "
                + "SEX sex, ID_NO idNo, ID_TYPE idType, PROVINCE province, ADDRESS address, "
                + "STATUS status, ADDED_USER addedUser, ADDED_DATE addedDate, UPDATED_USER updatedUser, "
                + "UPDATED_TIME updatedTime, TEL_FAX telFax from Customer where cust_Id = :custIdInput and status = 1");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("custIdInput", custId);
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("custId", Hibernate.LONG)
                .addScalar("name", Hibernate.STRING)
                .addScalar("birthDate", Hibernate.DATE)
                .addScalar("sex", Hibernate.STRING)
                .addScalar("idNo", Hibernate.STRING)
                .addScalar("idType", Hibernate.LONG)
                .addScalar("province", Hibernate.STRING)
                .addScalar("address", Hibernate.STRING)
                .addScalar("status", Hibernate.LONG)
                .addScalar("addedUser", Hibernate.STRING)
                .addScalar("addedDate", Hibernate.DATE)
                .addScalar("updatedUser", Hibernate.STRING)
                .addScalar("updatedTime", Hibernate.DATE)
                .addScalar("telFax", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(CustomerPre.class));
        buildParameter(query, params);
        List<CustomerPre> result = query.list();
        return result;
    }

    public String updateSubMbChangeInfoBySubId(Session cmPreSession, String locale, String name,
            String DOB, String contact, String address, String Sex, Long custId, Long subId, String province)
            throws ParseException {
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="name">
        if (name == null) {
            mess = LabelUtil.getKey("validate.required", locale, "custName");
            return mess;
        }
        name = name.trim();

        Integer length = ReflectUtils.getColumnLength(SubMbPre.class, "subName");
        if (length != null && length > 0 && name.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("custName", locale), length);
            return mess;
        }
        //</editor-fold>
        Date nowDate = getSysDateTime(cmPreSession);
        //<editor-fold defaultstate="collapsed" desc="birthDate">
        if (DOB == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        DOB = DOB.trim();
        if (DOB.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (!ValidateUtils.isDateddMMyyyy(DOB)) {
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        Date bDate;
        try {
            bDate = DateTimeUtils.toDateddMMyyyy(DOB);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        if (bDate == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (bDate.after(nowDate)) {
            mess = LabelUtil.getKey("greater.than", locale, "birthDate", "nowDate");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="contact">
        contact = contact.trim();
        if (!contact.isEmpty()) {
            length = ReflectUtils.getColumnLength(SubMbPre.class, "telMobile");
            if (length != null && length > 0 && contact.length() > length) {
                mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("telMobile", locale), length);
                return mess;
            }
        }

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="address">
        if (address != null) {
            address = address.trim();
            if (!address.isEmpty()) {
                length = ReflectUtils.getColumnLength(SubMbPre.class, "address");
                if (length != null && length > 0 && address.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("streetBlock", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="gender">
        if (Sex != null) {
            Sex = Sex.trim();
            if (!Sex.isEmpty()) {
                length = ReflectUtils.getColumnLength(SubMbPre.class, "gender");
                if (length != null && length > 0 && Sex.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("sex", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>

        //</editor-fold>
        SubMbPre subMb = new SubMbPre();
        subMb.setSubName(name);
        subMb.setBirthDate(bDate);
        subMb.setCustId(custId);
        subMb.setTelMobile(contact);
        subMb.setAddress(address);
        subMb.setGender(Sex);
        subMb.setProvince(province);
        subMb.setSubId(subId);
//        cmPreSession.update(subMb);
//        cmPreSession.flush();
        try {
            String sql = "update sub_mb set sub_name = ?,birth_date = ?, cust_id = ?, "
                    + "tel_mobile = ?, address = ?, gender = ?, province = ? where sub_id = ? ";
            Query query = cmPreSession.createSQLQuery(sql);
            query.setParameter(0, subMb.getSubName());
            query.setParameter(1, subMb.getBirthDate());
            query.setParameter(2, subMb.getCustId());
            query.setParameter(3, subMb.getTelMobile());
            query.setParameter(4, subMb.getAddress());
            query.setParameter(5, subMb.getGender());
            query.setParameter(6, subMb.getProvince());
            query.setParameter(7, subMb.getSubId());
            query.executeUpdate();
            cmPreSession.flush();
            mess = ConvertUtils.toStringValue(1L);
        } catch (Exception ex) {
            mess = ConvertUtils.toStringValue(0L);
        }
        LogUtils.info(logger, "RequestChangeInformationSupplier.update:sub_mb=" + LogUtils.toJson(subMb));
        return mess;
    }

    public String updateSubMbBySubId(Session cmPreSession, String locale, String name,
            String DOB, String contact, String address, String Sex, Long custId, Long subId,
            String province)
            throws ParseException {
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="name">
        if (name == null) {
            mess = LabelUtil.getKey("validate.required", locale, "custName");
            return mess;
        }
        name = name.trim();

        Integer length = ReflectUtils.getColumnLength(SubMbPre.class, "subName");
        if (length != null && length > 0 && name.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("custName", locale), length);
            return mess;
        }
        //</editor-fold>
        Date nowDate = getSysDateTime(cmPreSession);
        //<editor-fold defaultstate="collapsed" desc="birthDate">
        if (DOB == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        DOB = DOB.trim();
        if (DOB.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (!ValidateUtils.isDateddMMyyyy(DOB)) {
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        Date bDate;
        try {
            bDate = DateTimeUtils.toDateddMMyyyy(DOB);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        if (bDate == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (bDate.after(nowDate)) {
            mess = LabelUtil.getKey("greater.than", locale, "birthDate", "nowDate");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="contact">
        contact = contact.trim();
        if (!contact.isEmpty()) {
            length = ReflectUtils.getColumnLength(SubMbPre.class, "telMobile");
            if (length != null && length > 0 && contact.length() > length) {
                mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("telMobile", locale), length);
                return mess;
            }
        }

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="address">
        if (address != null) {
            address = address.trim();
            if (!address.isEmpty()) {
                length = ReflectUtils.getColumnLength(SubMbPre.class, "address");
                if (length != null && length > 0 && address.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("streetBlock", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="gender">
        if (Sex != null) {
            Sex = Sex.trim();
            if (!Sex.isEmpty()) {
                length = ReflectUtils.getColumnLength(SubMbPre.class, "gender");
                if (length != null && length > 0 && Sex.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("sex", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>

        //</editor-fold>
        SubMbPre subMb = new SubMbPre();
        subMb.setSubName(name);
        subMb.setBirthDate(bDate);
        subMb.setCustId(custId);
        subMb.setTelMobile(contact);
        subMb.setAddress(address);
        subMb.setGender(Sex);
        subMb.setSubId(subId);
        subMb.setProvince(province);
//        cmPreSession.update(subMb);
//        cmPreSession.flush();
        try {
            String sql = "update sub_mb set sub_name = ?,birth_date = ?, cust_id = ?, "
                    + "tel_mobile = ?, address = ?, gender = ?, Change_Datetime = sysdate, "
                    + "PROVINCE = ? where sub_id = ? ";
            Query query = cmPreSession.createSQLQuery(sql);
            query.setParameter(0, subMb.getSubName());
            query.setParameter(1, subMb.getBirthDate());
            query.setParameter(2, subMb.getCustId());
            query.setParameter(3, subMb.getTelMobile());
            query.setParameter(4, subMb.getAddress());
            query.setParameter(5, subMb.getGender());
            query.setParameter(6, subMb.getProvince());
            query.setParameter(7, subMb.getSubId());
            query.executeUpdate();
            cmPreSession.flush();
            mess = ConvertUtils.toStringValue(1L);
        } catch (Exception ex) {
            mess = ConvertUtils.toStringValue(0L);
        }
        LogUtils.info(logger, "RequestChangeInformationSupplier.update:sub_mb=" + LogUtils.toJson(subMb));
        return mess;
    }

    public String updateSubMbByCustId(Session cmPreSession, String locale, String name,
            String DOB, String contact, String address, String Sex, Long custId, Long subId) throws ParseException {
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="name">
        if (name == null) {
            mess = LabelUtil.getKey("validate.required", locale, "custName");
            return mess;
        }
        name = name.trim();

        Integer length = ReflectUtils.getColumnLength(SubMbPre.class, "subName");
        if (length != null && length > 0 && name.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("custName", locale), length);
            return mess;
        }
        //</editor-fold>
        Date nowDate = getSysDateTime(cmPreSession);
        //<editor-fold defaultstate="collapsed" desc="birthDate">
        if (DOB == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        DOB = DOB.trim();
        if (DOB.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (!ValidateUtils.isDateddMMyyyy(DOB)) {
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        Date bDate;
        try {
            bDate = DateTimeUtils.toDateddMMyyyy(DOB);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        if (bDate == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (bDate.after(nowDate)) {
            mess = LabelUtil.getKey("greater.than", locale, "birthDate", "nowDate");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="contact">
        contact = contact.trim();
        if (!contact.isEmpty()) {
            length = ReflectUtils.getColumnLength(SubMbPre.class, "telMobile");
            if (length != null && length > 0 && contact.length() > length) {
                mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("telMobile", locale), length);
                return mess;
            }
        }

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="address">
        if (address != null) {
            address = address.trim();
            if (!address.isEmpty()) {
                length = ReflectUtils.getColumnLength(SubMbPre.class, "address");
                if (length != null && length > 0 && address.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("streetBlock", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="gender">
        if (Sex != null) {
            Sex = Sex.trim();
            if (!Sex.isEmpty()) {
                length = ReflectUtils.getColumnLength(SubMbPre.class, "gender");
                if (length != null && length > 0 && Sex.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("sex", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>

        //</editor-fold>
        SubMbPre subMb = new SubMbPre();
        subMb.setSubName(name);
        subMb.setBirthDate(bDate);
        subMb.setCustId(custId);
        subMb.setTelMobile(contact);
        subMb.setAddress(address);
        subMb.setGender(Sex);
//        cmPreSession.update(subMb);
//        cmPreSession.flush();
        try {
            String sql = "update sub_mb set sub_name = ?,birth_date = ?, cust_id = ?, "
                    + "tel_mobile = ?, address = ?, gender = ? where cust_id = ? ";
            Query query = cmPreSession.createSQLQuery(sql);
            query.setParameter(0, subMb.getSubName());
            query.setParameter(1, subMb.getBirthDate());
            query.setParameter(2, subMb.getCustId());
            query.setParameter(3, subMb.getTelMobile());
            query.setParameter(4, subMb.getAddress());
            query.setParameter(5, subMb.getGender());
            query.setParameter(6, subMb.getCustId());
            query.executeUpdate();
            cmPreSession.flush();
            mess = ConvertUtils.toStringValue(1L);
        } catch (Exception ex) {
            mess = ConvertUtils.toStringValue(0L);
        }
        LogUtils.info(logger, "RequestChangeInformationSupplier.update:sub_mb=" + LogUtils.toJson(subMb));
        return mess;
    }

    public String updateSubMb(Session cmPreSession, String locale, String name,
            String DOB, String contact, String address, String Sex, Long custId, Long subId) throws ParseException {
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="name">
        if (name == null) {
            mess = LabelUtil.getKey("validate.required", locale, "custName");
            return mess;
        }
        name = name.trim();

        Integer length = ReflectUtils.getColumnLength(SubMbPre.class, "subName");
        if (length != null && length > 0 && name.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("custName", locale), length);
            return mess;
        }
        //</editor-fold>
        Date nowDate = getSysDateTime(cmPreSession);
        //<editor-fold defaultstate="collapsed" desc="birthDate">
        if (DOB == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        DOB = DOB.trim();
        if (DOB.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (!ValidateUtils.isDateddMMyyyy(DOB)) {
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        Date bDate;
        try {
            bDate = DateTimeUtils.toDateddMMyyyy(DOB);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        if (bDate == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (bDate.after(nowDate)) {
            mess = LabelUtil.getKey("greater.than", locale, "birthDate", "nowDate");
            return mess;
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="contact">
        contact = contact.trim();
        if (!contact.isEmpty()) {
            length = ReflectUtils.getColumnLength(SubMbPre.class, "telMobile");
            if (length != null && length > 0 && contact.length() > length) {
                mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("telMobile", locale), length);
                return mess;
            }
        }

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="address">
        if (address != null) {
            address = address.trim();
            if (!address.isEmpty()) {
                length = ReflectUtils.getColumnLength(SubMbPre.class, "address");
                if (length != null && length > 0 && address.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("streetBlock", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="gender">
        if (Sex != null) {
            Sex = Sex.trim();
            if (!Sex.isEmpty()) {
                length = ReflectUtils.getColumnLength(SubMbPre.class, "gender");
                if (length != null && length > 0 && Sex.length() > length) {
                    mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("sex", locale), length);
                    return mess;
                }
            }
        }
        //</editor-fold>

        //</editor-fold>
        SubMbPre subMb = new SubMbPre();
        subMb.setSubName(name);
        subMb.setBirthDate(bDate);
        subMb.setCustId(custId);
        subMb.setTelMobile(contact);
        subMb.setAddress(address);
        subMb.setGender(Sex);
        subMb.setSubId(subId);
//        cmPreSession.update(subMb);
//        cmPreSession.flush();
        try {
            String sql = "update sub_mb set sub_name = ?,birth_date = ?, cust_id = ?, "
                    + "tel_mobile = ?, address = ?, gender = ? where sub_id = ? ";
            Query query = cmPreSession.createSQLQuery(sql);
            query.setParameter(0, subMb.getSubName());
            query.setParameter(1, subMb.getBirthDate());
            query.setParameter(2, subMb.getCustId());
            query.setParameter(3, subMb.getTelMobile());
            query.setParameter(4, subMb.getAddress());
            query.setParameter(5, subMb.getGender());
            query.setParameter(6, subMb.getSubId());
            query.executeUpdate();
            cmPreSession.flush();
            mess = ConvertUtils.toStringValue(subMb.getSubId());
        } catch (Exception ex) {
            mess = ConvertUtils.toStringValue(0L);
        }
        LogUtils.info(logger, "RequestChangeInformationSupplier.update:sub_mb=" + LogUtils.toJson(subMb));
        return mess;
    }

    public String insertSubIdNo(Session cmPreSession, Long subId, String idNo, String isdn) {
        String mess;
        try {
            String sql = "insert into sub_id_no (SUB_ID,STA_DATETIME,ID_NO,APP,STATUS,ISDN) "
                    + " values(?,sysdate,?,?,?,?)";
            Query query = cmPreSession.createSQLQuery(sql);
            query.setParameter(0, subId);
            query.setParameter(1, idNo);
            query.setParameter(2, "CM");
            query.setParameter(3, "1");
            query.setParameter(4, isdn);
            query.executeUpdate();
            cmPreSession.flush();
            mess = ConvertUtils.toStringValue(1L);
        } catch (Exception e) {
            mess = ConvertUtils.toStringValue(0L);
        }
        LogUtils.info(logger, "CustomerBussiness.insert:sub_id_no.sub_id=" + LogUtils.toJson(subId));
        return mess;
    }

    public String insertCustomer(Session cmPreSession, String locale, String name,
            String DOB, String Sex, String idNo, Long idType, String province,
            String address, String addedUser, Date addedDate, String updatedUser,
            Date updatedDate, String contact) throws ParseException {
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="idNo">
        if (idNo == null) {
            mess = LabelUtil.getKey("validate.required", locale, "idNo");
            return mess;
        }
        idNo = idNo.trim();
        if (idNo.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "idNo");
            return mess;
        }
        Integer length = ReflectUtils.getColumnLength(CustomerPre.class, "idNo");
        if (length != null && length > 0 && idNo.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("idNo", locale), length);
            return mess;
        }
        //</editor-fold>
        Date nowDate = getSysDateTime(cmPreSession);
        //<editor-fold defaultstate="collapsed" desc="birthDate">
        if (DOB == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        DOB = DOB.trim();
        if (DOB.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (!ValidateUtils.isDateddMMyyyy(DOB)) {
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        Date bDate;
        try {
            bDate = DateTimeUtils.toDateddMMyyyy(DOB);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        if (bDate == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (bDate.after(nowDate)) {
            mess = LabelUtil.getKey("greater.than", locale, "birthDate", "nowDate");
            return mess;
        }
        //</editor-fold>
        //</editor-fold>
        CustomerPre cus = new CustomerPre();
        cus.setName(name);
        cus.setIdType(idType);
        cus.setIdNo(idNo);
        cus.setProvince(province);

        cus.setAddress(address);
        if (addedUser != null) {
            cus.setAddedUser(addedUser.trim().toUpperCase());
        }
        cus.setBirthDate(bDate);
        cus.setSex(Sex);
        cus.setBusType("INDN");
        cus.setStatus(Constants.STATUS_USE);
        cus.setTelFax(contact);
        cus.setNotes("Change information by mBCCS");
        Long custId = getSequence(cmPreSession, Constants.SEQ_CUSTOMER);
        cus.setCustId(custId);
        Long custIdReturn;
        try {
            String sql = "insert into customer (cust_id,bus_type,name,id_no,id_type,province,"
                    + "address,added_user,added_date,birth_date,sex,status,CORRECT_CUS,tel_fax,notes) "
                        + " values(?,?,?,?,?,?,?,?,sysdate,?,?,?,?,?,?)";
            Query query = cmPreSession.createSQLQuery(sql);
            query.setParameter(0, cus.getCustId());
            query.setParameter(1, cus.getBusType());
            query.setParameter(2, cus.getName());
            query.setParameter(3, cus.getIdNo());
            query.setParameter(4, cus.getIdType());
            query.setParameter(5, cus.getProvince());
            query.setParameter(6, cus.getAddress());
            query.setParameter(7, cus.getAddedUser());
            query.setParameter(8, cus.getBirthDate());
            query.setParameter(9, cus.getSex());
            query.setParameter(10, Constant.STATUS_USE);
            query.setParameter(11, "1");
            query.setParameter(12, cus.getTelFax());
            query.setParameter(13, cus.getNotes());
            query.executeUpdate();
            cmPreSession.flush();
            custIdReturn = cus.getCustId();
        } catch (Exception e) {
            custIdReturn = 0L;
        }
        LogUtils.info(logger, "CustomerBussiness.insert:cus=" + LogUtils.toJson(cus));
        mess = ConvertUtils.toStringValue(custIdReturn);
        return mess;
    }


    public String updateCustomerChangeInfo(Session cmPreSession, String locale, String name,
            Long idType, String idNo, String DOB, String address, Long custId, String contact,
            String sex, String province)
            throws ParseException {
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="idNo">
        if (idNo == null) {
            mess = LabelUtil.getKey("validate.required", locale, "idNo");
            return mess;
        }
        idNo = idNo.trim();
        if (idNo.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "idNo");
            return mess;
        }
        Integer length = ReflectUtils.getColumnLength(CustomerPre.class, "idNo");
        if (length != null && length > 0 && idNo.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("idNo", locale), length);
            return mess;
        }
        //</editor-fold>
        Date nowDate = getSysDateTime(cmPreSession);
        //<editor-fold defaultstate="collapsed" desc="birthDate">
        if (DOB == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        DOB = DOB.trim();
        if (DOB.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (!ValidateUtils.isDateddMMyyyy(DOB)) {
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        Date bDate;
        try {
            bDate = DateTimeUtils.toDateddMMyyyy(DOB);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        if (bDate == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (bDate.after(nowDate)) {
            mess = LabelUtil.getKey("greater.than", locale, "birthDate", "nowDate");
            return mess;
        }
        //</editor-fold>
        //</editor-fold>
        CustomerPre customer = new CustomerPre();
        customer.setName(name);
        customer.setIdType(idType);
        customer.setIdNo(idNo.toUpperCase());
        customer.setBirthDate(bDate);
        customer.setAddress(address);
        customer.setCustId(custId);
        customer.setTelFax(contact);
        customer.setSex(sex);
        customer.setProvince(province);
        customer.setNotes("Change information by mBCCS");
//        cmPreSession.update(customer);
//        cmPreSession.flush();
        try {
            System.out.println("supplier . customer.getAddress() = " + customer.getAddress());
            cmPreSession.beginTransaction();
            String sql = "update customer set name = ?,id_type = ?, id_no = ?, "
                    + "birth_date = ?, address = ?, tel_fax = ?, notes = ?, sex = ?, province = ? "
                    + "where cust_id = ? ";
            Query query = cmPreSession.createSQLQuery(sql);
            query.setParameter(0, customer.getName());
            query.setParameter(1, customer.getIdType());
            query.setParameter(2, customer.getIdNo());
            query.setParameter(3, customer.getBirthDate());
            query.setParameter(4, customer.getAddress());
            query.setParameter(5, customer.getTelFax());
            query.setParameter(6, customer.getNotes());
            query.setParameter(7, customer.getSex());
            query.setParameter(8, customer.getProvince());
            query.setParameter(9, customer.getCustId());
            query.executeUpdate();
            cmPreSession.flush();
            mess = ConvertUtils.toStringValue(customer.getCustId());
            System.out.println("updateCustomer " + customer.getCustId() + " mess = " + mess);
        } catch (Exception ex) {
            mess = ConvertUtils.toStringValue(0L);
        }
        LogUtils.info(logger, "RequestChangeInformationSupplier.update:customer=" + LogUtils.toJson(customer));
        return mess;
    }

    public String updateCustomer(Session cmPreSession, String locale, String name,
            Long idType, String idNo, String DOB, String address, Long custId, String contact) throws ParseException {
        String mess;
        //<editor-fold defaultstate="collapsed" desc="validate">
        //<editor-fold defaultstate="collapsed" desc="idNo">
        if (idNo == null) {
            mess = LabelUtil.getKey("validate.required", locale, "idNo");
            return mess;
        }
        idNo = idNo.trim();
        if (idNo.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "idNo");
            return mess;
        }
        Integer length = ReflectUtils.getColumnLength(CustomerPre.class, "idNo");
        if (length != null && length > 0 && idNo.length() > length) {
            mess = LabelUtil.formatKey("validate.maxlength", locale, LabelUtil.getKey("idNo", locale), length);
            return mess;
        }
        //</editor-fold>
        Date nowDate = getSysDateTime(cmPreSession);
        //<editor-fold defaultstate="collapsed" desc="birthDate">
        if (DOB == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        DOB = DOB.trim();
        if (DOB.isEmpty()) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (!ValidateUtils.isDateddMMyyyy(DOB)) {
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        Date bDate;
        try {
            bDate = DateTimeUtils.toDateddMMyyyy(DOB);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            mess = LabelUtil.formatKey("validate.invalid.date", locale, LabelUtil.getKey("birthDate", locale), DateTimeUtils.DATE_PATTERN_ddMMyyyy);
            return mess;
        }
        if (bDate == null) {
            mess = LabelUtil.getKey("validate.required", locale, "birthDate");
            return mess;
        }
        if (bDate.after(nowDate)) {
            mess = LabelUtil.getKey("greater.than", locale, "birthDate", "nowDate");
            return mess;
        }
        //</editor-fold>
        //</editor-fold>
        System.out.println("supplier . address = " + address);
        CustomerPre customer = new CustomerPre();
        customer.setName(name);
        customer.setIdType(idType);
        customer.setIdNo(idNo.toUpperCase());
        customer.setBirthDate(bDate);
        customer.setAddress(address);
        customer.setCustId(custId);
        customer.setTelFax(contact);
        customer.setNotes("Change information by mBCCS");
//        cmPreSession.update(customer);
//        cmPreSession.flush();
        try {
            System.out.println("supplier . customer.getAddress() = " + customer.getAddress());
            String sql = "update customer set name = ?,id_type = ?, id_no = ?, "
                    + "birth_date = ?, address = ?, tel_fax = ?, notes = ? where cust_id = ? ";
            Query query = cmPreSession.createSQLQuery(sql);
            query.setParameter(0, customer.getName());
            query.setParameter(1, customer.getIdType());
            query.setParameter(2, customer.getIdNo());
            query.setParameter(3, customer.getBirthDate());
            query.setParameter(4, customer.getAddress());
            query.setParameter(5, customer.getTelFax());
            query.setParameter(6, customer.getNotes());
            query.setParameter(7, customer.getCustId());
            query.executeUpdate();
            cmPreSession.flush();
            mess = ConvertUtils.toStringValue(customer.getCustId());
            System.out.println("updateCustomer " + customer.getCustId() + " mess = " + mess);
        } catch (Exception ex) {
            mess = ConvertUtils.toStringValue(0L);
        }
        LogUtils.info(logger, "RequestChangeInformationSupplier.update:customer=" + LogUtils.toJson(customer));
        return mess;
    }

    public List<ReasonPre> getReasonFromCodes(Session cmPreSession, List<String> codes) {
        StringBuilder sql = new StringBuilder().append("SELECT code code, name name, "
                + "reason_id reasonId FROM reason WHERE status = 1 ");
        if (codes == null || codes.size() <= 0) {
            sql.append(" AND (1=1)");
        } else {
            sql.append(" AND code IN (:codes) ");
        }
        Query query = cmPreSession.createSQLQuery(sql.toString())
                .addScalar("code", Hibernate.STRING)
                .addScalar("name", Hibernate.STRING)
                .addScalar("reasonId", Hibernate.LONG)
                .setResultTransformer(Transformers.aliasToBean(ReasonPre.class));
        
        if (codes == null || codes.size() <= 0) {
        } else {
            query.setParameterList("codes", codes);
        }
        List<ReasonPre> result = query.list();
        return result;
    }


    
    
    public OTPObject getOTPFromService_(String isdn) {
        try {
            List<Param> lstParam = new ArrayList<Param>();
            Param prName = new Param();
            lstParam.add(new Param("isdn", isdn));
            OTPObject resultGetOTP = (OTPObject) new WebServiceClient().sendRequestViaBccsGW_Param("getOtp", OTPObject.class, lstParam);
            LogUtils.info(logger, "Change information getOTP: " + LogUtils.toJson(resultGetOTP));
            return resultGetOTP;
        } catch (Exception exception) {
            LogUtils.error(logger, exception.getMessage(), exception);
            logger.error("Error get otp", exception);
            return null;
        }
    }
    
    public boolean checkOTP_(String isdn, String otp){
        try {
            List<Param> lstParam = new ArrayList<Param>();
            //isdn
            lstParam.add(new Param("isdn", isdn));
            //otp
            lstParam.add(new Param("otp", otp));
            OTPObject resultGetOTP = (OTPObject) new WebServiceClient().sendRequestViaBccsGW_Param("checkOtp", OTPObject.class, lstParam);
            LogUtils.info(logger, "Change information checkOTP: " + LogUtils.toJson(resultGetOTP));
            if(resultGetOTP != null && "00".equals(resultGetOTP.getErrorCode())){
                return true;
            }
            return false;
        } catch (Exception exception) {
            LogUtils.error(logger, exception.getMessage(), exception);
            return false;
        }
    }


}
