/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.bussiness.SubFbConfigBusiness;
import com.viettel.bccs.cm.dao.CustImageDetailDAO;
import com.viettel.bccs.cm.model.PotentialCustomer;
import com.viettel.bccs.cm.model.PotentialCustomerHistory;
import com.viettel.bccs.cm.model.PotentialCustomerHistoryDto;
import com.viettel.bccs.cm.model.RequestUpdateLocation;
import com.viettel.bccs.cm.model.SubFbConfig;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.ws.model.input.NewImageInput;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author duyetdk
 */
public class RequestUpdateLocationSupplier extends BaseSupplier {

    public RequestUpdateLocationSupplier() {
        logger = Logger.getLogger(RequestUpdateLocationSupplier.class);
    }

    public RequestUpdateLocation insert(Session cmPosSession, Long custId, String cusName, String address, Long staffId,
            Date nowDate, String xLocation, String yLocation, String cxLocation, String cyLocation, String createUser,
            com.viettel.im.database.BO.Shop shop, String staffOwnType, String reasonRequest, String account,
            Long custImageId) throws Exception {

        RequestUpdateLocation re = new RequestUpdateLocation();
        Long rulId = getSequence(cmPosSession, Constants.REQUEST_UPDATE_LOCATION_SEQ);
        re.setRulId(rulId);
        re.setCusId(custId);
        re.setxLocation(xLocation);
        re.setyLocation(yLocation);
        re.setCxLocation(cxLocation);
        re.setCyLocation(cyLocation);
        re.setCreateUser(createUser.trim().toUpperCase());
        re.setCreateDate(nowDate);
        re.setLastUpdateDate(nowDate);
        re.setStatus(Constants.WAITING);
        re.setCusName(cusName);
        re.setAddress(address);
        re.setStaffId(staffId);
        re.setShopId(shop.getShopId());
        re.setShopPath(shop.getShopPath() + "_");
        re.setProvince(shop.getProvince());
        re.setStaffOwnType(staffOwnType);
        re.setReasonRequest(reasonRequest);
        re.setAccount(account);
        re.setCustImageId(custImageId);
        cmPosSession.save(re);
        cmPosSession.flush();
        return re;
    }

    public void buildQuery(StringBuilder sql, HashMap params, Date date, Long status, Long duration,
            String province, String createUser, String cusName) {
        if (sql == null || params == null) {
            return;
        }
        if (duration != null && duration > 0L) {
            sql.append(" and lastUpdateDate >= trunc(sysdate - :duration) ");
            params.put("duration", duration);
        }
        if (duration != null && duration > Long.parseLong(ResourceUtils.getResource("DURATION"))) {
            sql.append(" and lastUpdateDate >= trunc(sysdate - :durationOver) ");
            params.put("durationOver", Long.parseLong(ResourceUtils.getResource("DURATION")));
        }
//        if (date != null) {
//            sql.append(" and trunc(createDate) = trunc(:createDate) ");
//            params.put("createDate", date);
//        }
        if ((duration == null || duration <= 0L)) {
            sql.append(" and lastUpdateDate >= trunc(sysdate) ");
        }
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        if (createUser != null && !createUser.trim().isEmpty()) {
            sql.append(" and lower(createUser) like :createUser ");
            params.put("createUser", "%" + createUser.trim().toLowerCase() + "%");
        }
        if (cusName != null && !cusName.trim().isEmpty()) {
            sql.append(" and lower(cusName) like :cusName ");
            params.put("cusName", "%" + cusName.trim().toLowerCase() + "%");
        }
        if (!"VTC".equals(province)) {
            sql.append(" and province = :province ");
            params.put("province", province);
        }

    }

    public Long count(Session cmPosSession, Date date, Long status, Long duration, String province, String createUser, String custName) {
        StringBuilder sql = new StringBuilder().append(" select count(*) from RequestUpdateLocation where 1 = 1 ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        buildQuery(sql, params, date, status, duration, province, createUser, custName);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public List<RequestUpdateLocation> find(Session cmPosSession, Date date, Long status, Long duration, Integer start, Integer max,
            String province, String createUser, String custName) {
        StringBuilder sql = new StringBuilder().append(" select a.rul_Id rulId,a.cus_Id cusId,a.cus_Name cusName,a.address address,a.staff_Id staffId,a.latitude xLocation, "
                + " a.longitude yLocation,a.current_latitude cxLocation,a.current_longitude cyLocation,a.status status,a.description description,a.create_User createUser,a.create_Date createDate,a.last_Update_User lastUpdateUser, "
                + " a.last_Update_Date lastUpdateDate,(select count(b.rul_Id) from Request_Update_Location b "
                + " where b.status = :stt and b.cus_Id = a.cus_Id) countApproved"
                + " from Request_Update_Location a where 1 = 1 ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("stt", Constants.APPROVED);
        if (duration != null && duration > 0L) {
            sql.append(" and a.last_Update_Date >= trunc(sysdate - :duration) ");
            params.put("duration", duration);
        }
        if (duration != null && duration > Long.parseLong(ResourceUtils.getResource("DURATION"))) {
            sql.append(" and a.last_Update_Date >= trunc(sysdate - :durationOver) ");
            params.put("durationOver", Long.parseLong(ResourceUtils.getResource("DURATION")));
        }

        if ((duration == null || duration <= 0L)) {
            sql.append(" and a.last_Update_Date >= trunc(sysdate) ");
        }
        if (status != null) {
            sql.append(" and a.status = :status ");
            params.put("status", status);
        }
        if (createUser != null && !createUser.trim().isEmpty()) {
            sql.append(" and lower(a.create_User) like :createUser ");
            params.put("createUser", "%" + createUser.trim().toLowerCase() + "%");
        }
        if (custName != null && !custName.trim().isEmpty()) {
            sql.append(" and lower(a.cus_Name) like :cusName ");
            params.put("cusName", "%" + custName.trim().toLowerCase() + "%");
        }
        if (!"VTC".equals(province)) {
            sql.append(" and a.province = :province ");
            params.put("province", province);
        }
        sql.append(" order by a.last_Update_Date desc ");
        Query query = cmPosSession.createSQLQuery(sql.toString())
                .addScalar("rulId", Hibernate.LONG)
                .addScalar("cusId", Hibernate.LONG)
                .addScalar("cusName", Hibernate.STRING)
                .addScalar("address", Hibernate.STRING)
                .addScalar("staffId", Hibernate.LONG)
                .addScalar("xLocation", Hibernate.STRING)
                .addScalar("yLocation", Hibernate.STRING)
                .addScalar("cxLocation", Hibernate.STRING)
                .addScalar("cyLocation", Hibernate.STRING)
                .addScalar("status", Hibernate.LONG)
                .addScalar("description", Hibernate.STRING)
                .addScalar("createUser", Hibernate.STRING)
                .addScalar("createDate", Hibernate.TIMESTAMP)
                .addScalar("lastUpdateUser", Hibernate.STRING)
                .addScalar("lastUpdateDate", Hibernate.TIMESTAMP)
                .addScalar("countApproved", Hibernate.LONG)
                .setResultTransformer(Transformers.aliasToBean(RequestUpdateLocation.class));
        buildParameter(query, params);
        if (start != null) {
            query.setFirstResult(start);
        }
        if (max != null) {
            query.setMaxResults(max);
        }
        List<RequestUpdateLocation> result = query.list();
        return result;
    }

    public List<RequestUpdateLocation> findById(Session cmPosSession, Long rulId, Long status, String province) {
        StringBuilder sql = new StringBuilder().append(" from RequestUpdateLocation where rulId = :rulId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("rulId", rulId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        if (!"VTC".equals(province)) {
            sql.append(" and province = :province ");
            params.put("province", province);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<RequestUpdateLocation> result = query.list();
        return result;
    }

    public Long countById(Session cmPosSession, Long cusId, Long status) {
        StringBuilder sql = new StringBuilder().append(" select count(*) from RequestUpdateLocation where cusId = :cusId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("cusId", cusId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public Long countApproved(Session cmPosSession, Long cusId) {
        StringBuilder sql = new StringBuilder().append(" select count(*) from RequestUpdateLocation where cusId = :cusId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("cusId", cusId);

        sql.append(" and status = :status ");
        params.put("status", Constants.APPROVED);

        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public List<RequestUpdateLocation> findApproved(Session cmPosSession, Long cusId, Integer start, Integer max) {
        StringBuilder sql = new StringBuilder().append(" from RequestUpdateLocation where cusId = :cusId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("cusId", cusId);
        sql.append(" and status = :status ");
        params.put("status", Constants.APPROVED);
        sql.append(" order by createDate desc ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        if (start != null) {
            query.setFirstResult(start);
        }
        if (max != null) {
            query.setMaxResults(max);
        }
        List<RequestUpdateLocation> result = query.list();
        return result;
    }

    public Long countWaiting30days(Session cmPosSession, Long cusId) throws Exception {
        StringBuilder sql = new StringBuilder().append(" select count(*) from RequestUpdateLocation where cusId = :cusId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("cusId", cusId);
        sql.append(" and status = :status ");
        params.put("status", Constants.WAITING);
        sql.append(" and createDate >= trunc(sysdate - :duration) ");
        params.put("duration", Long.parseLong(ResourceUtils.getResource("DURATION")));
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public List<RequestUpdateLocation> findWaiting(Session cmPosSession, String province, Date date, Long duration, String createUser, String cusName) {
        StringBuilder sql = new StringBuilder().append(" from RequestUpdateLocation where status = :status ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("status", Constants.WAITING);
        if (!"VTC".equals(province)) {
            sql.append(" and province = :province ");
            params.put("province", province);
        }
        if (duration != null && duration > 0L) {
            sql.append(" and createDate >= trunc(sysdate - :duration) ");
            params.put("duration", duration);
        }
        if (duration != null && duration > Long.parseLong(ResourceUtils.getResource("DURATION"))) {
            sql.append(" and createDate >= trunc(sysdate - :durationOver) ");
            params.put("durationOver", Long.parseLong(ResourceUtils.getResource("DURATION")));
        }

        if ((duration == null || duration <= 0L)) {
            sql.append(" and createDate >= trunc(sysdate) ");
        }
        if (createUser != null && !createUser.trim().isEmpty()) {
            sql.append(" and lower(createUser) like lower(:createUser) ");
            params.put("createUser", "%" + createUser + "%");
        }
        if (cusName != null && !cusName.trim().isEmpty()) {
            sql.append(" and lower(cusName) like lower(:cusName) ");
            params.put("cusName", "%" + cusName + "%");
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<RequestUpdateLocation> result = query.list();
        return result;
    }

    public Long countWaitingOver30days(Session cmPosSession, Long cusId) {
        StringBuilder sql = new StringBuilder().append(" select count(*) from RequestUpdateLocation where cusId = :cusId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("cusId", cusId);
        sql.append(" and status = :status ");
        params.put("status", Constants.WAITING);
        sql.append(" and createDate < trunc(sysdate - :duration) ");
        params.put("duration", Long.parseLong(ResourceUtils.getResource("DURATION")));
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public List<RequestUpdateLocation> findWaitingOver30days(Session cmPosSession, Long cusId) {
        StringBuilder sql = new StringBuilder().append(" from RequestUpdateLocation where cusId = :cusId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("cusId", cusId);
        sql.append(" and status = :status ");
        params.put("status", Constants.WAITING);
        sql.append(" and createDate < trunc(sysdate - :duration) ");
        params.put("duration", Long.parseLong(ResourceUtils.getResource("DURATION")));
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<RequestUpdateLocation> result = query.list();
        return result;
    }

    public Long countApprovedAndReject(Session cmPosSession, Long cusId) {
        StringBuilder sql = new StringBuilder().append(" select count(*) from RequestUpdateLocation where cusId = :cusId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("cusId", cusId);
//        sql.append(" and lastUpdateDate >= trunc(sysdate - :durationOver) ");
//        params.put("durationOver", Long.parseLong(ResourceUtils.getResource("DURATION")));

        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public List<RequestUpdateLocation> findApprovedAndReject(Session cmPosSession, Long cusId, Integer start, Integer max) {
        StringBuilder sql = new StringBuilder().append(" select a.rul_Id rulId,a.cus_Id cusId,a.cus_Name cusName,a.address address,a.staff_Id staffId,a.latitude xLocation, "
                + " a.longitude yLocation,a.current_latitude cxLocation,a.current_longitude cyLocation,a.status status,a.description description,a.create_User createUser,a.create_Date createDate,a.last_Update_User lastUpdateUser, "
                + " a.last_Update_Date lastUpdateDate "
                + " from Request_Update_Location a where a.cus_Id = :cusId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("cusId", cusId);
//        sql.append(" and a.last_Update_Date >= trunc(sysdate - :durationOver) ");
//        params.put("durationOver", Long.parseLong(ResourceUtils.getResource("DURATION")));
        sql.append(" order by a.last_Update_Date desc ");
        Query query = cmPosSession.createSQLQuery(sql.toString())
                .addScalar("rulId", Hibernate.LONG)
                .addScalar("cusId", Hibernate.LONG)
                .addScalar("cusName", Hibernate.STRING)
                .addScalar("address", Hibernate.STRING)
                .addScalar("staffId", Hibernate.LONG)
                .addScalar("xLocation", Hibernate.STRING)
                .addScalar("yLocation", Hibernate.STRING)
                .addScalar("cxLocation", Hibernate.STRING)
                .addScalar("cyLocation", Hibernate.STRING)
                .addScalar("status", Hibernate.LONG)
                .addScalar("description", Hibernate.STRING)
                .addScalar("createUser", Hibernate.STRING)
                .addScalar("createDate", Hibernate.TIMESTAMP)
                .addScalar("lastUpdateUser", Hibernate.STRING)
                .addScalar("lastUpdateDate", Hibernate.TIMESTAMP)
                .setResultTransformer(Transformers.aliasToBean(RequestUpdateLocation.class));
        buildParameter(query, params);
        if (start != null) {
            query.setFirstResult(start);
        }
        if (max != null) {
            query.setMaxResults(max);
        }
        List<RequestUpdateLocation> result = query.list();
        return result;
    }

    public List<NewImageInput> getImageRequestLocationCustomer(Session cmPosSession, Long requestId, String typeImage) {
        /* 
         * requestId: Request_update_location_id
         *          : TASK_management_id 
         */
        List<NewImageInput> listImage = new ArrayList<NewImageInput>();
        CustImageDetailDAO custImageDetailDAO = new CustImageDetailDAO();
        /*Get image khi update location*/
        if (typeImage == null || "?".equals(typeImage) || "".equals(typeImage)) {

            List<RequestUpdateLocation> request = findById(cmPosSession, requestId, null, "VTC");

            if (request != null && !request.isEmpty()) {
                if (request.get(0).getCustImageId() != null) {
                    listImage = new CustImageDetailDAO().findById(cmPosSession, request.get(0).getCustImageId(), Constants.IMAGE_TYPE_LOCATION_CUSTOMER + "_" + request.get(0).getAccount(), logger, Constants.FTP_DIR_TASK);
                }
            }
        } else if (typeImage.equals(Constants.TYPE_IMAGE_INFRATRUCTURE)) {
            /*Get image ha tang khi view thong tin khi config imt*/
            SubFbConfig subFbConf = new SubFbConfigBusiness().findByTaskMngtId(cmPosSession, requestId, null);
            if (subFbConf != null) {
                /*Get infra image id -> imageInfra*/
                if (subFbConf.getIfraImgId() != null) {
                    List<NewImageInput> temp = custImageDetailDAO.findById(cmPosSession, subFbConf.getIfraImgId(), Constants.INFRA_IMG + "_" + subFbConf.getAccount(), logger, Constants.FTP_DIR_TASK);
                    if (temp != null && !temp.isEmpty()) {
                        listImage.addAll(temp);
                    }
                }
            }
        } else if (typeImage.equals(Constants.TYPE_IMAGE_GOOD)) {
            SubFbConfig subFbConf = new SubFbConfigBusiness().findByTaskMngtId(cmPosSession, requestId, null);
            if (subFbConf != null) {
                /*Get cust image id -> imageGood*/
                if (subFbConf.getCustImgId() != null) {
                    List<NewImageInput> temp = custImageDetailDAO.findById(cmPosSession, subFbConf.getCustImgId(), Constants.GOOD_IMG + "_" + subFbConf.getAccount(), logger, Constants.FTP_DIR_TASK);
                    if (temp != null && !temp.isEmpty()) {
                        listImage.addAll(temp);
                    }
                }
            }
        } else if (typeImage.equals(Constants.TYPE_IMAGE_SUB_ACTION)) {
            List<NewImageInput> temp = custImageDetailDAO.findById(cmPosSession, requestId, "", logger, Constants.FTP_DIR_SUB_ACTION);
            if (temp != null && !temp.isEmpty()) {
                listImage.addAll(temp);
            }
        }
        return listImage;
    }

    public List<RequestUpdateLocation> findByCustId(Session cmPosSession, Long custId, Long status) {
        StringBuilder sql = new StringBuilder().append(" FROM RequestUpdateLocation WHERE cusId = :custId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("custId", custId);
        if (status != null) {
            sql.append(" AND status = :status ");
            params.put("status", status);
        }
        sql.append(" ORDER BY create_date DESC ");

        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<RequestUpdateLocation> result = query.list();
        return result;
    }

    /**
     * @param account
     * @author: duyetdk
     * @since 06/6/2019
     * @des lay anh nha KH
     * @param cmPosSession
     * @param custId
     * @return
     * @throws Exception
     */
    public List<String> getImageLocationCustomer(Session cmPosSession, Long custId, String account) throws Exception {
        List<String> listImage = null;
//        List<RequestUpdateLocation> request = findByCustId(cmPosSession, custId, Constants.APPROVED);
        List<RequestUpdateLocation> lstRequest = findByCustId(cmPosSession, custId, null);
        List<String> list = new CustImageDetailDAO().getCustImageIdByAccount(cmPosSession, account);

        if (lstRequest != null && !lstRequest.isEmpty() && list != null && !list.isEmpty()) {
            String custImageId = String.valueOf(list.get(0));
            String preCustImageId = String.valueOf(0L);
            if (list.size() > 1) {
                preCustImageId = String.valueOf(list.get(1));
            }
            if (lstRequest.get(0).getCustImageId().equals(Long.valueOf(custImageId))) {
                if (Constants.APPROVED.equals(lstRequest.get(0).getStatus())) {
                    listImage = new CustImageDetailDAO().getCustImageLocation(cmPosSession, Long.valueOf(custImageId),
                            Constants.IMAGE_TYPE_LOCATION_CUSTOMER + "_" + account, logger);
                } else {
                    listImage = new CustImageDetailDAO().getCustImageLocation(cmPosSession, Long.valueOf(preCustImageId),
                            Constants.IMAGE_TYPE_LOCATION_CUSTOMER + "_" + account, logger);
                }
            } else {
                listImage = new CustImageDetailDAO().getCustImageLocation(cmPosSession, Long.valueOf(custImageId),
                        Constants.IMAGE_TYPE_LOCATION_CUSTOMER + "_" + account, logger);
            }
        } else {
            if (list != null && !list.isEmpty()) {
                String custImageId = String.valueOf(list.get(0));
                listImage = new CustImageDetailDAO().getCustImageLocation(cmPosSession, Long.valueOf(custImageId),
                        Constants.IMAGE_TYPE_LOCATION_CUSTOMER + "_" + account, logger);
            }
        }

        return listImage;
    }

    /**
     * @author duyetdk
     * @des lay account brcd
     * @param cmPosSession
     * @param custId
     * @return
     */
    public List<String> getAccountByCustId(Session cmPosSession, Long custId) {
        try {
            if (custId == null || custId == 0L) {
                return null;
            }
            String sql = "SELECT NVL(sub.account, sub.isdn) account "
                    + "  FROM customer cu, contract co, sub_adsl_ll sub "
                    + "  WHERE cu.cust_id = co.cust_id AND co.contract_id = sub.contract_id AND cu.cust_id = ? ";
            Query query = cmPosSession.createSQLQuery(sql);
            query.setParameter(0, custId);
            List lst = query.list();
            if (lst != null && !lst.isEmpty()) {
                return lst;
            }
        } catch (HibernateException ex) {
            System.out.println(ex);
        }
        return null;
    }

    /**
     * @author duyetdk
     * @since 05/07/2019
     * @des lay thue bao mobile tra sau
     * @param cmPosSession
     * @param custId
     * @return
     */
    public List<String> getMobileByCustId(Session cmPosSession, Long custId) {
        try {
            if (custId == null || custId == 0L) {
                return null;
            }
            String sql = "SELECT sub.isdn "
                    + "  FROM customer cu, contract co, sub_mb sub "
                    + "  WHERE cu.cust_id = co.cust_id AND co.contract_id = sub.contract_id AND cu.cust_id = ? ";
            Query query = cmPosSession.createSQLQuery(sql);
            query.setParameter(0, custId);
            List lst = query.list();
            if (lst != null && !lst.isEmpty()) {
                return lst;
            }
        } catch (HibernateException ex) {
            System.out.println(ex);
        }
        return null;
    }

    /**
     * @author duyetdk
     * @since 05/07/2019
     * @des lay thue bao homephone
     * @param cmPosSession
     * @param custId
     * @return
     */
    public List<String> getHomephoneByCustId(Session cmPosSession, Long custId) {
        try {
            if (custId == null || custId == 0L) {
                return null;
            }
            String sql = "SELECT sub.isdn "
                    + "  FROM customer cu, contract co, sub_hp sub "
                    + "  WHERE cu.cust_id = co.cust_id AND co.contract_id = sub.contract_id AND cu.cust_id = ? ";
            Query query = cmPosSession.createSQLQuery(sql);
            query.setParameter(0, custId);
            List lst = query.list();
            if (lst != null && !lst.isEmpty()) {
                return lst;
            }
        } catch (HibernateException ex) {
            System.out.println(ex);
        }
        return null;
    }

    public void buildQueryCust(StringBuilder sql, HashMap params, String name,
            String phoneNumber, String address, Long status, String province, String kindOfCustomer) {
        if (sql == null || params == null) {
            return;
        }
        if (status != null && status > 0L) {
            sql.append(" AND status = :status ");
            params.put("status", status);
        } else {
            sql.append(" AND (status = :statusConnect OR status = :statusNotConnect) ");
            params.put("statusConnect", Constants.CONNECTED);
            params.put("statusNotConnect", Constants.NOT_CONNECT);
        }
        if (name != null && !name.trim().isEmpty()) {
            sql.append(" AND LOWER(name) LIKE :name ");
            params.put("name", "%" + name.trim().toLowerCase() + "%");
        }
        if (phoneNumber != null && !phoneNumber.trim().isEmpty()) {
            sql.append(" AND LOWER(phoneNumber) LIKE :phoneNumber ");
            params.put("phoneNumber", "%" + phoneNumber.trim().toLowerCase() + "%");
        }
        if (address != null && !address.trim().isEmpty()) {
            sql.append(" AND LOWER(address) LIKE :address ");
            params.put("address", "%" + address.trim().toLowerCase() + "%");
        }
        if (province != null && !province.trim().isEmpty()) {
            sql.append(" AND province = :province ");
            params.put("province", province.trim().toUpperCase());
        }
        if (kindOfCustomer != null && !kindOfCustomer.trim().isEmpty()) {
            sql.append(" AND kindOfCustomer = :kindOfCustomer ");
            params.put("kindOfCustomer", kindOfCustomer.trim());
        }

    }

    public Long countPotentialCust(Session cmPosSession, String name, String phoneNumber,
            String address, Long status, String province, String kindOfCustomer) {
        StringBuilder sql = new StringBuilder().append(" SELECT COUNT(*) FROM PotentialCustomer WHERE 1 = 1 ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        buildQueryCust(sql, params, name, phoneNumber, address, status, province, kindOfCustomer);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public List<PotentialCustomer> getListPotentialCust(Session cmPosSession, Integer start, Integer max,
            String name, String phoneNumber, String address, Long status, String province, String kindOfCustomer) {
        StringBuilder sql = new StringBuilder().append(" FROM PotentialCustomer WHERE 1 = 1 ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        buildQueryCust(sql, params, name, phoneNumber, address, status, province, kindOfCustomer);
        sql.append(" ORDER BY updateDate DESC ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        if (start != null) {
            query.setFirstResult(start);
        }
        if (max != null) {
            query.setMaxResults(max);
        }
        List<PotentialCustomer> result = query.list();
        return result;
    }

    public PotentialCustomer findByPotentialCustId(Session cmPosSession, Long custId) {
        StringBuilder sql = new StringBuilder().append(" FROM PotentialCustomer WHERE custId = :custId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("custId", custId);
        sql.append(" AND ( status = :statusConnect OR status = :statusNotConnect) ");
        params.put("statusConnect", Constants.CONNECTED);
        params.put("statusNotConnect", Constants.NOT_CONNECT);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<PotentialCustomer> result = query.list();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    public void addNewPotentialCust(Session cmPosSession, String name, String phoneNumber,
            String address, String province, String email, String otherOperator, Double fee,
            String latitude, String longitude, String userLogin,
            String kindOfCustomer, String expectedService, String expectedScale, String serviceUsed) throws Exception {

        PotentialCustomer cust = new PotentialCustomer();
        Long custId = getSequence(cmPosSession, Constants.POTENTIAL_CUST_SEQ);
        cust.setCustId(custId);
        cust.setName(name);
        cust.setPhoneNumber(phoneNumber);
        cust.setAddress(address);
        cust.setProvince(province);
        cust.setEmail(email);
        cust.setOtherOperator(otherOperator);
        if (fee != null && fee > 0D) {
            cust.setFee(fee);
        }
        cust.setLatitude(latitude);
        cust.setLongitude(longitude);
        cust.setCreateDate(new Date());
        cust.setCreateUser(userLogin);
        cust.setUpdateDate(new Date());
        cust.setUpdateUser(userLogin);
        cust.setStatus(Constants.NOT_CONNECT);
        cust.setKindOfCustomer(kindOfCustomer);
        cust.setExpectedService(expectedService);
        cust.setExpectedScale(expectedScale);
        cust.setServiceUsed(serviceUsed);
        cmPosSession.save(cust);
        cmPosSession.flush();

        //phuonghc 
        Long custHisId = getSequence(cmPosSession, Constants.POTENTIAL_CUS_HIS_SEQ);
        PotentialCustomerHistory custHis = new PotentialCustomerHistory();
        custHis.setHisId(custHisId);
        custHis.setCustId(custId);
        custHis.setOldValue("");
        custHis.setNewValue("Not connect");
        custHis.setUserImpacted(userLogin);
        custHis.setCreateTime(new Date());
        custHis.setTypeImpacted("Create new");
        cmPosSession.save(custHis);

        cmPosSession.flush();
    }

    public void updatePotentialCust(Session cmPosSession, Long custId, String name, String phoneNumber,
            String address, String province, String email, String otherOperator, Double fee,
            String latitude, String longitude, Long status, String userLogin,
            String kindOfCustomer, String expectedService, String expectedScale, String serviceUsed) throws Exception {

        PotentialCustomer cust = findByPotentialCustId(cmPosSession, custId);
        Long currentStatus = cust.getStatus();
        if (cust == null) {
            return;
        }
        cust.setName(name);
        cust.setPhoneNumber(phoneNumber);
        cust.setAddress(address);
        cust.setProvince(province);
        cust.setEmail(email);
        cust.setOtherOperator(otherOperator);
        if (fee != null && fee > 0D) {
            cust.setFee(fee);
        } else {
            cust.setFee(null);
        }
        cust.setLatitude(latitude);
        cust.setLongitude(longitude);
        if (status != null && status > 0L) {
            cust.setStatus(status);
        }
        cust.setUpdateDate(new Date());
        cust.setUpdateUser(userLogin);
        cust.setKindOfCustomer(kindOfCustomer);
        cust.setExpectedService(expectedService);
        cust.setExpectedScale(expectedScale);
        cust.setServiceUsed(serviceUsed);
        cmPosSession.update(cust);
        cmPosSession.flush();

        //phuonghc 
        Long custHisId = getSequence(cmPosSession, Constants.POTENTIAL_CUS_HIS_SEQ);
        PotentialCustomerHistory custHis = new PotentialCustomerHistory();
        custHis.setHisId(custHisId);
        custHis.setCustId(custId);
        if (Constants.CONNECTED.longValue() == currentStatus.longValue()) {
            custHis.setOldValue("Connected");
        } else if (Constants.NOT_CONNECT.longValue() == currentStatus.longValue()) {
            custHis.setOldValue("Not connect");
        } else {
            custHis.setOldValue("");
        }
        if (status != null && status == 2L) {
            custHis.setNewValue("Not connect");
        } else {
            custHis.setNewValue("Connected");
        }


        custHis.setUserImpacted(userLogin);
        custHis.setCreateTime(new Date());
        custHis.setTypeImpacted("Update");
        cmPosSession.save(custHis);
        cmPosSession.flush();
    }

    public void deletePotentialCust(Session cmPosSession, Long custId, String userLogin) throws Exception {

        PotentialCustomer cust = findByPotentialCustId(cmPosSession, custId);
        if (cust == null) {
            return;
        }
        cust.setStatus(Constants.REMOVE);
        cust.setUpdateDate(new Date());
        cust.setUpdateUser(userLogin);
        cmPosSession.update(cust);
        cmPosSession.flush();
    }

    public PotentialCustomer findPotentialCustByPhoneNumber(Session cmPosSession, String phoneNumber) {
        StringBuilder sql = new StringBuilder().append(" FROM PotentialCustomer WHERE phoneNumber = :phoneNumber ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("phoneNumber", phoneNumber);
        sql.append(" AND ( status = :statusConnect OR status = :statusNotConnect) ");
        params.put("statusConnect", Constants.CONNECTED);
        params.put("statusNotConnect", Constants.NOT_CONNECT);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<PotentialCustomer> result = query.list();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    //phuonghc 15062020
    public List<PotentialCustomerHistoryDto> getListPotentialCustHistory(Session session, String custId) {
        String sql = " FROM PotentialCustomerHistory WHERE custId=:custId ORDER BY create_time DESC";
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("custId", Long.valueOf(custId));
        Query query = session.createQuery(sql);
        buildParameter(query, params);
        List<PotentialCustomerHistory> result = query.list();
        if (result == null || result.isEmpty()) {
            return new ArrayList<PotentialCustomerHistoryDto>();
        }
        //convert
        List<PotentialCustomerHistoryDto> potenCushisDto = new ArrayList<PotentialCustomerHistoryDto>();
        for (PotentialCustomerHistory entity : result) {
            PotentialCustomerHistoryDto atom = new PotentialCustomerHistoryDto();
            atom.setHisId(entity.getHisId());
            atom.setCustId(entity.getCustId());
            atom.setColumn(entity.getTypeImpacted());
            atom.setCreateTime(entity.getCreateTimeStr());
            atom.setNewValue(entity.getNewValue());
            atom.setOldValue(entity.getOldValue());
            atom.setUserImpacted(entity.getUserImpacted());
            potenCushisDto.add(atom);
        }
        return potenCushisDto;
    }
}