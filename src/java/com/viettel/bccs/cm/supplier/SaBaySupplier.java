/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.supplier;
import com.viettel.bccs.cm.model.Group_Kit_OTP;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ConvertUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author quangdm
 */
public class SaBaySupplier extends BaseSupplier {
      public SaBaySupplier() {
        logger = Logger.getLogger(SaBaySupplier.class);
    }
    public String insert(Session cmPosSession, String OTP,String msisdn) {
            Long otp_id = getSequence(cmPosSession, "group_kit_OTP_seq");
            Group_Kit_OTP group_Kit_OTP=new Group_Kit_OTP();
            group_Kit_OTP.setOtp_id(otp_id);
            group_Kit_OTP.setMsisdn(msisdn);
            group_Kit_OTP.setOtp(OTP);
            group_Kit_OTP.setCreate_time(new Date());
            cmPosSession.save(group_Kit_OTP);
            return OTP;
    }
    public boolean checkOTP(Session cmPosSession, String OTP,String msisdn) {
        Boolean re=false;
        StringBuilder sql = new StringBuilder().append("select second from ( select  (SYSDATE - create_time) * 60 * 60 * 24 second from group_kit_otp where otp= :otp  and msisdn= :msisdn ) a where a.second <=180 ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("otp", OTP);
        params.put("msisdn", msisdn);
        Query query = cmPosSession.createSQLQuery(sql.toString());
        buildParameter(query, params);
        List result = query.list();
        if(result.size()>0)
            re=true;
        return  re;
    }
}
