/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.supplier;

import com.google.gson.Gson;
import com.viettel.bccs.api.Task.BO.Connector;
import com.viettel.bccs.api.Task.BO.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubDeployment;
import com.viettel.bccs.api.Task.BO.SubStockModelRel;
import com.viettel.bccs.cm.bussiness.StaffBussiness;
import com.viettel.bccs.api.Task.DAO.TaskManagementDAO;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.api.Task.BO.TaskManagement;
import com.viettel.bccs.api.Task.BO.TaskStageItem;
import com.viettel.bccs.api.Task.DAO.ContractDAO;
import com.viettel.bccs.api.Task.DAO.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.dao.SubDeploymentDAO;
import com.viettel.bccs.api.Task.DAO.SubStockModelRelDAO;
import com.viettel.bccs.api.Task.DAO.TaskStageItemDAO;
import com.viettel.bccs.api.Util.Constant;
import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.api.supplier.webservice.NimsAuthenticationInfo;
import com.viettel.bccs.cm.bussiness.SubFbConfigBusiness;
import com.viettel.bccs.cm.dao.NotificationMessageDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.dao.SubBundleTvDAO;
import com.viettel.bccs.cm.dao.TaskShopManagementDAO;
import com.viettel.bccs.cm.dao.TaskStaffManagementDAO;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.SubBundleTv;
import com.viettel.bccs.cm.model.SubBundleTvLog;
import com.viettel.bccs.cm.model.SubFbConfig;
import com.viettel.bccs.cm.model.TaskShopManagement;
import com.viettel.bccs.cm.model.TaskStaffManagement;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.dao.pre.IsdnSendSmsDAO;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.output.Request;
import com.viettel.brcd.ws.model.output.ServiceResponse;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.brcd.ws.supplier.nims.lockinfras.LockAndUnlockInfrasResponse;
import com.viettel.brcd.ws.supplier.nims.lockinfras.NimsWsLockUnlockInfrasBusiness;
import com.viettel.brcd.ws.supplier.nims.sendSubscriptionInfo.NimsWsSendSubscriptionInfoBusiness;
import com.viettel.brcd.ws.supplier.nims.sendSubscriptionInfo.SendSubscriptionInfo;
import com.viettel.brcd.ws.supplier.nims.sendSubscriptionInfo.SendSubscriptionInfoResponse;
import com.viettel.brcd.ws.supplier.nims.sendSubscriptionInfo.SubscriptionInfoForm;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class ServiceSupplier extends BaseSupplier {

    private static final String TASK_NEW_REGISTER = "0";

    public ServiceSupplier() {
        logger = Logger.getLogger(ServiceSupplier.class);
    }

    public WSRespone configConnectImt(Session cmPosSession, Session nimSession, Session cmPre, Long staffId, Long taskManagementId, String locale, String splitter) {

        WSRespone wsRespone = new WSRespone();
        wsRespone.setErrorCode(Constants.ERROR_CODE_1);
        /*Kiem tra nhan vien*/
        Staff staff = new StaffBussiness().findById(cmPosSession, staffId);
        if (staff == null) {
            wsRespone.setErrorDecription(LabelUtil.getKey("staff.does.not.exist", locale));
            return wsRespone;
        }

        TaskManagement taskManage = new TaskManagementDAO().findById(cmPosSession, taskManagementId);
        /*Kiem tra task*/
        if (taskManage == null) {
            wsRespone.setErrorDecription(LabelUtil.getKey("not.found.task", locale));
            return wsRespone;
        }

        List<TaskShopManagement> taskShop = new TaskShopManagementDAO().findByProperty(cmPosSession, "taskMngtId", taskManagementId);

        if (taskShop == null || taskShop.isEmpty()) {
            wsRespone.setErrorDecription(LabelUtil.getKey("not.found.task", locale));
            return wsRespone;
        }

        List<TaskStaffManagement> taskStaff = new TaskStaffManagementDAO().findByProperty(cmPosSession, "taskShopMngtId", taskShop.get(0).getTaskShopMngtId());
        if (taskStaff == null || taskStaff.isEmpty()) {
            wsRespone.setErrorDecription(LabelUtil.getKey("not.found.task", locale));
            return wsRespone;
        }
        SubFbConfig subFbConf = new SubFbConfigBusiness().findByTaskMngtId(cmPosSession, taskManagementId, Constants.TYPE_CONF_IMT);
        SubFbConfig subFbConfTv = new SubFbConfigBusiness().findByTaskMngtId(cmPosSession, taskManagementId, Constants.TYPE_CONF_TV);

        /*Get from index, to index of cable*/
        List<TaskStageItem> lstTaskStageItem = new TaskStageItemDAO().findByTaskStaffMngtId(taskStaff.get(0).getTaskStaffMngtId(), cmPosSession);
        TaskStageItem cable = null;
        String cableType = "";
        /*cho cab Tight buffered cable 1fo*/
        String cable1Fo = ResourceUtils.getResource("CABLE_1_FO");
        String cable2Fo = ResourceUtils.getResource("CABLE_2_FO");
        for (TaskStageItem item : lstTaskStageItem) {
            if (cable1Fo.contains(item.getJsgId().toString()) && item.getFromIndex() != null && item.getToIndex() != null) {
                cable = item;
                cableType = "Cable 1FO";
                break;
            } else if (cable2Fo.contains(item.getJsgId().toString()) && item.getFromIndex() != null && item.getToIndex() != null) {
                cable = item;
                cableType = "Cable 2FO";
                break;
            }
        }

        List<SubStockModelRel> lstSubStockModelRel = new SubStockModelRelDAO().findBySubId(taskManage.getSubId(), cmPosSession);

        if (!Constants.OTHER_PRO_BUNDLE_TV.equals(taskManage.getJobCode())) {
            /*Kiem tra trang thai config*/
            if (subFbConf == null) {
                wsRespone.setErrorDecription("Record request config is missing");
                return wsRespone;
            }
            if (!Constants.APPROVED.equals(subFbConf.getStatus())) {
                wsRespone.setErrorDecription("This request is not approved");
                return wsRespone;
            }
        }

        if (subFbConf != null && !Constants.APPROVED.equals(subFbConf.getStatusConf())) {
            List<SubDeployment> lstSubDep = new SubDeploymentDAO().findBySubId(cmPosSession, subFbConf.getSubId());
            //List<com.viettel.bccs.api.Task.BO.SubDeployment> subDeploy = new com.viettel.bccs.api.Task.DAO.SubDeploymentDAO().findBySubId(cmPosSession, subFbConf.getSubId());
            if (lstSubDep == null || lstSubDep.isEmpty()) {
                wsRespone.setErrorDecription("Have no information infrastrcuture of this task");
                return wsRespone;
            }
            SubAdslLeaseline subAdslLl = new SubAdslLeaselineDAO().findById(taskManage.getSubId(), cmPosSession);
            boolean isGpon = Constants.IN_FRATYPE_GPON.equals(subAdslLl.getInfraType());
            String a = ResourceUtils.getResource(Constants.URL_CONFIG_IMT_GPON);
            String b = ResourceUtils.getResource(Constants.URL_CONFIG_IMT_AON);

            String urlStr = isGpon ? a : b;
            String data = "";
            StringBuilder request = new StringBuilder("{");
            StringBuilder cardOlt = new StringBuilder();
            StringBuilder portOlt = new StringBuilder();
            StringBuilder portIdOlt = new StringBuilder();
            if (isGpon) {
                if (lstSubStockModelRel == null || lstSubStockModelRel.isEmpty()) {
                    wsRespone.setErrorDecription("Find no goods for this task");
                    return wsRespone;
                }
                Common.getCardPortOltFromSplitter(nimSession, lstSubDep.get(0).getSplitterId(), cardOlt, portOlt, portIdOlt, lstSubDep.get(0).getDslamId());
                data = convertValueToFormDataFormat(data, "action", TASK_NEW_REGISTER.equals(taskManage.getReqType()) ? "config" : "change", Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "gpon_name", Common.getInraDeviceCode(nimSession, lstSubDep.get(0).getDslamId(), true), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "acc_name", taskManage.getAccount(), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "serial_gpon", lstSubStockModelRel.get(0).getSerialGpon(), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "dn", Common.getInraDeviceCode(nimSession, lstSubDep.get(0).getBoardId(), true), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "sn", Common.getInraDeviceCode(nimSession, lstSubDep.get(0).getCableBoxId(), true), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "spl", Common.getInraDeviceCode(nimSession, lstSubDep.get(0).getSplitterId(), true), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "port", Common.getPortNoFromDeviceAndPortId(nimSession, lstSubDep.get(0).getSplitterId(), lstSubDep.get(0).getPortNo()), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "card_olt", cardOlt.toString(), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "port_olt", portOlt.toString(), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "splitter", splitter != null && !"?".equals(splitter) && !"".equals(splitter) ? splitter : lstSubDep.get(0).getIsSplitter() != null && lstSubDep.get(0).getIsSplitter().equals("1") ? "yes" : "no", Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "cable_type", cableType, Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "index_sn", cable == null || cable.getFromIndex() == null ? "0" : cable.getFromIndex().toString(), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "index_sub", cable == null || cable.getToIndex() == null ? "0" : cable.getToIndex().toString(), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "lng", subFbConf.getCustLong(), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "lat", subFbConf.getCustLat(), Constants.BOUNDARY_FORM_DATA, request);
            } else {
                Connector connector = Common.getLocationOfDeviceAonFL(nimSession, lstSubDep.get(0).getCableBoxId());
                data = convertValueToFormDataFormat(data, "action", TASK_NEW_REGISTER.equals(taskManage.getReqType()) ? "config" : "change", Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "acc_name", taskManage.getAccount(), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "odf", connector.getOdfCode(), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "odf_port", ((SubDeployment) lstSubDep.get(0)).getCouplerNo().toString(), "mBCCS", request);
                data = convertValueToFormDataFormat(data, "sn", Common.getInraDeviceCode(nimSession, lstSubDep.get(0).getCableBoxId(), true), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "cable_type", cableType, Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "index_sn", cable == null || cable.getFromIndex() == null ? "0" : cable.getFromIndex().toString(), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "index_sub", cable == null || cable.getToIndex() == null ? "0" : cable.getToIndex().toString(), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "lng", subFbConf.getCustLong(), Constants.BOUNDARY_FORM_DATA, request);
                data = convertValueToFormDataFormat(data, "lat", subFbConf.getCustLat(), Constants.BOUNDARY_FORM_DATA, request);
            }
            if (!data.isEmpty()) {
                ServiceResponse serviceResponse;
//                if ("true".equalsIgnoreCase(com.viettel.brcd.common.util.ResourceBundleUtils.getResource("provisioning.allow.send.request", "provisioning"))) {
                String response = sendRequest(Constants.SERVICE_METHOD_POST, urlStr, data + "--" + Constants.BOUNDARY_FORM_DATA + "--", wsRespone, Constants.BOUNDARY_FORM_DATA);
                logger.info(request);
                logger.info(response);
                //String response = "{\"status\":\"201\",\"result_code\":\"0\",\"detail\":\"Power RX = -18.54dbm is good.\",\"ne_name\":\"PNP614_G_C320\",\"card_olt\":\"\",\"port_olt\":\"\",\"port_logic\":34}";
                serviceResponse = new Gson().fromJson(response, ServiceResponse.class);
                insertLogConfigIMT(cmPosSession, subFbConf.getId(), serviceResponse, request.append("}").toString().replaceFirst(",", ""), wsRespone.getErrorDecription(), response, staff.getStaffCode(), "CONFIG_IMT");
                /*unlock ha tang cu*/
                try {
                    LockAndUnlockInfrasResponse infrasResponse = NimsWsLockUnlockInfrasBusiness.
                            lockUnlockInfras(NimsWsLockUnlockInfrasBusiness.INFRAS_ACTION_UNLOCK,
                            subAdslLl.getAccount(),
                            null,
                            lstSubDep.get(0).getCableBoxId() == null ? 0l : lstSubDep.get(0).getCableBoxId(),
                            null, subAdslLl.getServiceType(), null, null, null, null);
                    infrasResponse = NimsWsLockUnlockInfrasBusiness.
                            lockUnlockInfras(NimsWsLockUnlockInfrasBusiness.INFRAS_ACTION_UNLOCK,
                            subAdslLl.getAccount(),
                            null,
                            lstSubDep.get(0).getSplitterId() == null ? 0l : lstSubDep.get(0).getSplitterId(),
                            null, subAdslLl.getServiceType(), null, null, null, null);
                } catch (Exception ex) {
                }

                /*config IMT thanh cong*/
                if (serviceResponse != null && "0".equals(serviceResponse.getResult_code())) {
                    com.viettel.bccs.api.Task.BO.Contract c = new ContractDAO().findById(subAdslLl.getContractId(), cmPosSession);
                    if (isGpon) {
                        Long portLogic = serviceResponse.getPort_logic() != null ? Long.parseLong(serviceResponse.getPort_logic()) : null;
                        if (portLogic == null) {
                            insertLogConfigIMT(cmPosSession, subFbConf.getId(), serviceResponse, data, wsRespone.getErrorDecription(), response, staff.getStaffCode(), "CONFIG_IMT");
                            wsRespone.setErrorDecription("can not find port logic from imt");
                        } else {
                            lstSubDep.get(0).setPortLogic(portLogic);
                            cmPosSession.update(lstSubDep.get(0));
                            /*config GPON success*/
                            subFbConf.setStatusConf(Constants.APPROVED);
                            subFbConf.setLastUpdateDate(new Date());
                            subFbConf.setLastUpdateUser(staff.getStaffCode());
                            subFbConf.setDescription(serviceResponse.getDetail());
                            cmPosSession.update(subFbConf);
                            cmPosSession.beginTransaction().commit();
                            cmPosSession.beginTransaction();

                            StringBuilder requestNims = new StringBuilder();
                            /*cap nhat thong tin sang nims*/
                            SendSubscriptionInfoResponse responseSubInfo = sendNimsGPON(
                                    taskManage.getAccount(),
                                    lstSubStockModelRel.get(0).getSerialGpon(),
                                    lstSubDep.get(0).getPortNo(),
                                    subAdslLl.getTelMobile(),
                                    c.getContractNo(),
                                    lstSubDep.get(0).getStationId(),
                                    subAdslLl.getDeployAddress() == null || subAdslLl.getDeployAddress().isEmpty() ? subAdslLl.getAddress() : subAdslLl.getDeployAddress(),
                                    subAdslLl.getServiceType(),
                                    portIdOlt,
                                    lstSubDep.get(0).getDslamId(),
                                    lstSubDep.get(0).getPortLogic(),
                                    Constant.GPON,
                                    requestNims);

                            String errorCode = "OK";
                            String message = "";

                            try {
                                if ((responseSubInfo == null || (responseSubInfo.getReturn() != null && responseSubInfo.getReturn().getResult().equals(Constant.INFRAS_RESULT_NOK)))) {
                                    subFbConf.setDescription("send nims info faild");
                                    errorCode = "NOK";
                                    message = responseSubInfo != null ? responseSubInfo.getReturn().getMessage() : "";
                                }
                                insertLogNIMS(cmPosSession, subFbConf.getId(), requestNims.toString(), errorCode, responseSubInfo == null ? "" : new Gson().toJson(responseSubInfo), staff.getStaffCode(), "SEND_NIMS_INFO");
                                //cmPosSession.save(log1);
                            } catch (Exception ex) {
                                logger.info("configConnectImt: " + ex.getMessage());
                            }

                            wsRespone.setErrorCode("0");
                        }
                    } else {
                        Long portLogic = serviceResponse.getPort_logic() != null ? Long.parseLong(serviceResponse.getPort_logic()) : null;
                        lstSubDep.get(0).setPortLogic(portLogic);
                        cmPosSession.update(lstSubDep.get(0));
                        StringBuilder requestNims = new StringBuilder();
                        SendSubscriptionInfoResponse responseSubInfo = sendNimsAON(cmPosSession, subAdslLl, lstSubDep.get(0), requestNims);
                        subFbConf.setStatusConf(Constants.APPROVED);
                        subFbConf.setLastUpdateDate(new Date());
                        subFbConf.setLastUpdateUser(staff.getStaffCode());
                        subFbConf.setDescription(serviceResponse.getDetail());
                        cmPosSession.update(subFbConf);
                        cmPosSession.beginTransaction().commit();
                        cmPosSession.beginTransaction();
                        String errorCode = "OK";
                        if ((responseSubInfo == null || (responseSubInfo.getReturn() != null && responseSubInfo.getReturn().getResult().equals(Constant.INFRAS_RESULT_NOK)))) {
                            subFbConf.setDescription("send nims info faild");
                            errorCode = "NOK";

                        }
                        insertLogNIMS(cmPosSession, subFbConf.getId(), requestNims.toString(), errorCode, responseSubInfo == null ? "" : new Gson().toJson(responseSubInfo), staff.getStaffCode(), "SEND_NIMS_INFO");
                        //cmPosSession.save(log1);
                        wsRespone.setErrorCode("0");

                    }
                    alertStaffToFinishTask(cmPosSession, cmPre, taskManage, taskStaff, staff, subAdslLl, locale);
                } else {
                    /*config fail*/
                    subFbConf.setStatusConf(Constants.REJECT);
                    subFbConf.setLastUpdateDate(new Date());
                    subFbConf.setLastUpdateUser(staff.getStaffCode());
                    subFbConf.setDescription(serviceResponse == null ? "See log for more detail" : serviceResponse.getDetail());
                    cmPosSession.update(subFbConf);
                    wsRespone.setErrorCode("1");
                }
//                } else {
//                    insertLogConfigIMT(cmPosSession, subFbConf.getId(), new Gson().fromJson("{\"errorCode\":\"0\",\"errorDecription\":\"Success\"}", ServiceResponse.class), request.append("}").toString().replaceFirst(",", ""), wsRespone.getErrorDecription(), "{\"errorCode\":\"0\",\"errorDecription\":\"Success\"}", staff.getStaffCode(), "CONFIG_IMT");
//                    serviceResponse = new ServiceResponse();
//                    serviceResponse.setDetail("SUCCESS");
//                    subFbConf.setStatusConf(Constants.APPROVED);
//                    subFbConf.setLastUpdateDate(new Date());
//                    subFbConf.setLastUpdateUser(staff.getStaffCode());
//                    subFbConf.setDescription(serviceResponse.getDetail());
//                    alertStaffToFinishTask(cmPosSession, cmPre, taskManage, taskStaff, staff, subAdslLl, locale);
//                }

                cmPosSession.beginTransaction().commit();
                cmPosSession.beginTransaction();
                wsRespone.setErrorDecription(serviceResponse != null ? serviceResponse.getDetail() : wsRespone.getErrorDecription());
            } else {
                return wsRespone;
            }
        }
        if (subFbConfTv != null && !Constants.APPROVED.equals(subFbConfTv.getStatusConf()) && Constants.APPROVED.equals(subFbConfTv.getStatus())) {
            SubBundleTv subTv = new SubBundleTvDAO().findBundleTvByAccount(cmPosSession, taskManage.getPathFileAttach());
            List<ApParam> param = new ApParamSupplier().findByTypeCode(cmPosSession, Constants.MAPPING_PACKAGE_TV, subTv.getRegReasonCode(), 1l);
            if (param == null || param.isEmpty()) {
                wsRespone.setErrorCode(Constants.RESPONSE_FAIL);
                wsRespone.setErrorDecription("Can not find package tv for reason " + subTv.getRegReasonCode());
                return wsRespone;
            }
            /*upgrade bandwith*/
            List<SubBundleTv> listTv = new SubBundleTvDAO().findBundleTvBySubId(cmPosSession, taskManage.getSubId());
            ServiceResponse serviceResponse = increaseBandWidth(cmPosSession, subFbConfTv.getSubId(), subFbConfTv.getId(), wsRespone, staff.getStaffCode(), String.valueOf(listTv.size()));
            if (serviceResponse == null || !"200".equals(serviceResponse.getResult_code())) {
                wsRespone.setErrorCode(Constants.RESPONSE_FAIL);
                wsRespone.setErrorDecription("Increase bandwith fail");
            } else {
                Request requestBundle = new Request();
                requestBundle.addUserDevicePackageList(subTv.getUserUniqueId(), subTv.getBoxSerial(), param.get(0).getParamValue());
                String data = new Gson().toJson(requestBundle);
                if ("true".equalsIgnoreCase(com.viettel.brcd.common.util.ResourceBundleUtils.getResource("provisioning.allow.send.request", "provisioning"))) {
                    ServiceSupplier serviceSup = new ServiceSupplier();
                    String response = serviceSup.sendRequest(Constants.SERVICE_METHOD_POST, ResourceUtils.getResource("BUNDLE_TV_SERVICE"), data, null, null);
                    if (response == null || response.isEmpty()) {
                        wsRespone.setErrorCode(Constants.RESPONSE_FAIL);
                        wsRespone.setErrorDecription("Error from call service addUserDevicePackageList bunlde tv");
                        subFbConfTv.setStatusConf(Constants.REJECT);
                        subFbConfTv.setDescription(wsRespone.getErrorDecription());

                        increaseBandWidth(cmPosSession, subFbConfTv.getSubId(), subFbConfTv.getId(), wsRespone, staff.getStaffCode(), String.valueOf(listTv.size() - 1));
                    } else {
                        WSRespone ws = new Gson().fromJson(response, WSRespone.class);
                        serviceResponse = ws == null || ws.getErrorDecription() == null ? null : new Gson().fromJson(ws.getErrorDecription(), ServiceResponse.class);
                        subFbConfTv.setStatus(Constants.APPROVED);
                        if (serviceResponse == null || !Constants.RESPONSE_SUCCESS.equals(serviceResponse.getResult())) {
                            wsRespone.setErrorCode(Constants.RESPONSE_FAIL);
                            wsRespone.setErrorDecription("Error from call service addUserDevicePackageList bunlde tv " + (serviceResponse != null ? serviceResponse.getMessage() : ""));
                            subFbConfTv.setStatusConf(Constants.REJECT);
                            subFbConfTv.setDescription(wsRespone.getErrorDecription());

                            increaseBandWidth(cmPosSession, subFbConfTv.getSubId(), subFbConfTv.getId(), wsRespone, staff.getStaffCode(), String.valueOf(listTv.size() - 1));
                        } else {
                            subFbConfTv.setStatusConf(Constants.APPROVED);
                            subFbConfTv.setStatus(Constants.APPROVED);
                            subFbConfTv.setDescription(null);
                            subTv.setStatus(1l);
                            subTv.setActStatus("000");
                            subTv.setFirstConnect(new Date());
                            wsRespone.setErrorCode("0");
                        }
                    }
                    /*Luu log request bundle tv*/
                    SubBundleTvLog log = new SubBundleTvLog();
                    log.setSubId(subFbConfTv.getSubId());
                    log.setAction(Request.ACTION_ADD_USER_DEVICE_PACKAGE_LIST);
                    log.setAccount(subFbConfTv.getAccount());
                    log.setRequest(data);
                    log.setResponse(response);
                    log.setCreateUser(staff.getStaffCode());
                    log.setCreateDate(new Date());

                    cmPosSession.save(log);
                    cmPosSession.flush();
                }
            }
            subFbConfTv.setLastUpdateDate(new Date());
            subFbConfTv.setLastUpdateUser(staff.getStaffCode());
            cmPosSession.update(subFbConfTv);
            cmPosSession.beginTransaction().commit();
            cmPosSession.beginTransaction();
        }
        return wsRespone;
    }

    /*
     * @des: call to rest service
     */
    public String sendRequest(String typeMethod, String urlStr, String data, WSRespone wsRespone, String boundary) {
        /*
         * data: is json string
         * example: "{\"name\":\"mkyong\", \"age\":33}";
         * 
         * typeMethod: POST/GET/...
         */
        StringBuilder response = new StringBuilder();
        if (wsRespone == null) {
            wsRespone = new WSRespone();
        }
        HttpURLConnection conn = null;
        try {
            Gson gson = new Gson();
            URL url = new URL(urlStr);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod(typeMethod);

            if (boundary != null) {
                conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            } else {
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
            }
            if (data != null) {
                OutputStream os = conn.getOutputStream();
                os.write(data == null || data.isEmpty() ? "{}".getBytes() : data.getBytes());
                os.flush();
            }
            if (conn.getResponseCode() != 200) {
                wsRespone.setErrorCode(String.valueOf(conn.getResponseCode()));
                wsRespone.setErrorDecription("Failed : HTTP error code : " + conn.getResponseCode());
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                String output;

                while ((output = br.readLine()) != null) {
                    response.append(output);
                }
                conn.disconnect();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            wsRespone.setErrorDecription("Exception: " + ex.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception ex1) {
                }
            }
        }
        return response.toString();
    }

    private String convertValueToFormDataFormat(String input, String field, String value, String boundary, StringBuilder strRequest) {
        String temp = input + "--" + boundary + "\r\nContent-Disposition: form-data; name=\"" + field + "\"\r\n\r\n" + (value == null ? "" : value) + "\r\n";
        strRequest.append(", ").append(field).append(": ").append(value);
        return temp;
    }

    private void insertLogConfigIMT(Session cmPosSession, Long subFbConfId, ServiceResponse serviceResponse, String request, String errorDescription, String response, String staffCode, String action) {
        /*insert log*/
        try {
            new SubFbConfigSupplier().insertLogImt(
                    cmPosSession,
                    subFbConfId,
                    serviceResponse != null && "0".equals(serviceResponse.getResult_code()) ? 1l : 2l,
                    request.replaceAll("\r\n", " ").replaceAll("Content-Disposition: form-data;", " ").replaceAll(Constants.BOUNDARY_FORM_DATA, ""),
                    response,
                    action,
                    serviceResponse != null && serviceResponse.getDetail() != null ? serviceResponse.getDetail().replaceAll("\r\n", " ") : errorDescription,
                    new Date(),
                    staffCode,
                    serviceResponse != null ? serviceResponse.getResult_code() : "01");
            cmPosSession.beginTransaction().commit();
            cmPosSession.beginTransaction();
        } catch (Exception ex) {
            cmPosSession.beginTransaction().rollback();
            cmPosSession.beginTransaction();
            logger.info("insertLogConfigIMT: " + ex.getMessage());
        }
        //cmPosSession.beginTransaction().commit();
    }

    private void insertLogNIMS(Session cmPosSession, Long subFbConfId, String request, String errorCode, String response, String staffCode, String action) {
        /*insert log*/
        try {
            new SubFbConfigSupplier().insertLogImt(
                    cmPosSession,
                    subFbConfId,
                    2l,
                    request,
                    response,
                    action,
                    "",
                    new Date(),
                    staffCode,
                    errorCode);
        } catch (Exception ex) {
            logger.error("insertLogNIMS: " + ex.getMessage());
        }
        //cmPosSession.beginTransaction().commit();
    }

    /*
     * GPON - connectorId is splitter_id
     * AON - connectorId is cable_box_id
     */
    public String getConnectorName(Session nims, Long connectorId, String infraType) {
        if (Constants.IN_FRATYPE_GPON.equals(infraType)) {
            return Common.getInraDeviceCode(nims, connectorId, true);
        } else {
            Connector connector = Common.getLocationOfDeviceAonFL(nims, connectorId);
            if (connector != null) {
                return connector.getOdfCode();
            }
        }
        return null;
    }

    public WSRespone sendNimsSubActiveInfo(Session cmPosSession, Session nimSession, Long staffId, Long taskManagementId) {
        WSRespone wsRespone = new WSRespone();
        wsRespone.setErrorCode(Constants.ERROR_CODE_1);
        TaskManagement taskManage = new TaskManagementDAO().findById(cmPosSession, taskManagementId);
        if (taskManage != null) {
            SubFbConfig subFbConf = new SubFbConfigBusiness().findByTaskMngtId(cmPosSession, taskManagementId, Constants.TYPE_CONF_IMT);
            if (subFbConf != null) {
                SubAdslLeaseline subAdslLl = new SubAdslLeaselineDAO().findById(taskManage.getSubId(), cmPosSession);
                SendSubscriptionInfoResponse responseSubInfo = null;
                List<SubDeployment> lstSubDep = new SubDeploymentDAO().findBySubId(cmPosSession, subFbConf.getSubId());
                if (Constants.INFRA_GPON.equals(subAdslLl.getInfraType())) {
                    List<SubStockModelRel> lstSubStockModelRel = new SubStockModelRelDAO().findBySubId(taskManage.getSubId(), cmPosSession);
                    StringBuilder portOlt = new StringBuilder();
                    StringBuilder cardOlt = new StringBuilder();
                    StringBuilder portIdOlt = new StringBuilder();
                    Common.getCardPortOltFromSplitter(nimSession, lstSubDep.get(0).getSplitterId(), cardOlt, portOlt, portIdOlt, lstSubDep.get(0).getDslamId());

                    com.viettel.bccs.api.Task.BO.Contract c = new ContractDAO().findById(subAdslLl.getContractId(), cmPosSession);
                    StringBuilder request = new StringBuilder();
                    responseSubInfo = sendNimsGPON(
                            taskManage.getAccount(),
                            lstSubStockModelRel.get(0).getSerialGpon(),
                            lstSubDep.get(0).getPortNo(),
                            subAdslLl.getTelMobile(),
                            c.getContractNo(),
                            lstSubDep.get(0).getStationId(),
                            subAdslLl.getDeployAddress(),
                            subAdslLl.getServiceType(),
                            portIdOlt,
                            lstSubDep.get(0).getDslamId(),
                            lstSubDep.get(0).getPortLogic(),
                            subAdslLl.getInfraType(),
                            request);
                } else {
                    responseSubInfo = sendNimsAON(cmPosSession, subAdslLl, lstSubDep.get(0), new StringBuilder());
                }
                if ((responseSubInfo == null || (responseSubInfo.getReturn() != null && responseSubInfo.getReturn().getResult().equals(Constant.INFRAS_RESULT_NOK)))) {
                    wsRespone.setErrorDecription("send nims info faild");
                } else {
                    wsRespone.setErrorCode(Constants.ERROR_CODE_0);
                    wsRespone.setErrorDecription("send nims info success");
                }
            } else {
                wsRespone.setErrorDecription("sub config not exists");
            }
        } else {
            wsRespone.setErrorDecription("task is not exists");
        }
        return wsRespone;
    }

    SendSubscriptionInfoResponse sendNimsGPON(
            String account, String serialGpon, Long portNo, String telMobile, String contractNo, Long stationId, String deployAdress,
            String serviceType, StringBuilder portIdOlt, Long dslamId, Long portLogic, String infraType, StringBuilder request) {

        SendSubscriptionInfo subInfo = new SendSubscriptionInfo();
        SubscriptionInfoForm subInfoForm = new SubscriptionInfoForm();
        subInfoForm.setAcceptanceDate(new Date().toString());
        subInfoForm.setAccountGline(account + "_gline");
        subInfoForm.setAccount(account);
        subInfoForm.setGponSerial(serialGpon);
        subInfoForm.setPortSpliterId(portNo);
        //subInfoForm.setStbSerial(stbSerial);
        subInfoForm.setContactMobile(telMobile);
        subInfoForm.setContractNo(contractNo);
        subInfoForm.setFax(telMobile);
        subInfoForm.setStationId(stationId);
        subInfoForm.setSubAddress(deployAdress);
        subInfoForm.setSetupAddress(deployAdress);
        subInfoForm.setSubType(serviceType);
        subInfoForm.setInfraType(infraType);
        subInfoForm.setPortId(!portIdOlt.toString().equals("") ? Long.parseLong(portIdOlt.toString()) : 0);
        subInfoForm.setDeviceId(dslamId);
        subInfoForm.setPortLogic(portLogic);
        //subInfoForm.setIsChangeAddGline(1l);
        subInfo.setForm(subInfoForm);
        //Set cac gia tri vao ActiveSubscriber
        subInfo.setUser(NimsAuthenticationInfo.getUserName());
        subInfo.setPass(NimsAuthenticationInfo.getPassword());
        subInfo.setLocal("en_US");
        SendSubscriptionInfoResponse responseSubInfo = null;
        try {
            request.append(new Gson().toJson(subInfo));
            responseSubInfo = new NimsWsSendSubscriptionInfoBusiness().sendSubscriptionInfoResponse(subInfo);
        } catch (Exception ex) {
        }
        return responseSubInfo;

    }

    SendSubscriptionInfoResponse sendNimsAON(Session cmSession, SubAdslLeaseline sub, SubDeployment subD, StringBuilder requestNims) {
        SubscriptionInfoForm subInfoForm = new SubscriptionInfoForm();
        subInfoForm.setInfraType("FCN");
        subInfoForm.setAccount(sub.getAccount());
        subInfoForm.setSetupAddress(sub.getDeployAddress() == null || sub.getDeployAddress().isEmpty() ? sub.getAddress() : sub.getDeployAddress());
        subInfoForm.setSubAddress(sub.getDeployAddress() == null || sub.getDeployAddress().isEmpty() ? sub.getAddress() : sub.getDeployAddress());
        subInfoForm.setDeviceId(subD.getDslamId());
        subInfoForm.setPortId(subD.getPortNo());
        subInfoForm.setSubType(sub.getServiceType());
        if (subD.getIsSplitter() == null || "0".equals(subD.getIsSplitter())) {
            subInfoForm.setOdfId(subD.getBoardId());
        } else {
            subInfoForm.setOdfId(subD.getCableBoxId());
        }
        subInfoForm.setCouplerNo(subD.getCouplerNo() != null ? subD.getCouplerNo() : null);
        subInfoForm.setSecondCouplerNo(subD.getCouplerNo() != null ? subD.getCouplerNo() : null);
        subInfoForm.setStationId(subD.getStationId());
        SendSubscriptionInfo subInfo = new SendSubscriptionInfo();
        subInfo.setForm(subInfoForm);
        //Set cac gia tri vao ActiveSubscriber
        subInfo.setUser(NimsAuthenticationInfo.getUserName());
        subInfo.setPass(NimsAuthenticationInfo.getPassword());
        subInfo.setLocal("en_US");
        requestNims.append(new Gson().toJson(subInfo));
        try {
            return new NimsWsSendSubscriptionInfoBusiness().sendSubscriptionInfoResponse(subInfo);
        } catch (Exception ex) {
        }
        return null;

    }

    public WSRespone changeStationInfraStructure(Session cmPosSession, Session nimSession, Long staffId, String newStation, String connector, String portNo, String account) throws Exception {
        WSRespone wsRespone = new WSRespone();
        wsRespone.setErrorCode(Constants.ERROR_CODE_1);
        try {
            if ((newStation == null || "".equals(newStation) || "?".equals(newStation))
                    && (connector == null || "".equals(connector) || "?".equals(connector))) {
                wsRespone.setErrorDecription("station and splitter is not null");
                return wsRespone;
            }
            com.viettel.bccs.cm.model.SubAdslLeaseline sub = new com.viettel.bccs.cm.dao.SubAdslLeaselineDAO().findByAccount(cmPosSession, account);
            if (sub == null) {
                wsRespone.setErrorDecription("account is not exists");
                return wsRespone;
            }

            Staff staff = new StaffBussiness().findById(cmPosSession, staffId);
            if (sub == null) {
                wsRespone.setErrorDecription("staff is not exists");
                return wsRespone;
            }

            List<SubDeployment> lstSubDep = new SubDeploymentDAO().findBySubId(cmPosSession, sub.getSubId());
            if (lstSubDep == null || lstSubDep.isEmpty()) {
                wsRespone.setErrorDecription("This account have no infrastructure");
                return wsRespone;
            }

            Long stationId = null;
            Long connectorId = null;
            Long boardId = null;
            Long cableBoxId = null;
            Long portId = null;
            Long dsLamId = null;
            if (newStation != null && !"".equals(newStation) && !"?".equals(newStation)) {
                stationId = Common.getStationId(nimSession, newStation);
            }

            if (Constant.GPON.equals(sub.getInfraType())) {
                /*unlock port nims*/
                LockAndUnlockInfrasResponse infrasResponse = NimsWsLockUnlockInfrasBusiness.
                        lockUnlockInfras(NimsWsLockUnlockInfrasBusiness.INFRAS_ACTION_UNLOCK,
                        sub.getAccount(),
                        null,
                        lstSubDep.get(0).getSplitterId(),
                        null, sub.getServiceType(), null, null, null, null);

                if (stationId != null) {
                    dsLamId = Common.getGponDeviceId(nimSession, stationId.toString());
                }

                if (connector != null && !"".equals(connector) && !"?".equals(connector)) {
                    connectorId = Common.getInraDeviceIdFromCode(nimSession, connector);
                    if (connectorId != null) {
                        String[] spliText = connector.split("-");
                        if (spliText.length == 4) {
                            boardId = Common.getInraDeviceIdFromCode(nimSession, spliText[0] + "-" + spliText[1]);
                            cableBoxId = Common.getInraDeviceIdFromCode(nimSession, spliText[0] + "-" + spliText[1] + "-" + spliText[2]);
                            stationId = Common.getStationIdOfDevice(nimSession, spliText[0] + "-" + spliText[1]);
                            dsLamId = Common.getGponDeviceId(nimSession, stationId == null ? "0" : stationId.toString());
                            String port = Common.getPortIdFromDeviceAndPort(nimSession, connectorId, portNo);
                            if (port != null) {
                                portId = Long.parseLong(port);
                            }
                        }
                    }
                }

            }
            if (stationId == null) {
                wsRespone.setErrorDecription("Can not find station " + newStation);
                return wsRespone;
            }
            SubDeployment newSub = new SubDeployment();
            lstSubDep.get(0).copySubDeployment(newSub);
            lstSubDep.get(0).setStatus(Constant.STATUS_NOT_USE);
            newSub.setSubdeployId(getSequence(cmPosSession, Constants.SUB_DEPLOYMENT_SEQ));

            newSub.setCreateDate(new Date());
            newSub.setBoardId(boardId);
            newSub.setCableBoxId(cableBoxId);
            newSub.setDslamId(dsLamId);
            newSub.setSplitterId(connectorId);
            newSub.setStationId(stationId);
            newSub.setPortNo(portId);

            sub.setBoardId(boardId);
            sub.setCableBoxId(cableBoxId);
            sub.setDslamId(dsLamId);
            sub.setStationId(stationId);
            sub.setPortNo(portId == null ? null : portId.toString());

            cmPosSession.save(newSub);
            cmPosSession.save(lstSubDep.get(0));
            cmPosSession.save(sub);

            wsRespone.setErrorCode(Constants.ERROR_CODE_0);
            wsRespone.setErrorDecription("Change success");

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info(ex.getMessage());
            throw ex;
        }
        return wsRespone;
    }

    public SendSubscriptionInfoResponse sendNimsUpdateInfrasByStaff(
            String account, Long odfIndoorCode, Long odfIndoorPort, Long odfIndoorPort2,
            Long odfOutdoorCode,
            Long odfOutdoorPort, Long odfOutdoorPort2,
            Long lengthCable, Long portLogic, String snCode,
            String splitterCode, Long portSplitter,
            String boxCableCode, String boxCablePort,
            String infraType, String subType, String setupAddress, String subAddress,
            String portId, Long deviceId, Long stationId, Long portNoMdfA, Long portNoMdfB,
            StringBuilder request) {

        SendSubscriptionInfo subInfo = new SendSubscriptionInfo();
        SubscriptionInfoForm subInfoForm = new SubscriptionInfoForm();
        subInfoForm.setAcceptanceDate(new Date().toString());
        subInfoForm.setAccountGline(account + "_gline");
        subInfoForm.setAccount(account);
        if (odfIndoorCode != null) {
            subInfoForm.setOdfId(odfIndoorCode);
        }
        if (odfOutdoorPort != null) {
            subInfoForm.setCouplerNo(odfOutdoorPort);
            subInfoForm.setSecondCouplerNo(odfOutdoorPort2);
        } else if (odfIndoorPort != null) {
            subInfoForm.setCouplerNo(odfIndoorPort);
            subInfoForm.setSecondCouplerNo(odfIndoorPort2);
        }
        if (lengthCable != null) {
            subInfoForm.setLengthCableSub(lengthCable);
        }
        logger.error("portSplitter:result=OK" + portSplitter);
        if (portSplitter != null) {
            subInfoForm.setPortSpliterId(portSplitter);
        }
        if (snCode != null && !snCode.isEmpty() && !snCode.equals("?")) {
            subInfoForm.setNodeTbCode(snCode);

        }
        if (splitterCode != null && !splitterCode.isEmpty() && !splitterCode.equals("?")) {
            subInfoForm.setSpliterCode(splitterCode);
        }
        if (boxCableCode != null && !boxCableCode.isEmpty() && !boxCableCode.equals("?")) {
            subInfoForm.setBoxCode(boxCableCode);
        }
        if (portLogic != null) {
            subInfoForm.setPortLogic(portLogic);
        }
        if (boxCablePort != null && !boxCablePort.isEmpty() && !boxCablePort.equals("?")) {
            subInfoForm.setPortCode(boxCablePort);
        }
        if (portNoMdfA != null) {
            subInfoForm.setPortNoMdfA(portNoMdfA);
        }
        if (portNoMdfB != null) {
            subInfoForm.setPortNoMdfB(portNoMdfB);
        }
        subInfoForm.setSubType(subType);
        subInfoForm.setInfraType(infraType);
        subInfoForm.setSetupAddress(setupAddress == null || setupAddress.isEmpty() ? subAddress : setupAddress);
        subInfoForm.setSubAddress(subAddress);
        subInfoForm.setDeviceId(deviceId);
        subInfoForm.setStationId(stationId);
        subInfoForm.setPortId(Long.valueOf(portId));
        subInfoForm.setIsChangePortLogic(1);
        subInfo.setForm(subInfoForm);

        //Set cac gia tri vao ActiveSubscriber
        subInfo.setUser(NimsAuthenticationInfo.getUserName());
        subInfo.setPass(NimsAuthenticationInfo.getPassword());
        subInfo.setLocal("en_US");
        SendSubscriptionInfoResponse responseSubInfo = null;
        try {
            request.append(new Gson().toJson(subInfo));
            responseSubInfo = new NimsWsSendSubscriptionInfoBusiness().sendSubscriptionInfoResponse(subInfo);
        } catch (Exception ex) {
        }
        return responseSubInfo;

    }

    public SendSubscriptionInfoResponse sendNimsUpdateInfrasByStaffGPON(
            String account,
            Long lengthCable, Long portLogic, String snCode,
            String splitterCode, Long portSplitter,
            String infraType, String subType, String setupAddress, String subAddress,
            String portId, Long deviceId, Long stationId,
            StringBuilder request, String serialGpon) {

        SendSubscriptionInfo subInfo = new SendSubscriptionInfo();
        SubscriptionInfoForm subInfoForm = new SubscriptionInfoForm();
        //@duyetdk: start
        subInfoForm.setFullName(account);
        subInfoForm.setCardIdentity(account);
        //@duyetdk: end
        subInfoForm.setAcceptanceDate(new Date().toString());
        subInfoForm.setAccountGline(account + "_gline");
        subInfoForm.setAccount(account);
        if (lengthCable != null) {
            subInfoForm.setLengthCableSub(lengthCable);
        }
        logger.error("portSplitter:result=OK" + portId);
        if (portSplitter != null) {
            subInfoForm.setPortSpliterId(portSplitter);
        }
        if (snCode != null && !snCode.isEmpty() && !snCode.equals("?")) {
            subInfoForm.setNodeTbCode(snCode);
        }
        if (splitterCode != null && !splitterCode.isEmpty() && !splitterCode.equals("?")) {
            subInfoForm.setSpliterCode(splitterCode);
        }
        subInfoForm.setIsChangePortLogic(1);
        subInfoForm.setSubType(subType);
        subInfoForm.setInfraType(infraType);
        subInfoForm.setGponSerial(serialGpon);
        subInfoForm.setSetupAddress(setupAddress == null || setupAddress.isEmpty() ? subAddress : setupAddress);
        subInfoForm.setSubAddress(subAddress);
        subInfoForm.setDeviceId(deviceId);
        subInfoForm.setStationId(stationId);
        subInfoForm.setPortId(!portId.toString().equals("") ? Long.parseLong(portId.toString()) : 0);
        subInfoForm.setPortLogic(portLogic);
        subInfo.setForm(subInfoForm);
        //Set cac gia tri vao ActiveSubscriber
        subInfo.setUser(NimsAuthenticationInfo.getUserName());
        subInfo.setPass(NimsAuthenticationInfo.getPassword());
        subInfo.setLocal("en_US");
        SendSubscriptionInfoResponse responseSubInfo = null;
        try {
            request.append(new Gson().toJson(subInfo));
            responseSubInfo = new NimsWsSendSubscriptionInfoBusiness().sendSubscriptionInfoResponse(subInfo);
        } catch (Exception ex) {
        }
        return responseSubInfo;

    }

    private ServiceResponse increaseBandWidth(Session cmPosSession, Long subId, Long subConfId, WSRespone wsRespone, String staffCode, String numAcc) {
        String data1 = "";
        SubAdslLeaseline sub = new SubAdslLeaselineDAO().findById(subId, cmPosSession);
        StringBuilder request = new StringBuilder("{");
        data1 = convertValueToFormDataFormat(data1, "account", sub.getAccount(), Constants.BOUNDARY_FORM_DATA, request);
        data1 = convertValueToFormDataFormat(data1, "qty_of_tv", numAcc, Constants.BOUNDARY_FORM_DATA, request);
        request.append("}");
        String urlStr = ResourceUtils.getResource(Constants.URL_IMT_BANDWITH);
        String response = sendRequest(Constants.SERVICE_METHOD_POST, urlStr, data1 + "--" + Constants.BOUNDARY_FORM_DATA + "--", wsRespone, Constants.BOUNDARY_FORM_DATA);
        ServiceResponse serviceResponse = new Gson().fromJson(response, ServiceResponse.class);
        insertLogConfigIMT(cmPosSession, subConfId, serviceResponse, request.append("}").toString().replaceFirst(",", ""), wsRespone.getErrorDecription(), response, staffCode, "CONFIG_BANDWITH");
        return serviceResponse;
    }

    private void alertStaffToFinishTask(Session cmPosSession,
            Session cmPre,
            TaskManagement taskManage,
            List<TaskStaffManagement> taskStaff,
            Staff staff,
            SubAdslLeaseline subAdslLl,
            String locale) {
        /*push notification*/
        Staff staffIsdn = new StaffDAO().findById(cmPosSession, taskStaff.get(0).getStaffId());
        List<String> listStr = Arrays.asList(subAdslLl.getAccount());
        List<String> listContentNotification = new IsdnSendSmsDAO().genarateSMSContent(cmPre, null, Constants.AP_PARAM_PARAM_TYPE_NOTIFICATION, "FINISH_TASK", listStr);

        try {
            if (listContentNotification != null && !listContentNotification.isEmpty()) {
                String tempNotification = "";
                for (String temp : listContentNotification) {
                    tempNotification += temp + Constants.NEW_LINE;
                }
                if (!tempNotification.isEmpty()) {
                    StringBuilder stringBui = new StringBuilder(tempNotification);
                    stringBui.replace(tempNotification.lastIndexOf(Constants.NEW_LINE), tempNotification.lastIndexOf(Constants.NEW_LINE) + Constants.NEW_LINE.length(), "");
                    tempNotification = stringBui.toString();
                    new NotificationMessageDAO().generateNotificationData(
                            cmPosSession,
                            staffIsdn.getStaffId(),
                            staffIsdn.getStaffCode(),
                            staff.getStaffCode(),
                            LabelUtil.getKey("Notification.FinishTask.title", locale),
                            tempNotification,/*message*/
                            subAdslLl.getAccount(),/*account*/
                            taskManage.getTaskMngtId(),
                            taskStaff.get(0).getTaskStaffMngtId(),/*taskStaffMngtId*/
                            Constants.NOTIFICATION_ACTION_TYPE_UPDATE_TASK/*actionType*/);
                }
            }
        } catch (Exception ex) {
            logger.info("notification: " + ex.getMessage());
        }
        List<String> lis1t = new ArrayList<String>();
        lis1t.add(taskManage.getAccount());
        if (staffIsdn.getTel() != null && staffIsdn.getIsdn() != null) {
            com.viettel.bccs.cm.bussiness.SendMessge.send(cmPre, staffIsdn.getTel() == null ? staffIsdn.getTel() : staffIsdn.getIsdn()/*isdn*/, lis1t, new Date(), staff.getStaffCode(), null/*shop_code*/, Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.CONFIG_IMT_SUCCESS);
        }
    }
}
