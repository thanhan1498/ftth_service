package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.Shop;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ShopSupplier extends BaseSupplier {

    public ShopSupplier() {
        logger = Logger.getLogger(ShopSupplier.class);
    }

    public List<Shop> findById(Session cmPosSession, Long shopId, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Shop where shopId = :shopId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("shopId", shopId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Shop> result = query.list();
        return result;
    }

    public List<Shop> findByShopCode(Session cmPosSession, String shopCode, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Shop where shopCode = :shopCode ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("shopCode", shopCode);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Shop> result = query.list();
        return result;
    }
}
