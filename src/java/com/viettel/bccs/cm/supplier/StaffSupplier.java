package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.Staff;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class StaffSupplier extends BaseSupplier {

    public StaffSupplier() {
        logger = Logger.getLogger(StaffSupplier.class);
    }

    public List<Staff> findById(Session cmPosSession, Long staffId, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Staff where staffId = :staffId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("staffId", staffId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Staff> result = query.list();
        return result;
    }

    public List<Staff> findByCode(Session cmPosSession, String staffCode, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Staff where upper(staffCode) = upper( :staffCode ) ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("staffCode", staffCode);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Staff> result = query.list();
        return result;
    }

    public List<Staff> findByShop(Session cmPosSession, Long shopId, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Staff where shopId = :shopId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("shopId", shopId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Staff> result = query.list();
        return result;
    }

    public List<Staff> findByShopAndChannel(Session cmPosSession, Long shopId, Long channelTypeId, Long status) {
        StringBuilder sql = new StringBuilder()
                .append(" from Staff where shopId = :shopId and channelTypeId = :channelTypeId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("shopId", shopId);
        params.put("channelTypeId", channelTypeId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Staff> result = query.list();
        return result;
    }

    public List<Staff> findByIdIm(Session imSession, Long staffId, Long status) {
        StringBuilder sql = new StringBuilder().append("select name, isdn from Staff where staff_Id = :staffId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("staffId", staffId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = imSession.createSQLQuery(sql.toString());
        buildParameter(query, params);
        List result = query.list();
        List<Staff> listStaff = new ArrayList<Staff>();
        for (Object obj : result) {
            Object[] array = (Object[])obj;
            Staff staff = new Staff();
            staff.setIsdn(array[1]== null ? "" : array[1].toString());
            staff.setName(array[0]== null ? "" : array[0].toString());
            listStaff.add(staff);
        }
        return listStaff;
    }
}
