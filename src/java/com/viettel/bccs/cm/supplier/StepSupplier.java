package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.Step;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class StepSupplier extends BaseSupplier {

    public StepSupplier() {
        logger = Logger.getLogger(StepSupplier.class);
    }

    public List<Step> findById(Session cmPosSession, Long stepId, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Step where stepId = :stepId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("stepId", stepId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Step> result = query.list();
        return result;
    }
}
