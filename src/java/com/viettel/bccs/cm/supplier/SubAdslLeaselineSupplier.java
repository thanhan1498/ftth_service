package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.ws.model.output.CoordinateSubDrawCable;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class SubAdslLeaselineSupplier extends BaseSupplier {

    public SubAdslLeaselineSupplier() {
        logger = Logger.getLogger(SubAdslLeaselineSupplier.class);
    }

    public List<SubAdslLeaseline> findById(Session cmPosSession, Long subId) {
        StringBuilder sql = new StringBuilder().append(" from SubAdslLeaseline where subId = :subId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("subId", subId);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<SubAdslLeaseline> result = query.list();
        return result;
    }
   
    /**
     * @author : duyetdk
     * @des: find coordinate by subId
     * @param account
     * @since 15-03-2018
     */
    public List<CoordinateSubDrawCable> findCoordinateBySubId(Session cmPosSession, String account) {
        StringBuilder sql = new StringBuilder().append(" select d.lat lat, d.lng lng, d.location location, d.order_number oderNumber "
                + " from sub_req_adsl_ll s "
                + " join sub_draw_cable d on s.id = d.req_id "
                + " and s.account = :account "
                + " and d.status = :status "
                + " order by d.order_number ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("account", account);
        params.put("status", Constants.STATUS_USE);
        Query query = cmPosSession.createSQLQuery(sql.toString())
                .addScalar("lat", Hibernate.DOUBLE)
                .addScalar("lng", Hibernate.DOUBLE)
                .addScalar("location", Hibernate.LONG)
                .addScalar("oderNumber", Hibernate.LONG)
                .setResultTransformer(Transformers.aliasToBean(CoordinateSubDrawCable.class));
        buildParameter(query, params);
        List<CoordinateSubDrawCable> result = query.list();
        return result;
    }
    
    /**
     * @author : duyetdk
     * @des: findByIsdnOrAccount
     * @since 16/05/2019
     * @param cmPosSession
     * @param isdn
     * @return 
     */
    public List<SubAdslLeaseline> findByIsdn(Session cmPosSession, String isdn) {
        String sql = " FROM SubAdslLeaseline WHERE isdn = :isdn OR account = :account ";
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("isdn", isdn.trim());
        params.put("account", isdn.trim());
        Query query = cmPosSession.createQuery(sql);
        buildParameter(query, params);
        List<SubAdslLeaseline> result = query.list();
        return result;
    }
}
