/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.StaffDetail;
import com.viettel.bccs.cm.model.SubFbConfig;
import com.viettel.bccs.cm.model.SubFbConfigDetail;
import com.viettel.bccs.cm.model.SubFbConfigLog;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.bccs.cm.util.ResourceUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author duyetdk
 */
public class SubFbConfigSupplier extends BaseSupplier {

    public SubFbConfigSupplier() {
        logger = Logger.getLogger(SubFbConfigSupplier.class);
    }

    public List<SubFbConfig> find(Session cmPosSession, Long id, Long taskManagementId, Long status, String account, Long statusConfig, Long subId, String typeConf) {
        StringBuilder sql = new StringBuilder().append(" from SubFbConfig where 1=1 ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        if (id != null && id > 0L) {
            sql.append(" and id = :id ");
            params.put("id", id);
        }
        if (taskManagementId != null && taskManagementId > 0L) {
            sql.append(" and taskManagementId = :taskManagementId ");
            params.put("taskManagementId", taskManagementId);
        }
        if (subId != null && subId > 0L) {
            sql.append(" and subId = :subId ");
            params.put("subId", subId);
        }
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        if (account != null && !account.trim().isEmpty()) {
            sql.append(" and account = :account ");
            params.put("account", account);
        }
        if (statusConfig != null) {
            sql.append(" and statusConf = :statusConfig ");
            params.put("statusConfig", statusConfig);
        }
        if (typeConf != null) {
            sql.append(" and typeConf = :typeConf ");
            params.put("typeConf", typeConf);
        }
        sql.append(" order by task_management_id desc");

        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<SubFbConfig> result = query.list();
        return result;
    }

    public void buildQuery(StringBuilder sql, HashMap params, Long status, Long statusConfig, Long duration,
            String createUser, String account, String province) {
        if (sql == null || params == null) {
            return;
        }
        if (duration != null && duration > 0L) {
            sql.append(" and createDate >= trunc(sysdate - :duration) ");
            params.put("duration", duration);
        }
        if (duration != null && duration > Long.parseLong(ResourceUtils.getResource("DURATION"))) {
            sql.append(" and createDate >= trunc(sysdate - :durationOver) ");
            params.put("durationOver", Long.parseLong(ResourceUtils.getResource("DURATION")));
        }

        if (duration == null || duration <= 0L) {
            sql.append(" and createDate >= trunc(sysdate) ");
        }
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        if (statusConfig != null) {
            sql.append(" and statusConf = :statusConfig ");
            params.put("statusConfig", statusConfig);
        }
        if (createUser != null && !createUser.trim().isEmpty()) {
            sql.append(" and lower(createUser) like lower(:createUser) ");
            params.put("createUser", "%" + createUser + "%");
        }
        if (account != null && !account.trim().isEmpty()) {
            sql.append(" and lower(account) like lower(:account) ");
            params.put("account", "%" + account + "%");
        }
        if (province != null && !province.trim().isEmpty()) {
            sql.append(" and lower(province) = lower(:province) ");
            params.put("province", province.trim());
        }
    }

    public Long count(Session cmPosSession, Long status, Long statusConfig, Long duration, String createUser, String account, String province) {
        StringBuilder sql = new StringBuilder().append(" select count(*) from SubFbConfig where 1 = 1 and (typeConf = :typeConf or typeConf = :typeTv or typeConf = :typeDigital) ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("typeConf", Constants.TYPE_CONF_IMT);
        params.put("typeTv", Constants.TYPE_CONF_TV);
        params.put("typeDigital", Constants.TYPE_CONF_DIGITAL);
        buildQuery(sql, params, status, statusConfig, duration, createUser, account, province);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public List<SubFbConfigDetail> findAll(Session cmPosSession, Long status, Long statusConfig, Long duration, Integer start, Integer max,
            String createUser, String account, String province) {
        StringBuilder sql = new StringBuilder().append(" select s.ID Id,s.TASK_MANAGEMENT_ID taskManagementId,s.STATUS status,s.STATUS_CONF statusConf,s.SUB_ID subId,s.ACCOUNT account, "
                + " s.INFRA_IMG_ID ifraImgId,nvl(s.INFRA_LAT,0) infraLat,nvl(s.INFRA_LONG,0) infraLong,s.CUST_IMG_ID custImgId,nvl(s.CUST_LAT,0) custLat,nvl(s.CUST_LONG,0) custLong,s.CREATE_USER createUser, "
                + " s.CREATE_DATE createDate,s.LAST_UPDATE_USER lastUpdateUser,s.LAST_UPDATE_DATE lastUpdateDate,s.REASON_CODE reasonCode,s.DESCRIPTION description, t.TASK_NAME taskName "
                + " from SUB_FB_CONF s, TASK_MANAGEMENT t where 1 = 1 and s.TASK_MANAGEMENT_ID = t.TASK_MNGT_ID and (s.type_Config = :typeConf or s.type_Config = :typeTv or type_Config = :typeDigital)");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("typeConf", Constants.TYPE_CONF_IMT);
        params.put("typeTv", Constants.TYPE_CONF_TV);
        params.put("typeDigital", Constants.TYPE_CONF_DIGITAL);
        if (duration != null && duration > 0L) {
            sql.append(" and s.CREATE_DATE >= trunc(sysdate - :duration) ");
            params.put("duration", duration);
        }
        if (duration != null && duration > Long.parseLong(ResourceUtils.getResource("DURATION"))) {
            sql.append(" and s.CREATE_DATE >= trunc(sysdate - :durationOver) ");
            params.put("durationOver", Long.parseLong(ResourceUtils.getResource("DURATION")));
        }

        if ((duration == null || duration <= 0L)) {
            sql.append(" and s.CREATE_DATE >= trunc(sysdate) ");
        }
        if (status != null) {
            sql.append(" and s.STATUS = :status ");
            params.put("status", status);
        }
        if (statusConfig != null) {
            sql.append(" and s.STATUS_CONF = :statusConfig ");
            params.put("statusConfig", statusConfig);
        }
        if (createUser != null && !createUser.trim().isEmpty()) {
            sql.append(" and lower(s.CREATE_USER) like lower(:createUser) ");
            params.put("createUser", "%" + createUser + "%");
        }
        if (account != null && !account.trim().isEmpty()) {
            sql.append(" and lower(s.ACCOUNT) like lower(:account) ");
            params.put("account", "%" + account + "%");
        }
        if (province != null && !province.trim().isEmpty()) {
            sql.append(" and lower(s.PROVINCE) = lower(:province) ");
            params.put("province", province.trim());
        }
        sql.append(" order by s.LAST_UPDATE_DATE desc ");
        Query query = cmPosSession.createSQLQuery(sql.toString())
                .addScalar("Id", Hibernate.LONG)
                .addScalar("taskManagementId", Hibernate.LONG)
                .addScalar("status", Hibernate.LONG)
                .addScalar("statusConf", Hibernate.LONG)
                .addScalar("subId", Hibernate.LONG)
                .addScalar("account", Hibernate.STRING)
                .addScalar("ifraImgId", Hibernate.LONG)
                .addScalar("infraLat", Hibernate.STRING)
                .addScalar("infraLong", Hibernate.STRING)
                .addScalar("custImgId", Hibernate.LONG)
                .addScalar("custLat", Hibernate.STRING)
                .addScalar("custLong", Hibernate.STRING)
                .addScalar("createUser", Hibernate.STRING)
                .addScalar("createDate", Hibernate.TIMESTAMP)
                .addScalar("lastUpdateUser", Hibernate.STRING)
                .addScalar("lastUpdateDate", Hibernate.TIMESTAMP)
                .addScalar("reasonCode", Hibernate.STRING)
                .addScalar("description", Hibernate.STRING)
                .addScalar("taskName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(SubFbConfigDetail.class));
        buildParameter(query, params);
        if (start != null) {
            query.setFirstResult(start);
        }
        if (max != null) {
            query.setMaxResults(max);
        }
        List<SubFbConfigDetail> result = query.list();
        return result;
    }

    public SubFbConfig insert(Session cmPosSession, Long taskManagementId, Long subId, String account, Long ifraImgId,
            String infraLat, String infraLong, Long custImgId, String custLat, String custLong, Date nowDate,
            String createUser, String province, String typeConf, Long status, Long statusConf) {
        SubFbConfig subFbConfig = new SubFbConfig();
        Long subFbId = getSequence(cmPosSession, Constants.SUB_FB_CONF_SEQ);
        subFbConfig.setId(subFbId);
        subFbConfig.setTaskManagementId(taskManagementId);
        subFbConfig.setStatus(status);
        subFbConfig.setSubId(subId);
        subFbConfig.setAccount(account);
        subFbConfig.setIfraImgId(ifraImgId);
        subFbConfig.setInfraLat(infraLat);
        subFbConfig.setInfraLong(infraLong);
        subFbConfig.setCustImgId(custImgId);
        subFbConfig.setCustLat(custLat);
        subFbConfig.setCustLong(custLong);
        subFbConfig.setCreateDate(nowDate);
        subFbConfig.setLastUpdateDate(nowDate);
        subFbConfig.setCreateUser(createUser.trim().toUpperCase());
        subFbConfig.setProvince(province != null ? province.trim().toUpperCase() : "");
        subFbConfig.setTypeConfig(typeConf == null ? Constants.TYPE_CONF_IMT : typeConf);
        subFbConfig.setStatusConf(statusConf);
        cmPosSession.save(subFbConfig);
        cmPosSession.flush();
        return subFbConfig;
    }

    public SubFbConfig update(Session cmPosSession, SubFbConfig subFbConfig, Long subId, String account, Long ifraImgId,
            String infraLat, String infraLong, Long custImgId, String custLat, String custLong,
            Date nowDate, String updateUser) {
        subFbConfig.setStatus(Constants.PENDING);
        subFbConfig.setSubId(subId);
        subFbConfig.setAccount(account);
        if (ifraImgId != null && ifraImgId > 0L) {
            subFbConfig.setIfraImgId(ifraImgId);
        }
        if (infraLat != null && !infraLat.trim().isEmpty()) {
            subFbConfig.setInfraLat(infraLat);
        }
        if (infraLong != null && !infraLong.trim().isEmpty()) {
            subFbConfig.setInfraLong(infraLong);
        }
        if (custImgId != null && custImgId > 0L) {
            subFbConfig.setCustImgId(custImgId);
        }
        if (custLat != null && !custLat.trim().isEmpty()) {
            subFbConfig.setCustLat(custLat);
        }
        if (custLong != null && !custLong.trim().isEmpty()) {
            subFbConfig.setCustLong(custLong);
        }
        subFbConfig.setCreateDate(nowDate);
        subFbConfig.setLastUpdateDate(nowDate);
        subFbConfig.setLastUpdateUser(updateUser.trim().toUpperCase());

        cmPosSession.update(subFbConfig);
        cmPosSession.flush();
        return subFbConfig;
    }

    public Long countBy(Session cmPosSession, Long taskManagementId, Long status_a, Long status_b) throws Exception {
        StringBuilder sql = new StringBuilder().append(" select count(*) from SubFbConfig where taskManagementId = :taskManagementId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("taskManagementId", taskManagementId);
        if (status_a != null && status_b != null) {
            sql.append(" and (status = :status_a or status = :status_b) ");
            params.put("status", status_a);
            params.put("status", status_b);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public SubFbConfigLog insertLog(Session cmPosSession, Long subFbConfigId, Long statusConfig,
            String request, String response, String action, String description, Date nowDate, String createUser, String errorCode) {
        String temp = null;
        if (description != null && description.length() > 200) {
            temp = description.substring(0, 190);
        }
        SubFbConfigLog subFbConfigLog = new SubFbConfigLog();
        Long subFbLogId = getSequence(cmPosSession, Constants.SUB_FB_CONF_LOG_SEQ);
        subFbConfigLog.setId(subFbLogId);
        subFbConfigLog.setSubFbConfigId(subFbConfigId);
        subFbConfigLog.setStatusConfig(statusConfig);
        subFbConfigLog.setRequest(request);
        subFbConfigLog.setResponse(response);
        subFbConfigLog.setAction(action);
        subFbConfigLog.setDescription(temp);
        subFbConfigLog.setCreateDate(nowDate);
        subFbConfigLog.setCreateUser(createUser);
        subFbConfigLog.setErrorCode(errorCode);

        cmPosSession.save(subFbConfigLog);
        cmPosSession.flush();
        return subFbConfigLog;
    }

    public void insertLogImt(Session cmPosSession, Long subFbConfigId, Long statusConfig,
            String request, String response, String action, String description, Date nowDate, String createUser, String errorCode) {
        String temp = null;
        description = description == null ? "" : description.replaceAll("  ", "");
        if (description != null && description.length() > 200) {
            temp = description.substring(0, 190);
        }
        SubFbConfigLog subFbConfigLog = new SubFbConfigLog();
        Long subFbLogId = getSequence(cmPosSession, Constants.SUB_FB_CONF_LOG_SEQ);
        subFbConfigLog.setId(subFbLogId);
        subFbConfigLog.setSubFbConfigId(subFbConfigId);
        subFbConfigLog.setStatusConfig(statusConfig);
        subFbConfigLog.setRequest(request);
        subFbConfigLog.setResponse(response);
        subFbConfigLog.setAction(action);
        subFbConfigLog.setDescription(temp);
        subFbConfigLog.setCreateDate(nowDate);
        subFbConfigLog.setCreateUser(createUser);
        subFbConfigLog.setErrorCode(errorCode);
        cmPosSession.save(subFbConfigLog);
        cmPosSession.flush();
    }

    public int insertImageDetail(Session cmSession, Long cusImgId, String imageName, String imageType, String objType, String userCreate, String lattitude, String longitude)
            throws Exception {
        try {
            String sql = "insert into cust_image_detail (id,cust_img_id,create_date,image_name,image_type,obj_type,status,latitude,longitude)  values(cust_image_detail_seq.nextval,?,sysdate,?,?,?,?,?,?)";

            Query query = cmSession.createSQLQuery(sql);
            query.setParameter(0, cusImgId);
            query.setParameter(1, imageName);
            query.setParameter(2, imageType);
            query.setParameter(3, objType);
            query.setParameter(4, Long.valueOf(1L));
            query.setParameter(5, lattitude);
            query.setParameter(6, longitude);
            return query.executeUpdate();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        }
    }

    public List<SubFbConfigLog> findAllHistoryById(Session cmPosSession, Long subFbConfigId) {
        StringBuilder sql = new StringBuilder().append(" from SubFbConfigLog where 1=1 and subFbConfigId = :subFbConfigId order by createDate desc");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("subFbConfigId", subFbConfigId);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<SubFbConfigLog> result = query.list();
        return result;
    }

    public void updateImage(Session cmSession, Long id) {
        try {
            String sql = "update cust_image set status = ? where id = ?";
            Query query = cmSession.createSQLQuery(sql);
            query.setParameter(0, Long.valueOf(Constants.STATUS_NOT_USE));
            query.setParameter(1, id);
            query.executeUpdate();
            cmSession.flush();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public List<StaffDetail> findStaff(Session imSession, String staffCode, Long staffId) {
        StringBuilder sql = new StringBuilder().append("select OWNER_CODE from account_agent s where isdn = ? ");
        Query query = imSession.createSQLQuery(sql.toString());
        String isdn = staffCode.startsWith("0") ? staffCode.replaceFirst("0", "") : staffCode.startsWith("855") ? staffCode.replaceFirst("855", "") : staffCode;
        query.setParameter(0, isdn);
        List listObj = query.list();
        if (listObj != null && !listObj.isEmpty()) {
            staffCode = listObj.get(0).toString();
        }
        sql = new StringBuilder().append(" select s.name name,s.tel phone,sh.shop_code team,sh.province province from staff s, shop sh "
                + " where s.shop_id = sh.shop_id ");

        HashMap<String, Object> params = new HashMap<String, Object>();

        if (staffCode != null && !staffCode.trim().isEmpty()) {
            sql.append(" and lower(s.STAFF_CODE) = lower(:staffCode) ");
            params.put("staffCode", staffCode.trim());
        }
        if (staffId != null && staffId > 0L) {
            sql.append(" and s.STAFF_ID = :staffId ");
            params.put("staffId", staffId);
        }

        query = imSession.createSQLQuery(sql.toString())
                .addScalar("name", Hibernate.STRING)
                .addScalar("phone", Hibernate.STRING)
                .addScalar("team", Hibernate.STRING)
                .addScalar("province", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(StaffDetail.class));
        buildParameter(query, params);
        List<StaffDetail> result = query.list();
        return result;
    }
}
