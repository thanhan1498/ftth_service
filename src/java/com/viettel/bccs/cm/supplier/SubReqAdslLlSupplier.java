package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.SubReqAdslLl;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubReqAdslLlSupplier extends BaseSupplier {

    public SubReqAdslLlSupplier() {
        logger = Logger.getLogger(SubReqAdslLlSupplier.class);
    }

    public List<SubReqAdslLl> findById(Session cmPosSession, Long id) {
        StringBuilder sql = new StringBuilder().append(" from SubReqAdslLl where id = :id ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("id", id);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<SubReqAdslLl> result = query.list();
        return result;
    }
}
