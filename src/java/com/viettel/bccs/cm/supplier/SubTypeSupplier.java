package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.SubType;
import com.viettel.bccs.cm.util.Constants;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class SubTypeSupplier extends BaseSupplier {

    public SubTypeSupplier() {
        logger = Logger.getLogger(SubTypeSupplier.class);
    }

    public List<SubType> findById(Session cmPosSession, String subType, Long status) {
        StringBuilder sql = new StringBuilder().append(" from SubType where subType = :subType ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("subType", subType);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by name ");
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<SubType> result = query.list();
        return result;
    }

    public List<SubType> findByMap(Session cmPosSession, String serviceType, String productCode) {
        /*StringBuilder sql = new StringBuilder()
                .append(" select s from SubType s, MapSubTypeProduct m, ApParam ap ")
                .append(" where s.subType = m.subType and s.status = :status and m.status = :status and m.productCode = :productCode ")
                .append(" and ").append(createLike(" m.serviceType ", "serviceType"))
                .append(" order by s.name ");*/
        
        StringBuilder sql = new StringBuilder();
        sql.append(" From SubType as s where status = :status ")
                .append(" and subType in (Select subType From MapSubTypeProduct Where productCode = :productCode and (lower(serviceType) like lower(:serviceType) escape '\\') and status = :status1) ")
                .append(" or subType in ( Select paramValue From ApParam where paramType = 'DEFINE_VIP_SUB_TYPE' and status = 1 and instr(lower(paramName), lower(:serviceType1)) > 0) ")
                .append(" order by NLSSORT(subType,'NLS_SORT=vietnamese') ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("status", Constants.STATUS_USE);
        params.put("status1", Constants.STATUS_USE);
        params.put("productCode", productCode);
        params.put("serviceType", formatLike(serviceType));
        params.put("serviceType1", serviceType);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<SubType> result = query.list();
        return result;
    }
}
