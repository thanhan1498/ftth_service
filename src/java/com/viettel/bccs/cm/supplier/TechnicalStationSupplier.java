package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.TechnicalStation;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class TechnicalStationSupplier extends BaseSupplier {

    public TechnicalStationSupplier() {
        logger = Logger.getLogger(TechnicalStationSupplier.class);
    }

    public List<TechnicalStation> findByStation(Session cmPosSession, Long stationId, Long status) {
        /*StringBuilder sql = new StringBuilder().append(" from TechnicalStation where stationId = :stationId ");
         HashMap<String, Object> params = new HashMap<String, Object>();
         params.put("stationId", stationId);
         if (status != null) {
         sql.append(" and status = :status ");
         params.put("status", status);
         }
         sql.append(" order by teamId desc ");
         Query query = cmPosSession.createQuery(sql.toString());
         buildParameter(query, params);
         List<TechnicalStation> result = query.list();
         return result;*/

        StringBuilder sql = new StringBuilder().append("select "
                + "TEAM_ID teamId, "
                + "TEAM_CODE teamCode, "
                + "STATION_ID stationId, "
                + "STATION_CODE stationCode, "
                + "STATUS status, "
                + "null as provinceCode, "
                + "null as provinceName "
                + "from technical_station where station_Id = :stationId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("stationId", stationId);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        sql.append(" order by team_Id desc ");
        Query query = cmPosSession.createSQLQuery(sql.toString())
                .addScalar("stationId", Hibernate.LONG)
                .addScalar("stationCode", Hibernate.STRING)
                .addScalar("provinceCode", Hibernate.STRING)
                .addScalar("provinceName", Hibernate.STRING)
                .addScalar("teamId", Hibernate.LONG)
                .addScalar("teamCode", Hibernate.STRING)
                .addScalar("status", Hibernate.LONG)
                .setResultTransformer(Transformers.aliasToBean(TechnicalStation.class));
        buildParameter(query, params);
        List<TechnicalStation> result = query.list();
        return result;
    }

    public List<TechnicalStation> findStationByTeam(Session cmPosSession, Long teamId, String province) {
        String sql = "SELECT sh.province provinceCode , a.name provinceName,  t.STATION_ID as stationId, t.STATION_CODE as stationCode "
                + "  FROM shop sh "
                + "  join area a "
                + "    on sh.province = a.area_code "
                + "  join technical_station t "
                + "    on t.team_id = sh.shop_id"
                + " where t.status = 1 ";
        if (teamId != null) {
            sql += " and sh.shop_id = ? ";
        }
        if (province != null) {
            sql += " and sh.PROVINCE = ? ";
        }
        sql += " group by sh.province  , a.name ,  t.STATION_ID  , t.STATION_CODE"
                + " order by sh.province, stationCode desc  ";
        Query query = cmPosSession.createSQLQuery(sql)
                .addScalar("stationId", Hibernate.LONG)
                .addScalar("stationCode", Hibernate.STRING)
                .addScalar("provinceCode", Hibernate.STRING)
                .addScalar("provinceName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(TechnicalStation.class));

        if (teamId != null) {
            query.setParameter(0, teamId);
        }
        if (province != null) {
            query.setParameter(0, province);
        }
        return query.list();
    }
}
