package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.Token;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class TokenSupplier extends BaseSupplier {

    public TokenSupplier() {
        logger = Logger.getLogger(TokenSupplier.class);
    }

    public List<Token> findByToken(Session cmPosSession, String token, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Token where token = :token ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("token", token);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Token> result = query.list();
        return result;
    }

    public List<Token> findByUser(Session cmPosSession, String userName, Long status) {
        StringBuilder sql = new StringBuilder().append(" from Token where upper(userName) = upper( :userName ) ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("userName", userName);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Token> result = query.list();
        return result;
    }

    public List<Token> findByStaffId(Session cmPosSession, Long staffId) {
        StringBuilder sql = new StringBuilder().append(" from Token where staffId = :staffId and status = :status");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("staffId", staffId);
        params.put("status", Constants.STATUS_LOGED);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<Token> result = query.list();
        return result;
    }

    public Token insert(Session cmPosSession, String userName, String serial, String tokenVal, Date issueDate, Long staffId, String permission, String staffName, Long status, String deviceId) {
        Long tokenId = getSequence(cmPosSession, Constants.TOKEN_SEQ);
        Token token = new Token(tokenId, userName.toUpperCase(), serial, tokenVal, issueDate, issueDate, staffId, staffName, status, deviceId);
        cmPosSession.save(token);
        cmPosSession.flush();
        return token;
    }

    public Token update(Session cmPosSession, Token token) {
        cmPosSession.update(token);
        cmPosSession.flush();
        return token;
    }

    public void disableToken(Session cmPosSession, String tokenVal) {
        List<Token> list = findByToken(cmPosSession, tokenVal, Constants.STATUS_USE);
        if (list != null && !list.isEmpty()) {
            Token token = list.get(0);
            token.setStatus(Constants.STATUS_NOT_USE);
            cmPosSession.update(token);
            cmPosSession.flush();
        }

    }
}
