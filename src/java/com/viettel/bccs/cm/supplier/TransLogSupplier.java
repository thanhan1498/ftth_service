package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.TransLog;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class TransLogSupplier extends BaseSupplier {

    public TransLogSupplier() {
        logger = Logger.getLogger(TransLogSupplier.class);
    }

    public TransLog insert(Session cmPosSession, Long tokenId, String token, String className, String method, String params, Long excute, Date issueDate, String response) {
        Long transId = getSequence(cmPosSession, Constants.TRANS_LOG_SEQ);
        TransLog result = new TransLog(transId, tokenId, token, issueDate, className, method, params, excute, response);
        cmPosSession.save(result);
        cmPosSession.flush();
        return result;
    }
}
