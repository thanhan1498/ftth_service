package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.VAdslRequest;
import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.bccs.cm.util.DateTimeUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class VAdslRequestSupplier extends BaseSupplier {

    public VAdslRequestSupplier() {
        logger = Logger.getLogger(VAdslRequestSupplier.class);
    }

    public void buildQuery(StringBuilder sql, HashMap params, Long isSmart, String addUser, String custName, String idNo, String serviceAlias, String account, Long subReqStatus, Date fromDate, Date toDate) {
        if (sql == null || params == null) {
            return;
        }
        //<editor-fold defaultstate="collapsed" desc="isSmart">
        if (isSmart != null) {
            sql.append(" and isSmart = :isSmart ");
            params.put("isSmart", isSmart);
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="addUser">
        if (addUser != null) {
            addUser = addUser.trim();
            if (!addUser.isEmpty()) {
                sql.append(" and upper(addUser) = upper( :addUser ) ");
                params.put("addUser", addUser);
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="custName">
        if (custName != null) {
            custName = custName.trim();
            if (!custName.isEmpty()) {
                sql.append(" and ").append(createLike("custName", "custName"));
                params.put("custName", formatLike(custName));
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="idNo">
        if (idNo != null) {
            idNo = idNo.trim();
            if (!idNo.isEmpty()) {
                sql.append("  and ( upper(idNo) = upper( :idNo ) or upper(busPermitNo) = upper( :idNo )) ");
                params.put("idNo", idNo);
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="serviceAlias">
        if (serviceAlias != null) {
            serviceAlias = serviceAlias.trim();
            if (!serviceAlias.isEmpty()) {
                sql.append(" and serviceAlias = :serviceAlias ");
                params.put("serviceAlias", serviceAlias);
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="account">
        if (account != null) {
            account = account.trim();
            if (!account.isEmpty()) {
                sql.append(" and account = :account ");
                params.put("account", account);
            }
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="subReqStatus">
        if (subReqStatus != null) {
            sql.append(" and subReqStatus = :subReqStatus ");
            params.put("subReqStatus", subReqStatus);
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fromDate">
        if (fromDate != null) {
            sql.append(" and reqDate >= :fromDate ");
            params.put("fromDate", fromDate);
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="toDate">
        if (toDate != null) {
            sql.append(" and reqDate < :toDate ");
            params.put("toDate", DateTimeUtils.addDay(toDate, 1));
        }
        //</editor-fold>
    }

    public Long count(Session cmPosSession, Long isSmart, String addUser, String custName, String idNo, String serviceAlias, String account, Long subReqStatus, Date fromDate, Date toDate, List<Long> lstSubReqStatus) {
        StringBuilder sql = new StringBuilder().append(" select count(*) from VAdslRequest where 1 = 1 ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        buildQuery(sql, params, isSmart, addUser, custName, idNo, serviceAlias, account, subReqStatus, fromDate, toDate);
        if (lstSubReqStatus != null && !lstSubReqStatus.isEmpty()) {
            sql.append(" and subReqStatus in ( :lstSubReqStatus ) ");
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        if (lstSubReqStatus != null && !lstSubReqStatus.isEmpty()) {
            query.setParameterList("lstSubReqStatus", lstSubReqStatus);
        }
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }

    public List<VAdslRequest> find(Session cmPosSession, Long isSmart, String addUser, String custName, String idNo, String serviceAlias, String account, Long subReqStatus, Date fromDate, Date toDate, List<Long> lstSubReqStatus, Integer start, Integer max) {
        StringBuilder sql = new StringBuilder().append(" from VAdslRequest where 1 = 1 ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        buildQuery(sql, params, isSmart, addUser, custName, idNo, serviceAlias, account, subReqStatus, fromDate, toDate);
        if (lstSubReqStatus != null && !lstSubReqStatus.isEmpty()) {
            sql.append(" and subReqStatus in ( :lstSubReqStatus ) ");
        }
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        if (lstSubReqStatus != null && !lstSubReqStatus.isEmpty()) {
            query.setParameterList("lstSubReqStatus", lstSubReqStatus);
        }
        if (start != null) {
            query.setFirstResult(start);
        }
        if (max != null) {
            query.setMaxResults(max);
        }
        List<VAdslRequest> result = query.list();
        return result;
    }
}
