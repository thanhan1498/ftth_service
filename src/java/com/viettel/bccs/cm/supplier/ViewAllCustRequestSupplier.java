package com.viettel.bccs.cm.supplier;

import com.viettel.bccs.cm.model.ViewAllCustRequest;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ViewAllCustRequestSupplier extends BaseSupplier {

    public ViewAllCustRequestSupplier() {
        logger = Logger.getLogger(ViewAllCustRequestSupplier.class);
    }

    public List<ViewAllCustRequest> findById(Session cmPosSession, Long subReqId) {
        StringBuilder sql = new StringBuilder().append(" from ViewAllCustRequest where subReqId = :subReqId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("subReqId", subReqId);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<ViewAllCustRequest> result = query.list();
        return result;
    }

    public List<ViewAllCustRequest> findByCustReqId(Session cmPosSession, Long custReqId) {
        StringBuilder sql = new StringBuilder().append(" from ViewAllCustRequest where custReqId = :custReqId ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("custReqId", custReqId);
        Query query = cmPosSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<ViewAllCustRequest> result = query.list();
        return result;
    }
}
