/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.supplier.im;

import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ConvertUtils;
import com.viettel.brcd.ws.model.output.DebitStaffDetail;
import com.viettel.brcd.ws.model.output.DebitTransHistoryDetail;
import com.viettel.im.database.BO.BankReceipt;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author duyetdk
 */
public class DebitSupplier extends BaseSupplier {

    //<editor-fold defaultstate="collapsed" desc="getListDebitStaff">
    /**
     * @author: duyetdk
     * @since 9/3/2019
     * @des: lay danh sach cong no cua nhan vien
     * @param imPosSession
     * @param staffCode
     * @param shopPath
     * @param status
     * @param staffIdLogin
     * @return
     */
    public List<DebitStaffDetail> getListDebitStaff(Session imPosSession, String staffCode, String shopPath, Long status, Long staffIdLogin, String currency) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT ds.id, st.staff_id staffId, st.name staffName, st.account_emoney phoneNumber, ds.debit_current_amount currentAmount, ");
        sql.append(" ds.debit_amount_limit limitAmount, ds.debit_before_amount beforeAmount, ds.debit_pay_on_date deadlineAmount, ds.debit_deadline_date deadline, ");
        sql.append(" ds.create_date createDate, ds.create_user createUser, ds.action_type actionType, ds.description, st.staff_code staffCode, sh.shop_code shopCode, nvl(ds.currency, 'USD') currency ");
        sql.append(" FROM staff st, shop sh, debit_staff ds WHERE st.staff_id = ds.staff_id AND st.shop_id = sh.shop_id and ds.status = 1  ");
//        sql.append(" AND ds.create_date >= trunc(sysdate - :duration) ");

        HashMap<String, Object> params = new HashMap<String, Object>();
//        params.put("duration", Long.parseLong(ResourceUtils.getResource("DURATION")));
        if (shopPath != null && !shopPath.trim().isEmpty()) {
            sql.append(" AND LOWER(sh.shop_path) LIKE :shopPath ");
            params.put("shopPath", "%" + shopPath.trim().toLowerCase() + "%");
        }

        if (staffCode != null && !staffCode.trim().isEmpty()) {
            sql.append(" AND LOWER(st.staff_code) LIKE :staffCode ");
            params.put("staffCode", "%" + staffCode.trim().toLowerCase() + "%");
        }

        if (status != null) {
            if (Constants.NOT_OK.equals(status)) {
                sql.append(" AND ds.debit_current_amount > 0 ");
            } else {
                sql.append(" AND ds.debit_current_amount <= 0 ");
            }
        }

        if (staffIdLogin != null && staffIdLogin > 0L) {
            sql.append(" AND ds.staff_id = :staffId ");
            params.put("staffId", staffIdLogin);
        }
        if (currency != null && !currency.isEmpty()) {
            sql.append(" AND nvl(ds.currency, 'USD') = :currency ");
            params.put("currency", currency);
        }
        sql.append(" ORDER BY ds.debit_deadline_date DESC ");
        Query query = imPosSession.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("currentAmount", Hibernate.DOUBLE)
                .addScalar("limitAmount", Hibernate.DOUBLE)
                .addScalar("beforeAmount", Hibernate.DOUBLE)
                .addScalar("deadlineAmount", Hibernate.DOUBLE)
                .addScalar("deadline", Hibernate.TIMESTAMP)
                .addScalar("description", Hibernate.STRING)
                .addScalar("createUser", Hibernate.STRING)
                .addScalar("createDate", Hibernate.TIMESTAMP)
                .addScalar("actionType", Hibernate.STRING)
                .addScalar("staffCode", Hibernate.STRING)
                .addScalar("shopCode", Hibernate.STRING)
                .addScalar("staffId", Hibernate.LONG)
                .addScalar("staffName", Hibernate.STRING)
                .addScalar("phoneNumber", Hibernate.STRING)
                .addScalar("currency", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(DebitStaffDetail.class));

        buildParameter(query, params);
        List<DebitStaffDetail> result = query.list();
        return result;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="buildQueryTransHistory">
    /**
     * @author: duyetdk
     * @since 11/3/2019
     * @des: truy van lich su giao dich
     * @param sql
     * @param params
     * @param debitStaffId
     * @param payMethod
     * @param payCode
     * @param status
     */
    public void buildQueryTransHistory(StringBuilder sql, HashMap params, Long debitStaffId, String payMethod,
            String payCode, Long status) {
        if (sql == null || params == null) {
            return;
        }
        sql.append(" AND dl.debit_staff_id = :debitStaffId ");
        params.put("debitStaffId", debitStaffId);
        if (status != null) {
            if (Constants.NOT_OK.equals(status)) {
                sql.append(" AND dl.payment_status = 0 ");
            } else {
                sql.append(" AND dl.payment_status = 1 ");
            }
        }

        if (payMethod != null && !payMethod.trim().isEmpty()) {
            sql.append(" AND LOWER(dl.payment_method) = :payMethod ");
            params.put("payMethod", payMethod.trim().toLowerCase());
        }

        if (payCode != null && !payCode.trim().isEmpty()) {
            sql.append(" AND LOWER(dtd.payment_code) LIKE :payCode ");
            params.put("payCode", "%" + payCode.trim().toLowerCase() + "%");
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="countTransHistory">
    /**
     * @author: duyetdk
     * @since 11/3/2019
     * @des: count tong so ban ghi lich su giao dich cua tung nhan vien
     * @param imPosSession
     * @param debitStaffId
     * @param payMethod
     * @param payCode
     * @param status
     * @return
     */
    public Long countTransHistory(Session imPosSession, Long debitStaffId, String payMethod,
            String payCode, Long status) {
        StringBuilder sql = new StringBuilder().append(" SELECT count(*) FROM debit_log dl, debit_trans dt, debit_trans_detail dtd WHERE dt.id = dtd.debit_trans_id AND dtd.debit_log_id = dl.id ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        buildQueryTransHistory(sql, params, debitStaffId, payMethod, payCode, status);
        Query query = imPosSession.createSQLQuery(sql.toString());
        buildParameter(query, params);
        Object obj = query.uniqueResult();
        return ConvertUtils.toLong(obj);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="transHistory">
    /**
     * @author: duyetdk
     * @since 11/3/2019
     * @des: lay lich su giao dich tung nhan vien
     * @param imPosSession
     * @param debitStaffId
     * @param payMethod
     * @param payCode
     * @param status
     * @param start
     * @param max
     * @return
     */
    public List<DebitTransHistoryDetail> transHistory(Session imPosSession, Long debitStaffId, String payMethod,
            String payCode, Long status, Integer start, Integer max) {
        HashMap<String, Object> params = new HashMap<String, Object>();
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT dt.id debitTransId, dt.trans_code transCode, dt.trans_type transType, dt.trans_name transName, ");
        sql.append(" dl.payment_status status, dtd.debit_amount amount, dl.payment_method payMethod, dtd.payment_code payCode, ");
        sql.append(" dtd.create_date transDate, dl.description, dtd.create_user createUser, dt.description receiveFrom, nvl(dt.currency, 'USD') currency ");
        sql.append(" FROM debit_log dl, debit_trans dt, debit_trans_detail dtd WHERE dt.id = dtd.debit_trans_id AND dtd.debit_log_id = dl.id ");
        buildQueryTransHistory(sql, params, debitStaffId, payMethod, payCode, status);
        sql.append(" ORDER BY dtd.create_date DESC ");
        Query query = imPosSession.createSQLQuery(sql.toString())
                .addScalar("debitTransId", Hibernate.LONG)
                .addScalar("transCode", Hibernate.STRING)
                .addScalar("transType", Hibernate.LONG)
                .addScalar("transName", Hibernate.STRING)
                .addScalar("status", Hibernate.LONG)
                .addScalar("amount", Hibernate.DOUBLE)
                .addScalar("payMethod", Hibernate.STRING)
                .addScalar("payCode", Hibernate.STRING)
                .addScalar("transDate", Hibernate.TIMESTAMP)
                .addScalar("description", Hibernate.STRING)
                .addScalar("createUser", Hibernate.STRING)
                .addScalar("receiveFrom", Hibernate.STRING)
                .addScalar("currency", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(DebitTransHistoryDetail.class));
        buildParameter(query, params);
        if (start != null) {
            query.setFirstResult(start);
        }
        if (max != null) {
            query.setMaxResults(max);
        }
        List<DebitTransHistoryDetail> result = query.list();
        return result;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getListBankReceipt">
    /**
     * @author: duyetdk
     * @since 15/5/2019
     * @des: lay danh sach giao dich dang cho phe duyet
     * @param imPosSession
     * @param staffId
     * @param status
     * @return
     */
    public List<BankReceipt> getListBankReceipt(Session imPosSession, Long staffId, String status) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT bank_receipt_id bankReceiptId, amount, content, shop_id shopId ");
        sql.append(" FROM bank_receipt WHERE clear_debit_trans = 1 ");

        HashMap<String, Object> params = new HashMap<String, Object>();
        if (status != null && !status.trim().isEmpty()) {
            sql.append(" AND status = :status ");
            params.put("status", status);
        }

        if (staffId != null && staffId > 0L) {
            sql.append(" AND staff_id = :staffId ");
            params.put("staffId", staffId);
        }

        sql.append(" ORDER BY create_datetime ");
        Query query = imPosSession.createSQLQuery(sql.toString())
                .addScalar("bankReceiptId", Hibernate.LONG)
                .addScalar("amount", Hibernate.DOUBLE)
                .addScalar("content", Hibernate.STRING)
                .addScalar("shopId", Hibernate.LONG)
                .setResultTransformer(Transformers.aliasToBean(BankReceipt.class));

        buildParameter(query, params);
        List<BankReceipt> result = query.list();
        return result;
    }
    //</editor-fold>
}
