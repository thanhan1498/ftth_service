package com.viettel.bccs.cm.supplier.im;

import com.viettel.bccs.api.Util.Constant;
import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.im.database.BO.AccountAgent;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vietnn
 */
public class ImSupplier extends BaseSupplier {

    public ImSupplier() {
        logger = Logger.getLogger(ImSupplier.class);
    }

    public List<AccountAgent> findAccountAgent(Session imSession, String ownerCode, String serial, Long status) {
        StringBuilder sql = new StringBuilder().append(" from AccountAgent where upper(ownerCode) = upper( :ownerCode ) and serial = :serial ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("ownerCode", ownerCode);
        params.put("serial", serial);
        if (status != null) {
            sql.append(" and status = :status ");
            params.put("status", status);
        }
        Query query = imSession.createQuery(sql.toString());
        buildParameter(query, params);
        List<AccountAgent> result = query.list();
        return result;
    }

    public AccountAgent checkAccountAgentByIsdn(Session imSession, String isdn) {
        StringBuilder sql = new StringBuilder().append(" from AccountAgent where isdn = ? and status = ?  ");
        Query query = imSession.createQuery(sql.toString());
        query.setParameter(0, isdn);
        query.setParameter(1, Constant.STATUS_USE);
        List<AccountAgent> result = query.list();
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }

    public boolean checkAccountAgentIsN3_P3(Session imSession, String ownerCode) {
        StringBuilder sql = new StringBuilder().append(" from Staff where upper(staff_code) = ? and status = ? and agent_type in (26,44)");
        Query query = imSession.createQuery(sql.toString());
        query.setParameter(0, ownerCode.toUpperCase());
        query.setParameter(1, Constant.STATUS_USE);
        List result = query.list();
        if (result != null && result.size() > 0) {
            return true;
        }
        return false;
    }

    public boolean checkAccountAgentByOwnerCode(Session imSession, String ownerCode) {
        StringBuilder sql = new StringBuilder().append(" from AccountAgent where upper(ownerCode) = ? and status = ? ");
        Query query = imSession.createQuery(sql.toString());
        query.setParameter(0, ownerCode.toUpperCase());
        query.setParameter(1, Constant.STATUS_USE);
        List result = query.list();
        if (result != null && result.size() > 0) {
            return true;
        }
        return false;
    }

    public boolean checkPassword(Session imSession, String isdn, String password) {
        StringBuilder sql = new StringBuilder().append(" from AccountAgent where isdn = ? and password = ? and accountType = '10' and status = ?  ");
        Query query = imSession.createQuery(sql.toString());
        query.setParameter(0, isdn);
        query.setParameter(1, password);
        query.setParameter(2, Constant.STATUS_USE);
        List result = query.list();
        if (result != null && result.size() > 0) {
            return true;
        }
        return false;
    }

    public void updatePassword(Session imSession, String isdn, String password) throws Exception {
        try {
            String sql = " update agent set mpin = ?, last_modified = sysdate where msisdn = ? and status = ? ";
            Query query = imSession.createSQLQuery(sql);
            query.setParameter(0, password);
            query.setParameter(1, isdn);
            query.setParameter(2, Constant.STATUS_USE);
            LogUtils.info(logger, "isdn -- " + isdn + " password -- " + password);
            int excute = query.executeUpdate();
            imSession.flush();
            LogUtils.info(logger, "updatePassword -- " + excute);
        } catch (Exception ex) {
            LogUtils.info(logger, ex.getMessage());
            LogUtils.info(logger, "updatePassword -- fail");
            throw ex;
        }
    }

    public AccountAgent getAccountAgentByOwnerCode(Session imSession, String ownerCode) {
        StringBuilder sql = new StringBuilder().append(" from AccountAgent where upper(ownerCode) = ? and status = ? for update nowait 15 ");
        Query query = imSession.createQuery(sql.toString());
        query.setParameter(0, ownerCode.toUpperCase());
        query.setParameter(1, Constant.STATUS_USE);
        List<AccountAgent> result = query.list();
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }
    
    public AccountAgent getAccountAgentInfo(Session imSession, String isdn) {
        StringBuilder sql = new StringBuilder().append(" from AccountAgent where isdn = ? and status = ?  ");
        Query query = imSession.createQuery(sql.toString());
        query.setParameter(0, isdn);
        query.setParameter(1, Constant.STATUS_USE);
        List<AccountAgent> result = query.list();
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }
}
