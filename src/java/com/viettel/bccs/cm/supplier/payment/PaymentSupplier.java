package com.viettel.bccs.cm.supplier.payment;

import com.viettel.bccs.api.debit.DAO.DebitBusiness;
import com.viettel.bccs.cm.bussiness.ActionAuditBussiness;
import com.viettel.bccs.cm.bussiness.ActionDetailBussiness;
import com.viettel.bccs.cm.common.util.Constant;
import com.viettel.bccs.cm.bussiness.ContractBussiness;
import com.viettel.bccs.cm.bussiness.ShopBussiness;
import com.viettel.bccs.cm.bussiness.StaffBussiness;
import com.viettel.bccs.cm.dao.CustomerDAO;
import com.viettel.bccs.cm.dao.ReasonDAO;
import com.viettel.bccs.cm.dao.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.dao.SubDepositAdslLlDAO;
import com.viettel.bccs.cm.database.DAO.InvoiceListUtilsDAO;
import com.viettel.bccs.cm.model.ActionAudit;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.StaffInfo;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubDepositAdslLl;
import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.bccs.cm.supplier.CustomerSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.dao.im.IMDAO;
import com.viettel.brcd.ws.model.output.CustomerPayment;
import com.viettel.brcd.ws.model.output.PayAdvanceBillOut;
import com.viettel.brcd.ws.model.output.PaymentReportRevenue;
import com.viettel.brcd.ws.model.output.PaymentReportSubscriber;
import com.viettel.brcd.ws.model.output.UnpaidPaymentDetail;
import com.viettel.common.ViettelService;
import com.viettel.im.database.BO.AccountAgent;
import com.viettel.im.database.BO.InvoiceListBean;
import com.viettel.im.database.DAO.InvoiceListDAO;
import com.viettel.payment.API.PaymentDebit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class PaymentSupplier extends BaseSupplier {

    private static final String FINISH_SQL_LINE = " ";

    public PaymentSupplier() {
        logger = Logger.getLogger(PaymentSupplier.class);
    }

    public String getCustomerDebit(Session paymentSession, Long custId) throws Exception {
        String debit = new PaymentDebit().getDebitByCustomer(paymentSession, custId);
        return debit;
    }

    public PayAdvanceBillOut payAdvance(Session paymentSession, Session imSession, Session cmPosSession, Long reasonId, Long staffId, Long contractId, Long invoiceListId, String isdn, String tid, Double payAmountDis, Double promotionDis) throws Exception {
        String message = null;
        PayAdvanceBillOut paymentOut = new PayAdvanceBillOut();
        paymentOut.setErrorCode(Constants.ERROR_CODE_1);

        StaffInfo staffInfo = new StaffInfo();
        CustomerPayment customerPayment = new CustomerPayment();
        paymentOut.setLstStaffInfo(staffInfo);
        paymentOut.setCustomerPayment(customerPayment);

        //Check reason
        Reason reason = new ReasonDAO().findById(cmPosSession, reasonId);
        if (reason == null) {
            message = "Reason is not exists";
        } else {
            //Check Contract
            com.viettel.bccs.cm.model.Contract contract = new ContractBussiness().findByIdActive(cmPosSession, contractId);
            if (contract == null) {
                message = "Contract is not exists";
            } else {
                //Check Customer
                List<com.viettel.bccs.cm.model.Customer> cus = new CustomerSupplier().findById(cmPosSession, contract.getCustId(), Constants.STATUS_USE);
                if (cus == null || cus.isEmpty()) {
                    message = "Customer is not exists";
                } else {
                    /*Check AccountAgent*/
                    com.viettel.im.database.DAO.AccountAgentDAO accAgentDAO = new com.viettel.im.database.DAO.AccountAgentDAO();
                    accAgentDAO.setSession(imSession);
                    AccountAgent accAgent = accAgentDAO.findByIdOwner(staffId, Constant.OBJECT_TYPE_STAFF, Constant.STATUS_USE);
                    Staff staff = new StaffBussiness().findById(cmPosSession, staffId);
                    if (accAgent == null && !Constants.SHOP_TYPE_AGENT_DELEGATE.equals(staff.getType())) {
                        message = "Account Agent is not exists";
                    } else {
                        /*Check limit for Pay Advance*/
                        Double limitPayment = 0d;
                        Double curPayment = 0d;
                        if (accAgent != null) {
                            limitPayment = accAgent.getLimitDebtPayment() == null ? 0d : accAgent.getLimitDebtPayment();
                            curPayment = accAgent.getCurrentDebtPayment() == null ? 0d : accAgent.getCurrentDebtPayment();
                        }
                        if (limitPayment.compareTo(curPayment + reason.getPayAdvAmount()) < 0
                                && !Constants.SHOP_TYPE_AGENT_DELEGATE.equals(staff.getType())) {
                            message = "Your limit payment is not enough. Max pay advanced amount: " + String.valueOf(limitPayment - curPayment);
                        } else {
                            /*Kiem tra dải hóa dơn*/
                            InvoiceListDAO invoiceListDAO = new InvoiceListDAO(imSession);
                            List<InvoiceListBean> lstInvoice = null;
                            Shop shop = new ShopBussiness().findById(cmPosSession, staff.getShopId());
                            if (shop == null) {
                                message = "Shop of Staff is not exists";
                            } else {
                                /*Dai hoa don default lay tu kho VT nen k phu thuoc vao bien chuyen vao*/
                                lstInvoice = new InvoiceListUtilsDAO().getAvailableInvoiceList(imSession, shop.getShopCode(), staff.getStaffCode());
                                if (lstInvoice == null || lstInvoice.isEmpty()) {
                                    message = "can not find Invoice List";
                                } else {
                                    Double disCount = 0d;
                                    /*Kiem tra CDBR co thuoc chinh sach moi*/
                                    boolean flagIsNewPlanCDBR = new CustomerSupplier().isCheckNewConnect(cmPosSession, contractId);
                                    if (flagIsNewPlanCDBR && tid == null) {
                                        List<SubAdslLeaseline> sub = new SubAdslLeaselineDAO().findByContractId(cmPosSession, contractId);
                                        if (sub != null && !sub.isEmpty()) {
                                            SubAdslLeaseline subAdslLl = sub.get(0);
                                            double deposit = new SubDepositAdslLlDAO().getTotalCurrentDeposit(cmPosSession, subAdslLl.getSubId());
                                            /*Kiem tra tien coc*/
                                            if (deposit > 0d) {
                                                /*Kiem tra no cuoc*/
                                                PaymentSupplier paymentSup = new PaymentSupplier();
                                                boolean isDebit = paymentSup.isAllDebitSub(paymentSession, subAdslLl.getSubId());
                                                if (!isDebit) {
                                                    /*Kiem tra so lan Pay advance*/
                                                    Long numOfPayAdvance = paymentSup.NumberOfPayAdvance(paymentSession, subAdslLl.getContractId());
                                                    if (numOfPayAdvance > 0) {
                                                        disCount = deposit;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    Double promotion = disCount < reason.getPayAdvAmount() ? 0d : disCount - reason.getPayAdvAmount();
                                    /*check limit cong no cua nhan vien va xem co giao dich nao nhan vien chua nop tien ve metfone*/
                                    if (Constants.SHOP_TYPE_AGENT_DELEGATE.equals(staff.getType())) {
                                        if (!new DebitBusiness().checkLockTransaction(imSession, staffId, reason.getPayAdvAmount() - disCount,"USD")) {
                                            message = "You are over limit or transaction is expred, please transfer money to Metfone";
                                            paymentOut.setErrorDecription(message);
                                            return paymentOut;
                                        }
                                    }
                                    InvoiceListBean invoiceListBean = new IMDAO().getInvoiceListById(imSession, lstInvoice.get(0).getInvoiceListId());
                                    if (invoiceListBean != null) {
                                        String currInvoiceNo = invoiceListBean.getCurrInvoiceNo() != null ? String.valueOf(invoiceListBean.getCurrInvoiceNo()) : null;
                                        /*Ghi Log*/
                                        com.viettel.bccs.cm.supplier.InvoiceListStaffSupplier.insertInvoiceListStaffLog(cmPosSession, staffId, staff.getStaffCode(), currInvoiceNo, reason.getCode(), payAmountDis == null ? reason.getPayAdvAmount() : payAmountDis, new Date(), contractId, invoiceListBean.getInvoiceListId(), accAgent != null && accAgent.getAccountType().equals(Constants.AGENT_ACCOUNT_TYPE_CTV_COLLECTION) ? accAgent.getOwnerCode() : "");
                                        //cmPosSession.flush();
                                        /*insert vao InvoiceListStaff de tien trinh payment xu ly */
                                        com.viettel.bccs.cm.supplier.InvoiceListStaffSupplier.insertInvoiceListStaff(paymentSession, staffId, staff.getStaffCode(), currInvoiceNo, reason.getCode(), payAmountDis == null ? reason.getPayAdvAmount() : payAmountDis, new Date(), contractId, invoiceListBean.getInvoiceListId(), promotionDis == null ? reason.getPromotion() + promotion : promotionDis, accAgent != null && accAgent.getAccountType().equals(Constants.AGENT_ACCOUNT_TYPE_CTV_COLLECTION) ? accAgent.getOwnerCode() : "", tid);
                                        //paymentSession.flush();
                                        /*Cap nhat hoa don*/
                                        Long resultCallIM = invoiceListDAO.updateInvoiceToUsing(shop.getShopId(), staffId, invoiceListBean.getSerialNo(), invoiceListBean.getBlockNo(), invoiceListBean.getCurrInvoiceNo(), cus.get(0).getName(), Double.valueOf(reason.getPayAdvAmount()), reason.getPayAdvAmount() / 1.1, com.viettel.bccs.cm.common.util.Constant.INVOICE_VTC_PAC.equals(shop.getShopCode()) ? shop.getShopCode() : null);
                                        /*increase current debt payment*/
                                        if (accAgent != null && accAgent.getAccountType().equals(Constants.AGENT_ACCOUNT_TYPE_CTV_COLLECTION)) {
                                            accAgent.setCurrentDebtPayment(curPayment + reason.getPayAdvAmount() - disCount);
                                            accAgent.setLastUpdateTime(new Date());
                                            accAgent.setLastUpdateUser("PAYMENT_ADVANCE");
                                            imSession.update(accAgent);
                                        }
                                        //imSession.flush();
                                        /**
                                         * Luong rut tien dat coc deposit tru
                                         * vao cuoc thue bao theo chinh sach
                                         * CDBR moi
                                         */
                                        Long changeCode = 0L;
                                        ViettelService changeSubInfoResponse = new ViettelService();
                                        SubAdslLeaseline subAdslLl = new SubAdslLeaselineDAO().findByAccount(cmPosSession, isdn);
                                        if (disCount > 0d && tid == null) {
                                            /*Luu log rut tien*/
                                            Date nowDate = new Date();
                                            SubDepositAdslLl subDepositAdslLl = new SubDepositAdslLl(
                                                    subAdslLl.getSubId(),
                                                    Constant.STATUS_USE,
                                                    staff.getStaffCode(),
                                                    "mBCCS",
                                                    Constants.REASON_TYPE_PAY_ADVANCE.toString(),
                                                    nowDate,
                                                    subAdslLl.getServiceType(),
                                                    -disCount,
                                                    invoiceListBean.getInvoiceListId(),
                                                    invoiceListBean.getCurrInvoiceNo());
                                            subDepositAdslLl.setId(getSequence(cmPosSession, Constants.SUB_DEPOSIT_ADSL_LL_SEQ));
                                            /*Tao phieu chi IM*/
                                            Customer cust = new CustomerDAO().findById(cmPosSession, contract.getCustId());
                                            Long receivePtExpenseId = new SubDepositAdslLlDAO().insertDepositOfInventory(imSession, cmPosSession, reasonId, Constant.IM_DEPOSIT_TYPE_RECEIVER,
                                                    Constants.REASON_TYPE_PAY_ADVANCE, subAdslLl.getDeposit(), Constant.SERVICE_ADSL_ID_WEBSERVICE,
                                                    cust, staffId, shop.getShopId(), staff.getStaffCode(), subAdslLl.getAccount(), subAdslLl.getSubId(), null);
                                            subDepositAdslLl.setReceiptEnxenseId(receivePtExpenseId);
                                            cmPosSession.save(subDepositAdslLl);
                                            /*Ghi log action audit*/

                                            ActionAudit action = new ActionAuditBussiness().insert(cmPosSession, nowDate, Constant.ACTION_ADD_DEPOSIT, reasonId, shop.getShopCode(), staff.getStaffCode(), Constant.ACTION_AUDIT_PK_TYPE_SUBSCRIBER, subAdslLl.getSubId(), "mBCCS", "withdraw " + subAdslLl.getDeposit().toString() + " to pay advance");
                                            // CommonLog.logAction(cmPosSession, "mBCCS", actionAuditId, subId, Constant.ACTION_AUDIT_PK_TYPE_SUBSCRIBER, reasonId,
                                            //         Constant.ACTION_ADD_DEPOSIT, "withdraw " + subAdslLl.getDeposit().toString() + " to pay advance", accAgent.getOwnerCode(), nowDate);

                                            //CommonLog.logAuditDetail(cmPosSession, subId, actionAuditId, "SUB_ADSL_LL", "DEPOSIT", subAdslLl.getDeposit(), 0, nowDate);
                                            new ActionDetailBussiness().insert(cmPosSession, action.getActionAuditId(), "SUB_ADSL_LL", subAdslLl.getSubId(), "DEPOSIT", subAdslLl.getDeposit(), 0, nowDate);
                                            /*clear deposit*/
                                            subAdslLl.setDeposit(0d);
                                            cmPosSession.update(subDepositAdslLl);

                                            /*call provisioning to update change deposit*/
                                            Map lstParam = new HashMap();
                                            lstParam.put("DEPOSIT", 0);

                                            /*if (!subAdslLl.getStatus().equals(Constant.SUB_STATUS_CANCEL) && !subAdslLl.getStatus().equals(Constant.SUB_STATUS_TRANSFER_CONTRACT)) {
                                             req.setAttribute("viewDetail", "true");
                                             changeSubInfoResponse = ProvisioningAP.changeSubscriberInfo(subAdslLl, lstParam, Constant.SERVICE_ALIAS_ADSL, userToken, req);
                                             if (changeSubInfoResponse != null
                                             && changeSubInfoResponse.get("responseCode") != null
                                             && "0".equals(changeSubInfoResponse.get("responseCode").toString().trim())) {
                                             changeCode = Long.valueOf(changeSubInfoResponse.get("responseCode").toString().trim());
                                             }
                                             }*/
                                        }

                                        /* --------------------------------------------- */
                                        if (resultCallIM < 0) {
                                            message = "An error occured when update invoice!";
                                        } else {
                                            //Set Staff info
                                            if (accAgent != null) {
                                                staffInfo.setIsdn(accAgent.getIsdn());
                                            } else {
                                                staffInfo.setIsdn(staff.getIsdn());
                                            }

                                            staffInfo.setStaffName(staff.getStaffCode());
                                            //Set service info
                                            customerPayment.setCustomerName(cus.get(0).getName());
                                            String nameInvoice = new com.viettel.bccs.cm.database.DAO.InvoiceListUtilsDAO().getFullNameOfInvoice(imSession, paymentSession, invoiceListBean.getSerialNo(), invoiceListBean.getBlockNo(), invoiceListBean.getCurrInvoiceNo());
                                            customerPayment.setTinNo(nameInvoice);
                                            customerPayment.setPayMethod("Pay Advance");
                                            customerPayment.setReason(reason.getName());
                                            customerPayment.setIsdn(contract.getMainIsdn());
                                            customerPayment.setContractNo(contract.getContractNo());
                                            String temp = disCount > 0d ? String.valueOf(disCount) + " $ - deposit" : "";
                                            customerPayment.setDescription(temp);
                                            customerPayment.setDiscount(String.valueOf(reason.getPromotion()));
                                            customerPayment.setTotalAmount(String.valueOf(reason.getPayAdvAmount() - disCount));
                                            customerPayment.setPayAmount(String.valueOf(reason.getPayAdvAmount() + reason.getPromotion()));
                                            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                            customerPayment.setCreateDateStr(dateFormat.format(new Date()));
                                            message = "Success";
                                            paymentOut.setErrorCode(Constants.ERROR_CODE_0);
                                        }
                                    } else {
                                        message = "can not find Invoice List";
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }
        paymentOut.setErrorDecription(message);
        return paymentOut;
    }

    public List findCollectionStaffList(Session cmPos, Long staffId) {

        StringBuilder sqlBuffer = new StringBuilder();
        sqlBuffer.append(" SELECT ");
        sqlBuffer.append(" co.COLLECTION_STAFF_ID as collectionStaffId, ");
        sqlBuffer.append(" co.COLLECTION_STAFF_CODE as collectionStaffCode, ");
        sqlBuffer.append(" co.NAME as name ");
        sqlBuffer.append(" FROM ");
        sqlBuffer.append(" payment.COLLECTION_STAFF co ");
        sqlBuffer.append(" WHERE 1 = 1 ");
        sqlBuffer.append(" AND ");
        sqlBuffer.append(" co.COLLABORATOR = 1 ");//La CTV
        sqlBuffer.append(" AND ");
        sqlBuffer.append(" (co.STATUS = 1 or co.STATUS =  2) ");
        sqlBuffer.append(" AND ");
        sqlBuffer.append(" co.COLLECTION_STAFF_ID = ? ");
        sqlBuffer.append(" ORDER BY co.COLLECTION_STAFF_CODE ASC ");

        Query queryObject = cmPos.createSQLQuery(sqlBuffer.toString());
        queryObject.setParameter(0, staffId);

        return queryObject.list();
    }

    /**
     * @des: kiem tra no cuoc theo subId
     * @param payment
     * @param subId
     * @return
     * @throws Exception
     */
    public boolean isAllDebitSub(Session payment, Long subId) throws Exception {
        PaymentDebit paymentDebit = new PaymentDebit();
        return paymentDebit.getCheckAllDebitSub(payment, subId);

    }

    /**
     * @des: dem so lan pay advance
     * @param session
     * @param contractId
     * @return
     */
    public Long NumberOfPayAdvance(Session payment, Long contractId) {

        String sql = "SELECT to_char(count(1)) FROM Invoice_list_staff s WHERE s.contract_id = ?";
        Query query = payment.createSQLQuery(sql);
        query.setParameter(0, contractId);
        String numOfPayAdvance = (String) query.uniqueResult();
        return Long.valueOf(numOfPayAdvance == null ? "0" : numOfPayAdvance);

    }

    //phuonghc 06072020
    public List<PaymentReportSubscriber> getTotalSubcribersByUserLevel(Session sessionPayment, String branchCode, String showroomCode, String staffCode) {
        String groupBy = "";
        StringBuilder sql = new StringBuilder("SELECT COUNT(*) totalPerUnit FROM SUB_ASSIGN" + FINISH_SQL_LINE);
        sql.append("WHERE CYCLE_DATE = TRUNC(TO_DATE(SYSDATE), 'mm') AND status = 1 " + FINISH_SQL_LINE);
        if (StringUtils.isNotEmpty(branchCode)) {
            sql.append("AND PROVINCE_CODE=:branchCode" + FINISH_SQL_LINE);
        }
        if (StringUtils.isNotEmpty(showroomCode)) {
            sql.append("AND SHOP_CODE=:showroomCode" + FINISH_SQL_LINE);
        }
        if (StringUtils.isNotEmpty(staffCode)) {
            sql.append("AND STAFF_CODE=:staffCode" + FINISH_SQL_LINE);
        }
        if (StringUtils.isEmpty(branchCode)) {
            groupBy = "PROVINCE_CODE";
            sql.append("GROUP BY PROVINCE_CODE");
        } else {
            if (StringUtils.isNotEmpty(branchCode) && StringUtils.isEmpty(showroomCode)) {
                groupBy = "SHOP_CODE";
                sql.append("GROUP BY SHOP_CODE");
            } else {
                groupBy = "STAFF_CODE";
                sql.append("GROUP BY STAFF_CODE");
            }
        }
        if (StringUtils.isNotEmpty(groupBy)) {
            sql.insert(7, groupBy + " unitName, ");
        } else {
            String notify = "Invalid value to calculate total subscriber";
            logger.error(notify);
            throw new HibernateException(notify);
        }

        Query query = sessionPayment.createSQLQuery(sql.toString())
                .addScalar("unitName", Hibernate.STRING)
                .addScalar("totalPerUnit", Hibernate.INTEGER)
                .setResultTransformer(Transformers.aliasToBean(PaymentReportSubscriber.class));
        if (StringUtils.isNotEmpty(branchCode)) {
            query.setParameter("branchCode", branchCode);
        }
        if (StringUtils.isNotEmpty(showroomCode)) {
            query.setParameter("showroomCode", showroomCode);
        }
        if (StringUtils.isNotEmpty(staffCode)) {
            query.setParameter("staffCode", staffCode);
        }
        try {
            List<PaymentReportSubscriber> result = query.list();
            if (result == null || result.isEmpty()) {
                return new ArrayList<PaymentReportSubscriber>();
            }
            return result;
        } catch (HibernateException he) {
            logger.error("An error occurded when count total subscriber," + he.getMessage());
            return new ArrayList<PaymentReportSubscriber>();
        }
    }

    public List<PaymentReportSubscriber> getTotalPaidSubscriberByUserLevel(Session sessionPayment, String branchCode, String showroomCode, String staffCode) {
        String groupBy = "";
        StringBuilder sql = new StringBuilder("SELECT COUNT(*) totalPerUnit "
                + "FROM SUB_ASSIGN sa INNER JOIN SUBSCRIBER sub "
                + "ON sa.account = sub.isdn "
                + "INNER JOIN DEBIT_SUB ds "
                + "ON ds.sub_id = sub.sub_id "
                + "AND sa.contract_id = ds.contract_id "
                + "AND sa.cycle_date = trunc(TO_DATE(SYSDATE), 'mm') "
                + "AND sa.status = 1 "
                + "AND ds.sta_of_cycle - ds.payment <=0.5 AND ds.bill_cycle = TRUNC(TO_DATE(SYSDATE), 'mm')" + FINISH_SQL_LINE);
        if (StringUtils.isNotEmpty(branchCode)) {
            sql.append(" AND sa.PROVINCE_CODE=:branchCode" + FINISH_SQL_LINE);
        }
        if (StringUtils.isNotEmpty(showroomCode)) {
            sql.append("AND sa.SHOP_CODE =:showroomCode" + FINISH_SQL_LINE);
        }
        if (StringUtils.isNotEmpty(staffCode)) {
            sql.append("AND sa.STAFF_CODE=:staffCode" + FINISH_SQL_LINE);
        }
        if (StringUtils.isEmpty(branchCode)) {
            groupBy = "sa.PROVINCE_CODE";
            sql.append("GROUP BY sa.PROVINCE_CODE");
        } else {
            if (StringUtils.isNotEmpty(branchCode) && StringUtils.isEmpty(showroomCode)) {
                groupBy = "sa.SHOP_CODE";
                sql.append("GROUP BY sa.SHOP_CODE");
            } else {
                groupBy = "sa.STAFF_CODE";
                sql.append("GROUP BY sa.STAFF_CODE");
            }
        }
        if (StringUtils.isNotEmpty(groupBy)) {
            sql.insert(7, groupBy + " unitName, ");
        } else {
            String notify = "Invalid value to calculate total subscriber paid";
            logger.error(notify);
            throw new HibernateException(notify);
        }
        Query query = sessionPayment.createSQLQuery(sql.toString())
                .addScalar("unitName", Hibernate.STRING)
                .addScalar("totalPerUnit", Hibernate.INTEGER)
                .setResultTransformer(Transformers.aliasToBean(PaymentReportSubscriber.class));
        if (StringUtils.isNotEmpty(branchCode)) {
            query.setParameter("branchCode", branchCode);
        }
        if (StringUtils.isNotEmpty(showroomCode)) {
            query.setParameter("showroomCode", showroomCode);
        }
        if (StringUtils.isNotEmpty(staffCode)) {
            query.setParameter("staffCode", staffCode);
        }
        try {
            List<PaymentReportSubscriber> result = query.list();
            if (result == null || result.isEmpty()) {
                return new ArrayList<PaymentReportSubscriber>();
            }
            return result;
        } catch (HibernateException he) {
            logger.error("An error occurded when count total subscriber paid!," + he.getMessage());
            return new ArrayList<PaymentReportSubscriber>();
        }
    }

    public List<PaymentReportRevenue> getTotalRevenueByUserLevel(Session sessionPayment, String branchCode, String showroomCode, String staffCode) {
        String groupBy = "";
        StringBuilder sql = new StringBuilder("SELECT SUM(ds.sta_of_cycle) totalPerUnit "
                + "FROM SUB_ASSIGN sa INNER JOIN SUBSCRIBER sub ON sa.account = sub.isdn "
                + "INNER JOIN DEBIT_SUB ds ON ds.sub_id = sub.sub_id "
                + "AND sa.contract_id = ds.contract_id "
                + "AND sa.cycle_date = trunc(TO_DATE(SYSDATE), 'mm') "
                + "AND sa.status = 1 "
                + "AND ds.bill_cycle = TRUNC(TO_DATE(SYSDATE), 'mm') ");

        if (StringUtils.isNotEmpty(branchCode)) {
            sql.append("AND sa.province_code =:branchCode" + FINISH_SQL_LINE);
        }
        if (StringUtils.isNotEmpty(showroomCode)) {
            sql.append("AND sa.shop_code =:showroomCode" + FINISH_SQL_LINE);
        }
        if (StringUtils.isNotEmpty(staffCode)) {
            sql.append("AND sa.staff_code =:staffCode" + FINISH_SQL_LINE);
        }
        if (StringUtils.isEmpty(branchCode)) {
            groupBy = "sa.PROVINCE_CODE";
            sql.append("GROUP BY sa.PROVINCE_CODE");
        } else {
            if (StringUtils.isNotEmpty(branchCode) && StringUtils.isEmpty(showroomCode)) {
                groupBy = "sa.SHOP_CODE";
                sql.append("GROUP BY sa.SHOP_CODE");
            } else {
                groupBy = "sa.STAFF_CODE";
                sql.append("GROUP BY sa.STAFF_CODE");
            }
        }
        if (StringUtils.isNotEmpty(groupBy)) {
            sql.insert(7, groupBy + " unitName, ");
        } else {
            String notify = "Invalid value to calculate total subscriber";
            logger.error(notify);
            throw new HibernateException(notify);
        }

        Query query = sessionPayment.createSQLQuery(sql.toString())
                .addScalar("unitName", Hibernate.STRING)
                .addScalar("totalPerUnit", Hibernate.DOUBLE)
                .setResultTransformer(Transformers.aliasToBean(PaymentReportRevenue.class));
        if (StringUtils.isNotEmpty(branchCode)) {
            query.setParameter("branchCode", branchCode);
        }
        if (StringUtils.isNotEmpty(showroomCode)) {
            query.setParameter("showroomCode", showroomCode);
        }
        if (StringUtils.isNotEmpty(staffCode)) {
            query.setParameter("staffCode", staffCode);
        }

        try {
            List<PaymentReportRevenue> result = query.list();
            if (result == null || result.isEmpty()) {
                return new ArrayList<PaymentReportRevenue>();
            }
            return result;
        } catch (HibernateException he) {
            logger.error("An error occurded when count total subscriber paid!," + he.getMessage());
            return new ArrayList<PaymentReportRevenue>();
        }

    }

    public List<PaymentReportRevenue> getTotalPaidRevenueByUserLevel(Session sessionPayment, String branchCode, String showroomCode, String staffCode) {
        String groupBy = "";
        StringBuilder sql = new StringBuilder("SELECT SUM(ds.payment) totalPerUnit "
                + "FROM SUB_ASSIGN sa INNER JOIN SUBSCRIBER sub ON sa.account = sub.isdn "
                + "INNER JOIN DEBIT_SUB ds ON ds.sub_id = sub.sub_id "
                + "AND sa.contract_id = ds.contract_id "
                + "AND sa.cycle_date = trunc(TO_DATE(SYSDATE), 'mm') "
                + "AND sa.status = 1 "
                + "AND ds.bill_cycle = TRUNC(TO_DATE(SYSDATE), 'mm') ");

        if (StringUtils.isNotEmpty(branchCode)) {
            sql.append("AND sa.province_code =:branchCode" + FINISH_SQL_LINE);
        }
        if (StringUtils.isNotEmpty(showroomCode)) {
            sql.append("AND sa.shop_code =:showroomCode" + FINISH_SQL_LINE);
        }
        if (StringUtils.isNotEmpty(staffCode)) {
            sql.append("AND sa.staff_code =:staffCode" + FINISH_SQL_LINE);
        }
        if (StringUtils.isEmpty(branchCode)) {
            groupBy = "sa.PROVINCE_CODE";
            sql.append("GROUP BY sa.PROVINCE_CODE");
        } else {
            if (StringUtils.isNotEmpty(branchCode) && StringUtils.isEmpty(showroomCode)) {
                groupBy = "sa.SHOP_CODE";
                sql.append("GROUP BY sa.SHOP_CODE");
            } else {
                groupBy = "sa.STAFF_CODE";
                sql.append("GROUP BY sa.STAFF_CODE");
            }
        }
        if (StringUtils.isNotEmpty(groupBy)) {
            sql.insert(7, groupBy + " unitName, ");
        } else {
            String notify = "Invalid value to calculate total subscriber";
            logger.error(notify);
            throw new HibernateException(notify);
        }

        Query query = sessionPayment.createSQLQuery(sql.toString())
                .addScalar("unitName", Hibernate.STRING)
                .addScalar("totalPerUnit", Hibernate.DOUBLE)
                .setResultTransformer(Transformers.aliasToBean(PaymentReportRevenue.class));
        if (StringUtils.isNotEmpty(branchCode)) {
            query.setParameter("branchCode", branchCode);
        }
        if (StringUtils.isNotEmpty(showroomCode)) {
            query.setParameter("showroomCode", showroomCode);
        }
        if (StringUtils.isNotEmpty(staffCode)) {
            query.setParameter("staffCode", staffCode);
        }

        try {
            List<PaymentReportRevenue> result = query.list();
            if (result == null || result.isEmpty()) {
                return new ArrayList<PaymentReportRevenue>();
            }
            return result;
        } catch (HibernateException he) {
            logger.error("An error occurded when count total subscriber paid!," + he.getMessage());
            return new ArrayList<PaymentReportRevenue>();
        }

    }

    public List<UnpaidPaymentDetail> getListUnpaidDetailByUserLevel(Session sessionPayment, String branchCode, String showroomCode, String staffCode) {
        boolean isSearchDetailSubscriber = false;
        StringBuilder sql = new StringBuilder("SELECT contractId FROM (SELECT sad.contract_id contractId, RANK() OVER (ORDER BY sad.customer_name) myrank  "
                + "FROM sub_assign_detail sad INNER JOIN sub_assign sa ON sad.contract_id = sa.contract_id AND sa.cycle_date = TRUNC(TO_DATE(SYSDATE), 'mm') "
                + "AND sad.contract_id IN (SELECT sa.contract_id FROM sub_assign sa INNER JOIN debit_sub ds ON sa.contract_id = ds.contract_id "
                + "AND ds.bill_cycle = TRUNC(SYSDATE,'mm') AND ds.sta_of_cycle - ds.payment > 0.5 AND sa.cycle_date = TRUNC(TO_DATE(SYSDATE), 'mm') ");
        if (StringUtils.isNotEmpty(branchCode)) {
            sql.append("AND sa.PROVINCE_CODE=:branchCode" + FINISH_SQL_LINE);
        }
        if (StringUtils.isNotEmpty(showroomCode)) {
            sql.append("AND sa.SHOP_CODE=:showroomCode" + FINISH_SQL_LINE);
        }
        if (StringUtils.isNotEmpty(staffCode)) {
            isSearchDetailSubscriber = true;
            sql.append("AND sa.STAFF_CODE=:staffCode" + FINISH_SQL_LINE);
        }
        sql.append(")");

        if (isSearchDetailSubscriber) {
            sql.append(") WHERE myRank BETWEEN 1 AND 1000");
        } else {
            sql.append(") WHERE myRank BETWEEN 1 AND 100");
        }

        Query query = sessionPayment.createSQLQuery(sql.toString())
                .addScalar("contractId", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(UnpaidPaymentDetail.class));
        if (StringUtils.isNotEmpty(branchCode)) {
            query.setParameter("branchCode", branchCode);
        }
        if (StringUtils.isNotEmpty(showroomCode)) {
            query.setParameter("showroomCode", showroomCode);
        }
        if (StringUtils.isNotEmpty(staffCode)) {
            query.setParameter("staffCode", staffCode);
        }
        try {
            List<UnpaidPaymentDetail> result = query.list();
            if (result == null || result.isEmpty()) {
                return new ArrayList<UnpaidPaymentDetail>();
            }
            return result;
        } catch (HibernateException he) {
            logger.error("An error occurded when count total subscriber," + he.getMessage());
            return new ArrayList<UnpaidPaymentDetail>();
        }
    }

    public String getCustIdFromContractId(Session sessionPayment, String contractId) {
        String sql = "SELECT DISTINCT CUST_ID FROM PAYMENT_SUB WHERE CONTRACT_ID=:contractId";
        Query query = sessionPayment.createSQLQuery(sql.toString());
        query.setParameter("contractId", contractId);
        List result = query.list();
        if (result == null || result.isEmpty()) {
            return "";
        }
        return result.get(0).toString();

    }
}
