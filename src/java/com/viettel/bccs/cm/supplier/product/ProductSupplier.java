package com.viettel.bccs.cm.supplier.product;

import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ValidateUtils;
import com.viettel.brcd.ws.model.output.Group;
import com.viettel.brcd.ws.model.output.Product;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author vanghv1
 */
public class ProductSupplier extends BaseSupplier {

    public ProductSupplier() {
        logger = Logger.getLogger(ProductSupplier.class);
    }

    public List<Group> getProductGroups(Session productSession, Long telecomServiceId) {
        StringBuilder sql = new StringBuilder()
                .append(" select distinct p.group_product as groupName ")
                .append(" from product p, product_offer po ")
                .append(" where p.product_id = po.product_id and p.telecom_service_id = :telecomServiceId and p.status = :pStatus ")
                .append(" and po.status = :poStatus and po.offer_type_id = :offType ")
                .append(" order by p.group_product ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("telecomServiceId", telecomServiceId);
        params.put("pStatus", Constants.STATUS_USE);
        params.put("poStatus", Constants.STATUS_USE);
        params.put("offType", "2");
        Query query = productSession.createSQLQuery(sql.toString())
                .addScalar("groupName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(Group.class));
        buildParameter(query, params);
        List<Group> result = query.list();
        return result;
    }

    public List<Product> getProducts(Session productSession, Long telecomServiceId, String groupProduct) {
        StringBuilder sql = new StringBuilder()
                .append(" select distinct po.offer_id as productId, po.offer_name as productName, p.product_code as productCode ")
                .append(" from product_offer po, product p ")
                .append(" where po.product_id = p.product_id and po.status = :poStatus and p.status = :pStatus ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("poStatus", Constants.STATUS_USE);
        if (telecomServiceId != null) {
            sql.append(" and p.telecom_service_id = :telecomServiceId ");
            params.put("telecomServiceId", telecomServiceId);
        }
        params.put("pStatus", Constants.STATUS_USE);
        if (!ValidateUtils.isNullOrEmpty(groupProduct)) {
//            sql.append(" and p.group_product is null ");
//        } else {
            sql.append(" and p.group_product = :groupProduct ");
            params.put("groupProduct", groupProduct.trim());
        }
        sql.append(" order by po.offer_name asc ");
        Query query = productSession.createSQLQuery(sql.toString())
                .addScalar("productId", Hibernate.LONG)
                .addScalar("productCode", Hibernate.STRING)
                .addScalar("productName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(Product.class));
        buildParameter(query, params);
        List<Product> result = query.list();
        return result;
    }
    
    /**
     * @author duyetdk
     * @since 12/5/2019
     * @des lay danh sach loai thue bao tra truoc
     * @param productSession
     * @param telecomServiceId
     * @param groupProduct
     * @return 
     */
    public List<Product> getListProductPre(Session productSession, Long telecomServiceId, String groupProduct) {
        StringBuilder sql = new StringBuilder()
                .append(" SELECT DISTINCT po.offer_id as productId, po.offer_name as productName, p.product_code as productCode ")
                .append(" FROM product_offer po, product p, telecom_service ts ")
                .append(" WHERE po.product_id = p.product_id AND p.telecom_service_id = ts.telecom_service_id AND po.status = :poStatus ")
                .append(" AND p.telecom_service_id = :telecomServiceId AND p.status = :pStatus ")
                .append(" AND ( po.end_date >= trunc(SYSDATE) OR po.end_date IS NULL ) AND MOD(p.service_type_id, 2) = 0 ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("poStatus", Constants.STATUS_USE);
        params.put("telecomServiceId", telecomServiceId);
        params.put("pStatus", Constants.STATUS_USE);
        if (!ValidateUtils.isNullOrEmpty(groupProduct)) {
            sql.append(" and p.group_product = :groupProduct ");
            params.put("groupProduct", groupProduct.trim());
        }
        sql.append(" ORDER BY po.offer_name asc ");
        Query query = productSession.createSQLQuery(sql.toString())
                .addScalar("productId", Hibernate.LONG)
                .addScalar("productCode", Hibernate.STRING)
                .addScalar("productName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(Product.class));
        buildParameter(query, params);
        List<Product> result = query.list();
        return result;
    }
}
