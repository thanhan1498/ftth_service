/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.util;

import com.viettel.bccs.cm.controller.DebitController;
import java.io.FileInputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

/**
 *
 * @author metfone
 */
public class AppCryptUtils {
    static Logger logger = Logger.getLogger(DebitController.class);

    public AppCryptUtils() {
    }
    
    public static String encryptRSA(String plainData, String publicKeyFilePath) {
        try {
            FileInputStream fis = new FileInputStream(publicKeyFilePath);
            byte[] byteKeyFromFile = new byte[fis.available()];
            fis.read(byteKeyFromFile);
            fis.close();

            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(byteKeyFromFile);
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PublicKey pubKey = factory.generatePublic(keySpec);

            // Mã hoá dữ liệu
            Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            c.init(Cipher.ENCRYPT_MODE, pubKey);
            byte encryptedByte[] = c.doFinal(plainData.getBytes());
            String encrypted = Base64.encodeBase64String(encryptedByte);
            return encrypted;
        } catch (Exception ex) {
            logger.error(ex);
        }
        return null;
    }
    
    public static String decryptRSA(String encryptedData, String privateKeyFilePath) {
        try {
            FileInputStream fis = new FileInputStream(privateKeyFilePath);
            byte[] byteKeyFromFile = new byte[fis.available()];
            fis.read(byteKeyFromFile);
            fis.close();
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(byteKeyFromFile);
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PrivateKey priKey = factory.generatePrivate(keySpec);
            // Giải mã dữ liệu
            Cipher c2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            c2.init(Cipher.DECRYPT_MODE, priKey);
            String decrypted = new String(c2.doFinal(Base64.decodeBase64(encryptedData)));
            return decrypted;
        } catch (Exception ex) {
            logger.error(ex);
        }
        return null;
    }

}
