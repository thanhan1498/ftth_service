/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.util;

import java.util.Random;
import org.apache.commons.lang.StringUtils;

/**
 * CommonUtils
 *
 * @author phuonghc
 */
public class CommonUtils {

    public static String rand(int min, int max) throws Exception {
        try {
            Random rn = new Random();
            int range = max - min + 1;
            int randomNum = min + rn.nextInt(range);
            return String.valueOf(randomNum);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Long checkIntAndLong(Double source) {
        String str = source.toString();
        try {
            if (StringUtils.isEmpty(str)) {
                return null;
            }
            String[] fm = str.split("\\.");
            if (fm.length > 0 && fm.length < 2) {
                return Long.parseLong(str);
            }
            if (fm.length >= 2) {
                Long i = Long.parseLong(fm[1]);
                return i > 0 ? null : Long.parseLong(fm[0]);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }
}
