package com.viettel.bccs.cm.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author linhlh2
 */
public class Constants {

    public static final String SESSION_SALARY = "salary";
    public static final String SESSION_CM_PRE = "cm_pre";
    public static final String SESSION_CM_POS = "cm_pos";
    public static final String SESSION_MERCHANT = "bccs_merchant";
    public static final String SESSION_ANY_PAY = "any_pay";
    public static final String SESSION_IM = "im";
    public static final String SESSION_CC = "cc";
    public static final String SESSION_PRODUCT = "product";
    public static final String SESSION_PAYMENT = "payment";
    public static final String SESSION_RA = "ra";
    public static final String SESSION_BI = "bi";
    public static final String SESSION_NIMS = "nims";
    public static final String SESSION_TARGET = "target";
    public static final String OBJECT = "object";
    public static final String OBJECT_ID = "object-id";
    public static final String PARENT_WINDOW = "parent-window";
    public static final String MENU_WINDOW = "menu-window";
    public static final String DATA = "data";
    public static final String ID = "ID";
    public static final String MAP_PARAMETER = "map-parameter";
    public static final String LIST_PARAMETER = "list-parameter";
    public static final String TITLE = "title";
    /**
     * Column
     */
    //Cong viec giao cho to doi
    public static final String CONTRACT_NO = "contractNo";
    public static final String SUB_ID = "subId";
    public static final String TASK_MNGT_ID = "taskMngtId";
    public static final String TASK_SHOP_MNGT_ID = "taskShopMngtId";
    public static final String TASK_STAFF_MNGT_ID = "taskStaffMngtId";
    public static final String JSG_ID = "jsgId";
    public static final String QUANTITY = "quantity";
    public static final String EXPORT_STATUS = "exportStatus";
    /**
     * Column
     */
    /**
     * Style
     */
    public static final String STYLE_COLUMN_MULTILINE = "white-space: normal;word-wrap:break-word;text-wrap:none;";
    public static final String STYLE_TEXT_ALIGN_CENTER = "text-align:center;";
    public static final String STYLE_TEXT_ALIGN_LEFT = "text-align:left;";
    public static final String STYLE_TEXT_ALIGN_RIGHT = "text-align:right;";
    public static final String STYLE_BORDER_NONE = "border:none;";
    public static final String STYLE_NO_PADDING = "padding:0;";
    public static final String STYLE_FONT_WEIGHT_BOLD = "font-weight: bold;";
    public static final String STYLE_FONT_STYLE_ITALIC = "font-style: italic;";
    /**
     * Class
     */
    public static final String CLASS_LINK_BUTTON = "link-button";
    public static final String CLASS_LINK_BUTTON_CL = "link-button-cl";
    public static final String CLASS_NO_STYLE = "no-style";
    public static final String CLASS_PROCESS_MENU_BAR = "processMenuBar";
    public static final String ERROR_CODE_0 = "0"; //sucess
    public static final String ERROR_CODE_1 = "1"; //error
    public static final String ERROR_CODE_2 = "2"; //invalid
    public static final String ERROR_CODE_3 = "3"; //time out
    public static final String ERROR_CODE_4 = "4"; //OTP is incorrect
    public static final String ERROR_CODE_5 = "5"; //OTP is exprired
    public static final String PRE_PAY = "0"; //tra truoc
    public static final String POS_PAY = "1"; //tra sau
    //Tham so ve dich vu
    public static final String SERVICE_SUB_TYPE_ADSL = "1";
    public static final String SERVICE_SUB_TYPE_LEASEDLINE = "2";
    public static final String SERVICE_ALIAS_MOBILE = "M";
    public static final String SERVICE_ALIAS_HOMEPHONE = "H";
    public static final String SERVICE_ALIAS_PSTN = "P";
    public static final String SERVICE_ALIAS_PSTN_TEMP = "P";
    public static final String SERVICE_ALIAS_ADSL = "A";
    public static final String SERVICE_ALIAS_FTTH = "F";
    public static final String SERVICE_ALIAS_ADSL_TEMP = "A";    //tam thoi them de lay thong tin trong bang reason cho A theo H
    public static final String SERVICE_ALIAS_FTTH_TEMP = "A";
    public static final String SERVICE_ALIAS_LEASEDLINE = "L";
    public static final String SERVICE_ALIAS_IPTV = "I";
    public static final String SERVICE_ALIAS_OFFICE_WAN = "O";
    public static final String SERVICE_ALIAS_WHITE_LL = "W";
    public static final String SERVICE_ALIAS_LEASEDLINE_TEMP = "A";
    public static final String SERVICE_ALIAS_VIDEO_CONFERENCE = "V";
    public static final String SERVICE_ALIAS_GROUP = "G";
    public static final String SERVICE_ALIAS_NONE = "NS";
    public static final String SERVICE_ALIAS_CAMERA = "C";
    public static final Long SERVICE_MOBILE_ID_WEBSERVICE = 1L;
    public static final Long SERVICE_HOMEPHONE_ID_WEBSERVICE = 2L;
    public static final Long SERVICE_PSTN_ID_WEBSERVICE = 3L;
    public static final Long SERVICE_ADSL_ID_WEBSERVICE = 4L;
    public static final Long SERVICE_LEASELINE_ID_WEBSERVICE = 5L;
    public static final Long SERVICE_BUNDLE_ID_WEBSERVICE = 6L;
    public static final Long SERVICE_OFFICE_WAN_ID_WEBSERVICE = 11L;
    public static final Long SERVICE_WHITE_LL_ID_WEBSERVICE = 12L;
    public static final Long SERVICE_GROUP_ID_WEBSERVICE = 18L;
    public static final Long SERVICE_IPTV_ID_WEBSERVICE = 9L;
    public static final Long SERVICE_VC_ID_WEBSERVICE = 14L;
    public static final Long SERVICE_FTTH_ID_WEBSERVICE = 15L;
    public static final Long IS_NEW_SUB_TRUE = 1L;
    public static final Long IS_NEW_SUB_FALSE = 0L;
    public static final Long SUB_REQ_STATUS_NEW = 1L;               // sub_req tiep nhan yeu cau
    public static final Long SUB_REQ_STATUS_SIGN_CONTRACT = 2L;     // sub_req da ky hop dong
    public static final Long SUB_REQ_STATUS_ACTIVE = 3L;            // sub_req dau noi
    public static final Long SUB_REQ_STATUS_NOT_RESPONSE = 4L;      // sub_req khong dap ung duoc
    public static final Long SUB_REQ_STATUS_CANCEL = 5L;            // sub_req bi huy tiep nhan yeu cau
    public static final Long SUB_REQ_STATUS_SURVEY_SUCCESS = 6L;    // sub_req khao sat thanh cong
    public static final Long SUB_REQ_STATUS_SURVEY_FAIL = 7L;       // sub_req khao sat that bai
    public static final Long SUB_REQ_STATUS_DEPLOY_SUCCESS = 8L;    // sub_req trien khai thanh cong
    public static final Long SUB_STATUS_DEL = 0L;             //Đa xóa
    public static final Long SUB_STATUS_NORMAL_INACTIVE = 1L; //Chưa đấu nối
    public static final Long SUB_STATUS_NORMAL_ACTIVED = 2L;  //Đã đấu nối
    public static final Long SUB_STATUS_CANCEL = 3L;          //Đã hủy
    public static final Long SUB_STATUS_BLOCK_CCQ = 6L;          //Đã hủy
    public static final Long SUB_STATUS_WAIT_ACTIVE = 7L;          //Chờ kích hoạt
    public static final Long SUB_STATUS_NEW_REQUEST = 8L;          //Tiếp nhận yêu cầu
    public static final Long SUB_STATUS_TRANSFER_CONTRACT = 4L; //Đã hủy do chuyển nhượng hợp đồng
    //Tham so cho hop dong
    public static final Long CONTRACT_STATUS_DELETE = 0L;                 //Hop dong da bi xoa
    public static final Long CONTRACT_STATUS_INACTIVE = 1L;               //Hop dong moi dang ky nhung chua kich hoat
    public static final Long CONTRACT_STATUS_ACTIVE = 2L;                 //Hop dong da kich hoat, dang co hieu luc
    public static final Long CONTRACT_STATUS_CANCEL = 3L;                 //Hop dong da bi huy
    public static final Long CONTRACT_STATUS_END = 4L;                    //Hop dong da cham dut
    public static final int CONTRACT_CONTRACT_NO_LENGTH = 10;            //Do dai ma hop dong
    public static final Long SUB_REQ_STATUS_DEPLOY_FAIL = 9L;
    public static final Long STATUS_GROUP_REQUEST = 7L;
    public static final Long STATUS_CUST_REQUEST_NEW = 1L;            // Yeu cau moi tiep nhan
    public static final Long STATUS_CUST_REQUEST_RESPONSE = 2L;       // Yeu cau da ky hop dong
    public static final Long STATUS_CUST_REQUEST_CLOSE = 3L;          // Yeu cau da duoc dau noi
    public static final Long STATUS_CUST_REQUEST_CANCEL = 4L;          // Yeu cau da bi huy
    public static final Long STATUS_CUST_REQUEST_NOT_RESPONSE = 5L;
    public static final String REASON_TYPE_REG_TYPE_NEW = "28";  // "28"
    public static final String ACTION_SUBSCRIBER_ACTIVE_NEW = "00";  //Thue bao hoa mang
    public static final String ACTION_CONTRACT_TERMINATE = "03";  //Tac dong cham dut hop dong
    public static final String ACTION_SUBSCRIBER_CHANGE_INFO = "04";  // Thay doi thong tin
    public static final String ACTION_SUBSCRIBER_CHANGE_SIM = "11";   //Doi SIM
    public static final String ACTION_SUBSCRIBER_CHANGE_IDNO = "151";   //Doi IDNO
    public static final String ACTION_SUBSCRIBER_CHANGE_ISDN = "12";  //Doi ISDN
    // --- Them moi ---
    public static final String ACTION_SUBSCRIBER_INPUT_INFO = "01";  //Nhap thue bao
    public static final String ACTION_SUBSCRIBER_SPLIT = "02";  //Tach thue bao
    public static final String ACTION_SERVICE_CHANGE = "05";  //Thay doi dich vu
    public static final String ACTION_SUBCRIBER_BLOCK_1_WAY = "06";  //Chan 1 chieu
    public static final String ACTION_SUBCRIBER_BLOCK_2_WAYS = "07";  //Chan 2 chieu
    public static final String ACTION_SUBCRIBER_ACTIVE_1_WAY = "08";  //Mo 1 chieu
    public static final String ACTION_SUBCRIBER_ACTIVE_2_WAYS = "09";  //Mo 2 chieu
    public static final String ACTION_MESSAGE = "10";  //Nhan tin
    public static final String ACTION_SERVICE_FAX_DATA_CONNECT = "13";  //Dau dich vu Fax, Data
    public static final String ACTION_SERVICE_FAX_DATA_CANCEL = "14";  //Huy dich vu Fax, Data
    public static final String ACTION_SUBCRIBER_CHANGE_REPRESENT = "15";  //Doi thue bao dai dien
    public static final String ACTION_CONTRACT_CHANGE_NAME = "16";  //Lam HD doi ten
    public static final String ACTION_UNBARED = "17";  //Unbared
    public static final String ACTION_ADD_DEPOSIT = "18";  //Bo sung tien dat coc
    public static final String ACTION_SMS_BLOCK = "19";  //Chan SMS
    public static final String ACTION_SMS_ACTIVE = "20";  //Mo SMS
    public static final String ACTION_CCQ_SUBCRIBER_NEW = "21";  //Dau moi cho thue bao CCQ
    public static final String ACTION_SUBCRIBER_RECONNECT = "22";  //Dau lai (F1) cho thue bao
    public static final String ACTION_DEBT_SPLIT = "23";  //Tach no
    public static final String ACTION_DEBT_TRANSFER = "24";  //Chuyen no
    public static final String ACTION_MOBI_CHANGE = "25";  //Cap nhat cat chuyen MOBI
    public static final String ACTION_BAIC_NOT_ACTIVE = "26";  //BAIC(NOT ACTIVE)
    public static final String ACTION_BAIC_ACTIVE_OP = "27";  //BAIC(ACTIVE OP)
    public static final String ACTION_BAOC_NOT_ACTIVE = "28";  //BAOC(NOT ACTIVE)
    public static final String ACTION_BAOC_ACTIVE_OP = "29";  //BAOC(ACTIVE OP)
    public static final String ACTION_REQUESTS_FROM_SUPPIER = "31";  //Cac yeu cau tu phia nha cung cap
    public static final String ACTION_DATA_SERVICE_CONNECT = "32";  //Dau dich vu DATA
    public static final String ACTION_DATA_SERVICE_CANCEL = "33";  //Huy dich vu DATA
    public static final String ACTION_HLR_RESET = "34";  //Reset HLR
    public static final String ACTION_PACKAGE_CHANGE = "35";  //Thay doi goi cuoc
    public static final String ACTION_COST_REPORT_COLLECT = "36";  //Tong hop thong bao cuoc
    public static final String ACTION_REPAIR_WRONG_SUBSCRIBER = "41";  //Dau noi sua sai cho thue bao
    public static final String ACTION_VIP_REASON_CHANGE_STATUS = "54";  //Ly do chuyen trang thai the VIP
    public static final String ACTION_SUBSCRIBER_RESET_ZONE = "55";   //Reset  zone
    public static final String ACTION_SUBSCRIBER_CHANGE_ZONE = "58";   //Change zone (hien tai dang dung chung gia tri voi Reset Zone)
    public static final String ACTION_SMS_FROM_WEB = "61";   //Nhan tin tu web
    public static final String ACTION_FAMILY_NUMBER_CHANGE = "62";   //Thay doi so co dinh Family
    public static final String ACTION_FAMILY_NUMBER_INPUT = "63";   //Nhap so co dinh Family
    public static final String ACTION_SERVICE_BLOCK_OR_ACTIVE = "64";   //Chan mo dich vu
    public static final String ACTION_DISTURB_BY_CUST_BLOCK = "70";   //Chan do khach hang quay roi CallCenter
    public static final String ACTION_DISTURB_BY_CUST_ACTIVE = "71";   //Mo do khach hang quay roi CallCenter
    public static final String ACTION_THC = "36";   //Tong hop cuoc
    // added by Tuantm - 07/01/2010
    public static final String ACTION_REPAIR_SUBCRIBER_NOTCONNECT = "150";      // Sua sai notConnect
    //New
    public static final String ACTION_TEAM_RETURN_TASK = "80";         //to tra viec
    public static final String ACTION_TASK_ASSIGN_STAFF_AGAIN = "81";         //giao lai viec cho nhan vien
    public static final String ACTION_TASK_NOT_COMPLETED = "82";         //Cong viec khong thuc hien duoc
    public static final String ACTION_CONTRACT_CANCEL = "90";         //huy hop dong
    public static final String ACTION_CONTRACT_UPDATE = "91";         //ham thuc hien cap nhat thong tin hop dong
    public static final String ACTION_CONTRACT_TRANSFER = "92";         //ham thuc hien chuyen nhuong hop dong
    public static final String ACTION_CONTRACT_MERGE = "93";       //ham thuc hien gop hop dong
    public static final String ACTION_CONTRACT_SPLIT = "94";         //ham thuc hien tách hợp đồng
    public static final String ACTION_CONTRACT_CANCEL_APP_INX = "95";         //ham thuc hien huy phu luc hop dong
    public static final String ACTION_CONTRACT_NEW = "96";            //Ham thuc hien them moi thong tin hop dong
    public static final String ACTION_CANCEL_REQUEST = "97";          //Hàm thực hiện hủy yêu cầu
    public static final String ACTION_CONTRACT_ADD_APPENDIX = "98";         //ham thuc hien them phu luc hop dong
    public static final String ACTION_CONTRACT_CHANGE_CUST_ID = "803";      //ham sua sai cmt tra sau R1260 toanpd1
    //New nhung chua dua vao trong DB
    public static final String ACTION_SUBSCRIBER_DELETE = "03";       //Ham thuc hien huy thue bao
    public static final String ACTION_SUBSCRIBER_BLOCK = "101";        //Ham thuc hien chan mo thue bao
    public static final String ACTION_SUBSCRIBER_CHANGE_VAS = "102";   //Ham thuc hien thay đổi GTGT
    public static final String ACTION_SUBSCRIBER_CHANGE_RES = "103";   //Hàm thực hiện thay đổi tai nguyen cua thue bao
    public static final String ACTION_SUBSCRIBER_CHANGE_PASS = "105";  //Hàm thực hiện thay đổi mật khẩu của thuê bao
    public static final String ACTION_SUBSCRIBER_CHANGE_ACC = "106";   //Hàm thực hiện thay đổi account của thuê bao
    public static final String ACTION_SUBSCRIBER_CHANGE_PRO = "107";   //Hàm thực hiện thay đổi product của thuê bao
    public static final String ACTION_SUBSCRIBER_UPDATE_INTERNET_VPN = "108";   //Hàm thực hiện thay đổi product của thuê bao
//    Phan ap_domain moi A- P
    public static final String ACTION_SUB_PSTN_FAMILY_NUMBER = "232";     //Hàm thực hiện familynumber pstn
    public static final String ACTION_SUB_CHANGE_PRODUCT_AP = "107";     //Hàm thực hiện thay đổi sản phẩm
    public static final String ACTION_SUB_CHANGE_EQUIPMENT = "148";     //Hàm thực hiện thay đổi Thiet bi
    public static final String ACTION_SUB_BLOCK_1_WAY_ADSL = "506";     //Khoa Account do KHYC
    public static final String ACTION_SUB_ACTIVE_1_WAY_ADSL = "508";     //Mo Account do KHYC
    public static final String ACTION_SUB_CHANGE_INS_ADDR = "513";     //Hàm thực hiện thay đổi địa chỉ lắp đặt của thuê bao
    public static final String ACTION_SUB_RESET_PORT = "514";    //Hàm thực hiện thay đổi thông tin port
    public static final String ACTION_SUB_UPDATE_PORT = "515";    //Hàm thực hiện thay đổi thông tin port
    public static final String ACTION_SUB_RESET_PASS_ACCOUNT_EMAIL = "518";    //Hàm thực hiện tạm ngưng account
    public static final String ACTION_SUB_RESET_MAC = "149";    //Hàm thực hiện reset MAC
    public static final String ACTION_SUB_OPEN_ADSL = "520";    //Hàm thực hiện khôi phục Account
    public static final String ACTION_SUB_BLOCK_ADSL = "521";    //Hàm thực hiện tạm ngưng account
    public static final String ACTION_SUB_DISCOUNT = "530";  //Chiet khau
    public static final String ACTION_SUB_CHANGE_STATIC_IP = "531";    //Hàm thực hiện thêm ip tính
    public static final String ACTION_SUB_UPDATE_STATIC_IP = "532";    //Hàm thực hiện thay đổi thông tin IP
    public static final String ACTION_SUB_UPDATE_EMAIL = "534";    //Hàm thực hiện thay đổi thông tin email
    public static final String ACTION_SUB_UPDATE_PROMOTION = "537";    //Hàm thực hiện thay đổi thông tin KM
    public static final String ACTION_TASK_DEPLOY = "523";    //Cập nhật triển khai
    public static final String ACTION_TASK_DElIVERY = "526";    //Nghiệm thu
    public static final String ACTION_TASK_REVOKE_FOR_SHOP = "527";    //Thu hồi cho tổ
    public static final String ACTION_TASK_REVOKE_FOR_STAFF = "528";    //Thu hồi cho nhân viên
    public static final String ACTION_TASK_ASSIGN_TO_STAFF = "600";    //Giao viec cho nhân viên (AnNP thêm vì fpt ko có)
    public static final String ACTION_TASK_ASSIGN_TO_SHOP = "601";    //Giao viec cho tổ (AnNP thêm vì fpt ko có)
    public static final String ACTION_TASK_SYSTEM_ERROR = "708";      //Công việc khiếu nại do lỗi hệ thống
    public static final String ACTION_TASK_QDKC = "709";            //Công việc quay đầu kết cuối
    public static final String ACTION_SUB_REPAIR_ITEM = "606";    //sửa sai vật tư
    public static final String ACTION_SUB_CHANGE_ISDN_AFTER_CHANGE_DEPLOY_ADDRESS = "652";
    public static final String ACTION_SUB_REPAIR_CHANGE_PRODUCT = "517";    //sửa sai đổi gói cước
    // remark correct customer action
    public static final String ACTION_REMARK_CORRECT_CUSTOMER = "99";

    /*V-Tracking*/
    public static final String ACTION_SUB_VTRACKING_ACCEPTANCE = "810";    //Nghiem thu V-Tracking
    /*Leasedline*/
    public static final String ACTION_SUB_CHANGE_PRODUCT_LL = "701";     //tac dong TNYC thay doi kenh
    public static final String ACTION_SUB_UPDATE_PRODUCT_LL = "702";     //Cap nhat interface
    public static final String ACTION_SUB_CHANGE_INTERFACE = "703";     //Them moi interface
    public static final String ACTION_SUB_CHANGE_SERVICE = "704";     //Thay doi dich vu GTGT Leasedline
    // ACTION - GROUP
    public static final String ACTION_CHANGE_MAIN_FAMILY_GROUP = "129"; // Thay doi chu nhom corporate
    public static final String ACTION_DESTROY_MEMBER_CORPORATE_GROUP = "130"; // Tach thanh vien khoi nhom corporate
    public static final String ACTION_DESTROY_CORPORATE_GROUP = "131"; // Huy nhom corporate
    public static final String ACTION_ADD_MEMBER_CORPORATE_GROUP = "133"; // Them thanh vien vao nhom corporate
    public static final String ACTION_CHANGE_MAIN_CORPORATE_GROUP = "132"; // Thay doi chu nhom corporate
    public static final String ACTION_EDIT_MEMBER_CORPORATE_GROUP = "134"; // Thay doi thanh vien corporate
    public static final String ACTION_DESTROY_FAMILY_GROUP = "135"; // Huy nhom family
    public static final String ACTION_DESTROY_MEMBER_FAMILY_GROUP = "136"; // Tach thanh vien khoi nhom family
    public static final String ACTION_ADD_MEMBER_FAMILY_GROUP = "137"; // Them thanh vien vao nhom family
    public static final String ACTION_EIDT_ISDN_DISCOUNTED_FAMILY_GROUP = "62"; // Thay doi so co dinh family
    public static final String ACTION_ACTIVE_GROUP_CORPORATE = "138";   // Tao nhom Corporate
    public static final String ACTION_ACTIVE_GROUP_FAMILY = "141";   // Tao nhom Corporate
    public static final String ACTION_SUBSCRIBER_CHANGE_REG_TYPE = "04"; //Hàm thực hiện thay đổi hình thức hoà mạng của thuê bao
    public static final String ACTION_RESET_RESOURCE_KPI = "155"; //Hàm thực hiện reset tai nguyen cho KPI
    public static final String SEARCH_ALL = "13579";
    public static final String ACTION_FOR_REASON = "19";
    public static final String ACTION_FOR_AUDIT = "20";
    public static final String PRODUCT_GROUP_FTTH = "FTTH";
    //Cong nghe
    public static final String IN_FRATYPE_CCN = "CCN";// cong nghe cap dong =>1
    public static final String IN_FRATYPE_FCN = "FCN";// cong nghe cap quang =>3
    public static final String IN_FRATYPE_CATV = "CATV";// cong nghe cap dong truc, truyen hinh cap =>2
    public static final String IN_FRATYPE_GPON = "GPON";// cong nghe bang rong co dinh gpon =>4
    public static final String IN_FRATYPE_CCN_CM = "1";// 
    public static final String IN_FRATYPE_FCN_CM = "3";// 
    public static final String IN_FRATYPE_CATV_CM = "2";//
    public static final String IN_FRATYPE_GPON_CM = "4";//
    /* start user manual config */
    public final static String USER_MANUAL_CONFIG = "USER_MANUAL_CONFIG";
    public final static String COUNTRY_CODE_PREFIX = "COUNTRY_CODE_PREFIX";
    public final static String MAX_LENGTH_SURVEY = "MAX_LENGTH_SURVEY";
    // gen account auto
    public final static String PRODUCT_GROUP_ADSL = "ADSL";
    public final static String PRODUCT_GROUP_LL = "LL";
    public final static String LENG_ACC_AUTO_INCREASE_LIST_NUMBER = "LEN_ACC_AUTO_INCREASE_LIST_NUM";
    public final static String AUTO_ACCOUNT_SEQ = "AUTO_ACCOUNT_SEQ";
    public final static Long OFFER_REQUEST_NEW = 1L;             // Tiep nhan yeu cau
    public final static Long OFFER_REQUEST_SIGN_CONTRACT = 2L;   // Ky hop dong
    public final static Long OFFER_REQUEST_ACTIVE = 3L;          // Dau noi
    public final static Long OFFER_REQUEST_CANCEL = 4L;          // Huy tiep nhan yeu cau
    public final static Long OFFER_REQUEST_SURVEY_FAIL = 5L;     // Khao sat that bai
    public final static Long OFFER_REQUEST_SURVEY_SUCCESS = 6L;  // Khao sat thanh cong
    //TuanPV -- Các trạng thái chặn mở thuê bao
    public static final String SUB_ACT_STATUS_BLOCK_OUT_GOING_CALL_BY_CUST = "100";
    public static final String SUB_ACT_STATUS_BLOCK_TWO_WAY_BY_CUST = "200";
    public static final String SUB_ACT_STATUS_UN_BLOCK_TWO_WAY_BY_CUST = "000";
    public static final Double DEPOSIT_DEFAULT = 0D;
    public static final Long SUB_QUOTA_DEFAULT = 0L;
    public static final String AP_PARAM_LIMIT_DATE = "LIMIT_DATE";
    public static final String AP_PARAM_LIMIT_DATE_ADSL = "LIMIT_DATE_ADSL";
    public static final String AP_PARAM_LIMIT_DATE_PSTN = "LIMIT_DATE_PSTN";
    public static final String AP_PARAM_LIMIT_DATE_FTTH = "LIMIT_DATE_FTTH";
    public static final String AP_PARAM_LIMIT_DATE_LL = "LIMIT_DATE_LL";
    public static final Long STATUS_USE = 1L;
    public static final Long STATUS_LOGED = 1L;
    public static final Long END_LOGED = 0L;
    public static String APPARAM_SPECIAL_REASON_TYPE = "SPECIAL_REASON_CODE";
    public static String APPARAM_SPECIAL_REASON_CODE = "SPECIAL_REASON_CODE";
    //End Thangpn
    public static final String AP_DOMAIN_TYPE_DEPOSIT_TYPE_VALUE = "305";
    public static final String AP_DOMAIN_TYPE_PAY_METHOD = "303";
    public static final String AP_DOMAIN_TYPE_PERSONAL_DOC = "301";
    public static final String IS_REQUIRED = "1";
    public static final String AP_DOMAIN_TYPE_PRINT_METHOD = "302";
    public static final String AP_DOMAIN_TYPE_SERVICE_TYPE = "300";
    public static final String AP_DOMAIN_TYPE_CONTRACT_TYPE = "29";
    public static final String AP_DOMAIN_TYPE_LINE_TYPE = "LINE_TYPE";
    public static final String AP_DOMAIN_TYPE_LINE_TYPE_LL = "LINE_TYPE_LL";
    public static final String AP_DOMAIN_TYPE_ACTION_DOC = "20";
    public static final String AP_DOMAIN_TYPE_DISCOUNT_TYPE = "DISCOUNT_TYPE";
    public static final String AP_DOMAIN_TYPE_DISCOUNT_TYPE_SUB_OW = "DISCOUNT_TYPE_SUB_OW";
    // madatory transaction
    public static final String MANDATORY_TRANSACTION_ACTION = "MANDATORY_TRANSACTION_ACTION";
    //trungtd: update 11052011: Bo sung them type: Hinh thuc phat thong bao cuoc
    public static final String AP_DOMAIN_TYPE_NOTICE_CHARGE_TYPE = "308";
    public static final String PAY_METHOD_CODE_ORDER_TO_RECEIPT = "02";// Hình thức thanh toán Ủy nhiệm thu
    public static final String PAY_METHOD_CODE_ORDER_TO_PAY = "03";// Hình thức thanh toán Ủy nhiệm chi
    public static final String PAY_METHOD_CODE_CARD_TO_PAY = "05";// Hình thức thanh toán Bằng thẻ cào
    public static final String RECEIVE_INVOICE = "1";// Co nhan hoa don cuoc
    public static final String NOT_RECEIVE_INVOICE = "2";// Khong nhan hoa don cuoc
    public static final String LENG_CONTRACT_NO_INCREASE_LIST_NUMBER = "LEN_CON_NO_INCREASE_LIST_NUM";
    public static final String CONTRACT_NO_SEQ = "CONTRACT_NO_SEQ";
    public static final String NOTICE_CHARGE_BY_EMAIL = "1";
    public static final String NOTICE_CHARGE_BY_SMS = "2";
    public static final String NOTICE_CHARGE_BY_SMS_EMAIL = "5";
    public static final String CONTRACT_TYPE_NORMAL = "0";
    public static final Long CONTRACT_BILL_CYCLE_DEFAULT = 1L;
    public static final Long CONTRACT_BANK_USE_STATUS = 1L;
    public static final String CONTRACT_ID_SEQ = "CONTRACT_ID_SEQ";
    public static final String CONTRACT_BANK_SEQ = "CONTRACT_BANK_SEQ";
    public static final String ACTION_AUDIT_SEQ = "ACTION_AUDIT_SEQ";
    public static final String GROUP_TYPE_CORPORATE = "CORPORATE";
    public static final String GROUP_TYPE_FAMILY = "FAMILY";
    public static final Long CONTRACT_OFFER_IS_BUNDLE = 1L;
    public static final String OFFER_SUB_ID_SEQ = "OFFER_SUB_ID_SEQ";
    public static final String CONTRACT_OFFER_ID_SEQ = "CONTRACT_OFFER_ID_SEQ";
    public static final Long CONTRACT_OFFER_INACTIVE = 0L;// contract_offer da duoc dau noi
    public static final Long CONTRACT_OFFER_ACTIVE = 1L;// contract_offer da duoc dau noi
    public static final String VPN_GROUP_SEQ = "VPN_GROUP_SEQ";
    public static final String DEPLOY_REQ_TYPE_NEW_CONTRACT = "0";
    public static final Long DEPLOY_REQ_STATUS_REGIS_CONTRACT = 5L;
    public static final Long DEPLOY_REQ_STATUS_UN_SATISFY = 8L;
    public static final String ACTION_AUDIT_PK_TYPE_CUSTOMER = "1";
    public static final String ACTION_AUDIT_PK_TYPE_CONTRACT = "2";
    public static final String ACTION_AUDIT_PK_TYPE_SUBSCRIBER = "3";
    public static final String WS_IP = "127.0.0.1";
    public static final Long STEP_ID_REG_ACTIVE = 5L;   // Dau noi thue bao
    public static final Long STEP_ID_REG_CONTRACT = 4L; // ky hop dong
    public static final Long STEP_ID_REG_DEPLOY = 3L;   // Trien khai
    public static final Long STEP_ID_REG_SURVEY = 2L;   // Khao sat
    public static final Long STATUS_NOT_USE = 0L;
    public static final String MAPPING_TEL_SERVICE = "MAPPING_TEL_SERVICE";
    public static final String MAPPING_BRCD_SERVICE = "MAPPING_BRCD_SERVICE";
    public static final String MAX_LENGTH_RADIUS_ADSL = "MAX_LENGTH_RADIUS_ADSL";
    public static final String MAX_LENGTH_RADIUS_FTTH = "MAX_LENGTH_RADIUS_FTTH";
    public static final String MAX_LENGTH_RADIUS_LL = "MAX_LENGTH_RADIUS_LL";
    public static final String MAX_LENGTH_CABLE_FTTH = "MAX_LENGTH_CABLE_FTTH";
    public static final String MAX_LENGTH_CABLE_LL = "MAX_LENGTH_CABLE_LL";
    public static final String MAX_LENGTH_CABLE_ADSL = "MAX_LENGTH_CABLE_ADSL";
    public static final Long MOBILE_TEL_SERVICE = 1L;
    public static final Long HOMEPHONE_TEL_SERVICE = 2L;
    public static final Long IS_NOT_INFO_COMPLETED = 0L;
    public static final Long SUB_REQ_IS_ACTIVE_ALL_OFFER_REQUEST = 0L;
    public static final Long OFFER_SUB_STATUS_USE = 1L;
    public static final Long CONTRACT_OFFER_STATUS_USE = 1L;
    public static final Long ALL_VALUE = -1L;
    public static final Long IS_SMART = 1L;
    public static final Long IS_SATISFY = 1L;
    public static final Long DEPLOY_REQ_STATUS_ACTIVE_SUB = 9L;
    public static final String INVOICE_LIST_STAFF_SEQ = "INVOICE_LIST_STAFF_SEQ";
    public static final String SUB_DEPOSIT_TYPE_CODE_ADSL = "HMSDA";
    public static final String IM_DEPOSIT_TYPE_PAY = "1";
    public static final Long REASON_TYPE_29_KHYC = 1245L;
    public static final String CORRECT_CUSTOMER = "1";
    public static final String IS_NOT_CORRECT_CUSTOMER = "0";
    public static final Long SERVICE_ADSL_ID = 1003L;
    public static final Long SERVICE_FTTH_ID = 1015L;
    public static final Long SERVICE_LEASEDLINE_ID = 1004L;
    public static final String SEQ_ACTION_DETAIL = "SEQ_ACTION_DETAIL";
    public static final String SEQ_ACTION_AUDIT = "ACTION_AUDIT_SEQ";
    public static final String TOKEN_SEQ = "TOKEN_SEQ";
    public static final String TRANS_LOG_SEQ = "TRANS_LOG_SEQ";
    public static final String EMPTY_STRING = "";
    public static final Long SUB_DEPLOYMENT_STAGE_ID_WORKING = 2L;// Đang đi bắn tín hiệu
    public static final String SUB_IP_AUTO_ASSIGN = "0";
    public static final String SUB_IP_FRAMED_IP_NETMASK = "Framed-IP";
    public static final Long SUB_IP_STATUS_USE = 1L;
    public static final Long SUB_IP_STATUS_RATING = 0L;
    public static final Long SUB_IP_STATUS_PAUSE = 2L;
    public static final Long SUB_PRODUCT_ADSL_LL_STATUS_USE = 1L;
    public static final String SUB_PRODUCT_ADSL_LL_ID_SEQ = "SUB_PRODUCT_ADSL_LL_ID_SEQ";
    public static final String SUB_ACT_STATUS_BLOCK_ONE_WAY_BY_CUST_REQ = "100";
    public static final String SUB_ACT_STATUS_NORMAL = "000";
    public static final Long TRANS_OF_CONTRACT = 1L;                            //Giao dich lien quan den hop dong
    public static final Long TRANS_OF_SUBSCRIBER = 2L;                          //Giao dich lien quan den thue bao
    public static final String TASK_PROGRESS_WORKING = "1";
    public static final String TASK_SOURCE_TYPE = "0";
    public static final String MODEL_TYPE_1 = "88";
    public static final String MODEL_TYPE_2 = "89";
    public static final String TASK_JOB_CODE_DEFAULT = "TKHDM";
    public static final String TASK_REQ_TYPE_NEW_CONNECTION = "0";// dau  moi
    public static final String TASK_MANAGEMENT_SEQ = "TASK_MANAGEMENT_SEQ";
    public static final String TASK_REQ_TYPE_CHANGE_DEPLOY_ADDRESS = "1";// thay doi dia chi lap dat
    public static final String TASK_REQ_TYPE_RECOVER_SUBSCRIBER = "7";
    public static final Long JOB_STAGE_NEW_CONTRACT = 1L;
    public static final String TASK_PROGRESS_START = "0";
    public static final String TASK_SHOP_MANAGEMENT_SEQ = "TASK_SHOP_MANAGEMENT_SEQ";
    public static final String TASK_STAFF_MANAGEMENT_SEQ = "TASK_STAFF_MANAGEMENT_SEQ";
    public static final String SMS_ASSIGN_TASK_APPARAM_TYPE = "ASSIGN_TASK_SMS";
    public static final String SMS_ASSIGN_TASK_APPARAM_CODE = "ASSIGN_TASK_SMS";
    public static final String SMS_PROCESS_STATUS_NEW = "0";
    public static final Long SMS_MAX_SEND_TIME = 2L;
    public static final String APP_CODE_ASSIGN_TASK = "ASSIGN_TASK";
    public static final Long QUANTITY_DEFAULT_FOR_SALE = 1L;
    public static final List EMPTY_ARRAY_LIST = new ArrayList();
    public static final Long SOURCE_ID_END = 9L;// Điểm cuối
    public static final String MODIFICATION_TYPE_NOTCONNECT_SUBSCRIBER = "403";// Sua sai notconnect
    //<editor-fold defaultstate="collapsed" desc="ViettelService">
    public static final String REQUEST = "request";
    public static final String RESPONSE_CODE = "responseCode";
    public static final String RESPONSE = "response";
    public static final String EXCEPTION = "exception";
    public static final String ACTION_LOG_PR_ID = "actionLogPrId";
    public static final String RESPONSE_INVALID = "-1";
    public static final String RESPONSE_SUCCESS = "0";
    public static final String RESPONSE_FAIL = "1";
    public static final String RESPONSE_EXCEPTION = "2";
    //</editor-fold>
    public static final String ACTION_LOG_PR_SEQ = "ACTION_LOG_PR_SEQ";
    public static final String MODIFICATION_TYPE_REGISTER_NEW = "0";// Đăng ký mới
    public static final String PORT_FORMAT_FAKE = "PORT_FORMAT_FAKE";
    public static final String PEAK_RATE_CONSTANT = "PEAK_RATE_CONSTANT";
    public static final String VAS_IP_ADSL_500K_DEFAULT_VASIP1 = "VASIP1";
    public static final Long EXCHANGE_TYPE_VAS_IP_ADSL = 132L;
    public static final String FELLOW_TYPE_ATTRIBUTE_VALUE_DEFAULT_VASIP1 = "12";
    public static final String FELLOW_TYPE_ATTRIBUTE = "FELLOW_TYPE";
    public static final String PROMOTION_ONLINE = "1";
    public static final String PROMOTION_OFFLINE = "0";
    public static final Long PROMOTION_TYPE_EFFECT = 0L;
    public static final Long PROMOTION_TYPE_EXPRIRE = 1L;
    public static final String IS_MANUAL_ONLY = "Y";
    public static final String SUB_DEPLOYMENT_SEQ = "SUB_DEPLOYMENT_SEQ";
    public static final String SEQ_ISDN_SEND_SMS = "SEQ_ISDN_SEND_SMS";
    public static final int DEADLINE_LEASELINE_INTERVAL = 5;
    public static final int DEADLINE_ADSL_INTERVAL = 5;
    public static final int DEADLINE_PSTN_INTERVAL = 5;
    public static final int DEADLINE_HOMEPHONE_INTERVAL = 5;
    public static final int DEADLINE_MOBILE_INTERVAL = 5;
    public static final String TASK_SEQ = "TASK_SEQ";
    public static final String SUB_DEPOSIT_ADSL_LL_SEQ = "SUB_DEPOSIT_ADSL_LL_SEQ";
    public static final String ADD_CUST_SEQ = "ADD_CUST_SEQ";
    public static final String SEQ_CUSTOMER = "SEQ_CUSTOMER";
    public static final String SEQ_ACTION_AUDIT_PRE = "SEQ_ACTION_AUDIT";
    public static final String ACTION_AUDIT_FILE_SEQ = "ACTION_AUDIT_FILE_SEQ";
    public static final String CUST_REQUEST_ID_SEQ = "CUST_REQUEST_ID_SEQ";
    public static final String SUB_ID_SEQ = "SUB_ID_SEQ";
    public static final String SUB_REQ_SEQ = "SUB_REQ_SEQ";
    public static final String REQ_OFFER_ID_SEQ = "REQ_OFFER_ID_SEQ";
    public static final String SUB_STOCK_MODEL_REL_SEQ = "SUB_STOCK_MODEL_REL_SEQ";
    public static final String SEARCH_SUB_REQ_STATUS = "SEARCH_SUB_REQ_STATUS";
    public static final String USER_NAME = "USER_NAME";
    public static final String PASS_WORD = "PASS_WORD";
    public static final String SESSION_ID = "SESSION_ID";
    public static final String NO = "no";
    public static final String TIME_OUT = "TIME_OUT";
    public static final String TIME_LIFE = "TIME_LIFE";
    public static final String TOKEN_TIME_OUT = "TOKEN_TIME_OUT";
    public static final Long DEFAULT_TIME_OUT = 30L;
    public static final Long DEFAULT_LIFE_TIME = 60L;
    public static final String MODIFICATION_TYPE_CANCLE = "1";
    public static final String IOEXCEPTION_RESPONSE_CODE = "-5555";
    public static final long SLOW_DURATION = 30000;
    public static final String MAP_CM_PERMISSION = "MAP_CM_PERMISSION";
    public static final String BUS_TYPE_PERSONAL_DOMESTIC = "0";
    public static final String BUS_TYPE_BUSINESS = "1";
    public static final String BUS_TYPE_FOREIGN = "2";
    public static final String MALE = "1";
    public static final String SUCCESS = "SUCCESS";
    public static final String TASK_DEPLOY = "0";
    public static final String TASK_TREAT = "1";
    public static final String TASK_SURVEY = "2";
    public static final String STAFF_START = "0";
    public static final String TEAM_START = "1";
    public static final String TASK_ASSIGN_SYSTEM_OLD = "-1";
    public static final String NOT_FOUND_TASK = "-2";
    public static final String ERROR_PROCESSING = "-3";
    public static final String NOT_FOUND_SUB_FOR_TASK = "-7";
    public static final String NOT_FOUND_TEAM_TO_ASSIGN_TASK = "-10";
    public static final String NOT_FOUND_SHOP_ASSIGNED_TASK = "-4";
    public static final String NOT_FOUND_INFOR_SHOP_ASSIGNED = "-5";
    public static final String NOT_FOUND_STAFF_ASSIGNED_TASK = "-6";
    public static final String NOT_FOUND_CUST_FOR_TASK = "-8";
    public static final String NOT_FOUND_STAFF_TO_ASSIGN = "-11";
    public static final String SERVICE_A_F_L = "4";
    public static final String SERVICE_P = "3";
    public static final String SERVICE_DIGITAL = "55";
    public static final String STAFF_PERFORM = "1";
    public static final String REQUEST_TO_CHANGE_STAFF = "2";
    public static final String FINISH_TASK = "3";
    public static final String REQUEST_TO_CHANGE_TEAM = "4";
    public static final String START = "0";
    public static final String PERFORMING = "1";
    public static final String FINISH = "2";
    public static final String CANCELED = "3";
    public static final String DEPLOY_ON_SMART_PHONE = "9";
    // is charging 
    public static final String REGISTER_INFO_ACTION_CODE = "04";
    public static final Long REASON_REGISTER_INFO = 2746L;
    public static final Long SHOP_DIRECT = 1L;
    public static final Long SHOP_TYPE_AGENT = 4L; //Cua hang la dai ly
    public static final Long SHOP_TYPE_SHOWROOM = 3L; //Cua hang la dai ly
    public static final Long SHOP_TYPE_AGENT_DELEGATE = 5L; //Cua hang la dai ly ủy quyền
    public static final Long SHOP_TYPE_AGENT_XNK = 6L; //Cua hang la dai ly xuất nhập khẩu
    public static final Long SHOP_TYPE_AGENT_MB = 7L; //Cua hang la dai ly MB
    public static final String REASON_CODE_FOR_AGENT = "REASON_CODE_FOR_AGENT"; //Danh mục các hình thức hòa mạng đại lý được phép đấu nối
    public static final String REASON_CODE_FOR_AGENT_XNK = "REASON_CODE_FOR_AGENT_XNK"; //Danh mục các hình thức hòa mạng đại lý XNK được phép đấu nối
    public static final String REASON_CODE_FOR_AGENT_DELEGATE = "REASON_CODE_FOR_AGENT_DELEGATE"; //Danh mục các hình thức hòa mạng đại lý ủy quyền được phép đấu nối
    public static final Long AP_PARAM_STATUS_USED = 1L;
    public static final Long AP_PARAM_STATUS_NOT_USED = 0L;
    public static String APPARAM_REASON_PERMIT_BY_USER = "REASON_PERMIT_BY_USER";
    public static String APPARAM_REASON_NOT_ALLOW_VIEW = "REASON_NOT_ALLOW_VIEW";
    public static final String PRE_PAID = "1";
    public static final String POST_PAID = "2";
    public static final String OUTPUT_FILE_POLICY = "OUTPUT_FILE_POLICY";
    public static final String GET_FILE_DIRECTORY = "GET_FILE_DIRECTORY";
    public static final String AGENT_ACCOUNT_TYPE_CTV_COLLECTION = "10";
    /*depositt_type 10 - withdraw for pay advance*/
    public static final Long REASON_TYPE_PAY_ADVANCE = 10l;
    public static final String TYPE_RECEIVE_DEPOSIT = "1";
    public static final String REASON_TYPE_RECEIVE_DEPOSIT = "RECEIVE_DEPOSIT_SHOP";
    public static final String REASON_TYPE_PAY_DEPOSIT = "PAY_DEPOSIT_SHOP";
    public static Long RECEIVE_TYPE_TDCCH = 4L; //thu dat coc tai cua hang
    public static final String STATUS_PAPER = "1"; //Da thu coc
    public static final String STATUS_NONE_APPROVE = "1";
    public static final String RECEIVE_PAY_METHOD = "1";//Tien mat
    /* duyetdk - start */
    public static final String REQUEST_UPDATE_LOCATION_SEQ = "REQUEST_UPDATE_LOCATION_SEQ";
    public static final String APPROVAL = "APPROVED";
    public static final String REJECT_OVER_30DAYS = "OVER 30 DAYS";
    public static final Long WAITING = 0L;
    public static final Long APPROVED = 1L;
    public static final Long REJECT = 2L;
    public static final Long PENDING = 3L;
    public static final Double EARTH_RADIUS = 6371000D;
    public static final Double DISTANCE = 100D;
    public static final Long PAGE_SIZE = 10L;
    public static final String ADSL = "ADSL";
    public static final String ACTION_REJECT_REQUEST_UPDATE_CUSTOMER_LOCATION = "1000"; //Ham huy yeu cau thay doi toa do KH
    public static final String APPROVE_ALL = "2000";//Ham phe duyet tat ca yeu cau thay doi toa do KH
    public static final Long DURATION = 30L;
    public static final String FLAG_NEW_FLOW_UPDATE_LOCATION = "NEW_FLOW_UPDATE_LOCATION_MBCCS";
    public static final String ACTIVE_SUBSCRIBER_IMG = "ACTIVE_SUBSCRIBER_IMAGE";
    public static final String INFRA_IMG = "INFRASTRUCTURE_IMAGE";
    public static final String GOOD_IMG = "GOOD_IMG";
    public static final String UPDATE_INFRASTRUCTURE_SEQ = "UPDATE_INFRASTRUCTURE_SEQ";
    public static final String REASON_CONFIG_IMT = "REASON_CONFIG_IMT";
    public static final String SUGGEST_CONFIG_IMT = "SUGGEST_CONFIG_IMT";
    public static final String ACTION_REJECT_SUB_FIXBROADBAND_CONFIG = "109"; //Ham huy yeu cau cap nhat ha tang
    public static final String REQUEST_CONFIG = "10";
    public static final String SUB_FB_CONF_SEQ = "SUB_FB_CONF_SEQ";
    public static final String SUB_FB_CONF_LOG_SEQ = "SUB_FB_CONF_LOG_SEQ";
    public static final String UPDATE_STATUS_PENDING_TO_WAITING = "110"; //cap nhat trang thai cho phe duyet subFbConfig
    public static final String ACTION_AUDIT_PK_TYPE_SUB_FB_CONF = "5";
    public static final String TYPE_IMAGE_INFRASTRUCTURE = "TYPE_IMAGE_INFRASTRUCTURE";
    public static final Long OK = 1L;
    public static final Long NOT_OK = 0L;
    public static final String SALE_TRANS_TYPE = "SALE_TRANS_TYPE";
    public static final String URL_EMONEY_INVOICE = "https://10.79.94.207:8282/ePayTest/invoice/new";
    public static final String URL_EMONEY_USSD_PAY = "https://10.79.94.207:8282/ePayTest/payment/ussd-pay";
    public static final String POST_METHOD = "POST";
    public static final String SSL = "SSL";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String ACCEPT = "Accept";
    public static final String AUTHORIZATION = "Authorization";
    public static final String E_LANGUAGE = "en";
    public static final Integer TYPE_USSD = 2;
    public static final String CURRENCY_CODE_USD = "USD";
    public static final String CURRENCY_CODE_KHR = "KHR";
    public static final String CLEAR_DEBIT_DES = "Clear debit metfone staff";
    public static final Long SUCCESS_CODE = 0L;
    public static final Double RATE = 4000D;
    public static final String METHOD_EMONEY = "EMONEY";
    public static final String METHOD_BANK_RECEIPT = "BANK_RECEIPT";
    public static final String METHOD_RECEIVING_DEBT = "RECEIVING_DEBT";
    public static final String EMONEY_PUBLIC_KEY = "MF_DEBIT.pub";
    public static final String EMONEY_PRIVATE_KEY = "MF_DEBIT.pri";
    public static final String ERROR_CODE_00 = "00"; //sucess
    public static final String COMPLAINT_CODE = "COMPLAINT";
    //sequence COMPLAIN
    public static String SEQ_COMPLAIN = "SEQ_COMPLAIN";
    //sequence ASSIGN_MANAGEMENT
    public static String ASSIGN_MANAGEMENT_SEQ = "ASSIGN_MANAGEMENT_SEQ";
    //sequence COMP_PROCESS
    public static String COMP_PROCESS_SEQ = "COMP_PROCESS_SEQ";
    /* Trang thai Tiep nhan khieu nai
     * complain.status: 0: Tiep nhan; 1: Dang xu ly; 2: Da xu ly ; 3: Dong khieu nai; 4: Xoa
     */
    public static final String COMPLAIN_ACCEPT_STATUS = "0";
    public static final String COMPLAIN_PROCESSING_STATUS = "1";
    public static final String COMPLAIN_PROCESSED_STATUS = "2";
    public static final String COMPLAIN_CLOSE_STATUS = "3";
    public static final String COMPLAIN_DELETED_STATUS = "4";
    /**
     * Nguyen nhan tra lai
     */
    public static final String COMPLAIN_RETURN_REASON_1 = "Nội dung";
    public static final String COMPLAIN_RETURN_REASON_2 = "Nghiệp vụ";
    public static final String COMPLAIN_RETURN_REASON_3 = "Sai trường";
    /**
     * Trang thai xu ly khieu nai tai Phong ban comp_process.dep_status: 0: Chua
     * xu ly; 1: dang xu ly: 2: da xu ly xong
     *
     */
    public static final String COMPLAIN_PROCESS_DEPSTATUS_0 = "0"; // Chua xu ly
    public static final String COMPLAIN_PROCESS_DEPSTATUS_1 = "1"; // Dang xu ly
    public static final String COMPLAIN_PROCESS_DEPSTATUS_2 = "2"; // Da xu ly

    /*Hinh thuc giao viec ASSIGN_MANAGEMENT*/
    //Tiep nhan
    public static final Long ASSIGN_TYPE_ACCEPT = 1L;
    //Giao viec
    public static final Long ASSIGN_TYPE_ASISGNED = 2L;
    //Tu choi
    public static final Long ASSIGN_TYPE_CANCEL = 3L;
    //Xu ly xong
    public static final Long ASSIGN_TYPE_PROCESSED = 4L;
    //Phoi hop
    public static final Long ASSIGN_TYPE_COOR = 5L;
    /*Trang thai: ASSIGN_MANAGEMENT.ACTIVE: 0: Khong su dung; 1: Dang su dung*/
    public static final Long ASSIGN_MANAGEMENT_ACTIVE = 1L;
    public static final Long ASSIGN_MANAGEMENT_DEACTIVE = 0L;
    public static final String TASK_SOURCE_TYPE_CC = "1";
    public static final String JOB_TYPE_COMPLAINT = "COMPLAIN";
    public static final Long STAGE_REPAIR_PROBLEM = 5L;
    public static final String TASK_SHOP_PROGRESS_START = "0";
    public static final String PARAM_TYPE_GROUP_HIDE_CHANGESIM_MOBILE = "GROUP_HIDE_CHANGESIM_MOBILE";
    public static final String PARAM_TYPE_ACCOUNT_NORMAL_CHANGESIM_MOBILE = "ACCOUNT_CHANGESIM_MOBILE";
    public static final String BUS_TYPE = "INDN";
    public static final String NOTE_UPDATE_BY_MBCCS = "Register Information by mBCCS";
    public static final String CORRECT_CUS = "1";
    public static final long STATUS_USE_SUB_MB = 2;
    public static final String ACT_STATUS_NOT_ACTIVE = "03";
    public static final String SUBSCRIBER_FORM = "SUBSCRIBER_FORM";
    public static final String ID_CARD = "ID_CARD";
    public static final String CUST_IMG_TYPE = "CUST_IMG";
    public static final String TASK_PROGRESS_END = "2";
    public static final String TASK_PROGRESS_PROCESS = "1";
    public static final String u02 = "u02";
    public static final String u03 = "u03";
    public static Long CALL_CENTER_DEP_ID = 1363L;
    public static final String QUERY_LOG_SEQ = "QUERY_LOG_SEQ";
    public static final Long REMOVE = 0L;
    public static final Long CONNECTED = 1L;
    public static final Long NOT_CONNECT = 2L;
    public static final String POTENTIAL_CUST_SEQ = "POTENTIAL_CUST_SEQ";
    public static final String POTENTIAL_CUS_HIS_SEQ = "FBB_POTENTIAL_CUS_HIS_SEQ";
    /* duyetdk - end */
    //url config imt
    public static final String URL_CONFIG_IMT_AON = "URL_CONFIG_IMT_AON";
    public static final String URL_CONFIG_IMT_GPON = "URL_CONFIG_IMT_GPON";
    public static final String URL_IMT_SALARY = "URL_IMT_SALARY";
    public static final String URL_IMT_BANDWITH = "URL_IMT_BANDWITH";
    public static final String SERVICE_METHOD_POST = "POST";
    public static final String SERVICE_METHOD_GET = "GET";
    public static final String BOUNDARY_FORM_DATA = "mBCCS";
    //config khoang cach chup anh
    public static final String MAX_DISTANCE_CON_GPON = "MAX_DISTANCE_CON_GPON";
    public static final String MAX_DISTANCE_CON_AON = "MAX_DISTANCE_CON_AON";
    public static final String MAX_DISTANCE_IMAGE = "MAX_DISTANCE_IMAGE";
    public static final Long STOCK_ID_CABLE_1FO_INDEX = 1004921l;
    public static final Long STOCK_ID_CABLE_2FO_INDEX = 1003361l;
    public static final Long STOCK_ID_CABLE_2FO_INDEX1 = 6601l;
    public static final String CHANGE_ADSL_TO_GPON_REASON_CODE = "REASON_CODE";
    public static final String CHANGE_ADSL_TO_GPON_AP_PARAM_TYPE = "CHANGE_ADSL_TO_GPON";
    public static final String IMAGE_TYPE_LOCATION_CUSTOMER = "LOCATION_IMAGE_CUST"; //Ham huy yeu cau thay doi toa do KH
    public static final String PARAM_TYPE_CC_ACTION_DETAIL = "CC_ACTION_DETAIL";
    public static final String CC_COMPLAIN_HISTORY = "1";
    public static final String CC_BIILING_COLLECTION = "2";
    public static final String SIGN_CONTRACT_SUCCESSFUL_SMS_CODE = "SIGN_CONTRACT_SUCCESSFUL_SMS";
    public static final String SIGN_CONTRACT_SUCCESSFUL_SMS_TYPE = "SIGN_CONTRACT_SUCCESSFUL_SMS";
    public static final String CONFIG_IMT_REJECT = "CONFIG_IMT_REJECT";
    public static final String CONFIG_IMT_SUCCESS = "CONFIG_IMT_SUCCESS";
    public static final String NEW_REQUEST_CONFIG_IMT = "NEW_REQUEST_CONFIG_IMT";
    public static final String TYPE_IMAGE_INFRATRUCTURE = "TYPE_IMAGE_INFRATRUCTURE";
    public static final String TYPE_IMAGE_GOOD = "TYPE_IMAGE_GOOD";
    public static final String TYPE_IMAGE_SUB_ACTION = "TYPE_IMAGE_SUB_ACTION";
    public static final String TOKEN_WEB = "c1u1o1n1g143045ef95bb959ab2448f9072c086c90d01a4";
    public static final String SMS_ASSIGN_TASK_ANNOUNCE_TEAM_SMS = "ANNOUNCE_TEAM_SMS";
    public static final String INFRA_AON = "AON";
    public static final String INFRA_GPON = "GPON";
    public static final String REASON_TYPE_DEPOSIT = "DEPOSIT";
    public static final String REASON_CODE_DEPOSIT_POS = "DEPOSIT_POS";
    public static final String REASON_CODE_WD_POS_PAYMENT = "WD_POS_PAYMENT";
    public static final String NOTIFICATION_ACTION_TYPE_UPDATE_TASK = "UPDATE_TASK";
    public static final String NOTIFICATION_ACTION_TYPE_ASSIGN_TASK_FOR_TEAM = "ASSIGN_TASK_FOR_TEAM";
    public static final String NOTIFICATION_ACTION_TYPE_REQUEST_CONFIG = "REQUEST_CONFIG";
    public static final String PARAM_TYPE_STAFF_IMT = "STAFF_CONFIG_IMT";
    public static final String AP_PARAM_PARAM_TYPE_SMS = "SEND_SMS";
    public static final String AP_PARAM_PARAM_TYPE_NOTIFICATION = "SEND_NOTIFICATION";
    public static final String NEW_LINE = "newLinenewLine";
    public static final String PARAM_TYPE_SHOP_KEY_AES = "METFONE_KEY";
    public static final String KEY_AES = "012bhvhaivan0123";/*k thay doi*/

    public static final String VTC_PAY_TOPUP = "VTC_PAY_TOPUP";
    public static final String VTC_PAY_PINCODE = "VTC_PAY_PINCODE";
    public static final String VTC_PORTAL = "VTC_PORTAL";
    public static final String VTC_EXCHANGE = "VTC_EXCHANGE";
    public static final String VTC_DATA = "VTC_DATA";
    public static final String VTC_DATA_VAS = "VTC_DATA_VAS";
    public static final String PARAM_TYPE_STAFF_ONLINE = "STAFF_ONLINE";
    public static final String PARAM_TYPE_SALE_SERVICE_CODE = "SALE_SERVICE_CODE";
    public static final Long OWNER_TYPE_STAFF = 2l;
    public static final Long OWNER_TYPE_SHOP = 1l;
    public static final String STOCK_MODEL_CODE_TOPTUP = "TOPUP";
    public static final String STOCK_MODEL_CODE_TOPTUP_KHR = "TOPUP_KHR_";
    public static Long TRANS_IMPORT = 2L; //Nhap kho
    public static Long TRANS_EXPORT = 1L; //Xuat kho
    public static Long TRANS_RECOVER = 3L; //Thu hoi hang
    public static Long TRANS_SALE_AGENT_VTC = 7L;
    public static String SALE_TRANS_TYPE_AGENT = "2"; //ban dai ly
    public static String SALE_TRANS_TYPE_SERVICE = "4"; //ban hang dich vu
    public static String SALE_TRANS_RETAIL = "1"; //ban dai ly
    public static Long SALE_PAY_NOT_BILL = 2L;//GIAO DICH DA THANH TOAN CHUA LAP  HOA DON
    public static String TRANS_CODE_PREFIX = "GD0000"; // Ma giao dich ban hang (default: GD)
    public static String TRANS_ID_LENGTH = "9"; // Do dai ma giao dich ban hang (default: 13)
    public static Long STATE_NEW = 1L; //Hang moi
    public static String PARAM_TYPE_MIN_DEBT_PINCODE_TOPUP = "WARNING_AMOUNT_TOPUP_PINCODE";
    public static String PARAM_TYPE_WARNING_NUMBER = "WARNING_NUMBER_FINANCE";
    public static final String WARNING_FINANCE = "WARNING_FINANCE";
    public static final String PARAM_TYPE_PASS_USER = "PASS_ACCOUNT";
    public static final String BCCS_MERCHANT_MERCHANT_SEQ = "MERCHANT_SEQ";
    public static final String BCCS_MERCHANT_SERVICES_SEQ = "SERVICES_SEQ";
    public static final String BCCS_MERCHANT_CLIENT_SERVICE_SEQ = "CLIENT_SERVICE_SEQ";
    public static final String BCCS_MERCHANT_TRANSACTION_LOG_SEQ = "TRANSACTION_LOG_SEQ";
    public static final String BCCS_MERCHANT_ACTION_LOG_SEQ = "ACTION_LOG_SEQ";
    public static final String METFONE_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoPumOUdivGiw4qN5AVzGiJwabKFABpNMQ/wY71d+PSihkRwohRGl8zkmQ/5hu2hCM65eT0SS9buySpcrR6iIFpgL3SpeX4gMhRwQr6OIMQpR7JnRNrDPeg2igeDrfEl9fnfenOyqUfhhZzFXnwYtQs6Kilhqk2I9hmn1b5qxJ3L6RJKJsSnhQ8eEKdU3h1Bgj+cV5w1sadgN31KmQhbgFhld65QSE96cDAgDdjU/CzfXe4CimjlK0uJpephWpJagZDHRhsTRuDADYfmx/w67CjWvMxsGIbjgYFGuGCbnIUsxyqhAEQPJoH8iSSvinPNVJkSyRl3ivb6KdDRRQxC0owIDAQAB";
    public static final String METFONE_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCg+6Y5R2K8aLDio3kBXMaInBpsoUAGk0xD/BjvV349KKGRHCiFEaXzOSZD/mG7aEIzrl5PRJL1u7JKlytHqIgWmAvdKl5fiAyFHBCvo4gxClHsmdE2sM96DaKB4Ot8SX1+d96c7KpR+GFnMVefBi1CzoqKWGqTYj2GafVvmrEncvpEkomxKeFDx4Qp1TeHUGCP5xXnDWxp2A3fUqZCFuAWGV3rlBIT3pwMCAN2NT8LN9d7gKKaOUrS4ml6mFaklqBkMdGGxNG4MANh+bH/DrsKNa8zGwYhuOBgUa4YJuchSzHKqEARA8mgfyJJK+Kc81UmRLJGXeK9vop0NFFDELSjAgMBAAECggEAeUujf10ORDANt3a84g3peV1Fe7wz5cBuonr8vroJ4EMRGalBzqHxjEXvUyQEt5Ri7VXgsZFVudnnCNTGQ4P0pjGCHu6hlRGJfXIcEHsDTxAbkk6DmNW4Ppqva3TxegBVkkj3bv3Th1fC7GH9R1D8IW5GbN8ZzlsjSHfl+mVwPevofoO8faIKMNwvnkEzTsYWVZI376b11sN2IehRyDloV2N7mvn+1aKJ+fUXAuj+lAJf233Nyz69e7B4UqtbPSa6YwGUjP53PqW5ygRZNjY2nYQDNpaF7w8ZnEvi1gs8Cb902WY7y9M25Q4Hs63RXMnexTNt3mYthp6Hu/lrOcrNoQKBgQD2bX1pgb+s6/aBqCmWtByDh66TsEHuxiftVPxWnHhaVZr1wYQrt1OBmHOwGF4K1PQBkmcKm3WftZqM2c7PlTiXijXxAI4xxb6PrZAzEdV1Fsd0S/o2IaYj6WERFJ9//Mrrfn3woHPBDpaAlShbByQXzaZVO3+9MeamDBFB8Z8WLwKBgQCnPHxm3vutKc8kFonBoZzkKt6gRfBP7QH+IV+vmEoZQEbmFpTmAskxPTDS5mUJzKADS2KqLRvsAk/zAgZ8IQEic+/E06v0TMo0ZUShEUSSGV2tliQA2UoUhIPUrtkzO4BNd9eqptY8PtoHXlc9dQcw02RN5RWecI2O37OKJfffzQKBgHn4m5h7Rhoa+PoX9+3F3E8l+zzSgu/iWKTZ6cQc0l0z/e0iCX9xz/1XLfu9M978pIGaqzHO3JteUc7hh8GrREWNfUF6Wuc7zyRibfs5GoRH8BUZ/hCI+yjPPOItaPz6bJ2/ECrn4CNwSwH90emsUSsO+QM63fERH+6UDn6rT2i1AoGBAKHMZVdpuPlqD9yMaa8K2y3tRAKwZ7YMSfdC9r5+ioIvq+iERH7chya6WvhQq8R1UwKc9tJKXk9SVWN4vClwDRb2MC2v1RfBC4jxyRpECKGJJRcCwcaIc6GyK5GjsMRedt18aw3R6EfVTz/9yE3PYaudedfbIbVuk6Z9S8kGvvCpAoGBAKXqvyvafwCPJ4nVUji1sugoxWxt/sZ5wqd4VLq3wtDOwHyQWDROqWIazwaqkK8UMgs0DA+x1MzUBDdUgGLOYqSEa1DbnC+6RGw4IPl7oHjVWMN5hLp+SsuS8wFLSajqLbeuojq61aOn2qB6dsXRe3t5Wdb+u2zRQhmJlLYQJGF1";
    public static final String TYPE_CONF_IMT = "IMT";
    public static final String TYPE_CONF_DIGITAL = "DIGITAL";
    public static final String TYPE_CONF_TV = "TV";
    public static final String TYPE_CONF_SOC = "SOC";
    public static final String ACTION_REQUEST_ANALYSIS_SOC = "REQUEST_ANALYSIS_SOC";
    public static final String SOC_KEY_FAIL = "FAIL";
    public static final String TYPE_CONF_WHITELIST_REASON = "SOC_REASON";
    public static final String TYPE_CONF_WHITELIST_TASK = "SOC_TASK";
    public static final String PARAM_TYPE_SERVICE_CHECK_SOC = "SERVICE_CHECK_SOC";
    public static final String MODIFICATION_TYPE_CHANGE_SIM_MB_HP = "101";
    public static String PROVISIONING_MESSAGE_TYPE = "1900";
    public static String PROVISIONING_PROCESS_CODE_ACTIVE_SUB_4G = "310006";
    public static final String ACTIVE_SUB_PRE_SIM4G_TPL_ID = "1";
    public static final String ACTIVE_SUB_PRE_SIM4G_AMBRMAXUL = "400000000";
    public static final String ACTIVE_SUB_PRE_SIM4G_AMBRMAXDL = "800000000";
    public static final String MODIFICATION_TYPE_BLOCK_TWO_WAY = "300";
    public static final String MODIFICATION_TYPE_UN_BLOCK_TWO_WAY = "301";
    public static final String MODIFICATION_TYPE_BLOCK_OUT_GOING_CALL = "300";
    public static final String MODIFICATION_TYPE_UN_BLOCK_OUT_GOING_CALL = "301";
    public static final String ERROR_CODE_EMONEY_004 = "004"; //error
    public static final String ERROR_CODE_EMONEY_005 = "005"; //error
    public static final String ERROR_CODE_EMONEY_006 = "006"; //error
    public static final String ERROR_CODE_EMONEY_007 = "007"; //error
    public static final String PARAM_TYPE_PAY_SINGLE_EMONEY = "EMONEY_SINGLE";
    public static final String PARAM_TYPE_PAY_ADVANCE_EMONEY = "EMONEY_ADVANCE";
    public static final String PARAM_TYPE_PAY_EMONEY = "EMONEY_CLEAR";
    public static final String PARAM_TYPE_CHANGE_INFO_SUCCESS = "CHANGE_INFO_SUCCESS";
    public static final Long STATUS_ACTIVE_SUB_ID_NO = 1L;
    public static final String CUST_IMAGE_SEQ = "ACCOUNT_IMAGE_SEQ";
    public static final String CUST_IMAGE_DETAIL_SEQ = "CUST_IMAGE_DETAIL_SEQ";
//    public static final String ACCOUNT_IMAGE_SEQ = "ACCOUNT_IMAGE_SEQ";
    public static final String URL_NIM_ACCOUNT_INFRAS = "http://10.79.65.51/tmskhmer/api/v1/internet/service/monitor_customer_infra/?";
    public static final String CONNECTING_BUNDLE_TV = "888";
    public static final String OTHER_PRO_BUNDLE_TV = "BUNDLE_TV";
    public static final String MAPPING_PACKAGE_TV = "MAPPING_PACKAGE_TV";
    public static final String PRODUCT_CHECK_COUNT = "PRODUCT_CHECK_COUNT";
    // Action_Audit Prefix
    public static final String ACTION_AUDIT_DB_PREFIX = "CNDBKDN";      //Cap nhat DB khong dau noi
    public static final String ACTION_AUDIT_SW_PREFIX = "DNKCNDB";      //Dau noi khong cap nhat DB
    public static final String ACTION_AUDIT_DB_SW_PREFIX = "DNCNDB";    //Dau noi cap nhat DB
    public static final String ACTION_AUDIT_DB_EX_PREFIX = "DNCNDBEX";    //Dau noi cap nhat DB
    public static final String SUB_ACT_STATUS_IDLE = "03";//Thuê bao đã đấu nối nhưng chưa kích hoạt
    public static final Long SUB_PRODUCT_MB_STATUS_USE = 1L;
    public static final Long STATUS_ACTIVE__SUB_ID_NO = 1L;   // tràng thai hoat dong
    public static final String UPDATE_ISDN_TO_USING_ACTION_CODE = "20";
    public static Long MAX_RESUL_SEARCH_SUB_ID_NO = 10L;
    public static final Long SUB_REL_PRODUCT_STATUS_USE = 1L;
    public static String SUB_PRO_STATUS_ACTIVE = "1";
    public static String SUB_PRO_STATUS_IDLE = "0";
    public static final String FTP_DIR_TASK = "ftp_dir_task";
    public static final String FTP_DIR_SUB_ACTION = "ftp_dir_sub";
    public static final String APPARAM_PROVISIONING_ACCOUNT = "PROVISIONING_ACCOUNT";
    public static final String PARAM_VALUE_EXCHANGE = "EXCHANGE";
    public static final String APP_MY_METFONE = "My Metfone";
    public static final Long SOURCE_APP_MY_METFONE = 9l;
    public static final String PARAM_TYPE_EMONEY_EXCHANGE = "EMONEY_EXCHANGE";
    public static final String PARAM_TYPE_PACKAGE_DATA = "PACKAGE_DATA";
    public static final String PACKAGE_DATA_SALE_CODE = "PACKAGE_DATA_SALE_CODE";
    //daibq
    public static final String IS_AUTO_ASSIGN = "1";

    public static final String USER_LEVEL_VTC = "1";
    public static final String USER_LEVEL_BRANCH = "2";
    public static final String USER_LEVEL_SHOWROOM = "3";

    public static final String ERROR_CODE_008 = "008";
    public static final String ERROR_CODE_009 = "009";
    public static final String ERROR_CODE_010 = "010";
    public static final String ERROR_CODE_011 = "011";
    public static final String ERROR_CODE_012 = "012";
    public static final String ERROR_CODE_013 = "013";
    public static final String ERROR_CODE_014 = "014";
    public static final String ERROR_CODE_015 = "015";
    public static final String ERROR_CODE_016 = "016";
    public static final String ERROR_CODE_017 = "017";
    public static final String ERROR_CODE_018 = "018";
    public static final String ERROR_CODE_019 = "019";
    public static final String ERROR_CODE_020 = "020";
    //phuonghc
    //DEPLOY_REQUIREMENT
    public static final String MAP_LIMIT_MATERIAL_INFRA_TYPE_GPON = "GPON";
    public static final String MAP_LIMIT_MATERIAL_INFRA_TYPE_AON = "AON";
    public static final String[] ALLOW_CHANGE_ADDRESS_ADSL = new String[]{"BAT,PAI", "KEB,KAM"};
    public static final Long DEPLOY_REQ_STATUS_REQUESTED = 1L;
    //neu co yeu cau thay doi dia chi lap dat sau do doi port thanh cong thi se co status =3
    public static final Long DEPLOY_REQ_STATUS_CHANGE_PORT_SUCCESS = 3L;
    public static final String DEPLOY_REQ_TYPE_CHANGE_DEPLOY_AREA = "1";
    public static final String DEPLOY_REQUIREMENT_SEQ = "DEPLOY_REQUIREMENT_SEQ";
    public static final String DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm";
    public static final long STATUS_DELETE = 0;
    public static final Long SOURCE_ID_START = 8L;  //điểm đầu
    public static final String TASK_SHOP_PROGRESS_ONGOING = "1";
    public static final String TASK_REQ_TYPE_CHANGE_EQUIPMENT = "8";
    public static final String TASK_STAFF_PROGRESS_START = "0";
    public static final String TASK_REQ_TYPE_KS_OW = "2"; // Loai cong viec khao sat Office_Wan
    public static final Long JOB_STAGE_COMPLAINT = 5L;
    //Ma giai doan: Khao sat thuc te
    public static final Long JOB_STAGE_ACTUAL_SURVEY = 2L;
    //Khao sat thuc te
    public static final String TASK_SOURCE_TYPE_ACTUAL_SURVEY = "3";
    public static final String TASK_REQ_TYPE_KS_FLOW_WHITE_LL = "12"; // Loai cong viec khao sat luong kenh trang
    public static final String TYPE_PAY_METHOD = "303";
// khieudv
    public static final String PARAM_TYPE_KH_LANGUAGE = "KH_LANGUAGE";
    public static final String PARAM_TYPE_LOCALE = "kh_KH";

    // khieudv Divide FTTH registering reasons into different groups
    public static String MBCCS_REASON_CORP = "MBCCS_REASON_CORP";
    public static String COR_SUB = "COR_";
    public static String MBCCS_REASON_FBB = "MBCCS_REASON_CORP";
    public static String FBB_SUB = "FBB_";
}
