package com.viettel.bccs.cm.util;

import org.apache.log4j.Logger;

/**
 * @author vanghv1
 * @version 1.0
 * @since 20/08/2014 4:53 PM
 */
public class ConvertUtils {

    protected static Logger logger = Logger.getLogger(ConvertUtils.class);

    private ConvertUtils() {
    }

    public static Long toLong(Object s) {
        Long result = null;
        if (s == null) {
            return result;
        }
        String ss = s.toString().trim();
        if (ss.isEmpty()) {
            return result;
        }
        try {
            result = Long.parseLong(ss);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage());
            result = null;
        }
        return result;
    }

    public static Double toDouble(Object s) {
        Double result = null;
        if (s == null) {
            return result;
        }
        String ss = s.toString().trim();
        if (ss.isEmpty()) {
            return result;
        }
        try {
            result = Double.parseDouble(ss);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage());
            result = null;
        }
        return result;
    }

    public static String toStringValue(Object a) {
        if (a == null) {
            return null;
        }
        Class c = a.getClass();
        if (c.equals(java.util.Date.class) || c.equals(java.sql.Timestamp.class)) {
            return DateTimeUtils.formatddMMyyyyHHmmss(a);
        }
        return String.valueOf(a).trim();
    }

}
