/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.util;

import com.google.gson.Gson;
import static com.viettel.bccs.cm.util.Constants.TYPE_USSD;
import com.viettel.brcd.ws.common.EmoneyPaymentClient;
import com.viettel.brcd.ws.model.output.EmoneyResponse;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author cuongdm
 */
public class EmoneyPayment {

    public EmoneyResponse createInvoiceEmoney(Long transId, String url, String authorization, Double amount, String emoneyAccount, String content, String currency) {
        //create invoice emoney - call API emoney init
        //String url = resource.getString("create_invoice_emoney");
        //resource.getString("Authorization")
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("e-language", "en");
        headers.put("Authorization", String.format("epa %s", authorization));
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        amount = Double.valueOf(df.format(amount));
        String jsonBodyParam = "{\"paymentType\":" + TYPE_USSD
                + ",\"content\":" + "\"" + content + "\""
                + ",\"transAmount\":" + amount
                + ",\"refId\":" + "\"" + transId.toString() + "\""
                + ",\"currency\":" + "\"" + currency + "\""
                + ",\"customerPhoneNumber\":" + "\"" + emoneyAccount + "\""
                + ",\"ccyAcceptPayment\":" + "\"" + currency + "\"}";
        System.out.println(url);
        System.out.println(jsonBodyParam);
        EmoneyResponse response = EmoneyPaymentClient.shared().postRequest(url, headers, jsonBodyParam);
        System.out.println(new Gson().toJson(response));
        return response;
    }

    public EmoneyResponse confirmPayEmoney(String url, String txPaymentTokenId, String authorization, String privateKey, String publicKey, StringBuilder requestStr, StringBuilder responseStr, String content) {
        //payment ussd emoney - call API emoney ussd
        //String url = resource.getString("confirm_pay_emoney");
        //emoney.private.key
        //emoney.public.key
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("e-language", "en");
        headers.put("Authorization", String.format("epa %s", authorization));

        //Get file from resources folder
        //ClassLoader classLoader = getClass().getClassLoader();
        String decrypt = EmoneyPaymentClient.decryptRSA(txPaymentTokenId, privateKey);
        System.out.println("private_key: " + privateKey);

        String encrypt = EmoneyPaymentClient.encryptRSA(decrypt, publicKey);
        System.out.println("public_key: " + publicKey);

        String jsonBodyParam = "{\"txPaymentTokenId\":" + "\"" + encrypt + "\""
                + ",\"paymentContent\":" + "\"" + content + "\"}";
        if (requestStr != null) {
            requestStr.append(jsonBodyParam);
        }
        System.out.println("jsonBodyParam: " + jsonBodyParam);
        EmoneyResponse response = EmoneyPaymentClient.shared().postRequest(url, headers, jsonBodyParam);
        String responseResult = new Gson().toJson(response);
        System.out.println("response: " + responseResult);
        if (responseStr != null) {
            responseStr.append(responseResult);
        }
        return response;
    }
}
