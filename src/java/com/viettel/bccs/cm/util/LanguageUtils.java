package com.viettel.bccs.cm.util;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 * @author vanghv1
 * @version 1.0
 * @since 20/08/2014 4:53 PM
 */
public class LanguageUtils {

    private static final String LOCALE_SEPARATOR = "_";
    private static final Logger logger = Logger.getLogger(LanguageUtils.class);
    private static HashMap<String, ResourceBundle> mapLanguageBundle = new HashMap<String, ResourceBundle>();

    private LanguageUtils() {
    }

    private static ResourceBundle getLanguageBundle(String language) {
        if (language == null) {
            return null;
        }
        language = language.trim();
        if (language.isEmpty()) {
            return null;
        }
        if (!mapLanguageBundle.containsKey(language)) {
            ResourceBundle resourceBundle = ResourceUtils.getResourceBundle(getLanguageFile(language));
            if (resourceBundle != null) {
                mapLanguageBundle.put(language, resourceBundle);
            }
        }
        return mapLanguageBundle.get(language);
    }

    private static String getLanguageFile(String language) {
        return new StringBuilder().append(ResourceUtils.getLanguageFileLocation()).append(LOCALE_SEPARATOR).append(language).toString();
    }

    public static String getString(String language, String key) {
        try {
            return getLanguageBundle(language).getString(key);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return key;
        }
    }

    private static String getString(ResourceBundle resourceBundle, String key) {
        try {
            return resourceBundle.getString(key);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return key;
        }
    }

    public static String getString(String language, String key, String... args) {
        try {
            ResourceBundle resourceBundle = getLanguageBundle(language);
            MessageFormat messageFormat = new MessageFormat(getString(resourceBundle, key));
            String[] arguments = new String[args.length];
            int i = 0;
            for (String arg : args) {
                arg = getString(resourceBundle, arg);
                arguments[i++] = arg;
            }
            return messageFormat.format(arguments);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return key;
        }
    }

    public static String formatString(String language, String key, Object... args) {
        try {
            ResourceBundle resourceBundle = getLanguageBundle(language);
            MessageFormat messageFormat = new MessageFormat(getString(resourceBundle, key));
            return messageFormat.format(args);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return key;
        }
    }

    public static String getString(String language, String[] args, String key) {
        try {
            ResourceBundle resourceBundle = getLanguageBundle(language);
            MessageFormat messageFormat = new MessageFormat(getString(resourceBundle, key));
            StringBuilder stringBuilder = new StringBuilder();
            if (args != null) {
                int n = args.length;
                if (n > 0) {
                    stringBuilder.append("(");
                    for (int i = 0; i < n - 1; i++) {
                        stringBuilder.append(getString(resourceBundle, args[i])).append(", ");
                    }
                    stringBuilder.append(getString(resourceBundle, args[n - 1])).append(")");
                }
            }
            return messageFormat.format(new String[]{stringBuilder.toString()});
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return key;
        }
    }
}
