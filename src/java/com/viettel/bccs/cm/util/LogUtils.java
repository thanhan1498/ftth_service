package com.viettel.bccs.cm.util;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

/**
 * @author vanghv1
 * @version 1.0
 * @since 20/08/2014 4:53 PM
 */
public class LogUtils {

    protected static final Gson gson = new Gson();

    public LogUtils() {
    }

    public static void info(Logger logger, Object obj) {
        logger.info(obj);
    }

    public static void info(Logger logger, Object obj, Throwable ex) {
        logger.info(obj, ex);
        ex.printStackTrace();
    }

    public static void error(Logger logger, Object obj) {
        logger.error(obj);
    }

    public static void error(Logger logger, Object obj, Throwable ex) {
        logger.error(obj, ex);
        ex.printStackTrace();
    }

    public static String toJson(Object obj) {
        return obj == null ? null : gson.toJson(obj);
    }
}
