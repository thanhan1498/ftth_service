package com.viettel.bccs.cm.util;

import com.viettel.bccs.cm.dao.BaseDAO;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.apache.log4j.Logger;

/**
 * @author vanghv1
 * @version 1.0
 * @since 20/08/2014 4:53 PM
 */
public class ReflectUtils {

    private static final Logger logger = Logger.getLogger(ReflectUtils.class.getName());
    private static final String GETTER_PREFIX = "get";
    private static final String GETTER_BOOLEAN_PREFIX = "is";
    private static final String SETTER_PREFIX = "set";
    private static final HashMap<String, String> mTableName = new HashMap<String, String>();
    private static final HashMap<String, HashMap<String, Integer>> mLength = new HashMap<String, HashMap<String, Integer>>();
    private static final HashMap<String, HashMap<String, String>> mColumn = new HashMap<String, HashMap<String, String>>();
    private static final Object lock = new Object();

    private ReflectUtils() {
    }

    public static String getTableName(Class<?> c) {
        new BaseDAO().info(logger, "ReflectUtils.getTableName;c=" + c);
        String tableName = null;
        try {
            if (c == null) {
                return tableName;
            }
            String cName = c.getName();
            if (cName == null || cName.isEmpty()) {
                return tableName;
            }
            if (mTableName.get(cName) == null) {
                synchronized (lock) {
                    Table table = c.getAnnotation(Table.class);
                    if (table != null) {
                        mTableName.put(cName, table.name());
                    } else {
                        mTableName.put(cName, Constants.EMPTY_STRING);
                    }
                }
            }
            tableName = mTableName.get(cName);
        } catch (Exception ex) {
            new BaseDAO().error(logger, "ReflectUtils.getTableName;Exception=" + ex.getMessage(), ex);
        } finally {
            new BaseDAO().info(logger, "ReflectUtils.getTableName;Result=" + tableName);
        }
        return tableName;
    }

    public static Integer getColumnLength(Class<?> c, String field) {
        new BaseDAO().info(logger, "ReflectUtils.getColumnLength;c=" + c + ";field=" + field);
        Integer length = null;
        try {
            if (c == null || field == null) {
                return length;
            }
            field = field.trim();
            if (field.isEmpty()) {
                return length;
            }
            String cName = c.getName();
            if (cName == null || cName.isEmpty()) {
                return length;
            }
            HashMap<String, Integer> map = mLength.get(cName);
            boolean isMapNull = false;
            if (map == null) {
                isMapNull = true;
                synchronized (lock) {
                    map = new HashMap<String, Integer>();
                }
            }
            new BaseDAO().info(logger, "ReflectUtils.getColumnLength;isMapNull=" + isMapNull);
            new BaseDAO().info(logger, "ReflectUtils.getColumnLength;(map.get(field) == null)=" + (map.get(field) == null));
            if (map.get(field) == null) {
                synchronized (lock) {
                    try {
                        Field f = c.getDeclaredField(field);
                        length = getColumnLength(f);
                    } catch (NoSuchFieldException ex) {
                        new BaseDAO().error(logger, "ReflectUtils.getColumnLength;NoSuchFieldException=" + ex.getMessage(), ex);
                    }
                    if (length == null || length <= 0) {
                        Class ic = getIdClass(c);
                        new BaseDAO().info(logger, "ReflectUtils.getColumnLength;ic=" + ic);
                        if (ic != null) {
                            try {
                                Field f = ic.getDeclaredField(field);
                                length = getColumnLength(f);
                            } catch (NoSuchFieldException ex) {
                                new BaseDAO().error(logger, "ReflectUtils.getColumnLength;NoSuchFieldException=" + ex.getMessage(), ex);
                            }
                        }
                    }
                    if (length == null || length < 0) {
                        length = 0;
                    }
                    map.put(field, length);
                }
            }
            if (isMapNull) {
                synchronized (lock) {
                    mLength.put(cName, map);
                }
            }
            length = map.get(field);
        } catch (Exception ex) {
            new BaseDAO().error(logger, "ReflectUtils.getColumnLength;Exception=" + ex.getMessage(), ex);
        } finally {
            new BaseDAO().info(logger, "ReflectUtils.getColumnLength;Result=" + length);
        }
        return length;
    }

    public static String getColumnName(Class<?> c, String field) {
        new BaseDAO().info(logger, "ReflectUtils.getColumnName;c=" + c + ";field=" + field);
        String colName = null;
        try {
            if (c == null || field == null) {
                return colName;
            }
            field = field.trim();
            if (field.isEmpty()) {
                return colName;
            }
            String cName = c.getName();
            if (cName == null || cName.isEmpty()) {
                return colName;
            }
            HashMap<String, String> map = mColumn.get(cName);
            boolean isMapNull = false;
            if (map == null) {
                isMapNull = true;
                synchronized (lock) {
                    map = new HashMap<String, String>();
                }
            }
            new BaseDAO().info(logger, "ReflectUtils.getColumnName;isMapNull=" + isMapNull);
            new BaseDAO().info(logger, "ReflectUtils.getColumnName;(map.get(field) == null)=" + (map.get(field) == null));
            if (map.get(field) == null) {
                synchronized (lock) {
                    try {
                        Field f = c.getDeclaredField(field);
                        colName = getColumnName(f);
                    } catch (NoSuchFieldException ex) {
                        new BaseDAO().error(logger, "ReflectUtils.getColumnName;NoSuchFieldException=" + ex.getMessage(), ex);
                    }
                    if (colName == null || colName.isEmpty()) {
                        Class ic = getIdClass(c);
                        new BaseDAO().info(logger, "ReflectUtils.getColumnName;ic=" + ic);
                        if (ic != null) {
                            try {
                                Field f = ic.getDeclaredField(field);
                                colName = getColumnName(f);
                            } catch (NoSuchFieldException ex) {
                                new BaseDAO().error(logger, "ReflectUtils.getColumnName;NoSuchFieldException=" + ex.getMessage(), ex);
                            }
                        }
                    }
                    if (colName == null) {
                        colName = Constants.EMPTY_STRING;
                    }
                    colName = colName.trim();
                    map.put(field, colName);
                }
            }
            if (isMapNull) {
                synchronized (lock) {
                    mColumn.put(cName, map);
                }
            }
            colName = map.get(field);
        } catch (Exception ex) {
            new BaseDAO().error(logger, "ReflectUtils.getColumnName;Exception=" + ex.getMessage(), ex);
        } finally {
            new BaseDAO().info(logger, "ReflectUtils.getColumnName;Result=" + colName);
        }
        return colName;
    }

    public static Class getIdClass(Class<?> c) {
        if (c == null) {
            return null;
        }
        IdClass ic = c.getAnnotation(IdClass.class);
        return ic == null ? null : ic.value();
    }

    public static String getColumnName(Method method) {
        if (method == null) {
            return null;
        }
        Column column = method.getAnnotation(Column.class);
        return column == null ? null : column.name();
    }

    public static String getColumnName(Field field) {
        if (field == null) {
            return null;
        }
        Column column = field.getAnnotation(Column.class);
        return column == null ? null : column.name();
    }

    public static Integer getColumnLength(Method method) {
        if (method == null) {
            return null;
        }
        Column column = method.getAnnotation(Column.class);
        return column == null ? null : column.length();
    }

    public static Integer getColumnLength(Field field) {
        if (field == null) {
            return null;
        }
        Column column = field.getAnnotation(Column.class);
        return column == null ? null : column.length();
    }

    public static Boolean getColumnNullAble(Method method) {
        if (method == null) {
            return null;
        }
        Column column = method.getAnnotation(Column.class);
        return column != null && column.nullable();
    }

    public static boolean isGetter(Method method) {
        if (Modifier.isPublic(method.getModifiers())
                && method.getParameterTypes().length == 0
                && method.getAnnotation(Transient.class) == null) {
            if (method.getName().startsWith(GETTER_PREFIX)
                    && !method.getReturnType().equals(Void.TYPE)) {
                return true;
            }
            if (method.getName().startsWith(GETTER_BOOLEAN_PREFIX)
                    && method.getReturnType().equals(Boolean.TYPE)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSetter(Method method) {
        return Modifier.isPublic(method.getModifiers())
                && method.getReturnType().equals(Void.TYPE)
                && method.getParameterTypes().length == 1
                && method.getName().startsWith(SETTER_PREFIX);
    }

    public static List<Method> getGetters(Class c) {
        List<Method> getters = new ArrayList<Method>();
        Method[] methods = c.getDeclaredMethods();
        for (Method method : methods) {
            if (isGetter(method)) {
                getters.add(method);
            }
        }
        return getters;
    }

    public static List<Method> getSetters(Class c) {
        List<Method> setters = new ArrayList<Method>();
        Method[] methods = c.getDeclaredMethods();
        for (Method method : methods) {
            if (isSetter(method)) {
                setters.add(method);
            }
        }
        return setters;
    }

    public static Method getGetterByName(Class c, String methodName) {
        Method[] methods = c.getDeclaredMethods();
        for (Method method : methods) {
            if (isGetter(method) && method.getName().equals(methodName)) {
                return method;
            }
        }
        return null;
    }

    public static Method getGetterByName(List<Method> getters, String methodName) {
        for (Method method : getters) {
            if (method.getName().equals(methodName)) {
                return method;
            }
        }
        return null;
    }

    public static List<String> getColumnBeanNames(Class c) {
        List<String> result = new ArrayList<String>();
        List<Method> getters = getGetters(c);
        for (Method method : getters) {
            result.add(getColumnBeanName(method));
        }
        return result;
    }

    public static String getColumnBeanName(Method method) {
        String methodName = method.getName();
        return methodName.substring(3, 4).toLowerCase() + methodName.substring(4);
    }

    public static HashMap<String, Method> getMapColumnAndGetter(Class c) {
        List<Method> getters = getGetters(c);
        HashMap<String, Method> mapGetter = new HashMap();
        for (Method method : getters) {
            String columnName = getColumnName(method);
            if (columnName != null && !mapGetter.containsKey(columnName)) {
                mapGetter.put(columnName, method);
            }
        }
        return mapGetter;
    }

    public static HashMap<String, Method> getMapColumnAndSetter(Class c) {
        List<Method> getters = getGetters(c);
        HashMap<String, Method> mapSetter = new HashMap();
        for (Method method : getters) {
            if (method != null) {
                Method setter = getSetter(c, method);
                if (setter != null) {
                    String columnName = getColumnName(method);
                    if (columnName != null && !mapSetter.containsKey(columnName)) {
                        mapSetter.put(columnName, setter);
                    }
                }
            }
        }
        return mapSetter;
    }

    public static Method getSetter(Class c, Method getter) {
        Method setter = null;
        String getterName = getter.getName();
        if (getterName != null && getterName.length() > 1) {
            String setterName = "s" + getterName.substring(1);
            Class returnType = getter.getReturnType();
            try {
                setter = c.getMethod(setterName, returnType);
            } catch (Exception ex) {
                new BaseDAO().error(logger, ex.getMessage(), ex);
            }
        }
        return setter;
    }
}
