package com.viettel.bccs.cm.util;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author vanghv1
 * @version 1.0
 * @since 20/08/2014 4:53 PM
 */
public class ValidateUtils {

    private static HashMap<String, Pattern> mapPattern = new HashMap<String, Pattern>();
    public static final String REG_DATE_MMyyyy = "^((0\\d)|(1[0-2]))\\/\\d\\d\\d\\d$";
    public static final String REG_DATE_ddMMyyyy = "^(([0-2]\\d)|3[0-1])\\/((0[1-9])|(1[0-2]))\\/\\d\\d\\d\\d$";
    public static final String REG_DATE_ddMMyyyyHHmmss = "^(([0-2]\\d)|3[0-1])\\/((0\\d)|(1[0-2]))\\/\\d\\d\\d\\d\\s(([0-1]\\d)|(2[0-3])):[0-5]\\d:[0-5]\\d$";
    public static final String EMAIL_REG = "^[a-zA-Z]([a-zA-Z0-9]*(\\.|_)?[a-zA-Z0-9]*)*@([a-zA-Z]{2,}\\.)+[a-zA-Z]{2,}$";
    public static final String EMAIL_PLUS_REG = "^((?!((_\\.)|(\\._))).)*$";
    public static final String URL_REG = "^www.[A-Za-z0-9]+.[A-Za-z0-9.]+$";
    public static final String INTEGER_REG = "^(\\+|-|\\d)\\d*$";
    public static final String FLOAT_REG = "^(\\+|-|\\d)\\d*(\\.)?\\d*$";
    public static final String ISDN_REG = "^(\\d|\\+)\\d{6,14}$";
    public static final String BUS_TYPE_REG = "^[a-zA-z0-9_]+$";
    public static final String STATEMENT_NUMBER_REG = "^[a-zA-z0-9\\-/_\\\\]+$";
    public static final String FILE_NAME_REG = "^(\\w|\\+|,)(\\w|\\+|,)*\\.[A-Za-z]{3,4}$";
    public static final String FILE_NAME_FOR_IMPORT_REG = "^[a-zA-Z0-9\\_]+\\.[a-zA-Z]*";

    private ValidateUtils() {
    }

    public static Pattern getPattern(String reg) {
        if (!mapPattern.containsKey(reg)) {
            mapPattern.put(reg, Pattern.compile(reg));
        }
        return mapPattern.get(reg);
    }

    public static boolean isNullOrEmpty(String value) {
        return value == null || value.trim().isEmpty();
    }

    public static boolean isNullOrEmpty(List value) {
        return value == null || value.isEmpty();
    }

    public static boolean isDateMMyyyy(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(REG_DATE_MMyyyy).matcher(value).matches();
    }

    public static boolean isDateddMMyyyy(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(REG_DATE_ddMMyyyy).matcher(value).matches();
    }

    public static boolean isDateddMMyyyyHHmmss(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(REG_DATE_ddMMyyyyHHmmss).matcher(value).matches();
    }

    public static boolean isEmail(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(EMAIL_REG).matcher(value).matches() && getPattern(EMAIL_PLUS_REG).matcher(value).matches();
    }

    public static boolean isUrl(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(URL_REG).matcher(value).matches();
    }

    public static boolean isInteger(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(INTEGER_REG).matcher(value).matches();
    }

    public static boolean isFloat(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(FLOAT_REG).matcher(value).matches();
    }

    public static boolean isFileName(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(FILE_NAME_REG).matcher(value).matches();
    }

    public static boolean isFileImport(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(FILE_NAME_FOR_IMPORT_REG).matcher(value).matches();
    }

    public static boolean isFileExtension(String value, String extension) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern("^[a-zA-Z0-9\\_]+\\.(" + extension + ")").matcher(value.toLowerCase()).matches();
    }

    public static boolean isIsdn(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(ISDN_REG).matcher(value).matches();
    }

    public static boolean isStatementNumber(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(STATEMENT_NUMBER_REG).matcher(value).matches();
    }

    public static boolean isBusType(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(BUS_TYPE_REG).matcher(value).matches();
    }

    /*
     * @author vanghv1
     * @since 01/01/2014
     * return true if a != b
     */
    public boolean isNotEquals(Object a, Object b) {
        boolean result = false;
        if (a == null && b == null) {
            return result;
        }
        if (a != null && b != null) {
            Class cA = a.getClass();
            Class cB = b.getClass();
            if (cA.equals(cB)) {
                if (a.equals(b)) {
                    return result;
                }
                if (cA.equals(String.class)) {
                    String strA = String.valueOf(a);
                    String strB = String.valueOf(b);
                    if (strA.trim().equals(strB.trim())) {
                        return result;
                    }
                }
            }
            if (cA.equals(java.sql.Timestamp.class) && cB.equals(java.util.Date.class)) {
                if (((java.sql.Timestamp) a).getTime() == ((java.util.Date) b).getTime()) {
                    return result;
                }
            }
            if (cA.equals(java.util.Date.class) && cB.equals(java.sql.Timestamp.class)) {
                if (((java.util.Date) a).getTime() == ((java.sql.Timestamp) b).getTime()) {
                    return result;
                }
            }
        }
        if (a != null && b == null && a.getClass().equals(String.class)) {
            if (String.valueOf(a).trim().isEmpty()) {
                return result;
            }
        }
        if (b != null && a == null && b.getClass().equals(String.class)) {
            if (String.valueOf(b).trim().isEmpty()) {
                return result;
            }
        }
        result = true;
        return result;
    }

//    public static boolean isNumeric(String str) {
//        try {
//            double d = Double.parseDouble(str);
//        } catch (NumberFormatException nfe) {
//            return false;
//        }
//        return true;
//    }
    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
}
