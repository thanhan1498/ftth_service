/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.ws;

import com.viettel.bccs.cm.controller.CmFtthWSController;
import com.viettel.brcd.ws.model.input.CreateInterruptFtthInput;
import com.viettel.brcd.ws.model.input.PushSmsInterruptFtthInput;
import com.viettel.brcd.ws.model.output.CmFtthWSOut;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.WebServiceContext;

/**
 *
 * @author partner1
 */
@WebService(serviceName = "CmFtthWS")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public class CmFtthWS {
    @Resource
    WebServiceContext wsContext;
    static CmFtthWSController cmFtthWSController;
    
    @WebMethod(exclude = true)
    public void setCmFtthWSController(CmFtthWSController cmFtthWSController) {
        this.cmFtthWSController = cmFtthWSController;
    }
    
    @WebMethod(operationName = "getCurBranchUser")
    public CmFtthWSOut getCurBranchUser(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "staffId") Long staffId) {
        return cmFtthWSController.getCurBranchUser(token, locale, staffId);
    }
    
    @WebMethod(operationName = "getReasonType")
    public CmFtthWSOut getReasonType(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmFtthWSController.getReasonType(token, locale);
    }
    
    @WebMethod(operationName = "getReasonDetail")
    public CmFtthWSOut getReasonDetail(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "reasonTypeId") Long reasonTypeId) {
        return cmFtthWSController.getReasonDetail(token, locale, reasonTypeId);
    }
    
    @WebMethod(operationName = "getBranchList")
    public CmFtthWSOut getBranchList(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmFtthWSController.getBranchList(token, locale);
    }
    
    @WebMethod(operationName = "getStaffOfBranch")
    public CmFtthWSOut getStaffOfBranch(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "branchShopId") Long branchShopId) {
        return cmFtthWSController.getStaffOfBranch(token, locale, branchShopId);
    }
    
    @WebMethod(operationName = "getBtsByBranch")
    public CmFtthWSOut getBtsByBranch(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "branchShopId") Long branchShopId) {
        return cmFtthWSController.getBtsByBranch(token, locale, branchShopId);
    }
    
    @WebMethod(operationName = "getConnectorByStation")
    public CmFtthWSOut getConnectorByStation(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "stationId") Long stationId) {
        return cmFtthWSController.getConnectorByStation(token, locale, stationId);
    }
    
    @WebMethod(operationName = "createInterruptFtth")
    public CmFtthWSOut createInterruptFtth(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "createInterruptFtthInput") CreateInterruptFtthInput createInterruptFtthInput) {
        return cmFtthWSController.createInterruptFtth(token, locale, createInterruptFtthInput);
    }
    
    @WebMethod(operationName = "getInterruptFtth")
    public CmFtthWSOut getInterruptFtth(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "staffCode") String staffCode
            , @WebParam(name = "reasonTypeId") Long reasonTypeId, @WebParam(name = "reasonDetailId") Long reasonDetailId, @WebParam(name = "influenceScope") String influenceScope
            , @WebParam(name = "fromDate") String fromDate, @WebParam(name = "toDate") String toDate, @WebParam(name = "shopId") Long shopId) {
        return cmFtthWSController.getInterruptFtth(token, locale, staffCode, reasonTypeId, reasonDetailId, influenceScope, fromDate, toDate, shopId);
    }
    
    @WebMethod(operationName = "getDetailInterruptFtth")
    public CmFtthWSOut getDetailInterruptFtth(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "interruptId") Long interruptId) {
        return cmFtthWSController.getDetailInterruptFtth(token, locale, interruptId);
    }
    
    @WebMethod(operationName = "getHistoryInterruptFtth")
    public CmFtthWSOut getHistoryInterruptFtth(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "interruptId") Long interruptId) {
        return cmFtthWSController.getHistoryInterruptFtth(token, locale, interruptId);
    }
    
    @WebMethod(operationName = "approveInterruptFtth")
    public CmFtthWSOut approveInterruptFtth(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "interruptId") Long interruptId, @WebParam(name = "staffCode") String staffCode) {
        return cmFtthWSController.approveInterruptFtth(token, locale, interruptId, staffCode);
    }
    
    @WebMethod(operationName = "cancelInterruptFtth")
    public CmFtthWSOut cancelInterruptFtth(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "interruptId") Long interruptId, @WebParam(name = "staffCode") String staffCode) {
        return cmFtthWSController.cancelInterruptFtth(token, locale, interruptId, staffCode);
    }
    
    @WebMethod(operationName = "pushSmsInterruptFtth")
    public CmFtthWSOut pushSmsInterruptFtth(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "pushSmsInterruptFtthInput") PushSmsInterruptFtthInput pushSmsInterruptFtthInput) {
        return cmFtthWSController.pushSmsInterruptFtth(token, locale, pushSmsInterruptFtthInput);
    }
     
    @WebMethod(operationName = "countCustomerBTSConnector")
    public CmFtthWSOut countCustomerBTSConnector(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "interruptId") Long interruptId) {
        return cmFtthWSController.countCustomerBTSConnector(token, locale, interruptId);
    }
     
    @WebMethod(operationName = "countCustomerServiceBtsConnector")
    public CmFtthWSOut countCustomerServiceBtsConnector(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "interruptId") Long interruptId, @WebParam(name = "btsId") Long btsId, @WebParam(name = "connectorId") Long connectorId) {
        return cmFtthWSController.countCustomerServiceBtsConnector(token, locale, interruptId, btsId, connectorId);
    }
    
    @WebMethod(operationName = "rejectInterruptFtth")
    public CmFtthWSOut rejectInterruptFtth(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "interruptId") Long interruptId, @WebParam(name = "staffCode") String staffCode) {
        return cmFtthWSController.rejectInterruptFtth(token, locale, interruptId, staffCode);
    }
}
