/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.ws;

import com.viettel.bccs.myMetfone.controller.MyMetfoneController;
import com.viettel.brcd.ws.model.input.ComplaintInput;
import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.WebServiceContext;
import com.viettel.brcd.ws.model.output.ParamOut;
import javax.jws.WebParam;

/**
 *
 * @author cuongdm
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public class CmMyMetfone {

    @Resource
    WebServiceContext wsContext;
    static MyMetfoneController myMetfoneController;

    @WebMethod(exclude = true)
    public void setMyMetfoneController(MyMetfoneController myMetfoneController) {
        this.myMetfoneController = myMetfoneController;
    }

    @WebMethod(operationName = "getComType")
    public ParamOut getComType(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "isdn") String isdn) {
        return myMetfoneController.getComType(token, locale, isdn);
    }

    @WebMethod(operationName = "submitComplaintMyMetfone")
    public ParamOut submitComplaint(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "complaintInput") ComplaintInput complaintInput) {
        return myMetfoneController.submitComplaint(token, locale, complaintInput);
    }

    @WebMethod(operationName = "getProcessList")
    public ParamOut getProcessList(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale) {
        return myMetfoneController.getProcessList(token, locale);
    }

    @WebMethod(operationName = "getComplaintHistory")
    public ParamOut getComplaintHistory(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "isdn") String isdn,
            @WebParam(name = "complaintId") Long complaintId,
            @WebParam(name = "rate") int rate) {
        return myMetfoneController.getComplaintHistory(token, locale, isdn, complaintId, rate);
    }

    @WebMethod(operationName = "reopenComplain")
    public ParamOut reopenComplain(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "complaintId") Long complaintId,
            @WebParam(name = "isdn") String isdn,
            @WebParam(name = "content") String content) {
        return myMetfoneController.reopenComplain(token, locale, complaintId, isdn, content);
    }

    @WebMethod(operationName = "closeComplain")
    public ParamOut closeComplain(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "complaintId") Long complaintId,
            @WebParam(name = "isdn") String isdn) {
        return myMetfoneController.closeComplain(token, locale, complaintId, isdn);
    }

    @WebMethod(operationName = "rateComplain")
    public ParamOut rateComplain(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "complaintId") String complaintId,
            @WebParam(name = "isdn") String isdn,
            @WebParam(name = "rate") String rate) {
        return myMetfoneController.rateComplain(token, locale, complaintId, isdn, rate);
    }
}
