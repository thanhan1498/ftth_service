/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.cm.ws;

import com.viettel.bccs.cm.controller.Portal.CmPortalController;
import com.viettel.brcd.ws.model.output.ParamOut;
import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.WebServiceContext;

/**
 *
 * @author cuongdm
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public class CmPortal {

    @Resource
    WebServiceContext wsContext;
    static CmPortalController cmPortalController;

    @WebMethod(exclude = true)
    public void setCmPortalController(CmPortalController cmPortalController) {
        this.cmPortalController = cmPortalController;
    }
    
    @WebMethod(operationName = "makeSaleTrans")
    public ParamOut makeSaleTrans(
            @WebParam(name = "token") String token,
            @WebParam(name = "encrypt") String encrypt,
            @WebParam(name = "signature") String signature) {
        return cmPortalController.makeSaleTrans(token, encrypt, signature);
    }
    
    @WebMethod(operationName = "getSubInfoSms")
    public ParamOut getSubInfoSms(
            @WebParam(name = "token") String token,
            @WebParam(name = "encrypt") String encrypt,
            @WebParam(name = "signature") String signature) {
        return cmPortalController.getSubInfoSms(token, encrypt, signature);
    }
}
