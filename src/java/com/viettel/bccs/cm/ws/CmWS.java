package com.viettel.bccs.cm.ws;

import com.viettel.bccs.api.Task.BO.TransLogsClient;
import com.viettel.bccs.cm.controller.CmController;
import com.viettel.bccs.cm.controller.RegOnlineController;
import com.viettel.bccs.cm.model.Codes;
import com.viettel.bccs.cm.model.FormChangeInformation;
import com.viettel.bccs.target.controller.TargetOnlineController;
import com.viettel.brcd.ws.model.input.ActivePreIn;
import com.viettel.brcd.ws.model.input.ActiveSubscriberPreIn;
import com.viettel.brcd.ws.model.input.CancelContractInput;
import com.viettel.brcd.ws.model.input.CancelRULInput;
import com.viettel.brcd.ws.model.input.CancelRequestInput;
import com.viettel.brcd.ws.model.input.CancelSubFbConfigInput;
import com.viettel.brcd.ws.model.input.ChangeDeploymentAddressIn;
import com.viettel.brcd.ws.model.input.ChangeSimIn;
import com.viettel.brcd.ws.model.input.ChangeSimInput;
import com.viettel.brcd.ws.model.input.ComplaintInput;
import com.viettel.brcd.ws.model.input.ContractIn;
import com.viettel.brcd.ws.model.input.Coordinate;
import com.viettel.brcd.ws.model.input.CustomerIn;
import com.viettel.brcd.ws.model.input.ImageInput;
import com.viettel.brcd.ws.model.input.InfrastructureUpdateIn;
import com.viettel.brcd.ws.model.input.PaymentInput;
import com.viettel.brcd.ws.model.input.RevokeTaskIn;
import com.viettel.brcd.ws.model.input.SearchSubRequestInput;
import com.viettel.brcd.ws.model.input.SearchTaskForLookUpInput;
import com.viettel.brcd.ws.model.input.SearchTaskForUpdateInput;
import com.viettel.brcd.ws.model.input.SearchTaskInput;
import com.viettel.brcd.ws.model.input.SearchTaskRevokeIn;
import com.viettel.brcd.ws.model.input.SubAdslLeaselineOut;
import com.viettel.brcd.ws.model.input.TaskToAssignIn;
import com.viettel.brcd.ws.model.input.UpdateNoteIn;
import com.viettel.brcd.ws.model.input.UpdateTaskIn;
import com.viettel.brcd.ws.model.input.WSRequest;
import com.viettel.brcd.ws.model.input.regOnline.RegOnlineInput;
import com.viettel.brcd.ws.model.output.ApDomainOut;
import com.viettel.brcd.ws.model.output.ApParamOut;
import com.viettel.brcd.ws.model.output.BankOut;
import com.viettel.brcd.ws.model.output.BundleTVInfo;
import com.viettel.brcd.ws.model.output.CableBoxOut;
import com.viettel.brcd.ws.model.output.ChangeInformationOut;
import com.viettel.brcd.ws.model.output.ClearDebitTransOut;
import com.viettel.brcd.ws.model.output.CompDepartmentGroupOut;
import com.viettel.brcd.ws.model.output.ComplainResultOut;
import com.viettel.brcd.ws.model.output.ComplaintAcceptSourceOut;
import com.viettel.brcd.ws.model.output.ComplaintAcceptTypeOut;
import com.viettel.brcd.ws.model.output.ComplaintGroupOut;
import com.viettel.brcd.ws.model.output.ComplaintTypeOut;
import com.viettel.brcd.ws.model.output.CoordinateOut;
import com.viettel.brcd.ws.model.output.CustomTypeOut;
import com.viettel.brcd.ws.model.output.CustomerOut;
import com.viettel.brcd.ws.model.output.DashboardBroadbanTab;
import com.viettel.brcd.ws.model.output.DebitOut;
import com.viettel.brcd.ws.model.output.DebitStaffOut;
import com.viettel.brcd.ws.model.output.DebitTransHistoryOut;
import com.viettel.brcd.ws.model.output.DebitTransOut;
import com.viettel.brcd.ws.model.output.DepositOut;
import com.viettel.brcd.ws.model.output.DistrictOut;
import com.viettel.brcd.ws.model.output.DomainOut;
import com.viettel.brcd.ws.model.output.FileOut;
import com.viettel.brcd.ws.model.output.GroupOut;
import com.viettel.brcd.ws.model.output.GroupReasonOut;
import com.viettel.brcd.ws.model.output.HistorySubFbConfigOut;
import com.viettel.brcd.ws.model.output.IdTypeOut;
import com.viettel.brcd.ws.model.output.ImageInfrastructureOut;
import com.viettel.brcd.ws.model.output.ImageRespone;
import com.viettel.brcd.ws.model.output.InforInfrastructureOut;
import com.viettel.brcd.ws.model.output.InforResolveComplainOut;
import com.viettel.brcd.ws.model.output.InforTaskToUpdateOut;
import com.viettel.brcd.ws.model.output.InvestOut;
import com.viettel.brcd.ws.model.output.InvoiceListOut;
import com.viettel.brcd.ws.model.output.LastUpdateConfigOut;
import com.viettel.brcd.ws.model.output.LimitValueOut;
import com.viettel.brcd.ws.model.output.LineTypeOut;
import com.viettel.brcd.ws.model.output.LinkFileOut;
import com.viettel.brcd.ws.model.output.ListReasonByCodePreOut;
import com.viettel.brcd.ws.model.output.LocalProductPpOut;
import com.viettel.brcd.ws.model.output.LockDebitTransOut;
import com.viettel.brcd.ws.model.output.LoginOut;
import com.viettel.brcd.ws.model.output.MaxRadiusOut;
import com.viettel.brcd.ws.model.output.NotificationTaskOut;
import com.viettel.brcd.ws.model.output.OTPOut;
import com.viettel.brcd.ws.model.output.OwnerOut;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.PayAdvanceBillOut;
import com.viettel.brcd.ws.model.output.PayContractOut;
import com.viettel.brcd.ws.model.output.PaymentOut;
import com.viettel.brcd.ws.model.output.PortOut;
import com.viettel.brcd.ws.model.output.PreRevokeOut;
import com.viettel.brcd.ws.model.output.PrecinctOut;
import com.viettel.brcd.ws.model.output.ProductOut;
import com.viettel.brcd.ws.model.output.ProductPricePlanOut;
import com.viettel.brcd.ws.model.output.PromotionOut;
import com.viettel.brcd.ws.model.output.ProvinceOut;
import com.viettel.brcd.ws.model.output.ReasonChangeSimOut;
import com.viettel.brcd.ws.model.output.ReasonConfigOut;
import com.viettel.brcd.ws.model.output.ReasonConnectOut;
import com.viettel.brcd.ws.model.output.ReasonOut;
import com.viettel.brcd.ws.model.output.ReasonPreConnectOut;
import com.viettel.brcd.ws.model.output.RequestStatusOut;
import com.viettel.brcd.ws.model.output.RequestUpdateLocationOut;
import com.viettel.brcd.ws.model.output.ResultTeamCode;
import com.viettel.brcd.ws.model.output.ResultUpdateTaskOut;
import com.viettel.brcd.ws.model.output.SalaryOut;
import com.viettel.brcd.ws.model.output.SearchSubRequestOut;
import com.viettel.brcd.ws.model.output.SearchTaskRevokeOut;
import com.viettel.brcd.ws.model.output.SerialOut;
import com.viettel.brcd.ws.model.output.ServiceInfoOut;
import com.viettel.brcd.ws.model.output.ServiceOut;
import com.viettel.brcd.ws.model.output.ServicePaymentOut;
import com.viettel.brcd.ws.model.output.SourceTypeOut;
import com.viettel.brcd.ws.model.output.StaffDetailOut;
import com.viettel.brcd.ws.model.output.StaffInfoOut;
import com.viettel.brcd.ws.model.output.StaffSalaryDetail;
import com.viettel.brcd.ws.model.output.StationInfo;
import com.viettel.brcd.ws.model.output.StepInfoOut;
import com.viettel.brcd.ws.model.output.SubFbConfigOut;
import com.viettel.brcd.ws.model.output.SubInfoByISDNPreOut;
import com.viettel.brcd.ws.model.output.SubTypeOut;
import com.viettel.brcd.ws.model.output.TaskDetailToAssignOut;
import com.viettel.brcd.ws.model.output.TaskForUpdateOut;
import com.viettel.brcd.ws.model.output.TaskHistoryOut;
import com.viettel.brcd.ws.model.output.TaskInfoDetailOut;
import com.viettel.brcd.ws.model.output.TaskInfrasDetailOut;
import com.viettel.brcd.ws.model.output.TaskLookForOut;
import com.viettel.brcd.ws.model.output.TaskProgressOut;
import com.viettel.brcd.ws.model.output.TaskToAssignOut;
import com.viettel.brcd.ws.model.output.TaskWarningOut;
import com.viettel.brcd.ws.model.output.UpdateResultOut;
import com.viettel.brcd.ws.model.output.UploadImageOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import java.util.List;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.WebServiceContext;
import com.viettel.brcd.ws.model.output.AccountByStaffOut;
import com.viettel.brcd.ws.model.output.AccountInfrasOut;
import com.viettel.brcd.ws.model.output.PotentialCustomerOut;
import com.viettel.brcd.ws.model.output.TechInforOut;

/**
 * @author vietnn6
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public class CmWS {

    @Resource
    WebServiceContext wsContext;
    static CmController cmController;
    static TargetOnlineController targetOnlineController;
    static RegOnlineController regOnlineController;

    @WebMethod(exclude = true)
    public void setCmController(CmController cmController) {
        this.cmController = cmController;
    }

    @WebMethod(exclude = true)
    public void setTargetOnlineController(TargetOnlineController targetOnlineController) {
        this.targetOnlineController = targetOnlineController;
    }

    @WebMethod(exclude = true)
    public void setRegOnlineController(RegOnlineController regOnlineController) {
        this.regOnlineController = regOnlineController;
    }

    @WebMethod(operationName = "getHelloWorld")
    public String getHelloWorld() {
        return cmController.getHelloWorld();
    }

    @WebMethod(operationName = "doLogin")
    public LoginOut doLogin(@WebParam(name = "userName") String userName, @WebParam(name = "password") String password,
            @WebParam(name = "serial") String serial, @WebParam(name = "locale") String locale) {
        return cmController.login(userName, password, serial, locale);
    }

    @WebMethod(operationName = "getListService")
    public ServiceOut getListService(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getServices(token, locale);
    }

    @WebMethod(operationName = "getListGroup")
    public GroupOut getListGroup(@WebParam(name = "token") String token, @WebParam(name = "serviceId") Long serviceId,
            @WebParam(name = "locale") String locale) {
        return cmController.getProductGroups(token, serviceId, locale);
    }

    @WebMethod(operationName = "getListProduct")
    public ProductOut getListProduct(@WebParam(name = "token") String token, @WebParam(name = "serviceId") Long serviceId,
            @WebParam(name = "groupCode") String groupCode, @WebParam(name = "locale") String locale) {
        return cmController.getProducts(token, serviceId, groupCode, locale);
    }

    @WebMethod(operationName = "getListProvince")
    public ProvinceOut getListProvince(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getProvinces(token, locale);
    }

    @WebMethod(operationName = "getListDistrict")
    public DistrictOut getListDistrict(@WebParam(name = "token") String token, @WebParam(name = "provinceCode") String provinceCode,
            @WebParam(name = "locale") String locale) {
        return cmController.getDistricts(token, provinceCode, locale);
    }

    @WebMethod(operationName = "getListPrecinct")
    public PrecinctOut getListPrecinct(@WebParam(name = "token") String token, @WebParam(name = "provinceCode") String provinceCode,
            @WebParam(name = "districtCode") String districtCode, @WebParam(name = "locale") String locale) {
        return cmController.getPrecincts(token, provinceCode, districtCode, locale);
    }

    @WebMethod(operationName = "getListPayment")
    public PaymentOut getListPayment(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getPaymethods(token, locale);
    }

    @WebMethod(operationName = "getListReasonConnect")
    public ReasonConnectOut getListReasonConnect(@WebParam(name = "token") String token, @WebParam(name = "numPort") Long numPort,
            @WebParam(name = "channelTypeId") Long channelTypeId, @WebParam(name = "province") String province, @WebParam(name = "productCode") String productCode, @WebParam(name = "serviceType") String serviceType,
            @WebParam(name = "locale") String locale, @WebParam(name = "infraType") String infraType, @WebParam(name = "vasProduct") String vasProduct) {
        return cmController.getRegTypes(token, channelTypeId, province, numPort, productCode, serviceType, locale, infraType, vasProduct);
    }

    @WebMethod(operationName = "getListLimitVal")
    public LimitValueOut getListLimitVal(@WebParam(name = "token") String token, @WebParam(name = "serviceType") String serviceType,
            @WebParam(name = "locale") String locale) {
        return cmController.getQuotas(token, serviceType, locale);
    }

    @WebMethod(operationName = "getListDeposit")
    public DepositOut getListDeposit(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getDeposits(token, locale);
    }

    @WebMethod(operationName = "getListPromotion")
    public PromotionOut getListPromotion(@WebParam(name = "token") String token, @WebParam(name = "productCode") String productCode,
            @WebParam(name = "locale") String locale) {
        return cmController.getPromotions(token, productCode, locale);
    }

    @WebMethod(operationName = "getListCustomer")
    public CustomerOut getListCustomer(@WebParam(name = "token") String token, @WebParam(name = "idNo") String idNo,
            @WebParam(name = "isdn") String isdn, @WebParam(name = "account") String account, @WebParam(name = "pageSize") Long pageSize,
            @WebParam(name = "pageNo") Long pageNo, @WebParam(name = "locale") String locale,
            @WebParam(name = "customerName") String customerName, @WebParam(name = "contact") String contact) {
        return cmController.searchCustomer(token, idNo, isdn, account, pageSize, pageNo, locale, customerName, contact);
    }

    @WebMethod(operationName = "getListCustomType")
    public CustomTypeOut getListCustomType(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getBusTypes(token, locale);
    }

    @WebMethod(operationName = "getListIdType")
    public IdTypeOut getListIdType(@WebParam(name = "token") String token, @WebParam(name = "busType") String busType,
            @WebParam(name = "locale") String locale) {
        return cmController.getIdTypes(token, busType, locale);
    }

    @WebMethod(operationName = "getListLineType")
    public LineTypeOut getListLineType(@WebParam(name = "token") String token, @WebParam(name = "serviceType") String serviceType,
            @WebParam(name = "locale") String locale) {
        return cmController.getLineTypes(token, serviceType, locale);
    }

    @WebMethod(operationName = "getListSubTypeService")
    public SubTypeOut getListSubTypeService(@WebParam(name = "token") String token, @WebParam(name = "productCode") String productCode,
            @WebParam(name = "serviceType") String serviceType, @WebParam(name = "locale") String locale) {
        return cmController.getSubTypes(token, productCode, serviceType, locale);
    }

    @WebMethod(operationName = "getCusInforDebit")
    public DebitOut getCusInforDebit(@WebParam(name = "token") String token, @WebParam(name = "custId") Long custId,
            @WebParam(name = "locale") String locale) {
        return cmController.getCustomerDebit(token, custId, locale);
    }

    @WebMethod(operationName = "getTeamCode")
    public ResultTeamCode getTeamCode(@WebParam(name = "token") String token, @WebParam(name = "stationId") Long stationId,
            @WebParam(name = "locale") String locale) {
        return cmController.getTeamCode(token, stationId, locale);
    }

    @WebMethod(operationName = "getSignContractReasons")
    public ReasonOut getSignContractReasons(@WebParam(name = "token") String token, @WebParam(name = "serviceAlias") String serviceAlias,
            @WebParam(name = "locale") String locale) {
        return cmController.getSignContractReasons(token, serviceAlias, locale);
    }

    @WebMethod(operationName = "getNickDomains")
    public DomainOut getNickDomains(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getNickDomains(token, locale);
    }

    @WebMethod(operationName = "getBanks")
    public BankOut getBanks(@WebParam(name = "token") String token, @WebParam(name = "bankCode") String bankCode,
            @WebParam(name = "bankName") String bankName, @WebParam(name = "pageSize") Long pageSize, @WebParam(name = "pageNo") Long pageNo,
            @WebParam(name = "locale") String locale) {
        return cmController.searchBank(token, bankCode, bankName, pageSize, pageNo, locale);
    }

    @WebMethod(operationName = "getNoticeCharges")
    public ApDomainOut getNoticeCharges(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getNoticeCharges(token, locale);
    }

    @WebMethod(operationName = "getPrintMethods")
    public ApDomainOut getPrintMethods(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getPrintMethods(token, locale);
    }

    @WebMethod(operationName = "getListProductPP")
    public ProductPricePlanOut getListProductPP(@WebParam(name = "token") String token, @WebParam(name = "offerId") Long offerId,
            @WebParam(name = "locale") String locale) {
        return cmController.getListProductPP(token, offerId, locale);
    }

    @WebMethod(operationName = "getListLocalProductPP")
    public LocalProductPpOut getListLocalProductPP(@WebParam(name = "token") String token, @WebParam(name = "offerId") Long offerId,
            @WebParam(name = "locale") String locale) {
        return cmController.getListLocalProductPP(token, offerId, locale);
    }

    @WebMethod(operationName = "getMaxLengthSurvey")
    public ApParamOut getMaxLengthSurvey(@WebParam(name = "token") String token, @WebParam(name = "seviceCode") String seviceCode, @WebParam(name = "locale") String locale) {
        return cmController.getMaxLengthSurvey(token, seviceCode, locale);
    }

    @WebMethod(operationName = "getSearchRequestStatus")
    public RequestStatusOut getSearchRequestStatus(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getSearchRequestStatus(token, locale);
    }

    @WebMethod(operationName = "getCancelRequestReasons")
    public ReasonOut getCancelRequestReasons(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getCancelRequestReasons(token, locale);
    }

    @WebMethod(operationName = "searchSubRequest")
    public SearchSubRequestOut searchSubRequest(@WebParam(name = "token") String token,
            @WebParam(name = "searchSubRequestInput") SearchSubRequestInput searchSubRequestInput, @WebParam(name = "locale") String locale) {
        return cmController.searchSubRequest(token, searchSubRequestInput, locale);
    }

    @WebMethod(operationName = "getNextStepInfo")
    public StepInfoOut getNextStepInfo(@WebParam(name = "token") String token, @WebParam(name = "subReqId") Long subReqId,
            @WebParam(name = "locale") String locale) {
        return cmController.getNextStepInfo(token, subReqId, locale);
    }

    @WebMethod(operationName = "getCancelContractReasons")
    public ReasonOut getCancelContractReasons(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getCancelContractReasons(token, locale);
    }

    @WebMethod(operationName = "signContract")
    public WSRespone signContract(@WebParam(name = "token") String token, @WebParam(name = "wsRequest") WSRequest wsRequest,
            @WebParam(name = "locale") String locale) {
        return cmController.signContract(token, wsRequest, locale);
    }

    @WebMethod(operationName = "activeSubscriber")
    public WSRespone activeSubscriber(@WebParam(name = "token") String token, @WebParam(name = "wsRequest") WSRequest wsRequest,
            @WebParam(name = "locale") String locale) {
        return cmController.activeSubscriber(token, wsRequest, locale);
    }

    @WebMethod(operationName = "cancelRequest")
    public WSRespone cancelRequest(@WebParam(name = "token") String token, @WebParam(name = "cancelRequestInput") CancelRequestInput cancelRequestInput,
            @WebParam(name = "locale") String locale) {
        return cmController.cancelRequest(token, cancelRequestInput, locale);
    }

    @WebMethod(operationName = "cancelContract")
    public WSRespone cancelContract(@WebParam(name = "cancelContractInput") CancelContractInput cancelContractInput,
            @WebParam(name = "locale") String locale) {
        return cmController.cancelContract(cancelContractInput, locale);
    }

    @WebMethod(operationName = "addRequestContract")
    public UpdateResultOut addRequestContract(@WebParam(name = "token") String token, @WebParam(name = "input") ContractIn input,
            @WebParam(name = "locale") String locale) {
        return cmController.addRequestContract(token, input, locale);
    }

    /**
     * @author : duyetdk
     * @des: edit set customer house lat long
     * @since 20-01-2018
     */
    @WebMethod(operationName = "setCustomHouseCoord")
    public UpdateResultOut setCustomHouseCoord(@WebParam(name = "token") String token, @WebParam(name = "posX") String posX, @WebParam(name = "posY") String posY,
            @WebParam(name = "customCode") Long customCode, @WebParam(name = "locale") String locale, @WebParam(name = "imageInput") List<ImageInput> lstImageList) {
        return cmController.setCustomHouseCoord(token, customCode, posX, posY, locale, lstImageList);
    }

    @WebMethod(operationName = "getMaxRadius")
    public MaxRadiusOut getMaxRadius(@WebParam(name = "token") String token, @WebParam(name = "seviceCode") String seviceCode, @WebParam(name = "locale") String locale) {
        return cmController.getMaxRadius(token, seviceCode, locale);
    }

    //<editor-fold defaultstate="collapsed" desc="vietnn6 Task management ">
    @WebMethod(operationName = "getListTaskToAssign")
    public TaskToAssignOut getListTaskToAssign(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "searchTaskInput") SearchTaskInput searchTaskInput) {
        return cmController.getListTaskToAssign(token, locale, searchTaskInput);
    }

    @WebMethod(operationName = "getSourceType")
    public SourceTypeOut getSourceType(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "type") int type) {
        return cmController.getSourceType(token, locale, type);
    }

    @WebMethod(operationName = "getTaskProgress")
    public TaskProgressOut getTaskProgress(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "type") int type) {
        return cmController.getTaskProgress(token, locale, type);
    }

    @WebMethod(operationName = "getTaskToAssign")
    public TaskDetailToAssignOut getTaskToAssign(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "taskMngtId") String strTaskMngtId, @WebParam(name = "endDateValue") String endDateValue, @WebParam(name = "assignTo") String strAssignTo) {
        return cmController.getTaskToAssign(token, locale, strTaskMngtId, endDateValue, strAssignTo);
    }

    @WebMethod(operationName = "getStaffForUpdateTask")
    public StaffInfoOut getStaffForUpdateTask(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "shopId") Long shopId) {
        return cmController.getStaffForUpdateTask(token, locale, shopId);
    }

    @WebMethod(operationName = "getServiceForTask")
    public ServiceInfoOut getServiceForTask(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getServiceForTask(token, locale);
    }

    @WebMethod(operationName = "getListTaskForUpdate")
    public TaskForUpdateOut getListTaskForUpdate(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "searchTaskForUpdateInput") SearchTaskForUpdateInput searchTaskInput) {
        return cmController.getListTaskForUpdate(token, locale, searchTaskInput);
    }

    @WebMethod(operationName = "getProgressForUpdateTask")
    public TaskProgressOut getProgressForUpdateTask(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "type") int type) {
        return cmController.getProgressForUpdateTask(token, locale, type);
    }

    @WebMethod(operationName = "getProgressForUpdateInfoTask")
    public TaskProgressOut getProgressForUpdateInfoTask(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "type") int type) {
        return cmController.getProgressForUpdateInfoTask(token, locale, type);
    }

    @WebMethod(operationName = "assignTask")
    public WSRespone assignTask(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "taskToAssignIn") TaskToAssignIn taskToAssignIn) {
        return cmController.assignTask(token, locale, taskToAssignIn);
    }

    @WebMethod(operationName = "getInforTaskToUpdate")
    public InforTaskToUpdateOut getInforTaskToUpdate(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "userLogin") String userLogin,
            @WebParam(name = "taskMngtId") String taskMngtId, @WebParam(name = "staffTaskMngtId") String staffTaskMngtId, @WebParam(name = "telServiceId") String telServiceId) {
        return cmController.getInforTaskToUpdate(token, locale, userLogin, taskMngtId, staffTaskMngtId, telServiceId);
    }

    @WebMethod(operationName = "getInforForResolveComplain")
    public InforResolveComplainOut getInforForResolveComplain(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale,
            @WebParam(name = "custReqId") Long custReqId, @WebParam(name = "taskId") Long taskId) {
        return cmController.getInforForResolveComplain(token, locale, custReqId, taskId);
    }

    @WebMethod(operationName = "updateNoteTask")
    public WSRespone updateNoteTask(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "updateNoteIn") UpdateNoteIn updateNoteIn) {
        return cmController.updateNoteTask(token, locale, updateNoteIn);
    }

    @WebMethod(operationName = "getInforForInfrastructure")
    public InforInfrastructureOut getInforForInfrastructure(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "taskStaffMngtId") Long taskStaffMngtId,
            @WebParam(name = "taskMngtId") Long taskMngtId, @WebParam(name = "telServiceId") Long telServiceId) {
        return cmController.getInforForInfrastructure(token, locale, taskStaffMngtId, taskMngtId, telServiceId);
    }

    @WebMethod(operationName = "getCableBox")
    public CableBoxOut getCableBox(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale,
            @WebParam(name = "boardId") Long boardId, @WebParam(name = "dslamId") Long dslamId, @WebParam(name = "stationId") Long stationId, @WebParam(name = "telServiceId") Long telServiceId, @WebParam(name = "subId") Long subId) {
        return cmController.getCableBox(token, locale, boardId, dslamId, stationId, telServiceId, subId);
    }

    @WebMethod(operationName = "updateInfrastructure")
    public WSRespone updateInfrastructure(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale,
            @WebParam(name = "infrastructureUpdateIn") InfrastructureUpdateIn infrastructureUpdateIn) {
        return cmController.updateInfrastructure(token, locale, infrastructureUpdateIn);
    }

    @WebMethod(operationName = "updateProgress")
    public ResultUpdateTaskOut updateProgress(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "updateTaskIn") UpdateTaskIn updateTaskIn) {
        return cmController.updateProgress(token, locale, updateTaskIn);
    }

    @WebMethod(operationName = "getReasonByReasonGroup")
    public GroupReasonOut getLstReason(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "reasonGroup") String reasonGroup) {
        return cmController.getLstReason(token, locale, reasonGroup);
    }

    @WebMethod(operationName = "getListPortByDevice")
    public PortOut getListPortByDevice(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "dslamId") String dslamId, @WebParam(name = "telServiceId") String telServiceId, @WebParam(name = "subId") String subId, @WebParam(name = "stationId") String stationId) {
        return cmController.getListPortByDevice(token, locale, dslamId, telServiceId, subId, stationId);
    }

    @WebMethod(operationName = "searchTaskForLookUp")
    public TaskLookForOut searchTaskForLookUp(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "searchTaskForLookUpInput") SearchTaskForLookUpInput searchTaskInput) {
        return cmController.searchTaskForLookUp(token, locale, searchTaskInput);
    }

    @WebMethod(operationName = "showDetailInfoTask")
    public TaskInfoDetailOut showDetailInfoTask(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "taskMngtId") String taskMngtId) {
        return cmController.showDetailInfoTask(token, locale, taskMngtId);
    }

    @WebMethod(operationName = "showDetailInfrasTask")
    public TaskInfrasDetailOut showDetailInfrasTask(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "taskMngtId") String taskMngtId) {
        return cmController.showDetailInfrasTask(token, locale, taskMngtId);
    }

    @WebMethod(operationName = "searchHistoryTask")
    public TaskHistoryOut searchHistoryTask(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "startDate") String startDate, @WebParam(name = "endDate") String endDate, @WebParam(name = "taskMngtId") String taskMngtId) {
        return cmController.searchHistoryTask(token, locale, startDate, endDate, taskMngtId);
    }

    @WebMethod(operationName = "showComplaintResult")
    public ComplainResultOut showComplaintResult(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "custReqId") String custReqId) {
        return cmController.showComplaintResult(token, locale, custReqId);
    }

    @WebMethod(operationName = "deleteTask")
    public WSRespone deleteTask(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "strTaskMngtId") String strTaskMngtId, @WebParam(name = "reason") String reason, @WebParam(name = "loginName") String loginName, @WebParam(name = "shopCodeLogin") String shopCodeLogin) {
        return cmController.deleteTask(token, locale, strTaskMngtId, reason, loginName, shopCodeLogin);
    }

    @WebMethod(operationName = "searchTaskForRevoke")
    public SearchTaskRevokeOut searchTaskForRevoke(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "searchTaskRevokeIn") SearchTaskRevokeIn searchTask) {
        return cmController.searchTaskForRevoke(token, locale, searchTask);
    }

    @WebMethod(operationName = "detailRevokeTask")
    public PreRevokeOut detailRevokeTask(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "taskStaffMngtId") String taskStaffMngtId, @WebParam(name = "telServiceId") String telServiceId, @WebParam(name = "revokeFor") String revokeFor) {
        return cmController.detailRevokeTask(token, locale, taskStaffMngtId, telServiceId, revokeFor);
    }

    @WebMethod(operationName = "revokeTask")
    public WSRespone revokeTask(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "revokeTaskIn") RevokeTaskIn revokeTaskIn) {
        return cmController.revokeTask(token, locale, revokeTaskIn);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="vietnn6 merge code VTG">
    @WebMethod(operationName = "createRequestNoInfra")
    public WSRespone createRequestNoInfra(@WebParam(name = "token") String token, @WebParam(name = "custId") Long custId, @WebParam(name = "serviceType") String serviceType,
            @WebParam(name = "Lat") Number Lat, @WebParam(name = "Lng") Number Lng, @WebParam(name = "locale") String locale) {
        return cmController.createRequestNoInfra(token, custId, serviceType, Lat, Lng, locale);
    }

    @WebMethod(operationName = "getListCoordinate")
    public CoordinateOut getListCoordinate(@WebParam(name = "token") String token,
            @WebParam(name = "reqId") Long reqId,
            @WebParam(name = "locale") String locale) {
        return cmController.getListCoordinate(token, reqId, locale);
    }

    @WebMethod(operationName = "getListCoordinateBySubId")
    public CoordinateOut getListCoordinateBySubId(@WebParam(name = "token") String token,
            @WebParam(name = "subId") Long subId,
            @WebParam(name = "locale") String locale) {
        return cmController.getListCoordinateBySubId(token, subId, locale);
    }

    @WebMethod(operationName = "uploadImage")
    public UploadImageOut uploadImage(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "userLogin") String userLogin, @WebParam(name = "subId") Long subId, @WebParam(name = "account") String account, @WebParam(name = "imageInput") List<ImageInput> lstImage) {
        return cmController.uploadImage(token, locale, userLogin, subId, account, lstImage);
    }

    @WebMethod(operationName = "getNotificationTask")
    public NotificationTaskOut getNotificationTask(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "serial") String serial) {
        return cmController.getNotificationTaskOut(token, locale, serial);
    }

    @WebMethod(operationName = "saveLocationAgain")
    public WSRespone saveLocationAgain(@WebParam(name = "token") String token, @WebParam(name = "subId") Long subId, @WebParam(name = "listCoordinate") List<Coordinate> listCoordinate,
            @WebParam(name = "cableLenght") Double cableLenght,
            @WebParam(name = "locale") String locale) {
        return cmController.saveLocationAgain(token, subId, listCoordinate, cableLenght, locale);
    }

    @WebMethod(operationName = "getListWarningTaskForStaff")
    public TaskWarningOut getListWarningTaskForStaff(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "shopId") String shopId, @WebParam(name = "staffId") String staffId) {
        return cmController.getListWarningTaskForStaff(token, locale, shopId, staffId);
    }

    @WebMethod(operationName = "getInvest")
    public InvestOut getInvest(@WebParam(name = "token") String token, @WebParam(name = "reqId") Long reqId, @WebParam(name = "cableLenght") Double cableLenght,
            @WebParam(name = "capacity") Long capacity, @WebParam(name = "boxType") Long boxType,
            @WebParam(name = "locale") String locale) {
        return cmController.getInvest(token, reqId, cableLenght, capacity, boxType, locale);
    }

    @WebMethod(operationName = "getInvestBySubID")
    public InvestOut getInvestBySubID(@WebParam(name = "token") String token, @WebParam(name = "subId") Long subId,
            @WebParam(name = "locale") String locale) {
        return cmController.getInvestBySubID(token, subId, locale);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="vietnn6 Xay dung bai toan 1 app">
    @WebMethod(operationName = "getListReasonConnectPre")
    public ReasonPreConnectOut getListReasonConnectPre(@WebParam(name = "token") String token, @WebParam(name = "userLogin") String user,
            @WebParam(name = "channelTypeId") Long channelTypeId, @WebParam(name = "province") String province, @WebParam(name = "productCode") String productCode, @WebParam(name = "serviceType") String serviceType,
            @WebParam(name = "locale") String locale) {
        return cmController.getRegTypesPre(token, user, channelTypeId, province, productCode, serviceType, locale);
    }

    @WebMethod(operationName = "activeSubscriberPre")
    public WSRespone activeSubscriberPre(@WebParam(name = "token") String token, @WebParam(name = "activePreIn") ActivePreIn wsRequest,
            @WebParam(name = "locale") String locale) {
        return cmController.activeSubscriberPre(token, wsRequest, locale);
    }

    @WebMethod(operationName = "changeSim")
    public WSRespone changeSim(@WebParam(name = "token") String token, @WebParam(name = "changeSimIn") ChangeSimIn wsRequest,
            @WebParam(name = "locale") String locale) {
        return cmController.changeSim(token, wsRequest, locale);
    }

    @WebMethod(operationName = "getListReasonChangeSim")
    public ReasonChangeSimOut getListReasonChangeSim(@WebParam(name = "token") String token, @WebParam(name = "type") String type, @WebParam(name = "isdn") String isdn,
            @WebParam(name = "locale") String locale) {
        return cmController.getListReasonChangeSim(token, isdn, type, locale);
    }

    @WebMethod(operationName = "getInfoSalary")
    public SalaryOut getInfoSalary(@WebParam(name = "token") String token, @WebParam(name = "staffId") String staffId, @WebParam(name = "type") String type,
            @WebParam(name = "locale") String locale) {
        return cmController.getInfoSalary(token, staffId, type, locale);
    }

    @WebMethod(operationName = "getFileByLink")
    public FileOut getFileByLink(@WebParam(name = "token") String token, @WebParam(name = "link") String link,
            @WebParam(name = "locale") String locale) {
        return cmController.getFileByLink(token, link, locale);
    }

    @WebMethod(operationName = "getLinkFilePolicy")
    public LinkFileOut getLinkFilePolicy(@WebParam(name = "token") String token, @WebParam(name = "fromDate") String fromdate, @WebParam(name = "toDate") String toDate, @WebParam(name = "objectType") String objectType, @WebParam(name = "serviceType") String serviceType, @WebParam(name = "formType") String formType,
            @WebParam(name = "locale") String locale) {
        return cmController.getLinkFilePolicy(token, fromdate, toDate, objectType, serviceType, formType, locale);
    }

    @WebMethod(operationName = "getCustomerToPayment")
    public PayContractOut getCustomerToPayment(@WebParam(name = "token") String token, @WebParam(name = "account") String account, @WebParam(name = "idNo") String idNo,
            @WebParam(name = "locale") String locale, @WebParam(name = "customerName") String customerName, @WebParam(name = "contact") String contact) {
        return cmController.getCustomerToPayment(token, account, idNo, locale, customerName, contact);
    }

    @WebMethod(operationName = "getInvoiceList")
    public InvoiceListOut getInvoiceList(@WebParam(name = "token") String token, @WebParam(name = "staffCode") String staffCode, @WebParam(name = "shopCode") String shopCode,
            @WebParam(name = "locale") String locale) {
        return cmController.getInvoiceList(token, staffCode, shopCode, locale);
    }

    @WebMethod(operationName = "searchServicePayment")
    public ServicePaymentOut searchServicePayment(@WebParam(name = "token") String token, @WebParam(name = "staffId") Long staffId, @WebParam(name = "invoiceListId") Long invoiceListId, @WebParam(name = "contractId") Long contractId,
            @WebParam(name = "locale") String locale) {
        return cmController.searchServicePayment(token, staffId, invoiceListId, contractId, locale);
    }

    @WebMethod(operationName = "paymentForService")
    public PayAdvanceBillOut paymentForService(@WebParam(name = "token") String token, @WebParam(name = "paymentIn") PaymentInput payIn, @WebParam(name = "locale") String locale) {
        return cmController.paymentForService(token, payIn, locale);
    }
//    </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="vietnn6 xay dung tinh nang moi tim kiem khach hang">

//    @WebMethod(operationName = "getCustomerByLocation")
//    public CusLocationOut getCustomerByLocation(@WebParam(name = "token") String token, @WebParam(name = "lat") String lat, @WebParam(name = "lng") String lng, @WebParam(name = "province") String province, @WebParam(name = "district") String district, @WebParam(name = "precinct") String precinct, @WebParam(name = "distance") Long distance, @WebParam(name = "name") String name, @WebParam(name = "locale") String locale) {
//        return cmController.findCusByLocation(token, lat, lng, province, district, precinct, distance, name, locale);
//    }
//
//    @WebMethod(operationName = "uploadImageCus")
//    public WSRespone uploadImageCus(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "imageInput") ImageCusInput imageInput) {
//        return cmController.uploadImageCus(token, locale, imageInput);
//    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="view image contract">
    @WebMethod(operationName = "viewImageContract")
    public ImageRespone viewImageContract(@WebParam(name = "account") String account) {
        return cmController.viewImageContract(account);
    }
    //</editor-fold>
    // luu log client

    @WebMethod(operationName = "syncTransLogClientMBCCS")
    public WSRespone syncTransLogClientMBCCS(
            @WebParam(name = "lstLog") List<TransLogsClient> lstLog) throws Exception {
        return cmController.syncTransLogClient(lstLog);
    }
    // login for IOS

    @WebMethod(operationName = "doLoginEncryptRSA")
    public LoginOut doLoginEncryptRSA(@WebParam(name = "userName") String userName, @WebParam(name = "password") String password, @WebParam(name = "locale") String locale, @WebParam(name = "deviceId") String deviceId) {
        return cmController.loginEncryptRSA(userName, password, locale, deviceId);
    }
    //<editor-fold defaultstate="collapsed" desc="upgrade customer management">

    @WebMethod(operationName = "addCustomInfor")
    public UpdateResultOut addCustomInfor(@WebParam(name = "token") String token, @WebParam(name = "customer") CustomerIn customer,
            @WebParam(name = "staffCode") String staffCode, @WebParam(name = "lstImage") List<ImageInput> lstImage, @WebParam(name = "locale") String locale) {
        return cmController.addCustomer(token, customer, locale, lstImage, staffCode);
    }

    @WebMethod(operationName = "viewImageCustomer")
    public ImageRespone viewImageCustomer(@WebParam(name = "custId") String custId) {
        return cmController.viewImageCustomer(custId);
    }

    @WebMethod(operationName = "getCodeAuthenCusByIsdn")
    public WSRespone getCodeAuthenCusByIsdn(
            @WebParam(name = "isdn") String isdn, @WebParam(name = "serial") String serial) throws Exception {
        return cmController.getCodeAuthenCusByIsdn(isdn, serial);
    }

    @WebMethod(operationName = "checkCodeAuthenCusBySerial")
    public WSRespone checkCodeAuthenCusBySerial(
            @WebParam(name = "code") String code, @WebParam(name = "serial") String serial) throws Exception {
        return cmController.checkCodeAuthenCusBySerial(code, serial);
    }

    @WebMethod(operationName = "checkIdNoCus")
    public WSRespone checkIdNoCus(
            @WebParam(name = "idNo") String idNo) throws Exception {
        return cmController.checkIdNoCus(idNo);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="add customer for prepaid">

    @WebMethod(operationName = "getListCustomerPre")
    public CustomerOut getListCustomerPre(@WebParam(name = "token") String token, @WebParam(name = "idNo") String idNo,
            @WebParam(name = "isdn") String isdn, @WebParam(name = "service") String service, @WebParam(name = "locale") String locale) {
        return cmController.getListCustomerPre(token, idNo, isdn, service, locale);
    }

    @WebMethod(operationName = "addCustomInforPre")
    public UpdateResultOut addCustomInforPre(@WebParam(name = "token") String token, @WebParam(name = "customer") CustomerIn customer,
            @WebParam(name = "staffCode") String staffCode, @WebParam(name = "lstImage") List<ImageInput> lstImage, @WebParam(name = "locale") String locale) {
        return cmController.addCustomerPre(token, customer, locale, lstImage, staffCode);
    }

    @WebMethod(operationName = "getListCustomTypePre")
    public CustomTypeOut getListCustomTypePre(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getBusTypesPre(token, locale);
    }

    @WebMethod(operationName = "checkIdNoCusPre")
    public WSRespone checkIdNoCusPre(
            @WebParam(name = "idNo") String idNo) throws Exception {
        return cmController.checkIdNoCusPre(idNo);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="View serial">

    @WebMethod(operationName = "getRangeSerial")
    public SerialOut getRangeSerial(@WebParam(name = "token") String token, @WebParam(name = "shopId") Long shopId, @WebParam(name = "stockModelId") Long stockModelId, @WebParam(name = "locale") String locale) {
        return cmController.getRangeSerial(token, shopId, stockModelId, locale);
    }
    //</editor-fold>

    // change pass for corboratore
    @WebMethod(operationName = "changePasswordOnBccs")
    public WSRespone changePasswordOnBccs(@WebParam(name = "isdn") String isdn, @WebParam(name = "oldPassword") String oldPassword, @WebParam(name = "newPassword") String newPassword, @WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.changePasswordOnBccs(isdn, oldPassword, newPassword, token, locale);
    }

    /**
     * @author : Cuongdm
     * @des: Lay danh sach reason chuc nang Pay Advance
     *
     * @param token
     * @param locale
     * @param isdn
     * @param serviceType
     * @return
     */
    @WebMethod(operationName = "getReasonPayAdvance")
    public ReasonOut getReasonPayAdvance(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "contractId") Long contractId, @WebParam(name = "isdn") String isdn) {
        return cmController.getReasonPayAdvance(token, locale, contractId, isdn);
    }

    /**
     * @author : Cuongdm
     * @des: Thuc hien Pay Advance
     * @param token
     * @param locale
     * @param reasonId
     * @param staffId
     * @param contractId
     * @param invoiceListId
     * @return
     */
    @WebMethod(operationName = "payAdvance")
    public PayAdvanceBillOut payAdvance(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "reasonId") Long reasonId, @WebParam(name = "staffId") Long staffId, @WebParam(name = "contractId") Long contractId, @WebParam(name = "invoiceListId") Long invoiceListId, @WebParam(name = "isdn") String isdn) {
        return cmController.payAdvance(token, locale, reasonId, staffId, contractId, invoiceListId, isdn);
    }

    /**
     * @author : duyetdk
     * @des: get list request update location
     * @since 20-01-2018
     */
    @WebMethod(operationName = "getListRequestUpdateCusLocation")
    public RequestUpdateLocationOut getListRequestUpdateCusLocation(@WebParam(name = "token") String token, @WebParam(name = "date") String date,
            @WebParam(name = "status") Long status, @WebParam(name = "duration") Long duration, @WebParam(name = "pageNo") Long pageNo,
            @WebParam(name = "locale") String locale, @WebParam(name = "createUser") String createUser, @WebParam(name = "custName") String custName) {
        return cmController.getListRequestUpdateCusLocation(token, date, status, duration, pageNo, locale, createUser, custName);
    }

    /**
     * @author : duyetdk
     * @des: reject or aprrove request update customer location
     * @since 20-01-2018
     */
    @WebMethod(operationName = "rejectOrAprroveRUL")
    public WSRespone rejectOrAprroveRUL(@WebParam(name = "token") String token, @WebParam(name = "cancelRULInput") CancelRULInput cancelRULInput, @WebParam(name = "locale") String locale) {
        return cmController.rejectOrAprroveRUL(token, cancelRULInput, locale);
    }

    /**
     * @author : duyetdk
     * @des: get list times of change
     * @since 20-01-2018
     */
    @WebMethod(operationName = "timesOfChange")
    public RequestUpdateLocationOut getListTimesOfChange(@WebParam(name = "token") String token, @WebParam(name = "custId") Long custId,
            @WebParam(name = "pageNo") Long pageNo, @WebParam(name = "locale") String locale) {
        return cmController.getListTimesOfChange(token, custId, pageNo, locale);
    }

    /**
     * @author : duyetdk
     * @des: approve all
     * @since 20-01-2018
     */
    @WebMethod(operationName = "approveAll")
    public WSRespone approveAll(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "date") String date,
            @WebParam(name = "duration") Long duration, @WebParam(name = "createUser") String createUser, @WebParam(name = "custName") String custName) {
        return cmController.approveAll(token, locale, date, duration, createUser, custName);
    }

    /**
     * @author : duyetdk
     * @des: reject or approve request on subFbConfig
     * @param token
     * @param cancelSubFbConfigInput (object)
     * @param locale
     * @since 08-03-2018
     */
    @WebMethod(operationName = "rejectOrApproveSubFbConfig")
    public WSRespone rejectOrApproveSubFbConfig(@WebParam(name = "token") String token, @WebParam(name = "cancelSubFbConfigInput") CancelSubFbConfigInput cancelSubFbConfigInput, @WebParam(name = "locale") String locale) {
        return cmController.rejectOrApproveSubFbConfig(token, cancelSubFbConfigInput, locale);
    }

    /**
     * @author : duyetdk
     * @des: get list from subFbConfig
     * @param token
     * @param status
     * @param statusConfig
     * @param duration
     * @param account
     * @param createUser
     * @param locale
     * @param pageNo
     * @param province
     * @since 15-03-2018
     */
    @WebMethod(operationName = "getListSubFbConfig")
    public SubFbConfigOut getListSubFbConfig(@WebParam(name = "token") String token, @WebParam(name = "status") Long status,
            @WebParam(name = "statusConfig") Long statusConfig, @WebParam(name = "duration") Long duration, @WebParam(name = "account") String account,
            @WebParam(name = "createUser") String createUser, @WebParam(name = "locale") String locale, @WebParam(name = "pageNo") Long pageNo, @WebParam(name = "province") String province) {
        return cmController.getListSubFbConfig(token, status, statusConfig, duration, account, createUser, locale, pageNo, province);
    }

    /**
     * @author : duyetdk
     * @des: get info when send request config
     * @param token
     * @param taskMngtId
     * @param account
     * @param locale
     * @since 15-03-2018
     */
    @WebMethod(operationName = "getLastUpdateConfig")
    public LastUpdateConfigOut getLastUpdateConfig(@WebParam(name = "token") String token, @WebParam(name = "taskMngtId") Long taskMngtId, @WebParam(name = "account") String account, @WebParam(name = "locale") String locale) {
        return cmController.getLastUpdateConfig(token, taskMngtId, account, locale);
    }

    /**
     * @author : duyetdk
     * @des: get list reason config
     * @param token
     * @param locale
     * @since 15-03-2018
     */
    @WebMethod(operationName = "getListReasonConfig")
    public ReasonConfigOut getListReasonConfig(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getListReasonConfig(token, locale);
    }

    /**
     * @author : duyetdk
     * @des: get list history config
     * @param token
     * @param id
     * @param locale
     * @since 15-03-2018
     */
    @WebMethod(operationName = "getListHistoryConfig")
    public HistorySubFbConfigOut getListHistoryConfig(@WebParam(name = "token") String token, @WebParam(name = "id") Long id, @WebParam(name = "locale") String locale) {
        return cmController.getListHistoryConfig(token, id, locale);
    }

    /**
     * @author : duyetdk
     * @des: get list image type
     * @param token
     * @param locale
     * @since 15-03-2018
     */
    @WebMethod(operationName = "getListImageType")
    public ImageInfrastructureOut getListImageType(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "action") String action, @WebParam(name = "taskMngtId") Long taskMngtId) {
        return cmController.getListImageType(token, locale, action, taskMngtId);
    }

    /**
     *
     * @author: cuongdm
     * @des: get info for dashboard broadband
     * @param token
     * @param taskMngtId
     * @param account
     * @return
     */
    @WebMethod(operationName = "getDashboardBroadbandInfo")
    public DashboardBroadbanTab getDashboardBroadbandInfo(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffId") Long staffId,
            @WebParam(name = "shopId") Long shopId,
            @WebParam(name = "isManagement") String isManagement) {
        return cmController.getDashboardBroadbandInfo(token, locale, staffId, shopId, isManagement);
    }

    /**
     *
     * @author: cuongdm
     * @des: config connect IMT
     * @param token
     * @param taskMngtId
     * @param account
     * @return
     */
    @WebMethod(operationName = "configConnectImt")
    public WSRespone configConnectImt(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffId") Long staffId,
            @WebParam(name = "splitter") String splitter,
            @WebParam(name = "taskManagementId") Long taskManagementId) {
        return cmController.configConnectImt(token, locale, staffId, taskManagementId, splitter);
    }

    @WebMethod(operationName = "getInfoInfraToView")
    public InforInfrastructureOut getInfoInfraToView(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "taskMngtId") Long taskMngtId) {
        return cmController.getInfoInfraToView(token, locale, taskMngtId);
    }

    /**
     * @author : duyetdk
     * @des: get info staff
     * @param token
     * @param staffCode
     * @param locale
     * @since 05-04-2018
     */
    @WebMethod(operationName = "getStaffInfo")
    public StaffDetailOut getStaffInfo(@WebParam(name = "token") String token, @WebParam(name = "staffCode") String staffCode, @WebParam(name = "locale") String locale) {
        return cmController.getStaffInfo(token, staffCode, locale);
    }

    @WebMethod(operationName = "getImageRequestLocationCustomer")
    public ParamOut getImageLocationCustomer(@WebParam(name = "requestId") Long requestId, @WebParam(name = "typeImage") String typeImage) {
        return cmController.getImageRequestLocationCustomer(requestId, typeImage);
    }

    @WebMethod(operationName = "getCCActionDetail")
    public ParamOut getCCActionDetail(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "account") String account, @WebParam(name = "actionType") String actionType) {
        return cmController.getCCActionDetail(token, locale, account, actionType);
    }

    @WebMethod(operationName = "getAccountOfCustomer")
    public ParamOut getAccountOfCustomer(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "customerId") Long customerId) {
        return cmController.getAccountOfCustomer(token, locale, customerId);
    }

    @WebMethod(operationName = "getActionDetailType")
    public ParamOut getActionDetailType(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getActionDetailType(token, locale);
    }

    @WebMethod(operationName = "sendNimsSubActiveInfo")
    public WSRespone sendNimsSubActiveInfo(@WebParam(name = "staffId") Long staffId, @WebParam(name = "taskManagementId") Long taskManagementId) {
        return cmController.sendNimsSubActiveInfo(staffId, taskManagementId);
    }

    @WebMethod(operationName = "changeStationInfraStructure")
    public WSRespone changeStationInfraStructure(
            @WebParam(name = "staffId") Long staffId,
            @WebParam(name = "newStation") String newStation,
            @WebParam(name = "connector") String connector,
            @WebParam(name = "portNo") String portNo,
            @WebParam(name = "account") String account) {
        return cmController.changeStationInfraStructure(staffId, newStation, connector, portNo, account);
    }

    @WebMethod(operationName = "getStationByTeam")
    public ParamOut getStationByTeam(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "account") String account,
            @WebParam(name = "subId") Long subId,
            @WebParam(name = "staffId") Long staffId) {
        return cmController.getStationByTeam(token, locale, account, subId, staffId);
    }

    @WebMethod(operationName = "getPortOdf")
    public ParamOut getPortOdf(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "odfId") Long odfId) {
        return cmController.getPortOdf(token, locale, odfId);
    }

    @WebMethod(operationName = "getStationInfoOfAccount")
    public StationInfo getStationInfoOfAccount(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "subId") Long subId) {
        return cmController.getStationInfoOfAccount(token, locale, subId);
    }

    @WebMethod(operationName = "getStaffofShop")
    public StaffInfoOut getStaffofShop(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "shopId") Long shopId) {
        return cmController.getStaffofShop(token, locale, shopId);
    }

    @WebMethod(operationName = "changeServiceAtoF")
    public WSRespone changeServiceAtoF(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "account") String account,
            @WebParam(name = "loginName") String loginName,
            @WebParam(name = "shopCodeLogin") String shopCodeLogin) {
        return cmController.changeServiceAtoF(token, locale, account, loginName, shopCodeLogin);
    }

    @WebMethod(operationName = "getStaffMapconnector")
    public ParamOut getStaffMapconnector(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale) {
        return cmController.getStaffMapconnector(token, locale);
    }

    @WebMethod(operationName = "getStaffSalaryDetail")
    public StaffSalaryDetail getStaffSalaryDetail(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffCode") String staffCode,
            @WebParam(name = "month") int month,
            @WebParam(name = "year") int year) {
        return cmController.getStaffSalaryDetail(token, locale, staffCode, month, year);
    }

    @WebMethod(operationName = "getKpiApParam")
    public ParamOut getKpiApParam(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale) {
        return cmController.getKpiApParam(token, locale);
    }

    @WebMethod(operationName = "getBrasIpPool")
    public ParamOut getBrasIpPool(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "account") String account) {
        return cmController.getBrasIpPool(token, locale, account);
    }

    @WebMethod(operationName = "getNotificationLog")
    public ParamOut getNotificationLog(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffId") Long staffId) {
        return cmController.getNotificationLog(token, locale, staffId);
    }

    @WebMethod(operationName = "getListOwnerSabay")
    public OwnerOut getListOwnerSabay(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "msisdn_n3_p3") String msisdn_n3_p3,
            @WebParam(name = "action_type") String action_type) {
        return cmController.getOwnerSabay(token, locale, msisdn_n3_p3, action_type);
    }

    @WebMethod(operationName = "getListMemberOwnerSabay")
    public OwnerOut getListMemberOwnerSabay(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "msisdn_n3_p3") String msisdn_n3_p3,
            @WebParam(name = "msisdn") String msisdn) {
        return cmController.getMemberOwnerSabay(token, locale, msisdn_n3_p3, msisdn);
    }

    @WebMethod(operationName = "getOTP_Sabay")
    public OwnerOut getOTP_Sabay(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "msisdn_n3_p3") String msisdn_n3_p3,
            @WebParam(name = "msisdn") String msisdn) {
        return cmController.getOTP_Sabay(token, locale, msisdn_n3_p3, msisdn);
    }

    @WebMethod(operationName = "sendOTP_Sabay")
    public OwnerOut sendOTP_Sabay(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "msisdn_n3_p3") String msisdn_n3_p3,
            @WebParam(name = "msisdn") String msisdn,
            @WebParam(name = "members") String members,
            @WebParam(name = "otp") String otp) {
        return cmController.sendOTP_Sabay(token, locale, msisdn_n3_p3, msisdn, members, otp);
    }

    @WebMethod(operationName = "CheckOwner_Sabay")
    public OwnerOut CheckOwner_Sabay(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "msisdn_n3_p3") String msisdn_n3_p3,
            @WebParam(name = "msisdn") String msisdn) {
        return cmController.CheckOwner_Sabay(token, locale, msisdn_n3_p3, msisdn);
    }

    @WebMethod(operationName = "Checkmember_Sabay")
    public OwnerOut Checkmember_Sabay(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "msisdn_n3_p3") String msisdn_n3_p3,
            @WebParam(name = "msisdn") String msisdn,
            @WebParam(name = "param") String param) {
        return cmController.Checkmember_Sabay(token, locale, msisdn_n3_p3, msisdn, param);
    }

    @WebMethod(operationName = "topUp")
    public ParamOut topUp(
            @WebParam(name = "token") String token,
            @WebParam(name = "encrypt") String encrypt,
            @WebParam(name = "signature") String signature) {
        return cmController.topUp(token, encrypt, signature);
    }

    @WebMethod(operationName = "getPinCode")
    public ParamOut getPinCode(
            @WebParam(name = "token") String token,
            @WebParam(name = "encrypt") String encrypt,
            @WebParam(name = "signature") String signature) {
        return cmController.getPinCode(token, encrypt, signature);
    }

    @WebMethod(operationName = "getToken")
    public LoginOut getToken(@WebParam(name = "account") String userName, @WebParam(name = "password") String password) {
        return cmController.getToken(userName, password);
    }

    @WebMethod(operationName = "verifyChangeSim")
    public ParamOut verifyChangeSim(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "msisdnAgent") String msisdnAgent,
            @WebParam(name = "msisdnCust") String msisdnCust,
            @WebParam(name = "lastSerial") String lastSerial) {
        return cmController.verifyChangeSim(token, locale, msisdnAgent, msisdnCust, lastSerial);
    }

    @WebMethod(operationName = "verifyChangeKit")
    public WSRespone verifyChangeKit(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "msisdnAgent") String msisdnAgent,
            @WebParam(name = "msisdnCust") String msisdnCust,
            @WebParam(name = "newMsisdnCust") String newMsisdnCust,
            @WebParam(name = "lastSerial") String lastSerial) {
        return cmController.verifyChangeKit(token, locale, msisdnAgent, msisdnCust, newMsisdnCust, lastSerial);
    }

    @WebMethod(operationName = "changeSim4G")
    public WSRespone changeSim4G(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "pinCode") String pinCode,
            @WebParam(name = "transId") String transId) {
        return cmController.changeSim4G(token, locale, pinCode, transId);
    }

    @WebMethod(operationName = "createNewInfoCus")
    public WSRespone createNewInfoCus(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "serial") String serial,
            @WebParam(name = "isdn") String isdn,
            @WebParam(name = "dealerIsdn") String dealerIsdn,
            @WebParam(name = "idType") String idType,
            @WebParam(name = "fullName") String fullName,
            @WebParam(name = "idNumber") String idNumber,
            @WebParam(name = "dob") String dob,
            @WebParam(name = "puk") String puk,
            @WebParam(name = "lstImage") List<ImageInput> lstImage) {
        return cmController.createNewInfoCus(token, locale, serial, isdn, dealerIsdn, idType, fullName, idNumber, dob, lstImage, puk);
    }

    @WebMethod(operationName = "confirmPincode")
    public ParamOut confirmPincode(
            @WebParam(name = "token") String token,
            @WebParam(name = "encrypt") String encrypt,
            @WebParam(name = "signature") String signature) {
        return cmController.confirmPincode(token, encrypt, signature);
    }

    @WebMethod(operationName = "confirmTopUp")
    public ParamOut confirmTopUp(
            @WebParam(name = "token") String token,
            @WebParam(name = "requestId") String requestId,
            @WebParam(name = "date") String date) {
        return cmController.confirmTopUp(token, requestId, date);
    }

    @WebMethod(operationName = "getAccountInformation")
    public ParamOut getAccountInformation(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "account") String account) {
        return cmController.getAccountInformation(token, locale, account);
    }

    @WebMethod(operationName = "updateRequestAnalysSOC")
    public ParamOut updateRequestAnalysSOC(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "complaintId") Long complainId,
            @WebParam(name = "result") Long result,
            @WebParam(name = "description") String description,
            @WebParam(name = "listErrorCode") List<String> listErrorCode) {
        return cmController.updateRequestAnalysSOC(token, locale, complainId, result, description, listErrorCode);
    }

    @WebMethod(operationName = "checkSocComplaint")
    public WSRespone checkSocComplaint(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "taskStaffMngtId") Long taskStaffMngtId,
            @WebParam(name = "progress") String progress,
            @WebParam(name = "staffCode") String staffCode) {
        return cmController.checkSocComplaint(token, locale, taskStaffMngtId, progress, staffCode);
    }

    /**
     * @author: duyetdk
     * @since 8/3/2019
     * @des: tim kiem danh sach giao dich chua nop tien
     * @param token
     * @param fromDate
     * @param toDate
     * @param staffId
     * @param locale
     * @return
     */
    @WebMethod(operationName = "getListDebitTransaction")
    public DebitTransOut getListDebitTransaction(
            @WebParam(name = "token") String token,
            @WebParam(name = "fromDate") String fromDate,
            @WebParam(name = "toDate") String toDate,
            @WebParam(name = "staffId") Long staffId,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "currency") String currency) {
        return cmController.getListDebitTransaction(fromDate, toDate, staffId, locale, token, currency);
    }

    /**
     * @author: duyetdk
     * @since 9/3/2019
     * @des: lay danh sach cong no cua nhan vien
     * @param token
     * @param staffCode
     * @param shopCode
     * @param status
     * @param staffId
     * @param shopId
     * @param locale
     * @return
     */
    @WebMethod(operationName = "getListDebitStaff")
    public DebitStaffOut getListDebitStaff(
            @WebParam(name = "token") String token,
            @WebParam(name = "staffCode") String staffCode,
            @WebParam(name = "shopCode") String shopCode,
            @WebParam(name = "status") Long status,
            @WebParam(name = "staffId") Long staffId,
            @WebParam(name = "shopId") Long shopId,
            @WebParam(name = "locale") String locale) {
        return cmController.getListDebitStaff(shopCode, staffCode, staffId, shopId, status, locale, token);
    }

    /**
     * @author: duyetdk
     * @since 11/3/2019
     * @des: lay lich su giao dich tung nhan vien
     * @param token
     * @param payCode
     * @param payMethod
     * @param status
     * @param pageNo
     * @param staffId
     * @param locale
     * @return
     */
    @WebMethod(operationName = "transHistory")
    public DebitTransHistoryOut transHistory(
            @WebParam(name = "token") String token,
            @WebParam(name = "payCode") String payCode,
            @WebParam(name = "payMethod") String payMethod,
            @WebParam(name = "status") Long status,
            @WebParam(name = "staffId") Long staffId,
            @WebParam(name = "pageNo") Long pageNo,
            @WebParam(name = "locale") String locale) {
        return cmController.transHistory(payCode, payMethod, staffId, status, pageNo, locale, token);
    }

    /**
     * @author: duyetdk
     * @since 12/3/2019
     * @des: tao yeu cau thanh toan
     * @param token
     * @param debitTransId
     * @param phoneNumber
     * @param staffId
     * @param amount
     * @param locale
     * @return
     */
    @WebMethod(operationName = "clearDebitTrans")
    public ClearDebitTransOut clearDebitTrans(
            @WebParam(name = "token") String token,
            @WebParam(name = "debitTransId") String debitTransId,
            @WebParam(name = "staffId") Long staffId,
            @WebParam(name = "amount") Double amount,
            @WebParam(name = "locale") String locale) {
        return cmController.clearDebitTrans(token, debitTransId, staffId, amount, locale);
    }

    /**
     * @author: duyetdk
     * @since 13/3/2019
     * @des: xac nhan giao dich sau khi Clear Debit
     * @param paymentContent
     * @param currencyCode
     * @param txPaymentTokenId
     * @param token
     * @param debitTransId
     * @param amount
     * @param staffId
     * @param locale
     * @param paymentCode
     * @return
     */
    @WebMethod(operationName = "confirmDebitTrans")
    public ClearDebitTransOut confirmDebitTrans(
            @WebParam(name = "token") String token,
            @WebParam(name = "debitTransId") String debitTransId,
            @WebParam(name = "amount") Double amount,
            @WebParam(name = "txPaymentTokenId") String txPaymentTokenId,
            @WebParam(name = "paymentCode") String paymentCode,
            @WebParam(name = "staffId") Long staffId,
            @WebParam(name = "currencyCode") String currencyCode,
            @WebParam(name = "paymentContent") String paymentContent,
            @WebParam(name = "locale") String locale) {
        return cmController.confirmDebitTrans(token, debitTransId, amount, paymentCode,
                txPaymentTokenId, currencyCode, paymentContent, staffId, locale);
    }

    /**
     * @author: duyetdk
     * @since 14/3/2019
     * @des: nhan no tu nhan vien khac
     * @param token
     * @param debitTransId
     * @param amount
     * @param paymentCode
     * @param staffId
     * @param receiveDebtStaffId
     * @param locale
     * @return
     */
    @WebMethod(operationName = "transferDebit")
    public ClearDebitTransOut transferDebit(
            @WebParam(name = "token") String token,
            @WebParam(name = "debitTransId") String debitTransId,
            @WebParam(name = "amount") Double amount,
            @WebParam(name = "paymentCode") String paymentCode,
            @WebParam(name = "staffId") Long staffId,
            @WebParam(name = "receiveDebtStaffId") Long receiveDebtStaffId,
            @WebParam(name = "locale") String locale) {
        return cmController.transferDebit(token, debitTransId, amount, paymentCode, receiveDebtStaffId, staffId, locale);
    }

    /**
     * @author: duyetdk
     * @since 1/4/2019
     * @des: check giao dich phat sinh
     * @param token
     * @param amount
     * @param staffId
     * @param locale
     * @return
     */
    @WebMethod(operationName = "checkLockTransaction")
    public LockDebitTransOut checkLockTransaction(
            @WebParam(name = "token") String token,
            @WebParam(name = "amount") Double amount,
            @WebParam(name = "staffId") Long staffId,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "currency") String currency) {
        return cmController.checkLockTransaction(token, amount, staffId, locale, currency);
    }

    @WebMethod(operationName = "checkInforBeforeChangeSim")
    public ParamOut checkInforBeforeChangeSim(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "isdn") String isdn,
            @WebParam(name = "idNo") String idNo) {
        return cmController.checkInforBeforeChangeSim(token, locale, isdn, idNo);
    }

    @WebMethod(operationName = "getReasonChangeSim")
    public ParamOut getReasonChangeSim(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "shopId") Long shopId,
            @WebParam(name = "isdn") String isdn) {
        return cmController.getReasonChangeSim(token, locale, shopId, isdn);
    }

    @WebMethod(operationName = "changeSimPre")
    public ParamOut changeSimPre(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "changeSimInput") ChangeSimInput changeSimInput) {
        return cmController.changeSimPre(token, locale, changeSimInput);
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019 lay danh sach loai complaint duoc accept
     * @param token
     * @param locale
     * @param code
     * @return
     */
    @WebMethod(operationName = "getListComplaintAcceptType")
    public ComplaintAcceptTypeOut getListComplaintAcceptType(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "code") String code) {
        return cmController.getListComplaintAcceptType(token, locale, code);
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019
     * @des lay danh sach nguon complaint
     * @param token
     * @param locale
     * @return
     */
    @WebMethod(operationName = "getListComplaintAcceptSource")
    public ComplaintAcceptSourceOut getListComplaintAcceptSource(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale) {
        return cmController.getListComplaintAcceptSource(token, locale);
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019 lay danh sach loai complaint theo groupId
     * @param token
     * @param locale
     * @param groupId
     * @return
     */
    @WebMethod(operationName = "getListComplaintType")
    public ComplaintTypeOut getListComplaintType(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "groupId") Long groupId) {
        return cmController.getListComplaintType(groupId, locale, token);
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019
     * @des lay danh sach loai nhom complaint theo parentId
     * @param token
     * @param locale
     * @param parentId
     * @return
     */
    @WebMethod(operationName = "getListComplaintGroupType")
    public ComplaintGroupOut getListComplaintGroupType(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "parentId") Long parentId) {
        return cmController.getListComplaintGroupType(parentId, locale, token);
    }

    /**
     * @author: duyetdk
     * @since 9/4/2019
     * @des lay danh sach nhom complaint theo comp_level_id
     * @param token
     * @param locale
     * @return
     */
    @WebMethod(operationName = "getListComplaintGroup")
    public ComplaintGroupOut getListComplaintGroup(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale) {
        return cmController.getListComplaintGroup(locale, token);
    }

    /**
     * @author: duyetdk
     * @since 18/4/2019 get team giai quyet su co
     * @param token
     * @param locale
     * @param subId
     * @return
     */
    @WebMethod(operationName = "getListCompTeam")
    public CompDepartmentGroupOut getListCompTeam(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "subId") Long subId) {
        return cmController.getListCompTeam(locale, token, subId);
    }

    /**
     * @author: duyetdk
     * @since 18/4/2019
     * @des tiep nhan khieu nai
     * @param token
     * @param complaintInput
     * @param locale
     * @return
     */
    @WebMethod(operationName = "submitComplaint")
    public WSRespone submitComplaint(@WebParam(name = "token") String token,
            @WebParam(name = "complaintInput") ComplaintInput complaintInput,
            @WebParam(name = "locale") String locale) {
        return cmController.submitComplaint(token, complaintInput, locale);
    }

    /**
     * @author: duyetdk
     * @since 18/4/2019
     * @des lay thong tin subAdslLL
     * @param token
     * @param locale
     * @param isdn
     * @return
     */
    @WebMethod(operationName = "getListSubAdslLeaseline")
    public SubAdslLeaselineOut getListSubAdslLeaseline(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "isdn") String isdn) {
        return cmController.getListSubAdslLeaseline(locale, token, isdn);
    }

    @WebMethod(operationName = "getSubInfoForPayment")
    public ServicePaymentOut getSubInfoForPayment(
            @WebParam(name = "isdn") String isdn) {
        return cmController.getSubInfoForPayment(isdn);
    }

    @WebMethod(operationName = "payAdvanceSub")
    public ParamOut payAdvanceSub(
            @WebParam(name = "staffCode") String staffCode, @WebParam(name = "encrypt") String encrypt, @WebParam(name = "signature") String signature) {
        return cmController.payAdvanceSub(staffCode, encrypt, signature);
    }

    @WebMethod(operationName = "confirmPayAdvance")
    public ParamOut confirmPayAdvance(
            @WebParam(name = "staffCode") String staffCode, @WebParam(name = "tid") String tid, @WebParam(name = "date") String date) {
        return cmController.confirmPayAdvance(staffCode, tid, date);
    }

    /**
     * @author: duyetdk
     * @since 12/5/2019
     * @des lay thong tin nhom san pham
     * @param token
     * @param telServiceId
     * @param locale
     * @return
     */
    @WebMethod(operationName = "getListProductGroupPre")
    public GroupOut getListProductGroupPre(
            @WebParam(name = "token") String token,
            @WebParam(name = "telServiceId") Long telServiceId,
            @WebParam(name = "locale") String locale) {
        return cmController.getProductGroups(token, telServiceId, locale);
    }

    /**
     * @author: duyetdk
     * @since 12/5/2019
     * @des lay thong tin loai san pham
     * @param token
     * @param telServiceId
     * @param productGroup
     * @param locale
     * @return
     */
    @WebMethod(operationName = "getListProductPre")
    public ProductOut getListProductPre(
            @WebParam(name = "token") String token,
            @WebParam(name = "telServiceId") Long telServiceId,
            @WebParam(name = "productGroup") String productGroup,
            @WebParam(name = "locale") String locale) {
        return cmController.getListProductPre(token, telServiceId, productGroup, locale);
    }

    /**
     * @author: duyetdk
     * @since 12/5/2019
     * @des lay ly do dau noi
     * @param token
     * @param userNameLogin
     * @param channelTypeId
     * @param provinceCode
     * @param productCode
     * @param serviceType
     * @param locale
     * @return
     */
    @WebMethod(operationName = "getListConnectReasonPre")
    public ReasonPreConnectOut getListConnectReasonPre(
            @WebParam(name = "token") String token,
            @WebParam(name = "userNameLogin") String userNameLogin,
            @WebParam(name = "channelTypeId") Long channelTypeId,
            @WebParam(name = "provinceCode") String provinceCode,
            @WebParam(name = "productCode") String productCode,
            @WebParam(name = "serviceType") String serviceType,
            @WebParam(name = "locale") String locale) {
        return cmController.getRegTypesPre(token, userNameLogin, channelTypeId, provinceCode, productCode, serviceType, locale);
    }

    /**
     * @author: duyetdk
     * @since 12/5/2019
     * @des kich hoat thue bao
     * @param token
     * @param activeSubscriberPreIn
     * @param locale
     * @return
     */
    @WebMethod(operationName = "activeNewSubscriberPre")
    public WSRespone activeNewSubscriberPre(
            @WebParam(name = "token") String token,
            @WebParam(name = "activeSubscriberPreIn") ActiveSubscriberPreIn activeSubscriberPreIn,
            @WebParam(name = "locale") String locale) {
        return cmController.activeNewSubscriberPre(token, activeSubscriberPreIn, locale);
    }

    //@duyetdk update
    @WebMethod(operationName = "getLstAccountByStaff")
    public AccountByStaffOut getLstAccountByStaff(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffID") String staffID,
            @WebParam(name = "account") String account,
            @WebParam(name = "infrasType") String infrasType,
            @WebParam(name = "infrasStatus") String infrasStatus,
            @WebParam(name = "bts") String bts,
            @WebParam(name = "pageNo") Long pageNo,
            @WebParam(name = "connectorCode") String connectorCode,
            @WebParam(name = "deviceCode") String deviceCode) {
        return cmController.getLstAccountByStaff(token, locale, staffID, account, infrasType,
                infrasStatus, bts, pageNo, connectorCode, deviceCode);
    }

    @WebMethod(operationName = "getInfoFrasByAccount")
    public AccountInfrasOut getInfoFrasByAccount(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "account") String account,
            @WebParam(name = "infraType") String infraType,
            @WebParam(name = "stationCode") String stationCode) {
        return cmController.getInfoFrasByAccount(token, locale, account, infraType, stationCode);
    }

    //@duyetdk update
    @WebMethod(operationName = "updateInfrasByStaff")
    public WSRespone updateInfrasByStaff(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "account") String account,
            @WebParam(name = "staffId") String staffId,
            @WebParam(name = "odfIndoorCode") String odfIndoorCode,
            @WebParam(name = "odfIndoorPort") Long odfIndoorPort,
            @WebParam(name = "odfIndoorPortS") Long odfIndoorPortS,
            @WebParam(name = "odfOutdoorCode") String odfOutdoorCode,
            @WebParam(name = "odfOutdoorPort") Long odfOutdoorPort,
            @WebParam(name = "odfOutdoorPortS") Long odfOutdoorPortS,
            @WebParam(name = "lat") Double lat,
            @WebParam(name = "longy") Double longy,
            @WebParam(name = "lengthCable") Long lengthCable,
            @WebParam(name = "portLogic") Long portLogic,
            @WebParam(name = "snCode") String snCode,
            @WebParam(name = "splitterCode") String splitterCode,
            @WebParam(name = "portSpliiter") Long portSpliiter,
            @WebParam(name = "boxCableCode") String boxCableCode,
            @WebParam(name = "boxCablePort") String boxCablePort,
            @WebParam(name = "infrasType") String infrasType,
            @WebParam(name = "stationCode") String stationCode,
            @WebParam(name = "deviceId") Long deviceId,
            @WebParam(name = "portId") Long portId) {
        return cmController.updateInfrasByStaff(token, locale, account, staffId,
                odfIndoorCode, odfIndoorPort, odfIndoorPortS, odfOutdoorCode, odfOutdoorPort,
                odfOutdoorPortS,
                lat, longy, lengthCable,
                portLogic, snCode, splitterCode, portSpliiter,
                boxCableCode, boxCablePort, infrasType, stationCode, deviceId, portId);
    }

    @WebMethod(operationName = "getLocationStationCode")
    public AccountInfrasOut getLocationStationCode(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "stationCode") String stationCode) {
        return cmController.getLocationStationCode(token, locale, stationCode);
    }

    @WebMethod(operationName = "getProductVasFTTH")
    public ParamOut getProductVasFTTH(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getProductVasFTTH();
    }

    @WebMethod(operationName = "getBunldleTvProduct")
    public ProductOut getBunldleTvProduct(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getBunldleTvProduct();
    }

    @WebMethod(operationName = "printInvoiceBluetooth")
    public PayAdvanceBillOut printInvoiceBluetooth(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "accBundleTv") String accBundleTv) {
        return cmController.printInvoiceBluetooth(token, locale, accBundleTv);
    }

    @WebMethod(operationName = "getAccBundleTvInfo")
    public BundleTVInfo getAccBundleTvInfo(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "accBundleTv") String accBundleTv) {
        return cmController.getAccBundleTvInfo(token, locale, accBundleTv);
    }

    @WebMethod(operationName = "addUserBundleTv")
    public WSRespone addUserBundleTv(@WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "subId") Long subId) {
        return cmController.addUserBundleTv(token, locale, subId);
    }

    /**
     * @author: anhnv
     * @since 11/6/2019
     * @des tim thong tin thue bao
     * @param token
     * @param locale
     * @param serviceType
     * @param ISDN
     * @return
     */
    @WebMethod(operationName = "findSubInfoByISDNPre")
    public SubInfoByISDNPreOut findSubInfoByISDNPre(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "serviceType") String serviceType,
            @WebParam(name = "ISDN") String ISDN) {
//        return cmController.activeNewSubscriberPre(token, activeSubscriberPreIn, locale);
        return cmController.findSubInfoByISDNPre(token, locale, serviceType, ISDN);
    }

    /**
     * @author: anhnv
     * @since 11/6/2019
     * @des Lay danh sach ly do
     * @param token
     * @param locale
     * @param codes
     * @return
     */
    @WebMethod(operationName = "getListReasonByCodePre")
    public ListReasonByCodePreOut getListReasonByCodePre(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "codes") Codes codes) {
        return cmController.getListReasonByCodePre(token, locale, codes);
    }

    /**
     * @author: anhnv
     * @since 11/6/2019
     * @des Lay danh sach province
     * @param token
     * @param locale
     * @return
     */
    @WebMethod(operationName = "getListProvincePre")
    public ProvinceOut getListProvincePre(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale) {
        return cmController.getProvinces(token, locale);
    }

    /**
     * @author: anhnv
     * @since 11/6/2019
     * @des Lay thong tin token
     * @param token
     * @param locale
     * @param isdn
     * @return
     */
    @WebMethod(operationName = "getOTPChangeInformationPre")
    public OTPOut getOTPChangeInformationPre(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale,
            @WebParam(name = "isdn") String isdn) {
        return cmController.getOTPChangeInformationPre(token, locale, isdn);
    }

    /**
     * @author: anhnv
     * @since 11/6/2019
     * @des Update thong tin KH
     * @return
     */
    @WebMethod(operationName = "doChangeInformationPre")
    public ChangeInformationOut doChangeInformationPre(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale,
            @WebParam(name = "otp") String otp, @WebParam(name = "subId") Long subId,
            @WebParam(name = "serviceType") String serviceType, @WebParam(name = "custId") Long custId,
            @WebParam(name = "name") String name, @WebParam(name = "idType") Long idType,
            @WebParam(name = "province") String province, @WebParam(name = "address") String address,
            @WebParam(name = "DOB") String DOB, @WebParam(name = "Sex") String Sex,
            @WebParam(name = "isdn") String isdn, @WebParam(name = "contact") String contact,
            @WebParam(name = "form") FormChangeInformation form,
            @WebParam(name = "lstImageIDCard") FormChangeInformation lstImageIDCard,
            @WebParam(name = "lstOldImageIDCard") FormChangeInformation lstOldImageIDCard,
            @WebParam(name = "reasonCode") String reasonCode,
            @WebParam(name = "reasonId") Long reasonId,
            @WebParam(name = "idNo") String idNo) {
        return cmController.doChangeInformationPre(token, locale,
                otp, subId,
                serviceType, custId,
                name, idType,
                province, address,
                DOB, Sex,
                isdn, contact,
                form, lstImageIDCard, lstOldImageIDCard, reasonCode, reasonId, idNo);
    }

    /**
     * @author: duyetdk
     * @since 06/6/2019
     * @des hien thi anh nha KH
     * @param custId
     * @return
     */
    @WebMethod(operationName = "viewImageCustomerLocation")
    public ImageRespone viewImageCustomerLocation(@WebParam(name = "custId") String custId) {
        return cmController.viewImageCustomerLocation(custId);
    }

    @WebMethod(operationName = "checkServiceOnline")
    public ParamOut checkServiceOnline(@WebParam(name = "token") String token) {
        return cmController.checkServiceOnline(token);
    }

    @WebMethod(operationName = "searchRestoreIsdn")
    public ParamOut searchRestoreIsdn(@WebParam(name = "locale") String locale, @WebParam(name = "token") String token, @WebParam(name = "isdn") String isdn) {
        return cmController.searchRestoreIsdn(locale, token, isdn);
    }

    @WebMethod(operationName = "getReasonRestoreIsdn")
    public ReasonOut getReasonRestoreIsdn(@WebParam(name = "locale") String locale, @WebParam(name = "token") String token) {
        return cmController.getReasonRestoreIsdn(locale, token);
    }

    @WebMethod(operationName = "restoreIsdn")
    public WSRespone restoreIsdn(@WebParam(name = "locale") String locale, @WebParam(name = "token") String token, @WebParam(name = "changeSimInput") ChangeSimInput changeSimInput) {
        return cmController.restoreIsdn(locale, token, changeSimInput);
    }

    @WebMethod(operationName = "getGeneralTarget")
    public ParamOut getGeneralTarget(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffId") Long staffId,
            @WebParam(name = "branch") String branch,
            @WebParam(name = "center") String center,
            @WebParam(name = "date") String date) {
        return targetOnlineController.getGeneralTarget(token, locale, staffId, branch, center, date);
    }

    @WebMethod(operationName = "getTarget")
    public ParamOut getTarget(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "staffId") Long staffId, @WebParam(name = "formId") Long formId) {
        return targetOnlineController.getTarget(token, locale, staffId, formId);
    }

    @WebMethod(operationName = "getBranch")
    public ProvinceOut getBranch(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "staffId") Long staffId) {
        return targetOnlineController.getBranch(token, locale, staffId);
    }

    @WebMethod(operationName = "getCenterOfBranch")
    public ParamOut getCenterOfBranch(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "provinceCode") String provinceCode, @WebParam(name = "staffId") Long staffId) {
        return targetOnlineController.getCenterOfBranch(token, locale, provinceCode, staffId);
    }

    @WebMethod(operationName = "getStaffOfCenter")
    public ParamOut getStaffOfCenter(@WebParam(name = "token") String token, @WebParam(name = "locale") String locale, @WebParam(name = "provinceCode") String province, @WebParam(name = "centerCode") String center) {
        return targetOnlineController.getStaffOfCenter(token, locale, province, center);
    }

    @WebMethod(operationName = "getTargetGrapth")
    public ParamOut getTargetGrapth(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "receiverCode") String receiverCode,
            @WebParam(name = "targetId") String targetId) {
        return targetOnlineController.getTargetGrapth(token, locale, receiverCode, targetId);
    }

    @WebMethod(operationName = "getWarningTargetDetail")
    public ParamOut getWarningTargetDetail(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "receiverCode") String receiverCode,
            @WebParam(name = "targetId") Long targetId) {
        return targetOnlineController.getWarningTargetDetail(token, locale, receiverCode, targetId);
    }

    @WebMethod(operationName = "getTargetReport")
    public ParamOut getTargetReport(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffLoginId") Long staffLoginId,
            @WebParam(name = "targetId") Long targetId,
            @WebParam(name = "province") String province,
            @WebParam(name = "center") String center,
            @WebParam(name = "staffCode") String staffCode,
            @WebParam(name = "date") String date) {
        return targetOnlineController.getTargetReport(token, locale, staffLoginId, targetId, province, center, staffCode, date);
    }

    @WebMethod(operationName = "getRecoverActionType")
    public ParamOut getRecoverActionType(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale) {
        return cmController.getRecoverActionType(token, locale);
    }

    @WebMethod(operationName = "getRecoverReason")
    public ParamOut getRecoverReason(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "actionType") String actionType) {
        return cmController.getRecoverReason(token, locale, actionType);
    }

    @WebMethod(operationName = "updateRecoverReason")
    public ParamOut updateRecoverReason(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "actionType") String actionType,
            @WebParam(name = "reasonCode") String reasonCode,
            @WebParam(name = "x") String x,
            @WebParam(name = "y") String y,
            @WebParam(name = "contractId") Long contractId) {
        return cmController.updateRecoverReason(token, locale, contractId, actionType, reasonCode, x, y);
    }

    /**
     * @author duyetdk
     * @des lay ds connector gan theo tram
     * @param token
     * @param locale
     * @param stationId
     * @param stationCode
     * @return
     */
    @WebMethod(operationName = "getListConnectorByStation")
    public TechInforOut getListConnectorByStation(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "stationId") Long stationId,
            @WebParam(name = "stationCode") String stationCode) {
        return cmController.getListConnectorByStation(token, locale, stationId, stationCode);
    }

    @WebMethod(operationName = "getListPotentialCust")
    public PotentialCustomerOut getListPotentialCust(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "name") String name,
            @WebParam(name = "phoneNumber") String phoneNumber,
            @WebParam(name = "address") String address,
            @WebParam(name = "status") Long status,
            @WebParam(name = "pageNo") Long pageNo,
            @WebParam(name = "province") String province,
            @WebParam(name = "kindOfCustomer") String kindOfCustomer) {
        return cmController.getListPotentialCust(token, locale, name, phoneNumber, address, status, pageNo, province, kindOfCustomer);
    }

    @WebMethod(operationName = "addNewPotentialCust")
    public WSRespone addNewPotentialCust(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "name") String name,
            @WebParam(name = "phoneNumber") String phoneNumber,
            @WebParam(name = "address") String address,
            @WebParam(name = "province") String province,
            @WebParam(name = "email") String email,
            @WebParam(name = "fromOperator") String otherOperator,
            @WebParam(name = "fee") Double fee,
            @WebParam(name = "latitude") String latitude,
            @WebParam(name = "longitude") String longitude,
            @WebParam(name = "kindOfCustomer") String kindOfCustomer,
            @WebParam(name = "expectedService") String expectedService,
            @WebParam(name = "expectedScale") String expectedScale,
            @WebParam(name = "serviceUsed") String serviceUsed) {
        return cmController.addNewPotentialCust(token, locale, name, phoneNumber, address, province, email,
                otherOperator, fee, latitude, longitude,
                kindOfCustomer, expectedService, expectedScale, serviceUsed);
    }

    @WebMethod(operationName = "updatePotentialCust")
    public WSRespone updatePotentialCust(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "custId") Long custId,
            @WebParam(name = "name") String name,
            @WebParam(name = "phoneNumber") String phoneNumber,
            @WebParam(name = "address") String address,
            @WebParam(name = "province") String province,
            @WebParam(name = "email") String email,
            @WebParam(name = "fromOperator") String otherOperator,
            @WebParam(name = "fee") Double fee,
            @WebParam(name = "latitude") String latitude,
            @WebParam(name = "longitude") String longitude,
            @WebParam(name = "status") Long status,
            @WebParam(name = "kindOfCustomer") String kindOfCustomer,
            @WebParam(name = "expectedService") String expectedService,
            @WebParam(name = "expectedScale") String expectedScale,
            @WebParam(name = "serviceUsed") String serviceUsed) {
        return cmController.updatePotentialCust(token, locale, custId, name, phoneNumber, address, province,
                email, otherOperator, fee, latitude, longitude, status,
                kindOfCustomer, expectedService, expectedScale, serviceUsed);
    }

    @WebMethod(operationName = "deletePotentialCust")
    public WSRespone deletePotentialCust(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "custId") Long custId) {
        return cmController.deletePotentialCust(token, locale, custId);
    }

    @WebMethod(operationName = "buyData")
    public ParamOut buyData(
            @WebParam(name = "token") String token,
            @WebParam(name = "encrypt") String encrypt,
            @WebParam(name = "signature") String signature) {
        return cmController.buyData(token, encrypt, signature);
    }

    @WebMethod(operationName = "confirmTransactionEmoney")
    public ParamOut confirmTransactionEmoney(
            @WebParam(name = "token") String token,
            @WebParam(name = "requestId") String requestId,
            @WebParam(name = "date") String date) {
        return cmController.confirmTopUp(token, requestId, date);
    }

    @WebMethod(operationName = "confirmTransaction")
    public ParamOut confirmTransaction(
            @WebParam(name = "token") String token,
            @WebParam(name = "requestId") String requestId,
            @WebParam(name = "date") String date) {
        return cmController.confirmTopUp(token, requestId, date);
    }

    @WebMethod(operationName = "extendTelecomService")
    public ParamOut extendTelecomService(
            @WebParam(name = "token") String token,
            @WebParam(name = "encrypt") String encrypt,
            @WebParam(name = "signature") String signature) {
        return cmController.extendTelecomService(token, encrypt, signature);
    }

    @WebMethod(operationName = "changeDeviceAccFtth")
    public ParamOut changeDeviceAccFtth(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "account") String account,
            @WebParam(name = "deviceCode") String deviceCode) {
        return cmController.changeDeviceAccFtth(token, locale, account, deviceCode);
    }

    @WebMethod(operationName = "regOnlineFtth")
    public ParamOut regOnlineFtth(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "input") RegOnlineInput input) {
        return regOnlineController.regOnlineFtth(token, locale, input);
    }

    @WebMethod(operationName = "getInfoRegOnlineFtth")
    public ParamOut getInfoRegOnlineFtth(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "requestId") Long requestId,
            @WebParam(name = "customerPhone") String customerPhone,
            @WebParam(name = "customerName") String customerName,
            @WebParam(name = "code") String code,
            @WebParam(name = "source") String source,
            @WebParam(name = "fromDate") String fromDate,
            @WebParam(name = "toDate") String toDate,
            @WebParam(name = "province") String province,
            @WebParam(name = "assignShop") String assignShop,
            @WebParam(name = "assignStaff") String assignStaff,
            @WebParam(name = "staffCode") String staffCode,
            @WebParam(name = "filterStatus") Long filterStatus,
            @WebParam(name = "statusDeadline") Long statusDeadline,
            @WebParam(name = "statusReqCancel") Long statusReqCancel,
            @WebParam(name = "requesterCancel") String requesterCancel,
            @WebParam(name = "pageIndex") int pageIndex,
            @WebParam(name = "pageSize") int pageSize) {
        return regOnlineController.getInfoRegOnlineFtth(token, locale, requestId, customerPhone, customerName, code, source, fromDate, toDate, province, assignShop, assignStaff, staffCode, filterStatus, statusDeadline, statusReqCancel, pageIndex, pageSize, requesterCancel);
    }

    @WebMethod(operationName = "getRegOnlineHis")
    public ParamOut getRegOnlineHis(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffCode") String staffCode,
            @WebParam(name = "requestId") Long requestId) {
        return regOnlineController.getRegOnlineHis(token, locale, staffCode, requestId);
    }

    @WebMethod(operationName = "updateProgressRegOnline")
    public ParamOut updateProgressRegOnline(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "requestId") Long requestId,
            @WebParam(name = "staffCode") String staffCode,
            @WebParam(name = "assignProvince") String assignProvince,
            @WebParam(name = "assignShop") String assignShop,
            @WebParam(name = "assignStaff") String assignStaff,
            @WebParam(name = "status") Long status,
            @WebParam(name = "reason") String reason,
            @WebParam(name = "description") String description,
            @WebParam(name = "statusApprove") Long statusApprove) {
        return regOnlineController.updateProgressRegOnline(token, locale, requestId, staffCode, assignProvince, assignShop, assignStaff, status, reason, description, statusApprove);
    }

    @WebMethod(operationName = "getProgressRegOnline")
    public ParamOut getProgressRegOnline(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "type") Long type) {
        return regOnlineController.getProgressRegOnline(token, locale, type);
    }

    @WebMethod(operationName = "getRequestIdFromIsdn")
    public ParamOut getRequestIdFromIsdn(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "isdn") String isdn) {
        return regOnlineController.getRequestIdFromIsdn(token, locale, isdn);
    }

    @WebMethod(operationName = "getProvinceReqOnline")
    public ParamOut getProvinceReqOnline(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffCode") String staffCode,
            @WebParam(name = "filterStatus") Long filterStatus,
            @WebParam(name = "statusDeadline") Long statusDeadline) {
        return regOnlineController.getProvinceReqOnline(token, locale, staffCode, filterStatus, statusDeadline);
    }

    @WebMethod(operationName = "getReqOnline")
    public ParamOut getReqOnline(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffCode") String staffCode,
            @WebParam(name = "filterStatus") Long filterStatus) {
        return regOnlineController.getReqOnline(token, locale, staffCode, filterStatus);
    }

    @WebMethod(operationName = "getReasonRegOnline")
    public ParamOut getReasonRegOnline(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffCode") String staffCode) {
        return regOnlineController.getReasonRegOnline(token, locale);
    }

    @WebMethod(operationName = "getListShowroom")
    public ParamOut getListShowroom(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffCode") String staffCode,
            @WebParam(name = "province") String province) {
        return regOnlineController.getListShowroom(token, locale, staffCode, province);
    }

    @WebMethod(operationName = "getListStaffShowroom")
    public ParamOut getListStaffShowroom(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffCode") String staffCode,
            @WebParam(name = "shopCode") String shopCode) {
        return regOnlineController.getListStaffShowroom(token, locale, staffCode, shopCode);
    }

    @WebMethod(operationName = "buyDataVAS")
    public ParamOut buyDataVAS(
            @WebParam(name = "token") String token,
            @WebParam(name = "encrypt") String encrypt,
            @WebParam(name = "signature") String signature) {
        return cmController.buyDataVAS(token, encrypt, signature);
    }

    @WebMethod(operationName = "buyDataVASPartner")
    public ParamOut buyDataVASPartner(
            @WebParam(name = "token") String token,
            @WebParam(name = "encrypt") String encrypt,
            @WebParam(name = "signature") String signature) {
        return cmController.buyDataVASPartner(token, encrypt, signature);
    }

    @WebMethod(operationName = "getStaffSalaryDetailAPBus")
    public StaffSalaryDetail getStaffSalaryDetailAPBus(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffCode") String staffCode,
            @WebParam(name = "month") int month,
            @WebParam(name = "year") int year) {
        return cmController.getStaffSalaryDetailAPBus(token, locale, staffCode, month, year);
    }

    //phuonghc 15062020
    @WebMethod(operationName = "getPotentialCustomerHistory")
    public PotentialCustomerOut getPotentialCustomerHistory(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "custId") String custId) {
        return cmController.getPotentialCustomerHistory(token, locale, custId);
    }

    @WebMethod(operationName = "getParamForPotentialCustomer")
    public ParamOut getParamForPotentialCustomer(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale) {
        return cmController.getListParamForPotentialCustomer(token, locale);
    }


    //phuonghc 02072020 - create new API cho task 1187774 on wework.base.vn
    @WebMethod(operationName = "getPaymentReport")
    public ParamOut getPaymentReport(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffCode") String staffCode,
            @WebParam(name = "codeReport") String codeReport,
            @WebParam(name = "isChiefCenter") String isChiefCenter) {
        return cmController.getPaymentReport(token, locale, staffCode, codeReport, isChiefCenter);
    }

    @WebMethod(operationName = "getUnpaidDetail")
    public ParamOut getUnpaidDetail(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "staffCode") String staffCode,
            @WebParam(name = "codeReport") String codeReport,
            @WebParam(name = "isChiefCenter") String isChiefCenter) {
        return cmController.getUnpaidDetail(token, locale, staffCode, codeReport, isChiefCenter);

    }
    //phuonghc 05092020
    @WebMethod(operationName = "getListAccountForSubscriber")
    public ParamOut getListAccountForSubscriber(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "custId") String custId) {
        return cmController.getListAccountForSubscriber(token, locale, custId);
    }

    @WebMethod(operationName = "getListReasonByType")
    public ParamOut getListReasonByType(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "custId") String custId,
            @WebParam(name = "account") String account) {
        return cmController.getListReasonByType(token, locale, custId, account);
    }

    @WebMethod(operationName = "doingChangeDeploymentAddress")
    public ParamOut doingChangeDeploymentAddress(
            @WebParam(name = "token") String token,
            @WebParam(name = "locale") String locale,
            @WebParam(name = "lat") String lat,
            @WebParam(name = "lng") String lng,
            ////////////////
            @WebParam(name = "stationId") String stationId,
            @WebParam(name = "connectId") String connectId,
            @WebParam(name = "staffCode") String staffCode,
            @WebParam(name = "shopCode") String shopCode,
            @WebParam(name = "staffId") String staffId,
            @WebParam(name = "reasonId") String reasonId,
            @WebParam(name = "subId") String subId,
            @WebParam(name = "lineType") String lineType,
            @WebParam(name = "isdn") String isdn,
            @WebParam(name = "teamCode") String teamCode,
            ///////////
            @WebParam(name = "dateTimeDeploy") String dateTimeDeploy,
            @WebParam(name = "delayReason") String delayReason,
            ///////////
            @WebParam(name = "provinceCode") String provinceCode,
            @WebParam(name = "districtCode") String districtCode,
            @WebParam(name = "precinctCode") String precinctCode,
            @WebParam(name = "streetName") String streetName,
            @WebParam(name = "home") String home) {

        ChangeDeploymentAddressIn input = new ChangeDeploymentAddressIn(
                lat, lng, stationId,
                connectId, shopCode,
                staffCode, staffId, reasonId,
                subId, lineType, isdn,
                teamCode, dateTimeDeploy, delayReason,
                provinceCode, districtCode, precinctCode, streetName, home);
        ParamOut out = cmController.doingChangeDeploymentAddress(token, locale, input);
        return out;
    }

}
