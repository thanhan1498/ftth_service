/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.merchant.controller;

import com.viettel.bccs.cm.controller.BaseController;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.merchant.dao.ActionLogDAO;
import com.viettel.bccs.merchant.dao.TransactionLogDAO;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class MerchantController extends BaseController {

    public MerchantController() {
        logger = Logger.getLogger(MerchantController.class);
    }

    public void insertActionLog(HibernateHelper hibernateHelper, String merchantAccount, String requestId, String serviceCode, String processCode, String isdn, Double amount, Long saleTransId, String errorCode, String request, String reponse, Date requestDate, String description) {
        Session merchantSession = null;
        boolean hasErr = false;
        try {
            merchantSession = hibernateHelper.getSession(Constants.SESSION_MERCHANT);
            new ActionLogDAO().insertActionLog(merchantSession, merchantAccount, requestId, serviceCode, processCode, isdn, amount, saleTransId, errorCode, request, reponse, requestDate, description);
            commitTransactions(merchantSession);
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(merchantSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(merchantSession);
        }
    }

    public Long insertTransactionLog(HibernateHelper hibernateHelper, String merchantAccount, String requestId, String serviceCode, String processCode, String isdn, Double amount, Long saleTransId, String errorCode, String request, String reponse, Date requestDate, String description) {
        Session merchantSession = null;
        boolean hasErr = false;
        try {
            merchantSession = hibernateHelper.getSession(Constants.SESSION_MERCHANT);
            Long transactionId = new TransactionLogDAO().insertTransactionLog(merchantSession, merchantAccount, requestId, serviceCode, processCode, isdn, amount, saleTransId, errorCode, request, reponse, requestDate, description);
            commitTransactions(merchantSession);
            return transactionId;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(merchantSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(merchantSession);
        }
        return null;
    }
    
    public void insertActionLog(HibernateHelper hibernateHelper, String merchantAccount, String requestId, String serviceCode, String processCode, String isdn, Double amount, Long saleTransId, String errorCode, String request, String reponse, Date requestDate, String description, String ip) {
        Session merchantSession = null;
        boolean hasErr = false;
        try {
            merchantSession = hibernateHelper.getSession(Constants.SESSION_MERCHANT);
            new ActionLogDAO().insertActionLog(merchantSession, merchantAccount, requestId, serviceCode, processCode, isdn, amount, saleTransId, errorCode, request, reponse, requestDate, description, ip);
            commitTransactions(merchantSession);
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(merchantSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(merchantSession);
        }
    }
    
    public Long insertTransactionLog(HibernateHelper hibernateHelper, String merchantAccount, String requestId, String serviceCode, String processCode, String isdn, Double amount, Long saleTransId, String errorCode, String request, String reponse, Date requestDate, String description, String ip) {
        Session merchantSession = null;
        boolean hasErr = false;
        try {
            merchantSession = hibernateHelper.getSession(Constants.SESSION_MERCHANT);
            Long transactionId = new TransactionLogDAO().insertTransactionLog(merchantSession, merchantAccount, requestId, serviceCode, processCode, isdn, amount, saleTransId, errorCode, request, reponse, requestDate, description, ip);
            commitTransactions(merchantSession);
            return transactionId;
        } catch (Throwable ex) {
            hasErr = true;
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            if (hasErr) {
                try {
                    rollBackTransactions(merchantSession);
                } catch (Throwable ex) {
                    LogUtils.error(logger, ex.getMessage(), ex);
                }
            }
            closeSessions(merchantSession);
        }
        return null;
    }
}
