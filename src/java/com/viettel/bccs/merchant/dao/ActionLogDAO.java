/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.merchant.dao;

import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.merchant.model.ActionLog;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class ActionLogDAO extends BaseSupplier {

    public void insertActionLog(Session session, String merchantAccount, String requestId, String serviceCode, String processCode, String isdn, Double amount, Long saleTransId, String errorCode, String request, String reponse, Date requestDate, String description) {
        Long id = getSequence(session, Constants.BCCS_MERCHANT_ACTION_LOG_SEQ);
        ActionLog result = new ActionLog();
        result.setId(id);
        result.setMerchantAccount(merchantAccount);
        result.setRequestId(requestId);
        result.setServiceCode(serviceCode);
        result.setProcessCode(processCode);
        result.setIsdn(isdn);
        result.setAmount(amount);
        result.setSaleTransId(saleTransId);
        result.setErrorCode(errorCode);
        result.setRequest(request.length() >= 500 ? request.substring(0, 499) : request);
        result.setResponse(reponse.length() >= 500 ? reponse.substring(0, 499) : reponse);
        result.setDescription(description);
        result.setRequestDate(requestDate);
        session.save(result);
        session.flush();
    }

    public boolean isRequestIdExists(Session session, String requestId) {
        String sql = "select 1 from Action_Log where request_Id =?";
        Query query = session.createSQLQuery(sql);
        query.setParameter(0, requestId);
        List list = query.list();
        if (list == null || list.isEmpty()) {
            return false;
        }
        return true;
    }
    
    public void insertActionLog(Session session, String merchantAccount, String requestId, String serviceCode, String processCode, String isdn, Double amount, Long saleTransId, String errorCode, String request, String reponse, Date requestDate, String description, String ip) {
        Long id = getSequence(session, Constants.BCCS_MERCHANT_ACTION_LOG_SEQ);
        ActionLog result = new ActionLog();
        result.setId(id);
        result.setMerchantAccount(merchantAccount);
        result.setRequestId(requestId);
        result.setServiceCode(serviceCode);
        result.setProcessCode(processCode);
        result.setIsdn(isdn);
        result.setAmount(amount);
        result.setSaleTransId(saleTransId);
        result.setErrorCode(errorCode);
        result.setRequest(request.length() >= 500 ? request.substring(0, 499) : request);
        result.setResponse(reponse.length() >= 500 ? reponse.substring(0, 499) : reponse);
        result.setDescription(description);
        result.setRequestDate(requestDate);
        result.setIp(ip);
        session.save(result);
        session.flush();
    }
}
