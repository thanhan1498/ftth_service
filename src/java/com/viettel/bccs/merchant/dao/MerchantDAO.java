/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.merchant.dao;

import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.bccs.merchant.model.Merchant;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class MerchantDAO extends BaseSupplier {

    public MerchantDAO() {
        logger = Logger.getLogger(MerchantDAO.class);
    }

    public List<Merchant> getMerchantByAccount(Session bccsMerchant, String account) {
        StringBuilder sql = new StringBuilder().append(" from Merchant where merchantAccount = :merchantAccount and status = 1 ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("merchantAccount", account);
        Query query = bccsMerchant.createQuery(sql.toString());
        buildParameter(query, params);
        List<Merchant> result = query.list();
        return result;
    }
}
