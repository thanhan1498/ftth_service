/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.merchant.dao;

import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.bccs.merchant.model.MerchantPermission;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class MerchantPermissionDAO extends BaseSupplier {

    public MerchantPermissionDAO() {
        logger = Logger.getLogger(MerchantPermissionDAO.class);
    }

    public List<MerchantPermission> getRoleOfAccount(Session bccsMerchant, String account, String serviceCode) {
        StringBuilder sql = new StringBuilder().append(" from MerchantPermission where merchantAccount = :merchantAccount and serviceCode = :serviceCode ");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("merchantAccount", account);
        params.put("serviceCode", serviceCode);
        Query query = bccsMerchant.createQuery(sql.toString());
        buildParameter(query, params);
        List<MerchantPermission> result = query.list();
        return result;
    }
}
