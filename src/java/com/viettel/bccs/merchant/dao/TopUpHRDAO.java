/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.merchant.dao;

import com.viettel.bccs.cm.supplier.BaseSupplier;
import java.math.BigDecimal;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.HibernateException;

/**
 *
 * @author sameanl
 */
public class TopUpHRDAO extends BaseSupplier {

    public TopUpHRDAO() {
        logger = Logger.getLogger(TopUpHRDAO.class);
    }

    public boolean checkTopUpHoi(Session sessionIM, String isdn) {
        String sql = "select count(*) from sale_anypay_file where isdn=:isdn and to_char(create_date,'mm') = to_char(sysdate, 'mm') and to_char(create_date,'yyyy') = to_char(sysdate, 'yyyy')";
        Query query = sessionIM.createSQLQuery(sql);
        query.setParameter("isdn", isdn);

        try {
            List result = query.list();
            if (result != null && !result.isEmpty()) {
                BigDecimal numberOfRecordIsdn = (BigDecimal)result.get(0);
                if (numberOfRecordIsdn != null && numberOfRecordIsdn.longValue() > 0) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (HibernateException he) {
            logger.error("### An error while check topup hoi or not", he);
        }
        return true;
    }
}
