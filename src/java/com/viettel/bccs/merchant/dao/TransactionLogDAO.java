/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.merchant.dao;

import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.merchant.model.TransactionLog;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class TransactionLogDAO extends BaseSupplier {

    public Long insertTransactionLog(Session session, String merchantAccount, String requestId, String serviceCode, String processCode, String isdn, Double amount, Long saleTransId, String errorCode, String request, String reponse, Date requestDate, String description) {
        Long id = getSequence(session, Constants.BCCS_MERCHANT_TRANSACTION_LOG_SEQ);
        TransactionLog result = new TransactionLog();
        result.setId(id);
        result.setMerchantAccount(merchantAccount);
        result.setRequestId(requestId);
        result.setServiceCode(serviceCode);
        result.setProcessCode(processCode);
        result.setIsdn(isdn);
        result.setAmount(amount);
        result.setSaleTransId(saleTransId);
        result.setErrorCode(errorCode);
        result.setRequest(request.length() >= 500 ? request.substring(0, 499) : request);
        result.setResponse(reponse.length() >= 500 ? reponse.substring(0, 499) : reponse);
        result.setDescription(description);
        result.setRequestDate(requestDate);
        session.save(result);
        session.flush();
        return id;
    }

    public Long getSaleTransIdFromRequestId(Session session, String requestId, String merchantAccount) {
        String sql = "select sale_trans_id from transaction_log where request_Id =? and service_code = 'getPinCode' and merchant_account = ?";
        Query query = session.createSQLQuery(sql);
        query.setParameter(0, requestId);
        query.setParameter(1, merchantAccount);
        List list = query.list();
        if (list == null || list.isEmpty()) {
            return null;
        }
        if (list.get(0) == null || list.get(0).toString().isEmpty()) {
            return null;
        }
        return Long.parseLong(list.get(0).toString());
    }

    public String confimTopup(Session session, String requestId, String date, String merchantAccount) {
        String sql = "select nvl(error_code,1) "
                + " from transaction_log "
                + " where request_id = ? "
                + " and request_date>= to_date(?,'dd/mm/yyyy') "
                + " and request_date <= to_date(?,'dd/mm/yyyy') + 2 "
                + " and merchant_account = ?";
        Query query = session.createSQLQuery(sql);
        query.setParameter(0, requestId);
        query.setParameter(1, date);
        query.setParameter(2, date);
        query.setParameter(3, merchantAccount);
        List list = query.list();
        if (list == null || list.isEmpty()) {
            return null;
        }
        if (list.get(0) == null || list.get(0).toString().isEmpty()) {
            return null;
        }
        return list.get(0).toString();
    }
    
    public Long insertTransactionLog(Session session, String merchantAccount, String requestId, String serviceCode, String processCode, String isdn, Double amount, Long saleTransId, String errorCode, String request, String reponse, Date requestDate, String description, String ip) {
        Long id = getSequence(session, Constants.BCCS_MERCHANT_TRANSACTION_LOG_SEQ);
        TransactionLog result = new TransactionLog();
        result.setId(id);
        result.setMerchantAccount(merchantAccount);
        result.setRequestId(requestId);
        result.setServiceCode(serviceCode);
        result.setProcessCode(processCode);
        result.setIsdn(isdn);
        result.setAmount(amount);
        result.setSaleTransId(saleTransId);
        result.setErrorCode(errorCode);
        result.setRequest(request.length() >= 500 ? request.substring(0, 499) : request);
        result.setResponse(reponse.length() >= 500 ? reponse.substring(0, 499) : reponse);
        result.setDescription(description);
        result.setRequestDate(requestDate);
        result.setIp(ip);
        session.save(result);
        session.flush();
        return id;
    }
}
