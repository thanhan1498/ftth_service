/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.merchant.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author cuongdm
 */
public class Discount implements Serializable{
    private Long discountId;
    private String areaCode;
    private Long channelTypeId;
    private Double fromAmount;
    private Double toAmount;
    private Double discountRate;
    private Double discountAmount;
    private String type;
    private Date startDatetime;
    private Date endDatetime;
    private Long status;
    private Long discountGroupId;
    private String[] arrAreas;
    private Long[] arrChannels;

    /**
     * @return the discountId
     */
    public Long getDiscountId() {
        return discountId;
    }

    /**
     * @param discountId the discountId to set
     */
    public void setDiscountId(Long discountId) {
        this.discountId = discountId;
    }

    /**
     * @return the areaCode
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * @param areaCode the areaCode to set
     */
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    /**
     * @return the channelTypeId
     */
    public Long getChannelTypeId() {
        return channelTypeId;
    }

    /**
     * @param channelTypeId the channelTypeId to set
     */
    public void setChannelTypeId(Long channelTypeId) {
        this.channelTypeId = channelTypeId;
    }

    /**
     * @return the fromAmount
     */
    public Double getFromAmount() {
        return fromAmount;
    }

    /**
     * @param fromAmount the fromAmount to set
     */
    public void setFromAmount(Double fromAmount) {
        this.fromAmount = fromAmount;
    }

    /**
     * @return the toAmount
     */
    public Double getToAmount() {
        return toAmount;
    }

    /**
     * @param toAmount the toAmount to set
     */
    public void setToAmount(Double toAmount) {
        this.toAmount = toAmount;
    }

    /**
     * @return the discountRate
     */
    public Double getDiscountRate() {
        return discountRate;
    }

    /**
     * @param discountRate the discountRate to set
     */
    public void setDiscountRate(Double discountRate) {
        this.discountRate = discountRate;
    }

    /**
     * @return the discountAmount
     */
    public Double getDiscountAmount() {
        return discountAmount;
    }

    /**
     * @param discountAmount the discountAmount to set
     */
    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the startDatetime
     */
    public Date getStartDatetime() {
        return startDatetime;
    }

    /**
     * @param startDatetime the startDatetime to set
     */
    public void setStartDatetime(Date startDatetime) {
        this.startDatetime = startDatetime;
    }

    /**
     * @return the endDatetime
     */
    public Date getEndDatetime() {
        return endDatetime;
    }

    /**
     * @param endDatetime the endDatetime to set
     */
    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the discountGroupId
     */
    public Long getDiscountGroupId() {
        return discountGroupId;
    }

    /**
     * @param discountGroupId the discountGroupId to set
     */
    public void setDiscountGroupId(Long discountGroupId) {
        this.discountGroupId = discountGroupId;
    }

    /**
     * @return the arrAreas
     */
    public String[] getArrAreas() {
        return arrAreas;
    }

    /**
     * @param arrAreas the arrAreas to set
     */
    public void setArrAreas(String[] arrAreas) {
        this.arrAreas = arrAreas;
    }

    /**
     * @return the arrChannels
     */
    public Long[] getArrChannels() {
        return arrChannels;
    }

    /**
     * @param arrChannels the arrChannels to set
     */
    public void setArrChannels(Long[] arrChannels) {
        this.arrChannels = arrChannels;
    }
}
