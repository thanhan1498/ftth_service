/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.merchant.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author cuongdm
 */
@Entity
@Table(name = "TRANSACTION_LOG")
public class TransactionLog implements Serializable{
    @Id
    @Column(name = "ID", length = 10, precision = 10, scale = 0)
    private Long id;
    @Column(name = "MERCHANT_ACCOUNT", length = 256)
    private String merchantAccount;
    @Column(name = "REQUEST_ID", length = 64)
    private String requestId;
    @Column(name = "SERVICE_CODE", length = 256)
    private String serviceCode;
    @Column(name = "PROCESS_CODE", length = 256)
    private String processCode;
    @Column(name = "ISDN", length = 32)
    private String isdn;
    @Column(name = "AMOUNT", length = 22, precision = 22, scale = 5)
    private Double amount;
    @Column(name = "SALE_TRANS_ID", length = 10, precision = 10, scale = 0)
    private Long saleTransId;
    @Column(name = "ERROR_CODE", length = 32)
    private String errorCode;
    @Column(name = "REQUEST", length = 500)
    private String request;
    @Column(name = "RESPONSE", length = 500)
    private String response;
    @Column(name = "DESCRIPTION", length = 200)
    private String description;
    @Column(name = "REQUEST_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDate;
    @Column(name = "IP", length = 100)
    private String ip;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the merchantAccount
     */
    public String getMerchantAccount() {
        return merchantAccount;
    }

    /**
     * @param merchantAccount the merchantAccount to set
     */
    public void setMerchantAccount(String merchantAccount) {
        this.merchantAccount = merchantAccount;
    }

    /**
     * @return the requestId
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * @param requestId the requestId to set
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * @return the serviceCode
     */
    public String getServiceCode() {
        return serviceCode;
    }

    /**
     * @param serviceCode the serviceCode to set
     */
    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    /**
     * @return the processCode
     */
    public String getProcessCode() {
        return processCode;
    }

    /**
     * @param processCode the processCode to set
     */
    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    /**
     * @return the isdn
     */
    public String getIsdn() {
        return isdn;
    }

    /**
     * @param isdn the isdn to set
     */
    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the saleTransId
     */
    public Long getSaleTransId() {
        return saleTransId;
    }

    /**
     * @param saleTransId the saleTransId to set
     */
    public void setSaleTransId(Long saleTransId) {
        this.saleTransId = saleTransId;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * @return the request
     */
    public String getRequest() {
        return request;
    }

    /**
     * @param request the request to set
     */
    public void setRequest(String request) {
        this.request = request;
    }

    /**
     * @return the response
     */
    public String getResponse() {
        return response;
    }

    /**
     * @param response the response to set
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the requestDate
     */
    public Date getRequestDate() {
        return requestDate;
    }

    /**
     * @param requestDate the requestDate to set
     */
    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }
}
