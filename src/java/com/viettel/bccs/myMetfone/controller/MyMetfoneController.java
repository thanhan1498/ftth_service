/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.myMetfone.controller;

import com.viettel.brcd.ws.model.input.ComplaintInput;
import com.viettel.brcd.ws.model.output.ParamOut;

/**
 *
 * @author cuongdm
 */
public interface MyMetfoneController {

    ParamOut getComType(String token, String locale, String isdn);

    ParamOut submitComplaint(String token, String locale, ComplaintInput complaintInput);

    ParamOut getProcessList(String token, String locale);

    ParamOut getComplaintHistory(String token, String locale, String isdn, Long complaintId, int rate);

    ParamOut reopenComplain(String token, String locale, Long complaintId, String isdn, String content);

    ParamOut closeComplain(String token, String locale, Long complaintId, String isdn);

    ParamOut rateComplain(String token, String locale, String complaintId, String isdn, String rate);
}
