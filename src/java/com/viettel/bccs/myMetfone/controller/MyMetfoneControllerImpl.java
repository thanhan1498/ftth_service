/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.myMetfone.controller;

import com.viettel.bccs.cc.controller.ComplaintController;
import com.viettel.bccs.cm.controller.BaseController;
import com.viettel.bccs.cm.controller.TokenController;
import com.viettel.bccs.cm.controller.TransLogController;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.input.ComplaintInput;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.eafs.util.SpringUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author cuongdm
 */
public class MyMetfoneControllerImpl extends BaseController implements MyMetfoneController {

    private transient HibernateHelper hibernateHelper;

    public HibernateHelper getHibernateHelper() {
        if (this.hibernateHelper == null) {
            this.hibernateHelper = (HibernateHelper) SpringUtil.getBean("hibernateHelper");
            setHibernateHelper(this.hibernateHelper);
        }
        return hibernateHelper;
    }

    public void setHibernateHelper(HibernateHelper hibernateHelper) {
        this.hibernateHelper = hibernateHelper;
    }

    @Override
    public ParamOut getComType(String token, String locale, String isdn) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        Long sTimeTr = System.currentTimeMillis();
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            result = new ComplaintController().getComType(hibernateHelper, token, locale, isdn);
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("groupId", isdn);
            new TransLogController().insert(hibernateHelper, token, MyMetfoneControllerImpl.class.getName(),
                    "getComType", LogUtils.toJson(paramTr),
                    (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut submitComplaint(String token, String locale, ComplaintInput complaintInput) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        Long sTimeTr = System.currentTimeMillis();
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            if (complaintInput != null) {
                complaintInput.setSourceComplaint(Constants.APP_MY_METFONE);
            }
            WSRespone response = new ComplaintController().submitComplaintMyMetfone(hibernateHelper, complaintInput, locale, token);
            result = new ParamOut(response.getErrorCode(), response.getErrorDecription());
        }
        Long eTimeTr = System.currentTimeMillis();
        try {
            HashMap paramTr = new HashMap();
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("complaintInput", complaintInput);
            new TransLogController().insert(hibernateHelper, token, MyMetfoneControllerImpl.class.getName(),
                    "submitComplaint", LogUtils.toJson(paramTr),
                    (eTimeTr - sTimeTr), LogUtils.toJson(result));
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ParamOut getProcessList(String token, String locale) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            return new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            List<ApParam> list = new ArrayList<ApParam>();

            list.add(new ApParam(0l, LabelUtil.getKey("complain.status0", locale)));
            list.add(new ApParam(1l, LabelUtil.getKey("complain.status1", locale)));
            list.add(new ApParam(2l, LabelUtil.getKey("complain.status2", locale)));
            list.add(new ApParam(3l, LabelUtil.getKey("complain.status3", locale)));
            ParamOut temp = new ParamOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale));
            temp.setListProcess(list);
            return temp;
        }
    }

    @Override
    public ParamOut getComplaintHistory(String token, String locale, String isdn, Long complaintId, int rate) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        ParamOut result;
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            result = new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            return new ComplaintController().getComplaintHistory(hibernateHelper, locale, isdn, complaintId, rate);
        }
        return result;
    }

    @Override
    public ParamOut reopenComplain(String token, String locale, Long complaintId, String isdn, String content) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            return new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            return new ComplaintController().reopenComplain(hibernateHelper, locale, isdn, complaintId, content);
        }
    }

    @Override
    public ParamOut closeComplain(String token, String locale, Long complaintId, String isdn) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            return new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            return new ComplaintController().closeComplain(hibernateHelper, locale, isdn, complaintId);
        }
    }

    @Override
    public ParamOut rateComplain(String token, String locale, String complaintId, String isdn, String rate) {
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            return new ParamOut(wsRespone.getErrorCode(), wsRespone.getErrorDecription());
        } else {
            return new ComplaintController().rateComplain(hibernateHelper, locale, isdn, complaintId, rate);
        }
    }
}
