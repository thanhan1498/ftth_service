/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.target.controller;

import com.viettel.bccs.cm.controller.BaseController;
import com.viettel.bccs.cm.controller.TokenController;
import com.viettel.bccs.cm.controller.TransLogController;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import com.viettel.eafs.util.SpringUtil;
import java.util.HashMap;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.brcd.ws.model.output.ProvinceOut;

/**
 *
 * @author cuongdm
 */
public class TargetControllerImpl extends BaseController implements TargetOnlineController {

    private transient HibernateHelper hibernateHelper;

    public HibernateHelper getHibernateHelper() {
        if (this.hibernateHelper == null) {
            this.hibernateHelper = (HibernateHelper) SpringUtil.getBean("hibernateHelper");
            setHibernateHelper(this.hibernateHelper);
        }
        return hibernateHelper;
    }

    public void setHibernateHelper(HibernateHelper hibernateHelper) {
        this.hibernateHelper = hibernateHelper;
    }

    @Override
    public ParamOut getGeneralTarget(String token, String locale, Long staffId, String branch, String center, String date) {
        ParamOut paramOut;
        Long sTimeTr = System.currentTimeMillis();
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            paramOut = new ParamOut();
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            paramOut = new TargetReportController().getGeneralTarget(hibernateHelper, staffId, branch, center, date);
        }
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("staffId", staffId);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, TargetControllerImpl.class.getName(), "getGeneralTarget", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(paramOut));
        }
        return paramOut;
    }

    @Override
    public ParamOut getTarget(String token, String locale, Long staffId, Long formId) {
        ParamOut paramOut;
        Long sTimeTr = System.currentTimeMillis();
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            paramOut = new ParamOut();
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            paramOut = new TargetReportController().getListTarget(hibernateHelper, staffId, formId);
        }
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("staffId", staffId);
            paramTr.put("formId", formId);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, TargetControllerImpl.class.getName(), "getTarget", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(paramOut));
        }
        return paramOut;
    }

    @Override
    public ProvinceOut getBranch(String token, String locale, Long staffId) {
        ProvinceOut paramOut;
        Long sTimeTr = System.currentTimeMillis();
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            paramOut = new ProvinceOut();
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            paramOut = new TargetReportController().getProvinces(hibernateHelper, locale, staffId);
        }
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("staffId", staffId);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, TargetControllerImpl.class.getName(), "getTarget", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(paramOut));
        }
        return paramOut;
    }

    @Override
    public ParamOut getCenterOfBranch(String token, String locale, String provinceCode, Long staffId) {
        ParamOut paramOut;
        Long sTimeTr = System.currentTimeMillis();
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            paramOut = new ParamOut();
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            paramOut = new TargetReportController().getCenterOfBranch(hibernateHelper, provinceCode, staffId);
        }
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("provinceCode", provinceCode);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, TargetControllerImpl.class.getName(), "getTarget", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(paramOut));
        }
        return paramOut;
    }

    @Override
    public ParamOut getStaffOfCenter(String token, String locale, String province, String center) {
        ParamOut paramOut;
        Long sTimeTr = System.currentTimeMillis();
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            paramOut = new ParamOut();
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            paramOut = new TargetReportController().getStaffOfCenter(hibernateHelper, province, center);
        }
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("center", center);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, TargetControllerImpl.class.getName(), "getStaffOfCenter", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(paramOut));
        }
        return paramOut;
    }

    @Override
    public ParamOut getTargetReport(String token, String locale, Long staffLoginId, Long targetId, String province, String center, String staffCode, String date) {
        ParamOut paramOut;
        Long sTimeTr = System.currentTimeMillis();
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            paramOut = new ParamOut();
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            paramOut = new TargetReportController().getTargetReport(hibernateHelper, staffLoginId, targetId, province, center, staffCode, date);
        }
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("center", center);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, TargetControllerImpl.class.getName(), "getStaffOfCenter", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(paramOut));
        }
        return paramOut;
    }

    @Override
    public ParamOut getTargetGrapth(String token, String locale, String receiverCode, String targetId) {
        ParamOut paramOut;
        Long sTimeTr = System.currentTimeMillis();
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            paramOut = new ParamOut();
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            paramOut = new TargetReportController().getTargetGrapth(hibernateHelper, receiverCode, targetId);
        }
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("receiverCode", receiverCode);
            paramTr.put("targetId", targetId);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, TargetControllerImpl.class.getName(), "getTargetGrapth", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(paramOut));
        }
        return paramOut;
    }

    @Override
    public ParamOut getWarningTargetDetail(String token, String locale, String receiverCode, Long targetId) {
        ParamOut paramOut;
        Long sTimeTr = System.currentTimeMillis();
        WSRespone wsRespone = new TokenController().validateToken(hibernateHelper, token, locale);
        if (wsRespone != null && !Constants.ERROR_CODE_0.equals(wsRespone.getErrorCode())) {
            paramOut = new ParamOut();
            paramOut.setErrorCode(wsRespone.getErrorCode());
            paramOut.setErrorDecription(wsRespone.getErrorDecription());
        } else {
            paramOut = new TargetReportController().getWarningTargetDetail(hibernateHelper, receiverCode, targetId);
        }
        Long eTimeTr = System.currentTimeMillis();
        HashMap paramTr = new HashMap();
        try {
            paramTr.put("token", token);
            paramTr.put("locale", locale);
            paramTr.put("receiverCode", receiverCode);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
        } finally {
            new TransLogController().insert(hibernateHelper, token, TargetControllerImpl.class.getName(), "getWarningTargetDetail", LogUtils.toJson(paramTr), (eTimeTr - sTimeTr), LogUtils.toJson(paramOut));
        }
        return paramOut;
    }
}
