/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.target.controller;

import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.ProvinceOut;

/**
 *
 * @author cuongdm
 */
public interface TargetOnlineController { 

    ParamOut getGeneralTarget(String token, String locale, Long staffId, String branch, String center, String date);

    ParamOut getTarget(String token, String locale, Long staffId, Long formId);

    ProvinceOut getBranch(String token, String locale, Long staffId);

    ParamOut getCenterOfBranch(String token, String locale, String provinceCode, Long staffId);

    ParamOut getStaffOfCenter(String token, String locale, String province, String center);

    ParamOut getTargetReport(String token, String locale, Long staffLoginId, Long targetId, String province, String center, String staffCode, String date);

    ParamOut getTargetGrapth(String token, String locale, String receiverCode, String targetId);

    ParamOut getWarningTargetDetail(String token, String locale, String receiverCode, Long targetId);
}
