/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.target.controller;

import com.viettel.bccs.cm.controller.AreaController;
import com.viettel.bccs.cm.controller.BaseController;
import com.viettel.bccs.cm.dao.ShopDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.LogUtils;
import com.viettel.bccs.target.model.RankingObject;
import com.viettel.bccs.target.model.Target;
import com.viettel.bccs.target.model.TargetResultOnline;
import com.viettel.bccs.target.model.TargetResultOnlineReport;
import com.viettel.bccs.target.model.TargetWarning;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.ws.model.output.CustomerOut;
import com.viettel.brcd.ws.model.output.ParamOut;
import com.viettel.brcd.ws.model.output.Province;
import com.viettel.brcd.ws.model.output.ProvinceOut;
import com.viettel.eafs.dataaccess.helper.HibernateHelper;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author cuongdm
 */
public class TargetReportController extends BaseController {

    private static final String REPORT_VIEW_OBJECT = "2";
    private static final String REPORT_VIEW_FORM = "1";

    private static final String UNIT_VTC = "1";
    private static final String UNIT_BRANCH = "2";
    private static final String UNIT_SHOWROOM = "3";

    public TargetReportController() {
        logger = Logger.getLogger(TargetReportController.class);
    }

    public ParamOut getStaffOfCenter(HibernateHelper hibernateHelper, String province, String center) {
        Session sessionTarget = null;
        ParamOut paramOut = new ParamOut();
        paramOut.setErrorCode(Constants.ERROR_CODE_1);
        try {
            sessionTarget = hibernateHelper.getSession(Constants.SESSION_TARGET);
            StringBuilder sql = new StringBuilder("SELECT a.staff_code     staffcode, "
                    + "       a.name, "
                    + "       a.province, "
                    + "       b.shop_code || ' - ' || b.center_code address "
                    + "  FROM bccs_im.staff a, bccs_im.shop b "
                    + " WHERE a.status = 1 "
                    + "   AND a.shop_id = b.shop_id "
                    + "   AND a.TYPE = 5 ");
            //and province = ? group by center
            List param = new ArrayList();
            if (province != null && !province.isEmpty()) {
                sql.append(" and a.province = ? ");
                param.add(province);
            }
            if (center != null && !center.isEmpty()) {
                sql.append(" and b.center_code like ?");
                param.add("%" + center + "%");
            }
            sql.append(" order by a.province, b.center_code, b.shop_code, a.staff_code  ");
            Query query = sessionTarget.createSQLQuery(sql.toString())
                    .addScalar("staffCode", Hibernate.STRING)
                    .addScalar("name", Hibernate.STRING)
                    .addScalar("province", Hibernate.STRING)
                    .addScalar("address", Hibernate.STRING)
                    .setResultTransformer(Transformers.aliasToBean(Staff.class));
            for (int i = 0; i < param.size(); i++) {
                query.setParameter(i, param.get(i));
            }
            List<Staff> list = query.list();
            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription("Success");
            paramOut.setListStaff(list);
        } catch (Exception ex) {
            ex.printStackTrace();
            paramOut.setErrorDecription(ex.getMessage());
        } finally {
            closeSessions(sessionTarget);
        }
        return paramOut;
    }

    public ParamOut getCenterOfBranch(HibernateHelper hibernateHelper, String provinceCode, Long staffId) {
        Session sessionTarget = null;
        Session sessionCm = null;
        ParamOut paramOut = new ParamOut();
        paramOut.setErrorCode(Constants.ERROR_CODE_1);
        try {
            sessionTarget = hibernateHelper.getSession(Constants.SESSION_TARGET);
            sessionCm = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            Staff staff = new StaffDAO().findById(sessionCm, staffId);
            if (staff == null) {
                paramOut.setErrorDecription("Can not find staff");
                return paramOut;
            }
            Shop shop = new ShopDAO().findById(sessionCm, staff.getShopId());
            if (shop == null) {
                paramOut.setErrorDecription("Can not find Shop");
                return paramOut;
            }
            List param = new ArrayList();

            StringBuilder sql = new StringBuilder("select center from (SELECT a.center_code center FROM bccs_im.shop a WHERE a.status = 1 ");
            sql.append(" and a.agent_type in (92) ");
            sql.append(" and a.center_code is not null ");

            if (provinceCode != null && !provinceCode.isEmpty()) {
                sql.append(" and province = ? ");
                param.add(provinceCode);
            }
            if (UNIT_SHOWROOM.equals(shop.getShopType())) {
                sql.append(" and a.shop_id = ? ");
                param.add(shop.getShopId());
            }
            sql.append(" group by center_code ) order by center ");
            Query query = sessionTarget.createSQLQuery(sql.toString());
            for (int i = 0; i < param.size(); i++) {
                query.setParameter(i, param.get(i));
            }
            List list = query.list();
            List<Shop> lstShop = new ArrayList<Shop>();
            if (list != null && !list.isEmpty()) {
                for (Object obj : list) {
                    Shop s = new Shop();
                    s.setShopCode(obj.toString());
                    lstShop.add(s);
                }
            }
            paramOut.setListCenter(lstShop);
            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription("Success");
        } catch (Exception ex) {
            ex.printStackTrace();
            paramOut.setErrorDecription(ex.getMessage());
        } finally {
            closeSessions(sessionTarget, sessionCm);
        }
        return paramOut;
    }

    public ParamOut getGeneralTarget(HibernateHelper hibernateHelper, Long staffId, String branch, String center, String date) {
        Session sessionTarget = null;
        Session sessionCm = null;
        ParamOut paramOut = new ParamOut();
        paramOut.setErrorCode(Constants.ERROR_CODE_1);
        try {
            sessionTarget = hibernateHelper.getSession(Constants.SESSION_TARGET);
            sessionCm = hibernateHelper.getSession(Constants.SESSION_CM_POS);

            Staff staff = new StaffDAO().findById(sessionCm, staffId);
            if (staff == null) {
                paramOut.setErrorDecription("Can not find staff");
                return paramOut;
            }
            Shop shop = new ShopDAO().findById(sessionCm, staff.getShopId());
            if (shop == null) {
                paramOut.setErrorDecription("Can not find Shop");
                return paramOut;
            }
            Long shopType = Long.parseLong(shop.getShopType());
            Long assignmentType = shopType;
            String sql;
            Query query;
            if (branch != null && !branch.isEmpty() && (center == null || center.isEmpty())) {
                assignmentType = Long.parseLong(UNIT_BRANCH);
            } else if (branch != null && !branch.isEmpty() && center != null && !center.isEmpty()) {
                assignmentType = Long.parseLong(UNIT_SHOWROOM);
            }
            if (assignmentType < shopType) {
                paramOut.setErrorDecription("Check your role, you can not find parent data");
                return paramOut;
            }
            List param = new ArrayList();

            sql = "SELECT   receiver_code receiverCode, rank, total_score totalScore, assignment_type assignmentType, shop_path shopPath, rankUp, sumDate "
                    + "  FROM   (SELECT   receiver_code, "
                    + "                   r2.RANK, "
                    + "                   round(total_score, 2) total_score , "
                    + "                   form_assignment_id, "
                    + "                   assignment_type, "
                    + "                   shop_path , "
                    + "                   RANK () "
                    + "                       OVER (PARTITION BY receiver_code, form_assignment_id "
                    + "                             ORDER BY r2.sum_date DESC) "
                    + "                       AS rk, "
                    + "                   lead(rank,1)  over (PARTITION BY receiver_code,form_assignment_id ORDER BY sum_date desc) - rank AS rankUp,"
                    + "                   case when sum_date = trunc(sysdate) then nvl(modify_date, create_date) else sum_date end sumDate "
                    + "            FROM   result_online_rank r2 "
                    + "           WHERE   1=1 "
                    + "                   And r2.sum_date >= TRUNC (to_date(?,'dd/mm/yyyy'), 'MM') "
                    + "                   AND sum_date < TRUNC (to_date(?,'dd/mm/yyyy') + 1) ";
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            if (date == null || date.isEmpty()) {
                date = format.format(new Date());
            }
            param.add(date);
            param.add(date);
            if (!UNIT_VTC.equals(shop.getShopType())) {
                sql += "                      and unit = ? ";
                param.add(shop.getProvince());
            }
            if (branch != null && !branch.isEmpty()) {
                sql += " and unit = ? ";
                param.add(branch);
            }
            if (center != null && !center.isEmpty()) {
                sql += " and center = ? ";
                param.add(center);
            }
            sql += "                  and assignment_type  >= ? "
                    + "               and assignment_type  <= ? + 1 "
                    + "               and shop_path || '_' like ? "
                    + "         ) "
                    + " WHERE   rk = 1 "
                    + " order by assignment_type, rank";
            param.add(assignmentType);
            param.add(assignmentType);
            param.add("%" + shop.getShopId() + "%");

            query = sessionTarget.createSQLQuery(sql).
                    addScalar("receiverCode", Hibernate.STRING).
                    addScalar("totalScore", Hibernate.DOUBLE).
                    addScalar("rank", Hibernate.STRING).
                    addScalar("assignmentType", Hibernate.LONG).
                    addScalar("shopPath", Hibernate.STRING).
                    addScalar("rankUp", Hibernate.STRING).
                    addScalar("sumDate", Hibernate.TIMESTAMP).
                    setResultTransformer(Transformers.aliasToBean(TargetResultOnlineReport.class));
            for (int i = 0; i < param.size(); i++) {
                query.setParameter(i, param.get(i));
            }
            //daibq check level shop
            List<TargetResultOnlineReport> results = query.list();
            List<TargetResultOnlineReport> response = new ArrayList<TargetResultOnlineReport>();
            String top = getParam(sessionTarget, "PERCENT_TOP");
            String bot = getParam(sessionTarget, "PERCENT_BOT");
            Date maxDate = null;
            if (results != null && results.size() > 0) {
                int numParent = 0;
                for (int i = 0; i < results.size(); i++) {
                    TargetResultOnlineReport object = results.get(i);
                    /*set ban ghi cha*/
                    if (object.getAssignmentType().equals(assignmentType)) {
                        object.setIsParent("true");
                        numParent++;
                    }


                    /*set thoi gian du lieu gan nhat*/
                    if (maxDate == null) {
                        maxDate = object.getSumDate();
                    } else {
                        if (maxDate.before(object.getSumDate())) {
                            maxDate = object.getSumDate();
                        }
                    }
                    String[] strShopPath = object.getShopPath().substring(1).split("_");
                    String shopLv2 = "";
                    String shopLv3 = "";
                    Shop shopCheck = null;
                    if (strShopPath.length > 2) {
                        shopLv2 = strShopPath[1].trim();
                        shopLv3 = strShopPath[2].trim();
                    } else if (strShopPath.length == 2) {
                        shopLv2 = strShopPath[1].trim();
                    }
                    if (!shopLv2.isEmpty()) {
                        shopCheck = new ShopDAO().getShopById(sessionTarget, Long.parseLong(shopLv2));
                        if (shopCheck != null && ("1".equals(shopCheck.getShopType()) || "2".equals(shopCheck.getShopType()))) {
                            object.setBranchCode(shopCheck.getShopCode());
                        } else if (shopCheck != null && ("3".equals(shopCheck.getShopType()))) {
                            object.setCenterCode(shopCheck.getCenterCode());
                        }
                    }
                    if (!shopLv3.isEmpty()) {
                        shopCheck = new ShopDAO().getShopById(sessionTarget, Long.parseLong(shopLv3));
                        if (shopCheck != null && ("1".equals(shopCheck.getShopType()) || "2".equals(shopCheck.getShopType()))) {
                            object.setBranchCode(shopCheck.getShopCode());
                        } else if (shopCheck != null && ("3".equals(shopCheck.getShopType()))) {
                            object.setCenterCode(shopCheck.getCenterCode());
                        }
                    }

                    if (object.getAssignmentType() == 4) {
                        object.setStaffCode(object.getReceiverCode());
                    }
                    object.setShopPath(null);
                    response.add(object);
                    //daibq end
                }
                Double numTop = top != null ? Double.parseDouble(top) * (results.size() - numParent) / 100.0d : 0d;
                Double numBot = bot != null ? Double.parseDouble(bot) * (results.size() - numParent) / 100.0d : 0d;

                for (int i = numParent; i < results.size(); i++) {
                    TargetResultOnlineReport object = results.get(i);
                    /*set color top and bottom for object*/
                    int topPosition = i + 1;
                    int botPosition = results.size() - i;
                    if (!"true".equals(object.getIsParent())) {
                        if (topPosition <= numTop) {
                            object.setColor("green");
                        } else if (botPosition <= numBot) {
                            object.setColor("red");
                        }
                    }
                }
            }
            if (maxDate != null) {
                paramOut.setDateTimeStr(format.format(maxDate));
            }
            paramOut.setListTargetResult(response);
            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription("Success");
        } catch (Exception ex) {
            ex.printStackTrace();
            paramOut.setErrorDecription(ex.getMessage());
        } finally {
            closeSessions(sessionTarget, sessionCm);
        }
        return paramOut;
    }

    public ParamOut getListTarget(HibernateHelper hibernateHelper, Long staffId, Long formId) {
        CustomerOut result = null;
        Session sessionTarget = null;
        Session sessionCm = null;
        ParamOut paramOut = new ParamOut();
        paramOut.setErrorCode(Constants.ERROR_CODE_1);
        try {
            sessionTarget = hibernateHelper.getSession(Constants.SESSION_TARGET);
            sessionCm = hibernateHelper.getSession(Constants.SESSION_CM_POS);
            Staff staff = new StaffDAO().findById(sessionCm, staffId);
            if (staff == null) {
                paramOut.setErrorDecription("Can not find staff");
                return paramOut;
            }
            Shop shop = new ShopDAO().findById(sessionCm, staff.getShopId());
            if (shop == null) {
                paramOut.setErrorDecription("Can not find Shop");
                return paramOut;
            }
            Query query;
            String sql;
            if (formId == null) {
                if (UNIT_VTC.equals(shop.getShopType())) {
                    sql = "SELECT  tp.TARGET_PROFILE_ID targetId, tp.target_name targetName "
                            + "                    FROM target_assignment t1, form_assignment fa , TARGET_FORM_MAPPING tfm, target_profile tp "
                            + "                    WHERE t1.status = 1 "
                            + "                      AND fa.status = 1"
                            + "                      and t1.receiver_type = 3 "
                            + "                      and t1.form_assignment_id = fa.form_assignment_id "
                            + "                      and trunc(t1.assignment_time, 'MM') = trunc(sysdate, 'MM') "
                            + "                      and EXISTS (select c.shop_code from bccs_im.shop c where c.status = 1 and c.shop_code = t1.receiver_code and c.agent_type = 91 and c.company = 2) "
                            + "                      and fa.form_assignment_id = tfm.form_assignment_id "
                            + "                      and tfm.target_profile_id = tp.target_profile_id "
                            + "                    GROUP BY tp.TARGET_PROFILE_ID , tp.target_name";
                    query = sessionTarget.createSQLQuery(sql)
                            .addScalar("targetId", Hibernate.LONG)
                            .addScalar("targetName", Hibernate.STRING)
                            .setResultTransformer(Transformers.aliasToBean(Target.class));
                } else if (UNIT_BRANCH.equals(shop.getShopType())) {
                    sql = "SELECT   tp.TARGET_PROFILE_ID targetId, tp.target_name targetName "
                            + "  FROM   target_assignment t1, form_assignment fa, TARGET_FORM_MAPPING tfm, target_profile tp  "
                            + " WHERE       t1.status = 1  "
                            + "         AND fa.status = 1 "
                            + "         AND t1.form_assignment_id = fa.form_assignment_id  "
                            + "         AND trunc(t1.assignment_time, 'MM') = trunc(sysdate, 'MM') "
                            + "         AND ( (t1.receiver_type = 3  "
                            + "                AND (UPPER (t1.receiver_code) = ?  "
                            + "                     OR EXISTS  "
                            + "                           (SELECT   shop_code  "
                            + "                              FROM   bccs_im.shop  "
                            + "                             WHERE       status = 1  "
                            + "                                     AND shop_code = t1.receiver_code  "
                            + "                                     AND agent_type = 92)))  "
                            + "              OR (t1.receiver_type = 4  "
                            + "                  AND EXISTS  "
                            + "                         (SELECT   t2.name  "
                            + "                            FROM       bccs_im.staff t2  "
                            + "                                   INNER JOIN  "
                            + "                                       bccs_im.shop t3  "
                            + "                                   ON t2.shop_id = t3.shop_id  "
                            + "                           WHERE       t2.status = 1  "
                            + "                                   AND t2.staff_code = t1.receiver_code  "
                            + "                                   AND t3.shop_path || '_' LIKE ?)))  "
                            + "   and fa.form_assignment_id = tfm.form_assignment_id "
                            + "   and tfm.target_profile_id = tp.target_profile_id "
                            + "GROUP BY   tp.TARGET_PROFILE_ID , tp.target_name";
                    query = sessionTarget.createSQLQuery(sql)
                            .addScalar("targetId", Hibernate.LONG)
                            .addScalar("targetName", Hibernate.STRING)
                            .setResultTransformer(Transformers.aliasToBean(Target.class));
                    query.setParameter(0, shop.getShopCode());
                    query.setParameter(1, "%_" + shop.getShopId().toString() + "_%");
                } else {
                    sql = "select tp.TARGET_PROFILE_ID targetId, tp.target_name targetName  "
                            + "                     from target_assignment t1, form_assignment fa , TARGET_FORM_MAPPING tfm, target_profile tp  "
                            + "                    where t1.status = 1   "
                            + "                      AND fa.status = 1"
                            + "                      and trunc(t1.assignment_time, 'MM') = trunc(sysdate, 'MM')   "
                            + "                      and t1.form_assignment_id = fa.form_assignment_id   "
                            + "                      and ((t1.receiver_type = 3 and t1.receiver_code = ?) or   "
                            + "                          (t1.receiver_type = 4 and exists   "
                            + "                           (select t2.name   "
                            + "                               from bccs_im.staff t2   "
                            + "                              inner join bccs_im.shop t3   "
                            + "                                 on t2.shop_id = t3.shop_id   "
                            + "                              where t2.status = 1   "
                            + "                                and t2.staff_code = t1.receiver_code   "
                            + "                                and t3.shop_path || '_' like ?)))   "
                            + "   and fa.form_assignment_id = tfm.form_assignment_id "
                            + "   and tfm.target_profile_id = tp.target_profile_id "
                            + "                    group by tp.TARGET_PROFILE_ID , tp.target_name";
                    query = sessionTarget.createSQLQuery(sql)
                            .addScalar("targetId", Hibernate.LONG)
                            .addScalar("targetName", Hibernate.STRING)
                            .setResultTransformer(Transformers.aliasToBean(Target.class));
                    query.setParameter(0, shop.getShopCode());
                    query.setParameter(1, "%_" + shop.getShopId().toString() + "_%");
                }
            } else {
                sql = "select tp.TARGET_PROFILE_ID targetId , tp.target_name targetName  "
                        + "  from Form_Assignment fa, TARGET_FORM_MAPPING tfm, target_profile tp "
                        + " where fa.form_assignment_id = tfm.form_assignment_id "
                        + "   and tfm.target_profile_id = tp.target_profile_id "
                        + " and fa.form_assignment_id = ? ";
                query = sessionTarget.createSQLQuery(sql)
                        .addScalar("targetId", Hibernate.LONG)
                        .addScalar("targetName", Hibernate.STRING)
                        .setResultTransformer(Transformers.aliasToBean(Target.class));
                query.setParameter(0, formId);
            }
            List<Target> list = query.list();
            paramOut.setListTarget(list);
            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription("Success");
        } catch (Exception ex) {
            ex.printStackTrace();
            paramOut.setErrorDecription(ex.getMessage());
        } finally {
            closeSessions(sessionTarget, sessionCm);
            LogUtils.info(logger, "CustomerController.searchCustomer:result=" + LogUtils.toJson(result));
        }
        return paramOut;
    }

    public ProvinceOut getProvinces(HibernateHelper hibernateHelper, String locale, Long staffId) {
        ProvinceOut result = null;
        Session cmPosSession = null;
        try {
            cmPosSession = hibernateHelper.getSession(Constants.SESSION_CM_POS);

            List<Province> provinces = new AreaController().getProvinces(cmPosSession);
            List<Province> provincesTemp = new ArrayList<Province>();
            Staff staff = new StaffDAO().findById(cmPosSession, staffId);
            if (staff == null) {
                provinces = null;
            }
            Shop shop = new ShopDAO().findById(cmPosSession, staff == null ? 0l : staff.getShopId());
            if (shop == null) {
                provinces = null;
            }
            if (shop != null && shop.getProvinceCode() != null && provinces != null) {
                if (!"VTC".equals(shop.getProvinceCode())) {
                    for (int i = 0; i < provinces.size(); i++) {
                        if (provinces.get(i).getProvinceCode().equals(shop.getProvinceCode())) {
                            provincesTemp.add(provinces.get(i));
                            break;
                        }
                    }
                } else {
                    provincesTemp.addAll(provinces);
                }
            }
            result = new ProvinceOut(Constants.ERROR_CODE_0, LabelUtil.getKey("common.success", locale), provincesTemp);
        } catch (Exception ex) {
            LogUtils.error(logger, ex.getMessage(), ex);
            result = new ProvinceOut(Constants.ERROR_CODE_1, LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));

        } finally {
            closeSessions(cmPosSession);
            //LogUtils.info(logger, "AreaController.getProvinces:result=" + LogUtils.toJson(result));
        }
        return result;
    }

    private List<TargetResultOnlineReport> getTargetResult(Session target, Session cmPos, Long staffIf, String provinceCode, String centerCode, String staffCode, Long targetId, String date) throws Exception {
        Staff staff = new StaffDAO().findById(cmPos, staffIf);
        if (staff == null) {
            return null;
        }
        Shop shop = new ShopDAO().findById(cmPos, staff.getShopId());
        if (shop == null) {
            return null;
        }
        if (!"VTC".equals(shop.getProvinceCode())) {
            provinceCode = shop.getProvinceCode();
        }

        List param = new ArrayList();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        StringBuilder select = new StringBuilder(" SELECT   trunc(sum_date,'mm') sumDate, "
                + "         form_assignment_id formAssignmentId, "
                + "         target_assignment_id targetAssignmentId, "
                + "         receiver_code receiverCode, "
                + "         target_profile_id targetProfileId, "
                + "         target_name targetName, "
                + "         round(scores, 2) scores, "
                + "         round(scores_bonus, 2) scoresBonus, "
                + "         RANK rank, "
                + "         result_day resultDay, "
                + "         compare_values_yesterday compareValuesYesterday, "
                + "         compare_percent_yesterday comparePercentYesterday, "
                + "         result_month resultMonth, "
                + "         percent_result_month percentResultMonth, "
                + "         compare_values_last_month compareValuesLastMonth, "
                + "         compare_percent_last_month comparePercentLastMonth,"
                + "         target_values targetValues,"
                + "         unit,"
                + "         center,"
                + "         weight,"
                + "         receiver_type receiverType,"
                + "                 CASE  "
                + "                     WHEN result_month  >= "
                + "                    target_values / TO_NUMBER (TO_CHAR (LAST_DAY (sum_date), 'dd'))* TO_NUMBER (TO_CHAR (case when trunc(sum_date, 'mm') = trunc(sysdate, 'mm') then trunc(sysdate) else sum_date end, 'dd')) "
                + "                     THEN  "
                + "                         'green'  "
                + "                     ELSE  "
                + "                         'red'  "
                + "                 END  "
                + "             color, "
                + "         assignment_type assignmentType,"
                + "         to_char(sum_date, 'dd/mm/yyyy hh24:mi:ss') createDatetime, "
                + "         rename1 receiverName FROM   (SELECT   rs.*, "
                + "                   RANK () "
                + "                       OVER ( "
                + "                           PARTITION BY trunc(sum_date,'MM'), "
                + "                                        form_assignment_id, target_profile_id, "
                + "                                        receiver_code "
                + "                           ORDER BY sum_date desc, result_online_id DESC) "
                + "                       AS rk,"
                + "                       CASE "
                + "                         WHEN receiver_type = 4  "
                + "                         THEN "
                + "                            (SELECT   REPLACE(name, ' ', '_') "
                + "                             FROM   bccs_im.staff "
                + "                             WHERE   staff_code = receiver_code) "
                + "                         ELSE "
                + "                            (SELECT   REPLACE(name, ' ', '_') "
                + "                             FROM   bccs_im.shop "
                + "                            WHERE   shop_code = receiver_code) "
                + "                       END rename1 "
                + "            FROM   result_online rs"
                + "            where status = 1  "
                + "            and status_action = 1 "
                + "            and sum_date >= trunc(to_date(?,'dd/mm/yyyy'),'MM') "
                + "            and sum_date < trunc(to_date(?,'dd/mm/yyyy')) + 1 ) "
                + " WHERE   rk = 1 ");

        /*if (staffCode != null && !staffCode.isEmpty()) {
            select.append(" and receiver_Code = ? ");
            param.add(staffCode);
        }
        if (targetId != null && targetId != 0) {
            select.append(" and target_Profile_Id = ? ");
            param.add(targetId);
        }*/
        if (date == null || date.isEmpty()) {
            date = format.format(new Date());
        }
        param.add(date);
        param.add(date);
        String shopType = "";
        if (targetId != null && targetId > 0) {

            select.append(" AND assignment_type >= ? ");
            select.append(" AND assignment_type <= ?  +1 ");
            //daibq
            if ((staffCode == null || staffCode.isEmpty())
                    && (provinceCode == null || provinceCode.isEmpty())
                    && (centerCode == null || centerCode.isEmpty())) {
                shopType = shop.getShopType();
            } else if (provinceCode != null && !provinceCode.isEmpty()
                    && (staffCode == null || staffCode.isEmpty())
                    && (centerCode == null || centerCode.isEmpty())) {
                shopType = "2";
            } else {
                shopType = "3";
            }
            param.add(shopType);
            param.add(shopType);

            if (provinceCode != null && !provinceCode.isEmpty()) {
                select.append(" and unit = ? ");
                param.add(provinceCode);
            }
            if (centerCode != null && !centerCode.isEmpty()) {
                select.append(" and center = ? ");
                param.add(centerCode);
            }
            if (staffCode != null && !staffCode.isEmpty()) {
                select.append(" and receiver_Code = ? ");
                param.add(staffCode);
            }
            select.append(" and target_Profile_Id = ? ");
            param.add(targetId);

            if (provinceCode == null || provinceCode.isEmpty()) {
            }
        } else {
            if (UNIT_VTC.equals(shop.getShopType())) {
            } else if (UNIT_BRANCH.equals(shop.getShopType())) {
                select.append(" AND unit = ?   ");
                param.add(shop.getProvince());
            } else {
                select.append(" AND unit = ?   ");
                param.add(shop.getProvince());

                String sql = "select center_code from bccs_im.shop where status = 1 and shop_id = ?";
                Query query1 = target.createSQLQuery(sql);
                query1.setParameter(0, staff.getShopId());
                List data = query1.list();
                if (data != null && !data.isEmpty() && data.get(0) != null) {
                    select.append(" AND center = ?   ");
                    param.add(data.get(0).toString());
                }
            }
            if ((provinceCode == null || provinceCode.isEmpty())
                    && (centerCode == null || centerCode.isEmpty())
                    && (staffCode == null || staffCode.isEmpty())) {
                // select.append(" AND receiver_Code = 'TVC'   ");
                //daibq
                select.append(" AND receiver_Code = 'VTC'   ");
            } else if (provinceCode != null && !provinceCode.isEmpty()
                    && (centerCode == null || centerCode.isEmpty())
                    && (staffCode == null || staffCode.isEmpty())) {
                select.append(" AND receiver_Code = ?   ");
                param.add(provinceCode);
            } else if ((provinceCode != null && !provinceCode.isEmpty())
                    && (centerCode != null && !centerCode.isEmpty())
                    && (staffCode == null || staffCode.isEmpty())) {
                //daibq them dk
                String shopCodeByCenter = new ShopDAO().getShopCodeByCenterCode(target, centerCode);
                if (shopCodeByCenter == null || shopCodeByCenter.isEmpty()) {
                    return null;
                }
                select.append(" AND unit = ?   ");
                param.add(provinceCode);
                //daibq
                select.append(" AND center = ?   ");
                param.add(centerCode);
                select.append(" AND receiver_Code = ?   ");
                param.add(shopCodeByCenter);
            } else if ((provinceCode != null && !provinceCode.isEmpty())
                    && (centerCode != null && !centerCode.isEmpty())
                    && (staffCode != null && !staffCode.isEmpty())) {
                select.append(" AND unit = ?   ");
                param.add(provinceCode);
                select.append(" AND center = ?   ");
                param.add(centerCode);
                select.append(" AND receiver_Code = ?   ");
                param.add(staffCode);
            } else {
                select.append(" AND false   ");
            }
        }
        select.append(" and shop_Path || '_' like ? ");
        param.add("%_" + shop.getShopId().toString() + "_%");

        select.append(" order by assignment_type, unit, center, receiver_Code, trunc(create_time, 'mm'), form_Assignment_Id, target_Profile_Id ");
        Query query = target.createSQLQuery(select.toString()).
                addScalar("sumDate", Hibernate.TIMESTAMP).
                addScalar("formAssignmentId", Hibernate.LONG).
                addScalar("targetAssignmentId", Hibernate.LONG).
                addScalar("receiverCode", Hibernate.STRING).
                addScalar("receiverName", Hibernate.STRING).
                addScalar("targetProfileId", Hibernate.LONG).
                addScalar("targetName", Hibernate.STRING).
                addScalar("scores", Hibernate.DOUBLE).
                addScalar("scoresBonus", Hibernate.DOUBLE).
                addScalar("rank", Hibernate.LONG).
                addScalar("resultDay", Hibernate.LONG).
                addScalar("compareValuesYesterday", Hibernate.LONG).
                addScalar("comparePercentYesterday", Hibernate.DOUBLE).
                addScalar("resultMonth", Hibernate.LONG).
                addScalar("percentResultMonth", Hibernate.DOUBLE).
                addScalar("compareValuesLastMonth", Hibernate.LONG).
                addScalar("comparePercentLastMonth", Hibernate.DOUBLE).
                addScalar("targetValues", Hibernate.DOUBLE).
                addScalar("weight", Hibernate.LONG).
                addScalar("unit", Hibernate.STRING).
                addScalar("center", Hibernate.STRING).
                addScalar("receiverType", Hibernate.LONG).
                addScalar("assignmentType", Hibernate.LONG).
                addScalar("createDatetime", Hibernate.STRING).
                addScalar("color", Hibernate.STRING).
                setResultTransformer(Transformers.aliasToBean(TargetResultOnline.class));
        for (int i = 0; i < param.size(); i++) {
            query.setParameter(i, param.get(i));
        }

        List<TargetResultOnline> listReport = query.list();
        if (listReport == null || listReport.isEmpty()) {
            return null;
        }
        Map<Long, List<String>> map = new HashMap();
        List<TargetResultOnlineReport> listData;
        for (TargetResultOnline temp : listReport) {
            if (!map.containsKey(temp.getTargetProfileId())) {
                List<String> listTemp = new ArrayList<String>();
                listTemp.add(temp.getTargetName());
                listTemp.add(null);
                map.put(temp.getTargetProfileId(), listTemp);
            }
            if (shopType.equals(temp.getAssignmentType().toString())) {
                temp.setIsParent("true");
            }
        }
        listData = mergeDataTarget(listReport, map, null);
        String getRank = " SELECT   rank, round(total_score, 2) total_score "
                + "  FROM   (SELECT   r2.RANK, r2.total_score,RANK () OVER (ORDER BY r2.sum_date DESC) AS rk "
                + "            FROM   result_online_rank r2 "
                + "           WHERE       r2.sum_date >= trunc(sysdate,'MM')"
                + "                   AND sum_date < trunc(sysdate +1 ) "
                + "                   AND r2.form_assignment_id = ? "
                + "                   AND receiver_code = ?) "
                + " WHERE   rk = 1";
        query = target.createSQLQuery(getRank);
        if (listData != null && !listData.isEmpty()) {
            for (int i = 0; i < listData.size(); i++) {
                query.setParameter(0, listData.get(i).getFormId());
                query.setParameter(1, listData.get(i).getReceiverCode());
                List data = query.list();
                if (data != null && !data.isEmpty()) {
                    Object[] arr = (Object[]) data.get(0);
                    listData.get(i).setRank(arr[0] != null ? arr[0].toString() : "");
                    if (targetId == null || targetId == 0) {
                        listData.get(i).setTotalScore(arr[1] != null ? Double.parseDouble(arr[1].toString()) : 0d);
                    }
                }
                /*check warning*/
                for (int j = 0; j < listData.get(i).getData().size(); j++) {
                    TargetResultOnline temp = listData.get(i).getData().get(j);
                    Boolean isWarning = isWarning(target, temp.getReceiverCode(), temp.getTargetProfileId());
                    if (isWarning) {
                        listData.get(i).setIsWarning(isWarning.toString());
                        listData.get(i).getListTargetWarning().add(temp.getTargetProfileId());
                    }
                }
            }
        }

        return listData;
    }

    List<TargetResultOnlineReport> mergeDataTarget(List<TargetResultOnline> listReport, Map<Long, List<String>> map, Long formId) throws Exception {
        Map<Long, RankingObject> mapNode = new HashMap<Long, RankingObject>();
        RankingObject rank;
        List<TargetResultOnlineReport> listDataDate = new ArrayList<TargetResultOnlineReport>();
        if (listReport != null && !listReport.isEmpty()) {
            String currentRow = null;
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            TargetResultOnlineReport temp = null;
            for (TargetResultOnline data : listReport) {
                for (Map.Entry<Long, List<String>> entry : map.entrySet()) {
                    //System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
                    if (entry.getKey().equals(data.getTargetProfileId())) {
                        List<String> listData = entry.getValue();
                        String time = listData.get(1);
                        Date tempDate = data.getSumDate();
                        Date maxDate = time == null || time.isEmpty() ? null : format.parse(time);
                        if (tempDate != null && (maxDate == null || tempDate.after(maxDate))) {
                            listData.remove(1);
                            listData.add(format.format(tempDate));
                        }
                        break;
                    }
                }
                if (currentRow == null) {
                    temp = new TargetResultOnlineReport(map);
                    temp.setReceiverCode(data.getReceiverCode());
                    temp.setReceiverType(data.getReceiverType());
                    temp.setReceiverName(data.getReceiverName());
                    temp.setFormId(data.getFormAssignmentId().toString());
                    temp.setDate(format.format(data.getSumDate()));
                    temp.setCenterCode(data.getCenter());
                    temp.setData(data);
                    temp.setIsParent(data.getIsParent());
                    temp.setColor(data.getColor());
                    if (temp.getRank() == null || Long.parseLong(temp.getRank()) > data.getRank()) {
                        temp.setRank(data.getRank() == null ? "" : data.getRank().toString());
                    }
                    temp.setUnit(data.getUnit());
                    currentRow = format.format(data.getSumDate()) + data.getReceiverCode() + data.getFormAssignmentId().toString();
                    listDataDate.add(temp);
                } else {
                    if (currentRow.equals(format.format(data.getSumDate()) + data.getReceiverCode() + data.getFormAssignmentId().toString())) {
                        temp.setData(data);
                    } else {
                        if (!mapNode.containsKey(temp.getReceiverType())) {
                            rank = new RankingObject(temp.getTotalScoreBonus());
                            mapNode.put(temp.getReceiverType(), rank);
                        } else {
                            rank = mapNode.get(temp.getReceiverType());
                            rank.insert(rank.getRoot(), temp.getTotalScoreBonus());
                        }
                        temp = new TargetResultOnlineReport(map);
                        temp.setReceiverCode(data.getReceiverCode());
                        temp.setReceiverType(data.getReceiverType());
                        temp.setReceiverName(data.getReceiverName());
                        temp.setDate(format.format(data.getSumDate()));
                        temp.setFormId(data.getFormAssignmentId().toString());
                        temp.setData(data);
                        temp.setCenterCode(data.getCenter());
                        temp.setRank(data.getRank() == null ? "" : data.getRank().toString());
                        temp.setUnit(data.getUnit());
                        temp.setColor(data.getColor());
                        temp.setIsParent(data.getIsParent());
                        currentRow = format.format(data.getSumDate()) + data.getReceiverCode() + data.getFormAssignmentId().toString();
                        listDataDate.add(temp);
                    }
                }
            }
            TargetResultOnlineReport lastObj = listDataDate.get(listDataDate.size() - 1);
            if (!mapNode.containsKey(lastObj.getReceiverType())) {
                rank = new RankingObject(lastObj.getTotalScoreBonus());
                mapNode.put(temp.getReceiverType(), rank);
            } else {
                rank = mapNode.get(lastObj.getReceiverType());
                rank.insert(rank.getRoot(), lastObj.getTotalScoreBonus());
            }
        }
        /*if (formId == null) {
            for (int i = 0; i < listDataDate.size(); i++) {
                TargetResultOnlineReport obj = listDataDate.get(i);
                rank = mapNode.get(obj.getReceiverType());
                int rankIndex = rank.getRank(rank.getRoot(), obj.getTotalScoreBonus());
                obj.setRank(String.valueOf(rankIndex + 1));
            }
        }*/
        return listDataDate;
    }

    public ParamOut getTargetReport(HibernateHelper hibernateHelper, Long staffLoginId, Long targetId, String province, String center, String staffCode, String date) {
        Session sessionTarget = null;
        Session sessionCm = null;
        ParamOut paramOut = new ParamOut();
        paramOut.setErrorCode(Constants.ERROR_CODE_1);
        try {
            sessionTarget = hibernateHelper.getSession(Constants.SESSION_TARGET);
            sessionCm = hibernateHelper.getSession(Constants.SESSION_CM_POS);

            List<TargetResultOnlineReport> list = getTargetResult(sessionTarget, sessionCm, staffLoginId, province, center, staffCode, targetId, date);

            paramOut.setListTargetResult(list);
            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription("Success");
        } catch (Exception ex) {
            ex.printStackTrace();
            paramOut.setErrorDecription(ex.getMessage());
        } finally {
            closeSessions(sessionTarget, sessionCm);
        }
        return paramOut;
    }

    public ParamOut getTargetGrapth(HibernateHelper hibernateHelper, String receiverCode, String targetId) {
        Session sessionTarget = null;
        ParamOut paramOut = new ParamOut();
        paramOut.setErrorCode(Constants.ERROR_CODE_1);
        try {
            sessionTarget = hibernateHelper.getSession(Constants.SESSION_TARGET);

            String sql = "SELECT   TO_CHAR (sum_date, 'dd/mm/yyyy') AS createDatetime, "
                    + "         target_name targetName, "
                    + "         result_day resultDay "
                    + "  FROM   result_online "
                    + " WHERE       target_profile_id = ? "
                    + "         AND receiver_code = ? ";

            String current = " AND sum_date >= TRUNC (SYSDATE, 'MM')  "
                    + "         AND sum_date < TRUNC (SYSDATE + 1)  "
                    + " order by sum_date";

            String lastMonth = " AND sum_date >= TRUNC(TRUNC (SYSDATE, 'MM') - 1, 'MM')   "
                    + "         AND sum_date < TRUNC (SYSDATE, 'MM') "
                    + " order by sum_date";

            Query query = sessionTarget.createSQLQuery(sql + current).
                    addScalar("targetName", Hibernate.STRING).
                    addScalar("resultDay", Hibernate.LONG).
                    addScalar("createDatetime", Hibernate.STRING).
                    setResultTransformer(Transformers.aliasToBean(TargetResultOnline.class));
            query.setParameter(0, targetId);
            query.setParameter(1, receiverCode);
            List<TargetResultOnline> list = query.list();
            paramOut.setCurrentMonth(list);

            query = sessionTarget.createSQLQuery(sql + lastMonth).
                    addScalar("targetName", Hibernate.STRING).
                    addScalar("resultDay", Hibernate.LONG).
                    addScalar("createDatetime", Hibernate.STRING).
                    setResultTransformer(Transformers.aliasToBean(TargetResultOnline.class));
            query.setParameter(0, targetId);
            query.setParameter(1, receiverCode);
            list = query.list();
            paramOut.setLastMonth(list);

            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription("Success");
        } catch (Exception ex) {
            ex.printStackTrace();
            paramOut.setErrorDecription(ex.getMessage());
        } finally {
            closeSessions(sessionTarget);
        }
        return paramOut;
    }

    public boolean isWarning(Session target, String receiverCode, Long targetId) {
        try {
            Query query;
            String sql;
            /*Kiem tra xem chi tieu da hoan thanh chua*/
            sql = "select isSuccess from(\n"
                    + "SELECT   case when result_month >= target_values then 1 else 0 end isSuccess\n"
                    + "    FROM   result_online\n"
                    + "   WHERE       sum_date >= TRUNC (SYSDATE, 'mm')\n"
                    + "           AND receiver_code = ?\n"
                    + "           AND target_profile_id = ?\n"
                    + "ORDER BY   sum_date DESC\n"
                    + ") where rownum = 1";
            query = target.createSQLQuery(sql);
            query.setParameter(0, receiverCode);
            query.setParameter(1, targetId);
            List list = query.list();
            if (list != null && !list.isEmpty() && "1".equals(list.get(0).toString())) {
                return false;
            }

            /*Lay tham so 'so ngay' xet suy giam*/
            String paramVal = getParam(target, "NUM_DATE");
            if (paramVal != null && !paramVal.isEmpty()) {
                sql = "SELECT   TO_CHAR (sum_date, 'dd/mm/yyyy') AS createDatetime,   "
                        + "           target_name targetName,   "
                        + "           result_day resultDay,   "
                        + "           result_day   "
                        + "           - LAG (result_day, 1, 0) OVER (ORDER BY sum_date)   "
                        + "               AS scores   "
                        + "    FROM   result_online   "
                        + "   WHERE       sum_date >= TRUNC (SYSDATE - ? - 1)   "
                        + "           AND sum_date >= TRUNC (SYSDATE, 'mm') "
                        + "           AND receiver_code = ?   "
                        + "           AND target_profile_id = ?   "
                        + "ORDER BY   sum_date";
                query = target.createSQLQuery(sql).
                        addScalar("createDatetime", Hibernate.STRING).
                        addScalar("targetName", Hibernate.STRING).
                        addScalar("resultDay", Hibernate.LONG).
                        addScalar("scores", Hibernate.DOUBLE).
                        setResultTransformer(Transformers.aliasToBean(TargetResultOnline.class));
                query.setParameter(0, Long.parseLong(paramVal));
                query.setParameter(1, receiverCode);
                query.setParameter(2, targetId);
                List<TargetResultOnline> data = query.list();
                if (data != null && data.size() > Long.parseLong(paramVal)) {
                    boolean isIncrease = false;
                    for (int i = 1; i < data.size(); i++) {
                        if (data.get(i).getScores() != null && data.get(i).getScores() > 0) {
                            isIncrease = true;
                            break;
                        }
                    }
                    //dai bq fix neu co ban ghi canh bao thi dua ra canh bao
                    if (!isIncrease) {
                        return true;
                    }
                }
            }
            /*Lay tham so 'ngay xet'*/
            String paramValDate = getParam(target, "DATE_CHECK_WARNING");
            String paramValPercent = getParam(target, "PERCENT_TARGET");
            if (paramValDate != null && !paramValDate.isEmpty()
                    && paramValPercent != null && !paramValPercent.isEmpty()) {
                sql = "SELECT   percent_result_month "
                        + "  FROM   (SELECT   r.*, "
                        + "                   RANK () "
                        + "                       OVER ( "
                        + "                           PARTITION BY form_assignment_id, "
                        + "                                        receiver_code, "
                        + "                                        target_profile_id "
                        + "                           ORDER BY sum_date DESC) "
                        + "                       AS rk "
                        + "            FROM   result_online r "
                        + "           WHERE       sum_date >= trunc(sysdate,'MM') + ? "
                        + "                   AND target_profile_id = ? "
                        + "                   AND receiver_code = ?) "
                        + " WHERE   rk = 1 "
                        + " and percent_result_month < ?";
                query = target.createSQLQuery(sql);
                query.setParameter(0, Long.parseLong(paramValDate));
                query.setParameter(1, targetId);
                query.setParameter(2, receiverCode);
                query.setParameter(3, Double.parseDouble(paramValPercent));
                List data = query.list();
                if (data != null && !data.isEmpty()) {
                    return true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    private String getParam(Session target, String paramCode) {
        String sql = "select param_value from bccs_im.ap_param where param_type = 'WARNING_TARGET' and param_code = ? and status = 1";
        Query query = target.createSQLQuery(sql);
        query.setParameter(0, paramCode);
        List list = query.list();
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0).toString();
    }

    public ParamOut getWarningTargetDetail(HibernateHelper hibernateHelper, String receiverCode, Long targetId) {
        Session sessionTarget = null;
        ParamOut paramOut = new ParamOut();
        List<TargetWarning> list = new ArrayList<TargetWarning>();
        paramOut.setListTargetWarning(list);
        paramOut.setErrorCode(Constants.ERROR_CODE_1);
        try {
            sessionTarget = hibernateHelper.getSession(Constants.SESSION_TARGET);
            String paramVal = getParam(sessionTarget, "NUM_DATE");
            Query query;
            String sql;
            if (paramVal != null && !paramVal.isEmpty()) {
                sql = "SELECT   TO_CHAR (sum_date, 'dd/mm/yyyy') AS createDatetime,   "
                        + "           target_name targetName,   "
                        + "           result_day resultDay,   "
                        + "           result_day - LAG (result_day, 1, 0) OVER (ORDER BY sum_date) AS scores   "
                        + "    FROM   result_online   "
                        + "   WHERE       sum_date >= TRUNC (SYSDATE - ? - 1)   "
                        + "           AND receiver_code = ?   "
                        + "           AND target_profile_id = ?   "
                        + "ORDER BY   sum_date";
                query = sessionTarget.createSQLQuery(sql).
                        addScalar("createDatetime", Hibernate.STRING).
                        addScalar("targetName", Hibernate.STRING).
                        addScalar("resultDay", Hibernate.LONG).
                        addScalar("scores", Hibernate.DOUBLE).
                        setResultTransformer(Transformers.aliasToBean(TargetResultOnline.class));
                query.setParameter(0, Long.parseLong(paramVal));
                query.setParameter(1, receiverCode);
                query.setParameter(2, targetId);
                List<TargetResultOnline> data = query.list();
                if (data != null && data.size() > Long.parseLong(paramVal)) {
                    boolean isIncrease = false;
                    for (int i = 1; i < data.size(); i++) {
                        if (data.get(i).getScores() != null && data.get(i).getScores() > 0) {
                            isIncrease = true;
                            break;
                        }
                    }
                    if (!isIncrease) {
                        TargetWarning temp = new TargetWarning();
                        temp.setDescription("Has been declining for " + paramVal + "  consecutive days");
                        temp.setTargetName(data.get(0).getTargetName());
                        temp.setListDateVal(data);
                        temp.setTypeWarning("1");
                        list.add(temp);
                    }
                }
            }
            /*Lay tham so 'ngay xet'*/
            String paramValDate = getParam(sessionTarget, "DATE_CHECK_WARNING");
            String paramValPercent = getParam(sessionTarget, "PERCENT_TARGET");
            if (paramValDate != null && !paramValDate.isEmpty()
                    && paramValPercent != null && !paramValPercent.isEmpty()) {
                sql = "SELECT   target_name targetName "
                        + "  FROM   (SELECT   r.*, "
                        + "                   RANK () "
                        + "                       OVER ( "
                        + "                           PARTITION BY form_assignment_id, "
                        + "                                        receiver_code, "
                        + "                                        target_profile_id "
                        + "                           ORDER BY sum_date DESC) "
                        + "                       AS rk "
                        + "            FROM   result_online r "
                        + "           WHERE       sum_date >= trunc(sysdate,'MM') + ? "
                        + "                   AND target_profile_id = ? "
                        + "                   AND receiver_code = ?) "
                        + " WHERE   rk = 1 "
                        + " and percent_result_month < ?";
                query = sessionTarget.createSQLQuery(sql);
                query.setParameter(0, Long.parseLong(paramValDate));
                query.setParameter(1, targetId);
                query.setParameter(2, receiverCode);
                query.setParameter(3, Double.parseDouble(paramValPercent));
                List data = query.list();
                if (data != null && !data.isEmpty()) {
                    TargetWarning temp = new TargetWarning();
                    temp.setDescription("Less than " + paramValPercent + "% of the target");
                    temp.setTargetName(data.get(0).toString());
                    temp.setTypeWarning("2");
                    list.add(temp);
                }
            }
            paramOut.setErrorCode(Constants.ERROR_CODE_0);
            paramOut.setErrorDecription("Success");
        } catch (Exception ex) {
            ex.printStackTrace();
            paramOut.setErrorDecription(ex.getMessage());
        } finally {
            closeSessions(sessionTarget);
        }
        return paramOut;
    }
}
