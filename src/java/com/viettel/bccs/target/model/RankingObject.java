/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.target.model;

/**
 *
 * @author cuongdm
 */
public class RankingObject {

    public RankingObject() {
    }

    public RankingObject(Double data) {
        this.root = new Node();
        this.root.data = data;
        this.root.left = null;
        this.root.right = null;
        this.root.leftSize = 0;
    }
    private Node root;

    /**
     * @return the root
     */
    public Node getRoot() {
        return root;
    }

    /**
     * @param root the root to set
     */
    public void setRoot(Node root) {
        this.root = root;
    }

    public class Node {

        Double data;
        Node left, right;
        int leftSize;
    }

    public Node newNode(Double data) {
        Node temp = new Node();
        temp.data = data;
        temp.left = null;
        temp.right = null;
        temp.leftSize = 0;
        return temp;
    }

// Inserting a new Node.  
    public Node insert(Node root, Double data) {
        if (root == null) {
            return newNode(data);
        }

        // Updating size of left subtree.  
        if (data.compareTo(root.data) > 0) {
            root.left = insert(root.left, data);
            root.leftSize++;
        } else if (data.compareTo(root.data) < 0) {
            root.right = insert(root.right, data);
        }

        return root;
    }

// Function to get Rank of a Node x.  
    public int getRank(Node root, Double x) {
        // Step 1.  
        if (root.data.compareTo(x) == 0) {
            return root.leftSize;
        }

        // Step 2.  
        if (x.compareTo(root.data) > 0) {
            if (root.left == null) {
                return -1;
            } else {
                return getRank(root.left, x);
            }
        } // Step 3.  
        else {
            if (root.right == null) {
                return -1;
            } else {
                int rightSize = getRank(root.right, x);
                return root.leftSize + 1 + rightSize;
            }
        }
    }
}
