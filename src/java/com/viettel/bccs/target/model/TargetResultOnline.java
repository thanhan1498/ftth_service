/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.target.model;

import java.util.Date;

/**
 *
 * @author cuongdm
 */
public class TargetResultOnline {

    private Long resultOnlineId;
    private Date sumDate;
    private Long formAssignmentId;
    private Long targetAssignmentId;
    private String receiverCode;
    private String receiverName;
    private Long shopId;
    private Long parShopId;
    private String shopPath;
    private String shopType;
    private String shopCode;
    private String parShopCode;
    private String center;
    private String unit;
    private Long targetProfileId;
    private String targetName;
    private Double targetValues;
    private Double scores;
    private Double bonus;
    private Long rank;
    private Long resultDay;
    private Long compareValuesYesterday;
    private Double comparePercentYesterday;
    private Long resultMonth;
    private Double percentResultMonth;
    private Long compareValuesLastMonth;
    private Double comparePercentLastMonth;
    private int stt;
    private Long receiverType;
    private Double scoresBonus;
    private Long statusAction;
    private Long status;
    private Long weight;
    private String createDatetime;
    private Long assignmentType;
    private String color;
    private String isParent;

    public Long getAssignmentType() {
        return assignmentType;
    }

    public void setAssignmentType(Long assignmentType) {
        this.assignmentType = assignmentType;
    }

    /**
     * @return the resultOnlineId
     */
    public Long getResultOnlineId() {
        return resultOnlineId;
    }

    /**
     * @param resultOnlineId the resultOnlineId to set
     */
    public void setResultOnlineId(Long resultOnlineId) {
        this.resultOnlineId = resultOnlineId;
    }

    /**
     * @return the sumDate
     */
    public Date getSumDate() {
        return sumDate;
    }

    /**
     * @param sumDate the sumDate to set
     */
    public void setSumDate(Date sumDate) {
        this.sumDate = sumDate;
    }

    /**
     * @return the formAssignmentId
     */
    public Long getFormAssignmentId() {
        return formAssignmentId;
    }

    /**
     * @param formAssignmentId the formAssignmentId to set
     */
    public void setFormAssignmentId(Long formAssignmentId) {
        this.formAssignmentId = formAssignmentId;
    }

    /**
     * @return the targetAssignmentId
     */
    public Long getTargetAssignmentId() {
        return targetAssignmentId;
    }

    /**
     * @param targetAssignmentId the targetAssignmentId to set
     */
    public void setTargetAssignmentId(Long targetAssignmentId) {
        this.targetAssignmentId = targetAssignmentId;
    }

    /**
     * @return the receiverCode
     */
    public String getReceiverCode() {
        return receiverCode;
    }

    /**
     * @param receiverCode the receiverCode to set
     */
    public void setReceiverCode(String receiverCode) {
        this.receiverCode = receiverCode;
    }

    /**
     * @return the receiverName
     */
    public String getReceiverName() {
        return receiverName;
    }

    /**
     * @param receiverName the receiverName to set
     */
    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    /**
     * @return the shopId
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * @param shopId the shopId to set
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * @return the parShopId
     */
    public Long getParShopId() {
        return parShopId;
    }

    /**
     * @param parShopId the parShopId to set
     */
    public void setParShopId(Long parShopId) {
        this.parShopId = parShopId;
    }

    /**
     * @return the shopPath
     */
    public String getShopPath() {
        return shopPath;
    }

    /**
     * @param shopPath the shopPath to set
     */
    public void setShopPath(String shopPath) {
        this.shopPath = shopPath;
    }

    /**
     * @return the shopType
     */
    public String getShopType() {
        return shopType;
    }

    /**
     * @param shopType the shopType to set
     */
    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    /**
     * @return the shopCode
     */
    public String getShopCode() {
        return shopCode;
    }

    /**
     * @param shopCode the shopCode to set
     */
    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    /**
     * @return the parShopCode
     */
    public String getParShopCode() {
        return parShopCode;
    }

    /**
     * @param parShopCode the parShopCode to set
     */
    public void setParShopCode(String parShopCode) {
        this.parShopCode = parShopCode;
    }

    /**
     * @return the center
     */
    public String getCenter() {
        return center;
    }

    /**
     * @param center the center to set
     */
    public void setCenter(String center) {
        this.center = center;
    }

    /**
     * @return the unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * @param unit the unit to set
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * @return the targetProfileId
     */
    public Long getTargetProfileId() {
        return targetProfileId;
    }

    /**
     * @param targetProfileId the targetProfileId to set
     */
    public void setTargetProfileId(Long targetProfileId) {
        this.targetProfileId = targetProfileId;
    }

    /**
     * @return the targetName
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * @param targetName the targetName to set
     */
    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    /**
     * @return the targetValues
     */
    public Double getTargetValues() {
        return targetValues;
    }

    /**
     * @param targetValues the targetValues to set
     */
    public void setTargetValues(Double targetValues) {
        this.targetValues = targetValues;
    }

    /**
     * @return the scores
     */
    public Double getScores() {
        return scores;
    }

    /**
     * @param scores the scores to set
     */
    public void setScores(Double scores) {
        this.scores = scores;
    }

    /**
     * @return the bonus
     */
    public Double getBonus() {
        return bonus;
    }

    /**
     * @param bonus the bonus to set
     */
    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }

    /**
     * @return the rank
     */
    public Long getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(Long rank) {
        this.rank = rank;
    }

    /**
     * @return the resultDay
     */
    public Long getResultDay() {
        return resultDay;
    }

    /**
     * @param resultDay the resultDay to set
     */
    public void setResultDay(Long resultDay) {
        this.resultDay = resultDay;
    }

    /**
     * @return the compareValuesYesterday
     */
    public Long getCompareValuesYesterday() {
        return compareValuesYesterday;
    }

    /**
     * @param compareValuesYesterday the compareValuesYesterday to set
     */
    public void setCompareValuesYesterday(Long compareValuesYesterday) {
        this.compareValuesYesterday = compareValuesYesterday;
    }

    /**
     * @return the comparePercentYesterday
     */
    public Double getComparePercentYesterday() {
        return comparePercentYesterday;
    }

    /**
     * @param comparePercentYesterday the comparePercentYesterday to set
     */
    public void setComparePercentYesterday(Double comparePercentYesterday) {
        this.comparePercentYesterday = comparePercentYesterday;
    }

    /**
     * @return the resultMonth
     */
    public Long getResultMonth() {
        return resultMonth;
    }

    /**
     * @param resultMonth the resultMonth to set
     */
    public void setResultMonth(Long resultMonth) {
        this.resultMonth = resultMonth;
    }

    /**
     * @return the percentResultMonth
     */
    public Double getPercentResultMonth() {
        return percentResultMonth;
    }

    /**
     * @param percentResultMonth the percentResultMonth to set
     */
    public void setPercentResultMonth(Double percentResultMonth) {
        this.percentResultMonth = percentResultMonth;
    }

    /**
     * @return the compareValuesLastMonth
     */
    public Long getCompareValuesLastMonth() {
        return compareValuesLastMonth;
    }

    /**
     * @param compareValuesLastMonth the compareValuesLastMonth to set
     */
    public void setCompareValuesLastMonth(Long compareValuesLastMonth) {
        this.compareValuesLastMonth = compareValuesLastMonth;
    }

    /**
     * @return the comparePercentLastMonth
     */
    public Double getComparePercentLastMonth() {
        return comparePercentLastMonth;
    }

    /**
     * @param comparePercentLastMonth the comparePercentLastMonth to set
     */
    public void setComparePercentLastMonth(Double comparePercentLastMonth) {
        this.comparePercentLastMonth = comparePercentLastMonth;
    }

    /**
     * @return the stt
     */
    public int getStt() {
        return stt;
    }

    /**
     * @param stt the stt to set
     */
    public void setStt(int stt) {
        this.stt = stt;
    }

    /**
     * @return the receiverType
     */
    public Long getReceiverType() {
        return receiverType;
    }

    /**
     * @param receiverType the receiverType to set
     */
    public void setReceiverType(Long receiverType) {
        this.receiverType = receiverType;
    }

    /**
     * @return the scoresBonus
     */
    public Double getScoresBonus() {
        return scoresBonus;
    }

    /**
     * @param scoresBonus the scoresBonus to set
     */
    public void setScoresBonus(Double scoresBonus) {
        this.scoresBonus = scoresBonus;
    }

    /**
     * @return the statusAction
     */
    public Long getStatusAction() {
        return statusAction;
    }

    /**
     * @param statusAction the statusAction to set
     */
    public void setStatusAction(Long statusAction) {
        this.statusAction = statusAction;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the weight
     */
    public Long getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(Long weight) {
        this.weight = weight;
    }

    /**
     * @return the createDatetime
     */
    public String getCreateDatetime() {
        return createDatetime;
    }

    /**
     * @param createDatetime the createDatetime to set
     */
    public void setCreateDatetime(String createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the isParent
     */
    public String getIsParent() {
        return isParent;
    }

    /**
     * @param isParent the isParent to set
     */
    public void setIsParent(String isParent) {
        this.isParent = isParent;
    }

}
