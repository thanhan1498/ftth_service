/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.target.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author cuongdm
 */
public class TargetResultOnlineReport {
    
    private String stt;
    private String unit;
    private String receiverCode;
    private Long receiverType;
    private String receiverName;
    private String formId;
    private String rank;
    private String date;
    private Double totalScore = 0d;
    private Double totalScoreBonus = 0d;
    private String ki;
    private String createTime;
    private Date sumDate;
    private String isWarning;
    private String centerCode;
    private List<Long> listTargetWarning = new ArrayList<Long>();
    private List<TargetResultOnline> data;
    //daibq
    private Long assignmentType;
    private String branchCode;
    private String staffCode;
    private String shopPath;
    private String color;
    private String rankUp;
    private String isParent;
    
    public String getShopPath() {
        return shopPath;
    }
    
    public void setShopPath(String shopPath) {
        this.shopPath = shopPath;
    }
    
    public String getBranchCode() {
        return branchCode;
    }
    
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    
    public String getStaffCode() {
        return staffCode;
    }
    
    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }
    
    public TargetResultOnlineReport() {
    }
    
    public TargetResultOnlineReport(Map<Long, List<String>> map) {
        data = new ArrayList<TargetResultOnline>();
        /*for (Map.Entry<Long, List<String>> entry : map.entrySet()) {
            List<String> list = entry.getValue();
            TargetResultOnline temp = new TargetResultOnline();
            temp.setTargetProfileId(entry.getKey());
            temp.setTargetName(list.get(0));
            data.add(temp);
        }*/
    }
    
    public void setData(TargetResultOnline target) {
        
        TargetResultOnline temp = new TargetResultOnline();
        temp.setTargetValues(target.getTargetValues());
        temp.setBonus(target.getBonus());
        temp.setRank(target.getRank());
        temp.setResultDay(target.getResultDay());
        temp.setCompareValuesYesterday(target.getCompareValuesYesterday());
        temp.setComparePercentYesterday(target.getComparePercentYesterday());
        temp.setResultMonth(target.getResultMonth());
        temp.setPercentResultMonth(target.getPercentResultMonth());
        temp.setCompareValuesLastMonth(target.getCompareValuesLastMonth());
        temp.setComparePercentLastMonth(target.getComparePercentLastMonth());
        temp.setScores(target.getScores());
        temp.setScoresBonus(target.getScoresBonus());
        temp.setWeight(target.getWeight());
        temp.setCreateDatetime(target.getCreateDatetime());
        temp.setReceiverCode(target.getReceiverCode());
        temp.setTargetProfileId(target.getTargetProfileId());
        temp.setTargetName(target.getTargetName());
        temp.setUnit(target.getUnit());
        temp.setCenter(target.getCenter());
        temp.setAssignmentType(target.getAssignmentType());
        temp.setColor(target.getColor());
        temp.setIsParent(target.getIsParent());
        totalScore += (target.getScores() == null ? 0d : target.getScores());
        totalScoreBonus += (target.getScoresBonus() == null ? 0d : target.getScoresBonus());
        data.add(temp);
        /*
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getTargetProfileId().equals(target.getTargetProfileId())) {
                    data.get(i).setTargetValues(target.getTargetValues());
                    data.get(i).setBonus(target.getBonus());
                    data.get(i).setRank(target.getRank());
                    data.get(i).setResultDay(target.getResultDay());
                    data.get(i).setCompareValuesYesterday(target.getCompareValuesYesterday());
                    data.get(i).setComparePercentYesterday(target.getComparePercentYesterday());
                    data.get(i).setResultMonth(target.getResultMonth());
                    data.get(i).setPercentResultMonth(target.getPercentResultMonth());
                    data.get(i).setCompareValuesLastMonth(target.getCompareValuesLastMonth());
                    data.get(i).setComparePercentLastMonth(target.getComparePercentLastMonth());
                    data.get(i).setScores(target.getScores());
                    data.get(i).setScoresBonus(target.getScoresBonus());
                    data.get(i).setWeight(target.getWeight());
                    data.get(i).setCreateDatetime(target.getCreateDatetime());
                    totalScore += (target.getScores() == null ? 0d : target.getScores());
                    totalScoreBonus += (target.getScoresBonus() == null ? 0d : target.getScoresBonus());
                    break;
                }
            }*/
    }

    /**
     * @return the stt
     */
    public String getStt() {
        return stt;
    }

    /**
     * @param stt the stt to set
     */
    public void setStt(String stt) {
        this.stt = stt;
    }

    /**
     * @return the receiverCode
     */
    public String getReceiverCode() {
        return receiverCode;
    }

    /**
     * @param receiverCode the receiverCode to set
     */
    public void setReceiverCode(String receiverCode) {
        this.receiverCode = receiverCode;
    }

    /**
     * @return the data
     */
    public List<TargetResultOnline> getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(List<TargetResultOnline> data) {
        this.data = data;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the totalScore
     */
    public Double getTotalScore() {
        return totalScore;
    }

    /**
     * @param totalScore the totalScore to set
     */
    public void setTotalScore(Double totalScore) {
        this.totalScore = totalScore;
    }

    /**
     * @return the rank
     */
    public String getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(String rank) {
        this.rank = rank;
    }

    /**
     * @return the totalScoreBonus
     */
    public Double getTotalScoreBonus() {
        return totalScoreBonus;
    }

    /**
     * @param totalScoreBonus the totalScoreBonus to set
     */
    public void setTotalScoreBonus(Double totalScoreBonus) {
        this.totalScoreBonus = totalScoreBonus;
    }

    /**
     * @return the formId
     */
    public String getFormId() {
        return formId;
    }

    /**
     * @param formId the formId to set
     */
    public void setFormId(String formId) {
        this.formId = formId;
    }

    /**
     * @return the unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * @param unit the unit to set
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * @return the ki
     */
    public String getKi() {
        return ki;
    }

    /**
     * @param ki the ki to set
     */
    public void setKi(String ki) {
        this.ki = ki;
    }

    /**
     * @return the receiverName
     */
    public String getReceiverName() {
        return receiverName;
    }

    /**
     * @param receiverName the receiverName to set
     */
    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    /**
     * @return the receiverType
     */
    public Long getReceiverType() {
        return receiverType;
    }

    /**
     * @param receiverType the receiverType to set
     */
    public void setReceiverType(Long receiverType) {
        this.receiverType = receiverType;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the sumDate
     */
    public Date getSumDate() {
        return sumDate;
    }

    /**
     * @param sumDate the sumDate to set
     */
    public void setSumDate(Date sumDate) {
        this.sumDate = sumDate;
    }

    /**
     * @return the isWarning
     */
    public String getIsWarning() {
        return isWarning;
    }

    /**
     * @param isWarning the isWarning to set
     */
    public void setIsWarning(String isWarning) {
        this.isWarning = isWarning;
    }

    /**
     * @return the listTargetWarning
     */
    public List<Long> getListTargetWarning() {
        return listTargetWarning;
    }

    /**
     * @param listTargetWarning the listTargetWarning to set
     */
    public void setListTargetWarning(List<Long> listTargetWarning) {
        this.listTargetWarning = listTargetWarning;
    }

    /**
     * @return the centerCode
     */
    public String getCenterCode() {
        return centerCode;
    }

    /**
     * @param centerCode the centerCode to set
     */
    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }
    
    public Long getAssignmentType() {
        return assignmentType;
    }
    
    public void setAssignmentType(Long assignmentType) {
        this.assignmentType = assignmentType;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the rankUp
     */
    public String getRankUp() {
        return rankUp;
    }

    /**
     * @param rankUp the rankUp to set
     */
    public void setRankUp(String rankUp) {
        this.rankUp = rankUp;
    }

    /**
     * @return the isParent
     */
    public String getIsParent() {
        return isParent;
    }

    /**
     * @param isParent the isParent to set
     */
    public void setIsParent(String isParent) {
        this.isParent = isParent;
    }
    
}
