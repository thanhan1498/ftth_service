/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bccs.target.model;

import java.util.List;

/**
 *
 * @author cuongdm
 */
public class TargetWarning {

    private String description;
    private String typeWarning;
    private String targetName; 
    private List<TargetResultOnline> listDateVal;

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the typeWarning
     */
    public String getTypeWarning() {
        return typeWarning;
    }

    /**
     * @param typeWarning the typeWarning to set
     */
    public void setTypeWarning(String typeWarning) {
        this.typeWarning = typeWarning;
    }

    /**
     * @return the listDateVal
     */
    public List<TargetResultOnline> getListDateVal() {
        return listDateVal;
    }

    /**
     * @param listDateVal the listDateVal to set
     */
    public void setListDateVal(List<TargetResultOnline> listDateVal) {
        this.listDateVal = listDateVal;
    }

    /**
     * @return the targetName
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * @param targetName the targetName to set
     */
    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

}
