package com.viettel.brcd.common;

import java.util.Properties;

/**
 *
 * @author linhlh2
 */
public interface Configuration {

    public void addProperties(Properties properties);

    public boolean contains(String key);

    public String get(String key);

    public String[] getArray(String key);

    public Properties getProperties();

    public Properties getProperties(String prefix, boolean removePrefix);

    public void removeProperties(Properties properties);

    public void set(String key, String value);
}
