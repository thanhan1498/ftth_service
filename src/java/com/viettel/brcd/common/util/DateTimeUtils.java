package com.viettel.brcd.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * @author vanghv1
 * @version 1.0
 * @since 20/08/2014 4:53 PM
 */
public class DateTimeUtils {

    //<editor-fold defaultstate="collapsed" desc="DATE_PATTERN">
    public static final String DATE_PATTERN_MMyyyy = "MM/yyyy";
    public static final String DATE_PATTERN_ddMMyyyy = "dd/MM/yyyy";
    public static final String DATE_PATTERN_ddMMyyyyHHmmss = "dd/MM/yyyy HH:mm:ss";
    public static final String DATE_PATTERN_ddMMyyyyHH24miss = "dd/MM/yyyy HH24:mi:ss";
    public static final String DATE_PATTERN_yyyyMMddHHmmss = "yyyyMMddHHmmss";
    //</editor-fold>
    private static HashMap<String, SimpleDateFormat> mapDateFormat = new HashMap<String, SimpleDateFormat>();

    public DateTimeUtils() {
    }

    public static SimpleDateFormat getSimpleDateFormat(String pattern) {
        if (pattern == null) {
            return null;
        }
        pattern = pattern.trim();
        if (pattern.isEmpty()) {
            return null;
        }
        if (!mapDateFormat.containsKey(pattern)) {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            sdf.setLenient(false);
            mapDateFormat.put(pattern, sdf);
        }
        return mapDateFormat.get(pattern);
    }

    public static Date toDate(String value, String pattern) throws ParseException {
        if (pattern == null) {
            return null;
        }
        SimpleDateFormat sdf = getSimpleDateFormat(pattern);
        if (sdf == null) {
            return null;
        }
        return sdf.parse(value);
    }

    public static Date toDateMMyyyy(String value) throws ParseException {
        if (!ValidateUtils.isDateMMyyyy(value)) {
            return null;
        }
        return getSimpleDateFormat(DATE_PATTERN_MMyyyy).parse(value);
    }

    public static Date toDateddMMyyyy(String value) throws ParseException {
        if (!ValidateUtils.isDateddMMyyyy(value)) {
            return null;
        }
        return getSimpleDateFormat(DATE_PATTERN_ddMMyyyy).parse(value);
    }

    public static Date toDateddMMyyyyHHmmss(String value) throws ParseException {
        if (!ValidateUtils.isDateddMMyyyyHHmmss(value)) {
            return null;
        }
        return getSimpleDateFormat(DATE_PATTERN_ddMMyyyyHHmmss).parse(value);
    }

    public static Date toDateyyyyMMddHHmmss(String value) throws ParseException {
        if (!ValidateUtils.isDateddMMyyyyHHmmss(value)) {
            return null;
        }
        return getSimpleDateFormat(DATE_PATTERN_yyyyMMddHHmmss).parse(value);
    }

    public static String formatDate(Date date, String pattern) {
        if (date == null || pattern == null) {
            return null;
        }
        SimpleDateFormat sdf = getSimpleDateFormat(pattern);
        if (sdf == null) {
            return null;
        }
        return sdf.format(date);
    }

    public static String formatMMyyyy(Object date) {
        if (date == null) {
            return null;
        }
        return getSimpleDateFormat(DATE_PATTERN_MMyyyy).format(date);
    }

    public static String formatddMMyyyy(Object date) {
        if (date == null) {
            return null;
        }
        return getSimpleDateFormat(DATE_PATTERN_ddMMyyyy).format(date);
    }

    public static String formatddMMyyyyHHmmss(Object date) {
        if (date == null) {
            return null;
        }
        return getSimpleDateFormat(DATE_PATTERN_ddMMyyyyHHmmss).format(date);
    }

    public static String formatyyyyMMddHHmmss(Object date) {
        if (date == null) {
            return null;
        }
        return getSimpleDateFormat(DATE_PATTERN_yyyyMMddHHmmss).format(date);
    }

    public static Date getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public static Date getFirstDayOfCurrentMonth() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public static Date getFirstDayOfMonthBefore() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) - 1, cal.get(Calendar.DATE));
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public static Integer getDayBetween(Date fromDate, Date toDate) {
        if (fromDate == null || toDate == null) {
            return null;
        }
        int DAY_IN_MILLIS = 24 * 60 * 60 * 1000;
        int dayBetween = (int) ((toDate.getTime() - fromDate.getTime()) / DAY_IN_MILLIS);
        return dayBetween;
    }

    public static Long getTimeBetween(Date fromDate, Date toDate) {
        if (fromDate == null || toDate == null) {
            return null;
        }
        Long timeBetween = toDate.getTime() - fromDate.getTime();
        return timeBetween;
    }

    public static Long getMinuteBetween(Date fromDate, Date toDate) {
        if (fromDate == null || toDate == null) {
            return null;
            
        }
        Long MINUTE_IN_MILLIS = 60000L;
        Long minuteBetween = (toDate.getTime() - fromDate.getTime()) / MINUTE_IN_MILLIS;
        return minuteBetween;
    }

    public static Date addTime(Date date, Long time) {
        if (date == null) {
            return null;
        }
        return new Date(date.getTime() + time);
    }

    public static Date addMonth(Date date, int month) {
        Date addDate = date;
        if (date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.MONTH, month);
            addDate = cal.getTime();
        }
        return addDate;
    }

    public static Date addDay(Date date, int day) {
        Date addDate = date;
        if (date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DATE, day);
            addDate = cal.getTime();
        }
        return addDate;
    }

    public static Date addMinute(Date date, int minute) {
        Date addDate = date;
        if (date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.MINUTE, minute);
            addDate = cal.getTime();
        }
        return addDate;
    }

    public static java.sql.Date toSqlDate(java.util.Date utilDate) {
        return new java.sql.Date(utilDate.getTime());
    }

    public static Date getSysdate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }
    
    public static String convertDateTimeToString(Date date) throws Exception {
        if (date == null){
            return "";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return dateFormat.format(date);
        } catch (Exception e) {
            throw e;
        }
    }
    
    public static String convertDateTimeToStringV2(Date date, String pattern) throws Exception {
        if (date == null){
            return "";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        try {
            return dateFormat.format(date);
        } catch (Exception e) {
            throw e;
        }
    }
}
