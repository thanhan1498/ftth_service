/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.common.util;

import com.viettel.bccs.cm.bussiness.common.FTPBusiness;
import com.viettel.bccs.cm.model.FTPBean;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.ws.model.output.SaveFileOutput;
import com.viettel.brcd.ws.vsa.ResourceBundleUtil;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

/**
 *
 * @author vietnn
 */
public class FileSaveProcess {

    private Logger logger = Logger.getLogger("loggerAction");

    public String getFile(String imgName, String isdn, Logger logger) {
        try {
            String data = "";
            FTPBean ftpBean = new FTPBean();

            String uploadDir = ResourceBundleUtil.getResource("ftp_dir");

            try {
                System.out.println("-------" + ftpBean.getHost() + "--------" + ftpBean.getUsername() + "----------" + ftpBean.getPassword() + "-------" + ftpBean.getWorkingDir() + "-----" + uploadDir + "----");
                data = FTPBusiness.retrieveFileFromFTPServer(ftpBean.getHost(), ftpBean.getUsername(), ftpBean.getPassword(), imgName, uploadDir + File.separator + isdn, ftpBean.getWorkingDir());
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("\n error get image " + e);
            }
            return data;
            // Luu file thanh cong
        } catch (Exception ex) {
            logger.info("Error" + ex);
        }
        return "";// Xay ra loi khi luu file
    }

    public boolean saveFile(String imgName, String data, String account, Logger logger) {
        SaveFileOutput out = new SaveFileOutput();
        FileOutputStream fos = null;
        boolean res = true;
        try {
            FTPBean ftpBean = new FTPBean();
            logger.info("\n start saveFile");
            String imagePath = ftpBean.getWorkingDir();
            String forder = imagePath + File.separator + account;
            logger.info("\n forder url" + forder);
            File forderFile = new File(forder);
            if (!forderFile.exists() && !forderFile.isDirectory()) {
                boolean check = forderFile.mkdir();
                logger.info("\n create forder " + check);
            }
            imagePath = imagePath + File.separator + account + File.separator + imgName;
            logger.info("\n imageName" + imagePath);
            File file = new File(imagePath);
            fos = new FileOutputStream(file);
            fos.write(Base64.decodeBase64(data));
            fos.close();

            String uploadDir = ResourceBundleUtil.getResource("ftp_dir") + "/" + account;

            try {
                System.out.println("-------" + ftpBean.getHost() + "--------" + ftpBean.getUsername() + "----------" + ftpBean.getPassword() + "-------" + ftpBean.getWorkingDir() + "------" + imagePath + "-----" + uploadDir + "----");
                res = FTPBusiness.putFileToFTPServer(imagePath, imgName, ftpBean.getHost(), ftpBean.getUsername(), ftpBean.getPassword(), uploadDir);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("\n error upload image " + e);
            }
            if (!res) {
                System.out.println("\n error upload imageName " + imagePath + " on sever 10.79.94.22");
            }
            // up to server
            return true;
        } catch (Exception ex) {
            out.setResCode(-1);
            res = false;
        }
        return res;// Xay ra loi khi luu file
    }

    public boolean saveFileCus(String imgName, String data) {
        FileOutputStream fos = null;
        try {
            String imagePath = ResourceBundleUtil.getResource("temp.folder.image.file");
            imagePath = imagePath + imgName;
            File file = new File(imagePath);
            fos = new FileOutputStream(file);
            fos.write(Base64.decodeBase64(data));
            fos.close();
            FTPBean ftpBean = new FTPBean();

//            String uploadDir = ResourceBundleUtil.getResource("ftp_dir");
            boolean res = false;
            try {
                System.out.println("-------" + ftpBean.getHost() + "--------" + ftpBean.getUsername() + "----------" + ftpBean.getPassword() + "-------" + ftpBean.getWorkingDir() + "------" + imagePath + "-----" + imagePath + "----");
                res = FTPBusiness.putFileToFTPServer(ftpBean.getHost(), ftpBean.getUsername(), ftpBean.getPassword(), imagePath, ftpBean.getWorkingDir(), imagePath);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("\n error upload image " + e);
            }
            if (!res) {
                System.out.println("\n error upload imageName " + imagePath + " on sever 10.79.94.22");
            }
            // up to server
            return true;
            // Luu file thanh cong
        } catch (Exception ex) {
            System.out.println("Error" + ex);
        }
        return false;// Xay ra loi khi luu file
    }

    public boolean saveFileNotFtp(String imgName, String data, String account, Logger logger, String dir) {
        SaveFileOutput out = new SaveFileOutput();
        FileOutputStream fos = null;
        boolean res = true;
        try {
            FTPBean ftpBean = new FTPBean();
            logger.info("\n start saveFile");
            String imagePath = ftpBean.getWorkingDir();
            String forder = imagePath + File.separator + account;
            logger.info("\n forder url" + forder);
            File forderFile = new File(forder);
            if (!forderFile.exists() && !forderFile.isDirectory()) {
                boolean check = forderFile.mkdir();
                logger.info("\n create forder " + check);
            }
            imagePath = imagePath + File.separator + account + File.separator + imgName;
            logger.info("\n imageName" + imagePath);
            File file = new File(imagePath);
            fos = new FileOutputStream(file);
            fos.write(Base64.decodeBase64(data));
            fos.close();
            try {
                String uploadDir = ResourceBundleUtil.getResource(dir);
                res = FTPBusiness.putFileToFTPServer(imagePath, imgName, ftpBean.getHost(), ftpBean.getUsername(), ftpBean.getPassword(), uploadDir + File.separator + account);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("\n error upload image " + e);
            }
            // up to server
            return true;
        } catch (Exception ex) {
            out.setResCode(-1);
            res = false;
        }
        return res;// Xay ra loi khi luu file
    }

    public String getFileNotFtp(String imgName, String folder, Logger logger, String ftpDir) {
        String strData = "";
        try {
            FTPBean ftpBean = new FTPBean();
            String uploadDir = ResourceBundleUtil.getResource(ftpDir);
            String[] listDir = uploadDir.split(",");
            if (listDir != null && listDir.length > 0) {
                for (int i = 0; i < listDir.length; i++) {
                    strData = FTPBusiness.retrieveFileFromFTPServer(ftpBean.getHost(), ftpBean.getUsername(), ftpBean.getPassword(), imgName, uploadDir + File.separator + folder, ftpBean.getWorkingDir());
                    if (strData != null && !strData.isEmpty()) {
                        break;
                    }
                }
                if (strData == null || strData.isEmpty()) {
                    logger.info("\n get saveFile");
                    String imagePath = ftpBean.getWorkingDir();
                    String forder = imagePath + File.separator + folder;
                    logger.info("\n forder url" + forder);

                    imagePath = imagePath + File.separator + folder + File.separator + imgName;
                    logger.info("\n imageName" + imagePath);
                    File file = new File(imagePath);
                    InputStream inputStream = new FileInputStream(file);
                    BufferedInputStream bInf = new BufferedInputStream(inputStream);
                    byte[] buffer = new byte[(int) file.length()];
                    bInf.read(buffer);
                    byte[] encode = Base64.encodeBase64(buffer);
                    strData = new String(encode, Charset.forName("UTF-8"));
//                    lstData.add(strData);
                    inputStream.close();
                }
            }
        } catch (Exception ex) {
            System.out.println("Error" + ex);
        }
        return strData;
    }

    /**
     * @author duyetdk
     * @param imgName
     * @param data
     * @param staffCode
     * @return
     */
    public boolean saveNewFile(String imgName, String data, String staffCode) {

        FileOutputStream fos = null;
        try {
            logger.info("\n start saveFile");
            String imagePath = ResourceBundleUtil.getResource("temp.folder.image.file.register");
            String forder = imagePath + File.separator + staffCode;
            logger.info("\n forder url" + forder);

            File forderFile = new File(forder);
            if (!forderFile.exists() && !forderFile.isDirectory()) {
                boolean check = forderFile.mkdir();
                logger.info("\n create forder " + check);
            }
            imagePath = imagePath + File.separator + staffCode + File.separator + imgName;
            logger.info("\n imageName" + imagePath);
            File file = new File(imagePath);
            fos = new FileOutputStream(file);
            fos.write(Base64.decodeBase64(data));

            FTPBean ftpBean = new FTPBean();
            String dir = ResourceBundleUtil.getResource("new_ftp_dir_u05");
            String host = ResourceBundleUtil.getResource("new_ftp_host_u05");
            String username = ResourceBundleUtil.getResource("new_ftp_username_u05");
            String password = ResourceBundleUtil.getResource("new_ftp_password_u05");

//            String folder = checkFolder(staffCode);
//            if (!folder.equals("")) {
//                    dir = ResourceBundleUtil.getResource("new_ftp_dir_u05");
//                    host = ResourceBundleUtil.getResource("new_ftp_host_u05");
//                    username = ResourceBundleUtil.getResource("new_ftp_username_u05");
//                    password = ResourceBundleUtil.getResource("new_ftp_password_u05");
//            }
            String uploadDir = dir + "/" + staffCode;
            boolean res = false;
            try {
                logger.info("-------" + host + "--------"
                        + username + "----------" + password + "-------" + ftpBean.getWorkingDir() + "------" + imagePath + "-----" + uploadDir + "----");
                res = FTPBusiness.putFileToNewFTPServer(host, username, password, imagePath, ftpBean.getWorkingDir() + forder, uploadDir);
            } catch (Exception e) {
                logger.info("\n error upload image " + e);
            }
            if (!res) {
                logger.info("\n error upload imageName " + imagePath + " on sever 10.79.94.22");
            }
            // up to server
            fos.close();
            return true;
            // Luu file thanh cong
        } catch (Exception ex) {
            logger.info("Error" + ex);
        }
        return false;// Xay ra loi khi luu file
    }
}
