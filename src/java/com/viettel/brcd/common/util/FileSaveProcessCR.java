/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.common.util;

import com.viettel.bccs.cm.bussiness.common.FTPBusiness;
import com.viettel.bccs.cm.bussiness.common.FTPBusinessCR;
import com.viettel.bccs.cm.model.FTPBean;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.ws.vsa.ResourceBundleUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

/**
 *
 * @author Anh Nguyen
 */
public class FileSaveProcessCR {
    private Logger logger = Logger.getLogger("loggerAction");
    private String pathStr = "/";
    private String ftp_dir = "ftp_dir";
    private String PATH_FILE_UPLOAD_IMG = "PATH_FILE_UPLOAD_IMG";


    public boolean saveFile(String imgName, String data, String isdn) {

        FileOutputStream fos = null;
        try {
            logger.info("\n start saveFile");
            String imagePath = ResourceUtils.getResource("temp.folder.image.file", "provisioning");
            String forder = imagePath + File.separator + isdn;
            logger.info("\n forder url" + forder);

            File forderFile = new File(forder);
            if (!forderFile.exists() && !forderFile.isDirectory()) {
                boolean check = forderFile.mkdir();
                logger.info("\n create forder " + check);
            }
            imagePath = imagePath + File.separator + isdn + File.separator + imgName;
            logger.info("\n imageName" + imagePath);
            File file = new File(imagePath);
            fos = new FileOutputStream(file);
            fos.write(Base64.decodeBase64(data));

            FTPBean ftpBean = new FTPBean();
            String dir = "";
            String host = "";
            String username = "";
            String password = "";
            dir = ResourceUtils.getResource("new_ftp_dir_u05", "provisioning");
            host = ResourceUtils.getResource("new_ftp_host_u05", "provisioning");
            username = ResourceUtils.getResource("new_ftp_username_u05", "provisioning");
            password = ResourceUtils.getResource("new_ftp_password_u05", "provisioning");
            
            String uploadDir = dir + "/" + isdn;
            boolean res = false;
            try {
                logger.info("-------" + host + "--------"
                        + username + "----------" + password + "-------" + ftpBean.getWorkingDir() + "------" + imagePath + "-----" + uploadDir + "----");
                res = FTPBusinessCR.putFileToFTPServer(host, username, password, imagePath, ftpBean.getWorkingDir() + forder, uploadDir);
            } catch (Exception e) {
                logger.info("\n error upload image " + e);
            }
            if (!res) {
                logger.info("\n error upload imageName " + imagePath + " on sever 10.79.94.22");
            }
            // up to server
            fos.close();
            return true;
            // Luu file thanh cong
        } catch (Exception ex) {
            logger.info("Error" + ex);
        }
        return false;// Xay ra loi khi luu file
    }

    private String checkFolder(String isdn) {
        try {
            logger.info("\n start checkFolder");
            String lastStr = isdn.substring(isdn.length() - 1);
            logger.info("\n lastStr = "+lastStr);
            String u02 = ResourceBundleUtil.getResource("u02_number");
            String u03 = ResourceBundleUtil.getResource("u03_number");

            String[] lstU02 = u02.split(";");
            if (lstU02 != null && lstU02.length > 0) {
                for (String num : lstU02) {
                    if (lastStr.equals(num)) {
                        logger.info("\n end checkFolder with folder = "+Constants.u02);
                        return Constants.u02;
                    }
                }
            }

            String[] lstU03 = u03.split(";");
            if (lstU03 != null && lstU03.length > 0) {
                for (String num : lstU03) {
                    if (lastStr.equals(num)) {
                        logger.info("\n end checkFolder with folder = "+Constants.u03);
                        return Constants.u03;
                    }
                }
            }

        } catch (Exception e) {
            logger.error(e.toString());
        }
        logger.info("\n end checkFolder with folder = empty");
        return "";
        
    }
}
