package com.viettel.brcd.common.util;

import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.dao.ActionLogPrDAO;
import com.viettel.bccs.cm.model.ActionLogPr;
import com.viettel.bccs.cm.model.pre.SubMbPre;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.common.ObjectClientChannel;
import com.viettel.common.ViettelService;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import com.viettel.im.database.BO.StockSim;
import com.viettel.bccs.cm.dao.interfaceCommon.InterfaceCommon;
import com.viettel.bccs.cm.model.pre.ActionLogPrPre;
import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.brcd.dao.pre.CommonCacheDB;
import com.viettel.brcd.dao.pre.SubMbPreDAO;
import com.viettel.common.ExchMsg;
import com.viettel.common.OriginalViettelMsg;
import java.util.HashMap;
import java.util.Map;

public class InterfacePr {

    protected static Logger logger = Logger.getLogger(InterfacePr.class);
    private static ObjectClientChannel channel;
    private static String ip;
    private static Integer port;
    private static String userName;
    private static String password;
    public static boolean allowSend;
    private static Map<String, ObjectClientChannel> mapChannelExch = null;

    static {
        loadConfig();
    }

    private static void loadConfig() {
        try {
            ip = ResourceBundleUtils.getResource("ip", "provisioning");
            port = Integer.parseInt(ResourceBundleUtils.getResource("port", "provisioning"));
            userName = ResourceBundleUtils.getResource("username", "provisioning");
            password = ResourceBundleUtils.getResource("password", "provisioning");
            allowSend = "true".equalsIgnoreCase(ResourceBundleUtils.getResource("provisioning.allow.send.request", "provisioning"));
            logger.info("InterfacePr.loadConfig:ip=" + ip + ";port=" + port + ";username=" + userName + ";password=" + password + ";allowSend=" + allowSend);
            System.out.println("--------------->InterfacePr.loadConfig:ip=" + ip + ";port=" + port + ";username=" + userName + ";password=" + password + ";allowSend=" + allowSend);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public static void connectPr() throws Throwable {
        if (channel == null) {
            try {
                channel = new ObjectClientChannel(ip, port, userName, password, true, true);
                channel.connect();
            } catch (Throwable ex) {
                channel = null;
                logger.error(ex.getMessage(), ex);
                throw ex;
            }
        }
    }

    public ViettelService request(Session cmPosSession, ViettelService requestPr, String shopCode, String staffCode, Date issueDate, String locale) {
        ViettelService responsePr = new ViettelService();
        if (requestPr == null || shopCode == null || staffCode == null) {
            responsePr = new ViettelService();
            responsePr.set(Constants.RESPONSE_CODE, Constants.RESPONSE_INVALID);
            responsePr.set(Constants.RESPONSE, LabelUtil.getKey("input.invalid", locale));
            return responsePr;
        }

        String response = "Không thực hiện gửi lệnh lên tổng đài để phục vụ test Migrate dữ liệu";
        String responseCode = Constants.RESPONSE_SUCCESS;
        String exception = null;

        try {
            if (allowSend) {
//                connectPr();
                System.out.println("---------------------->requestPr" + requestPr.toString());
                responsePr = (ViettelService) getObjectChannel().send(requestPr);
//                responsePr = (ViettelService) channel.send(requestPr);
                response = responsePr.toString();
                System.out.println("---------------------->response" + response);
                if (response != null) {
                    response = response.trim();
                    int n = response.length();
                    if (n >= 4000) {
                        response = response.substring(n - 3000, n);
                    }
                }

                Object obj = responsePr.get(Constants.RESPONSE_CODE);
                if (obj == null || !Constants.RESPONSE_SUCCESS.equals(obj.toString().trim())) {
                    responseCode = Constants.RESPONSE_FAIL;
                }
            }
        } catch (Throwable ex) {
            logger.error(ex.getMessage(), ex);
            responsePr = new ViettelService();
            responseCode = Constants.RESPONSE_EXCEPTION;
            response = null;
            exception = ex.getMessage();
        } finally {
            responsePr.set(Constants.REQUEST, requestPr.toString());
            responsePr.set(Constants.RESPONSE_CODE, responseCode);
            responsePr.set(Constants.RESPONSE, response);
            responsePr.set(Constants.EXCEPTION, exception);

            Object isdn = requestPr.get("MSISDN");
            if (isdn == null || "".equals(isdn.toString().trim())) {
                if (requestPr.get("SDN") != null && !"".equals(requestPr.get("SDN").toString().trim())) {
                    isdn = requestPr.get("SDN");
                }
                if (requestPr.get("USERNAME") != null && !"".equals(requestPr.get("USERNAME").toString().trim())) {
                    isdn = requestPr.get("USERNAME");
                }
            }

            if (!Constants.RESPONSE_SUCCESS.equals(responseCode) && !Constants.RESPONSE_EXCEPTION.equals(responseCode)) {
                responseCode = Constants.RESPONSE_FAIL;
            }

            ActionLogPr actionLogPr = new ActionLogPrDAO().insert(cmPosSession, issueDate, shopCode, staffCode, isdn == null ? null : isdn.toString().trim(), requestPr.toString(), response, responseCode, exception);
            responsePr.set(Constants.ACTION_LOG_PR_ID, actionLogPr.getId());
        }

        return responsePr;
    }

    /**
     * @Author: DungNV12
     * @Desc: Interface provisioning
     * @return
     * @throws Exception
     */
    private static ObjectClientChannel getObjectChannel() throws Exception {
        try {
            System.out.println("---------------------->PRO " + ip + ":" + port + ", " + userName + "/" + password);
            ObjectClientChannel channel = new ObjectClientChannel(ip, port, userName, password, true);
            channel.setReceiverTimeout(90000);
            channel.connect();
            return channel;
        } catch (Exception e) {
            throw e;
        }

    }

    private static ViettelService sendRequest(ViettelService requestPr, String function) {
        ViettelService responsePr = new ViettelService();
        String response = "Không thực hiện gửi lệnh lên tổng đài để phục vụ test Migrate dữ liệu";
        String responseCode = Constants.RESPONSE_SUCCESS;
        String exception = null;
        try {
            System.out.println("---------------------->requestPr " + function + ": " + requestPr.toString());
            if (allowSend) {
                responsePr = (ViettelService) getObjectChannel().send(requestPr);
            } else {
                responsePr.set(Constants.RESPONSE_CODE, Constants.RESPONSE_SUCCESS);
            }
            System.out.println("---------------------->response " + function + ": " + responsePr.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
            responsePr = new ViettelService();
            responseCode = Constants.RESPONSE_EXCEPTION;
            response = null;
            exception = ex.getMessage();

            responsePr.set(Constants.REQUEST, requestPr.toString());
            responsePr.set(Constants.RESPONSE_CODE, responseCode);
            responsePr.set(Constants.RESPONSE, response);
            responsePr.set(Constants.EXCEPTION, exception);
        }
        return responsePr;
    }

    public static ViettelService changeSim(Session imSession, SubMbPre sub, String newImsi, String newSerial) throws Exception {
        ViettelService requestPr = new ViettelService();
        try {
            requestPr.set("productId", sub.getProductCode().trim());
            requestPr.set("modificationType", Constants.MODIFICATION_TYPE_CHANGE_SIM_MB_HP);
            requestPr.set("MSISDN", "855" + sub.getIsdn());
            requestPr.set("OLD_IMSI", sub.getImsi());
            requestPr.set("OLD_SERIAL", sub.getSerial());

            // get sim information from new imsi or new serial
            StockSim stkSim = new InterfaceCommon().getSimPrePaidByImsiOrSerial(imSession, newImsi.trim(), newSerial);

            if (stkSim != null) {
                requestPr.set("NEW_IMSI", stkSim.getImsi());
                requestPr.set("NEW_SERIAL", stkSim.getSerial());
            }
            return sendRequest(requestPr, "changeSim");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static ViettelService activeService4G(String isdn) throws Exception {
        try {
            ViettelService requestPr = new ViettelService();
            requestPr.setMessageType(Constants.PROVISIONING_MESSAGE_TYPE);
            requestPr.setProcessCode(Constants.PROVISIONING_PROCESS_CODE_ACTIVE_SUB_4G);
            requestPr.set("MSISDN", "855" + isdn);
            requestPr.set("TPLID", Constants.ACTIVE_SUB_PRE_SIM4G_TPL_ID);
            requestPr.set("AMBRMAXUL", Constants.ACTIVE_SUB_PRE_SIM4G_AMBRMAXUL);
            requestPr.set("AMBRMAXDL", Constants.ACTIVE_SUB_PRE_SIM4G_AMBRMAXDL);
            return sendRequest(requestPr, "activeService4G");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static ViettelService blockOpenSub(Session cmPre, SubMbPre sub, String modificationType, String oldActStatus, String numWay) throws Exception {
        try {
            if (sub != null) {

                ViettelService requestPr = new ViettelService();
                requestPr.set("productId", sub.getProductCode().trim());
                requestPr.set("MSISDN", "855" + sub.getIsdn());
                requestPr.set("CURRENT_SERVICES", SubMbPreDAO.getCurrentServices(cmPre, sub));

                requestPr.set("modificationType", modificationType);
                if (Constants.MODIFICATION_TYPE_BLOCK_TWO_WAY.equals(modificationType) || Constants.MODIFICATION_TYPE_BLOCK_OUT_GOING_CALL.equals(modificationType)) {
                    requestPr.set("NUM_WAY", numWay);
                } else if (Constants.MODIFICATION_TYPE_UN_BLOCK_TWO_WAY.equals(modificationType) || Constants.MODIFICATION_TYPE_UN_BLOCK_OUT_GOING_CALL.equals(modificationType)) {
                    requestPr.set("NUM_WAY", numWay);
                    requestPr.set("ACT_STATUS", oldActStatus);
                }
                return sendRequest(requestPr, "blockOpenSub");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static OriginalViettelMsg lockFlagCallHlr(Session cmPre, String msisdn, String value, String staffCode, String shopCode) throws Exception {
        try {
            ExchMsg requestPr = new ExchMsg();
            requestPr.setCommand("HLR_HW_MODI_SUB_LOCK");
            if (!msisdn.startsWith("855")) {
                msisdn = "855" + msisdn;
            }
            requestPr.set("MSISDN", msisdn);
            requestPr.set("OC", value);
            System.out.println("SEND_PROVISIONING: " + requestPr);
            return request(cmPre, requestPr, "lockFlagCallHlr", staffCode, shopCode);

        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    public static OriginalViettelMsg request(Session cmPre, ExchMsg requestExc, String action, String staffCode, String shopCode) throws Exception {

        ActionLogPrPre actionLogPr = new ActionLogPrPre();
        OriginalViettelMsg responseExc = null;
        String response = "Test bussiness flow over exchange";
        boolean isViewAction = false;
        try {
            if (!isViewAction) {
                BaseSupplier baseSupplier = new BaseSupplier();
                Long id = baseSupplier.getSequence(cmPre, "seq_action_log_pr");
                actionLogPr.setId(id);
            }
            actionLogPr.setCreateDate(new Date());
            actionLogPr.setRequest(requestExc.toString());
            actionLogPr.setUserName(staffCode);
            actionLogPr.setShopCode(shopCode);
            Object isdn = requestExc.get("MSISDN");
            if (isdn == null || "".equals(isdn.toString().trim())) {
                if (requestExc.get("ISDN") != null && !"".equals(requestExc.get("ISDN").toString().trim())) {
                    isdn = requestExc.get("ISDN");
                }
                if (requestExc.get("USERNAME") != null && !"".equals(requestExc.get("USERNAME").toString().trim())) {
                    isdn = requestExc.get("USERNAME");
                }
            }
            if (isdn != null) {
                actionLogPr.setIsdn(isdn.toString());
            }

            // if test bussiness flow then didn't send request to provisioning
            System.out.println("---------------------->requestPr " + action + ": " + requestExc.toString());
            if (allowSend) {
                responseExc = (OriginalViettelMsg) getExchConnectionByTelecomService(cmPre, Constants.SERVICE_ALIAS_MOBILE).send(requestExc);
                System.out.println("---------------------->responsePr " + action + ": " + responseExc.toString());
            } else {
                actionLogPr.setResponseCode("0");
                actionLogPr.setResponse(response);
                ExchMsg excMsg = new ExchMsg();
                excMsg.setError("0");
                excMsg.setDescription("Test bussiness flow over exchange");
                responseExc = excMsg;
                System.out.println("---------------------->responsePr " + action + ": " + responseExc.toString());
                return responseExc;
            }

            if (responseExc != null) {
                // Fix loi > 4000 ky tu
                String responseXML = responseExc.toString();
                if (responseXML != null && responseXML.toString().trim().length() >= 4000) {
                    int lengReponse = responseXML.toString().trim().length();
//                    responseXML = responseXML.substring(lengReponse - 3000, lengReponse);
                    responseXML = responseXML.substring(0, 4000);
                }
                if (responseExc.getError() != null
                        && !"".equals(responseExc.getError())
                        && "0".equals(responseExc.getError().toString().trim())) {
                    actionLogPr.setResponse(responseXML);
                    actionLogPr.setResponseCode(responseExc.getError().toString().trim());
                } else {
                    actionLogPr.setResponse(responseXML);
                    actionLogPr.setResponseCode("1");
                }
            }
        } catch (Exception ex) {
            ExchMsg excMsg = new ExchMsg();
            excMsg.setError(Constants.IOEXCEPTION_RESPONSE_CODE);
            excMsg.set("requestPr", requestExc.toString());
            excMsg.setDescription(ex.toString());
            responseExc = excMsg;
            actionLogPr.setException(ex.getMessage());
        } finally {
            if (!isViewAction) {
                cmPre.save(actionLogPr);
            }
        }
        if (responseExc != null) {
            responseExc.set("requestPr", requestExc.toString());
        } else {
            ExchMsg excMsg = new ExchMsg();
            excMsg.setError("-1");
            excMsg.set("requestPr", requestExc.toString());
            excMsg.setDescription("Provisioning Timeout");
            responseExc = excMsg;
        }
        return responseExc;
    }

    public static OriginalViettelMsg addMoney(Session cmPre, String accountId, String msisdn, String amount, String staffCode, String shopCode) throws Exception {
        try {

            ExchMsg requestPr = new ExchMsg();
            requestPr.setCommand("OCSHW_ADJUST_MONEY");
            //requestPr.setExchId("ocshw_ver3");
            //requestPr.setExchType("ocs_hw_v3");
            requestPr.set("ACCT_REST_ID", accountId);
            requestPr.set("ISDN", "855" + msisdn);
            requestPr.set("ADD_MONEY", amount);

            System.out.println("SEND_PROVISIONING: " + requestPr);
            return request(cmPre, requestPr, "addMoney", staffCode, shopCode);

        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    public static OriginalViettelMsg connectKI(Session cmPre, String opsNo, String cardType, String operType, String k4sno,
            String kiValue, String imsi, String alg, String hlrsn, String staffCode, String shopCode) throws Exception {
        try {
            ExchMsg requestPr = new ExchMsg();
            requestPr.setCommand("HLR_HW_ADD_KI");
            if ("USIM".equals(cardType)) {
                requestPr.set("OPSNO", opsNo);
            }
            requestPr.set("CARDTYPE", cardType);
            requestPr.set("OPERTYPE", operType);
            requestPr.set("K4SNO", k4sno);
            requestPr.set("KIVALUE", kiValue);
            requestPr.set("IMSI", imsi);
            requestPr.set("ALG", alg);
            requestPr.set("HLRSN", hlrsn);
            System.out.println("SEND_PROVISIONING: " + requestPr);
            return request(cmPre, requestPr, "connectKI", staffCode, shopCode);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static ViettelService request(Session cmPre, ViettelService requestPr, String staffCode, String shopCode) throws Exception {

        ActionLogPrPre actionLogPr = new ActionLogPrPre();
        ViettelService responsePr = null;
        String response = "Test bussiness flow over exchange";
        boolean isViewAction = false;

        try {
            if (!isViewAction) {
                BaseSupplier baseSupplier = new BaseSupplier();
                Long id = baseSupplier.getSequence(cmPre, "SEQ_ACTION_LOG_PR");
                // save log
                actionLogPr.setId(id);
            }
            actionLogPr.setCreateDate(new Date());
            actionLogPr.setRequest(requestPr.toString());
            actionLogPr.setUserName(staffCode);
            actionLogPr.setShopCode(shopCode);
            Object isdn = requestPr.get("MSISDN");
            if (isdn != null) {
                actionLogPr.setIsdn(isdn.toString());
            }

            // if test bussiness flow then didn't send request to provisioning
            System.out.println("---------------------->requestPr " + requestPr.toString());
            if (allowSend) {
                responsePr = (ViettelService) getObjectChannel().send(requestPr);
            } else {
                actionLogPr.setResponseCode("0");
                actionLogPr.setResponse(response);
                responsePr = new ViettelService();
                responsePr.set("responseCode", "0");
                responsePr.set("content", "Test bussiness flow over exchange");
                return responsePr;
            }
            System.out.println("---------------------->responsePr " + responsePr.toString());
            if (responsePr != null) {
                String responseXML = responsePr.toString();
                if (responseXML != null && responseXML.toString().trim().length() >= 4000) {
                    int lengReponse = responseXML.toString().trim().length();
                    responseXML = responseXML.toString().trim().substring(lengReponse - 3000, lengReponse);
                }

                if (responsePr.get("responseCode") != null && !"".equals(responsePr.get("responseCode"))
                        && "0".equals(responsePr.get("responseCode").toString().trim())) {
                    actionLogPr.setResponse(responseXML);
                    actionLogPr.setResponseCode(responsePr.get("responseCode").toString());
                } else {
                    actionLogPr.setResponse(responseXML);
                    actionLogPr.setResponseCode("1");
                }
            }
        } catch (Exception ex) {
            responsePr = new ViettelService();
            responsePr.set("requestPr", requestPr.toString());
            responsePr.set("responseCode", Constants.IOEXCEPTION_RESPONSE_CODE);
            responsePr.set("content", ex.toString());
            actionLogPr.setException(ex.getMessage());
            ex.printStackTrace();
        } finally {
            if (!isViewAction) {
                cmPre.save(actionLogPr);
            }
        }

        if (responsePr != null) {
            responsePr.set("requestPr", requestPr.toString());
        } else {
            responsePr = new ViettelService();
            responsePr.set("requestPr", requestPr.toString());
            responsePr.set("responseCode", "-1");
            responsePr.set("content", "Provisioning Timeout Or getConnection Error");
        }
        return responsePr;
    }

    public static OriginalViettelMsg activateFirstSub(Session cmPre, String isdn, String serial, String command, String staffCode, String shopCode) throws Exception {

        ExchMsg requestExc = new ExchMsg();

        requestExc.setCommand(command);//"OCSHW_ACTIVEFIRST"
        requestExc.set("ClientTimeout", 120000);//2 phut
        requestExc.set("SERIALNO", serial);
        requestExc.set("ISDN", "855" + isdn);
        System.out.println("SEND_PROVISIONING: " + requestExc);

        return request(cmPre, requestExc, "activateFirstSub", staffCode, shopCode);

    }

    public static ObjectClientChannel getExchConnectionByTelecomService(Session session, String telecomService) {
        ObjectClientChannel clientChannel = null;
        ProConfig proConfig = null;
        try {
            if (mapChannelExch == null) {
                mapChannelExch = new HashMap<String, ObjectClientChannel>();
            }

            if (mapChannelExch.get(telecomService) != null) {
                clientChannel = mapChannelExch.get(telecomService);
            } else {
                Map<String, ProConfig> map = new HashMap<String, ProConfig>();
//                if (ProvisioningUtils.isReadConfigFile()){
//                    map = ProvisioningUtils.getMapProConfig();
//                }else {
                map = CommonCacheDB.getMapExchAccount(session);
                //}

                if (map != null && map.get(telecomService) != null) {
                    proConfig = map.get(telecomService);

                    if (proConfig != null) {
                        clientChannel = new ObjectClientChannel(proConfig.getIp(), Integer.parseInt(proConfig.getPort()),
                                proConfig.getUsername(), proConfig.getPassword(), true, true);
                        clientChannel.connect();

                        mapChannelExch.put(telecomService, clientChannel);
                    }
                } else {
                    new RuntimeException("Could not get Provisioning Connection Config");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Could not get Provisioning connection..." + proConfig.getIp() + "_" + Integer.parseInt(proConfig.getPort()) + "_"
                    + proConfig.getUsername() + "_" + proConfig.getPassword());
        }
        return clientChannel;
    }
}
