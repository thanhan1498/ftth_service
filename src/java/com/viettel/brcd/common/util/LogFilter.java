package com.viettel.brcd.common.util;

import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import viettel.passport.client.UserToken;

public class LogFilter
  implements Filter
{
  private static Logger logger = Logger.getLogger("LogFilter");
  private static final String SYS_NAME = "CTGTKH_QLHS";
  
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
    throws IOException, ServletException
  {
    String ip = "";
    Date startTime = null;
    Date endTime = null;
    String userName = "";
    String path = "";
    String uri = "";
    String className = "";
    String param = "";
    String clientKpiId = "";
    Long duration = Long.valueOf(0L);
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
    String requestId = "";
    HttpSession session = null;
    UserToken vsaUserToken = null;
    try
    {
      if ((request instanceof HttpServletRequest))
      {
        HttpServletRequest hRequest = (HttpServletRequest)request;
        clientKpiId = hRequest.getHeader("VTS-KPIID");
        session = hRequest.getSession();
        vsaUserToken = (UserToken)session.getAttribute("vsaUserToken");
        if (vsaUserToken != null) {
          userName = vsaUserToken.getUserName();
        }
        ip = hRequest.getHeader("X-ClientIP");
        if ((ip == null) || (ip.equals(""))) {
          ip = hRequest.getRemoteAddr();
        }
        path = hRequest.getServerName() + ":" + hRequest.getServerPort();
        uri = hRequest.getRequestURI();
        param = getAllParameter(hRequest);
        String[] methods = uri.split("!");
        String[] tmp = uri.split("\\/");
        if (tmp.length > 2) {
          uri = "/" + tmp[2];
        }
        if (methods.length > 1)
        {
          String[] method = methods[1].split("\\.");
          if (method.length > 0) {
            className = className + "." + method[0];
          }
        }
      }
      startTime = new Date();
      requestId = String.valueOf(startTime.getTime());
      String startTimeStr = dateFormat.format(startTime);
      String logStart = MessageFormat.format("|{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}", new Object[] { "start_action", "CTGTKH_QLHS", startTimeStr, userName, ip, path, uri, param, className, "", "", clientKpiId, requestId });
      logger.info(logStart);
      chain.doFilter(request, response);
      endTime = new Date();
      duration = Long.valueOf(endTime.getTime() - startTime.getTime());
    }
    finally
    {
      String checkDuration = "";
      if (duration.longValue() > 10000L) {
        checkDuration = "CHAM";
      }
      String endTimeStr = dateFormat.format(endTime);
      String logEnd = MessageFormat.format("|{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}", new Object[] { "end_action", "CTGTKH_QLHS", endTimeStr, userName, ip, path, uri, param, className, duration.toString(), checkDuration, clientKpiId, requestId });
      logger.info(logEnd);
    }
  }
  
  private static String getAllParameter(HttpServletRequest req)
  {
    StringBuilder params = new StringBuilder();
    try
    {
      Enumeration parameterNames = req.getParameterNames();
      while (parameterNames.hasMoreElements())
      {
        String paramName = (String)parameterNames.nextElement();
        
        params.append(paramName).append(":");
        
        String[] paramValues = req.getParameterValues(paramName);
        int length = paramValues.length;
        for (int i = 0; i < length; i++)
        {
          String paramValue = paramValues[i];
          params.append(paramValue).append(";");
        }
      }
    }
    catch (Exception ex) {}
    if (params.length() > 1) {
      params.deleteCharAt(params.length() - 1);
    }
    return params.toString();
  }
  
  public void init(FilterConfig fc)
    throws ServletException
  {}
  
  public void destroy() {}
}
