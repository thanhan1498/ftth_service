package com.viettel.brcd.common.util;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author vanghv1
 * @version 1.0
 * @since 20/08/2014 4:53 PM
 */
public class LogUtils {

    private static HashMap<String, Pattern> mapPattern = new HashMap<String, Pattern>();
    public static final String REG_DATE_MMyyyy = "^((0\\d)|(1[0-2]))\\/\\d\\d\\d\\d$";
    public static final String REG_DATE_ddMMyyyy = "^(([0-2]\\d)|3[0-1])\\/((0\\d)|(1[0-2]))\\/\\d\\d\\d\\d$";
    public static final String REG_DATE_ddMMyyyyHHmmss = "^(([0-2]\\d)|3[0-1])\\/((0\\d)|(1[0-2]))\\/\\d\\d\\d\\d\\s(([0-1]\\d)|(2[0-3])):[0-5]\\d:[0-5]\\d$";
    public static final String EMAIL_REG = "^[a-zA-Z]([a-zA-Z0-9]*(\\.|_)?[a-zA-Z0-9]*)*@([a-zA-Z]{2,}\\.)+[a-zA-Z]{2,}$";
    public static final String EMAIL_PLUS_REG = "^((?!((_\\.)|(\\._))).)*$";
    public static final String URL_REG = "^www.[A-Za-z0-9]+.[A-Za-z0-9.]+$";
    public static final String INTEGER_REG = "^(\\+|-|\\d)\\d*$";
    public static final String FLOAT_REG = "^(\\+|-|\\d)\\d*(\\.)?\\d*$";
    public static final String ISDN_REG = "^(\\d|\\+)\\d{6,14}$";
    public static final String BUS_TYPE_REG = "^[a-zA-z0-9_]+$";
    public static final String STATEMENT_NUMBER_REG = "^[a-zA-z0-9\\-/_\\\\]+$";
    public static final String FILE_NAME_REG = "^(\\w|\\+|,)(\\w|\\+|,)*\\.[A-Za-z]{3,4}$";

    private LogUtils() {
    }

    public static Pattern getPattern(String reg) {
        if (!mapPattern.containsKey(reg)) {
            mapPattern.put(reg, Pattern.compile(reg));
        }
        return mapPattern.get(reg);
    }

    public static boolean isNullOrEmpty(String value) {
        return value == null || value.trim().isEmpty();
    }

    public static boolean isNullOrEmpty(List value) {
        return value == null || value.isEmpty();
    }

    public static boolean isDateMMyyyy(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(REG_DATE_MMyyyy).matcher(value).matches();
    }

    public static boolean isDateddMMyyyy(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(REG_DATE_ddMMyyyy).matcher(value).matches();
    }

    public static boolean isDateddMMyyyyHHmmss(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(REG_DATE_ddMMyyyyHHmmss).matcher(value).matches();
    }

    public static boolean isEmail(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(EMAIL_REG).matcher(value).matches() && getPattern(EMAIL_PLUS_REG).matcher(value).matches();
    }

    public static boolean isUrl(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(URL_REG).matcher(value).matches();
    }

    public static boolean isInteger(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(INTEGER_REG).matcher(value).matches();
    }

    public static boolean isFloat(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(FLOAT_REG).matcher(value).matches();
    }

    public static boolean isFileName(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(FILE_NAME_REG).matcher(value).matches();
    }

    public static boolean isIsdn(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(ISDN_REG).matcher(value).matches();
    }

    public static boolean isStatementNumber(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(STATEMENT_NUMBER_REG).matcher(value).matches();
    }

    public static boolean isBusType(String value) {
        if (value == null) {
            return false;
        }
        value = value.trim();
        return !value.isEmpty() && getPattern(BUS_TYPE_REG).matcher(value).matches();
    }
}
