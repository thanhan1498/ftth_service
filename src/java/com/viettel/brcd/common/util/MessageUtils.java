/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.common.util;

import com.viettel.bccs.api.common.Common;
import com.viettel.brcd.ws.vsa.ResourceBundleUtil;
import java.io.FileReader;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Properties;
import java.io.File;

/**
 *
 * @author huypq15
 */
public class MessageUtils {

//    static private ResourceBundle rb = ResourceBundle.getBundle("smartphone_message");
    private static Map<String, Properties> messageMap = new HashMap<String, Properties>();

    public static String getMessage(String key, String locale) {
        //rb = ResourceBundle.getBundle("config1");
        if (locale == null || locale.isEmpty()) {                        
            return key;
        }
        if (messageMap.get(locale) == null) {            
            loadLanguageMessage(locale);
        }
        if (messageMap.get(locale) == null) {            
            return key;
        }
        if(!messageMap.get(locale).containsKey(key)){            
            loadLanguageMessage(locale);            
        }
        if(messageMap.get(locale).containsKey(key)){            
            return messageMap.get(locale).getProperty(key);            
        }
        return key;
    }

    public static String getMessage(String key, List<String> lstArgs, String locale) {
        try {
            if (!Common.isNullOrEmpty(key)) {
                String content = getMessage(key, locale);
                if (!Common.isNullOrEmpty(content) && lstArgs != null && !lstArgs.isEmpty()) {
                    for (int i = 0; i < lstArgs.size(); i++) {
                        content = content.replace("{" + i + "}", lstArgs.get(i));
                    }

                    return content;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public static String getSmsContent(String key, List<String> lstArgs, String locale) {
        try {
            if (!Common.isNullOrEmpty(key)) {
                String content = getMessage(key, locale);
                if (!Common.isNullOrEmpty(content) && lstArgs != null && !lstArgs.isEmpty()) {
                    for (int i = 0; i < lstArgs.size(); i++) {
                        content = content.replace("{" + i + "}", lstArgs.get(i));
                    }

                    return content;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public static void loadAllMessage() {
        FileReader r = null;
        try {

            String folder = ResourceBundleUtil.getResource("message.folder");
            File file = new File(folder);
            if (file.exists()) {
                File[] languageFile = file.listFiles();
                if (languageFile != null && languageFile.length > 0) {
                    for (int i = 0; i < languageFile.length; i++) {
                        if (!languageFile[i].getName().endsWith("mBCCS_msg.properties")) {
                            continue;
                        }
                        r = new FileReader(languageFile[i]);
                        Properties properties = new Properties();
                        properties.load(r);
                        messageMap.put(languageFile[i].getName().split("_")[0], properties);
                        r.close();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (r != null) {
                try {
                    r.close();
                } catch (Exception e) {
                }

            }
        }
    }

    public static void loadLanguageMessage(String locale) {
        FileReader r = null;
        try {
            String folder = ResourceBundleUtil.getResource("message.folder");
            File file = new File(folder + locale + "_mBCCS_msg.properties");            
            if (file.exists()) {
                r = new FileReader(file);
                Properties properties = new Properties();
                properties.load(r);
                messageMap.put(locale, properties);                
                r.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (r != null) {
                try {
                    r.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
