/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.common.util;


import com.viettel.bccs.cm.bussiness.BaseBussiness;
import java.util.Date;
import org.apache.log4j.Logger;



/**
 *
 * @author vietnn6
 */
public class PerformanceLogger extends BaseBussiness{

    private static final Logger logKPI = Logger.getLogger(PerformanceLogger.class);
    public static Long apCallId = 0L;
    public static final String LOG_TYPE_START_ACTION = "start_action";
    public static final String LOG_TYPE_END_ACTION = "end_action";
    public static final String APP_NAME = "WS_CDBR";
    public static final String USER_NAME = "WSBCRC";
    public static final String URI = "WSBCRC";
    public static final String PATH = "WSBCRC";

    public static synchronized Long getApCallId() {
        if (apCallId.equals(Long.MAX_VALUE)) {
            apCallId = 0L;
        }
        return apCallId++;
    }

    public static String saveLog(String userName, String uri, String className, String path, String param, Date startTime, Date endTime, String result) {
        try {
            /*String ipRemote = InetAddress.getLocalHost().getHostAddress();
            String startTimeStr = DateTimeUtils.formatDate(startTime, "yyyy/MM/dd HH:mm:ss:SSS");
            String clientKpiId = getApCallId().toString();
            String logStart = MessageFormat.format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}", new Object[]{LOG_TYPE_START_ACTION, APP_NAME, startTimeStr, userName, ipRemote, path, uri, param, className, "0", "", clientKpiId});
//            logKPI.info(logStart);
            LogUtils.info(logKPI,logStart);
            String endTimeStr = DateTimeUtils.formatDate(endTime, "yyyy/MM/dd HH:mm:ss:SSS");
            Long duration = endTime.getTime() - startTime.getTime();
            String logEnd = MessageFormat.format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}", new Object[]{LOG_TYPE_END_ACTION, APP_NAME, endTimeStr, userName, ipRemote, path, uri, param, className, duration.toString(), result, clientKpiId});
//            logKPI.info(logEnd);
            LogUtils.info(logKPI,logEnd);*/
            return "";
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
