package com.viettel.brcd.common.util;

import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.cm.bussiness.ApParamBussiness;
import com.viettel.bccs.cm.dao.AreaDAO;
import com.viettel.bccs.cm.dao.ContractOfferDAO;
import com.viettel.bccs.cm.dao.LocalProductPpDAO;
import com.viettel.bccs.cm.dao.ProductPricePlanDAO;
import com.viettel.bccs.cm.dao.PromotionTypeDAO;
import com.viettel.bccs.cm.dao.SubIpAdslLlDAO;
import com.viettel.brcd.dao.im.IMDAO;
import com.viettel.brcd.dao.pm.ProductDAO;
import com.viettel.bccs.cm.model.Contract;
import com.viettel.bccs.cm.model.ContractOffer;
import com.viettel.bccs.cm.model.LocalProductPp;
import com.viettel.bccs.cm.model.ProductPricePlan;
import com.viettel.bccs.cm.model.PromotionType;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.common.ViettelService;
import com.viettel.pm.common.util.PMConstant;
import com.viettel.pm.database.DAO.PMAPI;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.hibernate.Session;

public class Provisioning {

    public ViettelService activeSubscriber(Session cmPosSession, Session imSession, Session pmSession, String busType, Contract contract, SubAdslLeaseline sub, String modificationType, String type, String shopCode, String staffCode, boolean isBlock, Date issueDate, String locale) throws Exception {
        ViettelService requestPr = new ViettelService();
        requestPr.set("productId", sub.getProductCode());
        requestPr.set("modificationType", modificationType);
        if (Constants.MODIFICATION_TYPE_NOTCONNECT_SUBSCRIBER.equals(modificationType)) {
            if (type != null) {
                type = type.trim().toUpperCase();
            } else {
                type = "";
            }
            requestPr.set("TYPE", type);
        }

        ContractOffer contractOffer = new ContractOfferDAO().getSingleContractOfferBySubId(cmPosSession, sub.getSubId());
        HashMap params = getAdslParam(cmPosSession, imSession, pmSession, busType, contract, sub, contractOffer, Constants.MODIFICATION_TYPE_REGISTER_NEW);
        if (params != null && !params.isEmpty()) {
            params = getParamInOCS(cmPosSession, pmSession, params, sub, contractOffer, issueDate);
        }
        if (params != null && !params.isEmpty()) {
            Iterator paramKey = params.keySet().iterator();
            while (paramKey.hasNext()) {
                Object paramName = paramKey.next();
                Object paramValue = params.get(paramName);
                if (paramValue != null) {
                    requestPr.set(paramName.toString(), paramValue);
                } else {
                    requestPr.set(paramName.toString(), "");
                }
            }
        }

        return new InterfacePr().request(cmPosSession, requestPr, shopCode, staffCode, issueDate, locale);
    }

    public HashMap getAdslParam(Session cmPosSession, Session imSession, Session pmSession, String busType, Contract contract, SubAdslLeaseline sub, ContractOffer contractOffer, String modificationType) throws Exception {
        HashMap params = new HashMap();
        String center = new AreaDAO().getCenterCode(cmPosSession, sub.getDeployAreaCode());
        params.put("CENTER", center);
        String port = new IMDAO().getPortFormat(imSession, sub);
        if (port != null && !"".equals(port.trim())) {
            params.put("PORT", port);
        } else {
            params.put("PORT", "");
        }

        if (contractOffer != null) {
            Map attParams = PMAPI.getAttributeParam(contractOffer.getOfferId(), pmSession);
            if (attParams != null && !attParams.isEmpty()) {
                params.putAll(attParams);
            }
            Map offParams = PMAPI.getProductOfferProvisioningParam(contractOffer.getOfferId(), modificationType, PMConstant.PARAM_TYPE_1, pmSession);
            if (offParams != null && !offParams.isEmpty()) {
                params.putAll(offParams);
            }
        }

        if (Constants.SERVICE_ALIAS_LEASEDLINE.equals(sub.getServiceType())) {
            //<editor-fold defaultstate="collapsed" desc="get rentalCharge">
            Double rentalCharge = 0D;
            ProductPricePlan pp = new ProductPricePlanDAO().findById(cmPosSession, sub.getPricePlan());
            if (pp != null) {
                String code = pp.getCode();
                if (code != null) {
                    code = code.trim();
                    if (!code.isEmpty()) {
                        rentalCharge = Double.parseDouble(code);
                    }
                }
            }
            LocalProductPp lpp = new LocalProductPpDAO().findById(cmPosSession, sub.getLocalPricePlan());
            if (lpp != null) {
                String code = lpp.getCode();
                if (code != null) {
                    code = code.trim();
                    if (!code.isEmpty()) {
                        rentalCharge = Double.parseDouble(code);
                    }
                }
            }
            if (rentalCharge != null && rentalCharge > 0D) {
                params.put("DOM_RENTAL_CHARGE", rentalCharge);
            }
            //</editor-fold>

            if (pp != null) {
                String peakRate = new ApParamBussiness().getUserManualConfig(cmPosSession, Constants.PEAK_RATE_CONSTANT);
                if (peakRate == null || peakRate.isEmpty()) {
                    peakRate = "1024000";
                }
                Long peak = Long.parseLong(peakRate);
                peak = peak == null ? 0L : peak;
                Long rate = 0L;
                String speed = pp.getSpeed();
                if (speed != null) {
                    speed = speed.trim();
                    if (!speed.isEmpty()) {
                        rate = Long.parseLong(speed);
                    }
                }
                rate = rate == null ? 0L : rate;
                params.put("NEW_ERX_EGRESS_POLICY_VALUE", peak * rate);
                params.put("NEW_INPUT_AVERAGE_RATE", peak * rate);
                if (rate > 0L) {
                    Long inputPeakRate = 0L;
                    Long outputPeakRate = 0L;
                    inputPeakRate = Long.valueOf(peakRate) * rate;
                    outputPeakRate = inputPeakRate;
                    params.put("NEW_INPUT_AVERAGE_RATE", inputPeakRate);
                    params.put("NEW_INPUT_PEAK_RATE", inputPeakRate);
                    params.put("NEW_OUTPUT_AVERAGE_RATE", outputPeakRate);
                    params.put("NEW_OUTPUT_PEAK_RATE", outputPeakRate);

                }
            }
        }

        //<editor-fold defaultstate="collapsed" desc="ip">
        String ip = sub.getIpStatic();
        if (ip != null) {
            ip = ip.trim();
            if (!ip.isEmpty()) {
                params.put("IP", ip);
            }
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="account">
        String account = sub.getAccount();
        if (account != null) {
            account = account.trim().toLowerCase();
            if (!account.isEmpty()) {
                params.put("USERNAME", account);
            }
        }
        //</editor-fold>

        params.put("PASSWORD", sub.getPassword());
        params.put("REG_TYPE", sub.getRegType());
        params.put("MSISDN", sub.getAccount());
        params.put("SUBID", sub.getSubId());
        params.put("CUSTID", contract.getCustId());
        params.put("CONTRACT_ID", contract.getContractId());

        //<editor-fold defaultstate="collapsed" desc="deposit and quota">
        Double deposit = sub.getDeposit();
        Double quota = 0D;
        if (deposit != null && deposit > 0D) {
            quota += deposit;
        }
        if (sub.getQuota() != null && sub.getQuota() > 0) {
            quota += sub.getQuota();
        }
        if (quota == null || quota < 0D) {
            quota = Constants.SUB_QUOTA_DEFAULT.doubleValue();
        }
        params.put("LIMIT_USAGE", quota);
        if (deposit == null || deposit < 0D) {
            deposit = Constants.DEPOSIT_DEFAULT;
        }
        params.put("DEPOSIT", deposit);
        //</editor-fold>

        int trialDay = new ProductDAO().getProductTrialDay(pmSession, sub.getProductCode(), null);
        if (trialDay > 0) {
            params.put("ACT_STATUS", sub.getActStatus());
        } else {
            params.put("BLOCK_AAA", "TRUE");
            String actStatus = sub.getActStatus();
            actStatus = actStatus.substring(0, 1) + "1" + actStatus.substring(2);
            params.put("ACT_STATUS", actStatus);
        }
        params.put("STATUS", sub.getStatus());
        String regisDate = DateTimeUtils.formatddMMyyyyHHmmss(new Date());
        params.put("COMPLETED_DATE", regisDate);
        params.put("CUST_STA_DATE", regisDate);
        params.put("CONTRACT_EFFECTDATE", regisDate);
        params.put("SUB_EFFECTDATE", regisDate);
        params.put("EFFECT_DATE", regisDate);
        params.put("BILL_CYCLE_FROM", contract.getBillCycleFrom());
        params.put("CONTRACT_BUS_TYPE", busType);
        params.put("SUB_TYPE", sub.getSubType());

        return params;
    }

    public HashMap getParamInOCS(Session cmPosSession, Session pmSession, HashMap params, SubAdslLeaseline sub, ContractOffer contractOffer, Date issueDate) {
        if (params != null && !params.isEmpty() && sub != null) {
            String lstPPOcsPost = "";
            String lstEffectDate = "";
            String lstExprireDate = "";
            PromotionType promotionType = new PromotionTypeDAO().findByCode(cmPosSession, sub.getPromotionCode());
            if (promotionType != null) {
                if (Constants.PROMOTION_ONLINE.equals(promotionType.getPromType())) {
                    String pp = promotionType.getPricePlan();
                    if (pp != null) {
                        String pt = "yyyy-MM-dd HH:mm:ss";
                        String effectDate = DateTimeUtils.formatDate(new PromotionTypeDAO().getDayOfMonth(cmPosSession, promotionType.getCycle(), promotionType.getMonthAmount(), Constants.PROMOTION_TYPE_EFFECT, sub.getStaDatetime(), issueDate), pt);
                        effectDate = effectDate == null ? "" : effectDate;
                        String exprireDate = DateTimeUtils.formatDate(new PromotionTypeDAO().getDayOfMonth(cmPosSession, promotionType.getCycle(), promotionType.getMonthAmount(), Constants.PROMOTION_TYPE_EXPRIRE, sub.getStaDatetime(), issueDate), pt);
                        if (exprireDate != null) {
                            lstPPOcsPost = pp;
                            String[] lstPrice = lstPPOcsPost.split("-");
                            String os = "";
                            if (lstPrice != null && lstPrice.length > 0) {
                                for (int i = 0; i < lstPrice.length; i++) {
                                    lstEffectDate += os + effectDate;
                                    lstExprireDate += os + exprireDate;
                                    os = "&";
                                }
                            }
                        }
                    }
                }
            }
            String ip = sub.getIpStatic();
            if (ip != null) {
                ip = ip.trim();
                if (!ip.isEmpty()) {
                    if (new SubIpAdslLlDAO().isRatingStatus(cmPosSession, sub.getSubId(), ip)) {
                        params.put("FELLOW_NUMBER_LIST", ip);
                        String fellowType = new ProductDAO().getFellowTypeByVasIpAdsl(pmSession, contractOffer);
                        params.put("FELLOW_TYPE_LIST", fellowType);
                        String pricePlan = new ProductDAO().getVasIpAdslPricePlanCode(pmSession, contractOffer);
                        pricePlan = pricePlan == null ? "" : pricePlan.trim();
                        if (lstPPOcsPost != null) {
                            lstPPOcsPost = lstPPOcsPost.trim();
                        }
                        if (lstPPOcsPost != null && !lstPPOcsPost.isEmpty()) {
                            lstPPOcsPost += "-" + pricePlan;
                            lstEffectDate += "&" + "NULL";
                            lstExprireDate += "&" + "NULL";
                        } else {
                            lstPPOcsPost = pricePlan;
                            lstEffectDate += "NULL";
                            lstExprireDate += "NULL";
                        }
                    }
                }
            }
            if (lstPPOcsPost != null) {
                lstPPOcsPost = lstPPOcsPost.trim();
                if (!lstPPOcsPost.isEmpty()) {
                    params.put("GEN_ADD_PP_OCSPOST_LIST", lstPPOcsPost);
                    params.put("GEN_EFFECT_DATE_LIST", lstEffectDate);
                    params.put("GEN_EXPIRE_DATE_LIST", lstExprireDate);
                }
            }
        }
        return params;
    }
}
