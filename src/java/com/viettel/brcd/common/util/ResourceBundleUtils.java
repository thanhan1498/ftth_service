package com.viettel.brcd.common.util;

import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * @author vanghv1
 * @version 1.0
 * @since 20/08/2014 4:53 PM
 */
public class ResourceBundleUtils {

    private static final String DEFAULT_RESOURCE = "config";
    private static HashMap<String, ResourceBundle> mapResourceBundle = new HashMap<String, ResourceBundle>();

    private ResourceBundleUtils() {
    }

    public static ResourceBundle getResourceBundle(String fileName) {
        if (fileName == null) {
            return null;
        }
        fileName = fileName.trim();
        if (fileName.isEmpty()) {
            return null;
        }
        if (!mapResourceBundle.containsKey(fileName)) {
            ResourceBundle resourceBundle = ResourceBundle.getBundle(fileName);
            if (resourceBundle != null) {
                mapResourceBundle.put(fileName, resourceBundle);
            }
        }
        return mapResourceBundle.get(fileName);
    }

    public static String getResource(String key) {
        return getResource(key, DEFAULT_RESOURCE);
    }

    public static String getResource(String key, String fileName) {
        return getResourceBundle(fileName).getString(key);
    }

    public static String getDBCConfigFileLocation() {
        return getResource("dbc_config_file_location");
    }

    public static String getEncryptedFileLocation() {
        return getResource("encrypted_file_location");
    }

    public static String getDefaultLocale() {
        return getResource("def.Locale");
    }

    public static String getUseCache() {
        return getResource("use_cache_data");
    }

    public static String getDbcType() {
        return getResource("dbc_config_type");
    }
}
