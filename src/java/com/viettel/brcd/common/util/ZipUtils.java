/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.common.util;


import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.cm.bussiness.common.FTPBusiness;
import com.viettel.bccs.cm.model.FTPBean;
import com.viettel.brcd.ws.vsa.ResourceBundleUtil;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;


/**
 *
 * @author kdvt_kientt6
 */
public class ZipUtils {

    public ZipUtils() {
    }

    public static void createZipFile(File file, String inputFileName) throws IOException {
        // Create a buffer for reading the files
        byte[] buf = new byte[1024];
        try {
            // Create the ZIP file
            //String outFilename = "outfile.zip";


            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(inputFileName + ".zip"));
            // Compress the files
            FileInputStream in = new FileInputStream(file);

            // Add ZIP entry to output stream.
            out.putNextEntry(new ZipEntry(inputFileName));

            // Transfer bytes from the file to the ZIP file
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            // Complete the entry
            out.closeEntry();
            in.close();
            // Complete the ZIP file
            out.close();
        } catch (IOException e) {
            throw e;
        }
    }

    public static void createZipFile(File file, String desFolder, String inputFileName) throws IOException {
        // Create a buffer for reading the files
        byte[] buf = new byte[1024];
        try {
            // Create the ZIP file
            //String outFilename = "outfile.zip";


            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(desFolder + File.separator + inputFileName + ".zip"));
            out.setMethod(ZipOutputStream.DEFLATED);
            out.setLevel(Deflater.BEST_COMPRESSION);

            // Compress the files
            FileInputStream in = new FileInputStream(file);

            // Add ZIP entry to output stream.
            out.putNextEntry(new ZipEntry(inputFileName));

            // Transfer bytes from the file to the ZIP file
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            // Complete the entry
            out.closeEntry();
            in.close();
            // Complete the ZIP file
            out.close();
        } catch (IOException e) {
            System.out.println("error on zip file");
            throw e;
        }
    }

    public static void addFilesToExistingZip(File zipFile, File[] files) throws IOException {
        // get a temp file
        File tempFile = File.createTempFile(zipFile.getName(), null);
        // delete it, otherwise you cannot rename your existing zip to it.
        tempFile.delete();

        boolean renameOk = zipFile.renameTo(tempFile);
        if (!renameOk) {
            throw new RuntimeException("could not rename the file " + zipFile.getAbsolutePath() + " to " + tempFile.getAbsolutePath());
        }
        byte[] buf = new byte[1024];

        //ZipInputStream zin = new ZipInputStream(new FileInputStream(tempFile));
        ZipInputStream zin = new ZipInputStream(new FileInputStream(zipFile));
        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile));

        ZipEntry entry = zin.getNextEntry();
        while (entry != null) {
            String name = entry.getName();
            boolean notInFiles = true;
            for (File f : files) {
                if (f.getName().equals(name)) {
                    notInFiles = false;
                    break;
                }
            }
            if (notInFiles) {
                // Add ZIP entry to output stream.
                out.putNextEntry(new ZipEntry(name));
                // Transfer bytes from the ZIP file to the output file
                int len;
                while ((len = zin.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
            entry = zin.getNextEntry();
        }
        // Close the streams        
        zin.close();
        // Compress the files
        for (int i = 0; i < files.length; i++) {
            InputStream in = new FileInputStream(files[i]);
            // Add ZIP entry to output stream.
            out.putNextEntry(new ZipEntry(files[i].getName()));
            // Transfer bytes from the file to the ZIP file
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            // Complete the entry
            out.closeEntry();
            in.close();
        }
        // Complete the ZIP file
        out.close();
        tempFile.delete();
    }

    public static void unzipFileIntoDirectory(ZipFile zipFile, File jiniHomeParentDir) {
        Enumeration files = zipFile.entries();
        File f = null;
        FileOutputStream fos = null;

        while (files.hasMoreElements()) {
            try {
                ZipEntry entry = (ZipEntry) files.nextElement();
                InputStream eis = zipFile.getInputStream(entry);
                byte[] buffer = new byte[1024];
                int bytesRead = 0;

                f = new File(jiniHomeParentDir.getAbsolutePath() + File.separator + entry.getName());

                if (entry.isDirectory()) {
                    f.mkdirs();
                    continue;
                } else {
                    f.getParentFile().mkdirs();
                    f.createNewFile();
                }

                fos = new FileOutputStream(f);

                while ((bytesRead = eis.read(buffer)) != -1) {
                    fos.write(buffer, 0, bytesRead);
                }
                f = null;
                eis.close();
                entry = null;
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        // ignore
                    }
                }
            }
        }
    }

    //"E:\\anh.zip"  --> Unzip\\zipped
    public static void unzipZipsToMutiZips(ZipFile zipFile, File jiniHomeParentDir) throws Exception {
        Enumeration files = zipFile.entries();
        File f = null;
        FileOutputStream fos = null;

        while (files.hasMoreElements()) {
            try {
                ZipEntry entry = (ZipEntry) files.nextElement();
                InputStream eis = zipFile.getInputStream(entry);
                byte[] buffer = new byte[1024];
                int bytesRead = 0;

                f = new File(jiniHomeParentDir.getAbsolutePath() + File.separator + entry.getName());

                if (entry.isDirectory()) {
                    f.mkdirs();
                    continue;
                } else {
                    f.getParentFile().mkdirs();
                    f.createNewFile();
                }

                fos = new FileOutputStream(f);

                while ((bytesRead = eis.read(buffer)) != -1) {
                    fos.write(buffer, 0, bytesRead);
                }

            } catch (IOException e) {
                e.printStackTrace();
                continue;
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        // ignore
                    }
                }
            }
        }

    }

    public static void deleteZipEntry(File zipFile,
            String[] files) throws IOException {
        try {
            // get a temp file
            File tempFile = File.createTempFile(zipFile.getName(), null);
            // delete it, otherwise you cannot rename your existing zip to it.
            tempFile.delete();
            tempFile.deleteOnExit();
            boolean renameOk = zipFile.renameTo(tempFile);
            if (!renameOk) {
                throw new RuntimeException("could not rename the file " + zipFile.getAbsolutePath() + " to " + tempFile.getAbsolutePath());
            }
            ZipFile tempZipFile = new ZipFile(tempFile.getAbsolutePath());
            int numberOfEntries = tempZipFile.size();
            int numOfDelete = 0;
            Boolean isDeleteAllZip = false;
            byte[] buf = new byte[1024];
            ZipInputStream zin = new ZipInputStream(new FileInputStream(tempFile));
            ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(zipFile));

            ZipEntry entry = zin.getNextEntry();
            int numOfEntry = 0;
            while (entry != null) {
                String name = entry.getName();
                boolean toBeDeleted = false;
                for (String f : files) {
                    //if (f.equals(name)) {
                    if (compareFileName(f, name)) {
                        toBeDeleted = true;
                        numOfDelete++;
                        break;
                    }
                }
                if (numOfDelete == numberOfEntries) {
                    isDeleteAllZip = true;
                }
                if (!toBeDeleted || isDeleteAllZip) {
                    // Add ZIP entry to output stream.
                    zout.putNextEntry(new ZipEntry(name));
                    // Transfer bytes from the ZIP file to the output file
                    int len;
                    while ((len = zin.read(buf)) > 0) {
                        zout.write(buf, 0, len);
                    }
                }
                if (isDeleteAllZip) {
                    break;
                }
                entry = zin.getNextEntry();
            }
            // Close the streams		
            zin.close();
            // Compress the files
            // Complete the ZIP file
            zout.close();
            tempFile.delete();
            if (isDeleteAllZip) {
                System.out.println("file zip chi co 1 file anh, xoa ca file zip " + zipFile.delete());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Boolean compareFileName(String fileName1, String fileName2) throws Exception {
        int idx1 = fileName1.lastIndexOf(".");
        int idx2 = fileName2.lastIndexOf(".");
        if (idx1 != -1 && idx2 != -1) {
            String ext1 = fileName1.substring(idx1);
            String ext2 = fileName2.substring(idx2);
            if (ext1.toLowerCase().equals(ext2.toLowerCase())) {
                return fileName1.substring(0, idx1).equals(fileName2.substring(0, idx2));
            }
        }
        return false;
    }

    public static String unzipFileAndRenameIntoRootDirectory(ZipFile zipFile, File jiniHomeParentDir, String actionCode, Long reasonId, Date nowDate) {
        Enumeration files = zipFile.entries();
        File f = null;
        FileOutputStream fos = null;
        String lstFileName = "";
        while (files.hasMoreElements()) {
            try {
                ZipEntry entry = (ZipEntry) files.nextElement();
                InputStream eis = zipFile.getInputStream(entry);
                byte[] buffer = new byte[1024];
                int bytesRead = 0;

                f = new File(jiniHomeParentDir.getAbsolutePath() + File.separator + entry.getName());

                if (entry.isDirectory()) {
                    f.mkdirs();
                    continue;
                } else {
                    f.getParentFile().mkdirs();
                    f.createNewFile();
                }

                fos = new FileOutputStream(f);

                while ((bytesRead = eis.read(buffer)) != -1) {
                    fos.write(buffer, 0, bytesRead);
                }
                eis.close();
                if (f.isFile()) {
                    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                    String strDate = dateFormat.format(nowDate);

                    // rename image file
                    String fileName = f.getName();
                    System.out.println("single file name: " + fileName);
                    String centerName = fileName.substring(0, fileName.indexOf("."));
                    if (centerName.contains("-")) {
                        centerName = centerName.substring(0, centerName.indexOf("-"));
                    }
                    String newFileName = strDate + "_" + centerName + "_" + actionCode + "_" + reasonId + fileName.substring(fileName.indexOf("."));
                    lstFileName += ";" + newFileName;
                    f.renameTo(new File(jiniHomeParentDir.getAbsolutePath() + File.separator + newFileName));
                }
                f = null;
                entry = null;
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        // ignore
                    }
                }
            }
        }
        return !Common.isNullOrEmpty(lstFileName) ? lstFileName.substring(1) : "";
    }

    
    public static ArrayList<String> unzipFilePutToFtpServer(ZipFile zipFile, File jiniHomeParentDir, String actionCode, Long reasonId, Date nowDate) {
        ArrayList<String> lstFileRes = new ArrayList<String>();
        Enumeration files = zipFile.entries();
        File f = null;
        FileOutputStream fos = null;
        while (files.hasMoreElements()) {
            try {
                ZipEntry entry = (ZipEntry) files.nextElement();
                InputStream eis = zipFile.getInputStream(entry);
                byte[] buffer = new byte[1024];
                int bytesRead = 0;

                f = new File(jiniHomeParentDir.getAbsolutePath() + File.separator + entry.getName());

                if (entry.isDirectory()) {
                    f.mkdirs();
                    continue;
                } else {
                    f.getParentFile().mkdirs();
                    f.createNewFile();
                }

                fos = new FileOutputStream(f);


                FTPBean ftpBean = new FTPBean();


                while ((bytesRead = eis.read(buffer)) != -1) {
                    fos.write(buffer, 0, bytesRead);
                }
                eis.close();
                if (f.isFile()) {
                    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                    String strDate = dateFormat.format(nowDate);

                    // rename image file
                    String fileName = f.getName();
//                    System.out.println("single file name: " + fileName);
//                    String centerName = fileName.substring(0, fileName.indexOf("."));
//                    if (centerName.contains("-")) {
//                        centerName = centerName.substring(0, centerName.indexOf("-"));
//                    }
//                    String newFileName = strDate + "_" + centerName + "_" + actionCode + "_" + reasonId + fileName.substring(fileName.indexOf("."));
                    String filePath = jiniHomeParentDir.getAbsolutePath() + File.separator + fileName;
//
//                    f.renameTo(new File(jiniHomeParentDir.getAbsolutePath() + File.separator + newFileName));
//                    System.out.println("filePath: "  + filePath);
                    boolean res = false;
                    String uploadDir = ResourceBundleUtil.getResource("ftp_dir");
                    try {
                        System.out.println("-------" + ftpBean.getHost() + "--------" + ftpBean.getUsername() + "----------" + ftpBean.getPassword() + "-------" + ftpBean.getWorkingDir() + "------" + filePath + "-----" + uploadDir + "----");
                        res = FTPBusiness.putFileToFTPServer(ftpBean.getHost(), ftpBean.getUsername(), ftpBean.getPassword(), filePath, ftpBean.getWorkingDir(), uploadDir);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (res) {
                        File file = new File(ResourceBundleUtil.getResource("register.upload.image"));
                        if (file.isDirectory()) {
                            cleanFolder(file);
                        }
                        fileName = uploadDir + File.separator + fileName;
                        lstFileRes.add(fileName);
                        f.delete();
                    } else {
                        //QuocDM1 151219 code bo sung phan tra ve neu loi khi upload image
                        fileName = "ERROR";
                        lstFileRes.add(fileName);
                        f.delete();
                    }
                }
                f = null;
                entry = null;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        // ignore
                    }
                }
            }
        }
        System.out.println("-----------------------------End upload file to FTP-------------------------------------");
        return lstFileRes;
    }

    private static void cleanFolder(File file) {
        try {
            String[] listFile = file.list();
            String path = file.getPath();
            if (listFile.length > 0) {
                for (int i = 0; i < listFile.length; i++) {
                    File f = new File(path + File.separator + listFile[i]);
                    System.out.println(path + File.separator + listFile[i]);
                    if (f.isDirectory()) {
                        if (f.list().length > 0) {
                            cleanFolder(f);
                            f.delete();
                        } else {
                            f.delete();
                        }
                    } else {
                        f.delete();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        File file = new File("E:\\TestImage\\IMG_0810.JPG");
        createZipFile(file, "E:\\TestImage", "IMG_0810");
        //File f = new File("IMG_0810.rar");
        //f.renameTo(new File("E:\\IMG_0810.rar"));
        /*
         * File file= new File("E:\\TestImage\\IMG_0808.JPG");
         * createZipFile(file, "IMG_0808"); //createZipFile(file,
         * "E:\\TestImage\\IMG_0808"); File file1= new
         * File("E:\\TestImage\\IMG_0809.JPG"); File file2= new
         * File("E:\\TestImage\\IMG_0810.JPG"); File[] files = {file1,file2};
         * //addFilesToExistingZip(new File("E:\\TestImage\\IMG_0808.zip"),
         * files); addFilesToExistingZip(new File("IMG_0808.zip"), files);
         */
        /*
         * ZipUtils zipUtils = new ZipUtils();
         * zipUtils.zipDirectory("E:\\Anh1\\", "E:\\anh2");
         */
        //File file = new File("E:\\anh.zip");
        /*
         * ZipFile zipFile = new ZipFile("E:\\anh.zip"); File folder = new
         * File("Unzip"); unzipFileIntoDirectory(zipFile, folder); ZipUtils
         * zipUtils = new ZipUtils(); zipUtils.zipDirectory("Unzip",
         * folder.getAbsolutePath() + File.separator + "zipped");
         */
        //File zipFile = new File("E:\\Test\\66124_anh.zip");
        //deleteZipEntry(zipFile, new String[]{"anhhoso1.JPG"});
        //deleteZipEntry(zipFile, new String[]{"New Folder\\Huong dan nghiep vu cho CNVT trong dau noi- Phong Ho Tro Kenh.xls"});
    }
}
