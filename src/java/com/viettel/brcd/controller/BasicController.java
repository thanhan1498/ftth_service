/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.controller;

import java.io.Serializable;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Window;
import viettel.passport.client.UserToken;

/**
 *
 * @author linhlh2
 */
public class BasicController<T extends Component> extends GenericForwardComposer<T>
        implements Serializable {

    private String userName;

    @Override
    public void doBeforeComposeChildren(T comp) throws Exception {
        super.doBeforeComposeChildren(comp);

        UserToken userToken = (UserToken) Sessions.getCurrent().
                getAttribute("vsaUserToken");

        if(userToken!=null){
            userName = userToken.getUserName();
        } else {
            userName = "Anonymous";
        }
    }

    

    /**
     * Get the params map that are overhanded at creation time. <br>
     * Reading the params that are binded to the createEvent.<br>
     *
     * @param event
     * @return params map
     */
    @SuppressWarnings("unchecked")
    public Map getCreationArgsMap(Event event) {
        final CreateEvent ce = (CreateEvent) ((ForwardEvent) event).getOrigin();

        return ce.getArg();
    }

    @SuppressWarnings("unchecked")
    public void doOnCreateCommon(Window w, Event fe) throws Exception {
        final CreateEvent ce = (CreateEvent) ((ForwardEvent) fe).getOrigin();

        this.args = ce.getArg();
    }

    public String getUserName() {
        return userName;
    }

    private static final long serialVersionUID = -1171206258809472640L;

    protected transient Map args;
}


