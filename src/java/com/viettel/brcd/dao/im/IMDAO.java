package com.viettel.brcd.dao.im;

import com.viettel.bccs.cm.bussiness.ActionAuditBussiness;
import com.viettel.bccs.cm.dao.ApDomainDAO;
import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.bccs.cm.bussiness.ContractBussiness;
import com.viettel.bccs.cm.dao.CustomerDAO;
import com.viettel.bccs.cm.dao.InvoiceListStaffDAO;
import com.viettel.bccs.cm.dao.ReasonDAO;
import com.viettel.bccs.cm.dao.SaleTransDAO;
import com.viettel.bccs.cm.dao.SubBundleTvDAO;
import com.viettel.bccs.cm.dao.SubStockModelRelDAO;
import com.viettel.bccs.cm.dao.SubStockModelRelReqDAO;
import com.viettel.bccs.cm.model.ActionAudit;
import com.viettel.bccs.cm.model.Contract;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.SaleTrans;
import com.viettel.bccs.cm.model.SaleTransTv;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubBundleTv;
import com.viettel.bccs.cm.model.SubStockModelRelReq;
import com.viettel.bccs.cm.model.Mapping;
import com.viettel.bccs.cm.model.im.SaleAnyPayFile;
import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.bccs.merchant.model.Discount;
import com.viettel.brcd.ws.model.output.BrasIpPool;
import com.viettel.im.common.util.Constant;
import com.viettel.im.database.BO.APIpResource;
import com.viettel.im.database.BO.APResource;
import com.viettel.im.database.BO.APStockModelBean;
import com.viettel.im.database.BO.AccountAgent;
import com.viettel.im.database.BO.Deposit;
import com.viettel.im.database.BO.DslamModelType;
import com.viettel.im.database.BO.GPFResult;
import com.viettel.im.database.BO.InvoiceListBean;
import com.viettel.im.database.BO.Price;
import com.viettel.im.database.BO.StockModel;
import com.viettel.im.database.DAO.BaseImJdbcDAO;
import com.viettel.im.database.DAO.InvoiceListDAO;
import com.viettel.im.database.DAO.WebServiceAPDAO;
import static com.viettel.im.database.DAO.WebServiceAPDAO.convertErrCodeToString;
import com.viettel.im.database.DAO.WebServiceAPNewDAO;
import com.viettel.im.database.DAO.WebServiceIMDAO;
//import com.viettel.im.database.DAO.MultiLaguageDAO;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

/**
 * @author vanghv1
 */
public class IMDAO extends BaseDAO {

    public IMDAO() {
        _log = Logger.getLogger(IMDAO.class);
    }

    public DslamModelType getDslamModelType(Session imSession, Long dslamId) throws Exception {
        WebServiceAPDAO webIMDAO = new WebServiceAPDAO(imSession);
        info(_log, "IMDAO.getDslamModelType:dslamId=" + dslamId);
        DslamModelType dslamModelType = webIMDAO.getDslamModelType(dslamId);
        info(_log, "IMDAO.getDslamModelType:dslamModelType=" + toJson(dslamModelType));
        return dslamModelType;
    }

    public String updateInvoiceToUsing(Session paymentSession, Session imSession, Shop shop, Staff staff, Reason reason, Customer customer, SubAdslLeaseline sub, Date nowDate, String locale) throws Exception {
        info(_log, "IMDAO.updateInvoiceToUsing:serviceType=" + sub.getServiceType());
        if (Constants.SERVICE_ALIAS_ADSL.equals(sub.getServiceType()) || Constants.SERVICE_ALIAS_FTTH.equals(sub.getServiceType())) {
            //<editor-fold defaultstate="collapsed" desc="update invoice">
            Double payAdvAmount = reason.getPayAdvAmount();
            info(_log, "IMDAO.updateInvoiceToUsing:payAdvAmount=" + payAdvAmount);
            if (payAdvAmount != null && payAdvAmount > 0D) {
                InvoiceListDAO invoiceListDAO = new InvoiceListDAO(imSession);
                info(_log, "IMDAO.updateInvoiceToUsing:shopCode=" + shop.getShopCode() + ";staffCode=" + staff.getStaffCode());
                List<InvoiceListBean> invoices = invoiceListDAO.getAvailableInvoiceList(shop.getShopCode(), staff.getStaffCode());
                if (invoices == null || invoices.isEmpty()) {
                    return LabelUtil.getKey("invoice.not.exists", locale);
                }
                InvoiceListBean invoice = invoices.get(0);
                //Chuyen sang dung hibernate
                invoice = invoiceListDAO.getInvoiceListByIdUpdateMode(invoice.getInvoiceListId());
                String currInvoiceNo = invoice.getCurrInvoiceNo() == null ? null : String.valueOf(invoice.getCurrInvoiceNo());
                info(_log, "IMDAO.updateInvoiceToUsing:currInvoiceNo=" + currInvoiceNo);
                new InvoiceListStaffDAO().insert(paymentSession, staff.getStaffId(), staff.getStaffCode(), currInvoiceNo, sub.getRegType(), payAdvAmount, nowDate, sub.getContractId(), invoice.getInvoiceListId(), reason.getPromotion());
                String userUsing = sub.getUserUsing();
                if (userUsing != null) {
                    userUsing = userUsing.trim();
                }
                if (userUsing == null || userUsing.isEmpty()) {
                    userUsing = customer.getName();
                }
                if (userUsing != null) {
                    userUsing = userUsing.trim();
                }
                info(_log, "IMDAO.updateInvoiceToUsing:call IM updateInvoiceToUsing;shopId=" + shop.getShopId() + ";staffId=" + staff.getStaffId()
                        + ";serialNo=" + invoice.getSerialNo()
                        + ";blockNo=" + invoice.getBlockNo()
                        + ";currInvoiceNo=" + invoice.getCurrInvoiceNo()
                        + ";userUsing=" + userUsing
                        + ";payAdvAmount=" + payAdvAmount);
                Long resultCallIM = invoiceListDAO.updateInvoiceToUsing(shop.getShopId(), staff.getStaffId(), invoice.getSerialNo(), invoice.getBlockNo(), invoice.getCurrInvoiceNo(), userUsing, payAdvAmount, payAdvAmount / 1.1);
                info(_log, "IMDAO.updateInvoiceToUsing:call IM updateInvoiceToUsing;result=" + resultCallIM);
                if (resultCallIM == null || resultCallIM < 0L) {
                    return LabelUtil.formatKey("invoice.update.exception", locale, resultCallIM);
                }
            }
            //</editor-fold>
        }
        return null;
    }

    public Long saveDeposit(Session imSession, Deposit deposit) throws Exception {
        WebServiceIMDAO webIMDAO = new WebServiceIMDAO(imSession);
        info(_log, "IMDAO.saveDeposit:call IM saveDeposit;Deposit=" + toJson(deposit));
        Long result = webIMDAO.saveDeposit(deposit);
        imSession.flush();
        info(_log, "IMDAO.saveDeposit:call IM saveDeposit;result=" + result);
        return result;
    }

    public String executeSaleTrans(Session cmPosSession, Session imSession, String saleServiceCode, Long transType, Contract contract, Customer customer, SubAdslLeaseline subscriber,
            Shop shop, Staff staff, Reason reason, String actionCode, Date nowDate, String locale) throws Exception {
        if (saleServiceCode == null) {
            return LabelUtil.getKey("reason.not.mapping.saleService", locale);
        }
        saleServiceCode = saleServiceCode.trim();
        if (saleServiceCode.isEmpty()) {
            return LabelUtil.getKey("reason.not.mapping.saleService", locale);
        }
        if (actionCode == null) {
            if (new ApDomainDAO().isMandatoryTrasaction(cmPosSession, actionCode)) {
                return LabelUtil.getKey("reason.not.mapping.saleService", locale);
            }
            return LabelUtil.getKey("validate.invalid", locale, "actionCode");
        }
        actionCode = actionCode.trim();
        if (actionCode.isEmpty()) {
            if (new ApDomainDAO().isMandatoryTrasaction(cmPosSession, actionCode)) {
                return LabelUtil.getKey("reason.not.mapping.saleService", locale);
            }
            return LabelUtil.getKey("validate.invalid", locale, "actionCode");
        }

        Customer transCustomer = null;
        Contract transContract = null;
        SubAdslLeaseline transSubscriber = null;

        if (Constants.TRANS_OF_CONTRACT.equals(transType)) {
            if (contract == null) {
                return LabelUtil.getKey("validate.invalid", locale, "contract");
            }
            transContract = (Contract) BeanUtils.cloneBean(contract);
            if (customer != null) {
                transCustomer = (Customer) BeanUtils.cloneBean(customer);
            }
            if (transCustomer == null) {
                transCustomer = new CustomerDAO().findById(cmPosSession, transContract.getCustId());
            }
            if (subscriber != null) {
                transSubscriber = (SubAdslLeaseline) BeanUtils.cloneBean(subscriber);
            }
        } else if (Constants.TRANS_OF_SUBSCRIBER.equals(transType)) {
            if (subscriber == null) {
                return LabelUtil.getKey("validate.invalid", locale, "subscriber");
            }
            transSubscriber = (SubAdslLeaseline) BeanUtils.cloneBean(subscriber);
            if (contract != null) {
                transContract = (Contract) BeanUtils.cloneBean(contract);
            }
            if (customer != null) {
                transCustomer = (Customer) BeanUtils.cloneBean(customer);
            }
            if (transContract == null) {
                transContract = new ContractBussiness().findById(cmPosSession, transSubscriber.getContractId());
                if (transCustomer == null) {
                    transCustomer = new CustomerDAO().findById(cmPosSession, transContract.getCustId());
                }
            }
        } else {
            return LabelUtil.getKey("validate.invalid", locale, "transType");
        }

        boolean isCheckGood = true;
        String isdnPstn = null;
        Long dslamId = null;
        Long telServiceId = null;
        if (transSubscriber != null) {
            dslamId = transSubscriber.getDslamId();
            telServiceId = getWebServiceId(transSubscriber.getServiceType());
        }
        if (telServiceId == null) {
            return LabelUtil.getKey("validate.invalid", locale, "serviceType");
        }

        List<APStockModelBean> models = new ArrayList<APStockModelBean>();
        List<SubStockModelRelReq> modelReqs = new ArrayList<SubStockModelRelReq>();
        List<APResource> resources = new ArrayList<APResource>();
        if (transSubscriber != null) {
            if (Constants.ACTION_SUBSCRIBER_ACTIVE_NEW.equals(actionCode)) {
                Long teamId = transSubscriber.getTeamId();
                Long teamIdEnd = null;
                modelReqs = new SubStockModelRelReqDAO().findBySubId(cmPosSession, transSubscriber.getSubId(), null);
                models = getListApStockModel(modelReqs, teamId, teamIdEnd);
                String ipStatic = transSubscriber.getIpStatic();
                if (ipStatic != null) {
                    ipStatic = ipStatic.trim();
                    if (!ipStatic.isEmpty()) {
                        APIpResource apIpRsc = new APIpResource(ipStatic);
                        resources.add(apIpRsc);
                    }
                }
            } else if (Constants.ACTION_SUB_CHANGE_STATIC_IP.equals(actionCode)) {
                String ipStatic = transSubscriber.getIpStatic();
                if (ipStatic != null) {
                    ipStatic = ipStatic.trim();
                    if (!ipStatic.isEmpty()) {
                        APIpResource apIpRsc = new APIpResource(ipStatic);
                        resources.add(apIpRsc);
                    }
                }
            }
        }

        SaleTrans saleTrans = new SaleTransDAO().getSaleTrans(transCustomer, transContract, transSubscriber, telServiceId, shop.getShopId(), staff.getStaffId(), nowDate);
        saleTrans.setCreateStaffId(staff.getStaffId());

        com.viettel.im.database.BO.APSaleTrans apSaleTransIm = new com.viettel.im.database.BO.APSaleTrans();
        BeanUtils.copyProperties(apSaleTransIm, saleTrans);
        apSaleTransIm.setSaleServiceCode(saleServiceCode);
        apSaleTransIm.setLstAPModel(models);
        apSaleTransIm.setPstn(isdnPstn);
        apSaleTransIm.setLstAPResource(resources);
        apSaleTransIm.setDslamId(dslamId);
        if (!isCheckGood) {
            apSaleTransIm.setCheckGood(isCheckGood);
        }

        WebServiceAPDAO webIMDAO = new WebServiceAPDAO(imSession);
        WebServiceAPDAO.APResult resultIm = webIMDAO.saveAPSaleTrans(apSaleTransIm);

        String mess = convertErrCodeToString(resultIm, new Locale(locale));

        Long saleTransId = apSaleTransIm.getSaleTransId();
        if (saleTransId != null && saleTransId > 0) {
            saleTrans.setSaleTransId(saleTransId);
            saleTrans.setNote(mess);
            saleTrans.setTransResult(new Long(resultIm.ordinal()));
            saleTrans.setActionCode(actionCode);

            cmPosSession.save(saleTrans);
//            cmPosSession.flush();
            if (subscriber.getAccBundleTv() != null) {

                SubBundleTv tv = new SubBundleTvDAO().findBundleTvByAccount(cmPosSession, subscriber.getAccBundleTv());
                ActionAudit audit = new ActionAuditBussiness().insert(cmPosSession, nowDate, Constants.ACTION_SUBSCRIBER_ACTIVE_NEW, tv.getRegReasonId(), shop.getShopCode(), staff.getStaffCode(), Constants.ACTION_AUDIT_PK_TYPE_SUBSCRIBER, tv.getSubId(), Constants.WS_IP, "Register bundle tv");
                Reason reasonTv = new ReasonDAO().findById(cmPosSession, tv.getRegReasonId());
                String saleCode = getSaleServiceCode(cmPosSession, Constants.SERVICE_FTTH_ID_WEBSERVICE, reasonTv.getReasonId(), subscriber.getProductCode(), tv.getProductCode(), Constants.CONNECTING_BUNDLE_TV);
                Object[] saleFee = getDetailPriceFromSaleCode(imSession, saleCode);
                Double fee = 0d;
                if (saleFee != null) {
                    if (saleFee[3] != null) {
                        fee += Double.parseDouble(saleFee[3].toString());
                    }
                    if (saleFee[4] != null) {
                        fee += Double.parseDouble(saleFee[4].toString());
                    }
                }

                models = new ArrayList<APStockModelBean>();
                List<SubStockModelRelReq> modelReqsTv = new ArrayList<SubStockModelRelReq>();
                resources = new ArrayList<APResource>();
                if (transSubscriber != null) {
                    if (Constants.ACTION_SUBSCRIBER_ACTIVE_NEW.equals(actionCode)) {
                        Long teamId = transSubscriber.getTeamId();
                        Long teamIdEnd = null;
                        modelReqsTv = new SubStockModelRelReqDAO().findBySubId(cmPosSession, transSubscriber.getSubId(), Constants.OTHER_PRO_BUNDLE_TV);
                        models = getListApStockModel(modelReqsTv, teamId, teamIdEnd);
                        String ipStatic = transSubscriber.getIpStatic();
                        if (ipStatic != null) {
                            ipStatic = ipStatic.trim();
                            if (!ipStatic.isEmpty()) {
                                APIpResource apIpRsc = new APIpResource(ipStatic);
                                resources.add(apIpRsc);
                            }
                        }
                    }
                }

                SaleTrans saleTransCmTv = new SaleTransDAO().getSaleTrans(transCustomer, transContract, transSubscriber, telServiceId, shop.getShopId(), staff.getStaffId(), nowDate);
                saleTransCmTv.setCreateStaffId(staff.getStaffId());

                com.viettel.im.database.BO.APSaleTrans apSaleTransImTv = new com.viettel.im.database.BO.APSaleTrans();
                BeanUtils.copyProperties(apSaleTransImTv, saleTransCmTv);

                apSaleTransImTv.setSaleServiceCode(saleCode);
                apSaleTransImTv.setLstAPModel(models);
                apSaleTransImTv.setPstn(subscriber.getAccBundleTv());
                apSaleTransImTv.setLstAPResource(resources);
                apSaleTransImTv.setDslamId(dslamId);
                if (!isCheckGood) {
                    apSaleTransImTv.setCheckGood(isCheckGood);
                }

                WebServiceAPDAO.APResult resultImTv = new SaleTransNotInvoice().saveAPSaleTransTV(apSaleTransImTv, imSession);
                saleTransCmTv.setSaleTransId(apSaleTransImTv.getSaleTransId());
                saleTransCmTv.setNote(mess);
                saleTransCmTv.setTransResult(new Long(resultImTv.ordinal()));
                saleTransCmTv.setActionCode(actionCode);
                mess = convertErrCodeToString(resultImTv, new Locale(locale));

                if (mess != null && !mess.isEmpty()) {
                    return mess;
                }

                Long saleTransIdTv = apSaleTransImTv.getSaleTransId();

                SaleTransTv saleTransTV = new SaleTransTv();
                saleTransTV.setIsdn(subscriber.getAccBundleTv());
                saleTransTV.setSaleTransId(apSaleTransImTv.getSaleTransId());
                saleTransTV.setSaleTransDate(nowDate);
                saleTransTV.setStatus(1l);
                saleTransTV.setShopId(shop.getShopId());
                saleTransTV.setStaffId(staff.getStaffId());
                saleTransTV.setStaffCode(staff.getStaffCode().toUpperCase());
                saleTransTV.setReasonId(tv.getRegReasonId());
                saleTransTV.setSaleServiceCode(saleCode);
                saleTransTV.setAction("REGISTER");
                saleTransTV.setFee(reason.getPayAdvAmount());
                saleTransTV.setDeployFee(saleFee[3].toString() != null ? Double.parseDouble(saleFee[3].toString()) : 0d);
                saleTransTV.setModemFee(saleFee[4].toString() != null ? Double.parseDouble(saleFee[4].toString()) : 0d);
                saleTransTV.setTotal(fee + reasonTv.getPayAdvAmount());
                saleTransTV.setSubId(tv.getSubId());
                saleTransTV.setIsdn(tv.getAccount());

                cmPosSession.save(saleTransCmTv);
                cmPosSession.save(saleTransTV);
                new SubStockModelRelDAO().insert(cmPosSession, modelReqsTv, saleTransIdTv);
            }
            if (Constants.ACTION_SUBSCRIBER_ACTIVE_NEW.equals(actionCode)) {
                new SubStockModelRelDAO().insert(cmPosSession, modelReqs, saleTransId);
            }
        }

        return mess;
    }

    private List<APStockModelBean> getListApStockModel(List<SubStockModelRelReq> modelReqs, Long teamId, Long teamIdEnd) {
        List<APStockModelBean> result = new ArrayList<APStockModelBean>();
        if (modelReqs != null && !modelReqs.isEmpty()) {
            for (SubStockModelRelReq model : modelReqs) {
                if (model != null) {
                    APStockModelBean aPStockModelBean = new APStockModelBean();
                    aPStockModelBean.setStockModelId(model.getStockModelId());
                    aPStockModelBean.setCheckSerial(false);
                    aPStockModelBean.setQuantity(Constants.QUANTITY_DEFAULT_FOR_SALE);
                    aPStockModelBean.setLstSerial(Constants.EMPTY_ARRAY_LIST);
                    if (teamIdEnd != null && Constants.SOURCE_ID_END.equals(model.getSourceId())) {
                        aPStockModelBean.setPosId(teamIdEnd);
                    } else {
                        aPStockModelBean.setPosId(teamId);
                    }
                    result.add(aPStockModelBean);
                }
            }
        }
        return result;
    }

    public String getPortFormat(Session imSession, SubAdslLeaseline sub) throws Exception {
        WebServiceAPDAO webIMDAO = new WebServiceAPDAO(imSession);
        Long dslamId = sub.getDslamId();
        boolean isNoc = isNoc(imSession, dslamId);
        if (isNoc) {
            return Constants.PORT_FORMAT_FAKE;
        }
        String pf = null;
        String charsic = sub.getCharsic();
        if (charsic != null) {
            charsic = charsic.trim();
        }
        String slotCard = sub.getSlotCard();
        if (slotCard != null) {
            slotCard = slotCard.trim();
        }
        String portNo = sub.getPortNo();
        if (portNo != null) {
            portNo = portNo.trim();
        }
        if (charsic != null && !charsic.isEmpty() && slotCard != null && !slotCard.isEmpty() && portNo != null && !portNo.isEmpty()) {
            GPFResult result = webIMDAO.getPortFormat(dslamId, Long.parseLong(charsic), Long.parseLong(slotCard), Long.parseLong(portNo));
            pf = result.getPortFormat();
        }

        if (pf != null) {
            pf = pf.trim();
            if (pf.startsWith("D-") || pf.startsWith("d-")) {
                pf = pf.substring(2);
            }
        }

        return pf;
    }

    public boolean isNoc(Session imSession, Long dslamId) {
        WebServiceAPNewDAO aPDAO = new WebServiceAPNewDAO(imSession);
        if (dslamId == null) {
            return false;//Nếu dslamId=null thì không gửi cần kiểm tra bên IM
        }
        Long dslamType = null;
        try {
            dslamType = aPDAO.getDslamTypeById(dslamId);
        } catch (Exception ex) {
            _log.error(ex.getMessage(), ex);
        }
        return dslamType != null ? dslamType.equals(1L) : false;
    }

    public InvoiceListBean getInvoiceListById(Session imSession, Long invoiceListId) throws Exception {
        String methodName = "getInvoiceListById(Long invoiceListId)";
        StringBuilder params = new StringBuilder();
        params.append("invoiceListId=").append(invoiceListId);
        try {
            InvoiceListBean invoiceListBean = null;

            if ((invoiceListId != null) && (invoiceListId.compareTo(Long.valueOf(0L)) > 0)) {
                StringBuilder strQuery = new StringBuilder("");
                List parameterList = new ArrayList();

                strQuery.append("select    invoice_list_id as invoiceListId, ");
                strQuery.append("          book_type_id as bookTypeId, ");
                strQuery.append("          serial_no as serialNo, ");
                strQuery.append("          block_no as blockNo, ");
                strQuery.append("          from_invoice as fromInvoice, ");
                strQuery.append("          to_invoice as toInvoice, ");
                strQuery.append("          curr_invoice_no as currInvoiceNo, ");
                strQuery.append("          shop_id as shopId, ");
                strQuery.append("          (select shop_code from shop where shop_id = a.shop_id) as shopCode, ");
                strQuery.append("          (select name from shop where shop_id = a.shop_id) as shopName, ");
                strQuery.append("          staff_id as staffId, ");
                strQuery.append("          (select staff_code from staff where staff_id = a.staff_id) as staffCode, ");
                strQuery.append("          (select name from staff where staff_id = a.staff_id) as staffName, ");
                strQuery.append("          a.status as status ");
                strQuery.append("FROM      invoice_list a ");
                strQuery.append("WHERE     1 = 1 ");

                strQuery.append("and a.invoice_list_id = ? ");
                parameterList.add(invoiceListId);

                strQuery.append("AND (a.invoice_type = ? ");
                parameterList.add(Constant.INVOICE_TYPE_BILL_FOR_PAYMENT);
                strQuery.append("or a.BOOK_TYPE_ID in (select BOOK_TYPE_ID from book_type where book_type.invoice_type = ?)) ");
                parameterList.add(Constant.INVOICE_TYPE_BILL_FOR_PAYMENT);

                strQuery.append(" for update nowait ");

                Query query = imSession.createSQLQuery(strQuery.toString()).addScalar("invoiceListId", Hibernate.LONG).addScalar("bookTypeId", Hibernate.LONG).addScalar("serialNo", Hibernate.STRING).addScalar("blockNo", Hibernate.STRING).addScalar("fromInvoice", Hibernate.LONG).addScalar("toInvoice", Hibernate.LONG).addScalar("currInvoiceNo", Hibernate.LONG).addScalar("shopId", Hibernate.LONG).addScalar("shopCode", Hibernate.STRING).addScalar("shopName", Hibernate.STRING).addScalar("staffId", Hibernate.LONG).addScalar("staffCode", Hibernate.STRING).addScalar("staffName", Hibernate.STRING).addScalar("status", Hibernate.LONG).setResultTransformer(Transformers.aliasToBean(InvoiceListBean.class));

                for (int i = 0; i < parameterList.size(); i++) {
                    query.setParameter(i, parameterList.get(i));
                }

                List listInvoiceListBean = query.list();

                if ((listInvoiceListBean != null) && (listInvoiceListBean.size() == 1)) {
                    invoiceListBean = (InvoiceListBean) listInvoiceListBean.get(0);
                } else {
                    invoiceListBean = null;
                }

            }

            Long tmpInvoiceListId = Long.valueOf(invoiceListBean != null ? invoiceListBean.getInvoiceListId().longValue() : -1L);
            BaseImJdbcDAO.addMethodCallLog("com.viettel.im.database.DAO.InvoiceListDAO", methodName, params.toString(), new StringBuilder().append("INVOICE_LIST_ID=").append(tmpInvoiceListId).toString());

            return invoiceListBean;
        } catch (Exception ex) {
            ex.printStackTrace();

            BaseImJdbcDAO.addMethodCallLog("com.viettel.im.database.DAO.InvoiceListDAO", methodName, params.toString(), new StringBuilder().append("EXCEPTION=").append(ex.toString()).toString());
        }
        return null;
    }

    public List<BrasIpPool> getBrasPool(Session imSession, String account) {
        String sql = "SELECT BRAS as brasIp FROM bccs_im.usercheck WHERE username = ? and status = 0 ";
        Query query = imSession.createSQLQuery(sql)
                .addScalar("brasIp", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(BrasIpPool.class));
        query.setParameter(0, account);
        return query.list();
    }

    public StockModel getStockModelIdByCode(Session imSession, String code, Long ownerType, Long ownerId) {
        String sql = "SELECT stm.STOCK_MODEL_ID  stockModelId,STOCK_MODEL_CODE stockModelCode,  name,STOCK_TYPE_ID  stockTypeId, IS_KHR isKHR "
                + "  FROM STOCK_TOTAL skt "
                + "  JOIN STOCK_MODEL stm "
                + "    ON stm.STOCK_MODEL_ID = skt.STOCK_MODEL_ID "
                + "   AND skt.OWNER_TYPE = ? "
                + "   AND skt.OWNER_ID = ? "
                + " WHERE 1 = 1 "
                + "   and stm.stock_model_code = ? "
                + "   AND skt.STATE_ID = 1 ";

        Query query = imSession.createSQLQuery(sql)
                .addScalar("stockModelId", Hibernate.LONG)
                .addScalar("stockModelCode", Hibernate.STRING)
                .addScalar("name", Hibernate.STRING)
                .addScalar("stockTypeId", Hibernate.LONG)
                .addScalar("isKHR", Hibernate.LONG)
                .setResultTransformer(Transformers.aliasToBean(StockModel.class));
        query.setParameter(0, ownerType);
        query.setParameter(1, ownerId);
        query.setParameter(2, code);
        List<StockModel> list = query.list();
        if (list == null || list.isEmpty()) {
            return null;
        } else {
            return list.get(0);
        }
    }

    public Price getPriceOfStock(Session imSession, Long stockModelId, String pricePolicy) {
        String sql = "SELECT price_id  priceId,"
                + "  stock_model_id  stockModelId,"
                + "  type,"
                + "  price,"
                + "  status,"
                + "  description, "
                + "  sta_date staDate,"
                + "  end_date  endDate,"
                + "  vat,"
                + "  create_date createDate,"
                + "  user_name userName,"
                + "  area_code areaCode, "
                + "  price_policy pricePolicy, "
                + "  price,"
                + "  pledge_amount pledgeAmount, "
                + "  pledge_time pledgeTime,"
                + "  prior_pay priorPay"
                + "  FROM PRICE pri "
                + " WHERE pri.STOCK_MODEL_ID = ? "
                + "   AND pri.TYPE = 1  "
                /* 1: ban le, gia ban dai ly, ctv; db
                 * 2: gia thuc hien dich vu; 
                 * 3: gia khuyen mai; 
                 * 4: gia ban kem dich vu; 
                 * 11: gia ban noi bo*/
                + "   AND pri.PRICE_POLICY = ? "
                + "   AND pri.STA_DATE <= sysdate "
                + "   AND ((pri.END_DATE >= sysdate AND pri.END_DATE IS NOT NULL) OR "
                + "       pri.END_DATE IS NULL) "
                + "   AND pri.STATUS = 1 "
                + " ORDER BY pri.PRICE_ID";
        Query query = imSession.createSQLQuery(sql)
                .addScalar("priceId", Hibernate.LONG)
                .addScalar("stockModelId", Hibernate.LONG)
                .addScalar("type", Hibernate.STRING)
                .addScalar("price", Hibernate.DOUBLE)
                .addScalar("status", Hibernate.LONG)
                .addScalar("description", Hibernate.STRING)
                .addScalar("staDate", Hibernate.DATE)
                .addScalar("endDate", Hibernate.DATE)
                .addScalar("vat", Hibernate.LONG)
                .addScalar("createDate", Hibernate.DATE)
                .addScalar("userName", Hibernate.STRING)
                .addScalar("areaCode", Hibernate.STRING)
                .addScalar("pricePolicy", Hibernate.STRING)
                .addScalar("price", Hibernate.DOUBLE)
                .addScalar("pledgeAmount", Hibernate.DOUBLE)
                .addScalar("pledgeTime", Hibernate.LONG)
                .addScalar("priorPay", Hibernate.LONG)
                .setResultTransformer(Transformers.aliasToBean(Price.class));
        query.setParameter(0, stockModelId);
        query.setParameter(1, pricePolicy);
        List<Price> list = query.list();
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    public boolean checkStockGood(Session imSession, Long stockModelId, Long ownerId, Long ownerType, int amount) {
        String sql = "select * "
                + "  from stock_total "
                + " where STOCK_MODEL_ID = ? "
                + "   and OWNER_ID = ? "
                + "   and OWNER_TYPE = ? "
                + "   and STATE_ID = 1 "
                + "   and QUANTITY_ISSUE - ? >= 0";
        Query query = imSession.createSQLQuery(sql);
        query.setParameter(0, stockModelId);
        query.setParameter(1, ownerId);
        query.setParameter(2, ownerType);
        query.setParameter(3, amount);
        List list = query.list();
        if (list == null || list.isEmpty()) {
            return false;
        }
        return true;
    }

    public Discount getDiscount(Session imSession, Double amount, Long stockModelId, String discountPolicy) {
        String sql = "select DISCOUNT_RATE_NUMERATOR discountRate,"
                + "          DISCOUNT_RATE_DENOMINATOR discountAmount,"
                + "          DISCOUNT_ID discountId"
                + "  from Discount d "
                + " where FROM_AMOUNT <= ? "
                + "   and TO_AMOUNT >= ? "
                + "   and START_DATETIME <= sysdate "
                + "   and nvl(end_Datetime, sysdate) >= sysdate "
                + "   and exists "
                + " (select 1 "
                + "          from Discount_Model_Map m "
                + "         where m.stock_Model_Id = ? "
                + "           and m.status = 1 "
                + "           and m.discount_group_id = d.discount_group_id "
                + "           and m.discount_Group_Id in (select discount_Group_Id "
                + "                                       from Discount_Group "
                + "                                      where discount_Policy = ? "
                + "                                        and status = 1))";
        Query query = imSession.createSQLQuery(sql)
                .addScalar("discountRate", Hibernate.DOUBLE)
                .addScalar("discountAmount", Hibernate.DOUBLE)
                .addScalar("discountId", Hibernate.LONG)
                .setResultTransformer(Transformers.aliasToBean(Discount.class));
        query.setParameter(0, amount);
        query.setParameter(1, amount);
        query.setParameter(2, stockModelId);
        query.setParameter(3, discountPolicy);
        List<Discount> list = query.list();
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    public String getStockTypeName(Session imSession, Long stockTypeId) {
        String sql = "select NAME from stock_type where STOCK_TYPE_ID= ?";
        Query query = imSession.createSQLQuery(sql);
        query.setParameter(0, stockTypeId);
        List list = query.list();
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0).toString();
    }

    public int expStockTotal(Session imSession, Long ownerType, Long ownerId, Long stateId, Long stockModelId, Long quantity) {
        /*String sql = " select * from Stock_total "
         + "where owner_id = ?, owner_type = ?, stock_model_id = ?, and state_id = ? ,and quantity >= ?,and quantity_issue >= ? "
         + "FOR UPDATE nowait 15 ";
         Query q = imSession.createSQLQuery(sql);
         q.setParameter(0, ownerId);
         q.setParameter(1, ownerType);
         q.setParameter(2, stockModelId);
         q.setParameter(3, stateId);
         q.setParameter(4, quantity);
         q.setParameter(5, quantity);
         q.list();*/

        String SQL_UPDATE_STOCK_TOTAL = " update Stock_total "
                + "set quantity = quantity - ?, "
                + "quantity_issue = quantity_issue - ?, "
                + "modified_date= sysdate"
                + " where owner_id = ? "
                + "and owner_type = ? "
                + "and stock_model_id = ? "
                + "and state_id = ? "
                + "and quantity >= ? "
                + "and quantity_issue >= ?";

        Query q = imSession.createSQLQuery(SQL_UPDATE_STOCK_TOTAL);
        q.setParameter(0, quantity);
        q.setParameter(1, quantity);
        q.setParameter(2, ownerId);
        q.setParameter(3, ownerType);
        q.setParameter(4, stockModelId);
        q.setParameter(5, stateId);
        q.setParameter(6, quantity);
        q.setParameter(7, quantity);
        return q.executeUpdate();
    }

    public Double getCurrentTopUpPinCodeInDate(Session imSession, Long shopId, String isdn) {
        String sql = "select nvl(sum(AMOUNT_TAX),0) "
                + "  from sale_trans "
                + " where 1 = 1 "
                + "   and SALE_TRANS_DATE >= trunc(sysdate) "
                + "   and SALE_TRANS_DATE < trunc(sysdate + 1) ";
        String isdnFilter = " and isdn = ? ";
        String shopFilter = " and RECEIVER_ID = ? ";
        if (shopId != null) {
            sql += shopFilter;
        } else {
            sql += isdnFilter;
        }
        Query q = imSession.createSQLQuery(sql);
        if (shopId != null) {
            q.setParameter(0, shopId);
        } else {
            q.setParameter(0, isdn);
        }
        List temp = q.list();
        if (temp == null || temp.isEmpty()) {
            return 0d;
        }
        return Double.parseDouble(temp.get(0).toString());
    }

    public void getPinCode(Session imSession, String serialStockCard, Long ownerId, Long ownerType, StringBuilder id, StringBuilder serial, StringBuilder pinCode) {
        String sql = "select ID, SERIAL, PIN  "
                + "  from PIN_CODE  "
                + " where SERIAL = ?  "
                + "   and status = 1  ";
        Query q = imSession.createSQLQuery(sql);
        q.setParameter(0, serialStockCard);
        List temp = q.list();
        if (temp != null && !temp.isEmpty()) {
            Object[] arr = (Object[]) temp.get(0);
            id.append(arr[0].toString());
            serial.append(arr[1].toString());
            pinCode.append(arr[2].toString());
        }
    }

    public void updatePinCodeToUsed(Session imSession, String id, Long saleTransId, String user) {
        String sql = "update PIN_CODE "
                + "   set STATUS        = 0, "
                + "       SALE_TRANS_ID = ?, "
                + "       MODIFY_DATE   = sysdate, "
                + "       MODIFY_USER   = ? "
                + " where id = ?";
        Query q = imSession.createSQLQuery(sql);
        q.setParameter(0, saleTransId);
        q.setParameter(1, user);
        q.setParameter(2, id);
        q.executeUpdate();
        imSession.flush();
    }

    public AccountAgent getAccountAgentByOwnerCode(Session imSession, String ownerCode) {
        String sql = "select CURRENT_DEBT_PAYMENT currentDebtPayment,"
                + "LIMIT_DEBT_PAYMENT limitDebtPayment "
                + "from Account_Agent "
                + "where upper(owner_Code) = ? and status = ? ";
        Query query = imSession.createSQLQuery(sql)
                .addScalar("currentDebtPayment", Hibernate.DOUBLE)
                .addScalar("limitDebtPayment", Hibernate.DOUBLE)
                .setResultTransformer(Transformers.aliasToBean(AccountAgent.class));
        query.setParameter(0, ownerCode.toUpperCase());
        query.setParameter(1, com.viettel.bccs.api.Util.Constant.STATUS_USE);
        List<AccountAgent> result = query.list();
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }

    public AccountAgent getAccountAgentByIsdn(Session imSession, String isdn) {
        String sql = "select CURRENT_DEBT_PAYMENT currentDebtPayment,"
                + "LIMIT_DEBT_PAYMENT limitDebtPayment,"
                + "OWNER_CODE ownerCode "
                + "from Account_Agent "
                + "where isdn = ? and status = ? ";
        Query query = imSession.createSQLQuery(sql)
                .addScalar("currentDebtPayment", Hibernate.DOUBLE)
                .addScalar("limitDebtPayment", Hibernate.DOUBLE)
                .addScalar("ownerCode", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(AccountAgent.class));
        query.setParameter(0, isdn);
        query.setParameter(1, com.viettel.bccs.api.Util.Constant.STATUS_USE);
        List<AccountAgent> result = query.list();
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }

    public AccountAgent getAccountAgentByIsdnforLock(Session imSession, String isdn, String ownerCode) {
        String sql = "select CURRENT_DEBT_PAYMENT currentDebtPayment,"
                + "LIMIT_DEBT_PAYMENT limitDebtPayment,"
                + "OWNER_CODE ownerCode "
                + "from Account_Agent "
                + "where status = ? ";
        if (isdn != null && !isdn.isEmpty()) {
            sql += " and isdn = ?";
        }
        if (ownerCode != null && !ownerCode.isEmpty()) {
            sql += " and owner_code = ?";
        }
        Query query = imSession.createSQLQuery(sql)
                .addScalar("currentDebtPayment", Hibernate.DOUBLE)
                .addScalar("limitDebtPayment", Hibernate.DOUBLE)
                .addScalar("ownerCode", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(AccountAgent.class));
        query.setParameter(0, com.viettel.bccs.api.Util.Constant.STATUS_USE);
        if (isdn != null && !isdn.isEmpty()) {
            query.setParameter(1, isdn);
        }
        if (ownerCode != null && !ownerCode.isEmpty()) {
            query.setParameter(1, ownerCode);
        }
        List<AccountAgent> result = query.list();
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }

    public int updateAccountAgent(Session imSession, Double amount, String user, String isdn) {
        String sql = "update account_agent "
                + "   set CURRENT_DEBT_PAYMENT        =  CURRENT_DEBT_PAYMENT + ?, "
                + "       LAST_UPDATE_TIME = sysdate, "
                + "       LAST_UPDATE_USER   = ? "
                + " where owner_code = ? and status = 1";
        Query q = imSession.createSQLQuery(sql);
        q.setParameter(0, amount);
        q.setParameter(1, user);
        q.setParameter(2, isdn);
        int a = q.executeUpdate();
        imSession.flush();
        return a;
    }

    public String getSerialStockCard(Session imSession, Long ownerType, Long ownerId, Long stockModelId) {
        try {
            String sql = "select SERIAL "
                    + "  from stock_card "
                    + " where stock_modeL_id = ? "
                    + "   and owner_type = ? "
                    + "   and owner_id = ? "
                    + "   and status = 1 "
                    + "   and state_id = 1 "
                    + "   and rownum <= 1 "
                    + "   for update nowait";
            Query q = imSession.createSQLQuery(sql);
            q.setParameter(0, stockModelId);
            q.setParameter(1, ownerType);
            q.setParameter(2, ownerId);
            List list = q.list();
            if (list != null && !list.isEmpty()) {
                return list.get(0).toString();
            }
        } catch (Exception ex) {
        }
        return null;
    }

    public void updateStockCardToUsed(Session sessionIM, String serial, Long ownerId, Long ownerType) {
        String sql = "update stock_card set status = '0', sale_date = sysdate, owner_id =?, owner_type=? where to_number(serial) = ?";
        Query q = sessionIM.createSQLQuery(sql);
        q.setParameter(0, ownerId.intValue());
        q.setParameter(1, ownerType.intValue());
        q.setParameter(2, serial);
        q.executeUpdate();
        sessionIM.flush();
    }

    public void getPinCodeFromSaleTransId(Session imSession, StringBuilder serial, StringBuilder pinCode, Long saleTransId) {
        String sql = "select SERIAL, PIN  "
                + "  from PIN_CODE  "
                + " where SALE_TRANS_ID = ?  ";
        Query q = imSession.createSQLQuery(sql);
        q.setParameter(0, saleTransId);
        List temp = q.list();
        if (temp != null && !temp.isEmpty()) {
            Object[] arr = (Object[]) temp.get(0);
            serial.append(arr[0].toString());
            pinCode.append(arr[1].toString());
        }
    }

    public void saveVcRequest(Session sessionIm, String serial, Long requestType, Long transId, Long requestId, Long userSaleId, Long userShopId, String userLogin) throws Exception {
        String sql = "insert into vc_request "
                + "  (REQUEST_ID, "
                + "   STATUS, "
                + "   USER_ID, "
                + "   CREATE_TIME, "
                + "   REQUEST_TYPE, "
                + "   FROM_SERIAL, "
                + "   TO_SERIAL, "
                + "   TRANS_ID, "
                + "   SHOP_ID, "
                + "   STAFF_ID) "
                + "values ("
                + " ?, 0, ?, sysdate,?,?,?,?,?,?)";
        Query q = sessionIm.createSQLQuery(sql);
        q.setParameter(0, requestId);
        q.setParameter(1, userLogin);
        q.setParameter(2, requestType);
        q.setParameter(3, serial);
        q.setParameter(4, serial);
        q.setParameter(5, transId);
        q.setParameter(6, userShopId);
        q.setParameter(7, userSaleId);
        q.executeUpdate();
        sessionIm.flush();
    }

    public static String getSaleServiceCode(Session cmSession, Long telServiceId, Long reasonId, String productCode, String vasCode, String actionCode)
            throws Exception {
        List params = new ArrayList();

        String saleServiceCode = null;

        if (reasonId == null) {
            return saleServiceCode;
        }

        String sql = "Select new Mapping(m.saleServiceCode) from Mapping m, Reason r "
                + "where m.reasonId = r.reasonId and r.status = 1 and m.status = 1 and m.channel is null and m.reasonId = ? and m.actionCode = ? ";

        params.add(reasonId);
        params.add(actionCode);

        if (telServiceId != null && telServiceId.longValue() != 0) {
            sql += " and m.telServiceId = ? ";
            params.add(telServiceId);
        } else { //Truong hop bundle
            sql += " and m.telServiceId is null ";
        }

        if (productCode != null && !"".equals(productCode.trim())) {
            sql += " and (m.productCode = ? or m.productCode is null) ";
            params.add(productCode.trim());
        } else {
            sql += " and m.productCode is null ";
        }

        if (vasCode != null && !"".equals(vasCode.trim())) {
            sql += " and m.vas = ? ";
            params.add(vasCode.trim());
        } else {
            sql += " and m.vas is null ";
        }

        Query query = cmSession.createQuery(sql);

        if (params != null && params.size() > 0) {
            for (int i = 0; i < params.size(); i++) {
                query.setParameter(i, params.get(i));
            }
        }

        List<Mapping> lstMapping = (List<Mapping>) query.list();

        if (lstMapping != null && lstMapping.size() > 0) {
            saleServiceCode = lstMapping.get(0).getSaleServiceCode();
        }
        return saleServiceCode;
    }

    public static Object[] getDetailPriceFromSaleCode(Session imSession, String saleServiceCode) {
        String sql = "select s.sale_services_id, "
                + "       s.name, "
                + "       s.code, "
                + "       sum(nvl(ssp.price,0)) salePrice, "
                + "       sum(nvl(p.price,0)) modemPrice "
                + "  from sale_services s "
                + "  left join sale_services_price ssp "
                + "    on s.sale_services_id = ssp.sale_services_id "
                + "   and ssp.status = 1 "
                + "  left join sale_services_model ssm "
                + "    on s.sale_services_id = ssm.sale_services_id "
                + "   and ssm.status = 1 "
                + "  left join sale_services_detail ssd "
                + "    on ssm.sale_services_model_id = ssd.sale_services_model_id "
                + "   and ssd.status = 1 "
                + "  left join price p "
                + "    on ssd.price_id = p.price_id "
                + "   and p.status = 1 "
                + " where s.status = 1 "
                + "   and s.code = ? "
                + " group by s.sale_services_id, s.name, s.code";
        Query query = imSession.createSQLQuery(sql);
        query.setParameter(0, saleServiceCode);
        List list = query.list();
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (Object[]) list.get(0);
    }

    public Double updateAccountAgentEgifts(Session imSession, double amount, String isdnTopup, String requestId, String ownerCode) {
        String sql = "select column_value currentDebtPayment from table (func_update_egifts_account(?,?,?,?))";

        Query query = imSession.createSQLQuery(sql)
                .addScalar("currentDebtPayment", Hibernate.DOUBLE)
                .setResultTransformer(Transformers.aliasToBean(AccountAgent.class));
        query.setParameter(0, amount);
        query.setParameter(1, isdnTopup);
        query.setParameter(2, requestId);
        query.setParameter(3, ownerCode);

        List<AccountAgent> result = query.list();
        if (result != null && result.size() > 0) {
            return result.get(0).getCurrentDebtPayment();
        }
        return null;
    }

    public Long findAppParams(Session sessionIm, String type, String code, String name) {
        Long value = -1L;
        String sql = "select value from app_params_topup where type=:type and code=:code and name=:name";
        Query query = sessionIm.createSQLQuery(sql);
        query.setParameter("type", type);
        query.setParameter("code", code);
        query.setParameter("name", name);
        try {
            List result = query.list();
            if (result != null && !result.isEmpty()) {
                value = Long.valueOf(result.get(0).toString());
            }
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return value;
    }

    public int insertSaleAnypayFile(Session sessionIM, SaleAnyPayFile ele) {
        int result = 0;
        Long id = new BaseSupplier().getSequence(sessionIM, "SALE_ANYPAY_FILE_SEQ");
        String sql = "insert into sale_anypay_file (ID, CODE, SUB_ID, ISDN, SERIAL, AMOUNT, STATUS, CREATE_USER, CREATE_DATE, UPDATE_DATE, "
                + "DESCRIPTION, CHANNEL_CODE, CHANNEL_TYPE_ID, SALE_TRANS_ID, TOPUP_BASIC, TOPUP_VALID_SUB, TOPUP_PROMOTION, PROMOTION_VALID, "
                + "SMS_CONTENT, PROGRAM, REASON_ID, REQUESTER, DOCUMENT_NO, TRANS_TYPE, PROCESS_STATUS, IS_CORPORATE, "
                + "REASON_NAME, IP, TOPUP_DATA, DATA_VALID, REASON_CM_ID) values (?,?,?,?,?,?,?,?,sysdate,sysdate,"
                + "?,?,?,?,?,?,?,?,"
                + "?,?,?,?,?,?,?,?,"
                + "?,?,?,?,?)";
        Query query = sessionIM.createSQLQuery(sql);
        try {
            int index = 0;
            query.setParameter(index++, id);
            query.setParameter(index++, ele.getCode());
            query.setParameter(index++, ele.getSubId());
            query.setParameter(index++, ele.getIsdn());
            query.setParameter(index++, ele.getSerial());
            query.setParameter(index++, ele.getAmount(), new LongType());
            query.setParameter(index++, ele.getStatus());
            query.setParameter(index++, ele.getCreateUser());

            query.setParameter(index++, ele.getDescription());
            query.setParameter(index++, ele.getChannelCode());
            query.setParameter(index++, ele.getChannelTypeId(), new LongType());
            query.setParameter(index++, ele.getSaleTransId(), new LongType());
            query.setParameter(index++, ele.getTopupBasic());
            query.setParameter(index++, ele.getTopupBasicValid());
            query.setParameter(index++, ele.getTopupPromotion());
            query.setParameter(index++, ele.getPromotionValid());

            query.setParameter(index++, ele.getSmsContent());
            query.setParameter(index++, ele.getProgram());
            query.setParameter(index++, ele.getReasonId());
            query.setParameter(index++, ele.getRequester()); //null
            query.setParameter(index++, ele.getDocumentNo());
            query.setParameter(index++, ele.getTransType());
            query.setParameter(index++, ele.getProcessStatus());
            query.setParameter(index++, ele.getIsCorporate());

            query.setParameter(index++, ele.getReasonName());
            query.setParameter(index++, ele.getIp());
            query.setParameter(index++, ele.getTopupData());
            query.setParameter(index++, ele.getDataValid());
            query.setParameter(index++, ele.getReasonCmId());

            result = query.executeUpdate();
            sessionIM.flush();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return result;
    }
}
