/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.dao.im;

import com.viettel.im.database.BO.APSaleTrans;
import com.viettel.im.database.BO.APStockModelBean;
import com.viettel.im.database.BO.SaleServices;
import com.viettel.im.database.DAO.BaseImJdbcDAO;
import com.viettel.im.database.DAO.WebServiceAPDAO;
import com.viettel.im.database.DAO.WebServiceAPDAO.APResult;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class SaleTransNotInvoice {
    public APResult saveAPSaleTransTV(APSaleTrans apSaleTrans, Session sessionDB) {
        if (apSaleTrans == null) {
            return logSaleTrans("", APResult.ERR_INVALID_PARAMS);
        }
        String params = apSaleTrans.toString();
        if ((apSaleTrans.getSaleServiceCode() == null) || (apSaleTrans.getTelecomServiceId() == null) || (apSaleTrans.getShopId() == null) || (apSaleTrans.getStaffId() == null)) {
            return APResult.ERR_NOT_ENOUGH_PARAMS;
        }
        try {
            SaleServices saleService = com.viettel.im.database.DAO.SaleServicesDAO.findByCode(sessionDB, apSaleTrans.getSaleServiceCode());
            if (saleService == null) {
                return logSaleTrans(params, APResult.ERR_SALE_SERVICES_NOT_FOUND);
            }
            Long saleServicesId = saleService.getSaleServicesId();

            Long[] owner = new Long[2];
            APResult result = identifyOwner(apSaleTrans, owner, sessionDB);
            if (result != APResult.SUCCESS) {
                return logSaleTrans(params, result);
            }

            String pricePolicy = findPricePolicy(apSaleTrans.getShopId(), apSaleTrans.getStaffId(), sessionDB);
            if (pricePolicy.equals("")) {
                return logSaleTrans(params, APResult.ERR_PRICE_POLICY_NOT_FOUND);
            }
            com.viettel.im.database.BO.SaleServicesPrice saleServicesPrice = findSaleServicesPrice(saleServicesId, pricePolicy, sessionDB);
            if (saleServicesPrice == null) {
                return logSaleTrans(params, APResult.ERR_SERVICES_PRICE_NOT_FOUND);
            }
            Long saleServicesPriceId = saleServicesPrice.getSaleServicesPriceId();

            List<com.viettel.im.database.BO.APStockModelBean> lstAPMdlVat = new ArrayList<APStockModelBean>();
            List<com.viettel.im.database.BO.APStockModelBean> lstAPMdlNotVat = new ArrayList<APStockModelBean>();

            result = checkBindingGood(pricePolicy, saleServicesId, apSaleTrans, lstAPMdlVat, lstAPMdlNotVat, sessionDB);
            if (result != APResult.SUCCESS) {
                sessionDB.clear();
                return logSaleTrans(params, result);
            }
            Date sysDate = new Date();

            Long saleTransIdVat = getSequence("SALE_TRANS_SEQ", sessionDB);
            Long saleTransIdNotVat = getSequence("SALE_TRANS_SEQ", sessionDB);
            com.viettel.im.database.BO.SaleTrans saleTransVat = null;
            com.viettel.im.database.BO.SaleTrans saleTransNotVat = null;

            com.viettel.im.database.BO.SaleTransDetail servicesSaleTransDetail = new com.viettel.im.database.BO.SaleTransDetail();
            Long servicesSaleTransDetailId = getSequence("SALE_TRANS_DETAIL_SEQ", sessionDB);
            servicesSaleTransDetail.setSaleTransDetailId(servicesSaleTransDetailId);
            servicesSaleTransDetail.setSaleTransDate(sysDate);
            servicesSaleTransDetail.setStateId(com.viettel.im.common.util.Constant.STATE_NEW);
            servicesSaleTransDetail.setQuantity(Long.valueOf(1L));

            Double serviceAmount = Double.valueOf(saleServicesPrice.getPrice().doubleValue() * 1.0D);
            servicesSaleTransDetail.setAmount(serviceAmount);

            Double servicesNotVatAmount = Double.valueOf(serviceAmount.doubleValue() / (1.0D + saleServicesPrice.getVat().doubleValue() / 100.0D));
            Double servicesVatAmount = Double.valueOf(serviceAmount.doubleValue() - servicesNotVatAmount.doubleValue());
            servicesSaleTransDetail.setVatAmount(servicesVatAmount);
            servicesSaleTransDetail.setSaleServicesId(saleServicesId);
            servicesSaleTransDetail.setSaleServicesPriceId(saleServicesPriceId);

            servicesSaleTransDetail.setSaleServicesCode(saleService.getCode());
            servicesSaleTransDetail.setSaleServicesName(saleService.getName());
            servicesSaleTransDetail.setSaleServicesPrice(saleServicesPrice.getPrice());
            servicesSaleTransDetail.setSaleServicesPriceVat(saleServicesPrice.getVat());

            if ((saleServicesPrice.getVat() != null) && (saleServicesPrice.getVat().longValue() > 0L)) {
                servicesSaleTransDetail.setSaleTransId(saleTransIdVat);
                apSaleTrans.setSaleTransId(saleTransIdVat);

                saleTransVat = createSaleTransGeneral(null, saleTransIdVat, null, sysDate, apSaleTrans, saleServicesId, saleServicesPriceId);
                saleTransVat.setAmountTax(Double.valueOf(saleTransVat.getAmountTax().doubleValue() + serviceAmount.doubleValue()));
                saleTransVat.setAmountNotTax(Double.valueOf(saleTransVat.getAmountNotTax().doubleValue() + servicesNotVatAmount.doubleValue()));
                saleTransVat.setTax(Double.valueOf(saleTransVat.getTax().doubleValue() + servicesVatAmount.doubleValue()));

                saleTransVat.setVat(saleServicesPrice.getVat());

                for (com.viettel.im.database.BO.APStockModelBean apStkMdl : lstAPMdlVat) {
                    result = saveDetailSaleTrans(false, apStkMdl, saleTransVat, apSaleTrans, saleService, saleServicesPrice, sessionDB);
                    if (result != APResult.SUCCESS) {
                        sessionDB.clear();
                        return logSaleTrans(params, result);
                    }
                }
                saleTransVat.setCreateStaffId(apSaleTrans.getCreateStaffId());

                sessionDB.save(saleTransVat);

                if (lstAPMdlNotVat.size() > 0) {
                    saleTransNotVat = createSaleTransGeneral(null, saleTransIdNotVat, saleTransIdVat, sysDate, apSaleTrans, saleServicesId, saleServicesPriceId);
                    saleTransNotVat.setVat(Long.valueOf(0L));
                    saleTransNotVat.setSaleTransType("5");
                    for (com.viettel.im.database.BO.APStockModelBean apStkMdl : lstAPMdlNotVat) {
                        result = saveDetailSaleTrans(false, apStkMdl, saleTransNotVat, apSaleTrans, saleService, saleServicesPrice, sessionDB);
                        if (result != APResult.SUCCESS) {
                            sessionDB.clear();
                            return logSaleTrans(params, result);
                        }
                    }
                    sessionDB.save(saleTransNotVat);
                }
            } else {
                servicesSaleTransDetail.setSaleTransId(saleTransIdNotVat);
                if (lstAPMdlVat.size() > 0) {
                    apSaleTrans.setSaleTransId(saleTransIdVat);
                    saleTransNotVat = createSaleTransGeneral(null, saleTransIdNotVat, saleTransIdVat, sysDate, apSaleTrans, saleServicesId, saleServicesPriceId);
                } else {
                    apSaleTrans.setSaleTransId(saleTransIdNotVat);
                    saleTransNotVat = createSaleTransGeneral(null, saleTransIdNotVat, null, sysDate, apSaleTrans, saleServicesId, saleServicesPriceId);
                }

                saleTransNotVat.setAmountTax(Double.valueOf(saleTransNotVat.getAmountTax().doubleValue() + serviceAmount.doubleValue()));
                saleTransNotVat.setAmountNotTax(Double.valueOf(saleTransNotVat.getAmountNotTax().doubleValue() + servicesNotVatAmount.doubleValue()));
                saleTransNotVat.setTax(Double.valueOf(saleTransNotVat.getTax().doubleValue() + servicesVatAmount.doubleValue()));
                saleTransNotVat.setVat(Long.valueOf(0L));
                saleTransNotVat.setSaleTransType("5");

                for (APStockModelBean apStkMdl : lstAPMdlNotVat) {
                    result = saveDetailSaleTrans(false, apStkMdl, saleTransNotVat, apSaleTrans, saleService, saleServicesPrice, sessionDB);
                    if (result != APResult.SUCCESS) {
                        sessionDB.clear();
                        return logSaleTrans(params, result);
                    }
                }
                sessionDB.save(saleTransNotVat);

                if (lstAPMdlVat.size() > 0) {
                    saleTransVat = createSaleTransGeneral(null, saleTransIdVat, null, sysDate, apSaleTrans, saleServicesId, saleServicesPriceId);

                    saleTransVat.setVat(Long.valueOf(10L));
                    for (APStockModelBean apStkMdl : lstAPMdlVat) {
                        result = saveDetailSaleTrans(false, apStkMdl, saleTransVat, apSaleTrans, saleService, saleServicesPrice, sessionDB);
                        if (result != APResult.SUCCESS) {
                            sessionDB.clear();
                            return logSaleTrans(params, result);
                        }
                    }
                    sessionDB.save(saleTransVat);
                }

            }

            if (apSaleTrans.getLstAPResource() != null) {
                for (com.viettel.im.database.BO.APResource apRsc : apSaleTrans.getLstAPResource()) {
                    if ((apRsc instanceof com.viettel.im.database.BO.APIpResource)) {
                        com.viettel.im.database.BO.APIpResource apIp = (com.viettel.im.database.BO.APIpResource) apRsc;
                        com.viettel.im.database.BO.ResourceInTrans rscInTran = new com.viettel.im.database.BO.ResourceInTrans(new com.viettel.im.database.BO.ResourceInTransId(apIp.getIpPool(), com.viettel.im.common.util.Constant.RESOURCE_TYPE_IP, apSaleTrans.getSaleTransId(), Long.valueOf(0L)));
                        sessionDB.save(rscInTran);
                    }
                }

            }

            sessionDB.save(servicesSaleTransDetail);
            if ((saleTransVat != null) && (saleTransNotVat != null)) {
                saleTransVat.setFromSaleTransId(saleTransNotVat.getSaleTransId());
                apSaleTrans.setSaleTransId(saleTransVat.getSaleTransId());
                sessionDB.update(saleTransVat);
            } else if (saleTransVat != null) {
                apSaleTrans.setSaleTransId(saleTransVat.getSaleTransId());
            } else if (saleTransNotVat != null) {
                apSaleTrans.setSaleTransId(saleTransNotVat.getSaleTransId());
            }

            List<com.viettel.im.database.BO.DebitInfo> lstDebitInfo = apSaleTrans.getLstDebitInfo();
            if (lstDebitInfo != null) {
                for (com.viettel.im.database.BO.DebitInfo info : lstDebitInfo) {
                    String[] arrMess = new String[3];
                    arrMess = reduceDebit(info.getOwnerId(), info.getOnwerType(), info.getBasicDebit(), false, null, sessionDB);
                    if (!arrMess[0].equals("")) {
                        return logSaleTrans(apSaleTrans.toString(), APResult.ERR_UPDATE_DEBIT);
                    }
                    List<APStockModelBean> lstApMdl = info.getLstModel();
                    //giam tru han muc so luong
                    for (APStockModelBean apMdl : lstApMdl) {
                        arrMess = reduceDebitTotal(info.getOwnerId(), info.getOnwerType(), apMdl.getStockModelId(), com.viettel.im.common.util.Constant.STATE_NEW, com.viettel.im.common.util.Constant.STATUS_DEBIT_DEPOSIT, Long.valueOf(1L), false, null, sessionDB);
                        if (!arrMess[0].equals("")) {
                            return logSaleTrans(apSaleTrans.toString(), APResult.ERR_UPDATE_DEBIT_QUANTITY);
                        }
                    }
                }
            }
            com.viettel.im.database.BO.DebitInfo info;
            String[] arrMess;
            return logSaleTrans(apSaleTrans.toString(), APResult.SUCCESS);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return logSaleTrans(params, APResult.ERR_UNDEFINE);
    }

    private static APResult logSaleTrans(String params, APResult errCode) {
        BaseImJdbcDAO.addMethodCallLogAP("saveAPSaleTrans", WebServiceAPDAO.SAVE_AP_SALE_TRANS_METHOD, params, WebServiceAPDAO.convertErrCodeToStringForLog(errCode));
        return errCode;
    }

    private static com.viettel.im.database.BO.SaleTrans createSaleTransGeneral(com.viettel.im.database.BO.SaleTrans oldSaleTrans, Long saleTransId, Long fromSaTransId, Date saleDate, APSaleTrans apSaleTrans, Long saleServicesId, Long saleServicePricesId) {
        com.viettel.im.database.BO.SaleTrans saleTrans = new com.viettel.im.database.BO.SaleTrans();
        saleTrans.setSaleTransId(saleTransId);
        saleTrans.setSaleTransCode(String.format("GD%010d", new Object[]{saleTransId}));
        saleTrans.setStatus("2");
        if (apSaleTrans.isIsSupply()) {
            saleTrans.setSaleTransDate(apSaleTrans.getSaleTransDate());
        } else {
            saleTrans.setSaleTransDate(saleDate);
        }
        saleTrans.setTelecomServiceId(apSaleTrans.getTelecomServiceId());
        saleTrans.setSaleServiceId(saleServicesId);
        saleTrans.setSaleServicePriceId(saleServicePricesId);
        if (apSaleTrans.getPstn() != null) {
            saleTrans.setTelNumber(apSaleTrans.getPstn());
            saleTrans.setIsdn(apSaleTrans.getPstn());
        }
        if (fromSaTransId != null) {
            saleTrans.setFromSaleTransId(fromSaTransId);
        }
        saleTrans.setStaffId(apSaleTrans.getStaffId());
        saleTrans.setShopId(apSaleTrans.getShopId());

        saleTrans.setPayMethod("1");
        saleTrans.setContractNo(apSaleTrans.getContractNo());

        saleTrans.setCustName(apSaleTrans.getCustName());
        saleTrans.setAddress(apSaleTrans.getAddress());

        saleTrans.setSaleTransType("99");
        saleTrans.setReceiverId(220602l);
        saleTrans.setReceiverType(1l);
        saleTrans.setAmountTax(0.0D);
        saleTrans.setAmountNotTax(0.0D);
        saleTrans.setTax(0.0D);
        saleTrans.setAmountBasic(0.0D);
        saleTrans.setReceiverType(1l);
        if (oldSaleTrans != null) {
            saleTrans.setStockTransId(oldSaleTrans.getStockTransId());
            saleTrans.setReceiverId(oldSaleTrans.getReceiverId());
            saleTrans.setCreateStaffId(oldSaleTrans.getCreateStaffId());
            saleTrans.setSaleTransType(oldSaleTrans.getSaleTransType());
            saleTrans.setSaleTransDate(oldSaleTrans.getSaleTransDate());
            saleTrans.setShopId(oldSaleTrans.getShopId());
            saleTrans.setStaffId(oldSaleTrans.getStaffId());
            if (apSaleTrans.getContractNo() == null) {
                saleTrans.setContractNo(oldSaleTrans.getContractNo());
            }
        }
        saleTrans.setCreateStaffId(apSaleTrans.getCreateStaffId());

        return saleTrans;
    }

    private APResult identifyOwner(APSaleTrans apST, Long[] owner, Session sessionDB)
            throws Exception {
        if ((apST.getStaffId() != null) && (apST.getStaffId().compareTo(0L) > 0)) {
            com.viettel.im.database.BO.Staff staff = com.viettel.im.database.DAO.StaffDAO.findByIdAndStatus(sessionDB, apST.getStaffId(), com.viettel.im.common.util.Constant.STATUS_ACTIVE);
            if (staff != null) {
                Long shopId = staff.getShopId();
                if (shopId == null) {
                    return APResult.ERR_SHOP_NOT_FOUND;
                }
                apST.setShopId(shopId);
            } else {
                return APResult.ERR_STAFF_NOT_FOUND;
            }
            sessionDB.refresh(staff, org.hibernate.LockMode.UPGRADE);

            owner[0] = com.viettel.im.common.util.Constant.OWNER_TYPE_STAFF;
            owner[1] = apST.getStaffId();
        } else if ((apST.getShopId() != null) && (apST.getShopId().compareTo(0L) > 0)) {
            com.viettel.im.database.BO.Shop shop = com.viettel.im.database.DAO.ShopDAO.findByIdAndStatus(sessionDB, apST.getShopId(), com.viettel.im.common.util.Constant.STATUS_ACTIVE);
            if (shop == null) {
                return APResult.ERR_SHOP_NOT_FOUND;
            }
            sessionDB.refresh(shop, org.hibernate.LockMode.UPGRADE);

            owner[0] = com.viettel.im.common.util.Constant.OWNER_TYPE_SHOP;
            owner[1] = apST.getShopId();
        } else {
            return APResult.ERR_NOT_ENOUGH_PARAMS;
        }
        return APResult.SUCCESS;
    }

    public String findPricePolicy(Long shopId, Long staffId, Session session) {
        if ((shopId == null) && (staffId == null)) {
            return "";
        }
        Long tmpShopId = shopId;
        if ((shopId == null) && (staffId != null)) {
            tmpShopId = com.viettel.im.database.DAO.WebServiceIMUtil.findShopIdByStaffId(session, staffId);
        }
        StringBuilder queryString = new StringBuilder("");
        queryString.append("select price_policy, shop_id ");
        queryString.append("from shop ");
        queryString.append("where shop_id = ? and status = ? and rownum < ? ");
        Query queryObject = session.createSQLQuery(queryString.toString());
        queryObject.setParameter(0, tmpShopId);
        queryObject.setParameter(1, com.viettel.im.common.util.Constant.STATUS_ACTIVE);
        queryObject.setParameter(2, 2L);

        List lst = queryObject.list();
        if ((lst != null) && (lst.size() > 0)) {
            Object[] objs = (Object[]) (Object[]) lst.get(0);
            String tmpPricePolicy = objs[0] != null ? objs[0].toString() : "";
            return tmpPricePolicy;
        }
        return "";
    }

    public com.viettel.im.database.BO.SaleServicesPrice findSaleServicesPrice(Long saleServiceId, String pricePolicy, Session session) throws Exception {
        String queryString = "from SaleServicesPrice where saleServicesId = ?        and staDate <= ?        and (((endDate >= ?) and (endDate is not null)) or (endDate is null))        and status = ? and pricePolicy = ? and rownum < ? ";

        Query queryObject = session.createQuery(queryString);
        String curDate = com.viettel.im.common.util.DateTimeUtils.getSysDateTime("dd/MM/yyyy");
        queryObject.setParameter(0, saleServiceId);
        queryObject.setParameter(1, com.viettel.im.common.util.DateTimeUtils.convertStringToDateTime(new StringBuilder().append(curDate.trim()).append(" 23:59:59").toString()));
        queryObject.setParameter(2, com.viettel.im.common.util.DateTimeUtils.convertStringToDateTime(new StringBuilder().append(curDate.trim()).append(" 00:00:00").toString()));
        queryObject.setParameter(3, com.viettel.im.common.util.Constant.STATUS_ACTIVE);
        queryObject.setParameter(4, pricePolicy);
        queryObject.setParameter(5, 10l);

        List lst = queryObject.list();
        if ((lst != null) && (lst.size() > 0)) {
            com.viettel.im.database.BO.SaleServicesPrice saleServicesPrice = (com.viettel.im.database.BO.SaleServicesPrice) lst.get(0);
            return saleServicesPrice;
        }
        return null;
    }

    private APResult checkBindingGood(String pricePolicy, Long saleServicesId, APSaleTrans apSaleTrans, List<APStockModelBean> lstAPMdlVat, List<APStockModelBean> lstAPMdlNotVat, Session sessionDB)
            throws Exception {
        List<com.viettel.im.database.BO.APStockModelBean> lstAPModel = apSaleTrans.getLstAPModel();

        com.viettel.im.database.DAO.SaleServicesModelDAO ssmDAO = new com.viettel.im.database.DAO.SaleServicesModelDAO(sessionDB);
        com.viettel.im.database.DAO.SaleServicesStockDAO sssDAO = new com.viettel.im.database.DAO.SaleServicesStockDAO(sessionDB);

        com.viettel.im.database.DAO.ShopDAO shopDAO = new com.viettel.im.database.DAO.ShopDAO(sessionDB);

        List saleServicesModelIdLst = new ArrayList();
        com.viettel.im.database.BO.SaleServicesModel ssModel;
        com.viettel.im.database.BO.StockModel stkMdl;
        if (lstAPModel != null) {
            for (com.viettel.im.database.BO.APStockModelBean apStkMdl : lstAPModel) {
                Long stockModelId = apStkMdl.getStockModelId();
                Long quantity = apStkMdl.getQuantity();
                List<com.viettel.im.database.DAO.SerialPair> lstSerial = apStkMdl.getLstSerial();
                if (stockModelId == null) {
                    return APResult.ERR_STOCK_MODEL_NULL;
                }
                if (quantity == null) {
                    return APResult.ERR_MODEL_QUANTITY_NULL;
                }
                stkMdl = com.viettel.im.database.DAO.StockModelDAO.findById(sessionDB, stockModelId);
                if (stkMdl == null) {
                    return APResult.ERR_STOCK_MODEL_NOT_FOUND;
                }
                apStkMdl.setStockTypeId(stkMdl.getStockTypeId());

                ssModel = ssmDAO.findBySaleSerIdAndStkTypId(saleServicesId, stkMdl.getStockTypeId(), stkMdl.getStockModelId(), saleServicesModelIdLst);
                if (ssModel == null) {
                    return APResult.ERR_SALE_SERVICES_MODEL_NOT_FOUND;
                }
                saleServicesModelIdLst.add(ssModel.getSaleServicesModelId());
                if ((com.viettel.im.common.util.DataUtil.safeEqual(stkMdl.getCheckSerial(), 1L)) && (com.viettel.im.common.util.DataUtil.safeEqual(ssModel.getUpdateStock(), 1L))
                        && (lstSerial != null) && (lstSerial.size() > 0)) {
                    for (com.viettel.im.database.DAO.SerialPair pair : lstSerial) {
                        if (!pair.isValid()) {
                            return APResult.ERR_SERIAL_INVALID;
                        }
                    }
                }

                com.viettel.im.database.BO.Price price = getPriceInServices(ssModel.getSaleServicesModelId(), stockModelId, sessionDB);
                if ((price == null) || (price.getPrice() == null)) {
                    return APResult.ERR_MODEL_PRICE_NOT_FOUND;
                }
                apStkMdl.setUpdateStock(ssModel.getUpdateStock());
                apStkMdl.setCheckSerial(com.viettel.im.common.util.DataUtil.safeEqual(stkMdl.getCheckSerial(), 1L));
                apStkMdl.setPriceId(price.getPriceId());
                apStkMdl.setPrice(price.getPrice());
                apStkMdl.setPriceVat(price.getVat());
                if ((apSaleTrans.isCheckDebit()) && (!com.viettel.im.common.util.DataUtil.safeEqual(stkMdl.getStockTypeId(), com.viettel.im.common.util.Constant.STOCK_ISDN_HOMEPHONE)) && (!com.viettel.im.common.util.DataUtil.safeEqual(stkMdl.getStockTypeId(), com.viettel.im.common.util.Constant.STOCK_ISDN_MOBILE)) && (!com.viettel.im.common.util.DataUtil.safeEqual(stkMdl.getStockTypeId(), com.viettel.im.common.util.Constant.STOCK_ISDN_PSTN))) {
                    com.viettel.im.database.BO.Price basicPrice = findBasicPrice(stockModelId, pricePolicy, sessionDB);
                    if (basicPrice == null) {
                        apStkMdl.setBasicPrice(null);
                    } else {
                        apStkMdl.setBasicPrice(basicPrice.getPrice());
                    }
                }
                if ((price.getVat() == null) || (price.getVat() == 0L)) {
                    lstAPMdlNotVat.add(apStkMdl);
                } else {
                    lstAPMdlVat.add(apStkMdl);
                }
                int numOfStock = 0;
                if (shopDAO.isAgentChannel(apSaleTrans.getShopId())) {
                    apStkMdl.setOwnerType(com.viettel.im.common.util.Constant.OWNER_TYPE_SHOP);
                    apStkMdl.setOwnerId(apSaleTrans.getShopId());
                    apStkMdl.setCheckShopStock(true);
                }
                if (com.viettel.im.common.util.DataUtil.safeEqual(ssModel.getCheckShopStock(), 1L)) {
                    apStkMdl.setOwnerType(com.viettel.im.common.util.Constant.OWNER_TYPE_SHOP);
                    apStkMdl.setOwnerId(apSaleTrans.getShopId());
                    apStkMdl.setCheckShopStock(true);
                }
                if (apStkMdl.isCheckShopStock()) {
                    numOfStock++;
                }
                if (com.viettel.im.common.util.DataUtil.safeEqual(ssModel.getCheckStaffStock(), 1L)) {
                    apStkMdl.setOwnerType(com.viettel.im.common.util.Constant.OWNER_TYPE_STAFF);
                    apStkMdl.setOwnerId(apSaleTrans.getStaffId());
                    apStkMdl.setCheckStaffStock(true);
                    numOfStock++;
                }
                List listStckSpec = sssDAO.findStockSpecIncludeStockType(stkMdl.getStockTypeId(), saleServicesId);
                if (listStckSpec != null) {
                    apStkMdl.setCheckSpecStock(true);
                    numOfStock++;
                }
                if (com.viettel.im.common.util.DataUtil.safeEqual(ssModel.getUpdateStock(), 1L)) {
                    if (!com.viettel.im.common.util.DataUtil.safeEqual(stkMdl.getCheckSerial(), 1L)) {
                        if (numOfStock != 1) {
                            return APResult.ERR_CONFUSE_STOCK;
                        }
                    } else if ((lstSerial != null) && (lstSerial.size() > 0)) {
                        com.viettel.im.database.DAO.SerialPair pair = (com.viettel.im.database.DAO.SerialPair) lstSerial.get(0);
                        String selectedFields = " status, owner_type, owner_id ";
                        List lst;
                        if ((apStkMdl instanceof com.viettel.im.database.BO.APStockPstnBean)) {
                            lst = identifyStockToManipulate(apSaleTrans, apStkMdl, selectedFields, "isdn", pair.getFromSerial(), saleServicesId, ssModel, sessionDB);
                        } else {
                            lst = identifyStockToManipulate(apSaleTrans, apStkMdl, selectedFields, "serial", pair.getFromSerial(), saleServicesId, ssModel, sessionDB);
                        }
                        if (lst.size() == 1) {
                            Object[] objs = (Object[]) (Object[]) lst.get(0);

                            Long tmpOwnerType = Long.valueOf(objs[1] != null ? Long.parseLong(objs[1].toString()) : -1L);
                            Long tmpOwnerId = Long.valueOf(objs[2] != null ? Long.valueOf(objs[2].toString()) : -1L);
                            apStkMdl.setOwnerType(tmpOwnerType);
                            apStkMdl.setOwnerId(tmpOwnerId);
                        } else {
                            return APResult.ERR_SERIAL_NOT_IN_STOCK;
                        }
                    }
                }
                com.viettel.im.database.DAO.StockTypeDAO stockTypeDAO = new com.viettel.im.database.DAO.StockTypeDAO(sessionDB);
                com.viettel.im.database.BO.StockType stockType = com.viettel.im.database.DAO.StockTypeDAO.findById(sessionDB, stkMdl.getStockTypeId());
                apStkMdl.setStockTypeName(stockType.getName());
                apStkMdl.setStockModelCode(stkMdl.getStockModelCode());
                apStkMdl.setStockModelName(stkMdl.getName());
                apStkMdl.setAccountingModelCode(stkMdl.getAccountingModelCode());
                apStkMdl.setAccountingModelName(stkMdl.getAccountingModelName());
            }
        }
        boolean isEnough;
        if (apSaleTrans.isCheckGood()) {
            List<com.viettel.im.database.BO.SaleServicesModel> lstMdlType = ssmDAO.findByCode(apSaleTrans.getSaleServiceCode());
            isEnough = true;
            for (com.viettel.im.database.BO.SaleServicesModel mdlType : lstMdlType) {
                isEnough = false;
                if (lstAPModel != null) {
                    for (APStockModelBean apStkMdl : lstAPModel) {
                        if (com.viettel.im.common.util.DataUtil.safeEqual(mdlType.getStockTypeId(), apStkMdl.getStockTypeId())) {
                            isEnough = true;
                            break;
                        }
                    }
                    if (!isEnough) {
                        return APResult.ERR_LIST_STOCK_MODEL_NOT_ENOUHT;
                    }
                }
            }
        }
        return APResult.SUCCESS;
    }

    com.viettel.im.database.BO.Price getPriceInServices(Long saleServicesModelId, Long stockModelId, Session session) throws Exception {
        try {
            StringBuffer strQuery = new StringBuffer("from SaleServicesDetail ");
            strQuery.append("where stockModelId = ? and status = ? and saleServicesModelId = ? ");
            Query query = session.createQuery(strQuery.toString());
            query.setParameter(0, stockModelId);
            query.setParameter(1, com.viettel.im.common.util.Constant.STATUS_ACTIVE);
            query.setParameter(2, saleServicesModelId);

            List listSaleServicesDetail = query.list();
            if ((listSaleServicesDetail == null) || (listSaleServicesDetail.isEmpty())) {
                return null;
            }

            Long priceId = ((com.viettel.im.database.BO.SaleServicesDetail) listSaleServicesDetail.get(0)).getPriceId();
            if (priceId == null) {
                return null;
            }

            Date now = new Date();
            List listParameter = new ArrayList();
            strQuery = new StringBuffer("from Price ");
            strQuery.append("where 1 = 1  ");
            strQuery.append("and priceId = ?  ");
            listParameter.add(priceId);
            strQuery.append("and status = ?  ");
            listParameter.add(com.viettel.im.common.util.Constant.STATUS_ACTIVE);
            strQuery.append("and trunc(staDate) <= trunc(?) ");
            listParameter.add(now);
            strQuery.append("and (endDate is null or trunc(endDate) >= trunc(?)) ");
            listParameter.add(now);
            query = session.createQuery(strQuery.toString());
            for (int i = 0; i < listParameter.size(); i++) {
                query.setParameter(i, listParameter.get(i));
            }
            List listPrice = query.list();
            if ((listPrice != null) && (listPrice.size() > 0)) {
                return (com.viettel.im.database.BO.Price) listPrice.get(0);
            }
            return null;
        } catch (Exception re) {
            re.printStackTrace();
            throw re;
        }

    }

    private List identifyStockToManipulate(APSaleTrans apSaleTrans, APStockModelBean apStkMdl, String selectedFields, String whereFieldName, String whereFieldValue, Long saleServiceId, com.viettel.im.database.BO.SaleServicesModel saleServicesModel, Session session)
            throws Exception {
        List listResult = new ArrayList();
        try {
            com.viettel.im.database.DAO.SaleServicesModelDAO ssmDAO = new com.viettel.im.database.DAO.SaleServicesModelDAO(session);

            List colParameter = new ArrayList();
            String tableName = com.viettel.im.database.DAO.CommonDAO.getTableNameByStockType(apStkMdl.getStockTypeId(), "normal");
            if (saleServicesModel == null) {
                saleServicesModel = ssmDAO.findBySaleSerIdAndStkTypId(saleServiceId, apStkMdl.getStockTypeId(), apStkMdl.getStockModelId(), null);
            }
            if (saleServicesModel == null) {
                return listResult;
            }
            Long saleServicesModelId = saleServicesModel.getSaleServicesModelId();

            StringBuilder queryString = new StringBuilder(new StringBuilder().append("select ").append(selectedFields).append(" from ").append(tableName).append(" model ").append(" where 1 = 1 ").toString());
            if ((whereFieldName != null) && ((whereFieldName.toLowerCase().trim().equals("serial")) || (whereFieldName.toLowerCase().trim().equals("isdn")))) {
                queryString.append(new StringBuilder().append("and to_number(").append(whereFieldName).append(") =? ").toString());
            } else {
                queryString.append(new StringBuilder().append("and ").append(whereFieldName).append(" =? ").toString());
            }
            colParameter.add(whereFieldValue);

            queryString.append("and model.stock_model_id in (select stock_model_id from sale_services_detail where sale_services_model_id = ?) ");
            colParameter.add(saleServicesModelId);

            StringBuffer strShopWhereClause = new StringBuffer("(1 = 0) ");
            if (apStkMdl.isCheckSpecStock()) {
                strShopWhereClause.append("or (model.owner_type = ? and model.owner_id in (select shop_id from sale_services_stock where sale_services_model_id =?)) ");
                colParameter.add(com.viettel.im.common.util.Constant.OWNER_TYPE_SHOP);
                colParameter.add(saleServicesModelId);
            }
            if ((apStkMdl.isCheckStaffStock()) && (apSaleTrans.getStaffId() != null)) {
                strShopWhereClause.append("or (model.owner_type = ? and model.owner_id = ?) ");
                colParameter.add(com.viettel.im.common.util.Constant.OWNER_TYPE_STAFF);
                colParameter.add(apSaleTrans.getStaffId());
            }
            if ((apStkMdl.isCheckShopStock()) && (apSaleTrans.getShopId() != null)) {
                strShopWhereClause.append("or (model.owner_type = ? and model.owner_id = ?) ");
                colParameter.add(com.viettel.im.common.util.Constant.OWNER_TYPE_SHOP);
                colParameter.add(apSaleTrans.getShopId());
            }
            queryString.append(new StringBuilder().append(" and (").append(strShopWhereClause.toString()).append(")").toString());

            queryString.append(" and rownum < ? ");
            colParameter.add(Integer.valueOf(1000));

            Query queryObject = session.createSQLQuery(queryString.toString());
            for (int i = 0; i < colParameter.size(); i++) {
                queryObject.setParameter(i, colParameter.get(i));
            }
            return queryObject.list();
        } catch (Exception re) {
            re.printStackTrace();
            throw re;
        }

    }

    public com.viettel.im.database.BO.Price findBasicPrice(Long stockModelId, String pricePolicy, Session session)
            throws Exception {
        StringBuilder sqlBuffer = new StringBuilder();
        List parameterList = new ArrayList();

        sqlBuffer.append(" FROM ");
        sqlBuffer.append(" Price pri ");
        sqlBuffer.append(" WHERE 1 = 1 ");
        sqlBuffer.append(" AND ");
        sqlBuffer.append(" pri.stockModelId = ? ");
        parameterList.add(stockModelId);
        sqlBuffer.append(" AND ");
        sqlBuffer.append(" pri.type = ? ");
        parameterList.add("23");
        sqlBuffer.append(" AND ");
        sqlBuffer.append(" pri.pricePolicy = ? ");
        parameterList.add(pricePolicy);
        sqlBuffer.append(" AND ");
        sqlBuffer.append(" pri.staDate < trunc(sysdate) + 1 ");
        sqlBuffer.append(" AND (");
        sqlBuffer.append(" (pri.endDate >= trunc(sysdate) ");

        sqlBuffer.append(" AND ");
        sqlBuffer.append(" pri.endDate IS NOT NULL) ");

        sqlBuffer.append(" OR ");
        sqlBuffer.append(" pri.endDate IS NULL) ");


        sqlBuffer.append(" AND ");
        sqlBuffer.append(" pri.status = ? ");
        parameterList.add(1l);

        Query queryObject = session.createQuery(sqlBuffer.toString());

        for (int i = 0; i < parameterList.size(); i++) {
            queryObject.setParameter(i, parameterList.get(i));
        }
        List listPrice = queryObject.list();
        if ((listPrice != null) && (listPrice.size() == 1)) {
            return (com.viettel.im.database.BO.Price) listPrice.get(0);
        }
        return null;
    }

    private APResult saveDetailSaleTrans(boolean isRepair, APStockModelBean apStkMdl, com.viettel.im.database.BO.SaleTrans saleTrans, APSaleTrans apSaleTrans, SaleServices saleServices, com.viettel.im.database.BO.SaleServicesPrice saleServicesPrice, Session session)
            throws Exception {
        Long saleTransDetailId = getSequence("SALE_TRANS_DETAIL_SEQ", session);
        com.viettel.im.database.BO.SaleTransDetail saleTransDetail = new com.viettel.im.database.BO.SaleTransDetail();
        Double amount = Double.valueOf(apStkMdl.getPrice().doubleValue() * apStkMdl.getQuantity().longValue());

        Double notVatAmount = Double.valueOf(amount.doubleValue() / (1.0D + apStkMdl.getPriceVat().doubleValue() / 100.0D));
        Double vatAmount = Double.valueOf(amount.doubleValue() - notVatAmount.doubleValue());

        List<com.viettel.im.database.BO.SaleTransDetail> lstStd = null;
        if (apSaleTrans.isTransOW()) {
            lstStd = com.viettel.im.database.DAO.SaleTransDetailDAO.findBySaleTransIdAndStockModelId(session, saleTrans.getSaleTransId(), apStkMdl.getStockModelId());
        }
        if ((lstStd != null) && (lstStd.size() > 0)) {
            com.viettel.im.database.BO.SaleTransDetail tmpstd = (com.viettel.im.database.BO.SaleTransDetail) lstStd.get(0);
            tmpstd.setQuantity(Long.valueOf(tmpstd.getQuantity().longValue() + apStkMdl.getQuantity().longValue()));
            tmpstd.setAmount(Double.valueOf(tmpstd.getAmount().doubleValue() + amount.doubleValue()));
            tmpstd.setVatAmount(Double.valueOf(tmpstd.getVatAmount().doubleValue() + vatAmount.doubleValue()));
            saleTrans.setAmountTax(Double.valueOf(saleTrans.getAmountTax().doubleValue() + amount.doubleValue()));
            saleTrans.setAmountNotTax(Double.valueOf(saleTrans.getAmountNotTax().doubleValue() + notVatAmount.doubleValue()));
            saleTrans.setTax(Double.valueOf(saleTrans.getTax().doubleValue() + vatAmount.doubleValue()));

            session.saveOrUpdate(tmpstd);
        } else {
            saleTransDetail.setSaleTransDetailId(saleTransDetailId);
            saleTransDetail.setSaleTransId(saleTrans.getSaleTransId());
            saleTransDetail.setSaleTransDate(saleTrans.getSaleTransDate());
            saleTransDetail.setStockModelId(apStkMdl.getStockModelId());
            saleTransDetail.setStateId(com.viettel.im.common.util.Constant.STATE_NEW);
            saleTransDetail.setPriceId(apStkMdl.getPriceId());
            saleTransDetail.setQuantity(apStkMdl.getQuantity());


            saleTransDetail.setStockTypeId(apStkMdl.getStockTypeId());
            saleTransDetail.setStockTypeName(apStkMdl.getStockTypeName());
            saleTransDetail.setStockModelCode(apStkMdl.getStockModelCode());
            saleTransDetail.setStockModelName(apStkMdl.getStockModelName());
            saleTransDetail.setAccountingModelCode(apStkMdl.getAccountingModelCode());
            saleTransDetail.setAccountingModelName(apStkMdl.getAccountingModelName());
            saleTransDetail.setPrice(apStkMdl.getPrice());
            saleTransDetail.setPriceVat(apStkMdl.getPriceVat());
            saleTransDetail.setSaleServicesCode(saleServices.getCode());
            saleTransDetail.setSaleServicesName(saleServices.getName());



            saleTransDetail.setAmount(amount);
            saleTransDetail.setVatAmount(vatAmount);
            saleTransDetail.setSaleServicesId(saleTrans.getSaleServiceId());
            saleTransDetail.setSaleServicesPriceId(saleTrans.getSaleServicePriceId());

            session.save(saleTransDetail);
            saleTrans.setAmountTax(Double.valueOf(saleTrans.getAmountTax().doubleValue() + amount.doubleValue()));
            saleTrans.setAmountNotTax(Double.valueOf(saleTrans.getAmountNotTax().doubleValue() + notVatAmount.doubleValue()));
            saleTrans.setTax(Double.valueOf(saleTrans.getTax().doubleValue() + vatAmount.doubleValue()));
        }
        if (isRepair) {
            com.viettel.im.database.BO.StockTransDetail stDetail = new com.viettel.im.database.BO.StockTransDetail();
            stDetail.setStockTransId(saleTrans.getStockTransId());
            stDetail.setStockModelId(apStkMdl.getStockModelId());
            stDetail.setStateId(com.viettel.im.common.util.Constant.STATE_NEW);
            stDetail.setQuantityRes(Long.valueOf(1L));
            stDetail.setQuantityReal(Long.valueOf(1L));
            stDetail.setCreateDatetime(saleTrans.getSaleTransDate());
            session.save(stDetail);
        }
        com.viettel.im.database.DAO.StockTotalDAO stkTotalDAO = new com.viettel.im.database.DAO.StockTotalDAO(session);
        com.viettel.im.database.DAO.StockCommonDAO stkCmnDAO = new com.viettel.im.database.DAO.StockCommonDAO(session);
        if (!apSaleTrans.isTransConference()) {
            if (!com.viettel.im.common.util.DataUtil.safeEqual(apStkMdl.getUpdateStock(), Long.valueOf(1L))) {
                if (!(apStkMdl instanceof com.viettel.im.database.BO.APStockPstnBean)) {
                    if (apSaleTrans.isCheckDebit()) {
                        com.viettel.im.database.BO.DebitInfo debitInfo = apSaleTrans.getDebitInfo(session, apStkMdl.getPosId(), com.viettel.im.common.util.Constant.OWNER_TYPE_SHOP);

                        debitInfo.getLstModel().add(apStkMdl);
                        if (debitInfo.isCheckDebit()) {
                            if (apStkMdl.getBasicPrice() == null) {
                                return APResult.ERR_BASE_PRICE_NOT_FOUND;
                            }
                            debitInfo.setBasicDebit(Double.valueOf(debitInfo.getBasicDebit().doubleValue() + apStkMdl.getBasicPrice().doubleValue()));
                        }
                    }
                    com.viettel.im.database.BO.StockTotal stkTotal = stkTotalDAO.findByOwnerAndStockModel(apStkMdl.getPosId(), com.viettel.im.common.util.Constant.OWNER_TYPE_SHOP, apStkMdl.getStockModelId());
                    if (stkTotal == null) {
                        return APResult.ERR_STOCK_EMPTY;
                    }
                    if (stkTotal.getQuantityIssue().longValue() - apStkMdl.getQuantity().longValue() < 0L) {
                        return APResult.ERR_STOCK_QUANTIY_NOT_ENOUTH;
                    }
                    stkTotal.setQuantityIssue(Long.valueOf(stkTotal.getQuantityIssue().longValue() - apStkMdl.getQuantity().longValue()));
                    if (isRepair) {
                        if (saleTrans.getStockTransId() != null) {
                            if (stkTotal.getQuantity().longValue() - apStkMdl.getQuantity().longValue() < 0L) {
                                return APResult.ERR_STOCK_QUANTIY_NOT_ENOUTH;
                            }
                        }
                        List<com.viettel.im.database.DAO.SerialPair> lstSerial = apStkMdl.getLstSerial();
                        if ((lstSerial != null) && (lstSerial.size() > 0)) {
                            for (com.viettel.im.database.DAO.SerialPair pair : lstSerial) {
                                com.viettel.im.database.BO.SaleTransSerial saleTransSerial = new com.viettel.im.database.BO.SaleTransSerial();
                                Long saleTransSerialId = getSequence("SALE_TRANS_SERIAL_SEQ", session);
                                saleTransSerial.setSaleTransSerialId(saleTransSerialId);
                                saleTransSerial.setSaleTransDetailId(saleTransDetailId);
                                saleTransSerial.setStockModelId(apStkMdl.getStockModelId());
                                saleTransSerial.setSaleTransDate(saleTrans.getSaleTransDate());

                                saleTransSerial.setFromSerial(pair.getFromSerial());
                                saleTransSerial.setToSerial(pair.getToSerial());
                                saleTransSerial.setQuantity(pair.getQuantity());
                                session.save(saleTransSerial);


                                com.viettel.im.database.BO.StockTransSerial stockTransSerial = new com.viettel.im.database.BO.StockTransSerial();
                                stockTransSerial.setStockTransId(saleTrans.getStockTransId());
                                stockTransSerial.setStockModelId(apStkMdl.getStockModelId());
                                stockTransSerial.setCreateDatetime(saleTrans.getSaleTransDate());
                                stockTransSerial.setFromSerial(pair.getFromSerial());
                                stockTransSerial.setToSerial(pair.getToSerial());
                                stockTransSerial.setQuantity(pair.getQuantity());
                                stockTransSerial.setStateId(com.viettel.im.common.util.Constant.STATE_NEW);

                                session.save(stockTransSerial);
                                if (!(apStkMdl instanceof com.viettel.im.database.BO.APStockPstnBean)) {
                                    boolean bUpdateSuccess = stkCmnDAO.expStockSerialInSaleTrans(com.viettel.im.common.util.Constant.OWNER_TYPE_SHOP, apStkMdl.getPosId(), apStkMdl.getStockTypeId(), apStkMdl.getStockModelId(), pair.getFromSerial(), pair.getToSerial(), pair.getQuantity());
                                    if (!bUpdateSuccess) {
                                        return APResult.ERR_UPDATE_LIST_MODEL;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                saleTransDetail.setTransferGood("1");
                if (!(apStkMdl instanceof com.viettel.im.database.BO.APStockPstnBean)) {
                    if (apSaleTrans.isCheckDebit()) {
                        com.viettel.im.database.BO.DebitInfo debitInfo = apSaleTrans.getDebitInfo(session, apStkMdl.getOwnerId(), apStkMdl.getOwnerType());
                        debitInfo.getLstModel().add(apStkMdl);
                        if (debitInfo.isCheckDebit()) {
                            if (apStkMdl.getBasicPrice() == null) {
                                return APResult.ERR_BASE_PRICE_NOT_FOUND;
                            }
                            debitInfo.setBasicDebit(Double.valueOf(debitInfo.getBasicDebit().doubleValue() + apStkMdl.getBasicPrice().doubleValue()));
                        }
                    }
                    com.viettel.im.database.BO.StockTotal stkTotal = stkTotalDAO.findByOwnerAndStockModel(apStkMdl.getPosId(), com.viettel.im.common.util.Constant.OWNER_TYPE_SHOP, apStkMdl.getStockModelId());
                    if (stkTotal == null) {
                        return APResult.ERR_STOCK_QUANTIY_NOT_ENOUTH;
                    }
                    if (stkTotal.getQuantityIssue().longValue() - apStkMdl.getQuantity().longValue() < 0L) {
                        return APResult.ERR_STOCK_QUANTIY_NOT_ENOUTH;
                    }
                    stkTotal.setQuantityIssue(Long.valueOf(stkTotal.getQuantityIssue().longValue() - apStkMdl.getQuantity().longValue()));
                }
                if (apStkMdl.isCheckSerial().booleanValue()) {
                    List<com.viettel.im.database.DAO.SerialPair> lstSerial = apStkMdl.getLstSerial();
                    for (com.viettel.im.database.DAO.SerialPair pair : lstSerial) {
                        com.viettel.im.database.BO.SaleTransSerial saleTransSerial = new com.viettel.im.database.BO.SaleTransSerial();
                        Long saleTransSerialId = getSequence("SALE_TRANS_SERIAL_SEQ", session);
                        saleTransSerial.setSaleTransSerialId(saleTransSerialId);
                        saleTransSerial.setSaleTransDetailId(saleTransDetailId);
                        saleTransSerial.setStockModelId(apStkMdl.getStockModelId());
                        saleTransSerial.setSaleTransDate(saleTrans.getSaleTransDate());

                        saleTransSerial.setFromSerial(pair.getFromSerial());
                        saleTransSerial.setToSerial(pair.getToSerial());
                        saleTransSerial.setQuantity(pair.getQuantity());
                        session.save(saleTransSerial);
                        if (!(apStkMdl instanceof com.viettel.im.database.BO.APStockPstnBean)) {
                            boolean bUpdateSuccess = stkCmnDAO.expStockSerialInSaleTrans(apStkMdl.getOwnerType(), apStkMdl.getOwnerId(), apStkMdl.getStockTypeId(), apStkMdl.getStockModelId(), pair.getFromSerial(), pair.getToSerial(), pair.getQuantity());
                            if (!bUpdateSuccess) {
                                return APResult.ERR_UPDATE_LIST_MODEL;
                            }
                        }
                    }
                }
            }
        }
        return APResult.SUCCESS;
    }

    public String[] reduceDebit(Long ownerId, Long ownerType, Double amount, boolean checkResetDate, Date createDate, Session session) throws Exception {
        String[] strResult = new String[3];
        try {
            strResult[0] = "";
            com.viettel.im.database.BO.StockOwnerTmp stockOwnerTmp = getStockOwnerTmpCheckDebit(ownerId, ownerType, session);
            //neu check ngay reset
            if (stockOwnerTmp != null && stockOwnerTmp.getMaxDebit() != null) {
                session.refresh(stockOwnerTmp, org.hibernate.LockMode.UPGRADE);
                if (stockOwnerTmp.getCurrentDebit() == null) {
                    stockOwnerTmp.setCurrentDebit(0D);
                }
                if (checkResetDate) {
                    if (createDate != null) {
                        createDate = com.viettel.im.common.util.DateTimeUtils.convertStringToDateTimeVunt(com.viettel.im.common.util.DateTimeUtils.convertDateTimeToString(createDate));
                    }

                    if (stockOwnerTmp.getDateReset() != null) {
                        if (createDate != null && com.viettel.im.common.util.DateTimeUtils.convertStringToDateTimeVunt(com.viettel.im.common.util.DateTimeUtils.convertDateTimeToString(getSysdate(session))).compareTo(com.viettel.im.common.util.DateTimeUtils.addDate(createDate, stockOwnerTmp.getDateReset().intValue())) >= 0) {
                        } else {
                            stockOwnerTmp.setCurrentDebit(stockOwnerTmp.getCurrentDebit() - amount);
                        }
                    } else {
                        stockOwnerTmp.setCurrentDebit(stockOwnerTmp.getCurrentDebit() - amount);
                    }
                    session.update(stockOwnerTmp);
                    return strResult;

                } else {
                    stockOwnerTmp.setCurrentDebit(stockOwnerTmp.getCurrentDebit() - amount);
                    session.update(stockOwnerTmp);
                    return strResult;
                }
            } else {
                return strResult;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            strResult[0] = "ERR.SAE.147";
            return strResult;
        }
    }

    public static com.viettel.im.database.BO.StockOwnerTmp getStockOwnerTmpCheckDebit(Long ownerId, Long ownerType, Session session) {
        String sql = "From StockOwnerTmp where ownerId = ? and ownerType = ?";
        Query query = session.createQuery(sql);
        query.setParameter(0, ownerId);
        query.setParameter(1, ownerType.toString());
        List<com.viettel.im.database.BO.StockOwnerTmp> list = query.list();
        if (list != null && list.size() > 0) {
            return list.get(0);
        } else {
            return null;
        }

    }

    public Date getSysdate(Session session) {
        SQLQuery query = session.createSQLQuery("Select sysdate From dual");
        List<Date> lst = query.list();
        if (lst.size() > 0) {
            return lst.get(0);
        }
        return new Date();
    }

    public String[] reduceDebitTotal(Long ownerId, Long ownerType, Long stockModelId, Long stateId, Long status, Long amount, boolean checkResetDate, Date createDate, Session session) throws Exception {
        String[] strResult = new String[3];
        try {
            strResult[0] = "";
            com.viettel.im.database.BO.StockTotal stockTotal = getStockTotalCheckDebit(ownerId, ownerType, stockModelId, stateId, status, session);
            //neu check ngay reset
            if (stockTotal != null) {
                //session.refresh(stockTotal, LockMode.UPGRADE);
                if (stockTotal.getMaxDebit() != null) {
                    if (stockTotal.getCurrentDebit() == null) {
                        stockTotal.setCurrentDebit(0L);
                    }
                    if (checkResetDate) {
                        if (createDate != null) {
                            createDate = com.viettel.im.common.util.DateTimeUtils.convertStringToDateTimeVunt(com.viettel.im.common.util.DateTimeUtils.convertDateTimeToString(createDate));
                        }
                        if (stockTotal.getDateReset() != null) {
                            if (createDate != null && com.viettel.im.common.util.DateTimeUtils.convertStringToDateTimeVunt(com.viettel.im.common.util.DateTimeUtils.convertDateTimeToString(getSysdate(session))).compareTo(com.viettel.im.common.util.DateTimeUtils.addDate(createDate, stockTotal.getDateReset().intValue())) >= 0) {
                            } else {
                                //stockTotal.setCurrentDebit(stockTotal.getCurrentDebit() - amount);
                                updateStockTotal(ownerId, ownerType, stockModelId, stateId, status, -amount, session);
                            }
                        } else {
                            //stockTotal.setCurrentDebit(stockTotal.getCurrentDebit() - amount);
                            updateStockTotal(ownerId, ownerType, stockModelId, stateId, status, -amount, session);
                        }
                        //getSession().update(stockTotal);
                        return strResult;

                    } else {
                        updateStockTotal(ownerId, ownerType, stockModelId, stateId, status, -amount, session);
                        //stockTotal.setCurrentDebit(stockTotal.getCurrentDebit() - amount);
                        //getSession().update(stockTotal);
                        return strResult;
                    }
                } else {
                    return strResult;
                }
            } else {
                return strResult;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            strResult[0] = "ERR.SAE.147";
            return strResult;
        }
    }

    public com.viettel.im.database.BO.StockTotal getStockTotalCheckDebit(Long ownerId, Long ownerType, Long stockModelId, Long stateId, Long status, Session session) {
        String sql = "From StockTotal where id.ownerId = ? and id.ownerType = ? and id.stockModelId = ?"
                + " and id.stateId = ? and status = ?";
        Query query = session.createQuery(sql);
        query.setParameter(0, ownerId);
        query.setParameter(1, ownerType);
        query.setParameter(2, stockModelId);
        query.setParameter(3, stateId);
        query.setParameter(4, status);
        List<com.viettel.im.database.BO.StockTotal> list = query.list();
        if (list != null && list.size() > 0) {
            return list.get(0);
        } else {
            return null;
        }

    }

    public int updateStockTotal(Long ownerId, Long ownerType, Long stockModelId, Long stateId, Long status, Long amount, Session session) {
        String strUpdateStockTotalQuery = "update stock_total "
                + "set current_Debit = nvl(current_Debit,0) + " + amount
                + ", modified_date = sysdate "
                + "where owner_id = ? and owner_type = ? and stock_model_id = ? and state_id = ? and status = ? ";
        Query qUpdateStockTotal = session.createSQLQuery(strUpdateStockTotalQuery);
        //Neu la giao dich ban hang noi bo --> cong hang vao kho cua hang cac truong hop ban hang khac cong hang vao kho nhan vien
        qUpdateStockTotal.setParameter(0, ownerId);
        qUpdateStockTotal.setParameter(1, ownerType);
        qUpdateStockTotal.setParameter(2, stockModelId);
        qUpdateStockTotal.setParameter(3, stateId);
        qUpdateStockTotal.setParameter(4, status);
        int i = qUpdateStockTotal.executeUpdate();
        return i;
    }

    public long getSequence(String sequenceName, Session session) throws Exception {
        String strQuery = "SELECT " + sequenceName + " .NextVal FROM Dual";
        Query queryObject = session.createSQLQuery(strQuery);
        BigDecimal bigDecimal = (BigDecimal) queryObject.uniqueResult();
        return bigDecimal.longValue();
    }
}
