package com.viettel.brcd.dao.pm;

import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.bccs.cm.model.ContractOffer;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.pm.database.BO.AttributeOffer;
import com.viettel.pm.database.BO.PricePlan;
import com.viettel.pm.database.DAO.PMAPI;
import java.util.List;
import java.util.Map;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ProductDAO extends BaseDAO {

    public static final String TRIAL_ATTRIBUTE_CODE = "TRIAL_DAY";

    public int getProductTrialDay(Session pmSession, String productCode, Long offerId) throws Exception {
        int trialDay = 0;
        if (offerId == null || offerId <= 0L) {
            offerId = PMAPI.getOfferIdByProductCode(productCode, pmSession);
        }
        if (offerId != null && offerId > 0L) {
            Map<String, String> atts = PMAPI.getAttributeParam(offerId, pmSession);
            if (atts != null && !atts.isEmpty()) {
                String value = atts.get(TRIAL_ATTRIBUTE_CODE);
                if (value != null) {
                    value = value.trim();
                    if (!value.isEmpty()) {
                        trialDay = Integer.parseInt(value);
                    }
                }
            }
        }
        return trialDay;
    }

    public String getVasIpAdslPricePlanCode(Session pmSession, ContractOffer contractOffer) {
        String result = Constants.VAS_IP_ADSL_500K_DEFAULT_VASIP1;
        if (contractOffer == null) {
            return result;
        }
        Long offerId = contractOffer.getOfferId();
        if (offerId == null || offerId <= 0L) {
            return result;
        }
        List<PricePlan> lstPricePlan = PMAPI.getListPricePlanByOfferId(offerId, pmSession);
        if (lstPricePlan != null && !lstPricePlan.isEmpty()) {
            for (PricePlan price : lstPricePlan) {
                if (price != null) {
                    if (Constants.EXCHANGE_TYPE_VAS_IP_ADSL.equals(price.getExchangeId())) {
                        String ppCode = price.getPricePlanCode();
                        if (ppCode != null) {
                            ppCode = ppCode.trim();
                            if (!ppCode.isEmpty()) {
                                return ppCode;
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    public String getFellowTypeByVasIpAdsl(Session pmSession, ContractOffer contractOffer) {
        String result = Constants.FELLOW_TYPE_ATTRIBUTE_VALUE_DEFAULT_VASIP1;
        if (contractOffer == null) {
            return result;
        }
        Long offerId = contractOffer.getOfferId();
        if (offerId == null || offerId <= 0L) {
            return result;
        }
        List<AttributeOffer> atts = PMAPI.getListAtrrForOffer(offerId, pmSession);
        if (atts != null && !atts.isEmpty()) {
            for (AttributeOffer att : atts) {
                if (att != null) {
                    String attName = att.getAttributeName();
                    if (attName != null) {
                        attName = attName.trim();
                        if (Constants.FELLOW_TYPE_ATTRIBUTE.equalsIgnoreCase(attName)) {
                            return att.getDefauleValue();
                        }
                    }
                }
            }
        }
        return result;
    }
}
