/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.dao.pre;

import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.bccs.cm.model.pre.ActionAuditPre;
import com.viettel.bccs.cm.model.pre.ActionDetailPre;
import com.viettel.bccs.cm.model.pre.ActionLogPrPre;
import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.brcd.common.util.InterfacePr;
import com.viettel.common.OriginalViettelMsg;
import com.viettel.common.ViettelService;
import java.util.Date;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class ActionLog extends BaseDAO {
    
    public static void logAuditDetail(Session cmPre, Long rowId, Long actionAuditId, String tableName, String colName,
            Object oldValue, Object newValue, Date issueDateTime)
            throws Exception {
        BaseSupplier baseSupplier = new BaseSupplier();
        Long actionDetailId = baseSupplier.getSequence(cmPre, "SEQ_ACTION_DETAIL");
        ActionDetailPre actionDetail = new ActionDetailPre();
        
        actionDetail.setActionDetailId(actionDetailId);
        actionDetail.setTableName(tableName);
        actionDetail.setRowId(rowId);
        actionDetail.setIssueDateTime(issueDateTime);
        actionDetail.setColName(colName);
        actionDetail.setOldValue((oldValue == null) ? "" : String.valueOf(oldValue));
        actionDetail.setNewValue((newValue == null) ? "" : String.valueOf(newValue));
        actionDetail.setActionAuditId(actionAuditId);
        cmPre.save(actionDetail);
        
    }
    
    public static void logAction(Session cmPre, Long actionAuditId, Long pkId, String pkType,
            Long reasonId, String actionCode, String logDescription, Date nowDate, String staffCode, String shopCode, String ip, Long fileId) throws Exception {
        try {
            ActionAuditPre actionAudit = new ActionAuditPre();
            actionAudit.setActionAuditId(actionAuditId);
            actionAudit.setActionCode(actionCode);
            actionAudit.setIp(ip);
            actionAudit.setIssueDatetime(nowDate);
            actionAudit.setReasonId(reasonId);
            actionAudit.setUserName(staffCode);
            actionAudit.setShopCode(shopCode);
            actionAudit.setPkType(pkType);
            actionAudit.setPkId(pkId);
            actionAudit.setDescription(logDescription);
            actionAudit.setFileId(fileId);
            cmPre.save(actionAudit);
        } catch (Exception e) {
            throw e;
        }
    }
    
    public static void saveActionLogPr(Session cmPre, ViettelService responsePr, String staffCode, String shopCode) {
        ActionLogPrPre actionLogPr = new ActionLogPrPre();
        String response = "Không thực hiện gửi lệnh lên tổng đài để phục vụ test Migrate dữ liệu";
        try {
            BaseSupplier baseSupplier = new BaseSupplier();
            Long id = baseSupplier.getSequence(cmPre, "SEQ_ACTION_LOG_PR");

            // save log
            actionLogPr.setId(id);
            actionLogPr.setCreateDate(new Date());
            
            if (responsePr.get("requestPr") != null) {
                String requestPr = responsePr.get("requestPr").toString();
                actionLogPr.setRequest(requestPr);

                // Edit by TuanTM2 - 23/11/2009: update them truong ISDN
                if (requestPr != null && !"".equals(requestPr)) {
                    int intdex_i = requestPr.indexOf("<MSISDN>");
                    if (intdex_i > 0) {
                        int intdex_j = requestPr.indexOf("</MSISDN>");
                        String isdn = requestPr.substring(intdex_i + 8, intdex_j);
                        if (isdn != null && !"".equals(isdn)) {
                            actionLogPr.setIsdn(isdn);
                        }
                    }
                }
            }
            actionLogPr.setUserName(staffCode);
            actionLogPr.setShopCode(shopCode);
            
            if (!InterfacePr.allowSend) {
                actionLogPr.setResponseCode("0");
                actionLogPr.setResponse(response);
            }
            
            if (responsePr != null) {
                // Fix loi > 4000 ky tu
                String responseXML = responsePr.toString();
                if (responseXML != null && responseXML.toString().trim().length() >= 4000) {
                    responseXML = responseXML.substring(0, 4000);
//                    int lengReponse = responseXML.toString().trim().length();
//                    responseXML = responseXML.substring(lengReponse - 3000, lengReponse);
                }
                if (responsePr.get("responseCode") != null
                        && !"".equals(responsePr.get("responseCode"))) {
                    actionLogPr.setResponse(responseXML);
                    actionLogPr.setResponseCode(responsePr.get("responseCode").toString().trim());
                } else {
                    actionLogPr.setResponse(responseXML);
                    actionLogPr.setResponseCode("1");
                }
            } else {
                actionLogPr.setRequest("Can not connect to provisioning");
                actionLogPr.setResponse("Can not connect to provisioning");
            }
        } catch (Exception ex) {
            actionLogPr.setRequest("Can not connect to provisioning");
            actionLogPr.setResponse("Can not connect to provisioning");
            actionLogPr.setException(ex.getMessage());
            actionLogPr.setResponseCode("1"); //fail
        } finally {
            cmPre.save(actionLogPr);
        }
    }
    
    public static void logActionV2(Long actionAuditId, Long pkId, String pkType,
            Long reasonId, String actionCode, String logDescription, Date nowDate, Session cmPreSession, String staffCode, String shopCode) throws Exception {
        try {
            ActionAuditPre actionAudit = new ActionAuditPre();
            actionAudit.setActionAuditId(actionAuditId);
            actionAudit.setActionCode(actionCode);
            actionAudit.setIp("mBCCS");
            actionAudit.setIssueDatetime(nowDate);
            actionAudit.setReasonId(reasonId);
            actionAudit.setUserName(staffCode);
            actionAudit.setShopCode(shopCode);
            actionAudit.setPkType(pkType);
            actionAudit.setPkId(pkId);
            actionAudit.setDescription(logDescription);
            cmPreSession.save(actionAudit);
        } catch (Exception e) {
            System.out.println("  " + e.getMessage());
            throw e;
        }
    }
    
    public static void logAuditDetailV2(Long rowId, Long actionAuditId, String tableName, String colName,
            Object oldValue, Object newValue, Date issueDateTime, Session cmPreSession)
            throws Exception {
        ActionDetailPre actionDetail = new ActionDetailPre();
        BaseSupplier baseSupplier = new BaseSupplier();
        Long actionDetailId = baseSupplier.getSequence(cmPreSession, "SEQ_ACTION_DETAIL");
        
        actionDetail.setActionDetailId(actionDetailId);
        actionDetail.setTableName(tableName);
        actionDetail.setRowId(rowId);
        actionDetail.setIssueDateTime(issueDateTime);
        actionDetail.setColName(colName);
        actionDetail.setOldValue((oldValue == null) ? "" : String.valueOf(oldValue));
        actionDetail.setNewValue((newValue == null) ? "" : String.valueOf(newValue));
        actionDetail.setActionAuditId(actionAuditId);
        cmPreSession.save(actionDetail);
        
    }
    
    public static void saveActionLogPr(Session cmPre, OriginalViettelMsg responsePr, String staffCode, String shopCode) {
        ActionLogPrPre actionLogPr = new ActionLogPrPre();
        String response = "";
        try {
            BaseSupplier baseSupplier = new BaseSupplier();
            Long id = baseSupplier.getSequence(cmPre, "SEQ_ACTION_LOG_PR");

            // save log
            actionLogPr.setId(id);
            actionLogPr.setCreateDate(new Date());
            
            if (responsePr.get("requestPr") != null) {
                String requestPr = responsePr.get("requestPr").toString();
                actionLogPr.setRequest(requestPr);

                // Edit by TuanTM2 - 23/11/2009: update them truong ISDN
                if (requestPr != null && !"".equals(requestPr)) {
                    int intdex_i = requestPr.indexOf("<MSISDN>");
                    if (intdex_i > 0) {
                        int intdex_j = requestPr.indexOf("</MSISDN>");
                        String isdn = requestPr.substring(intdex_i + 8, intdex_j);
                        if (isdn != null && !"".equals(isdn)) {
                            actionLogPr.setIsdn(isdn);
                        }
                    }
                }
            }
            actionLogPr.setUserName(staffCode);
            actionLogPr.setShopCode(shopCode);
            
            if (!InterfacePr.allowSend) {
                actionLogPr.setResponseCode("0");
                actionLogPr.setResponse(response);
            }
            
            if (responsePr != null) {
                // Fix loi > 4000 ky tu
                String responseXML = responsePr.toString();
                if (responseXML != null && responseXML.toString().trim().length() >= 4000) {
                    responseXML = responseXML.substring(0, 4000);
//                    int lengReponse = responseXML.toString().trim().length();
//                    responseXML = responseXML.substring(lengReponse - 3000, lengReponse);
                }
                if (responsePr.get("responseCode") != null
                        && !"".equals(responsePr.get("responseCode"))) {
                    actionLogPr.setResponse(responseXML);
                    actionLogPr.setResponseCode(responsePr.get("responseCode").toString().trim());
                } else {
                    actionLogPr.setResponse(responseXML);
                    actionLogPr.setResponseCode("1");
                }
            } else {
                actionLogPr.setRequest("Can not connect to provisioning");
                actionLogPr.setResponse("Can not connect to provisioning");
            }
        } catch (Exception ex) {
            actionLogPr.setRequest("Can not connect to provisioning");
            actionLogPr.setResponse("Can not connect to provisioning");
            actionLogPr.setException(ex.getMessage());
            actionLogPr.setResponseCode("1"); //fail
        } finally {
            cmPre.save(actionLogPr);
        }
    }
}
