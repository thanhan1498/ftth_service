package com.viettel.brcd.dao.pre;

import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.bccs.cm.model.pre.ActionLogPrPre;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

public class ActionLogPrDAO extends BaseDAO {

    public ActionLogPrPre findById(Session cmPosSession, Long id) {
        if (id == null || id <= 0L) {
            return null;
        }

        StringBuilder sql = new StringBuilder().append(" from ActionLogPrPre where id = ? ");
        Query query = cmPosSession.createQuery(sql.toString()).setParameter(0, id);

        List<ActionLogPrPre> logs = query.list();
        if (logs != null && !logs.isEmpty()) {
            return logs.get(0);
        }

        return null;
    }

    public ActionLogPrPre insert(Session sessoin, Date createDate, String userName, String shopCode, String isdn, String request, String response, String responseCode, String exception) {
        Long actionLogPrId = getSequence(sessoin, Constants.ACTION_LOG_PR_SEQ);
        return insert(sessoin, actionLogPrId, createDate, userName, shopCode, isdn, request, response, responseCode, exception);
    }

    public ActionLogPrPre insert(Session sessoin, Long actionLogPrId, Date createDate, String userName, String shopCode, String isdn, String request, String response, String responseCode, String exception) {
        ActionLogPrPre actionLogPr = new ActionLogPrPre();
        actionLogPr.setId(actionLogPrId);
        actionLogPr.setCreateDate(createDate);
        actionLogPr.setUserName(userName);
        actionLogPr.setShopCode(shopCode);
        actionLogPr.setIsdn(isdn);
        actionLogPr.setRequest(request);
        actionLogPr.setResponse(response);
        actionLogPr.setResponseCode(responseCode);
        actionLogPr.setException(exception);

        sessoin.save(actionLogPr);
        sessoin.flush();

        return actionLogPr;
    }
}
