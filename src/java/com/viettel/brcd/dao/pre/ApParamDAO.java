package com.viettel.brcd.dao.pre;

import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.pre.ApParamPre;
import com.viettel.bccs.cm.util.Constants;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class ApParamDAO extends BaseDAO {

    public List<ApParamPre> findByType(Session session, String type) {
        if (type == null) {
            return null;
        }
        type = type.trim();
        if (type.isEmpty()) {
            return null;
        }
        String sql = " from ApParamPre where paramType = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, type).setParameter(1, Constants.STATUS_USE);
        return query.list();
    }

    public List<ApParamPre> findByTypeCode(Session session, String type, String code) {
        if (type == null || code == null) {
            return null;
        }
        type = type.trim();
        if (type.isEmpty()) {
            return null;
        }
        code = code.trim();
        if (code.isEmpty()) {
            return null;
        }
        String sql = " from ApParamPre where paramType = ? and paramCode = ? and status = ? ";
        Query query = session.createQuery(sql).setParameter(0, type).setParameter(1, code).setParameter(2, Constants.STATUS_USE);
        return query.list();
    }

    public String getValueByTypeCode(Session session, String type, String code) {
        List<ApParamPre> params = findByTypeCode(session, type, code);
        if (params != null && !params.isEmpty()) {
            for (ApParamPre param : params) {
                if (param != null) {
                    String value = param.getParamValue();
                    if (value != null) {
                        value = value.trim();
                        if (!value.isEmpty()) {
                            return value;
                        }
                    }
                }
            }
        }
        return null;
    }

    public String getUserManualConfig(Session session, String type) {
        return getValueByTypeCode(session, type, Constants.USER_MANUAL_CONFIG);
    }

    /**
     * @author duyetdk
     * @param cmPosSession
     * @param paramType
     * @return
     */
    public List<ApParam> getApParamByType(Session cmPosSession, String paramType) {
        String sql = "select p from ApParam p where p.paramType = ? and status=1 ";
        Query query = cmPosSession.createQuery(sql);
        query.setParameter(0, paramType);
        List result = query.list();
        if (result != null && result.size() > 0) {
            return (List<ApParam>) result;
        }
        return new ArrayList<ApParam>();
    }

    public List<ApParam> searchApParam(Session sesssionCmPos, String paramType, String paramCode, Long status, String province, Logger log) {
        List<ApParam> result = null;
        try {
            List lstParam = new ArrayList();
            String sqlQuery = "from ApParam where 1=1 ";

            if (paramCode != null && !"".equals(paramCode.trim())) {
                sqlQuery += " and lower(paramCode) like lower(?) ";
                lstParam.add("%" + paramCode + "%");
            }

            if (paramType != null && !"".equals(paramType)) {
                sqlQuery += " and lower(paramType) like lower(?) ";
                lstParam.add("%" + paramType + "%");
            }

            if (province != null && !"".equals(province)) {
                sqlQuery += " and lower(paramValue) like lower(?) ";
                lstParam.add("%" + province + "%");
            }

            if (status != null && status >= 0L) {
                sqlQuery += " and status = ? ";
                lstParam.add(status);
            }

            Query query = sesssionCmPos.createQuery(sqlQuery);
            for (int i = 0; i < lstParam.size(); i++) {
                query.setParameter(i, lstParam.get(i));
            }

            result = query.list();

        } catch (Exception ex) {
            log.error("Error: ", ex);
        }
        return result;
    }

}
