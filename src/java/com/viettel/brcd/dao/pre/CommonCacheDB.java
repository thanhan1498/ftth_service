/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.dao.pre;

import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.pre.ApParamPre;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.common.util.ProConfig;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class CommonCacheDB {

    private static Map<String, Long> mapProductCount;
    private static Map<String, ProConfig> mapExchAccount = null;
    private static final Object lock = new Object();

    public static void clearCache() {
        synchronized (lock) {
            mapProductCount = null;
            mapExchAccount = null;
        }
    }

    public static Map<String, Long> getProductCheckCount(Session cmPre) {
        if (mapProductCount == null || mapProductCount.isEmpty()) {
            mapProductCount = new HashMap<String, Long>();
            synchronized (lock) {
                String sql = "FROM ApParamPre WHERE status = ? AND paramType = ?";
                Query query = cmPre.createQuery(sql);
                query.setParameter(0, Constants.STATUS_USE);
                query.setParameter(1, Constants.PRODUCT_CHECK_COUNT);
                List<ApParamPre> lst = query.list();
                for (int i = 0; i < lst.size(); i++) {
                    if (lst.get(i).getParamCode() != null && !"".equals(lst.get(i).getParamCode().trim())
                            && lst.get(i).getParamValue() != null && !"".equals(lst.get(i).getParamValue().trim())) {
                        mapProductCount.put(lst.get(i).getParamCode(), Long.parseLong(lst.get(i).getParamValue()));
                    }
                }
            }
        }
        return mapProductCount;
    }

    public static Map<String, ProConfig> getMapExchAccount(Session session) {

        if (mapExchAccount == null || mapExchAccount.isEmpty()) {
            synchronized (lock) {
                mapExchAccount = new HashMap<String, ProConfig>();

                String sql = "From ApParamPre Where status = ? and paramCode = ? and paramName = ?";
                List lstParam = new ArrayList();
                lstParam.add(Constants.AP_PARAM_STATUS_USED);
                lstParam.add(Constants.APPARAM_PROVISIONING_ACCOUNT);
                lstParam.add(Constants.PARAM_VALUE_EXCHANGE);

                Query query = session.createQuery(sql);
                if (lstParam != null && lstParam.size() > 0) {
                    for (int i = 0; i < lstParam.size(); i++) {
                        query.setParameter(i, lstParam.get(i));
                    }
                }

                List lstApparam = query.list();
                if (lstApparam != null && lstApparam.size() > 0) {
                    for (int i = 0; i < lstApparam.size(); i++) {
                        ApParamPre apParam = (ApParamPre) lstApparam.get(i);

                        if (apParam.getParamValue() != null && !"".equals(apParam.getParamValue().trim())
                                && apParam.getParamType() != null && !"".equals(apParam.getParamType().trim())) {
                            String value = apParam.getParamValue().trim();

                            String[] arr = value.split(",");
                            if (arr != null && arr.length >= 4) {
                                ProConfig proConfig = new ProConfig();
                                proConfig.setIp(arr[0].trim());
                                proConfig.setPort(arr[1].trim());
                                proConfig.setUsername(arr[2].trim());
                                proConfig.setPassword(arr[3].trim());

                                mapExchAccount.put(apParam.getParamType(), proConfig);
                            }
                        }
                    }
                }
            }
        }
        return mapExchAccount;
    }
}
