/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.dao.pre;

import com.viettel.bccs.cm.dao.interfaceCommon.InterfaceCommon;
import com.viettel.bccs.cm.model.pre.SubIdNoIdPre;
import java.util.List;
import java.util.Date;
import com.viettel.bccs.cm.model.pre.SubIsdnMbPre;
import com.viettel.bccs.cm.model.pre.SubSimMbPre;
import com.viettel.bccs.cm.model.pre.SubMbPre;
import com.viettel.bccs.cm.model.pre.SubIdNoPre;
import com.viettel.bccs.cm.model.pre.SubProductMbPre;
import com.viettel.bccs.cm.model.pre.SubRelProductPre;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.im.database.DAO.WebServiceIMDAO;
import com.viettel.pm.database.BO.ProductVasFeature;
import com.viettel.pm.database.DAO.PMAPI;
import java.util.ArrayList;
import org.hibernate.Query;
import org.hibernate.Session;
import com.viettel.common.ViettelService;
import com.viettel.im.database.BO.StockSim;
import com.viettel.pm.common.util.PMConstant;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author cuongdm
 */
public class CommonPre {

    public String replaceTextResponse(String result, List lstParam) {
        if (result != null && !"".equals(result.trim()) && lstParam != null && !lstParam.isEmpty()) {
            for (int i = 0; i < lstParam.size(); i++) {
                result = result.replace("{" + String.valueOf(i) + "}", lstParam.get(i) != null ? String.valueOf(lstParam.get(i)) : "");
            }
        }
        return result;
    }

    public void addSubIsdnMb(Session cmPre, Long subId, String isdn, Date startDate) throws Exception {
        SubIsdnMbPre subIsdnMb = new SubIsdnMbPre();
        subIsdnMb.setSubId(subId);
        subIsdnMb.setIsdn(isdn);
        subIsdnMb.setStaDatetime(startDate);
        subIsdnMb.setEndDatetime(null);
        subIsdnMb.setStatus(Constants.STATUS_USE);
        cmPre.save(subIsdnMb);
        cmPre.flush();
    }

    public void addSubSimMb(Session cmPre, Long subId, String imsi, String serial, Date startDate) throws Exception {
        SubSimMbPre SubSimMb = new SubSimMbPre();
        SubSimMb.setSubId(subId);
        SubSimMb.setImsi(imsi);
        SubSimMb.setSerial(serial);
        SubSimMb.setStaDatetime(startDate);
        SubSimMb.setEndDatetime(null);
        SubSimMb.setStatus(Constants.STATUS_USE);
        cmPre.save(SubSimMb);
        cmPre.flush();
    }

    public void updateSubProductMb(Session cmPre, SubMbPre subMb, Session cmPreSession, Date nowDate) throws Exception {
        SubProductMbPre subProductMb = new SubProductMbPre();
        subProductMb.setSubId(subMb.getSubId());
        subProductMb.setOfferId(subMb.getOfferId());
        subProductMb.setProductCode(subMb.getProductCode());
        subProductMb.setEffectDate(nowDate);
        subProductMb.setStatus(Constants.SUB_PRODUCT_MB_STATUS_USE);
        cmPre.save(subProductMb);
        cmPre.flush();
    }

    public void createSubIdNo(Session cmPre, SubMbPre subMb, String idNo, Date date, String app) {
        SubIdNoPre subIdNo = new SubIdNoPre();
        SubIdNoIdPre subIdNoId = new SubIdNoIdPre();
        subIdNoId.setSubId(subMb.getSubId());
        subIdNoId.setStaDatetime(date);
        subIdNo.setId(subIdNoId);
        subIdNo.setIdNo(idNo);
        subIdNo.setIsdn(subMb.getIsdn());
        subIdNo.setStatus(Constants.STATUS_ACTIVE__SUB_ID_NO);
        subIdNo.setApp(app);
        cmPre.save(subIdNo);
        cmPre.flush();
    }

    public static String updateIsdnToUsing(Session imSession, Long telSerivceId, String isdn)
            throws Exception {
        WebServiceIMDAO webIMDAO = new WebServiceIMDAO(imSession);
        Long returnCode = webIMDAO.changeIsdnStatus(isdn, telSerivceId);
        String errorMessage = com.viettel.bccs.cm.dao.interfaceCommon.InterfaceCommon.getReturnMessage(Constants.UPDATE_ISDN_TO_USING_ACTION_CODE, returnCode);
        return errorMessage;
    }

    public void recoverSubIdNo(Long subId, Session cmPreSession, Long actionAuditId, Date nowDate) throws Exception {
        if (subId != null) {
            String sql = " From SubIdNoPre where id.subId = ? and status = ? order by endDatetime desc";
            Query query = cmPreSession.createQuery(sql);
            query.setParameter(0, subId);
            query.setParameter(1, Constants.STATUS_NOT_USE);
            query.setMaxResults(Constants.MAX_RESUL_SEARCH_SUB_ID_NO.intValue());
            List lstSubIdNo = query.list();

            if (lstSubIdNo != null && lstSubIdNo.size() > 0) {
                SubIdNoPre subIdNo = (SubIdNoPre) lstSubIdNo.get(0);

                subIdNo.setStatus(Constants.STATUS_USE);
                Date endDate = subIdNo.getEndDatetime();
                subIdNo.setEndDatetime(null);

                cmPreSession.update(subIdNo);

                if (actionAuditId != null) {
                    //Ghi Log thay đổi trường status
                    ActionLog.logAuditDetail(cmPreSession, subId, actionAuditId,
                            "SUB_ID_NO", "STATUS", Constants.STATUS_NOT_USE, Constants.STATUS_USE, nowDate);
                    //Ghi Log thay đổi trường end_date
                    ActionLog.logAuditDetail(cmPreSession, subId, actionAuditId,
                            "SUB_ID_NO", "END_DATETIME", endDate, null, nowDate);
                }
            }
        }
    }

    public List<SubRelProductPre> getListDefaultVas(String productCode, Long subId, Session pmSession, Date nowDate) {
        List<ProductVasFeature> lstProduct = PMAPI.getListParamForProduct(productCode, pmSession);
        List<SubRelProductPre> lstSubRelProduct = new ArrayList<SubRelProductPre>();

        if (lstProduct != null && lstProduct.size() > 0) {
            for (int i = 0; i < lstProduct.size(); i++) {
                ProductVasFeature productVasFeature = lstProduct.get(i);
                if (productVasFeature.getIsConnected() != null && productVasFeature.getIsConnected().equals(0L)) {
                    SubRelProductPre subRelProduct = new SubRelProductPre();
                    subRelProduct.setIsConnected(productVasFeature.getIsConnected());
                    subRelProduct.setMainProductCode(productCode);
                    subRelProduct.setRelProductCode(productVasFeature.getVasCode());
                    subRelProduct.setSubId(subId);
                    subRelProduct.setRegDate(nowDate);
                    subRelProduct.setStaDatetime(nowDate);
                    subRelProduct.setStatus(Constants.SUB_REL_PRODUCT_STATUS_USE);

                    lstSubRelProduct.add(subRelProduct);
                }
            }
        }
        return lstSubRelProduct;
    }

    public List<SubRelProductPre> getListVasRecover(String productCode, Long subId, Session pmSession, Session cmPreSession, String endDate) {
        List<ProductVasFeature> lstProduct = PMAPI.getListParamForProduct(productCode, pmSession);

        List<ProductVasFeature> lstVasDefault = new ArrayList<ProductVasFeature>();
        if (lstProduct != null && lstProduct.size() > 0) {
            for (int i = 0; i < lstProduct.size(); i++) {
                ProductVasFeature productVasFeature = lstProduct.get(i);
                if (productVasFeature.getIsConnected() != null && productVasFeature.getIsConnected().equals(0L)) {
                    lstVasDefault.add(productVasFeature);
                }
            }
        }
        // 1ngay/1140 = 1 phut
        String sql = " from SubRelProductPre where subId = ?"
                + "and endDatetime >= to_date(?, 'DD/MM/YYYY HH24:MI:SS') - 1/1140"
                + "and (endDatetime < to_date(?, 'DD/MM/YYYY HH24:MI:SS') + 1/1140)";

        Query query = cmPreSession.createQuery(sql);
        query.setParameter(0, subId);
        query.setParameter(1, endDate);
        query.setParameter(2, endDate);

        List lstSubRel = query.list();

        if (lstSubRel != null && lstSubRel.size() > 0) {
            for (int i = lstSubRel.size() - 1; i >= 0; i--) {
                for (int j = 0; j < lstVasDefault.size(); j++) {
                    if (((SubRelProductPre) lstSubRel.get(i)).getRelProductCode().equals(lstVasDefault.get(j).getVasCode())) {
                        lstSubRel.remove(i);
                        break;
                    }
                }
            }
        }
        return lstSubRel;
    }

    public String getInfoSim(Session cmPre, String serial) {
        String sql = "SELECT   cardtype TYPE, "
                + "                 alg, "
                + "                 a3a8, "
                + "                 eki, "
                + "                 imsi, "
                + "                 opsno "
                + "          FROM   reg_kit_owner.stock_sim "
                + "         WHERE   serial = ?  ";
        Query query = cmPre.createSQLQuery(sql);
        query.setString(0, serial);
        List<Object[]> result = query.list();
        if (result.isEmpty()) {
            return "";
        }
        return result.get(0)[0].toString() + ";" + result.get(0)[1].toString()
                + ";" + result.get(0)[2].toString() + ";" + result.get(0)[3].toString()
                + ";" + result.get(0)[4].toString() + ";" + (result.get(0)[5] == null ? "" : result.get(0)[5].toString());

    }

    public ViettelService activePrePaidSubscriber(Session imSession, SubMbPre sub, Session pmSession, Session cmPreSession, boolean isRecover, String staffCode, String shopCode) throws Exception {
        try {
            if (sub != null) {
                ViettelService requestPr = new ViettelService();
                requestPr.set("productId", sub.getProductCode().trim());
                requestPr.set("modificationType", Constants.MODIFICATION_TYPE_REGISTER_NEW);

                Map lstParam = getSubscriberParam(cmPreSession, imSession, pmSession, sub, Constants.MODIFICATION_TYPE_REGISTER_NEW, isRecover);

                if (lstParam != null && !lstParam.isEmpty()) {
                    Iterator paramKey = lstParam.keySet().iterator();
                    while (paramKey.hasNext()) {
                        Object paramName = paramKey.next();
                        Object paramValue = lstParam.get(paramName);
                        if (paramValue != null) {
                            requestPr.set(paramName.toString(), paramValue);
                        } else {
                            requestPr.set(paramName.toString(), "");
                        }
                    }
                }
                ViettelService vtService = com.viettel.brcd.common.util.InterfacePr.request(cmPreSession, requestPr, staffCode, shopCode);
                return vtService;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static Map getSubscriberParam(Session cmPreSession, Session imSession, Session pmSession, SubMbPre sub, String modificationType, boolean isRecover) throws Exception {
        try {

            // get param from cm
            Map lstParam = new HashMap();
            lstParam.put("MSISDN", "855" + sub.getIsdn());

            // get resource from im
            lstParam.put("SERIAL", sub.getSerial());
            lstParam.put("IMSI", sub.getImsi());

            StockSim stkSim = new InterfaceCommon().getSimPrePaidByImsiOrSerial(imSession, sub.getImsi(), sub.getSerial());
            if (stkSim != null) {
                lstParam.put("EKI", stkSim.getEki());
                lstParam.put("K4SNO", stkSim.getKind());
                lstParam.put("CARDTYPE", new WebServiceIMDAO(imSession).getCardTypeByImsi(stkSim.getImsi()));
            }

            Map attributeParam = PMAPI.getAttributeParam(sub.getOfferId(), pmSession);
            if (attributeParam != null && !attributeParam.isEmpty()) {
                lstParam.putAll(attributeParam);
            }

            /* price_plan infor */
            Map pmParam = PMAPI.getProductOfferProvisioningParam(sub.getOfferId(), modificationType, PMConstant.PARAM_TYPE_1, pmSession);
            if (pmParam != null && !pmParam.isEmpty()) {
                lstParam.putAll(pmParam);
            }

            /* vas price_plan */
            Map vasParam = new HashMap();
            List<SubRelProductPre> lstCurrVas = getListCurrVas(sub.getSubId(), cmPreSession);
            if (lstCurrVas != null && lstCurrVas.size() > 0) {
                for (SubRelProductPre subRelProduct : lstCurrVas) {
                    if (subRelProduct.getRelProductCode() != null && !"".equals(subRelProduct.getRelProductCode().trim())
                            && subRelProduct.getIsConnected().equals(0L)) {
                        Map vasPPMap = PMAPI.getVasProvisioningParam(sub.getProductCode(), subRelProduct.getRelProductCode().trim(),
                                modificationType, PMConstant.PARAM_TYPE_1, pmSession);

                        Map vasATMap = PMAPI.getVasAttributeParam(sub.getProductCode(), subRelProduct.getRelProductCode().trim(), pmSession);
                        if (vasATMap != null && !vasATMap.isEmpty()) {
                            vasPPMap.putAll(vasATMap);
                        }
                        if (vasPPMap != null && !vasPPMap.isEmpty()) {
                            Iterator paramKey = vasPPMap.keySet().iterator();
                            while (paramKey.hasNext()) {
                                Object paramName = paramKey.next();
                                Object paramValue = vasPPMap.get(paramName);
                                if (vasParam.containsKey(paramName)) {
                                    vasParam.put(paramName, vasParam.get(paramName) + "_" + paramValue);
                                } else {
                                    vasParam.put(paramName, paramValue);
                                }
                            }
                        }
                    }
                }
            }

            // remove cac tham so cung ten voi goi cuoc chinh
            if (!vasParam.isEmpty()) {
                Object[] keys = vasParam.keySet().toArray();
                for (int i = keys.length - 1; i >= 0; i--) {
                    Object key = keys[i];
                    if (lstParam.containsKey(key)) {
                        vasParam.remove(key);
                    }
                }
            }

            if (vasParam.size() > 0) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String effectDate = simpleDateFormat.format(new Date());

                lstParam.put("EFFECT_DATE", effectDate);
                lstParam.putAll(vasParam);
            }

            // if recover or repair notconnected pre subscriber
            if (isRecover) {
                if (!Constants.SUB_ACT_STATUS_IDLE.equals(sub.getActStatus())) {
                    lstParam.put("START_MONEY", 0L);
                    lstParam.put("ACTIVE", Constants.SUB_PRO_STATUS_ACTIVE);
                } else {
                    lstParam.put("ACTIVE", Constants.SUB_PRO_STATUS_IDLE);
                }
            }

            return lstParam;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static List<SubRelProductPre> getListCurrVas(Long subId, Session session) throws Exception {
        try {
            String sql = " from SubRelProductPre where subId = ? and status = ? and endDatetime is null";

            Query query = session.createQuery(sql);
            query.setParameter(0, subId);
            query.setParameter(1, Constants.SUB_REL_PRODUCT_STATUS_USE);

            List<SubRelProductPre> lstSubRelProduct = query.list();

            return lstSubRelProduct;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void insertRecoveryRequest(Session cmPreSession, String isdn, int status,
            String custName, String serial, String imsi, Long reasonId, String idNo, Long subId,
            Long custId, Integer recType, Long shopId, String shopCode, String staffCode) throws Exception {
        String sql = "insert into cm_pre2.recovery_subscriber_request"
                + "(request_id,status,isdn,cust_name,serial,imsi,reason_id,id_no,sub_id,"
                + "cust_id,rec_type,shop_id,shop_code,create_user,create_date) "
                + " values(cm_pre2.recovery_subscriber_req_seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,sysdate)";
        PreparedStatement query = cmPreSession.connection().prepareStatement(sql);
        query.setObject(1, status);
        query.setObject(2, isdn);
        query.setObject(3, custName);
        query.setObject(4, serial);
        query.setObject(5, imsi);
        query.setObject(6, reasonId);
        query.setObject(7, idNo);
        query.setObject(8, subId);
        query.setObject(9, custId);
        query.setObject(10, recType);
        query.setObject(11, shopId);
        query.setObject(12, shopCode);
        query.setObject(13, staffCode);
        query.executeQuery();
    }
}
