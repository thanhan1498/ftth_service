/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.dao.pre;

import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.bccs.cm.model.pre.CustomerPre;
import com.viettel.bccs.cm.util.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class CustomerPreDAO extends BaseDAO {

    public CustomerPre findByCustId(Session cmPre, Long custId) {

        String queryString = "from CustomerPre  where custId = ? ";
        Query queryObject = cmPre.createQuery(queryString);
        queryObject.setParameter(0, custId);
        List<CustomerPre> list = queryObject.list();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public String checkProductCount(Session cmPreSession, String serviceType, String idNo, String productCode, int increment) {
        try {
            if (idNo == null || idNo.isEmpty()) {
                return "";
            }
            Map<String, Long> lstProductCheckCount = CommonCacheDB.getProductCheckCount(cmPreSession);
            //check productCode co thuoc goi cuoc dac biet khong neu la goi cuoc dac biet thi kiem tra co vuot qua dinh muc khong
            if (lstProductCheckCount.containsKey(productCode)) {
                long quota = lstProductCheckCount.get(productCode);
                String sql = "";
                if ("M".equals(serviceType)) {
                    sql = " From CustomerPre cus, SubMbPre sub "
                            + " Where cus.custId = sub.custId And sub.status = ? and lower(cus.idNo) = ? AND sub.productCode = ?";
                } else if ("H".equals(serviceType)) {
                    sql = "From CustomerPre cus, SubHpPre sub "
                            + " Where cus.custId = sub.custId And sub.status = ? and lower(cus.idNo) = ? AND sub.productCode = ?";
                }
                if (!sql.isEmpty()) {
                    List lstTemp = new ArrayList();
                    lstTemp.add(Constants.SUB_STATUS_NORMAL_ACTIVED);
                    lstTemp.add(idNo.toLowerCase());
                    lstTemp.add(productCode);
                    Query query = cmPreSession.createQuery(sql);
                    if (lstTemp != null && !lstTemp.isEmpty()) {
                        for (int i = 0; i < lstTemp.size(); i++) {
                            query.setParameter(i, lstTemp.get(i));
                        }
                    }
                    List lst = query.list();
                    if (lst.size() + increment > quota) {
                        List lstParam = new ArrayList();
                        lstParam.add(idNo);
                        lstParam.add(quota);
                        lstParam.add(productCode);
                        String result = "Identity {0} registered  overdue quota {1} subscriber {2}.";
                        return new CommonPre().replaceTextResponse(result, lstParam);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

}
