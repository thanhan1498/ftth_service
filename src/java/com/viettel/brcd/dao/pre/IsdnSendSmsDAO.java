package com.viettel.brcd.dao.pre;

import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.pre.ApParamPre;
import com.viettel.bccs.cm.model.pre.IsdnSendSms;
import com.viettel.bccs.cm.util.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;

/**
 * @author vanghv1
 */
public class IsdnSendSmsDAO extends BaseDAO {

    public static final Long STAFF_CHANGE_TYPE = 14L;

    public List<String> genarateSMSContent(Session cmPreSession, String content, String paramType, String paramCode, List<String> params) {
        List<ApParamPre> listSms = null;
        List<String> listContent = new ArrayList<String>();
        if (content == null || content.trim().isEmpty()) {
            listSms = new ApParamDAO().findByTypeCode(cmPreSession, paramType, paramCode);
        }
        if (content != null && !content.trim().isEmpty()) {
            content = content.trim();
            if (!content.isEmpty()) {
                if (params != null && !params.isEmpty()) {
                    for (int i = 0; i < params.size(); i++) {
                        content = content.replace("@_" + i, StringValueOf(params.get(i)));
                    }
                }
                listContent.add(content);
            }
        } else if (listSms != null && !listSms.isEmpty()) {
            for (ApParamPre param : listSms) {
                content = param.getParamValue();
                if (params != null && !params.isEmpty()) {
                    for (int i = 0; i < params.size(); i++) {
                        content = content.replace("@_" + i, StringValueOf(params.get(i)));
                    }
                }
                listContent.add(content);
            }
        }
        return listContent;
    }

    public void insertAssignTaskSms(Session cmPreSession, Session cmPosSession, String content, List<Staff> staffs, Date nowDate, String shopCode, String staffCode) {
        if (content != null) {
            content = content.trim();
            if (!content.isEmpty()) {
                //List<Staff> staffs = new StaffDAO().findByShopAndChannel(cmPosSession, teamId, STAFF_CHANGE_TYPE);
                if (staffs != null && !staffs.isEmpty()) {
                    for (Staff staff : staffs) {
                        insert(cmPreSession, staff.getTel(), content, nowDate, staffCode, shopCode, Constants.APP_CODE_ASSIGN_TASK);
                    }
                }
            }
        }
    }

    public void insert(Session cmPreSession, String isdn, String content, Date nowDate, String staffCode, String shopCode, String appCode) {
        if (isdn != null) {
            isdn = isdn.trim();
            if (!isdn.isEmpty()) {
                IsdnSendSms isdnSendSms = new IsdnSendSms();
                Long id = getSequence(cmPreSession, Constants.SEQ_ISDN_SEND_SMS);
                isdnSendSms.setId(id);
                isdnSendSms.setIsdn(isdn);
                isdnSendSms.setSmsContent(content);
                isdnSendSms.setInsertDate(nowDate);
                isdnSendSms.setUserName(staffCode);
                isdnSendSms.setShopCode(shopCode);
                isdnSendSms.setMaxSendTime(Constants.SMS_MAX_SEND_TIME);
                isdnSendSms.setProcessStatus(Constants.SMS_PROCESS_STATUS_NEW);
                isdnSendSms.setAppCode(appCode);

                cmPreSession.save(isdnSendSms);
                cmPreSession.flush();
            }
        }
    }

    public void insertSMSWhenAssignTaskForTeam(Session sessionCmPre, Session sessionCmPos, String content, Long teamOfStaffManageConnector, Date nowDate, String loginName, String shopCode) {
        List<Staff> lstStaff = new StaffDAO().findByShop(sessionCmPos, teamOfStaffManageConnector);
        if (lstStaff != null && !lstStaff.isEmpty()) {
            for (Staff staff : lstStaff) {
                if (STAFF_CHANGE_TYPE.equals(staff.getChannelTypeId())) {
                    this.insert(sessionCmPre, staff.getTel(), content, nowDate, loginName, shopCode, "ASSIGN_TASK");
                }
            }
        }
    }

    public void insertSMSWhenSignContractSuccessful(Session cmPreSession, String isdn, String content, Date nowDate, String loginName, String shopCode) {
        this.insert(cmPreSession, isdn, content, nowDate, loginName, shopCode, "SIGN_CONTRACT_SUCCESSFUL");
    }
    
    public Long insertIsdnSendSms(Session cmPreSession, String isdn, String content, Date nowDate, String staffCode, String shopCode, String appCode) {
        if (isdn != null) {
            IsdnSendSms isdnSendSms = new IsdnSendSms();
            Long id = getSequence(cmPreSession, Constants.SEQ_ISDN_SEND_SMS);
            isdnSendSms.setId(id);
            isdnSendSms.setIsdn(isdn);
            isdnSendSms.setSmsContent(content);
            isdnSendSms.setInsertDate(nowDate);
            isdnSendSms.setUserName(staffCode);
            isdnSendSms.setShopCode(shopCode);
            isdnSendSms.setMaxSendTime(Constants.SMS_MAX_SEND_TIME);
            isdnSendSms.setProcessStatus(Constants.SMS_PROCESS_STATUS_NEW);
            isdnSendSms.setAppCode(appCode);

            cmPreSession.save(isdnSendSms);
            return id;
        }
        return null;
    }
}
