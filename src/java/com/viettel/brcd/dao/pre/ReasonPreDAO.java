/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.dao.pre;

import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.bccs.cm.model.pre.ReasonPre;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class ReasonPreDAO extends BaseDAO {

    public ReasonPre findById(Session session, Long reasonId) {
        String sql = " from ReasonPre where reasonId = ? and status = 1";
        Query query = session.createQuery(sql).setParameter(0, reasonId);
        List<ReasonPre> list = query.list();
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
}
