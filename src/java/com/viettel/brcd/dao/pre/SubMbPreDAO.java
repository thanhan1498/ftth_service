/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.dao.pre;

import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.pre.SubMbPre;
import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class SubMbPreDAO extends BaseDAO {

    public SubMbPre findByIsdn(Session cmPre, String isdn) {

        String queryString = "from SubMbPre as sub where sub.isdn = ? "
                + " and sub.status = ? ";
        Query queryObject = cmPre.createQuery(queryString);
        queryObject.setString(0, isdn);
        queryObject.setLong(1, 2);
        List<SubMbPre> list = queryObject.list();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public static String getCurrentServices(Session cmPre, SubMbPre sub) throws Exception {
        StringBuffer currentServices = new StringBuffer();
        try {
            if (sub != null) {
                String sql = "select REL_PRODUCT_CODE "
                        + "  from SUB_REL_PRODUCT "
                        + " where status = 1 "
                        + "   and END_DATETIME is null "
                        + "   and sub_id = ?";

                Query query = cmPre.createQuery(sql);
                query.setParameter(0, sub.getSubId());
                List lstRelProductCode = query.list();
                if (lstRelProductCode != null && lstRelProductCode.size() > 0) {
                    for (int i = 0; i < lstRelProductCode.size(); i++) {
                        if (lstRelProductCode.get(i) != null && !"".equals(lstRelProductCode.get(i).toString().trim())) {
                            if (i == lstRelProductCode.size() - 1) {
                                currentServices.append(lstRelProductCode.get(i).toString().trim());
                            } else {
                                currentServices.append(lstRelProductCode.get(i).toString().trim()).append("-");
                            }
                        }
                    }
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return currentServices.toString();
    }
    
    /**
     * @author duyetdk
     * @param imSession
     * @param cmPosSession
     * @param isdn
     * @param userNameLogin
     * @return 
     */
    public boolean getGroupNameMobile(Session imSession,Session cmPosSession,String isdn, String userNameLogin) {
       ApParamDAO apParam = new ApParamDAO();
       String stringQuery = "select l.name"
               + " from stock_isdn_mobile a,"
               + " stock_model c,"
               + " isdn_filter_rules i,"
               + " filter_type l"
               + " where a.stock_model_id = c.stock_model_id"
               + " and a.rules_id = i.rules_id"
               + " and i.filter_type_id = l.filter_type_id"
               + " and a.isdn = ?";

       Query query = imSession.createSQLQuery(stringQuery);
       query.setParameter(0, isdn);

       List lstGroupName = query.list();
       String stringGroupName = null;
       if (lstGroupName != null && !lstGroupName.isEmpty()) {
           stringGroupName = lstGroupName.get(0) != null ? lstGroupName.get(0).toString() : null;
       }
       if (stringGroupName != null) {
           List<ApParam> lst = apParam.getApParamByType(cmPosSession,Constants.PARAM_TYPE_ACCOUNT_NORMAL_CHANGESIM_MOBILE);
           if (lst == null || lst.isEmpty() || !lst.get(0).getParamValue().toUpperCase().contains(userNameLogin.toUpperCase())) {
               lst = apParam.getApParamByType(cmPosSession,Constants.PARAM_TYPE_GROUP_HIDE_CHANGESIM_MOBILE);
               for (ApParam item : lst) {
                   if (item.getParamValue().contains(stringGroupName)) {
                       return true;
                   }
               }
           }
       }
       return false;
   }
}
