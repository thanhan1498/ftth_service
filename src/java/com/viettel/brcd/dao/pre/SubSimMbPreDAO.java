/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.dao.pre;

import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.bccs.cm.model.pre.SubSimMbPre;
import com.viettel.bccs.cm.util.Constants;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author cuongdm
 */
public class SubSimMbPreDAO extends BaseDAO {

    public static void changeSubSimMb(Long subId, String oldImsi, String newImsi, String newSerial, Date date, Long actionAuditId, Session cmPre) throws Exception {
        SubSimMbPre oldSubSimMb = null;
        SubSimMbPre newSubSimMb = null;
        //<editor-fold desc="Xóa bản ghi với Imsi cũ">
        String sql = " FROM SubSimMbPre WHERE status = 1 AND subId = ? AND imsi = ? ";
        Query query = cmPre.createQuery(sql);
        query.setParameter(0, subId);
        query.setParameter(1, oldImsi);
        List<SubSimMbPre> subList = query.list();
        if (subList != null && subList.size() == 1) {
            oldSubSimMb = subList.get(0);
            oldSubSimMb.setEndDatetime(date);
            oldSubSimMb.setStatus(Constants.STATUS_NOT_USE);
            cmPre.update(oldSubSimMb);

            ActionLog.logAuditDetail(cmPre, oldSubSimMb.getId(), actionAuditId, "SUB_SIM_MB", "STATUS", Constants.STATUS_USE, Constants.STATUS_NOT_USE, date);
        }
        //</editor-fold>

        //<editor-fold desc="Tạo bản ghi mới với Imsi mới">
        newSubSimMb = new SubSimMbPre();
        newSubSimMb.setSubId(subId);
        newSubSimMb.setImsi(newImsi);
        newSubSimMb.setSerial(newSerial);
        newSubSimMb.setStaDatetime(date);
        newSubSimMb.setEndDatetime(null);
        newSubSimMb.setStatus(Constants.STATUS_USE);
        cmPre.save(newSubSimMb);

        ActionLog.logAuditDetail(cmPre, newSubSimMb.getId(), actionAuditId, "SUB_SIM_MB", "ID", null, newSubSimMb.getId(), date);
        //</editor-fold>
    }
}
