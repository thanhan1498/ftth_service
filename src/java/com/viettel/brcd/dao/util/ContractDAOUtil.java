/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.dao.util;

import com.viettel.bccs.cm.util.Constants;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author linhlh2
 */
public class ContractDAOUtil {

    public static boolean validatePayAreaCode(Session sessionCmPos, String payAreaCode) {
        if (payAreaCode != null) {
            String sql = " from Area where areaCode = ?  AND status = 1 ";

            Query query = sessionCmPos.createQuery(sql);
            query.setParameter(0, payAreaCode.trim().toUpperCase());

            List lst = query.list();

            if (lst != null && lst.size() > 0) {
                return true;
            }
        }
        return false;
    }
}
