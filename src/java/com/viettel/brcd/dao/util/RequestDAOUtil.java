/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.dao.util;

import com.viettel.bccs.api.Task.DAO.ApParamDAO;
import com.viettel.bccs.api.common.Common;
import com.viettel.bccs.api.common.CommonLog;
import com.viettel.brcd.common.util.DateTimeUtils;
import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.bccs.cm.bussiness.CustRequestBussiness;
import com.viettel.bccs.cm.common.util.Constant;
import com.viettel.bccs.cm.dao.MappingDAO;
import com.viettel.bccs.cm.dao.ReasonDAO;
import com.viettel.bccs.cm.dao.StaffDAO;
import com.viettel.bccs.cm.dao.SubAdslLeaselineDAO;
import com.viettel.bccs.cm.dao.TechnicalConnectorDAO;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.ChangeService;
import com.viettel.bccs.cm.model.CustRequest;
import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.OfferRequest;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.ReqSubscriber;
import com.viettel.bccs.cm.model.SaleTrans;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.SubBundleTv;
import com.viettel.bccs.cm.model.SubReqAdslLl;
import com.viettel.bccs.cm.model.SubReqPstn;
import com.viettel.bccs.cm.model.SubStockModelRelReq;
import com.viettel.bccs.cm.model.TechnicalConnector;
import com.viettel.bccs.cm.supplier.BaseSupplier;
import com.viettel.brcd.util.CalendarUtil;
import com.viettel.brcd.util.EditorUtils;
import com.viettel.brcd.util.GetterUtil;
import com.viettel.brcd.util.LabelUtil;
import com.viettel.brcd.util.StringPool;
import com.viettel.brcd.util.Validator;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.common.util.ResourceBundleUtils;
import com.viettel.brcd.util.key.Values;
import com.viettel.brcd.ws.bccsgw.model.Input;
import com.viettel.brcd.ws.bccsgw.model.Param;
import com.viettel.brcd.ws.common.WebServiceClient;
import com.viettel.brcd.ws.model.input.ContractIn;
import com.viettel.brcd.ws.model.input.Coordinate;
import com.viettel.brcd.ws.model.output.UpdateResultOut;
import com.viettel.brcd.ws.model.output.WSRespone;
import com.viettel.brcd.ws.supplier.nims.getInfoInfras.GetSnFromSplReponse;
import com.viettel.brcd.ws.supplier.nims.lockinfras.LockAndUnlockInfrasResponse;
import com.viettel.brcd.ws.supplier.nims.lockinfras.NimsWsLockUnlockInfrasBusiness;
import com.viettel.brcd.ws.supplier.nims.lockinfras.ResultForm;
import com.viettel.im.database.BO.APSaleModelBean;
import com.viettel.im.database.BO.APStockModelBean;
import com.viettel.im.database.DAO.WebServiceAPDAO;
import com.viettel.im.database.DAO.WebServiceAPDAO.APResult;
import static com.viettel.im.database.DAO.WebServiceAPDAO.convertErrCodeToString;
import com.viettel.payment.API.PaymentDebit;
import com.viettel.pm.database.BO.Product;
import com.viettel.pm.database.DAO.PMAPI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author linhlh2
 */
public class RequestDAOUtil {
    
    private static final Logger _log = Logger.getLogger(RequestDAOUtil.class);
    private static Map<String, String> userManualMap = new HashMap<String, String>();
    
    private SaleTrans getSaleTransForCheckAP(Long staffId, Long shopId, Date nowDate) throws Exception {
        
        SaleTrans saleTrans = new SaleTrans();
        saleTrans.setShopId(shopId);
        saleTrans.setStaffId(staffId);
        saleTrans.setSaleTransDate(nowDate);
        saleTrans.setStatus(1L);
        return saleTrans;
    }
    
    public List<ApParam> findParamByTypeAndCode(Session session, String paramType, String paramCode) {
        StringBuilder sb = new StringBuilder();
        sb.append(" From ApParam Where status = :status and paramCode = :paramCode and paramType = :paramType ");
        Query sql = session.createQuery(sb.toString());
        sql.setParameter("status", Constants.STATUS_USE);
        sql.setParameter("paramType", paramType);
        sql.setParameter("paramCode", paramCode);
        
        List<ApParam> params = sql.list();
        return params;
    }
    
    public List<String> findParamValues(Session session, String paramType, String paramCode) {
        List<ApParam> params = findParamByTypeAndCode(session, paramType, paramCode);
        if (params != null && !params.isEmpty()) {
            List<String> result = new ArrayList<String>();
            for (ApParam param : params) {
                if (param != null) {
                    String value = param.getParamValue();
                    if (value != null) {
                        value = value.trim();
                        String[] tmp = value.split(",");
                        if (tmp != null && tmp.length > 0) {
                            result.addAll(Arrays.asList(tmp));
                        }
                    }
                }
            }
            return result;
        }
        return null;
    }
    
    public String checkSaleTransAP(Session cmSession, Session imSession,
            Long staffId, Long shopId, String saleServiceCode,
            String locale, Long teamId, String regType, String province, Long[] arrStockTypeId, Long serviceId, Date nowDate) throws Exception {
        String message = "";
        SaleTrans saleTrans = null;
        //List lay ra mat hang di kem
        List<APStockModelBean> lstApStockModel = new ArrayList<APStockModelBean>();
        //List dung cho ADSL
        com.viettel.im.database.BO.APSaleTrans apSaleTransIm = null;

        //khoi tao danh sach tai nguyen - add tai nguyen vao BO roi add vao list nay de gui sang IM
        WebServiceAPDAO webIMDAO = new WebServiceAPDAO(imSession);
        
        if (saleServiceCode != null && !"".equals(saleServiceCode)) {
            saleTrans = getSaleTransForCheckAP(staffId, shopId, nowDate);
            apSaleTransIm = new com.viettel.im.database.BO.APSaleTrans();
            BeanUtils.copyProperties(apSaleTransIm, saleTrans);
            
            apSaleTransIm.setSaleServiceCode(saleServiceCode);
            //Set shopId,staffId
            //Lay thong tin cam ket, check thong tin cam ket
            List<String> lstSpecialRegType = findParamValues(cmSession, Constants.APPARAM_SPECIAL_REASON_TYPE, Constants.APPARAM_SPECIAL_REASON_CODE);
            boolean isCheckDLU = true;
            if (lstSpecialRegType != null && regType != null
                    && lstSpecialRegType.contains(regType.trim())) {
                isCheckDLU = false;
                apSaleTransIm.setProvinceCode(province);
            }
            apSaleTransIm.setIsCheckDLU(isCheckDLU);

            //Add mat hang vao list de check
            if (teamId != null && teamId > 0L) {
                if (arrStockTypeId != null && arrStockTypeId.length > 0) {
                    for (int i = 0; i < arrStockTypeId.length; i++) {
                        APStockModelBean beanModel = new APStockModelBean();
                        beanModel.setStockModelId(arrStockTypeId[i]);
                        if (Constants.SERVICE_ADSL_ID_WEBSERVICE.equals(serviceId)
                                || Constants.SERVICE_LEASELINE_ID_WEBSERVICE.equals(serviceId)
                                || Constants.SERVICE_FTTH_ID_WEBSERVICE.equals(serviceId)) {
                            
                            beanModel.setPosId(teamId);
                        }
                        
                        beanModel.setQuantity(1L);
                        lstApStockModel.add(beanModel);
                    }
                }
            }
            apSaleTransIm.setLstAPModel(lstApStockModel);
            StringBuilder paramIn = new StringBuilder()
                    .append("SaleTrans = [").append(apSaleTransIm).append("]")
                    .append(",checkSaleTransAP = [").append(saleServiceCode).append("]");
            new BaseDAO().info(_log, " START checkSaleTransAP " + paramIn.toString());
            
            APResult resultIm = null;
            try {
                resultIm = webIMDAO.checkAPSaleTrans(apSaleTransIm);
                if (resultIm != null && APResult.ERR_SERIAL_NOT_IN_STOCK.equals(resultIm)) {
                    return LabelUtil.getKey("com.request.saleTrans", locale);
                }
                new BaseDAO().info(_log, ">>>>>>>>>>>>>>>>>>>>>>>>>>> locale:" + locale);
                new BaseDAO().info(_log, ">>>>>>>>>>>>>>>>>>>>>>>>>>> resultIm:" + resultIm);
                new BaseDAO().info(_log, ">>>>>>>>>>>>>>>>>>>>>>>>>>> new Locale(locale):" + new Locale(locale));
                message = convertErrCodeToString(resultIm, new Locale(locale));
            } catch (Exception ex) {
                message = LabelUtil.getKey("com.request.errorSaleTrans", locale);
                new BaseDAO().info(_log, ex.getMessage(), ex);
            }
            
            StringBuilder paramOut = new StringBuilder()
                    .append("Result = [").append(resultIm).append("]")
                    .append(",Message = [").append(message).append("]");
            new BaseDAO().info(_log, " FINISH " + paramOut.toString());
            
            return message;
        }
        return "";
    }
    
    public UpdateResultOut addRequestContract(Session cmPosSession, Session imSession, Session pmSession, Session paymentSession, Session nimsSession, ContractIn input, String locale) {
        new BaseDAO().info(_log, "RequestDAOUtil.addRequestContract:input=" + new BaseDAO().toJson(input));
        
        UpdateResultOut result = new UpdateResultOut();
        boolean hasErr = false;
        
        try {
            Long productId = input.getSubscriber().getProductId();
            Long serviceId = input.getSubscriber().getServiceId();
            CustRequest custRequest = new CustRequest();
            // so thue bao trong mot yeu cau
            Long numOfSubscribers = 1L;
            StringBuilder serviceTypes = new StringBuilder();
            serviceTypes.append(new BaseDAO().getAliasByWebServiceId(serviceId));
            Date currDateTime = new BaseDAO().getSysDateTime(cmPosSession);
            BaseSupplier supplier = new BaseSupplier();
            Long custReqId = supplier.getSequence(cmPosSession, Constants.CUST_REQUEST_ID_SEQ);
            Long custId = input.getCustomerIn().getCustId();
            Long shopId = input.getShopId();
            Long staffId = input.getStaffId();
            Customer customer = null;
            Staff staff = null;
            Shop shop = null;
            System.out.println(custId);
            
            if (Validator.isNotNull(custId)) {
                customer = getCustomer(cmPosSession, custId);
            } else {
                hasErr = true;
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription(LabelUtil.getKey("custId.is.required", locale));
                result.setResult(false);
                return result;
            }
            
            if (Validator.isNull(customer)) {
                hasErr = true;
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription(LabelUtil.getKey("customer.does.not.exist", locale));
                result.setResult(false);
                return result;
            }
            
            System.out.println(staffId);
            
            if (Validator.isNotNull(staffId)) {
                staff = getStaff(cmPosSession, staffId);
            } else {
                hasErr = true;
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription(LabelUtil.getKey("staffId.is.required", locale));
                result.setResult(false);
                return result;
            }
            
            if (Validator.isNull(staff)) {
                hasErr = true;
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription(LabelUtil.getKey("staff.does.not.exist", locale));
                result.setResult(false);
                return result;
            }

            //<editor-fold defaultstate="collapsed" desc="update customer">
            customer.setCorrectCus(Constants.CORRECT_CUSTOMER); // set correct customer information
            customer.setUpdatedTime(currDateTime);
            customer.setUpdatedUser(staff.getStaffCode());
            cmPosSession.update(customer);
//            cmPosSession.flush();
            //</editor-fold>

            System.out.println(shopId);
            
            if (Validator.isNotNull(shopId)) {
                shop = (Shop) getShop(cmPosSession, shopId);
            } else {
                hasErr = true;
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription(LabelUtil.getKey("shopId.is.required", locale));
                result.setResult(false);
                return result;
            }
            
            if (Validator.isNull(shop)) {
                hasErr = true;
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription(LabelUtil.getKey("shop.does.not.exist", locale));
                result.setResult(false);
                return result;
            }
            //Check no cuoc
            //comment for test
            String debit = getSubDebitByCust(paymentSession, custId);
            if (Validator.isNotNull(debit)) {
                hasErr = true;
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription(LabelUtil.formatKey("customer.is.having.dept.charges", locale, debit));
                result.setResult(false);
                return result;
            }

            //Check bat buoc dat coc
            String sub_Type = input.getSubscriber().getSubType();
            String check_deposit = getCheckDeposit(cmPosSession, sub_Type);
            String productCode = input.getSubscriber().getProductCode();
            if ("1".equals(check_deposit)) {
                //Check dat coc toi thieu
                Double deposit = GetterUtil.getDouble(input.getSubscriber().getDeposit());
                
                if (Validator.isNotNull(deposit)
                        && Validator.isNotNull(productCode)) {
                    Double minDeposit = getMinDepositByProductCode(serviceId, productCode, cmPosSession, pmSession);
                    
                    if (minDeposit > deposit) {
                        hasErr = true;
                        result.setErrorCode(Constants.ERROR_CODE_1);
                        result.setErrorDecription(LabelUtil.formatKey("deposit.is.not.enough.for.product", locale, productCode, minDeposit));
                        result.setResult(false);
                        return result;
                    }
                }
            }

            //sinh account tu dong
            String account = genAccountAuto(cmPosSession, serviceTypes.toString(), customer.getProvince());

            /*Validate old account theo li do*/
            String regType = input.getSubscriber().getReasonRoam();
            if (regType != null) {
                ApParamDAO apParamDAO = new ApParamDAO();
                List<ApParam> listApParam = findParamByTypeAndCode(cmPosSession, Constants.AP_PARAM_PARAM_TYPE_SMS, Constants.CHANGE_ADSL_TO_GPON_REASON_CODE);
                boolean flagAtoGPON = false;
                if (listApParam != null && !listApParam.isEmpty()) {
                    for (ApParam value : listApParam) {
                        if (regType.contains(value.getParamValue())) {
                            flagAtoGPON = true;
                            break;
                        }
                    }
                }
                com.viettel.bccs.api.Task.BO.ApParam param = apParamDAO.getApParamByTypeCodeAndValue(cmPosSession, "REASON_CHANGE_ADSL_FTTH", regType);
                if (param != null || flagAtoGPON /*chinh sach doi ADSL -> GPON*/) {
                    if (input.getSubscriber().getOldAccount() == null || "".equals(input.getSubscriber().getOldAccount())) {
                        hasErr = true;
                        result.setErrorCode(Constants.ERROR_CODE_1);
                        result.setErrorDecription(LabelUtil.getKey("change.service.old.input", locale));
                        result.setResult(false);
                        return result;
                    } else {
                        SubAdslLeaseline lstOldAdsl = new SubAdslLeaselineDAO().findByAccount(cmPosSession, input.getSubscriber().getOldAccount().trim());
                        /*Change A to FTTH, FTTH to GPON*/
                        if (lstOldAdsl == null || param == null
                                || (!Constants.SERVICE_ALIAS_ADSL.equals(lstOldAdsl.getServiceType()) && "REASON_CHANGE_ADSL_FTTH".equals(param.getParamCode()))
                                || (!Constants.SERVICE_ALIAS_FTTH.equals(lstOldAdsl.getServiceType()) && "REASON_CHANGE_FTTH_GPON".equals(param.getParamCode()))
                                || Constants.SUB_STATUS_CANCEL == lstOldAdsl.getStatus()) {
                            
                            hasErr = true;
                            result.setErrorCode(Constants.ERROR_CODE_1);
                            result.setErrorDecription(LabelUtil.getKey("change.service.old.invalid", locale));
                            result.setResult(false);
                            return result;
                        }
                        /*Change A to GPON*/
                        if (flagAtoGPON) {
                            listApParam = findParamByTypeAndCode(cmPosSession, Constants.CHANGE_ADSL_TO_GPON_AP_PARAM_TYPE, productCode);
                            if (listApParam == null || listApParam.isEmpty()) {
                                hasErr = true;
                                result.setErrorCode(Constants.ERROR_CODE_1);
                                result.setErrorDecription(LabelUtil.getKey("change.service.new.invalid", locale));
                                result.setResult(false);
                                return result;
                            } else {
                                if (!listApParam.get(0).getParamValue().contains(lstOldAdsl.getProductCode())) {
                                    //Goi cuoc ADSL cu cua khach hang khong trong chinh sach thay doi ADSL -> GPON
                                    hasErr = true;
                                    result.setErrorCode(Constants.ERROR_CODE_1);
                                    result.setErrorDecription(LabelUtil.getKey("change.service.new.invalid.gpon", locale));
                                    result.setResult(false);
                                    return result;
                                }
                            }
                        }
                        // check account tồn tại trong change_service
                        if (checkExistsInChangerServiceAdsl(cmPosSession, input.getSubscriber().getOldAccount())) {
                            
                            hasErr = true;
                            result.setErrorCode(Constants.ERROR_CODE_1);
                            result.setErrorDecription(LabelUtil.getKey("change.service.old.already", locale));
                            result.setResult(false);
                            return result;
                        }
                    }
                }
            } else {
                hasErr = true;
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription(LabelUtil.getKey("reason.not.mapping.saleService", locale));
                result.setResult(false);
                return result;
            }
            //Check dich vu ban hang

            System.out.println(account);
            
            input.getInfrastruct().setAccount(account);

            // Thay doi do contraint
            custRequest.setCustRequestId(custReqId);
            custRequest.setCustId(custId);
            custRequest.setAddedUser(staff.getStaffCode());
            custRequest.setReqDate(currDateTime);
            custRequest.setStatus(Values.STATUS_ACTIVE);
            custRequest.setShopCode(shop.getShopCode());
            custRequest.setNumOfSubscribers(numOfSubscribers);
            custRequest.setServiceTypes(serviceTypes.toString());
            cmPosSession.save(custRequest);
            
            Long reasonId = getReasonIdByTypeAndCode(cmPosSession, input.getSubscriber().getReasonRoam(), Constants.REASON_TYPE_REG_TYPE_NEW);
            
            System.out.println("reasonId = " + reasonId);
            System.out.println("serviceId = " + serviceId);
            System.out.println("productCode = " + productCode);
            
            String saleCode = new MappingDAO().getSaleServiceCode(cmPosSession,
                    serviceId, reasonId, productCode,
                    Constants.ACTION_SUBSCRIBER_ACTIVE_NEW, null);
            
            System.out.println("saleCode = " + saleCode);
            
            List<APSaleModelBean> stockTypes = new ArrayList<APSaleModelBean>();
            // neu saleCode null hoac rong ==> bao loi Hình thức hòa mạng chưa được mapping với dịch vụ bán hàng

            if (Validator.isNotNull(saleCode)) {
                stockTypes = getStockType(saleCode, imSession);
            } else {
                hasErr = true;
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription(LabelUtil.getKey("com.request.errorMaping", locale));
                result.setResult(false);
                return result;
            }
            
            Long[] arrStockTypeId = new Long[stockTypes.size()];
            
            for (int i = 0; i < stockTypes.size(); i++) {
                List<APStockModelBean> lstStockModel = stockTypes.get(i).getLstStockModel();
                
                if (!lstStockModel.isEmpty()) {
                    arrStockTypeId[i] = lstStockModel.get(0).getStockModelId();
                }
            }

            /*Khai bao hang hoa cho bundle tv*/
            List<APSaleModelBean> stockTypesTv = new ArrayList<APSaleModelBean>();
            Long[] arrStockTypeTvId = null;
            String saleCodeTv = null;
            Long reasonIdTv = null;
            if (input.getSubscriber().getBundleTv() != null) {
                Reason reasonTv = new ReasonDAO().findById(cmPosSession, input.getSubscriber().getBundleTv().getRegReasonId());
                if (reasonTv == null) {
                    hasErr = true;
                    result.setErrorCode(Constants.ERROR_CODE_1);
                    result.setErrorDecription("Can not find reason for reg Bundle tv");
                    result.setResult(false);
                    return result;
                }
                reasonIdTv = reasonTv.getReasonId();
                saleCodeTv = new MappingDAO().getSaleServiceCode(cmPosSession,
                        serviceId, reasonTv.getReasonId(), productCode,
                        Constants.CONNECTING_BUNDLE_TV, input.getSubscriber().getBundleTv().getProductCode());
                stockTypesTv = getStockType(saleCodeTv, imSession);
                if (stockTypesTv == null || stockTypesTv.isEmpty()) {
                    hasErr = true;
                    result.setErrorCode(Constants.ERROR_CODE_1);
                    result.setErrorDecription("Can not find sale service for this reason " + reasonTv.getCode());
                    result.setResult(false);
                    return result;
                }
                arrStockTypeTvId = new Long[stockTypesTv.size()];
                for (int i = 0; i < stockTypes.size(); i++) {
                    List<APStockModelBean> lstStockModel = stockTypesTv.get(i).getLstStockModel();
                    if (!lstStockModel.isEmpty()) {
                        arrStockTypeTvId[i] = lstStockModel.get(0).getStockModelId();
                    }
                }
            }
            input.setStockTypeIds(arrStockTypeId);
            // goi ham message = InterfaceCmInventory.checkSaleService ==> neu mesage khac null khac rong => bao loi là mesage
            ReqSubscriber reqSub = new ReqSubscriber();
            Long subId = supplier.getSequence(cmPosSession, Constants.SUB_ID_SEQ);
            String mess = addSingleRequest(cmPosSession, imSession, nimsSession,
                    saleCode, customer, staff, shop, serviceId, arrStockTypeId,
                    custReqId, input, locale, reqSub, reasonId, input.getListCoordinate(), subId, saleCodeTv, arrStockTypeTvId, reasonIdTv);
            
            if (Validator.isNotNull(mess)) {
                hasErr = true;
                result.setErrorCode(Constants.ERROR_CODE_1);
                result.setErrorDecription(mess);
                result.setResult(false);
                return result;
            }
            
            if (input.getSubscriber().getOldAccount() != null
                    && !"".equals(input.getSubscriber().getOldAccount())
                    && !"?".equals(input.getSubscriber().getOldAccount())) {
                Long changeServiceId = Common.getSequence("CHANGE_SERVICE_SEQ", cmPosSession);
                ChangeService changeService = new ChangeService();
                changeService.setAccountAdsl(input.getSubscriber().getOldAccount());
                changeService.setAccountFtth(account.toLowerCase());
                changeService.setCreateDate(new java.sql.Date(new Date().getTime()));
                changeService.setId(changeServiceId);
                changeService.setStatus(Constant.STATUS_USE);
                changeService.setUserCreate(staff.getStaffCode());
                changeService.setDescription("Save account ADSL when connect FTTH");
                cmPosSession.save(changeService);
                // luu log view CC
                Long actionAuditId = Common.getSequence("ACTION_AUDIT_SEQ", cmPosSession);
                Reason reason = new ReasonDAO().findByCode(cmPosSession, regType);
                CommonLog.logActionV2(actionAuditId, subId, Constant.ACTION_AUDIT_PK_TYPE_SUBSCRIBER,
                        reason.getReasonId(), Constant.ACTION_REMARK_CORRECT_CUSTOMER,
                        "Change from ADSL to FTTH account:" + input.getSubscriber().getOldAccount().trim(),
                        staff.getStaffCode(), shop.getShopCode(), new Date(), cmPosSession);
            }

            //vietnn6 - insert toa do ban ve
            if (input.getListCoordinate() != null && input.getListCoordinate().size() > 0) {
                saveLocation(cmPosSession, input.getListCoordinate(), reqSub.getReqId(), 1L);
            }

            //Thuc hien check lock SIM, ISDN
            if (Validator.isNotNull(productId)
                    && "true".equalsIgnoreCase(ResourceBundleUtils.getResource("provisioning.allow.send.request", "provisioning"))) {
                String errorMessage = lockSimIsdn(cmPosSession, imSession, pmSession,
                        input, shopId, staffId, serviceId, locale);
                //Thuc hien set error vao trong request.
                System.out.println(errorMessage);
                
                if (Validator.isNotNull(errorMessage)) {
                    hasErr = true;
                    result.setErrorCode(Constants.ERROR_CODE_1);
                    result.setErrorDecription(errorMessage);
                    result.setResult(false);
                    return result;
                }
            }
            
            new BaseDAO().info(_log, "RequestDAOUtil.addRequestContract:start close CustRequest;CustReqId=" + custRequest.getCustRequestId());
            new CustRequestBussiness().closeRequest(cmPosSession, custRequest.getCustRequestId());
            new BaseDAO().info(_log, "RequestDAOUtil.addRequestContract:end close CustRequest");
            
            new BaseSupplier().commitTransactions(cmPosSession, imSession, pmSession);
            
            result.setErrorCode(Constants.ERROR_CODE_0);
            result.setErrorDecription(LabelUtil.getKey("common.success", locale));
            result.setReqId(reqSub.getReqId());
            result.setAccount(account);
            if (input.getSubscriber().getBundleTv() != null) {
                result.setAccBundleTv(input.getSubscriber().getBundleTv().getAccount());
            }
            result.setResult(true);
        } catch (Exception ex) {
            new BaseDAO().error(_log, ex.getMessage(), ex);
            hasErr = true;
            result.setErrorCode(Constants.ERROR_CODE_1);
            result.setErrorDecription(LabelUtil.formatKey("a.error.has.occurred", locale, ex.getMessage()));
            result.setResult(false);
        } finally {
            new BaseDAO().info(_log, "RequestDAOUtil.addRequestContract:result=" + new BaseDAO().toJson(result));
            if (hasErr) {
                try {
                    new BaseSupplier().rollBackTransactions(cmPosSession, imSession, pmSession, paymentSession);
                } catch (Throwable ex1) {
                    new BaseDAO().error(_log, ex1.getMessage(), ex1);
                }
            }
        }
        return result;
    }
    
    public String lockSimIsdn(Session cmPosSession, Session imSession, Session pmSession,
            ContractIn input, Long shopId, Long staffId, Long serviceId, String locale)
            throws Exception {
        String message = StringPool.BLANK;

        //Thuc hien lock A
        if (serviceId.equals(Constants.SERVICE_ADSL_ID_WEBSERVICE)) {
            Long cableBoxId = input.getInfrastruct().getCableBoxId();
            
            if (Validator.isNotNull(cableBoxId)) {
                /**
                 * Lock port tu NIMS
                 */
                //Dich vu ADSL
                String serviceType = Constants.SERVICE_ALIAS_ADSL;
                
                String productGroup = getProductGroupByProductCodeAndTelService(
                        pmSession, input.getSubscriber().getProductCode(), serviceId);
                
                if (Constants.PRODUCT_GROUP_FTTH.equals(productGroup)) {
                    serviceType = Constants.SERVICE_ALIAS_FTTH;
                }
                
                LockAndUnlockInfrasResponse infrasResponse = NimsWsLockUnlockInfrasBusiness.
                        lockUnlockInfras(NimsWsLockUnlockInfrasBusiness.INFRAS_ACTION_LOCK,
                                input.getInfrastruct().getAccount(),
                                null,
                                input.getInfrastruct().getCableBoxId(),
                                null, serviceType, null, null, null, null);
                ResultForm resultForm = infrasResponse.getReturn();
                
                System.out.println(resultForm.getResult());
                //Tra ve ket qua lock port
                if (resultForm.getResult().equals(NimsWsLockUnlockInfrasBusiness.INFRAS_LOCL_UNLOCK_OK)) {
                    message = StringPool.BLANK;
                    
                    input.setIsSatisfy(1L);
                } else {
                    message = LabelUtil.formatKey("lock.unlock.fail", locale, resultForm.getMessage());
                    
                    input.setIsSatisfy(0L);
                    
                    return message;
                }
                
            } else {
                message = LabelUtil.getKey("errors.add.request.connector", locale);
                
                input.setIsSatisfy(0L);
            }
            
            if (Validator.isNotNull(input.getInfrastruct().getIpStatic())) {
                message = lockIpStatic(imSession, input.getInfrastruct().getIpStatic(),
                        shopId, staffId, locale);
            }
            
            if (!"".equals(message)) {
                return message;
            }
        }
        
        if (serviceId.equals(Constants.SERVICE_LEASELINE_ID_WEBSERVICE)) {
            LockAndUnlockInfrasResponse infrasResponse = new LockAndUnlockInfrasResponse();
            
            infrasResponse = NimsWsLockUnlockInfrasBusiness.lockUnlockInfras(
                    NimsWsLockUnlockInfrasBusiness.INFRAS_ACTION_LOCK,
                    input.getInfrastruct().getAccount(), input.getInfrastruct().getIsdn(),
                    input.getInfrastruct().getCableBoxId(), null,
                    Constants.SERVICE_ALIAS_LEASEDLINE, null, null, null, null);
            
            ResultForm resultForm = infrasResponse.getReturn();
            
            System.out.println(resultForm.getResult());
            //Tra ve ket qua lock port
            if (resultForm.getResult().equals(NimsWsLockUnlockInfrasBusiness.INFRAS_LOCL_UNLOCK_OK)) {
                message = StringPool.BLANK;
                
                input.setIsSatisfy(1L);
            } else {
                message = LabelUtil.getKey("lock.unlock.fail", locale);
                
                input.setIsSatisfy(0L);
                
                return message;
            }
        }
        
        if (serviceId.equals(Constants.SERVICE_FTTH_ID_WEBSERVICE)) {
            LockAndUnlockInfrasResponse infrasResponse = new LockAndUnlockInfrasResponse();
            
            infrasResponse = NimsWsLockUnlockInfrasBusiness.lockUnlockInfras(
                    NimsWsLockUnlockInfrasBusiness.INFRAS_ACTION_LOCK,
                    input.getInfrastruct().getAccount(), input.getInfrastruct().getIsdn(),
                    input.getInfrastruct().getCableBoxId(), null,
                    Constants.SERVICE_ALIAS_FTTH, input.getInfrastruct().getTechnology(), null, input.getInfrastruct().getCableBoxCode(), input.getInfrastruct().getPortCode());
            
            ResultForm resultForm = infrasResponse.getReturn();
            
            System.out.println(resultForm.getResult());
            //Tra ve ket qua lock port
            if (resultForm.getResult().equals(NimsWsLockUnlockInfrasBusiness.INFRAS_LOCL_UNLOCK_OK)) {
                message = StringPool.BLANK;
                
                input.setIsSatisfy(1L);
            } else {
                message = LabelUtil.getKey("lock.unlock.fail", locale);
                
                input.setIsSatisfy(0L);
                
                return message;
            }
        }
        
        return StringPool.BLANK;
    }
    
    public Long getReasonIdByTypeAndCode(Session session, String reasonCode, String reasonType) {
        StringBuilder sb = new StringBuilder()
                .append(" from Reason where type = ? and code = ? ");
        
        Query query = session.createQuery(sb.toString())
                .setParameter(0, reasonType)
                .setParameter(1, reasonCode);
        
        List<Reason> reasons = query.list();
        if (reasons != null && !reasons.isEmpty()) {
            for (Reason reason : reasons) {
                if (reason != null) {
                    return reason.getReasonId();
                }
            }
        }
        
        return null;
    }
    
    public List<APSaleModelBean> getStockType(String saleCode, Session iSession) throws Exception {
        StringBuilder paramIn = new StringBuilder();
        paramIn.append("Method = [getStockType]");
        paramIn.append(",saleCode = [").append(saleCode).append("]");
        new BaseDAO().info(_log, " START " + paramIn.toString());
        
        WebServiceAPDAO aPDAO = new WebServiceAPDAO(iSession);
        List<APSaleModelBean> lstAPSaleModelBean = aPDAO.getListModelForAPSaleTrans(saleCode);
        
        StringBuilder paramOut = new StringBuilder();
        paramOut.append("Method = [getStockType]");
        paramOut.append(",lstAPSaleModelBean = [").append(lstAPSaleModelBean).append("]");
        new BaseDAO().info(_log, " FINISH " + paramOut.toString());
        
        return lstAPSaleModelBean;
    }
    
    public String addSingleRequest(Session cmPosSession, Session imSession, Session nimsSession, String saleCode, Customer customer,
            Staff staff, Shop shop, Long serviceId, Long[] arrStockTypeId,
            Long custReqId, ContractIn input, String locale, ReqSubscriber reqSub, Long reasonId, List<Coordinate> listCoordinate, Long subId,
            String saleCodeTv, Long[] arrStockTypeTvId, Long reasonIdTv) throws Exception {
        String result = StringPool.BLANK;
        
        BaseSupplier supplier = new BaseSupplier();
        Date currentDate = supplier.getSysDateTime(cmPosSession);
        if (subId == null) {
            subId = supplier.getSequence(cmPosSession, Constants.SUB_ID_SEQ);
        }
        Long id = supplier.getSequence(cmPosSession, Constants.SUB_REQ_SEQ);
        Long reqOfferId = supplier.getSequence(cmPosSession, Constants.REQ_OFFER_ID_SEQ);
        
        if (Validator.isNull(input.getInfrastruct().getTeamId())) {
            return LabelUtil.getKey("stationId.is.required", locale);
        }
        
        Long teamId;
        /*check staff manage connector to ghet team of staff, if not get team mange station*/
        List<TechnicalConnector> listTechConnector = new TechnicalConnectorDAO().findStaffManageConnector(cmPosSession, nimsSession, input.getInfrastruct().getCableBoxId(), "4".equals(input.getInfrastruct().getTechnology()) ? Constants.INFRA_GPON : Constants.INFRA_AON);
        if (listTechConnector == null || listTechConnector.isEmpty()) {
            teamId = getTeamIdByStationId(cmPosSession, input.getInfrastruct().getTeamId());
        } else {
            Staff staffManage = new StaffDAO().findById(cmPosSession, listTechConnector.get(0).getStaffId());
            if (staffManage != null) {
                teamId = staffManage.getShopId();
            } else {
                teamId = getTeamIdByStationId(cmPosSession, input.getInfrastruct().getTeamId());
            }
        }
        
        if (Validator.isNull(teamId)) {
            return LabelUtil.getKey("teamId.is.required", locale);
        }
        System.out.println("ServiceId" + serviceId);
        
        String errorSaleTrans = checkSaleTransAP(cmPosSession, imSession,
                staff.getStaffId(), shop.getShopId(), saleCode,
                locale, teamId, input.getSubscriber().getReasonRoam(), input.getCustomerIn().getProvinceCode(), arrStockTypeId, serviceId, currentDate);
        if (!Validator.isNull(errorSaleTrans)) {
            return errorSaleTrans;
        }
        if (!Validator.isNull(errorSaleTrans)) {
            return errorSaleTrans;
        }
        errorSaleTrans = checkSaleTransAP(cmPosSession, imSession,
                staff.getStaffId(), shop.getShopId(), saleCodeTv,
                locale, teamId, input.getSubscriber().getReasonRoam(), input.getCustomerIn().getProvinceCode(), arrStockTypeTvId, serviceId, currentDate);
        if (!Validator.isNull(errorSaleTrans)) {
            return errorSaleTrans;
        }
        if (!Validator.isNull(errorSaleTrans)) {
            return errorSaleTrans;
        }
        String contactName = customer.getContactName();
        if (contactName != null) {
            contactName = contactName.trim();
        }
        if (contactName == null || contactName.isEmpty()) {
            contactName = customer.getName();
        }
        if (contactName != null) {
            contactName = contactName.trim();
        }
        
        if (serviceId.equals(Constants.SERVICE_PSTN_ID_WEBSERVICE)) {
            SubReqPstn subReqPstn = new SubReqPstn();
            
            subReqPstn.setCustReqId(custReqId);
            subReqPstn.setId(id);
            subReqPstn.setIsdn(input.getInfrastruct().getIsdn());
            subReqPstn.setLineType(input.getSubscriber().getLineType());//truyen len
            subReqPstn.setProvince(customer.getProvince());
            subReqPstn.setDistrict(customer.getDistrict());
            subReqPstn.setPrecinct(customer.getPrecinct());
            subReqPstn.setStreetBlock(customer.getStreetBlock());
            subReqPstn.setQuota(GetterUtil.getLong(input.getSubscriber().getLimit()));//truyen len
            subReqPstn.setPromotionCode(input.getSubscriber().getPromotion());//truyen len
            subReqPstn.setDeposit(GetterUtil.getDouble(input.getSubscriber().getDeposit()));//truyen len
//            subReqPstn.setReasonDepositId(adslForm.getReasonDepositId());//truyen len

            subReqPstn.setRegType(input.getSubscriber().getReasonRoam());//truyen len
            subReqPstn.setDeployAddress(customer.getAddress());
            subReqPstn.setStaDatetime(currentDate);
            subReqPstn.setSubType(input.getSubscriber().getSubType());//truyen len
            subReqPstn.setIsSatisfy(input.getIsSatisfy());
            subReqPstn.setStatus(Constants.SUB_REQ_STATUS_NEW);
            
            subReqPstn.setActStatus(Constants.SUB_ACT_STATUS_BLOCK_TWO_WAY_BY_CUST);
            subReqPstn.setAddress(customer.getAddress());
            
            if (Validator.isNull(customer.getAreaCode())) {
                return LabelUtil.getKey("com.request.getareacode", locale);
                
            } else {
                subReqPstn.setDeployAreaCode(customer.getAreaCode());
                subReqPstn.setAddressCode(customer.getAreaCode());
            }
            subReqPstn.setProductCode(input.getSubscriber().getProductCode());//truyen len
            subReqPstn.setShopCode(shop.getShopCode());
            subReqPstn.setStaffId(staff.getStaffId());
            // phund pstn
            subReqPstn.setTeamId(teamId);//truyen len
            subReqPstn.setStreetBlockName(customer.getStreetBlockName());
            subReqPstn.setStreetName(customer.getStreetName());
            subReqPstn.setHome(customer.getHome());
            
            subReqPstn.setCreateDate(currentDate);
            new BaseDAO().info(_log, "subReqPstn.getCreateDate()=" + DateTimeUtils.formatddMMyyyyHHmmss(subReqPstn.getCreateDate()));
            subReqPstn.setUserCreated(staff.getStaffCode());
            subReqPstn.setVip("0");//Defalt is not vip
            subReqPstn.setRegReasonId(reasonId);//TODO

//            subReqPstn.setResourceMap(adslForm.getResourceMap());//truyen null
//            subReqPstn.setDluId(adslForm.getDslamId());// nha tram//truyen len
            subReqPstn.setUserUsing(contactName);//ten khach hang
            subReqPstn.setUserTitle(customer.getContactTitle());//loai khach hang
            subReqPstn.setLimitDate(CalendarUtil.getDate(currentDate, getLimitDate(cmPosSession, Constants.AP_PARAM_LIMIT_DATE, Constants.AP_PARAM_LIMIT_DATE_PSTN)));//sysdate + 7

            subReqPstn.setSubId(subId);
            subReqPstn.setIsNewSub(Constants.IS_NEW_SUB_TRUE);
            subReqPstn.setReqOfferId(reqOfferId);
            
            if (subReqPstn.getDeposit() == null || subReqPstn.getDeposit().equals(0L)) {
                subReqPstn.setDeposit(Constants.DEPOSIT_DEFAULT);
            }
            
            if (subReqPstn.getQuota() == null || subReqPstn.getQuota().equals(0L)) {
                subReqPstn.setQuota(Constants.SUB_QUOTA_DEFAULT);
            }

            //Quant add 08/03/2011
            subReqPstn.setEmail(customer.getEmail());
            subReqPstn.setTelFax(customer.getTelFax());
            subReqPstn.setTelMobile(customer.getTelFax());
//            subReqPstn.setNickName(customer.getNickName());
//            subReqPstn.setNickDomain(customer.getNickDomain());

//            new DeployRequirementDAO().updateSubIdWhenRequest(contractForm.getDeployRequirementId(), subId, cPosSession);
            // vas reg
            //Thuc hien copy SubStockModelRel vao trong SubStockModelRelReq
//            CommonSub.saveSubStockModelRelReq(cPosSession, lstSubStockModelRel, subId);
            //Thuc hien insert stock model type vao subStockModelRel
            /// comen
            saveSubStockModelType(cmPosSession, input.getStockTypeIds(), subId, null);
            // offer single
            subReqPstn.setReqOfferId(reqOfferId);
//            subReqPstn.setProject(adslForm.getProjectId());

            OfferRequest offerRequest = new OfferRequest();
            offerRequest.setCustReqId(custReqId);
            offerRequest.setQualities(1L);
            offerRequest.setReqOfferId(reqOfferId);
//            offerRequest.setOfferId(subReqPstn.getProductId());
            offerRequest.setOfferId(input.getSubscriber().getProductId());
            offerRequest.setIsBundle(0L);
            offerRequest.setStatus(Constants.OFFER_REQUEST_NEW);
            
            cmPosSession.save(offerRequest);
            cmPosSession.save(subReqPstn);
            reqSub.setReqId(id);
        } else if (serviceId.equals(Constants.SERVICE_ADSL_ID_WEBSERVICE)) {
            //     cSession.flush();
            SubReqAdslLl subReqAdslLl = new SubReqAdslLl();
            if (input.getRequestId() != null && input.getRequestId() > 0) {
                subReqAdslLl.setReqOnlineId(input.getRequestId());
            }
            //14062010 cuongnt - tam thoi luon de la A
            subReqAdslLl.setIsdn(Constants.SERVICE_ALIAS_ADSL + subId.toString());
            //subReqAdslLl.setIsdn(input.getInfrastruct().getIsdn());

            subReqAdslLl.setServiceType(Constants.SERVICE_ALIAS_ADSL);
            subReqAdslLl.setCustReqId(custReqId);
            subReqAdslLl.setId(id);
            subReqAdslLl.setIsSmart(Constants.IS_SMART);
            
            subReqAdslLl.setAccount(input.getInfrastruct().getAccount().toLowerCase());
            subReqAdslLl.setPassword(genPasswordAuto(customer.getName()));
            subReqAdslLl.setLineType(input.getSubscriber().getLineType());
//            subReqAdslLl.setLinePhone(adslForm.getLinePhone());
            subReqAdslLl.setDeployAddress(customer.getAddress());
            
            if (Validator.isNull(customer.getAreaCode())) {
                return LabelUtil.getKey("com.request.getareacode", locale);
                
            } else {
                subReqAdslLl.setDeployAreaCode(customer.getAreaCode());
                subReqAdslLl.setAddressCode(customer.getAreaCode());
            }
            subReqAdslLl.setProvince(customer.getProvince());
            subReqAdslLl.setDistrict(customer.getDistrict());
            subReqAdslLl.setPrecinct(customer.getPrecinct());
            subReqAdslLl.setStreetBlock(customer.getStreetBlock());
            subReqAdslLl.setStreetBlockName(customer.getStreetBlockName());
            subReqAdslLl.setStreetName(customer.getStreetName());
            subReqAdslLl.setHome(customer.getHome());
            subReqAdslLl.setStationId(input.getInfrastruct().getTeamId());
            subReqAdslLl.setQuota(GetterUtil.getLong(input.getSubscriber().getLimit()));//truyen len
            subReqAdslLl.setPromotionCode(input.getSubscriber().getPromotion());//truyen len
            subReqAdslLl.setDeposit(GetterUtil.getDouble(input.getSubscriber().getDeposit()));//truyen len
//            subReqAdslLl.setReasonDepositId(adslForm.getReasonDepositId());
            subReqAdslLl.setRegType(input.getSubscriber().getReasonRoam());//truyen len
            subReqAdslLl.setReqType("0");
            subReqAdslLl.setSourceType("0");
            subReqAdslLl.setIpStatic(input.getInfrastruct().getIpStatic());
            subReqAdslLl.setUserUsing(contactName);//ten khach hang
            subReqAdslLl.setUserTitle(customer.getContactTitle());//loai khach hang
            if ("4".equals(input.getInfrastruct().getTechnology())) {
                subReqAdslLl.setInfraType("GPON");
            }
            // resource
//            subReqAdslLl.setDslamId(adslForm.getDslamId());
//            subReqAdslLl.setBoardId(adslForm.getBoardId());
            subReqAdslLl.setCableBoxId(input.getInfrastruct().getCableBoxId());
            subReqAdslLl.setPortNo(input.getInfrastruct().getPort());
//            subReqAdslLl.setCharsic(adslForm.getCharsic());
//            subReqAdslLl.setSlotCard(adslForm.getSlot());
            subReqAdslLl.setTeamId(teamId);
            
            subReqAdslLl.setFirstConnect(null);//TODO currentDate //Nghiem thu moi insert
            // subReqAdslLl.setIsdn(input.getInfrastruct().getIsdn());
            subReqAdslLl.setSpeed(0L);//TODO
            subReqAdslLl.setAddress(customer.getAddress());
            subReqAdslLl.setProductCode(input.getSubscriber().getProductCode());
            subReqAdslLl.setVip("0");//Not VIP
            //luu cho NV phat trien
            subReqAdslLl.setShopCode(shop.getShopCode());
            subReqAdslLl.setStaffId(staff.getStaffId());
            
            subReqAdslLl.setCreateDate(currentDate);
            new BaseDAO().info(_log, "subReqAdslLl.getCreateDate()=" + DateTimeUtils.formatddMMyyyyHHmmss(subReqAdslLl.getCreateDate()));
            subReqAdslLl.setRegReasonId(reasonId);
            subReqAdslLl.setSubType(input.getSubscriber().getSubType());;//subType=1 ADSL, 2 Leaseline
            subReqAdslLl.setLimitDate(CalendarUtil.getDate(currentDate, getLimitDate(cmPosSession, Constants.AP_PARAM_LIMIT_DATE, Constants.AP_PARAM_LIMIT_DATE_ADSL)));//sysdate + 7

            subReqAdslLl.setStaDatetime(currentDate);
            subReqAdslLl.setIsSatisfy(input.getIsSatisfy());
            subReqAdslLl.setStatus(Constants.SUB_REQ_STATUS_NEW);
            
            subReqAdslLl.setActStatus(Constants.SUB_ACT_STATUS_BLOCK_OUT_GOING_CALL_BY_CUST);
            subReqAdslLl.setUserCreated(staff.getStaffCode());

            // offer single
            subReqAdslLl.setReqOfferId(reqOfferId);
            subReqAdslLl.setSubId(subId);
            subReqAdslLl.setIsNewSub(Constants.IS_NEW_SUB_TRUE);
            
            subReqAdslLl.setEmail(customer.getEmail());
            subReqAdslLl.setTelFax(customer.getTelFax());
            subReqAdslLl.setTelMobile(customer.getTelFax());
            subReqAdslLl.setCabLen(input.getInfrastruct().getCableLength());
            if (input.getCustomerIn().getLat() != null && !"".equals(input.getCustomerIn().getLat())) {
                subReqAdslLl.setLat(Double.parseDouble(input.getCustomerIn().getLat()));
            }
            if (input.getCustomerIn().getLng() != null && !"".equals(input.getCustomerIn().getLng())) {
                subReqAdslLl.setLng(Double.parseDouble(input.getCustomerIn().getLng()));
            }
            /*bo sung thong tin toa do connector*/
            if (listCoordinate != null && listCoordinate.size() > 0) {
                for (Coordinate coordinate : listCoordinate) {
                    /*
                     order = 1: customer
                     order = 2: connector
                     */
                    if (coordinate.getOrderNumber().equals(2l)) {
                        subReqAdslLl.setInfraLat(coordinate.getLat());
                        subReqAdslLl.setInfraLong(coordinate.getLng());
                        break;
                    }
                }
            }

//            subReqAdslLl.setNickName(adslForm.getNickName());
//            subReqAdslLl.setNickDomain(adslForm.getNickDomain());
            //Bo sung truong Nha tram
//            subReqAdslLl.setTeamId(adslForm.getTeamId());
//            subReqAdslLl.setProject(adslForm.getProjectId());
//            new DeployRequirementDAO().updateSubIdWhenRequest(contractForm.getDeployRequirementId(), subId, cPosSession);
            // vas reg
            //Thuc hien copy SubStockModelRel vao trong SubStockModelRelReq
//            CommonSub.saveSubStockModelRelReq(cPosSession, lstSubStockModelRel, subId);
            //Thuc hien insert stock model type vao subStockModelRel
            saveSubStockModelType(cmPosSession, input.getStockTypeIds(), subId, null);

            // save tbl offer_request
            OfferRequest offerRequest = new OfferRequest();
            offerRequest.setCustReqId(custReqId);
            offerRequest.setQualities(1L);
            offerRequest.setReqOfferId(reqOfferId);
//            offerRequest.setOfferId(subReqAdslLl.getProductId());
            offerRequest.setOfferId(input.getSubscriber().getProductId());
            offerRequest.setIsBundle(0L);
            offerRequest.setStatus(Constants.OFFER_REQUEST_NEW);
            
            cmPosSession.save(offerRequest);
            cmPosSession.save(subReqAdslLl);
            reqSub.setReqId(id);
            
        } else if (serviceId.equals(Constants.SERVICE_LEASELINE_ID_WEBSERVICE)) {
            SubReqAdslLl subReqAdslLl = new SubReqAdslLl();
            
            subReqAdslLl.setIsdn(Constants.SERVICE_ALIAS_LEASEDLINE + subId.toString());
            //14062010 cuongnt - tam thoi luon de la A
            subReqAdslLl.setServiceType(Constants.SERVICE_ALIAS_LEASEDLINE);
            subReqAdslLl.setCustReqId(custReqId);
            subReqAdslLl.setId(id);
            subReqAdslLl.setIsSmart(Constants.IS_SMART);
            subReqAdslLl.setAccount(input.getInfrastruct().getAccount().toLowerCase());
            subReqAdslLl.setPassword(genPasswordAuto(customer.getName()));
            subReqAdslLl.setLineType(input.getSubscriber().getLineType());
//            subReqAdslLl.setLinePhone(adslForm.getLinePhone());
            subReqAdslLl.setDeployAddress(customer.getAddress());
            if (Validator.isNull(customer.getAreaCode())) {
                return LabelUtil.getKey("com.request.getareacode", locale);
                
            } else {
                subReqAdslLl.setDeployAreaCode(customer.getAreaCode());
                subReqAdslLl.setAddressCode(customer.getAreaCode());
            }
            subReqAdslLl.setProvince(customer.getProvince());
            subReqAdslLl.setReqType("0");
            subReqAdslLl.setSourceType("0");
            subReqAdslLl.setDistrict(customer.getDistrict());
            subReqAdslLl.setPrecinct(customer.getPrecinct());
            subReqAdslLl.setStreetBlock(customer.getStreetBlock());
            subReqAdslLl.setStreetBlockName(customer.getStreetBlockName());
            subReqAdslLl.setStreetName(customer.getStreetName());
            subReqAdslLl.setHome(customer.getHome());
            subReqAdslLl.setQuota(GetterUtil.getLong(input.getSubscriber().getLimit()));//truyen len
            subReqAdslLl.setPromotionCode(input.getSubscriber().getPromotion());//truyen len
            subReqAdslLl.setDeposit(GetterUtil.getDouble(input.getSubscriber().getDeposit()));//truyen len
//            subReqAdslLl.setReasonDepositId(adslForm.getReasonDepositId());
            subReqAdslLl.setRegType(input.getSubscriber().getReasonRoam());//truyen len
            subReqAdslLl.setIpStatic(input.getInfrastruct().getIpStatic());
            subReqAdslLl.setUserUsing(contactName);//ten khach hang
            subReqAdslLl.setUserTitle(customer.getContactTitle());//loai khach hang 

            // resource
//            subReqAdslLl.setDslamId(adslForm.getDslamId());
//            subReqAdslLl.setBoardId(adslForm.getBoardId());
            subReqAdslLl.setCableBoxId(input.getInfrastruct().getCableBoxId());
            subReqAdslLl.setPortNo(input.getInfrastruct().getPort());
//            subReqAdslLl.setCharsic(adslForm.getCharsic());
//            subReqAdslLl.setSlotCard(adslForm.getSlot());
            subReqAdslLl.setTeamId(teamId);
            
            subReqAdslLl.setFirstConnect(null);//TODO currentDate //Nghiem thu moi insert
            // subReqAdslLl.setIsdn(input.getInfrastruct().getIsdn());
            subReqAdslLl.setSpeed(0L);//TODO
            subReqAdslLl.setAddress(customer.getAddress());
            subReqAdslLl.setProductCode(input.getSubscriber().getProductCode());
            subReqAdslLl.setVip("0");//Not VIP
            //luu cho NV phat trien
            subReqAdslLl.setShopCode(shop.getShopCode());
            subReqAdslLl.setStaffId(staff.getStaffId());
            
            subReqAdslLl.setCreateDate(currentDate);
            new BaseDAO().info(_log, "subReqAdslLl.getCreateDate()=" + DateTimeUtils.formatddMMyyyyHHmmss(subReqAdslLl.getCreateDate()));
            subReqAdslLl.setRegReasonId(reasonId);
            subReqAdslLl.setSubType(input.getSubscriber().getSubType());//subType=1 ADSL, 2 Leaseline
            subReqAdslLl.setLimitDate(CalendarUtil.getDate(currentDate, getLimitDate(cmPosSession, Constants.AP_PARAM_LIMIT_DATE, Constants.AP_PARAM_LIMIT_DATE_LL)));//sysdate + 7

            subReqAdslLl.setStaDatetime(currentDate);
            subReqAdslLl.setIsSatisfy(input.getIsSatisfy());
            subReqAdslLl.setStatus(Constants.SUB_REQ_STATUS_NEW);
            
            subReqAdslLl.setActStatus(Constants.SUB_ACT_STATUS_BLOCK_OUT_GOING_CALL_BY_CUST);
            subReqAdslLl.setUserCreated(staff.getStaffCode());

            // offer single
            subReqAdslLl.setReqOfferId(reqOfferId);
            subReqAdslLl.setSubId(subId);
            subReqAdslLl.setIsNewSub(Constants.IS_NEW_SUB_TRUE);
            
            subReqAdslLl.setPricePlan(input.getSubscriber().getPricePlan());
            subReqAdslLl.setLocalPricePlan(input.getSubscriber().getLocalPricePlan());
            subReqAdslLl.setEmail(customer.getEmail());
            subReqAdslLl.setTelFax(customer.getTelFax());
            subReqAdslLl.setStationId(input.getInfrastruct().getTeamId());
            subReqAdslLl.setTelMobile(customer.getTelFax());
            subReqAdslLl.setCabLen(input.getInfrastruct().getCableLength());
            if ("4".equals(input.getInfrastruct().getTechnology())) {
//                subReqAdslLl.setInfraType(input.getInfrastruct().getTechnology());
                subReqAdslLl.setInfraType("GPON");
            } else {
                subReqAdslLl.setInfraType("AON");
            }
            if (input.getCustomerIn().getLat() != null && !"".equals(input.getCustomerIn().getLat())) {
                subReqAdslLl.setLat(Double.parseDouble(input.getCustomerIn().getLat()));
            }
            if (input.getCustomerIn().getLng() != null && !"".equals(input.getCustomerIn().getLng())) {
                subReqAdslLl.setLng(Double.parseDouble(input.getCustomerIn().getLng()));
            }
            /*bo sung thong tin toa do connector*/
            if (listCoordinate != null && listCoordinate.size() > 0) {
                for (Coordinate coordinate : listCoordinate) {
                    /*
                     order = 1: customer
                     order = 2: connector
                     */
                    if (coordinate.getOrderNumber().equals(1l)) {
                        subReqAdslLl.setLat(coordinate.getLat());
                        subReqAdslLl.setLng(coordinate.getLng());
                    }
                    if (coordinate.getOrderNumber().equals(2l)) {
                        subReqAdslLl.setInfraLat(coordinate.getLat());
                        subReqAdslLl.setInfraLong(coordinate.getLng());
                    }
                }
            }
            /*Doi voi GPON luu them thong tin device tu truong deviceCode tra ve tu khao sat ha tang tu NIMS vao DSLAM_ID*/
            if ("4".equals(input.getInfrastruct().getTechnology())) {
                Long deviceId = Common.getInraDeviceIdFromCode(nimsSession, input.getInfrastruct().getDevice());
                subReqAdslLl.setDslamId(deviceId);
            }
//            subReqAdslLl.setNickName(adslForm.getNickName());
//            subReqAdslLl.setNickDomain(adslForm.getNickDomain());
            //Bo sung truong Nha tram
//            subReqAdslLl.setTeamId(adslForm.getTeamId());

//            new DeployRequirementDAO().updateSubIdWhenRequest(contractForm.getDeployRequirementId(), subId, cPosSession);
            // vas reg
            //Thuc hien copy SubStockModelRel vao trong SubStockModelRelReq
//            CommonSub.saveSubStockModelRelReq(cPosSession, lstSubStockModelRel, subId);
            //Thuc hien insert stock model type vao subStockModelRel
            saveSubStockModelType(cmPosSession, input.getStockTypeIds(), subId, null);
            // save tbl offer_request
            OfferRequest offerRequest = new OfferRequest();
            offerRequest.setCustReqId(custReqId);
            offerRequest.setQualities(1L);
            offerRequest.setReqOfferId(reqOfferId);
            offerRequest.setOfferId(input.getSubscriber().getProductId());
            offerRequest.setIsBundle(0L);
            offerRequest.setStatus(Constants.OFFER_REQUEST_NEW);
            
            cmPosSession.save(offerRequest);
            cmPosSession.save(subReqAdslLl);
            reqSub.setReqId(id);
        } else if (serviceId.equals(Constants.SERVICE_FTTH_ID_WEBSERVICE)) {
            
            SubReqAdslLl subReqAdslLl = new SubReqAdslLl();
            if (input.getRequestId() != null && input.getRequestId() > 0) {
                subReqAdslLl.setReqOnlineId(input.getRequestId());
            }
            subReqAdslLl.setIsdn(Constants.SERVICE_ALIAS_FTTH + subId.toString());
            //14062010 cuongnt - tam thoi luon de la A
            subReqAdslLl.setServiceType(Constants.SERVICE_ALIAS_FTTH);
            subReqAdslLl.setCustReqId(custReqId);
            subReqAdslLl.setId(id);
            subReqAdslLl.setIsSmart(Constants.IS_SMART);
            subReqAdslLl.setStationId(input.getInfrastruct().getTeamId());
            subReqAdslLl.setIsSatisfy(Constants.IS_SATISFY);
            subReqAdslLl.setReqType("0");
            subReqAdslLl.setSourceType("0");
            subReqAdslLl.setAccount(input.getInfrastruct().getAccount().toLowerCase());
            subReqAdslLl.setPassword(genPasswordAuto(customer.getName()));
            subReqAdslLl.setLineType(input.getSubscriber().getLineType());
//            subReqAdslLl.setLinePhone(adslForm.getLinePhone());
            subReqAdslLl.setDeployAddress(customer.getAddress());
            if (Validator.isNull(customer.getAreaCode())) {
                return LabelUtil.getKey("com.request.getareacode", locale);
                
            } else {
                subReqAdslLl.setDeployAreaCode(customer.getAreaCode());
                subReqAdslLl.setAddressCode(customer.getAreaCode());
            }
            subReqAdslLl.setProvince(customer.getProvince());
            subReqAdslLl.setDistrict(customer.getDistrict());
            subReqAdslLl.setPrecinct(customer.getPrecinct());
            subReqAdslLl.setStreetBlock(customer.getStreetBlock());
            subReqAdslLl.setStreetBlockName(customer.getStreetBlockName());
            subReqAdslLl.setStreetName(customer.getStreetName());
            subReqAdslLl.setHome(customer.getHome());
            subReqAdslLl.setQuota(GetterUtil.getLong(input.getSubscriber().getLimit()));//truyen len
            subReqAdslLl.setPromotionCode(input.getSubscriber().getPromotion());//truyen len
            subReqAdslLl.setDeposit(GetterUtil.getDouble(input.getSubscriber().getDeposit()));//truyen len
//            subReqAdslLl.setReasonDepositId(adslForm.getReasonDepositId());
            subReqAdslLl.setRegType(input.getSubscriber().getReasonRoam());//truyen len
            subReqAdslLl.setIpStatic(input.getInfrastruct().getIpStatic());
            subReqAdslLl.setUserUsing(contactName);//ten khach hang
            subReqAdslLl.setUserTitle(customer.getContactTitle());//loai khach hang 

            // resource
//            subReqAdslLl.setDslamId(adslForm.getDslamId());
//            subReqAdslLl.setBoardId(adslForm.getBoardId());
            subReqAdslLl.setCableBoxId(input.getInfrastruct().getCableBoxId());
            subReqAdslLl.setPortNo(input.getInfrastruct().getPort());
//            subReqAdslLl.setCharsic(adslForm.getCharsic());
//            subReqAdslLl.setSlotCard(adslForm.getSlot());
            subReqAdslLl.setTeamId(teamId);
            
            subReqAdslLl.setFirstConnect(null);//TODO currentDate //Nghiem thu moi insert
            //subReqAdslLl.setIsdn(input.getInfrastruct().getIsdn());
            subReqAdslLl.setSpeed(0L);//TODO
            subReqAdslLl.setAddress(customer.getAddress());
            subReqAdslLl.setProductCode(input.getSubscriber().getProductCode());
            subReqAdslLl.setVip("0");//Not VIP
            //luu cho NV phat trien
            subReqAdslLl.setShopCode(shop.getShopCode());
            subReqAdslLl.setStaffId(staff.getStaffId());
            
            subReqAdslLl.setCreateDate(currentDate);
            new BaseDAO().info(_log, "subReqAdslLl.getCreateDate()=" + DateTimeUtils.formatddMMyyyyHHmmss(subReqAdslLl.getCreateDate()));
            subReqAdslLl.setRegReasonId(reasonId);
            subReqAdslLl.setSubType(input.getSubscriber().getSubType());//subType=1 ADSL, 2 Leaseline
            subReqAdslLl.setLimitDate(CalendarUtil.getDate(currentDate, getLimitDate(cmPosSession, Constants.AP_PARAM_LIMIT_DATE, Constants.AP_PARAM_LIMIT_DATE_FTTH)));//sysdate + 7

            subReqAdslLl.setStaDatetime(currentDate);
            subReqAdslLl.setIsSatisfy(input.getIsSatisfy());
            subReqAdslLl.setStatus(Constants.SUB_REQ_STATUS_NEW);
            
            subReqAdslLl.setActStatus(Constants.SUB_ACT_STATUS_BLOCK_OUT_GOING_CALL_BY_CUST);
            subReqAdslLl.setUserCreated(staff.getStaffCode());

            // offer single
            subReqAdslLl.setReqOfferId(reqOfferId);
            subReqAdslLl.setSubId(subId);
            subReqAdslLl.setIsNewSub(Constants.IS_NEW_SUB_TRUE);
            if ("4".equals(input.getInfrastruct().getTechnology())) {
//                subReqAdslLl.setInfraType(input.getInfrastruct().getTechnology());
                subReqAdslLl.setInfraType("GPON");
            } else {
                subReqAdslLl.setInfraType("AON");
            }
            subReqAdslLl.setEmail(customer.getEmail());
            subReqAdslLl.setTelFax(customer.getTelFax());
            subReqAdslLl.setTelMobile(customer.getTelFax());
            subReqAdslLl.setCabLen(input.getInfrastruct().getCableLength());
            if (input.getCustomerIn().getLat() != null && !"".equals(input.getCustomerIn().getLat())) {
                subReqAdslLl.setLat(Double.parseDouble(input.getCustomerIn().getLat()));
            }
            if (input.getCustomerIn().getLng() != null && !"".equals(input.getCustomerIn().getLng())) {
                subReqAdslLl.setLng(Double.parseDouble(input.getCustomerIn().getLng()));
            }
            /*bo sung thong tin toa do connector*/
            if (listCoordinate != null && listCoordinate.size() > 0) {
                for (Coordinate coordinate : listCoordinate) {
                    /*
                     order = 1: customer
                     order = 2: connector
                     */
                    if (coordinate.getOrderNumber().equals(1l)) {
                        subReqAdslLl.setLat(coordinate.getLat());
                        subReqAdslLl.setLng(coordinate.getLng());
                    }
                    if (coordinate.getOrderNumber().equals(2l)) {
                        subReqAdslLl.setInfraLat(coordinate.getLat());
                        subReqAdslLl.setInfraLong(coordinate.getLng());
                    }
                }
            }
            /*Doi voi GPON luu them thong tin device tu truong deviceCode tra ve tu khao sat ha tang tu NIMS vao DSLAM_ID*/
            if ("4".equals(input.getInfrastruct().getTechnology())) {
                Long deviceId = Common.getInraDeviceIdFromCode(nimsSession, input.getInfrastruct().getDevice());
                subReqAdslLl.setDslamId(deviceId);
            }

            /*Bo sung thong tin bundle tv*/
            if (input.getSubscriber().getBundleTv() != null) {
                SubBundleTv tv = input.getSubscriber().getBundleTv();
                if (tv.getAccount() == null || tv.getAccount().isEmpty()) {
                    return "Account bundle tv is not null";
                }
                if (tv.getProductCode() == null || tv.getProductCode().isEmpty()) {
                    return "Product bundle tv is not null";
                }
                if (tv.getRegReasonCode() == null || tv.getRegReasonCode().isEmpty()) {
                    return "Reg reason code bundle tv is not null";
                }
                if (tv.getRegReasonId() == null) {
                    return "Reg reason id bundle tv is not null";
                }
                subReqAdslLl.setAccBundleTv(tv.getAccount());
                tv.setCreateDate(new Date());
                tv.setCreateUser(staff.getStaffCode());
                tv.setStatus(0l);
                tv.setSubId(subId);
                cmPosSession.save(tv);
            }
            
            saveSubStockModelType(cmPosSession, input.getStockTypeIds(), subId, null);
            if (saleCodeTv != null) {
                saveSubStockModelType(cmPosSession, arrStockTypeTvId, subId, Constants.OTHER_PRO_BUNDLE_TV);
            }
            OfferRequest offerRequest = new OfferRequest();
            offerRequest.setCustReqId(custReqId);
            offerRequest.setQualities(1L);
            offerRequest.setReqOfferId(reqOfferId);
            offerRequest.setOfferId(input.getSubscriber().getProductId());
            offerRequest.setIsBundle(0L);
            offerRequest.setStatus(Constants.OFFER_REQUEST_NEW);
            
            cmPosSession.save(offerRequest);
            cmPosSession.save(subReqAdslLl);
            reqSub.setReqId(id);
        }
        
        return result;
    }
    
    public String genAccountAuto(Session cmPosSession, String serviceType, String province)
            throws Exception {
        
        StringBuilder account = new StringBuilder();
        
        userManualMap = getUserManualConfig(cmPosSession);
        // Dinh dang account Ma dich vu + Ma tinh + sequence
        if (serviceType != null
                && serviceType.equals(Constants.SERVICE_ALIAS_LEASEDLINE)) {
            account.append(userManualMap.get(Constants.PRODUCT_GROUP_LL));
        } else {
            account.append(userManualMap.get(Constants.PRODUCT_GROUP_ADSL));
        }
        
        String addProvince = GetterUtil.getString(userManualMap.get(province), StringPool.BLANK);
        
        if (Validator.isNull(addProvince)) {
            addProvince = "23";
        }
        
        account.append(addProvince);

        // lay ve chuoi tu tang
        int len = GetterUtil.getInteger(
                userManualMap.get(Constants.LENG_ACC_AUTO_INCREASE_LIST_NUMBER));
        
        BaseSupplier supplier = new BaseSupplier();
        Long seqAccount = supplier.getSequence(cmPosSession, Constants.AUTO_ACCOUNT_SEQ);
        
        String listNumber = StringPool.BLANK;
        
        if (seqAccount.toString().length() < len) {
            listNumber = seqAccount.toString();
            
            for (int i = 0; i < len - seqAccount.toString().length(); i++) {
                listNumber = "0" + listNumber;
            }
            
        } else {
            listNumber = seqAccount.toString();
        }
        
        account.append(listNumber);
        
        return account.toString().toLowerCase();
    }
    
    private Map<String, String> getUserManualConfig(Session cSession) {
        if (userManualMap.isEmpty()) {
            try {
                StringBuilder sb = new StringBuilder();
                
                sb.append("select param_type as paramType, param_value as paramValue  ");
                sb.append("from ap_param where status = ? and param_code = ?");
                
                SQLQuery sql = cSession.createSQLQuery(sb.toString());
                
                sql.setParameter(0, Values.STATUS_ACTIVE);
                sql.setParameter(1, Constants.USER_MANUAL_CONFIG);
                
                sql.addScalar("paramType", Hibernate.STRING);
                sql.addScalar("paramValue", Hibernate.STRING);
                
                List<Object[]> results = sql.list();
                
                for (Object[] obj : results) {
                    userManualMap.put((String) obj[0], (String) obj[1]);
                }
            } catch (HibernateException ex) {
                new BaseDAO().error(_log, ex.getMessage(), ex);
            }
        }
        
        return userManualMap;
    }
    
    public String getProductGroupByProductCodeAndTelService(Session pSession,
            String productCode, Long telService) {
        
        String result = "";
        try {
            if (Validator.isNull(productCode)) {
                return result;
            }

            // Date: 27/05/2013 ==> tach dich vu FTTH, sua ham loi de khong anh huong toi cac chuc nang con lai.
            if (Constants.SERVICE_FTTH_ID_WEBSERVICE.equals(telService)) {
                return Constants.PRODUCT_GROUP_FTTH;
            }
            
            List<Product> ftths = getListFtthProduct(pSession);
            
            for (Product ftth : ftths) {
                if (productCode.equals(ftth.getProductCode())) {
                    return Constants.PRODUCT_GROUP_FTTH;
                }
            }

            /* [ Xử lý cho LL Cuongnt
             Hien tai Leaseline sẽ xử lý như FTTH nên tạm fix groupProduct của LL là FTTH để không phải thay đổi phần công việc
             */
            List<Product> leasedLines = getListLeasedlineProduct(pSession);
            
            for (Product ll : leasedLines) {
                if (ll != null && productCode.equals(ll.getProductCode())) {
                    return Constants.PRODUCT_GROUP_FTTH;
                }
            }
            
            if (Constants.SERVICE_LEASELINE_ID_WEBSERVICE.equals(telService)) {
                return Constants.PRODUCT_GROUP_FTTH;
            }
            /* Xử lý cho LL ]*/
            
            String productType = "P";
            
            System.out.println(productCode + "," + telService + "," + productType);
            
            String groupProduct = PMAPI.getGroupProductByProductCodeAndTelService(
                    productCode, telService, productType, pSession);
            
            if (Validator.isNotNull(groupProduct)) {
                result = groupProduct;
            }
        } catch (Exception ex) {
            new BaseDAO().error(_log, ex.getMessage(), ex);
        }
        
        System.out.println(result);
        
        return result;
    }
    
    public List<Product> getListFtthProduct(Session pSession) {
        List<Product> results = new ArrayList<Product>();
        
        try {
            StringBuilder sb = new StringBuilder();
            
            sb.append("select * from product where telecom_service_id=? and status = ? ");
            
            SQLQuery sql = pSession.createSQLQuery(sb.toString());
            
            sql
                    .addEntity(Product.class);
            
            sql.setParameter(
                    0, Constants.SERVICE_FTTH_ID_WEBSERVICE);
            sql.setParameter(
                    1, Values.STATUS_ACTIVE);
            
            results = sql.list();
        } catch (Exception ex) {
            new BaseDAO().error(_log, ex.getMessage(), ex);
        }
        
        return results;
    }
    
    public List<Product> getListLeasedlineProduct(Session pSession) {
        List<Product> results = new ArrayList<Product>();
        
        try {
            StringBuilder sb = new StringBuilder();
            
            sb.append("select * from product where telecom_service_id=? and status = ? ");
            
            SQLQuery sql = pSession.createSQLQuery(sb.toString());
            
            sql
                    .addEntity(Product.class);
            
            sql.setParameter(
                    0, Constants.SERVICE_LEASELINE_ID_WEBSERVICE);
            sql.setParameter(
                    1, Values.STATUS_ACTIVE);
            
            results = sql.list();
        } catch (Exception ex) {
            new BaseDAO().error(_log, ex.getMessage(), ex);
        }
        
        return results;
    }
    
    public String lockIpStatic(Session imSession, String ipStatic, Long shopId, Long staffId,
            String locale) throws Exception {
        Locale _locale = LabelUtil.getLocale(locale);
        
        WebServiceAPDAO webIMDAO = new WebServiceAPDAO(imSession);
        
        WebServiceAPDAO.APResult lockIp = webIMDAO.lockIppool(ipStatic, shopId, staffId);
        
        return convertErrCodeToString(lockIp, _locale);
    }
    
    public void saveSubStockModelType(Session cmPosSession, Long[] stockModelType,
            Long subId, String stockTypeName) throws Exception {
        
        if (stockModelType != null && subId != null) {
            SubStockModelRelReq subStockModelRelReq = null;
            
            Long subStockModelRelId;
            BaseSupplier supplier = new BaseSupplier();
            for (Long stockModelId : stockModelType) {
                subStockModelRelReq = new SubStockModelRelReq();
                subStockModelRelId = supplier.getSequence(cmPosSession, Constants.SUB_STOCK_MODEL_REL_SEQ);
                subStockModelRelReq.setSubStockModelRelId(subStockModelRelId);
                subStockModelRelReq.setSubId(subId);
                subStockModelRelReq.setStatus(Values.STATUS_ACTIVE);
                subStockModelRelReq.setStockModelId(stockModelId);
                subStockModelRelReq.setStockTypeName(stockTypeName);
                cmPosSession.save(subStockModelRelReq);
                cmPosSession.flush();
            }
        }
    }
    
    public Customer getCustomer(Session cSession, Long custId) {
        Customer customer = null;
        
        try {
            Criteria cri = cSession.createCriteria(Customer.class);
            
            cri.add(Restrictions.eq("custId", custId));
            
            customer = (Customer) cri.uniqueResult();
        } catch (Exception ex) {
            new BaseDAO().error(_log, ex.getMessage(), ex);
        }
        
        return customer;
    }
    
    public Staff getStaff(Session cSession, Long staffId) {
        Staff staff = null;
        
        try {
            Criteria cri = cSession.createCriteria(Staff.class);
            
            cri.add(Restrictions.eq("staffId", staffId));
            
            staff = (Staff) cri.uniqueResult();
        } catch (Exception ex) {
            new BaseDAO().error(_log, ex.getMessage(), ex);
        }
        
        return staff;
    }
    
    public Shop getShop(Session cSession, Long shopId) {
        Shop shop = null;
        
        try {
            Criteria cri = cSession.createCriteria(Shop.class);
            
            cri.add(Restrictions.eq("shopId", shopId));
            
            shop = (Shop) cri.uniqueResult();
        } catch (Exception ex) {
            new BaseDAO().error(_log, ex.getMessage(), ex);
        }
        
        return shop;
    }
    
    public String genPasswordAuto(String customerName) {
        if (Validator.isNull(customerName)) {
            // CustomerName Is Alphabet
            StringBuilder strBuffer = new StringBuilder();
            
            for (int j = 65; j <= (65 + 24); j++) {
                strBuffer.append((char) j);
            }
            
            customerName = strBuffer.toString();
        }
        StringBuilder buffer = new StringBuilder();
        
        customerName = EditorUtils.convertUnicode2Nosign(customerName);
        customerName = customerName.replaceAll(" ", "");
        
        Random random = new Random();
        
        customerName = customerName.concat("0123456789");
        char[] chars = customerName.toCharArray();
        
        for (int i = 0; i < 6; i++) {
            buffer.append(chars[random.nextInt(chars.length)]);
        }
        
        return buffer.toString();
    }
    
    public int getLimitDate(Session cSession, String type, String code) {
        int limit = 7;
        
        try {
            StringBuilder sb = new StringBuilder();
            
            sb.append("select param_value as limit from ap_param where ");
            sb.append("status = ? and param_code = ? and param_type = ?");
            
            int i = 0;
            
            SQLQuery sql = cSession.createSQLQuery(sb.toString());
            
            sql.setParameter(i++, Values.STATUS_ACTIVE);
            sql.setParameter(i++, code);
            sql.setParameter(i++, type);
            
            sql.addScalar("limit", Hibernate.STRING);
            
            List<String> results = sql.list();
            
            if (!results.isEmpty()) {
                limit = GetterUtil.getInteger(results.get(Values.FIRST_INDEX), limit);
            }
        } catch (Exception ex) {
            new BaseDAO().error(_log, ex.getMessage(), ex);
        }
        
        new BaseDAO().info(_log, "RequestDAOUtil.getLimitDate:limit=" + limit);
        
        return limit;
    }
    
    public Double getMinDepositByProductCode(Long serviceId, String productCode,
            Session cSession, Session pSession) {
        Double min = 0D;
        
        try {
            StringBuilder sb = new StringBuilder();
            
            sb.append("select deposit from map_product_deposit where product_code = :productCode ");
            sb.append("and status = :status and telecom_service_id = :serviceId order by deposit");
            
            SQLQuery sql = cSession.createSQLQuery(sb.toString());
            
            sql.setParameter("status", Values.STATUS_ACTIVE);
            sql.setParameter("productCode", productCode);
            sql.setParameter("serviceId", serviceId);
            
            sql.addScalar("deposit", Hibernate.DOUBLE);
            
            List<Double> results = (List<Double>) sql.list();
            
            if (Validator.isNull(results)) {
                // Neu map theo product_group
                String productGroup = PMAPI.getGroupProductByProductCode(
                        productCode, null, pSession);
                
                if (Validator.isNotNull(productGroup)) {
                    StringBuilder sb1 = new StringBuilder();
                    
                    sb1.append("select deposit from map_product_deposit where product_code is null ");
                    sb1.append("and product_group = :productGroup and status = :status ");
                    sb1.append("and telecom_service_id = :serviceId order by deposit");
                    
                    SQLQuery sql1 = cSession.createSQLQuery(sb1.toString());
                    
                    sql1.setParameter("status", Values.STATUS_ACTIVE);
                    sql1.setParameter("productGroup", productGroup);
                    sql1.setParameter("serviceId", serviceId);
                    
                    sql.addScalar("deposit", Hibernate.DOUBLE);
                    
                    results = (List<Double>) sql.list();
                }
                
            }
            
            if (!results.isEmpty()) {
                min = results.get(Values.FIRST_INDEX);
            }
        } catch (Exception ex) {
            new BaseDAO().error(_log, ex.getMessage(), ex);
        }
        
        new BaseDAO().info(_log, min);
        
        return min;
    }
    
    public Long getTeamIdByStationId(Session cSession, Long stationId) {
        Long teamId = null;
        
        try {
            StringBuilder sb = new StringBuilder();
            
            sb.append("select team_id as teamId from technical_station where station_id = :stationId");
            
            SQLQuery sql = cSession.createSQLQuery(sb.toString());
            
            sql.setParameter("stationId", stationId);
            
            sql.addScalar("teamId", Hibernate.LONG);
            
            List<Long> results = (List<Long>) sql.list();
            
            if (!results.isEmpty()) {
                teamId = results.get(Values.FIRST_INDEX);
            }
        } catch (Exception ex) {
            new BaseDAO().error(_log, ex.getMessage(), ex);
        }
        
        new BaseDAO().info(_log, "teamId: " + teamId);
        
        return teamId;
    }
    
    public String getSubDebitByCust(Session pmSession, Long cusId) throws Exception {
        
        String debit = "";
        try {
            PaymentDebit paymentDebit = new PaymentDebit();
            
            debit = paymentDebit.getDebitByCustomer(pmSession, cusId);
            return debit;
        } catch (Exception ex) {
            new BaseDAO().error(_log, ex.getMessage(), ex);
            throw ex;
        } finally {
            new BaseDAO().info(_log, "debit: " + debit);
        }
    }
    
    public String getCheckDeposit(Session cSession, String sub_type) {
        String check_deposit = null;
        
        try {
            StringBuilder sb = new StringBuilder();
            
            sb.append("select check_deposit as checkDeposit from sub_type where sub_type = :subType ");
            
            SQLQuery sql = cSession.createSQLQuery(sb.toString());
            
            sql.setParameter("subType", sub_type);
            
            sql.addScalar("checkDeposit", Hibernate.STRING);
            
            List<String> results = (List<String>) sql.list();
            
            if (!results.isEmpty()) {
                check_deposit = results.get(Values.FIRST_INDEX);
            }
        } catch (Exception ex) {
            new BaseDAO().error(_log, ex.getMessage(), ex);
        }
        
        new BaseDAO().info(_log, "check_deposit: " + check_deposit);
        
        return check_deposit;
    }
    
    public WSRespone addRequestNoInfra(Session cmPosSession, Long custId, String serviceType, Number Lat, Number Lng, String locale) {
        WSRespone result = new WSRespone("0", "Success");
        try {
            String sql = "insert into sub_req_no_infra (sub_req_no_infra_id, cust_id, service_type, is_smart,lat, lng,create_date) "
                    + " values (sub_req_no_infra_seq.nextval,?,?,1,?,?,sysdate)";
            Query query = cmPosSession.createSQLQuery(sql);
            query.setParameter(0, custId);
            query.setParameter(1, serviceType);
            query.setParameter(2, Lat);
            query.setParameter(3, Lng);
            query.executeUpdate();
            cmPosSession.flush();
            cmPosSession.getTransaction().commit();
        } catch (Exception ex) {
            result.setErrorCode("01");
            result.setErrorCode(ex.getMessage());
            return result;
        }
        return result;
    }
    
    public String saveLocation(Session cmPosSession, List<Coordinate> listCoordinate, Long reqId, Long drawType) {
        try {
            BaseSupplier supplier = new BaseSupplier();
            Date currentDate = supplier.getSysDateTime(cmPosSession);
            StringBuilder sql = new StringBuilder();
            sql = new StringBuilder(" update sub_draw_cable set status = 0 where req_id = ? ");
            Query query = cmPosSession.createSQLQuery(sql.toString());
            query.setParameter(0, reqId);
            query.executeUpdate();
            
            sql = new StringBuilder(" insert into  sub_draw_cable (sub_draw_cable_id, lat, lng, location, order_number, req_id, create_date,draw_type,status) ");
            sql.append(" values (sub_draw_cable_seq.nextval,?,?,?,?,?,?,?,1) ");
            for (int i = 0; i < listCoordinate.size(); i++) {
                query = cmPosSession.createSQLQuery(sql.toString());
                query.setParameter(0, listCoordinate.get(i).getLat());
                query.setParameter(1, listCoordinate.get(i).getLng());
                query.setParameter(2, listCoordinate.get(i).getLocation());
                query.setParameter(3, listCoordinate.get(i).getOrderNumber());
                query.setParameter(4, reqId);
                query.setParameter(5, currentDate);
                query.setParameter(6, drawType);
                query.executeUpdate();
            }
//            cmPosSession.flush();
//            cmPosSession.getTransaction().commit();
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return null;
    }
    
    public boolean checkExistsInChangerServiceAdsl(Session cmPosSession, String account) {
        String queryString = "select * from Change_Service where old_account = ? ";
        Query queryObject = cmPosSession.createSQLQuery(queryString);
        queryObject.setParameter(0, account);
        if (queryObject.list().size() > 0) {
            return true;
        }
        return false;
    }

    /**
     * @author duyetdk
     * @des lay ra dich vu bi khoa connector
     * @param cmPosSession
     * @param connectorCode
     * @param infraType
     * @return
     * @throws Exception
     */
    public List<String> getLockServiceByConnectorCode(Session cmPosSession, String connectorCode, String infraType) throws Exception {
        try {
            if (connectorCode == null || connectorCode.trim().isEmpty()) {
                return null;
            }
            
            String sql;
            Query query;
            if ("4".equals(infraType) || "GPON".equals(infraType)) {//4:GPON
                sql = "SELECT a.locked_service "
                        + " FROM nims_fcn.infra_device a WHERE a.device_code = ? ";
                query = cmPosSession.createSQLQuery(sql);
            } else {//con lai la AON
                sql = "SELECT b.locked_service "
                        + " FROM nims_cn.odn_odf b WHERE b.odf_code = ? ";
                query = cmPosSession.createSQLQuery(sql);
            }
            
            query.setParameter(0, connectorCode.trim());
            List lst = query.list();
            if (lst != null && !lst.isEmpty()) {
                return lst;
            }
        } catch (HibernateException ex) {
            System.out.println(ex);
        }
        return null;
    }

    /**
     * @author duyetdk
     * @param splitterCode
     * @return
     * @throws Exception
     */
    public GetSnFromSplReponse findBySplitterCode(String splitterCode) throws Exception {
        if (splitterCode == null || splitterCode.trim().isEmpty()) {
            return null;
        }
        Input input = new Input();
        List<Param> param = new ArrayList<Param>();
        param.add(new Param("splitterCode", splitterCode));
        input.setParam(param);
        GetSnFromSplReponse snId = (GetSnFromSplReponse) new WebServiceClient().sendRequestViaBccsGW(input, "getNodeTBBySplitterCode", GetSnFromSplReponse.class);
        if (snId == null || snId.getReturn() == null) {
            return null;
        }
        return snId;
    }

    /**
     * @author duyetdk
     * @param nimsSession
     * @param connectorId
     * @param infraType
     * @return
     * @throws Exception
     */
    public List<String> getConnectorCodeById(Session nimsSession, Long connectorId, String infraType) throws Exception {
        try {
            if (connectorId == null || connectorId == 0L) {
                return null;
            }
            String sql;
            Query query;
            if ("4".equals(infraType)) {//4:GPON
                sql = "SELECT a.device_code "
                        + " FROM nims_fcn.infra_device a WHERE a.device_id = ? ";
                query = nimsSession.createSQLQuery(sql);
            } else {//con lai la AON
                sql = "SELECT b.odf_code "
                        + " FROM nims_cn.odn_odf b WHERE b.odf_id = ? ";
                query = nimsSession.createSQLQuery(sql);
            }
            query.setParameter(0, connectorId);
            List lst = query.list();
            if (lst != null && !lst.isEmpty()) {
                return lst;
            }
        } catch (HibernateException ex) {
            System.out.println(ex);
        }
        return null;
    }
}
