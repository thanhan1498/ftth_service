package com.viettel.brcd.event;

///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package com.viettel.event;
//
//import com.viettel.vsm.model.FileEntry;
//import com.viettel.util.FileUtil;
//import com.viettel.util.StaticUtil;
//import com.viettel.util.StringPool;
//import com.viettel.util.key.LanguageKeys;
//import java.io.File;
//import org.zkoss.util.resource.Labels;
//import org.zkoss.zk.ui.event.Event;
//import org.zkoss.zk.ui.event.EventListener;
//import org.zkoss.zul.Messagebox;
//
///**
// *
// * @author linhlh2
// */
//public class DownloadFileEntryListener implements EventListener<Event>{
//
//    private FileEntry entry;
//
//    public DownloadFileEntryListener(FileEntry entry) {
//        this.entry = entry;
//    }
//
//    public void onEvent(Event t) throws Exception {
//        StringBuilder sb = new StringBuilder();
//
//        sb.append(StaticUtil.getSystemStoreDir());
//        sb.append(StringPool.SLASH);
//        sb.append(StaticUtil.getFileUploadDir());
//        sb.append(StringPool.SLASH);
//        sb.append(entry.getFolderId());
//        sb.append(StringPool.SLASH);
//        sb.append(entry.getFileId());
//
//        File file = new File(sb.toString());
//
//        if (file.exists()) {
//            FileUtil.download(file, entry.getName());
//        } else {
//            Messagebox.show(Labels.getLabel(
//                    LanguageKeys.FILE_NOT_FOUND),
//                    Labels.getLabel(LanguageKeys.ERROR),
//                    Messagebox.OK, Messagebox.ERROR);
//        }
//    }
//
//}
