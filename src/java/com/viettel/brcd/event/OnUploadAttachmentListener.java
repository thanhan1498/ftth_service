/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.event;

import com.viettel.brcd.util.ComponentUtil;
import com.viettel.brcd.util.FileUtil;
import com.viettel.brcd.util.StaticUtil;
import com.viettel.brcd.util.StringPool;
import com.viettel.brcd.util.Validator;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.util.key.LanguageKeys;
import java.io.IOException;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.A;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;

/**
 *
 * @author linhlh2
 */
public class OnUploadAttachmentListener implements EventListener<UploadEvent> {

    private List<Media> medium;
    private Div divFileList;

    public OnUploadAttachmentListener(List<Media> medium, Div divFileList) {
        this.medium = medium;
        this.divFileList = divFileList;
    }

    public void onEvent(UploadEvent event) throws Exception {
        Media[] medias = event.getMedias();

        Grid grid = ComponentUtil.createGrid(StringPool.BLANK,
                Constants.CLASS_NO_STYLE);

        grid.appendChild(ComponentUtil.createColumns(
                new String[]{"90%", "10%"}));

        grid.setModel(new ListModelList<Media>(medias));
        grid.setRowRenderer(new RowRenderer<Media>() {

            public void render(final Row row, final Media media, int index)
                    throws Exception {
                String name = media.getName();
                String extension = media.getFormat();
                int size = getSize(media);

                if(size == 0){
                    Messagebox.show(Labels.getLabel(
                            LanguageKeys.MESSAGE_FILE_IS_EMPTY),
                            Labels.getLabel(LanguageKeys.ERROR),
                            Messagebox.OK, Messagebox.ERROR);

                    row.getParent().removeChild(row);

                    return;
                }

                if (Validator.isNull(extension)
                        || !isInstanceValidFileExtension(extension)) {
                    Messagebox.show(Labels.getLabel(
                            LanguageKeys.MESSAGE_INVALID_FILE_UPLOAD_EXTENSION,
                            new String[]{StaticUtil.getAttachAllowExts()}),
                            Labels.getLabel(LanguageKeys.ERROR),
                            Messagebox.OK, Messagebox.ERROR);

                    row.getParent().removeChild(row);

                    return;
                }

                if (!isInstanceValidMaxSize(size)) {
                    Messagebox.show(Labels.getLabel(
                            LanguageKeys.MESSAGE_INVALID_FILE_MAX_SIZE,
                            new Long[]{getInstanceValidMaxSizeMB()}),
                            Labels.getLabel(LanguageKeys.ERROR),
                            Messagebox.OK, Messagebox.ERROR);

                    row.getParent().removeChild(row);

                    return;
                }

                row.appendChild(new Label(name));

                A rm = new A();

                rm.setImage(ComponentUtil.DELETE_ICON);

                rm.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

                    public void onEvent(Event event) throws Exception {
                        medium.remove(media);

                        row.getParent().removeChild(row);
                    }
                });

                row.appendChild(rm);

                row.setStyle(Constants.STYLE_NO_PADDING);

                medium.add(media);

            }
        });

        divFileList.appendChild(grid);
    }

    private boolean isInstanceValidFileExtension(String extension) {
        String[] exts = StaticUtil.getAttachAllowExt();

        return FileUtil.isValidFileExtension(extension, exts);
    }

    private boolean isInstanceValidMaxSize(int size) {
        Long maxSize = StaticUtil.getAttachMaxSize();

        return FileUtil.isValidMaxSize(size, maxSize);
    }

    private Long getInstanceValidMaxSizeMB() {
        Long maxSize = StaticUtil.getAttachMaxSize();

        return FileUtil.getMegabyte(maxSize);
    }

    private int getSize(Media media) {
        int size = 0;

        try {
            if (media.isBinary()) {
                size = media.getByteData().length;
            } else {

                size = IOUtils.toByteArray(media.getReaderData()).length;

            }
        } catch (IOException ex) {
        } finally {
            return size;
        }
    }
}
