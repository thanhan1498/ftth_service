/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.util;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import org.zkoss.util.Locales;
import org.zkoss.util.resource.Labels;

/**
 *
 * @author linhlh2
 */
public class LabelUtil {
    
    public static String validateLocale(String locale) {
        if (locale == null) {
            return LabelUtil.getKey("language.code.invalid", locale);
        }
        locale = locale.trim();
        if (locale.isEmpty()) {
            return LabelUtil.getKey("language.code.invalid", locale);
        }
        String[] locales = StaticUtil.getLocales();
        if (locales == null) {
            return LabelUtil.getKey("language.code.invalid", locale);
        }
        List<String> _locales = Arrays.asList(locales);
        if (!_locales.contains(locale)) {
            return LabelUtil.getKey("language.code.invalid", locale);
        }
        return null;
    }

    public static Locale getLocale(String locale) {
        String[] locales = StaticUtil.getLocales();

        List<String> _locales = Arrays.asList(locales);

        if (Validator.isNotNull(locale)
                && _locales.contains(locale)) {
            return new Locale(locale.substring(0, 2), locale.substring(3));
        }

        return new Locale("en", "US");
    }

    public static String getKey(String key, String locale) {
        String[] locales = StaticUtil.getLocales();

        List<String> _locales = Arrays.asList(locales);

        if (_locales.contains(locale)) {
            Locale prefer_locale = new Locale(locale.substring(0, 2), locale.substring(3));

            Locales.setThreadLocal(prefer_locale);
        }

        return Labels.getLabel(key);
    }

    public static String getKey(String key, Object[] args, String locale) {
        String[] locales = StaticUtil.getLocales();

        List<String> _locales = Arrays.asList(locales);

        if (_locales.contains(locale)) {
            Locale prefer_locale = new Locale(locale.substring(0, 2), locale.substring(3));

            Locales.setThreadLocal(prefer_locale);
        }

        return Labels.getLabel(key, args);
    }

    public static String formatKey(String key, String locale, Object... args) {
        String[] locales = StaticUtil.getLocales();

        List<String> _locales = Arrays.asList(locales);

        if (_locales.contains(locale)) {
            Locale prefer_locale = new Locale(locale.substring(0, 2), locale.substring(3));

            Locales.setThreadLocal(prefer_locale);
        }

        return Labels.getLabel(key, args);
    }

    public static String getKey(String key, String locale, String... args) {
        String[] locales = StaticUtil.getLocales();

        List<String> _locales = Arrays.asList(locales);

        if (_locales.contains(locale)) {
            Locale prefer_locale = new Locale(locale.substring(0, 2), locale.substring(3));

            Locales.setThreadLocal(prefer_locale);
        }

        String[] argKeys = new String[]{};
        if (args != null) {
            int n = args.length;
            argKeys = new String[n];
            for (int i = 0; i < n; i++) {
                String s = args[i];
                argKeys[i] = Labels.getLabel(s);
            }
        }

        return Labels.getLabel(key, argKeys);
    }
}
