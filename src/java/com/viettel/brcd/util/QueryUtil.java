/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.util;

import java.lang.reflect.Field;
import org.apache.commons.lang.StringEscapeUtils;
import org.hibernate.criterion.MatchMode;

/**
 *
 * @author linhlh2
 */
public class QueryUtil {

    public static final Long STATUS_DEACTIVE = 0L;
    public static final Long STATUS_ACTIVE = 1L;
    public static final Long ALL = -1L;
    public static final int FIRST_INDEX = 0;
    public static final int GET_ALL = -1;

    public static String addOrder(Class c, String orderByColumn,
            String orderByType) {
        StringBuilder sb = new StringBuilder();

        if (hasProperty(c, orderByColumn)) {
            sb.append(" order by ");
            sb.append(orderByColumn);
            sb.append(StringPool.SPACE);
            sb.append(orderByType);
        }

        return sb.toString();
    }

    public static String getFullStringParam(String param) {
        StringBuilder sb = new StringBuilder(5);

        sb.append(StringPool.PERCENT);
        sb.append(_replaceSpecialCharacter(param));
        sb.append(StringPool.PERCENT);

        return sb.toString();
    }
     public static String getFullStringParamLower(String param) {
        StringBuilder sb = new StringBuilder(5);

        sb.append(StringPool.PERCENT);
        sb.append(_replaceSpecialCharacter(param.toLowerCase()));
        sb.append(StringPool.PERCENT);
        

        return sb.toString();
    }

    public static String getLeftStringParam(String param) {
        StringBuilder sb = new StringBuilder(2);

        sb.append(StringPool.PERCENT);
        sb.append(_replaceSpecialCharacter(param));


        return sb.toString();
    }

    public static String getRightStringParam(String param) {
        StringBuilder sb = new StringBuilder(2);

        sb.append(_replaceSpecialCharacter(param));
        sb.append(StringPool.PERCENT);

        return sb.toString();
    }

    //LinhLH2 fix
    private static String _replaceSpecialCharacter(String param) {
            return param.replaceAll("%", "\\\\%");
//                    .replaceAll("_", "\\\\_");
    }

    private static boolean hasProperty(Class c, String name) {
        boolean has = false;

        try {
            Field field = c.getDeclaredField(name);

            if (Validator.isNotNull(field)) {
                has = true;
            }
        } catch (Exception e) {
        }

        return has;
    }
}
