/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.util;

import com.viettel.brcd.util.key.PropsKeys;


/**
 *
 * @author linhlh2
 */
public class StaticUtil {
    public static String getSystemStoreDir() {
        return PropsUtil.get(PropsKeys.SYSTEM_STORE_FILE_DIR);
    }

    public static Long getAttachMaxSize() {
        return GetterUtil.getLong(PropsUtil.get(
                PropsKeys.ATTACH_FILE_UPLOAD_ALLOW_MAX_SIZE));
    }

    public static String getTmpDir() {
        return PropsUtil.get(PropsKeys.TEMP_DIR);
    }

    public static String[] getAttachAllowExt() {
        return PropsUtil.getArray(
                PropsKeys.ATTACH_FILE_UPLOAD_ALLOW_EXTENSION);
    }

    public static String getAttachAllowExts() {
        return PropsUtil.get(
                PropsKeys.ATTACH_FILE_UPLOAD_ALLOW_EXTENSION);
    }
    
    public static String[] getLocales(){
        return PropsUtil.getArray(PropsKeys.LOCALE);
    }
}
