/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.util.key;

/**
 *
 * @author linhlh2
 */
public class LanguageKeys {

    /**
     * Language
     */
    public static final String VI_VN = "vi_VN";

    /**
     * Common name
     */
    public static final String ERROR = "common.error";

    public static final String NO_RECORD_FOUNND  = "common.no.record.found";
    public static final String EDIT = "common.edit";
    public static final String ADD = "common.add";
    public static final String COLUMN_NUMBER = "common.column.number";
    public static final String OPTION = "common.option";
    /**
     * Name
     */
    public static final String MENU_MAIN = "menu.main";
    public static final String MENU_CONTROL_PANEl = "menu.control.panel";
    public static final String MENU_ITEM_CATEGORY_MANAGEMENT =
            "menu.item.category.management";
//    public static final String MENU_ITEM_DATABASE_MANAGEMENT =
//            "menu.item.database.management";

    /**
     * Title
     */
    public static final String TITLE_ADD = "title.add";
    public static final String TITLE_EDIT = "title.edit";
    public static final String TITLE_EDIT_CATEGORY = "title.edit.category";
    public static final String TITLE_ADD_TABLE = "title.add.table";
    public static final String TITLE_EDIT_TABLE = "title.edit.table";


    /**
     * Button
     */
    public static final String BUTTON_DELETE = "button.delete";
    public static final String BUTTON_SAVE = "button.save";
    public static final String BUTTON_CANCEL = "button.cancel";
    public static final String BUTTON_EDIT = "button.update";
    public static final String BUTTON_LOCK = "button.lock";
    public static final String BUTTON_UNLOCK = "button.unlock";
    public static final String BUTTON_RIGHT = "button.right";
    public static final String BUTTON_PUBLIC = "button.public";
    public static final String BUTTON_PENDING = "button.pending";
    public static final String BUTTON_RESOURCE = "button.resource";
    public static final String BUTTON_ATTACH = "button.attach";

    /**
     * Status
     */
    public static final String STATUS_ACTIVE = "status.active";
    public static final String STATUS_LOCK = "status.lock";
    public static final String STATUS_NOT_ACTIVE = "status.not.active";

    /**
     * Message
     */
    public static final String MESSAGE_INSERT_FAIL =
            "message.insert.fail";
    public static final String MESSAGE_UPDATE_FAIL =
            "message.update.fail";
    public static final String MESSAGE_INSERT_SUCCESS =
            "message.insert.success";
    public static final String MESSAGE_UPDATE_SUCCESS =
            "message.update.success";
    public static final String MESSAGE_DELETE_SUCCESS =
            "message.delete.success";
    public static final String MESSAGE_DELETE_FAIL =
            "message.delete.fail";
    public static final String MESSAGE_LOCK_ITEM_SUCCESS =
            "message.lock.item.success";
    public static final String MESSAGE_LOCK_ITEM_FAIL =
            "message.lock.item.fail";
    public static final String MESSAGE_UNLOCK_ITEM_SUCCESS =
            "message.unlock.item.success";
    public static final String MESSAGE_UNLOCK_ITEM_FAIL =
            "message.unlock.item.fail";
    public static final String MESSAGE_ACTIVATE_SUCCESS =
            "message.activate.success";
    public static final String MESSAGE_ACTIVATE_FAIL =
            "message.activate.fail";
    public static final String MESSAGE_SELECT_RECORD =
            "message.select.record";
    public static final String MESSAGE_QUESTION_DELETE =
            "message.question.delete";
    public static final String MESSAGE_QUESTION_LOCK =
            "message.question.lock";
    public static final String MESSAGE_QUESTION_UNLOCK =
            "message.question.unlock";
    public static final String MESSAGE_QUESTION_PUBLIC =
            "message.question.public";
    public static final String MESSAGE_QUESTION_PENDING =
            "message.question.pending";
    public static final String MESSAGE_QUESTION_RESET =
            "message.question.reset";
    public static final String MESSAGE_QUESTION_ACTIVATE =
            "message.question.activate";
    public static final String MESSAGE_QUESTION_UPDATE =
            "message.question.update";
    public static final String MESSAGE_QUESTION_ADD =
            "message.question.add";
    public static final String MESSAGE_QUESTION_CHANGE_USERS_DEPT =
            "message.question.change.users.dept";
    public static final String MESSAGE_INFOR_DELETE =
            "message.infor.delete";
    public static final String MESSAGE_INFOR_LOCK =
            "message.infor.lock";
    public static final String MESSAGE_INFOR_UNLOCK =
            "message.infor.unlock";
    public static final String MESSAGE_INFOR_PUBLIC =
            "message.infor.public";
    public static final String MESSAGE_INFOR_PENDING =
            "message.infor.pending";
    public static final String MESSAGE_INFOR_WARNING =
            "message.infor.warning";
    public static final String MESSAGE_INFOR_RESET =
            "message.infor.reset";
    public static final String MESSAGE_INFOR_ACTIVATE =
            "message.infor.activate";
    public static final String MESSAGE_INFOR_ADD =
            "message.infor.add";
    public static final String MES_CHOOSE_KEY_PARENT =
            "mes.choose.key.parent";
    public static final String MESSAGE_MUST_BE_POSITIVE =
            "message.must.be.positive";
    public static final String MESSAGE_MAX_LENGTH_INVALID =
            "message.max.length.invalid";
    public static final String MESSAGE_MIN_LENGTH_INVALID =
            "message.min.length.invalid";
    public static final String MESSAGE_FORMAT_INVALID =
            "message.format.invalid";
    public static final String MESSAGE_VALID_CHARACTER_ONLY =
            "message.valid.character.only";
    public static final String MESSAGE_REQUIRED_INPUT =
            "message.required.input";
    public static final String MESSAGE_REQUIRED_SELECT =
            "message.required.select";
    public static final String MESSAGE_DUPLICATE =
            "message.duplicate";
    public static final String MESSAGE_NO_PROPERTY_WAS_FOUND =
            "message.no.property.was.found";
    public static final String MESSAGE_NO_DOCUMENT_WAS_FOUND =
            "message.no.document.was.found";
    public static final String MESSAGE_FILE_IS_EMPTY =
            "message.file.is.empty";
    public static final String MESSAGE_INVALID_FILE_UPLOAD_EXTENSION =
            "message.invalid.file.upload.extension";
    public static final String MESSAGE_INVALID_FILE_MAX_SIZE =
            "message.invalid.file.max.size";
    public static final String MESSAGE_INVALID_FILE_MAX_SIZE_KB=
            "message.invalid.file.max.size.KB";
    public static final String MESSAGE_IS_NOT_SAME =
            "message.is.not.same";
    public static final String MESSAGE_MUST_CONTAIN =
            "message.must.contain";
    public static final String UPPERCASE_LETTER =
            "common.uppercase.letter";
    public static final String LOWERCASE_LETTER =
            "common.lowercase.letter";
    public static final String DIGIT = "common.digit";
     public static final String MESSAGE_MUST_BE_NUMBER =
            "message.must.be.number";

    /**
     * Other
     */

}
