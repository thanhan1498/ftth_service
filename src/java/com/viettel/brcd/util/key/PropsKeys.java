/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.util.key;

/**
 *
 * @author linhlh2
 */
public class PropsKeys {

    public static final String SYSTEM_STORE_FILE_DIR = 
            "system.store.file.dir";

    public static final String ATTACH_FILE_UPLOAD_ALLOW_MAX_SIZE =
            "attach.file.upload.allow.max.size";

    public static final String TEMP_DIR =
            "temp.dir";
    
    public static final String ATTACH_FILE_UPLOAD_ALLOW_EXTENSION =
            "attach.file.upload.allow.extension";
    public static final String LOCALE =
            "locale";
}
