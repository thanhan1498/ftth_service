/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.brcd.util.key;

/**
 *
 * @author linhlh2
 */
public class ZkKeys {
    public static final String MOLD_3D = "3d";

    public static final String ROUNDED = "rounded";

    public static final String HFLEX_1 = "1";

    public static final String HFLEX_2 = "2";

    public static final String BEFORE_START = "before_start";

    public static final String ON_LOAD_DATA = "onLoadData";

    public static final String ON_LOAD_DATA_AND_REOPEN = "onLoadDataAndReOpen";
}
