package com.viettel.brcd.ws.bccsgw.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "gwOperationResponse", namespace = "http://webservice.bccsgw.viettel.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gwOperationResponse")
public class GwOperationResponse {

    @XmlElement(name = "Result")
    protected Output _return;

    public Output getReturn() {
        return _return;
    }

    public void setReturn(Output _return) {
        this._return = _return;
    }
}
