/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.bccsgw.model;

/**
 *
 * @author vtsoft
 */
public class WebserviceParam {

    private String wsCode;
    private String wsdlUrl;
    private String userName;
    private String password;
    private int recvTimeout;
    private int connectTimeout;

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public int getRecvTimeout() {
        return recvTimeout;
    }

    public void setRecvTimeout(int recvTimeout) {
        this.recvTimeout = recvTimeout;
    }

    public String getWsCode() {
        return wsCode;
    }

    public void setWsCode(String wsCode) {
        this.wsCode = wsCode;
    }

    public String getWsdlUrl() {
        return wsdlUrl;
    }

    public void setWsdlUrl(String wsdlUrl) {
        this.wsdlUrl = wsdlUrl;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public WebserviceParam(String wsCode, String wsdlUrl, String userName, String password, int recvTimeout, int connectTimeout) {
        this.wsCode = wsCode;
        this.wsdlUrl = wsdlUrl;
        this.userName = userName;
        this.password = password;
        this.recvTimeout = recvTimeout;
        this.connectTimeout = connectTimeout;
    }
}
