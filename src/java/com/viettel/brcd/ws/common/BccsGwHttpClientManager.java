/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.common;

/**
 *
 * @author thanhld8
 */
public class BccsGwHttpClientManager extends HttpClientManager {
    
    private static int SIZE = 5;
    private static int MAX_CONNECTIONS_PER_HOST = 20000;
    private static int MAX_TOTAL_CONNECTIONS = 20000;
    private static BccsGwHttpClientManager uniqueInstance = new BccsGwHttpClientManager();
    
    public BccsGwHttpClientManager() {
        super(SIZE, MAX_CONNECTIONS_PER_HOST, MAX_TOTAL_CONNECTIONS);
    }
    
    public static BccsGwHttpClientManager getInstantce() {
        return uniqueInstance;
    }
}
