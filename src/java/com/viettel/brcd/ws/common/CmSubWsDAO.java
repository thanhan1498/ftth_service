/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.common;

import org.apache.log4j.Logger;
import com.viettel.bccs.cm.dao.BaseDAO;
import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

/**
 *
 * @author kdvt_tuanbn2
 */
public class CmSubWsDAO {

    private static Logger log = Logger.getLogger(CmSubWsDAO.class);

    public static void updateValidSub(Long subId, Session cmSession) {
        try {
            boolean isUpdate = isExistValidSub(subId, cmSession);
            if (isUpdate) {
                String sql = "Update cm_pos2.cm_sub_ws set valid=2 where status=1 and valid=0 and sub_id=?";
                SQLQuery query = cmSession.createSQLQuery(sql);
                query.setParameter(0, subId);
                query.executeUpdate();
            }
        } catch (Exception ex) {
            new BaseDAO().error(log, "Error", ex);
        }
    }

    public static void updateValidSub(String account, Session cmSession) {
        try {
            String sql = "Update cm_pos2.cm_sub_ws set valid=2 where status=1 and valid=0 and account=?";
            SQLQuery query = cmSession.createSQLQuery(sql);
            query.setParameter(0, account);
            query.executeUpdate();
        } catch (Exception e) {
            new BaseDAO().error(log, "Error", e);
            //ex.printStackTrace();
        }
    }

    public static boolean isExistValidSub(Long subId, Session cmSession) {
        boolean retVal = false;
        try {
            String sql = "select sub_id from  cm_pos2.cm_sub_ws  where status=1 and valid=0 and sub_id=?";
            SQLQuery query = cmSession.createSQLQuery(sql);
            query.setParameter(0, subId);
            List list = query.list();
            retVal = list != null && list.size() > 0;
        } catch (Exception e) {
            new BaseDAO().error(log, "Error", e);
            //ex.printStackTrace();
        }
        return retVal;
    }

    //Cập nhật status theo account
    public static void updateStatusSub(String account, Session cmSession) {
        try {
            String sql = "Update cm_pos2.cm_sub_ws set valid=1 where status=1 and valid=0 and account=?";
            SQLQuery query = cmSession.createSQLQuery(sql);
            query.setParameter(0, account);
            query.executeUpdate();
        } catch (Exception e) {
            new BaseDAO().error(log, "Error", e);
            //ex.printStackTrace();
        }
    }
}
