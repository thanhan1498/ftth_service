/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.common;

import com.viettel.bccs.cm.bussiness.ApParamBussiness;
import com.viettel.bccs.cm.bussiness.common.DecryptBusiness;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.util.Constants;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.security.KeyFactory;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;
import javax.crypto.Cipher;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.apache.commons.codec.binary.Base64;
import org.hibernate.Session;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 *
 * @author HuyNQ36
 */
public class CommonWebservice {

    /**
     *
     * @param object
     * @return
     * @throws Exception
     */
    public static String marshal(Object object) throws Exception {

        if (object == null) {
            return "";
        }

        StringWriter sw = new StringWriter();
        JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
        Marshaller unmarshaller = jaxbContext.createMarshaller();
        unmarshaller.marshal(object, sw);

        return sw.toString().replaceAll("<\\?xml version=\"1\\.0\" encoding=\"UTF-8\" standalone=\"yes\"\\?>", "");
    }

    /**
     *
     * @param responseBody
     * @param aClass
     * @return
     * @throws Exception
     */
    public static Object unmarshal(String responseBody, Class aClass) throws Exception {
        JAXBContext jaxbContext = JAXBContext.newInstance(aClass);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return unmarshaller.unmarshal(new StringReader(responseBody));
    }

    public static String decryptData(Session cmPos, String data) {
        try {
            List<ApParam> apparam = new ApParamBussiness().findByType(cmPos, Constants.PARAM_TYPE_SHOP_KEY_AES);
            String dataDe = DecryptBusiness.decrypt(apparam.get(0).getParamValue(), data);
            return dataDe;
        } catch (Exception ex) {
        }
        return null;
    }

    public static String encryptData(Session cmPos, String data) {
        try {
            List<ApParam> apparam = new ApParamBussiness().findByType(cmPos, Constants.PARAM_TYPE_SHOP_KEY_AES);
            String dataDe = DecryptBusiness.encrypt(apparam.get(0).getParamValue(), data);
            return dataDe;
        } catch (Exception ex) {
        }
        return null;
    }

    public static String Decrypt(String dataEncrypted, String privateKey)
            throws Exception {
        RSAPrivateKey _privateKey = LoadPrivateKey(privateKey);
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

        cipher.init(2, _privateKey);

        dataEncrypted = dataEncrypted.replace("\r", "");
        dataEncrypted = dataEncrypted.replace("\n", "");
        int dwKeySize = _privateKey.getModulus().bitLength();
        int base64BlockSize = dwKeySize / 8 % 3 != 0 ? dwKeySize / 8 / 3 * 4 + 4 : dwKeySize / 8 / 3 * 4;

        int iterations = dataEncrypted.length() / base64BlockSize;
        ByteBuffer bb = ByteBuffer.allocate(100000);
        for (int i = 0; i < iterations; i++) {
            String sTemp = dataEncrypted.substring(base64BlockSize * i, base64BlockSize * i + base64BlockSize);

            byte[] bTemp = decodeBase64(sTemp);

            bTemp = reverse(bTemp);
            byte[] encryptedBytes = cipher.doFinal(bTemp);
            bb.put(encryptedBytes);
        }
        byte[] bDecrypted = bb.array();
        return new String(bDecrypted).trim();
    }

    private static RSAPrivateKey LoadPrivateKey(String key) throws Exception {
        String sReadFile = key.trim();
        if ((sReadFile.startsWith("-----BEGIN PRIVATE KEY-----")) && (sReadFile.endsWith("-----END PRIVATE KEY-----"))) {
            sReadFile = sReadFile.replace("-----BEGIN PRIVATE KEY-----", "");
            sReadFile = sReadFile.replace("-----END PRIVATE KEY-----", "");
            sReadFile = sReadFile.replace("\n", "");
            sReadFile = sReadFile.replace("\r", "");
            sReadFile = sReadFile.replace(" ", "");
        }

        byte[] b = decodeBase64(sReadFile);

        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(b);

        KeyFactory factory = KeyFactory.getInstance("RSA");
        return (RSAPrivateKey) factory.generatePrivate(spec);
    }

    private static byte[] decodeBase64(String dataToDecode) {
        BASE64Decoder b64d = new BASE64Decoder();
        byte[] bDecoded = (byte[]) null;
        try {
            bDecoded = b64d.decodeBuffer(dataToDecode);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bDecoded;
    }

    private static byte[] reverse(byte[] b) {
        int left = 0;
        int right = b.length - 1;

        while (left < right) {
            byte temp = b[left];
            b[left] = b[right];
            b[right] = temp;

            left++;
            right--;
        }
        return b;
    }

    public static String Encrypt(String dataToEncrypt, String pubCer)
            throws Exception {
        RSAPublicKey _publicKey = LoadPublicKey(pubCer);
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

        cipher.init(1, _publicKey);

        int keySize = _publicKey.getModulus().bitLength() / 8;
        int maxLength = keySize - 42;

        byte[] bytes = dataToEncrypt.getBytes();

        int dataLength = bytes.length;
        int iterations = dataLength / maxLength;
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i <= iterations; i++) {
            byte[] tempBytes = new byte[dataLength - maxLength * i > maxLength ? maxLength : dataLength - maxLength * i];

            System.arraycopy(bytes, maxLength * i, tempBytes, 0, tempBytes.length);

            byte[] encryptedBytes = cipher.doFinal(tempBytes);

            encryptedBytes = reverse(encryptedBytes);
            sb.append(encodeBase64(encryptedBytes));
        }

        String sEncrypted = sb.toString();
        sEncrypted = sEncrypted.replace("\r", "");
        sEncrypted = sEncrypted.replace("\n", "");
        return sEncrypted;
    }

    private static RSAPublicKey LoadPublicKey(String pubCer) throws Exception {
        RSAPublicKey publicKey = null;
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(decodeBase64(pubCer));
            publicKey = (RSAPublicKey) keyFactory.generatePublic(publicKeySpec);
        } catch (Exception e) {
        }
        return publicKey;
    }

    private static String encodeBase64(byte[] dataToEncode) {
        BASE64Encoder b64e = new BASE64Encoder();
        String strEncoded = "";
        try {
            strEncoded = b64e.encode(dataToEncode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strEncoded;
    }

    public static boolean Verify(String dataToVerify, String signedData, String pubCer)
            throws Exception {
        RSAPublicKey _publicKey = LoadPublicKey(pubCer);
        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initVerify(_publicKey);
        signature.update(dataToVerify.getBytes(), 0, dataToVerify.getBytes().length);

        byte[] bSign = decodeBase64(signedData);
        boolean pass = signature.verify(bSign);

        return pass;
    }

    public static String Sign(String dataToSign, String privateKey)
            throws Exception {
        RSAPrivateKey _privateKey = LoadPrivateKey(privateKey);
        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initSign(_privateKey);
        signature.update(dataToSign.getBytes());

        byte[] bSigned = signature.sign();

        String sResult = encodeBase64(bSigned);

        return sResult;
    }
}
