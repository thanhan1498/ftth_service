/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.common;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.viettel.brcd.ws.model.output.EmoneyResponse;
import java.util.Map;
import javax.ws.rs.core.MediaType;
import java.io.FileInputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
/**
 *
 * @author duyetdk
 */
public class EmoneyPaymentClient {
    
    private static Logger LOGGER = Logger.getLogger(EmoneyPaymentClient.class);
    private static final ClientConfig CONFIG = new DefaultClientConfig();
    private Client client;

    private static EmoneyPaymentClient instance;

    public EmoneyPaymentClient() {
    }
    
    public static EmoneyPaymentClient shared() {
        if (instance == null) {
            instance = new EmoneyPaymentClient();
        }
        return instance;
    }
    
    /**
     * 
     * @param fullUrl
     * @param headers
     * @param jsonParam
     * @return 
     */
    public EmoneyResponse postRequest(String fullUrl, Map<String, String> headers, String jsonParam) {
        EmoneyResponse response = null;
        try {
            response = new EmoneyResponse();
            client = Client.create(CONFIG);
            WebResource webResource = client.resource(fullUrl);
            
            WebResource.Builder clientBuilder = webResource.type(MediaType.APPLICATION_JSON);
            if (headers != null && !headers.isEmpty()) {
                for (Map.Entry<String, String> hd : headers.entrySet()) {
                    String key = hd.getKey();
                    String value = hd.getValue();
                    clientBuilder.header(key, value);
                }
            }
            ClientResponse clientResponse =  clientBuilder.post(ClientResponse.class, jsonParam);
            
            LOGGER.info("####EMONEY HTTP STATUS: " + clientResponse.getStatus());
            if (clientResponse.getStatus() != 200) {
                response.setHttpStatus(clientResponse.getStatus());
                return response;
            }
            
            String res = clientResponse.getEntity(String.class);
            response.setHttpStatus(clientResponse.getStatus());
            response.setJsonResponse(res);
            
        } catch (Exception ex) {
             LOGGER.info(ex);
        }
        return response;
    }
    
    /**
     * 
     * @param fullUrl
     * @param headers
     * @param jsonParam
     * @return 
     */
    public EmoneyResponse getRequest(String fullUrl, Map<String, String> headers, String jsonParam) {
        EmoneyResponse response = null;
        try {
            response = new EmoneyResponse();
            client = Client.create(CONFIG);
            WebResource webResource = client.resource(fullUrl);
            
            WebResource.Builder clientBuilder = webResource.type(MediaType.APPLICATION_JSON);
            if (headers != null && !headers.isEmpty()) {
                for (Map.Entry<String, String> hd : headers.entrySet()) {
                    String key = hd.getKey();
                    String value = hd.getValue();
                    clientBuilder.header(key, value);
                }
            }
            ClientResponse clientResponse =  clientBuilder.get(ClientResponse.class);
            
            LOGGER.info("####EMONEY HTTP STATUS: " + clientResponse.getStatus());
            if (clientResponse.getStatus() != 200) {
                response.setHttpStatus(clientResponse.getStatus());
                return response;
            }
            
            String res = clientResponse.getEntity(String.class);
            response.setHttpStatus(clientResponse.getStatus());
            response.setJsonResponse(res);
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return response;
    }
    
    public static String encryptRSA(String plainData, String publicKeyFilePath) {
        try {
            FileInputStream fis = new FileInputStream(publicKeyFilePath);
            byte[] byteKeyFromFile = new byte[fis.available()];
            fis.read(byteKeyFromFile);
            fis.close();

            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(byteKeyFromFile);
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PublicKey pubKey = factory.generatePublic(keySpec);

            // Mã hoá dữ liệu
            Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            c.init(Cipher.ENCRYPT_MODE, pubKey);
            byte encryptedByte[] = c.doFinal(plainData.getBytes());
            String encrypted = Base64.encodeBase64String(encryptedByte);
            return encrypted;
        } catch (Exception ex) {
             ex.printStackTrace();
        }
        return null;
    }
    
    public static String decryptRSA(String encryptedData, String privateKeyFilePath) {
        try {
            FileInputStream fis = new FileInputStream(privateKeyFilePath);
            byte[] byteKeyFromFile = new byte[fis.available()];
            fis.read(byteKeyFromFile);
            fis.close();
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(byteKeyFromFile);
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PrivateKey priKey = factory.generatePrivate(keySpec);
            // Giải mã dữ liệu
            Cipher c2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            c2.init(Cipher.DECRYPT_MODE, priKey);
            String decrypted = new String(c2.doFinal(Base64.decodeBase64(encryptedData)));
            return decrypted;
        } catch (Exception ex) {
             ex.printStackTrace();
        }
        return null;
    }
}
