/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.common;

import com.viettel.bccs.cm.dao.BaseDAO;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.log4j.Logger;

/**
 *
 * @author TuanNS3_S
 */
public class HttpClientManager {

    private static Logger log = Logger.getLogger(HttpClientManager.class);
    private List<HttpClient> listHttpclient;
    private int channelCount = 0;
    public static Long CALL_ID_CLIENT = 0L;

    public HttpClientManager(int size, int maxConnectionsPerHost, int maxTotalConnections) {
        Long callTest = CALL_ID_CLIENT++;
        new BaseDAO().info(log, callTest + "= Call_id HttpClientManager size" + size + " maxConnectionsPerHost " + maxConnectionsPerHost + " maxTotalConnections " + maxTotalConnections);
        listHttpclient = new ArrayList<HttpClient>();
        for (int i = 0; i < size; i++) {
            //init httpclient
            HttpClient httpclient = new HttpClient(new MultiThreadedHttpConnectionManager());
            httpclient.getHttpConnectionManager().getParams().setDefaultMaxConnectionsPerHost(maxConnectionsPerHost);
            httpclient.getHttpConnectionManager().getParams().setMaxTotalConnections(maxTotalConnections);
            listHttpclient.add(httpclient);
        }
        new BaseDAO().info(log, callTest + "= Call_id HttpClientManager size" + size + " maxConnectionsPerHost " + maxConnectionsPerHost + " maxTotalConnections " + maxTotalConnections);
    }

    public HttpClientManager(int size) {
        Long callTest = CALL_ID_CLIENT++;
        new BaseDAO().info(log, callTest + "= Call_id HttpClientManager size = " + size);
        listHttpclient = new ArrayList<HttpClient>();
        for (int i = 0; i < size; i++) {
            //init httpclient
            HttpClient httpclient = new HttpClient(new MultiThreadedHttpConnectionManager());
            listHttpclient.add(httpclient);
        }
        new BaseDAO().info(log, callTest + "= Call_id HttpClientManager size = " + size);
    }

    public HttpClient getChannel() {
        Long callTest = CALL_ID_CLIENT++;
        new BaseDAO().info(log, callTest + "= Call_id getChannel ");
        try {
            if (channelCount > listHttpclient.size() - 1) {
                //Kiem tra exchCount co lon hon so luong chanle trong list khong.
                channelCount = 0;
            }
            //Lay channel ra, tang Count roi return
            HttpClient channel = (HttpClient) listHttpclient.get(channelCount);
            channelCount++;
            new BaseDAO().info(log, callTest + "= Call_id getChannel ");
            return channel;
        } catch (Exception ex) {
            new BaseDAO().error(log, "Get channel in http channel list fails, return Null:.........", ex);
            return null;
        }
    }
}
