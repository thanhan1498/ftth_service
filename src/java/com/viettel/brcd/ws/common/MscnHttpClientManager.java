/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.common;

/**
 *
 * @author thanhld8
 */
public class MscnHttpClientManager extends HttpClientManager {
    
    private static int SIZE = 5;
    private static int MAX_CONNECTIONS_PER_HOST = 20000;
    private static int MAX_TOTAL_CONNECTIONS = 20000;
    private static MscnHttpClientManager uniqueInstance = new MscnHttpClientManager();
    
    public MscnHttpClientManager() {
        super(SIZE, MAX_CONNECTIONS_PER_HOST, MAX_TOTAL_CONNECTIONS);
    }
    
    public static MscnHttpClientManager getInstantce() {
        return uniqueInstance;
    }
}
