/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.common;

import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.brcd.ws.bccsgw.model.GwOperationResponse;
import com.viettel.brcd.ws.bccsgw.model.Input;
import com.viettel.brcd.ws.bccsgw.model.WebserviceParam;
import com.viettel.bccs.cm.model.ActionLogPr;
import com.viettel.brcd.util.Validator;
import com.viettel.brcd.ws.bccsgw.model.Param;
import java.io.StringReader;
import java.util.List;
import java.util.ResourceBundle;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.dom.DOMSource;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;

/**
 *
 * @author tula4
 */
public class WebServiceClient {

    private static final Logger log = Logger.getLogger(WebServiceClient.class);
    private static ResourceBundle resource = ResourceBundle.getBundle("provisioning");
    private static Long CALL_ID = 0L;
//
    protected ActionLogPr actionLogPr = new ActionLogPr();

    public ActionLogPr getActionLogPr() {
        return actionLogPr;
    }

    public void setActionLogPr(ActionLogPr actionLogPr) {
        this.actionLogPr = actionLogPr;
    }

    /**
     *
     * @param requestContent
     * @param wsdlUrl
     * @param recvTimeout
     * @param connectTimeout
     * @return
     * @throws Exception
     */
    public String sendRequest(String requestContent, String wsdlUrl, int recvTimeout, int connectTimeout) throws Exception {
        HttpClient httpclient = new HttpClient();
        httpclient.getParams().setParameter("http.socket.timeout", recvTimeout);
        httpclient.getParams().setParameter("http.connection.timeout", connectTimeout);
        //create an instance PostMethod
        PostMethod post = new PostMethod(wsdlUrl);
        String result = "";
        String responseContent = "";
        try {
            Long call_id = CALL_ID++;
            new BaseDAO().info(log, "CALL_ID: " + call_id + ":" + requestContent);

            RequestEntity entity = new StringRequestEntity(requestContent, "text/xml", "UTF-8");
            post.setRequestEntity(entity);
            httpclient.executeMethod(post);
            responseContent = post.getResponseBodyAsString();
            new BaseDAO().info(log, "CALL_ID: " + call_id + ":" + requestContent + " responseContent : " + responseContent);

            result = parseResult(post.getResponseBodyAsString());
            new BaseDAO().info(log, "CALL_ID: " + call_id + ":requestContent =" + requestContent + " end post.getResponseBodyAsString() : " + post.getResponseBodyAsString());


        } catch (Exception ex) {
            this.actionLogPr.setException(ex.getMessage());
            throw ex;
        } finally {
            this.actionLogPr.setRequest(requestContent);
            this.actionLogPr.setResponse(responseContent);
        }
        return result;
    }

    /**
     *
     * @param requestContent
     * @param wsdlUrl
     * @param recvTimeout
     * @param connectTimeout
     * @return
     * @throws Exception
     */
//    public Object sendRequestViaBccsGW(String requestContent, String wsdlUrl, String userName,
//            String password, String wsCode, int recvTimeout, int connectTimeout, Class responseClass) throws Exception {
//
//        Input input = new Input();
//        input.setUsername(userName);
//        input.setPassword(password);
//        input.setWscode(wsCode);
//
//        input.setRawData(requestContent);
//        String xmlText = CommonWebservice.marshal(input);
//
//        String request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservice.bccsgw.viettel.com/\"> "
//                + "<soapenv:Header/>"
//                + "<soapenv:Body>"
//                + "<web:gwOperation>"
//                + xmlText
//                + "</web:gwOperation>"
//                + "</soapenv:Body>"
//                + "</soapenv:Envelope>";
//
//        String responseBody = sendRequest(request, wsdlUrl, recvTimeout, connectTimeout);
//        GwOperationResponse response = (GwOperationResponse) CommonWebservice.unmarshal(responseBody, GwOperationResponse.class);
//        log.info(response.toString());
//        return CommonWebservice.unmarshal(parseResult(response.getReturn().getOriginal()), responseClass);
//    }
    /**
     *
     * @param requestContent
     * @param methodCode
     * @param responseClass
     * @return
     * @throws Exception
     */
    public Object sendRequestViaBccsGW(String requestContent, String methodCode, Class responseClass) throws Exception {

//        List<ActionLogPr> actionLogPrs = BaseHibernateDAOMDB.getListActionLogPrs();
        WebserviceParam wsParam = getParamByMethodCode(methodCode);
        Object object = sendRequestViaBccsGW(requestContent, wsParam.getWsdlUrl(), wsParam.getUserName(),
                wsParam.getPassword(), wsParam.getWsCode(), wsParam.getRecvTimeout(), wsParam.getConnectTimeout(), responseClass);
//        actionLogPrs.add(actionLogPr);
        return object;
    }
    public Object sendRequestViaBccsGW_Param(String methodCode, Class responseClass,List<Param> lstParam) throws Exception {
        WebserviceParam wsParam = getParamByMethodCode(methodCode);
        Object object = sendRequestViaBccsGW_Param(lstParam, wsParam.getWsdlUrl(), wsParam.getUserName(),
                wsParam.getPassword(), wsParam.getWsCode(), wsParam.getRecvTimeout(), wsParam.getConnectTimeout(), responseClass);
        return object;
    }

    /**
     *
     * @param response
     * @return
     * @throws Exception
     */
    public String parseResult(String response) throws Exception {
        String result = "";
        long currTime = System.currentTimeMillis();
        Long call_id = CALL_ID++;
        try {

            MessageFactory mf = MessageFactory.newInstance();
            // Create a message.  This example works with the SOAPPART.
            SOAPMessage soapMsg = mf.createMessage();
            SOAPPart part = soapMsg.getSOAPPart();
            new BaseDAO().info(log, call_id + "= call_id 1parseResult response =" + response);
            //InputStream is = new ByteArrayInputStream(response.getBytes());
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder builder = dbf.newDocumentBuilder();
            new BaseDAO().info(log, call_id + "= call_id 2parseResult");
            InputSource is = new InputSource(new StringReader(response));
            Document document = builder.parse(is);
            DOMSource domSource = new DOMSource(document);
            part.setContent(domSource);
            new BaseDAO().info(log, call_id + "= call_id 3parseResult");
            SOAPElement resResultSet = (SOAPElement) soapMsg.getSOAPBody().getChildElements().next();
            new BaseDAO().info(log, call_id + "= call_id 4parseResult");
            DOMImplementationLS domImplLS = (DOMImplementationLS) document.getImplementation();
            LSSerializer serializer = domImplLS.createLSSerializer();
            result = serializer.writeToString(resResultSet);
            new BaseDAO().info(log, call_id + "= call_id 5parseResult");
            Long test = (System.currentTimeMillis() - currTime + 500) / 1000;
            if (test > 5) {
                new BaseDAO().info(log, call_id + "= call_id FINISH parseResult CHAM_parseResult response " + response);
            }
        } catch (Exception ex) {
            throw ex;
        }

        return result;
    }

    /**
     *
     * @param methodCode
     * @return
     * @throws Exception
     */
    public WebserviceParam getParamByMethodCode(String methodCode) throws Exception {

        String wsCode = resource.getString(methodCode + "_wsCode");
        String wsdlUrl = resource.getString(methodCode + "_wsdlUrl");
        String userName = resource.getString(methodCode + "_userName");
        String password = resource.getString(methodCode + "_password");
        String recvTimeoutStr = resource.getString(methodCode + "_recvTimeout");
        String connectTimeoutStr = resource.getString(methodCode + "_connectTimeout");
        // <editor-fold defaultstate="collapsed" desc="set cac thuoc tinh default neu khong khai bao">
        if (Validator.isNull(wsCode)) {
            wsCode = methodCode;
        }
        if (Validator.isNull(wsdlUrl)) {
            wsdlUrl = resource.getString("default_wsdlUrl");
        }
        if (Validator.isNull(userName)) {
            userName = resource.getString("default_userName");
        }
        if (Validator.isNull(password)) {
            password = resource.getString("default_password");
        }
        if (Validator.isNull(recvTimeoutStr)) {
            recvTimeoutStr = resource.getString("default_recvTimeout");
        }
        if (Validator.isNull(connectTimeoutStr)) {
            connectTimeoutStr = resource.getString("default_connectTimeout");
        }
        // </editor-fold>
        Integer recvTimeout = Integer.valueOf(recvTimeoutStr);
        Integer connectTimeout = Integer.valueOf(connectTimeoutStr);

        return new WebserviceParam(wsCode, wsdlUrl, userName, password, recvTimeout, connectTimeout);
    }

    /**
     *
     * @param requestContent
     * @param wsdlUrl
     * @param userName
     * @param password
     * @param wsCode
     * @param recvTimeout
     * @param connectTimeout
     * @param responseClass
     * @return
     * @throws Exception
     */
    public Object sendRequestViaBccsGW(String requestContent, String wsdlUrl, String userName,
            String password, String wsCode, int recvTimeout, int connectTimeout, Class responseClass) throws Exception {

        Input input = new Input();
        input.setUsername(userName);
        input.setPassword(password);
        input.setWscode(wsCode);

        input.setRawData(requestContent);
        String xmlText = CommonWebservice.marshal(input);

        String request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservice.bccsgw.viettel.com/\"> "
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<web:gwOperation>"
                + xmlText
                + "</web:gwOperation>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";
        long currTime = System.currentTimeMillis();
        new BaseDAO().info(log, "currTime:" + currTime + ";REUEST:wsCode:" + wsCode + ":" + request);
        String responseBody = null;
        try {
            responseBody = sendRequest(request, wsdlUrl, recvTimeout, connectTimeout, BccsGwHttpClientManager.getInstantce().getChannel());
        } catch (Exception e) {
            new BaseDAO().error(log, "currTime:" + currTime + ";RESPON:wsCode:" + wsCode + ":" + " exception: ", e);
            Long test = (System.currentTimeMillis() - currTime + 500) / 1000;
            if (test > 10) {
                log.info(" FINISH sendRequest:" + "wsCode:" + wsCode + ":" + request + " duration = " + test + " CHAM_WS");
            }
            throw e;
        }
        new BaseDAO().info(log, "currTime:" + currTime + ";RESPON:wsCode:" + wsCode + ":" + responseBody);
        Long test = (System.currentTimeMillis() - currTime + 500) / 1000;
        if (test > 10) {
            new BaseDAO().info(log, " FINISH sendRequest:" + "wsCode:" + wsCode + ":" + request + " duration = " + test + " CHAM_WS");
        }
        GwOperationResponse response = (GwOperationResponse) CommonWebservice.unmarshal(responseBody, GwOperationResponse.class);
        return CommonWebservice.unmarshal(parseResult(response.getReturn().getOriginal()), responseClass);
    }
    
     /**
     *
     * @param requestContent
     * @param wsdlUrl
     * @param recvTimeout
     * @param connectTimeout
     * @return
     * @throws Exception
     */
    public Object sendRequestViaBccsGW_Param(List<Param>lstparam, String wsdlUrl, String userName,
            String password, String wsCode, int recvTimeout, int connectTimeout, Class responseClass) throws Exception {

        Input input = new Input();
        input.setUsername(userName);
        input.setPassword(password);
        input.setWscode(wsCode);
        input.setParam(lstparam);
        String xmlText = CommonWebservice.marshal(input);

        String request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservice.bccsgw.viettel.com/\"> "
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<web:gwOperation>"
                + xmlText
                + "</web:gwOperation>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";
        long currTime = System.currentTimeMillis();
        new BaseDAO().info(log, "currTime:" + currTime + ";REUEST:wsCode:" + wsCode + ":" + request);
        String responseBody = null;
        try {
            responseBody = sendRequest(request, wsdlUrl, recvTimeout, connectTimeout, BccsGwHttpClientManager.getInstantce().getChannel());
        } catch (Exception e) {
            new BaseDAO().error(log, "currTime:" + currTime + ";RESPON:wsCode:" + wsCode + ":" + " exception: ", e);
            Long test = (System.currentTimeMillis() - currTime + 500) / 1000;
            if (test > 10) {
                log.info(" FINISH sendRequest:" + "wsCode:" + wsCode + ":" + request + " duration = " + test + " CHAM_WS");
            }
            throw e;
        }
        new BaseDAO().info(log, "currTime:" + currTime + ";RESPON:wsCode:" + wsCode + ":" + responseBody);
        Long test = (System.currentTimeMillis() - currTime + 500) / 1000;
        if (test > 10) {
            new BaseDAO().info(log, " FINISH sendRequest:" + "wsCode:" + wsCode + ":" + request + " duration = " + test + " CHAM_WS");
        }
        GwOperationResponse response = (GwOperationResponse) CommonWebservice.unmarshal(responseBody, GwOperationResponse.class);
        String rel=parseResult(response.getReturn().getOriginal()); 
        String xmlReturn="<return>" + getTagValue(rel,"return")+ " </return>" ;
        return CommonWebservice.unmarshal(xmlReturn, responseClass);
    }
    public static String getTagValue(String xml, String tagName) {
        return xml.split("<" + tagName + ">")[1].split("</" + tagName + ">")[0];
    }
    
    /**
     *
     * @param requestContent
     * @param wsdlUrl
     * @param recvTimeout
     * @param connectTimeout
     * @return
     * @throws Exception
     */
    public String sendRequest(String requestContent, String wsdlUrl, int recvTimeout, int connectTimeout, HttpClient httpclient) throws Exception {
        //if (httpclient == null) {
            httpclient = new HttpClient();
        //}
        //create an instance PostMethod
        PostMethod post = new PostMethod(wsdlUrl);
        post.getParams().setParameter("http.socket.timeout", recvTimeout);
        post.getParams().setParameter("http.connection.timeout", connectTimeout);
        String result = "";
        String responseContent = "";
        try {
            CALL_ID++;
            new BaseDAO().info(log, "CALL_ID: " + CALL_ID + ":" + requestContent);
            System.out.println("CALL_ID: " + CALL_ID + ":" + requestContent);

            RequestEntity entity = new StringRequestEntity(requestContent, "text/xml", "UTF-8");
            post.setRequestEntity(entity);
            httpclient.executeMethod(post);
            responseContent = post.getResponseBodyAsString();

            new BaseDAO().info(log, "CALL_ID: " + CALL_ID + ":" + requestContent);
            new BaseDAO().info(log, "CALL_ID: " + CALL_ID + ":" + responseContent);
            System.out.println("CALL_ID: " + CALL_ID + ":" + requestContent);
            System.out.println("CALL_ID: " + CALL_ID + ":" + responseContent);

            result = parseResult(responseContent);

        } catch (Exception ex) {
            this.actionLogPr.setException(ex.getMessage());
            throw ex;
        } finally {
            try {
                post.releaseConnection();
                new BaseDAO().info(log, "CALL_ID: " + CALL_ID + ": post.releaseConnection END ");
                this.actionLogPr.setRequest(requestContent);
                this.actionLogPr.setResponse(responseContent);
            } catch (Exception e) {
                new BaseDAO().info(log, "CALL_ID: " + CALL_ID + ": Exception when release connection ", e);
            }
        }
        return result;
    }

    public Object sendRequestViaBccsGWParam(Input input, String wsdlUrl,
            String wsCode, int recvTimeout, int connectTimeout, Class responseClass) throws Exception {
        String xmlText = CommonWebservice.marshal(input);

        String request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservice.bccsgw.viettel.com/\"> "
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<web:gwOperation>"
                + xmlText
                + "</web:gwOperation>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";
        long currTime = System.currentTimeMillis();
        new BaseDAO().info(log, "currTime:" + currTime + ";REUEST:wsCode:" + wsCode + ":" + request);
        String responseBody = null;
        try {
            responseBody = sendRequest(request, wsdlUrl, recvTimeout, connectTimeout, BccsGwHttpClientManager.getInstantce().getChannel());
        } catch (Exception e) {
            new BaseDAO().error(log, "currTime:" + currTime + ";RESPON:wsCode:" + wsCode + ":" + " exception: ", e);
            Long test = (System.currentTimeMillis() - currTime + 500) / 1000;
            if (test > 10) {
                log.info(" FINISH sendRequest:" + "wsCode:" + wsCode + ":" + request + " duration = " + test + " CHAM_WS");
            }
            throw e;
        }
        new BaseDAO().info(log, "currTime:" + currTime + ";RESPON:wsCode:" + wsCode + ":" + responseBody);
        Long test = (System.currentTimeMillis() - currTime + 500) / 1000;
        if (test > 10) {
            new BaseDAO().info(log, " FINISH sendRequest:" + "wsCode:" + wsCode + ":" + request + " duration = " + test + " CHAM_WS");
        }
        GwOperationResponse response = (GwOperationResponse) CommonWebservice.unmarshal(responseBody, GwOperationResponse.class);
        return CommonWebservice.unmarshal(parseResult(response.getReturn().getOriginal()), responseClass);
    }

    public Object sendRequestViaBccsGW(Input input, String methodCode, Class responseClass) throws Exception {

//        List<ActionLogPr> actionLogPrs = BaseHibernateDAOMDB.getListActionLogPrs();
        WebserviceParam wsParam = getParamByMethodCode(methodCode);
        input.setUsername(wsParam.getUserName());
        input.setPassword(wsParam.getPassword());
        input.setWscode(wsParam.getWsCode());
        Object object = sendRequestViaBccsGWParam(input, wsParam.getWsdlUrl(),
                wsParam.getWsCode(), wsParam.getRecvTimeout(), wsParam.getConnectTimeout(), responseClass);
//        actionLogPrs.add(actionLogPr);
        return object;
    }
    
    /**
     * @author duyetdk
     * @param param
     * @param methodCode
     * @param responseClass
     * @return
     * @throws Exception 
     */
    public Object sendRequestWithParam(List<Param> param, String methodCode, Class responseClass) throws Exception {
        WebserviceParam wsParam = getParamByMethodCode(methodCode);
        Object object = sendRequestViaBccsGWParam(param, wsParam.getWsdlUrl(), wsParam.getUserName(),
                wsParam.getPassword(), wsParam.getWsCode(), wsParam.getRecvTimeout(), wsParam.getConnectTimeout(), responseClass);
        return object;
    }
    
    /**
     * @author duyetdk
     * @param param
     * @param wsdlUrl
     * @param userName
     * @param password
     * @param wsCode
     * @param recvTimeout
     * @param connectTimeout
     * @param responseClass
     * @return
     * @throws Exception 
     */
    public Object sendRequestViaBccsGWParam(List<Param> param, String wsdlUrl, String userName,
            String password, String wsCode, int recvTimeout, int connectTimeout, Class responseClass) throws Exception {

        Input input = new Input();
        input.setUsername(userName);
        input.setPassword(password);
        input.setWscode(wsCode);
        input.setParam(param);
        String xmlText = CommonWebservice.marshal(input);

        String request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservice.bccsgw.viettel.com/\"> "
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<web:gwOperation>"
                + xmlText
                + "</web:gwOperation>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";
        long currTime = System.currentTimeMillis();
        log.info("currTime:" + currTime + ";REUEST:wsCode:" + wsCode + ":" + request);
        String responseBody = null;
        try {
            responseBody = sendRequest(request, wsdlUrl, recvTimeout, connectTimeout, BccsGwHttpClientManager.getInstantce().getChannel());
        } catch (Exception e) {
            log.error("currTime:" + currTime + ";RESPON:wsCode:" + wsCode + ":" + " exception: ", e);
            Long test = (System.currentTimeMillis() - currTime + 500) / 1000;
            if (test > 10) {
                log.info(" FINISH sendRequest:" + "wsCode:" + wsCode + ":" + request + " duration = " + test + " CHAM_WS");
            }
            throw e;
        }
        log.info("currTime:" + currTime + ";RESPON:wsCode:" + wsCode + ":" + responseBody);
        Long test = (System.currentTimeMillis() - currTime + 500) / 1000;
        if (test > 10) {
            log.info(" FINISH sendRequest:" + "wsCode:" + wsCode + ":" + request + " duration = " + test + " CHAM_WS");
        }
        try {
            GwOperationResponse response = (GwOperationResponse) CommonWebservice.unmarshal(responseBody, GwOperationResponse.class);
            if (response != null && response.getReturn() != null && response.getReturn().getOriginal() != null) {
                return CommonWebservice.unmarshal(parseResult(response.getReturn().getOriginal()), responseClass);
            }
        } catch (Exception e) {
            log.info(e);
            return null;
        }
        return null;
    }
    
    /**
     * @author duyetdk
     * @param requestContent
     * @param methodCode
     * @param responseClass
     * @return
     * @throws Exception 
     */
    public Object sendRequestViaBccsGWTemp(String requestContent, String methodCode, Class responseClass) throws Exception {
        WebserviceParam wsParam = getParamByMethodCode(methodCode);
        Object object = sendRequestViaBccsGW(requestContent, wsParam.getWsdlUrl(), wsParam.getUserName(),
                wsParam.getPassword(), wsParam.getWsCode(), wsParam.getRecvTimeout(), wsParam.getConnectTimeout(), responseClass);
        return object;
    }
}
