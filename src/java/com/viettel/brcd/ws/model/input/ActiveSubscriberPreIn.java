/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

import java.util.List;

/**
 *
 * @author duyetdk
 */
public class ActiveSubscriberPreIn {

    private String userLogin;
    private String productCode;
    private String reasonCode;
    private String isdn;
    private String serial;
    private String service;
    private String idNumber;
    private Long idType;
    private String name;
    private String dob;
    private String gender;
    private String contact;
    private String province;
    private List<ImageInput> lstImageIDCard;
    private List<ImageInput> lstImageForm;

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public List<ImageInput> getLstImageIDCard() {
        return lstImageIDCard;
    }

    public void setLstImageIDCard(List<ImageInput> lstImageIDCard) {
        this.lstImageIDCard = lstImageIDCard;
    }

    public List<ImageInput> getLstImageForm() {
        return lstImageForm;
    }

    public void setLstImageForm(List<ImageInput> lstImageForm) {
        this.lstImageForm = lstImageForm;
    }

    public Long getIdType() {
        return idType;
    }

    public void setIdType(Long idType) {
        this.idType = idType;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
}
