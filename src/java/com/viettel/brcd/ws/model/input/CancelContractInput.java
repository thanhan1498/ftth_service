package com.viettel.brcd.ws.model.input;

public class CancelContractInput {

    private Long contractId;
    private String token;
    private Long reasonId;
    private Long reqId;

    public CancelContractInput(Long contractId, String token, Long reasonId, Long reqId) {
        this.contractId = contractId;
        this.token = token;
        this.reasonId = reasonId;
        this.reqId = reqId;
    }

    public CancelContractInput() {
    }

    public Long getReqId() {
        return reqId;
    }

    public void setReqId(Long reqId) {
        this.reqId = reqId;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }
}
