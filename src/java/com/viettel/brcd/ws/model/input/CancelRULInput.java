/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

/**
 *
 * @author duyetdk
 */
public class CancelRULInput {
    private Long rulId;
    private Long type;
    private String description;

    public CancelRULInput() {
    }

    public CancelRULInput(Long rulId, Long type, String description) {
        this.rulId = rulId;
        this.type = type;
        this.description = description;
    }

    public Long getRulId() {
        return rulId;
    }

    public void setRulId(Long rulId) {
        this.rulId = rulId;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
