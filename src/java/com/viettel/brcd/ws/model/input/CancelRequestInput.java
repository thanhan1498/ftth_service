package com.viettel.brcd.ws.model.input;

public class CancelRequestInput {

    private Long reqId;
    private Long staffId;
    private Long shopId;
    private Long reasonId;

    public CancelRequestInput(Long reqId, Long staffId, Long shopId, Long reasonId) {
        this.reqId = reqId;
        this.staffId = staffId;
        this.shopId = shopId;
        this.reasonId = reasonId;
    }

    public CancelRequestInput() {
    }

    public Long getReqId() {
        return reqId;
    }

    public void setReqId(Long reqId) {
        this.reqId = reqId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }
}
