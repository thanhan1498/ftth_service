/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

import com.viettel.bccs.cm.util.DateTimeUtils;
import java.util.Date;

/**
 * ChangeDeploymentAddressIn
 *
 * @since 2020
 * @author phuonghc
 */
public class ChangeDeploymentAddressIn {

    private String lat;
    private String lng;
    private String stationId;
    private String connectorId;
    private String shopCode;
    private String staffCode;
    private String staffId;
    private String reasonId;
    private String subId;
    private String lineType;
    private String isdn;
    private String teamCode;
    private String dateTimeDeploy;
    private String delayReason;

    private String provinceCode;
    private String districtCode;
    private String precinctCode;
    private String streetName;
    private String home;

    private Date limitDate;

    public ChangeDeploymentAddressIn() {
    }

    public ChangeDeploymentAddressIn(String lat, String lng, String stationId, String connectId, String shopCode, String staffCode, String staffId, String reasonId, String subId, String lineType, String isdn, String teamCode, String dateTimeDeploy, String delayReason, String provinceCode, String districtCode, String precinctCode, String streetName, String home) {
        this.lat = lat;
        this.lng = lng;
        this.stationId = stationId;
        this.connectorId = connectId;
        this.shopCode = shopCode;
        this.staffCode = staffCode;
        this.staffId = staffId;
        this.reasonId = reasonId;
        this.subId = subId;
        this.lineType = lineType;
        this.isdn = isdn;
        this.teamCode = teamCode;
        this.dateTimeDeploy = dateTimeDeploy;
        this.delayReason = delayReason;
        this.provinceCode = provinceCode;
        this.districtCode = districtCode;
        this.precinctCode = precinctCode;
        this.streetName = streetName;
        this.home = home;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getConnectorId() {
        return connectorId;
    }

    public void setConnectorId(String connectorId) {
        this.connectorId = connectorId;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }

    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getTeamCode() {
        return teamCode;
    }

    public void setTeamCode(String teamCode) {
        this.teamCode = teamCode;
    }

    public String getDateTimeDeploy() {
        return dateTimeDeploy;
    }

    public void setDateTimeDeploy(String dateTimeDeploy) {
        this.dateTimeDeploy = dateTimeDeploy;
    }

    public String getDelayReason() {
        return delayReason;
    }

    public void setDelayReason(String delayReason) {
        this.delayReason = delayReason;
    }

    public Date getLimitDate() {
        limitDate = DateTimeUtils.addDay(new Date(), 7);
        return limitDate;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getPrecinctCode() {
        return precinctCode;
    }

    public void setPrecinctCode(String precinctCode) {
        this.precinctCode = precinctCode;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

}
