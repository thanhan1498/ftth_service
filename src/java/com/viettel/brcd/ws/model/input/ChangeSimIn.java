/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

/**
 *
 * @author vietnn6
 */
public class ChangeSimIn {

    private String userLogin;
    private String reasonId;
    private String service;
    private String isdn;
    private String idCard;
    private String serial;
    private String recentIsdn1;
    private String recentIsdn2;
    private String recentIsdn3;
    private String productCode;
    private String topUpAmount;

    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getTopUpAmount() {
        return topUpAmount;
    }

    public void setTopUpAmount(String topUpAmount) {
        this.topUpAmount = topUpAmount;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getRecentIsdn1() {
        return recentIsdn1;
    }

    public void setRecentIsdn1(String recentIsdn1) {
        this.recentIsdn1 = recentIsdn1;
    }

    public String getRecentIsdn2() {
        return recentIsdn2;
    }

    public void setRecentIsdn2(String recentIsdn2) {
        this.recentIsdn2 = recentIsdn2;
    }

    public String getRecentIsdn3() {
        return recentIsdn3;
    }

    public void setRecentIsdn3(String recentIsdn3) {
        this.recentIsdn3 = recentIsdn3;
    }
}
