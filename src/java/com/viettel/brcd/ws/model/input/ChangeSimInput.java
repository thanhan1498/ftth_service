/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

/**
 *
 * @author cuongdm
 */
public class ChangeSimInput {

    private String isdn;
    private Long shopId;
    private Long staffId;
    private String staffCode;
    private String idNo;
    private String serial;
    private Long reasonId;
    private String saleServiceForm;
    private String commitForm;
    private Long isBlockOpen;/*1: open sim block*/
    private String idCardFront;
    private String idCardback;
    private Long subId;
    private String ip;

    /**
     * @return the idCardFront
     */
    public String getIdCardFront() {
        return idCardFront;
    }

    /**
     * @param idCardFront the idCardFront to set
     */
    public void setIdCardFront(String idCardFront) {
        this.idCardFront = idCardFront;
    }

    /**
     * @return the idCardback
     */
    public String getIdCardback() {
        return idCardback;
    }

    /**
     * @param idCardback the idCardback to set
     */
    public void setIdCardback(String idCardback) {
        this.idCardback = idCardback;
    }

    /**
     * @return the staffId
     */
    public Long getStaffId() {
        return staffId;
    }

    /**
     * @param staffId the staffId to set
     */
    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    /**
     * @return the staffCode
     */
    public String getStaffCode() {
        return staffCode;
    }

    /**
     * @param staffCode the staffCode to set
     */
    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    /**
     * @return the isdn
     */
    public String getIsdn() {
        return isdn;
    }

    /**
     * @param isdn the isdn to set
     */
    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    /**
     * @return the shopId
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     * @param shopId the shopId to set
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     * @return the idNo
     */
    public String getIdNo() {
        return idNo;
    }

    /**
     * @param idNo the idNo to set
     */
    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    /**
     * @return the serial
     */
    public String getSerial() {
        return serial;
    }

    /**
     * @param serial the serial to set
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     * @return the reasonId
     */
    public Long getReasonId() {
        return reasonId;
    }

    /**
     * @param reasonId the reasonId to set
     */
    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    /**
     * @return the saleServiceForm
     */
    public String getSaleServiceForm() {
        return saleServiceForm;
    }

    /**
     * @param saleServiceForm the saleServiceForm to set
     */
    public void setSaleServiceForm(String saleServiceForm) {
        this.saleServiceForm = saleServiceForm;
    }

    /**
     * @return the commitForm
     */
    public String getCommitForm() {
        return commitForm;
    }

    /**
     * @param commitForm the commitForm to set
     */
    public void setCommitForm(String commitForm) {
        this.commitForm = commitForm;
    }

    /**
     * @return the isBlockOpen
     */
    public Long getIsBlockOpen() {
        return isBlockOpen;
    }

    /**
     * @param isBlockOpen the isBlockOpen to set
     */
    public void setIsBlockOpen(Long isBlockOpen) {
        this.isBlockOpen = isBlockOpen;
    }

    /**
     * @return the subId
     */
    public Long getSubId() {
        return subId;
    }

    /**
     * @param subId the subId to set
     */
    public void setSubId(Long subId) {
        this.subId = subId;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }
}
