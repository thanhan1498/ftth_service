/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

/**
 *
 * @author duyetdk
 */
public class ComplaintInput {

    private Long complainId;
    private String complainerName; //complainant
    private String complainerPhone; //phone number
    private String complainerAddress; //adrress
    private Long subId;
    private String complainerPassport; //idNo
    private Long compTypeId; //complaint type
    private Long acceptSourceId; //receiving source
    private String compContent; //complaint content
    private String custLimitDate; //time to resolve
//    private Long depId; //coordination unit
    private String custName; //complainant
    private String district;
    private String precinct;
    private String sourceComplaint;
    private String image;
    private String errorPhone; //phone number
    private String tokenId;

    public Long getComplainId() {
        return complainId;
    }

    public void setComplainId(Long complainId) {
        this.complainId = complainId;
    }

    public String getComplainerName() {
        return complainerName;
    }

    public void setComplainerName(String complainerName) {
        this.complainerName = complainerName;
    }

    public String getComplainerPhone() {
        return complainerPhone;
    }

    public void setComplainerPhone(String complainerPhone) {
        this.complainerPhone = complainerPhone;
    }

    public String getComplainerAddress() {
        return complainerAddress;
    }

    public void setComplainerAddress(String complainerAddress) {
        this.complainerAddress = complainerAddress;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getComplainerPassport() {
        return complainerPassport;
    }

    public void setComplainerPassport(String complainerPassport) {
        this.complainerPassport = complainerPassport;
    }

    public Long getCompTypeId() {
        return compTypeId;
    }

    public void setCompTypeId(Long compTypeId) {
        this.compTypeId = compTypeId;
    }

    public Long getAcceptSourceId() {
        return acceptSourceId;
    }

    public void setAcceptSourceId(Long acceptSourceId) {
        this.acceptSourceId = acceptSourceId;
    }

    public String getCompContent() {
        return compContent;
    }

    public void setCompContent(String compContent) {
        this.compContent = compContent;
    }

    public String getCustLimitDate() {
        return custLimitDate;
    }

    public void setCustLimitDate(String custLimitDate) {
        this.custLimitDate = custLimitDate;
    }

//    public Long getDepId() {
//        return depId;
//    }
//
//    public void setDepId(Long depId) {
//        this.depId = depId;
//    }
    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    /**
     * @return the sourceComplaint
     */
    public String getSourceComplaint() {
        return sourceComplaint;
    }

    /**
     * @param sourceComplaint the sourceComplaint to set
     */
    public void setSourceComplaint(String sourceComplaint) {
        this.sourceComplaint = sourceComplaint;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return the errorPhone
     */
    public String getErrorPhone() {
        return errorPhone;
    }

    /**
     * @param errorPhone the errorPhone to set
     */
    public void setErrorPhone(String errorPhone) {
        this.errorPhone = errorPhone;
    }

    /**
     * @return the tokenId
     */
    public String getTokenId() {
        return tokenId;
    }

    /**
     * @param tokenId the tokenId to set
     */
    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

}
