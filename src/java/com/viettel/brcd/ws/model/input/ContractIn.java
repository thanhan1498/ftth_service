/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

import com.viettel.brcd.ws.model.output.Infrastruct;
import com.viettel.brcd.ws.model.output.Subscriber;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author linhlh2
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "contract")
public class ContractIn {

    @XmlElement(name = "customer")
    private CustomerIn customerIn;
    @XmlElement(name = "subscriber")
    private Subscriber subscriber;
    @XmlElement(name = "infrastruct")
    private Infrastruct infrastruct;
    @XmlElement(required = true)
    private Long shopId;
    @XmlElement(required = true)
    private Long staffId;
    private Long[] stockTypeIds;
    private Long isSatisfy;
    private List<Coordinate> listCoordinate;
    private Long requestId;

    public List<Coordinate> getListCoordinate() {
        return listCoordinate;
    }

    public void setListCoordinate(List<Coordinate> listCoordinate) {
        this.listCoordinate = listCoordinate;
    }

    public CustomerIn getCustomerIn() {
        return customerIn;
    }

    public void setCustomerIn(CustomerIn customerIn) {
        this.customerIn = customerIn;
    }

    public Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Subscriber subscriber) {
        this.subscriber = subscriber;
    }

    public Infrastruct getInfrastruct() {
        return infrastruct;
    }

    public void setInfrastruct(Infrastruct infrastruct) {
        this.infrastruct = infrastruct;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Long[] getStockTypeIds() {
        return stockTypeIds;
    }

    public void setStockTypeIds(Long[] stockTypeIds) {
        this.stockTypeIds = stockTypeIds;
    }

    public Long getIsSatisfy() {
        return isSatisfy;
    }

    public void setIsSatisfy(Long isSatisfy) {
        this.isSatisfy = isSatisfy;
    }

    /**
     * @return the requestId
     */
    public Long getRequestId() {
        return requestId;
    }

    /**
     * @param requestId the requestId to set
     */
    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }
}
