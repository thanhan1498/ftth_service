/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

/**
 *
 * @author Nguyen Ngoc Viet
 */
public class Coordinate {

    private Double Lat;
    private Double Lng;
    private Long orderNumber;
    private Long location;

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Long getLocation() {
        return location;
    }

    public void setLocation(Long location) {
        this.location = location;
    }

    public Coordinate() {
    }

    public Coordinate(Double Lat, Double Lng, Long order) {
        this.Lat = Lat;
        this.Lng = Lng;
        this.orderNumber = order;
    }

    public Double getLat() {
        return Lat;
    }

    public void setLat(Double Lat) {
        this.Lat = Lat;
    }

    public Double getLng() {
        return Lng;
    }

    public void setLng(Double Lng) {
        this.Lng = Lng;
    }
}
