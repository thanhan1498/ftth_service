/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

import com.viettel.bccs.cm.model.TechnicalConnector;
import com.viettel.bccs.cm.model.TechnicalStation;
import java.util.List;

/**
 *
 * @author Hitex-Athena90
 */
public class CreateInterruptFtthInput {
    Long reasonTypeId;
    Long reasonDetailId;
    String affectFromDate;
    String affectToDate;
    List<String> lstCustomerType;
    Long influenceScope;
    List<TechnicalStation> listBts;
    List<TechnicalConnector> listConnector;
    Long influenceLevel;
    List<String> serviceInterrupted;
    String moreDescription;
    String documentName;
    byte[] documentData;
    String staffCode;
    String shopCode;
    Long shopId;

    public CreateInterruptFtthInput() {
    }

    public byte[] getDocumentData() {
        return documentData;
    }

    public void setDocumentData(byte[] documentData) {
        this.documentData = documentData;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getAffectFromDate() {
        return affectFromDate;
    }

    public void setAffectFromDate(String affectFromDate) {
        this.affectFromDate = affectFromDate;
    }

    public String getAffectToDate() {
        return affectToDate;
    }

    public void setAffectToDate(String affectToDate) {
        this.affectToDate = affectToDate;
    }


    public Long getInfluenceLevel() {
        return influenceLevel;
    }

    public void setInfluenceLevel(Long influenceLevel) {
        this.influenceLevel = influenceLevel;
    }

    public Long getInfluenceScope() {
        return influenceScope;
    }

    public void setInfluenceScope(Long influenceScope) {
        this.influenceScope = influenceScope;
    }

    public List<TechnicalStation> getListBts() {
        return listBts;
    }

    public void setListBts(List<TechnicalStation> listBts) {
        this.listBts = listBts;
    }

    public List<TechnicalConnector> getListConnector() {
        return listConnector;
    }

    public void setListConnector(List<TechnicalConnector> listConnector) {
        this.listConnector = listConnector;
    }

    public List<String> getLstCustomerType() {
        return lstCustomerType;
    }

    public void setLstCustomerType(List<String> lstCustomerType) {
        this.lstCustomerType = lstCustomerType;
    }

    public String getMoreDescription() {
        return moreDescription;
    }

    public void setMoreDescription(String moreDescription) {
        this.moreDescription = moreDescription;
    }

    public Long getReasonDetailId() {
        return reasonDetailId;
    }

    public void setReasonDetailId(Long reasonDetailId) {
        this.reasonDetailId = reasonDetailId;
    }

    public Long getReasonTypeId() {
        return reasonTypeId;
    }

    public void setReasonTypeId(Long reasonTypeId) {
        this.reasonTypeId = reasonTypeId;
    }

    public List<String> getServiceInterrupted() {
        return serviceInterrupted;
    }

    public void setServiceInterrupted(List<String> serviceInterrupted) {
        this.serviceInterrupted = serviceInterrupted;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }
    
}
