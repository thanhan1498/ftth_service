/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.CustomerExtend;
import com.viettel.bccs.cm.util.DateTimeUtils;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author linhlh2
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "customer")
public class CustomerIn {

    /**
     * @return the taskMngtId
     */
    public Long getTaskMngtId() {
        return taskMngtId;
    }

    /**
     * @param taskMngtId the taskMngtId to set
     */
    public void setTaskMngtId(Long taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    /**
     * @return the deployLat
     */
    public String getDeployLat() {
        return deployLat;
    }

    /**
     * @param deployLat the deployLat to set
     */
    public void setDeployLat(String deployLat) {
        this.deployLat = deployLat;
    }

    /**
     * @return the deployLng
     */
    public String getDeployLng() {
        return deployLng;
    }

    /**
     * @param deployLng the deployLng to set
     */
    public void setDeployLng(String deployLng) {
        this.deployLng = deployLng;
    }

    @XmlElement
    private String customType;//Bus Type
    @XmlElement
    private String name;
    @XmlElement
    private String birthDate;
    @XmlElement
    private Long idType;
    @XmlElement
    private String idNo;
    @XmlElement
    private String busPermitNo;
    @XmlElement
    private String idIssuePlace;
    @XmlElement
    private String idIssueDate;
    @XmlElement
    private String address;
    @XmlElement
    private Long custId;
    @XmlElement
    private String provinceCode;
    @XmlElement
    private String districtCode;
    @XmlElement
    private String precinctCode;
    @XmlElement
    private String streetBlockName;
    @XmlElement
    private String streetBlock;
    @XmlElement(required = true, defaultValue = " ")
    private String sex;
    @XmlElement
    private String lat;//vi do
    @XmlElement
    private String lng;//kinh do
    @XmlElement
    private String custType;//loai KH ca nhan - doanh nghiep: 0 - ca nhan , 1 - doanh nghiep
    @XmlElement
    private String telMobile;
    private Long taskMngtId;
    private String deployLat;
    private String deployLng;

    public CustomerIn() {
    }

    public CustomerIn(CustomerExtend obj) {
        if (obj != null) {
            this.custId = obj.getCustId();
            this.customType = obj.getBusType();
            this.name = obj.getName();
            this.birthDate = DateTimeUtils.formatddMMyyyy(obj.getBirthDate());
            this.idNo = obj.getIdNo();
            this.busPermitNo = obj.getBusPermitNo();
            this.idIssuePlace = obj.getIdIssuePlace();
            this.idIssueDate = DateTimeUtils.formatddMMyyyy(obj.getIdIssueDate());
            this.address = obj.getAddress();
            this.idType = obj.getIdType();
            this.sex = obj.getSex();
            this.provinceCode = obj.getProvince();
            this.districtCode = obj.getDistrict();
            this.precinctCode = obj.getPrecinct();
            this.streetBlockName = obj.getStreetBlockName();
            this.lng = obj.getyLocation();
            this.lat = obj.getxLocation();
            this.telMobile = obj.getTelFax();
            this.taskMngtId = obj.getTaskMngtId();
            this.deployLat = obj.getDeployLat();
            this.deployLng = obj.getDeployLng();
        }
    }

    public CustomerIn(Customer obj) {
        if (obj != null) {
            this.custId = obj.getCustId();
            this.customType = obj.getBusType();
            this.name = obj.getName();
            this.birthDate = DateTimeUtils.formatddMMyyyy(obj.getBirthDate());
            this.idNo = obj.getIdNo();
            this.busPermitNo = obj.getBusPermitNo();
            this.idIssuePlace = obj.getIdIssuePlace();
            this.idIssueDate = DateTimeUtils.formatddMMyyyy(obj.getIdIssueDate());
            this.address = obj.getAddress();
            this.idType = obj.getIdType();
            this.sex = obj.getSex();
            this.provinceCode = obj.getProvince();
            this.districtCode = obj.getDistrict();
            this.precinctCode = obj.getPrecinct();
            this.streetBlockName = obj.getStreetBlockName();
            this.lng = obj.getyLocation();
            this.lat = obj.getxLocation();
            this.telMobile = obj.getTelFax();
        }
    }

    public String getStreetBlock() {
        return streetBlock;
    }

    public void setStreetBlock(String streetBlock) {
        this.streetBlock = streetBlock;
    }

    public Long getIdType() {
        return idType;
    }

    public void setIdType(Long idType) {
        this.idType = idType;
    }

    public String getCustomType() {
        return customType;
    }

    public void setCustomType(String customType) {
        this.customType = customType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getIdIssuePlace() {
        return idIssuePlace;
    }

    public void setIdIssuePlace(String idIssuePlace) {
        this.idIssuePlace = idIssuePlace;
    }

    public String getIdIssueDate() {
        return idIssueDate;
    }

    public void setIdIssueDate(String idIssueDate) {
        this.idIssueDate = idIssueDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public String getBusPermitNo() {
        return busPermitNo;
    }

    public void setBusPermitNo(String busPermitNo) {
        this.busPermitNo = busPermitNo;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getPrecinctCode() {
        return precinctCode;
    }

    public void setPrecinctCode(String precinctCode) {
        this.precinctCode = precinctCode;
    }

    public String getStreetBlockName() {
        return streetBlockName;
    }

    public void setStreetBlockName(String streetBlockName) {
        this.streetBlockName = streetBlockName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public String getTelMobile() {
        return telMobile;
    }

    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }

}
