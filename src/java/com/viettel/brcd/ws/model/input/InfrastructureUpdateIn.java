/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nguyen Tran Minh Nhut <tranminhnhutn@metfone.com.kh>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "infrastructureUpdateIn")
public class InfrastructureUpdateIn implements Cloneable {

    @XmlElement
    String taskMngtId;
    @XmlElement
    String taskStaffMngtId;
    @XmlElement
    String subId;
    @XmlElement
    String telServiceId;
    @XmlElement
    String cableBoxId;
    @XmlElement
    String plateA;
    @XmlElement
    String plateB;
    @XmlElement
    String plateCableBoxA;
    @XmlElement
    String plateCableBoxB;
    @XmlElement
    String boardId;
    @XmlElement
    String lineNo;
    @XmlElement
    String portNo;
    @XmlElement
    String loginName;
    @XmlElement
    String shopCodeLogin;
    @XmlElement
    String dsLamId;
    @XmlElement(name = "lstGood")
    HashMap<String, String> listGood;
    @XmlElement(name = "lstNewSerial")
    HashMap<String, String> listNewSerial;
    @XmlElement(name = "lstItem")
    HashMap<String, String> listItem;
    @XmlElement
    String isUpdateDeployment;
    @XmlElement
    String reasonId;
    @XmlElement
    String stationId;
    @XmlElement
    String isGpon;
    @XmlElement
    String gponSerial;
    @XmlElement
    String stbSerial;
    @XmlElement
    String portSpliterId;
    @XmlElement
    String accountGline;
    @XmlElement
    Long spitterId;
    @XmlElement
    Long portLogic;
    /* duyetdk: bo sung serialGpon,imgInfras,imgGoods,tab */
    @XmlElement(name = "imageInfra")
    List<NewImageInput> imgInfras;
    @XmlElement(name = "imageGood")
    List<NewImageInput> imgGoods;
    @XmlElement
    String tab;
    @XmlElement
    private String isSplitter;
    private String couplerNo;

    public Long getSpitterId() {
        return spitterId;
    }

    public void setSpitterId(Long spitterId) {
        this.spitterId = spitterId;
    }

    public Long getPortLogic() {
        return portLogic;
    }

    public void setPortLogic(Long portLogic) {
        this.portLogic = portLogic;
    }

    public String getIsGpon() {
        return isGpon;
    }

    public void setIsGpon(String isGpon) {
        this.isGpon = isGpon;
    }

    public String getGponSerial() {
        return gponSerial;
    }

    public void setGponSerial(String gponSerial) {
        this.gponSerial = gponSerial;
    }

    public String getStbSerial() {
        return stbSerial;
    }

    public void setStbSerial(String stbSerial) {
        this.stbSerial = stbSerial;
    }

    public String getPortSpliterId() {
        return portSpliterId;
    }

    public void setPortSpliterId(String portSpliterId) {
        this.portSpliterId = portSpliterId;
    }

    public String getAccountGline() {
        return accountGline;
    }

    public void setAccountGline(String accountGline) {
        this.accountGline = accountGline;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getIsUpdateDeployment() {
        return isUpdateDeployment;
    }

    public void setIsUpdateDeployment(String isUpdateDeployment) {
        this.isUpdateDeployment = isUpdateDeployment;
    }

    public String getTaskMngtId() {
        return taskMngtId;
    }

    public void setTaskMngtId(String taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    public String getTaskStaffMngtId() {
        return taskStaffMngtId;
    }

    public void setTaskStaffMngtId(String taskStaffMngtId) {
        this.taskStaffMngtId = taskStaffMngtId;
    }

    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public String getTelServiceId() {
        return telServiceId;
    }

    public void setTelServiceId(String telServiceId) {
        this.telServiceId = telServiceId;
    }

    public String getCableBoxId() {
        return cableBoxId;
    }

    public void setCableBoxId(String cableBoxId) {
        this.cableBoxId = cableBoxId;
    }

    public String getPlateA() {
        return plateA;
    }

    public void setPlateA(String plateA) {
        this.plateA = plateA;
    }

    public String getPlateB() {
        return plateB;
    }

    public void setPlateB(String plateB) {
        this.plateB = plateB;
    }

    public String getPlateCableBoxA() {
        return plateCableBoxA;
    }

    public void setPlateCableBoxA(String plateCableBoxA) {
        this.plateCableBoxA = plateCableBoxA;
    }

    public String getPlateCableBoxB() {
        return plateCableBoxB;
    }

    public void setPlateCableBoxB(String plateCableBoxB) {
        this.plateCableBoxB = plateCableBoxB;
    }

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    public String getLineNo() {
        return lineNo;
    }

    public void setLineNo(String lineNo) {
        this.lineNo = lineNo;
    }

    public String getPortNo() {
        return portNo;
    }

    public void setPortNo(String portNo) {
        this.portNo = portNo;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getShopCodeLogin() {
        return shopCodeLogin;
    }

    public void setShopCodeLogin(String shopCodeLogin) {
        this.shopCodeLogin = shopCodeLogin;
    }

    public String getDsLamId() {
        return dsLamId;
    }

    public void setDsLamId(String dsLamId) {
        this.dsLamId = dsLamId;
    }

    public HashMap<String, String> getListGood() {
        return listGood;
    }

    public void setListGood(HashMap<String, String> listGood) {
        this.listGood = listGood;
    }

    public HashMap<String, String> getListNewSerial() {
        return listNewSerial;
    }

    public void setListNewSerial(HashMap<String, String> listNewSerial) {
        this.listNewSerial = listNewSerial;
    }

    public HashMap<String, String> getListItem() {
        return listItem;
    }

    public void setListItem(HashMap<String, String> listItem) {
        this.listItem = listItem;
    }

    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }

    public List<NewImageInput> getImgInfras() {
        return imgInfras;
    }

    public void setImgInfras(List<NewImageInput> imgInfras) {
        this.imgInfras = imgInfras;
    }

    public List<NewImageInput> getImgGoods() {
        return imgGoods;
    }

    public void setImgGoods(List<NewImageInput> imgGoods) {
        this.imgGoods = imgGoods;
    }

    public String getTab() {
        return tab;
    }

    public void setTab(String tab) {
        this.tab = tab;
    }

    /**
     * @return the isSplitter
     */
    public String getIsSplitter() {
        return isSplitter;
    }

    /**
     * @param isSplitter the isSplitter to set
     */
    public void setIsSplitter(String isSplitter) {
        this.isSplitter = isSplitter;
    }

    @Override
    public InfrastructureUpdateIn clone() {
        try {
            InfrastructureUpdateIn clone = (InfrastructureUpdateIn) super.clone();
            if (this.imgInfras != null) {
                clone.setImgInfras(new ArrayList<NewImageInput>());
                for (NewImageInput image : this.imgInfras) {
                    clone.getImgInfras().add(new NewImageInput(image));
                }
            }
            if (this.imgGoods != null) {
                clone.setImgGoods(new ArrayList<NewImageInput>());
                for (NewImageInput image : this.imgGoods) {
                    clone.getImgGoods().add(new NewImageInput(image));
                }
            }
            return clone;
        } catch (Exception ex) {
        }
        return this;
    }

    /**
     * @return the couplerNo
     */
    public String getCouplerNo() {
        return couplerNo;
    }

    /**
     * @param couplerNo the couplerNo to set
     */
    public void setCouplerNo(String couplerNo) {
        this.couplerNo = couplerNo;
    }
}
