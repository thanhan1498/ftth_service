/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

/**
 *
 * @author duyetdk
 */
public class NewImageInput {

    private String image;
    private String imageName;
    private String imageType;
    private String lat;
    private String lon;

    public NewImageInput() {
    }

    public NewImageInput(NewImageInput object) {
        this.image = String.valueOf(object.getImage().length());
        this.imageName = object.getImageName();
        this.imageType = object.getImageType();
        this.lat = object.getLat();
        this.lon = object.getLon();
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }
}
