/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

import java.util.List;

/**
 *
 * @author Hitex-Athena90
 */
public class PushSmsInterruptFtthInput {

    Long interruptId;
    List<String> lstCustomerType;
    String content;
    String staffCode;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getInterruptId() {
        return interruptId;
    }

    public void setInterruptId(Long interruptId) {
        this.interruptId = interruptId;
    }

    public List<String> getLstCustomerType() {
        return lstCustomerType;
    }

    public void setLstCustomerType(List<String> lstCustomerType) {
        this.lstCustomerType = lstCustomerType;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }
}