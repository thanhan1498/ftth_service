/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

/**
 *
 * @author User-PC
 */
public class RevokeTaskIn {

    private Long taskStaffMngtId;
    private Long reason;
    private String loginName;
    private String shopCodeLogin;
    private Long subIdClient;
    private Long telServiceId;
    private Long revokeFor;
    private String note;

    public Long getTaskStaffMngtId() {
        return taskStaffMngtId;
    }

    public void setTaskStaffMngtId(Long taskStaffMngtId) {
        this.taskStaffMngtId = taskStaffMngtId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getShopCodeLogin() {
        return shopCodeLogin;
    }

    public void setShopCodeLogin(String shopCodeLogin) {
        this.shopCodeLogin = shopCodeLogin;
    }

    public Long getSubIdClient() {
        return subIdClient;
    }

    public void setSubIdClient(Long subIdClient) {
        this.subIdClient = subIdClient;
    }

    public Long getReason() {
        return reason;
    }

    public void setReason(Long reason) {
        this.reason = reason;
    }

    public Long getTelServiceId() {
        return telServiceId;
    }

    public void setTelServiceId(Long telServiceId) {
        this.telServiceId = telServiceId;
    }

    public Long getRevokeFor() {
        return revokeFor;
    }

    public void setRevokeFor(Long revokeFor) {
        this.revokeFor = revokeFor;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
