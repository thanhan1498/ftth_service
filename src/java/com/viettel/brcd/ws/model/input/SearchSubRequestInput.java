/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

public class SearchSubRequestInput {

    private String staffCode;
    private String custName;
    private String idNo;
    private String serviceAlias;
    private String account;
    private String reqStatus;
    private String fromDate;
    private String toDate;
    private Long pageSize;
    private Long pageNo;

    public SearchSubRequestInput() {
    }

    public SearchSubRequestInput(String staffCode, String custName, String idNo, String serviceAlias, String account, String reqStatus, String fromDate, String toDate, Long pageSize, Long pageNo) {
        this.staffCode = staffCode;
        this.custName = custName;
        this.idNo = idNo;
        this.serviceAlias = serviceAlias;
        this.account = account;
        this.reqStatus = reqStatus;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.pageSize = pageSize;
        this.pageNo = pageNo;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getServiceAlias() {
        return serviceAlias;
    }

    public void setServiceAlias(String serviceAlias) {
        this.serviceAlias = serviceAlias;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getReqStatus() {
        return reqStatus;
    }

    public void setReqStatus(String reqStatus) {
        this.reqStatus = reqStatus;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Long getPageNo() {
        return pageNo;
    }

    public void setPageNo(Long pageNo) {
        this.pageNo = pageNo;
    }
}
