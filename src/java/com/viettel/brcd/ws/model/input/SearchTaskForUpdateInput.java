/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

/**
 *
 * @author Nguyen Tran Minh Nhut
 */
public class SearchTaskForUpdateInput {

    Long service;
    Long staffId;
    Long custReqId;
    Long shopId;
    String sourceType;
    String account;
    String progress;
    String staDate;
    String endDate;
    /* duyetdk: bo sung truong statusConfig */
    Long status;
    Long statusConfig;
    Long page;
    Long pageSize;

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getCustReqId() {
        return custReqId;
    }

    public void setCustReqId(Long custReqId) {
        this.custReqId = custReqId;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getStaDate() {
        return staDate;
    }

    public void setStaDate(String staDate) {
        this.staDate = staDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Long getService() {
        return service;
    }

    public void setService(Long service) {
        this.service = service;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the page
     */
    public void setPage(Long page) {
        this.page = page;
    }

    /**
     * @return the pageSize
     */
    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * @return the page
     */
    public Long getPage() {
        return page;
    }

    /**
     * @return the pageSize
     */
    public Long getPageSize() {
        return pageSize;
    }

    /**
     * @return the statusConfig
     */
    public Long getStatusConfig() {
        return statusConfig;
    }

    /**
     * @param statusConfig the statusConfig to set
     */
    public void setStatusConfig(Long statusConfig) {
        this.statusConfig = statusConfig;
    }
}
