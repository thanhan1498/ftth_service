/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

/**
 *
 * @author Nguyen Tran Minh Nhut
 */
public class SearchTaskInput {
    String custReqId;
    String contractNo;
    String shopId;
    String sourceType;
    String account;
    String progress;
    String staDate;
    String endDate;
    String shopIdToken;
    Long assignTo;
    private Long page;
    private Long pageSize;

    public String getCustReqId() {
        return custReqId;
    }

    public void setCustReqId(String custReqId) {
        this.custReqId = custReqId;
    }
   

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getStaDate() {
        return staDate;
    }

    public void setStaDate(String staDate) {
        this.staDate = staDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopIdToken() {
        return shopIdToken;
    }

    public void setShopIdToken(String shopIdToken) {
        this.shopIdToken = shopIdToken;
    }

    public Long getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(Long assignTo) {
        this.assignTo = assignTo;
    }

    /**
     * @return the page
     */
    public Long getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Long page) {
        this.page = page;
    }

    /**
     * @return the pageSize
     */
    public Long getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    
   

}
