/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

/**
 *
 * @author User-PC
 */
public class SearchTaskRevokeIn {

    private Long shopId;
    private Long staffId;
    private String taskStaffProgress;
    private String startDate;
    private String endDate;
    private Long custReqId;
    private String contractNo;
    private String account;
    private String custName;
    private String telFax;
    private Long telServiceId;
    private Long revokeFor;
    private Long page;
    private Long pageSize;

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getTaskStaffProgress() {
        return taskStaffProgress;
    }

    public void setTaskStaffProgress(String taskStaffProgress) {
        this.taskStaffProgress = taskStaffProgress;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Long getCustReqId() {
        return custReqId;
    }

    public void setCustReqId(Long custReqId) {
        this.custReqId = custReqId;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getTelFax() {
        return telFax;
    }

    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    public Long getTelServiceId() {
        return telServiceId;
    }

    public void setTelServiceId(Long telServiceId) {
        this.telServiceId = telServiceId;
    }

    public Long getRevokeFor() {
        return revokeFor;
    }

    public void setRevokeFor(Long revokeFor) {
        this.revokeFor = revokeFor;
    }

    /**
     * @return the page
     */
    public Long getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Long page) {
        this.page = page;
    }

    /**
     * @return the pageSize
     */
    public Long getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }
}
