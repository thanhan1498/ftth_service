/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

import com.viettel.bccs.cm.model.SubAdslLeaseline;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SubAdslLeaselineOut")
public class SubAdslLeaselineOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "SubAdslLeaseline")
    private SubAdslLeaseline subAdslLeaseline;

    public SubAdslLeaselineOut() {
    }

    public SubAdslLeaselineOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public SubAdslLeaselineOut(String errorCode, String errorDecription, SubAdslLeaseline subAdslLeaseline) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.subAdslLeaseline = subAdslLeaseline;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public SubAdslLeaseline getSubAdslLeaseline() {
        return subAdslLeaseline;
    }

    public void setSubAdslLeaseline(SubAdslLeaseline subAdslLeaseline) {
        this.subAdslLeaseline = subAdslLeaseline;
    }

}
