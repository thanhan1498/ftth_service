/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

/**
 *
 * @author Nguyen Tran Minh Nhut <tranminhnhutn@metfone.com.kh>
 */
public class TaskToAssignIn {

    String isReAssign;
    String assignTo;
    String taskShopMngtId;
    String userNameLogin;
    Long staffIdLogin;
    String shopCodeLogin;
    Long staffAssignId;
    String staDate;
    String endDate;
    String telToAssign;
    String taskMngtId;
    Long shopAssignId;

    public String getIsReAssign() {
        return isReAssign;
    }

    public void setIsReAssign(String isReAssign) {
        this.isReAssign = isReAssign;
    }

    public String getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(String assignTo) {
        this.assignTo = assignTo;
    }

    public String getTaskShopMngtId() {
        return taskShopMngtId;
    }

    public void setTaskShopMngtId(String taskShopMngtId) {
        this.taskShopMngtId = taskShopMngtId;
    }

    public String getUserNameLogin() {
        return userNameLogin;
    }

    public void setUserNameLogin(String userNameLogin) {
        this.userNameLogin = userNameLogin;
    }

    public Long getStaffAssignId() {
        return staffAssignId;
    }

    public void setStaffAssignId(Long staffAssignId) {
        this.staffAssignId = staffAssignId;
    }

    public String getTaskMngtId() {
        return taskMngtId;
    }

    public void setTaskMngtId(String taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    public Long getShopAssignId() {
        return shopAssignId;
    }

    public void setShopAssignId(Long shopAssignId) {
        this.shopAssignId = shopAssignId;
    }

  

    public Long getStaffIdLogin() {
        return staffIdLogin;
    }

    public void setStaffIdLogin(Long staffIdLogin) {
        this.staffIdLogin = staffIdLogin;
    }

    public String getShopCodeLogin() {
        return shopCodeLogin;
    }

    public void setShopCodeLogin(String shopCodeLogin) {
        this.shopCodeLogin = shopCodeLogin;
    }

    public String getStaDate() {
        return staDate;
    }

    public void setStaDate(String staDate) {
        this.staDate = staDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTelToAssign() {
        return telToAssign;
    }

    public void setTelToAssign(String telToAssign) {
        this.telToAssign = telToAssign;
    }
}
