/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

/**
 *
 * @author duyetdk
 */

public class UpdateInfrastructureIn {
    private String account;
    private String infraType;
    private String stationCode;
    private String connectorCode;
    private String xLocation;
    private String yLocation;
    private Long isBranch;

    public UpdateInfrastructureIn() {
    }
    
    public UpdateInfrastructureIn(String account, String infraType, String stationCode, String connectorCode, String xLocation, String yLocation, Long isBranch) {
        this.account = account;
        this.infraType = infraType;
        this.stationCode = stationCode;
        this.connectorCode = connectorCode;
        this.xLocation = xLocation;
        this.yLocation = yLocation;
        this.isBranch = isBranch;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getInfraType() {
        return infraType;
    }

    public void setInfraType(String infraType) {
        this.infraType = infraType;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getConnectorCode() {
        return connectorCode;
    }

    public void setConnectorCode(String connectorCode) {
        this.connectorCode = connectorCode;
    }

    public String getxLocation() {
        return xLocation;
    }

    public void setxLocation(String xLocation) {
        this.xLocation = xLocation;
    }

    public String getyLocation() {
        return yLocation;
    }

    public void setyLocation(String yLocation) {
        this.yLocation = yLocation;
    }

    public Long getIsBranch() {
        return isBranch;
    }

    public void setIsBranch(Long isBranch) {
        this.isBranch = isBranch;
    }
    
}
