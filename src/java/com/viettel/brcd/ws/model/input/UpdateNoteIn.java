/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

/**
 *
 * @author Nguyen Tran Minh Nhut <tranminhnhutn@metfone.com.kh>
 */
public class UpdateNoteIn {
    
    String subId;
    String taskMngtId;
    String taskStaffMngtId;
    String note;
    String reasonId;
    String reasonGroup;
    String loginName;
    String shopCodeLogin;

    public String getReasonGroup() {
        return reasonGroup;
    }

    public void setReasonGroup(String reasonGroup) {
        this.reasonGroup = reasonGroup;
    }

    
    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public String getTaskMngtId() {
        return taskMngtId;
    }

    public void setTaskMngtId(String taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    public String getTaskStaffMngtId() {
        return taskStaffMngtId;
    }

    public void setTaskStaffMngtId(String taskStaffMngtId) {
        this.taskStaffMngtId = taskStaffMngtId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getShopCodeLogin() {
        return shopCodeLogin;
    }

    public void setShopCodeLogin(String shopCodeLogin) {
        this.shopCodeLogin = shopCodeLogin;
    }
    
}
