/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input;

/**
 *
 * @author Nguyen Tran Minh Nhut <tranminhnhutn@metfone.com.kh>
 */
public class UpdateTaskIn {
    String subId;
    String taskStaffMngtId;
    String telServiceId;
    String taskStaffProgress;
    String isSystemError;
    String note;
    String loginName;
    String shopCodeLogin;
    String reasonId;
    String complaintResult;
    String satisfileLevel;
    String compCauseId;
    String reasonGroup;
    //truongpv them thong tin deduct time
    String isDeductTime;
    String startTimeDeduct;
    String endTimeDeduct;
    String reasonDeduct;
    
    /* duyetdk: them truong noteConfig */
    String noteConfig;
    public UpdateTaskIn(String subId, String taskStaffMngtId, String telServiceId, String taskStaffProgress, String isSystemError, String note, String loginName, String shopCodeLogin, String reasonId, String complaintResult, String satisfileLevel, String compCauseId, String reasonGroup) {
        this.subId = subId;
        this.taskStaffMngtId = taskStaffMngtId;
        this.telServiceId = telServiceId;
        this.taskStaffProgress = taskStaffProgress;
        this.isSystemError = isSystemError;
        this.note = note;
        this.loginName = loginName;
        this.shopCodeLogin = shopCodeLogin;
        this.reasonId = reasonId;
        this.complaintResult = complaintResult;
        this.satisfileLevel = satisfileLevel;
        this.compCauseId = compCauseId;
        this.reasonGroup = reasonGroup;
    }

    public UpdateTaskIn() {
    }

    public String getIsDeductTime() {
        return isDeductTime;
    }

    public void setIsDeductTime(String isDeductTime) {
        this.isDeductTime = isDeductTime;
    }

    public String getStartTimeDeduct() {
        return startTimeDeduct;
    }

    public void setStartTimeDeduct(String startTimeDeduct) {
        this.startTimeDeduct = startTimeDeduct;
    }

    public String getEndTimeDeduct() {
        return endTimeDeduct;
    }

    public void setEndTimeDeduct(String endTimeDeduct) {
        this.endTimeDeduct = endTimeDeduct;
    }

    public String getReasonDeduct() {
        return reasonDeduct;
    }

    public void setReasonDeduct(String reasonDeduct) {
        this.reasonDeduct = reasonDeduct;
    }

    public String getReasonGroup() {
        return reasonGroup;
    }

    public void setReasonGroup(String reasonGroup) {
        this.reasonGroup = reasonGroup;
    }
    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public String getTaskStaffMngtId() {
        return taskStaffMngtId;
    }

    public void setTaskStaffMngtId(String taskStaffMngtId) {
        this.taskStaffMngtId = taskStaffMngtId;
    }

    public String getTelServiceId() {
        return telServiceId;
    }

    public void setTelServiceId(String telServiceId) {
        this.telServiceId = telServiceId;
    }

    public String getTaskStaffProgress() {
        return taskStaffProgress;
    }

    public void setTaskStaffProgress(String taskStaffProgress) {
        this.taskStaffProgress = taskStaffProgress;
    }

    public String getIsSystemError() {
        return isSystemError;
    }

    public void setIsSystemError(String isSystemError) {
        this.isSystemError = isSystemError;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getShopCodeLogin() {
        return shopCodeLogin;
    }

    public void setShopCodeLogin(String shopCodeLogin) {
        this.shopCodeLogin = shopCodeLogin;
    }

    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }

    public String getComplaintResult() {
        return complaintResult;
    }

    public void setComplaintResult(String complaintResult) {
        this.complaintResult = complaintResult;
    }

    public String getSatisfileLevel() {
        return satisfileLevel;
    }

    public void setSatisfileLevel(String satisfileLevel) {
        this.satisfileLevel = satisfileLevel;
    }

    public String getCompCauseId() {
        return compCauseId;
    }

    public void setCompCauseId(String compCauseId) {
        this.compCauseId = compCauseId;
    }

    public String getNoteConfig() {
        return noteConfig;
    }

    public void setNoteConfig(String noteConfig) {
        this.noteConfig = noteConfig;
    }
    
}
