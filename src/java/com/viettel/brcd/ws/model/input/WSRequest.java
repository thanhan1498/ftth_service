package com.viettel.brcd.ws.model.input;

public class WSRequest {

    private Long custId;
    private Long reasonId;
    private Long reqId;
    private String noticeCharge;
    private String printMethod;
    private String payMethod;
    private String bankCode;
    private String account;
    private String accountName;
    private String bankContractNo;
    private String bankContractDate;
    private String nickName;
    private String nickDomain;
    private String receiveInvoice;
    private String shopCode;
    private String staffCode;
    private String email;
    private String telMobile;
    private String telFax;
    private String province;
    private String district;
    private String precinct;
    private String address;
    private Long shopId;
    private Long staffId;
    private String custName;
    private String idNo;
    private String serviceAlias;
    private String isdn;
    private Long reqStatus;
    private String fromDate;
    private String toDate;
    //duyetdk
    private String dateDeploy;
    private String reasonDelay;

    public String getDateDeploy() {
        return dateDeploy;
    }

    public void setDateDeploy(String dateDeploy) {
        this.dateDeploy = dateDeploy;
    }

    public String getReasonDelay() {
        return reasonDelay;
    }

    public void setReasonDelay(String reasonDelay) {
        this.reasonDelay = reasonDelay;
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public Long getReqId() {
        return reqId;
    }

    public void setReqId(Long reqId) {
        this.reqId = reqId;
    }

    public String getNoticeCharge() {
        return noticeCharge;
    }

    public void setNoticeCharge(String noticeCharge) {
        this.noticeCharge = noticeCharge;
    }

    public String getPrintMethod() {
        return printMethod;
    }

    public void setPrintMethod(String printMethod) {
        this.printMethod = printMethod;
    }

    public String getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(String payMethod) {
        this.payMethod = payMethod;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getBankContractNo() {
        return bankContractNo;
    }

    public void setBankContractNo(String bankContractNo) {
        this.bankContractNo = bankContractNo;
    }

    public String getBankContractDate() {
        return bankContractDate;
    }

    public void setBankContractDate(String bankContractDate) {
        this.bankContractDate = bankContractDate;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickDomain() {
        return nickDomain;
    }

    public void setNickDomain(String nickDomain) {
        this.nickDomain = nickDomain;
    }

    public String getReceiveInvoice() {
        return receiveInvoice;
    }

    public void setReceiveInvoice(String receiveInvoice) {
        this.receiveInvoice = receiveInvoice;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelMobile() {
        return telMobile;
    }

    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }

    public String getTelFax() {
        return telFax;
    }

    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getServiceAlias() {
        return serviceAlias;
    }

    public void setServiceAlias(String serviceAlias) {
        this.serviceAlias = serviceAlias;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Long getReqStatus() {
        return reqStatus;
    }

    public void setReqStatus(Long reqStatus) {
        this.reqStatus = reqStatus;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
