/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.input.regOnline;

/**
 *
 * @author cuongdm
 */
public class RegOnlineInput {

    protected String customerName;
    protected String customerPhone;
    protected String customerMail;
    protected String province;
    protected String address;
    protected String speed;
    protected String packageDetail;
    protected String introductionCode;
    protected String ip;
    private Long type;
    //daibq bo sung
    private String isAutoAssign;

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the customerPhone
     */
    public String getCustomerPhone() {
        return customerPhone;
    }

    /**
     * @param customerPhone the customerPhone to set
     */
    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    /**
     * @return the customerMail
     */
    public String getCustomerMail() {
        return customerMail;
    }

    /**
     * @param customerMail the customerMail to set
     */
    public void setCustomerMail(String customerMail) {
        this.customerMail = customerMail;
    }

    /**
     * @return the province
     */
    public String getProvince() {
        return province;
    }

    /**
     * @param province the province to set
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the speed
     */
    public String getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(String speed) {
        this.speed = speed;
    }

    /**
     * @return the packageDetail
     */
    public String getPackageDetail() {
        return packageDetail;
    }

    /**
     * @param packageDetail the packageDetail to set
     */
    public void setPackageDetail(String packageDetail) {
        this.packageDetail = packageDetail;
    }

    /**
     * @return the introductionCode
     */
    public String getIntroductionCode() {
        return introductionCode;
    }

    /**
     * @param introductionCode the introductionCode to set
     */
    public void setIntroductionCode(String introductionCode) {
        this.introductionCode = introductionCode;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the type
     */
    public Long getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Long type) {
        this.type = type;
    }

    public String getIsAutoAssign() {
        return isAutoAssign;
    }

    public void setIsAutoAssign(String isAutoAssign) {
        this.isAutoAssign = isAutoAssign;
    }
}
