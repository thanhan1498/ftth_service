/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.AccountInfra;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author nhandvs
 */
public class AccountByStaffOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement
    private Long totalPage;
    @XmlElement
    private Integer totalSize;
    @XmlElement
    
    private List<AccountInfra> lstAccount;

    public Long getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Long totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Integer totalSize) {
        this.totalSize = totalSize;
    }

    
     public AccountByStaffOut(String errorCode, String errorDecription, List<AccountInfra> lstAccount) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.lstAccount = lstAccount;
    }

    public AccountByStaffOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public AccountByStaffOut() {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<AccountInfra> getLstAccount() {
        return lstAccount;
    }

    public void setLstAccount(List<AccountInfra> lstAccount) {
        this.lstAccount = lstAccount;
    }
    
}
