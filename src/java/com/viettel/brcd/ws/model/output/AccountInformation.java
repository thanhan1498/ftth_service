/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author cuongdm
 */
public class AccountInformation {
    private String custName;
    private String deployAddress;
    private String productCode;
    private String password;
    private String uplinkAccount;
    private String actStatusText;
    private String busTypeObjValue;
    private String contractId;
    private String custIdNo;
    private String serviceType;
    private String telecomServiceAlias;
    private String speed;
    private String status;
    private String subId;
    private String contactMobile;
    private String contactTelFax;
    private String ipStatic;
    private String ipBlock;

    /**
     * @return the custName
     */
    public String getCustName() {
        return custName;
    }

    /**
     * @param custName the custName to set
     */
    public void setCustName(String custName) {
        this.custName = custName;
    }

    /**
     * @return the deployAddress
     */
    public String getDeployAddress() {
        return deployAddress;
    }

    /**
     * @param deployAddress the deployAddress to set
     */
    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the uplinkAccount
     */
    public String getUplinkAccount() {
        return uplinkAccount;
    }

    /**
     * @param uplinkAccount the uplinkAccount to set
     */
    public void setUplinkAccount(String uplinkAccount) {
        this.uplinkAccount = uplinkAccount;
    }

    /**
     * @return the actStatusText
     */
    public String getActStatusText() {
        return actStatusText;
    }

    /**
     * @param actStatusText the actStatusText to set
     */
    public void setActStatusText(String actStatusText) {
        this.actStatusText = actStatusText;
    }

    /**
     * @return the busTypeObjValue
     */
    public String getBusTypeObjValue() {
        return busTypeObjValue;
    }

    /**
     * @param busTypeObjValue the busTypeObjValue to set
     */
    public void setBusTypeObjValue(String busTypeObjValue) {
        this.busTypeObjValue = busTypeObjValue;
    }

    /**
     * @return the contractId
     */
    public String getContractId() {
        return contractId;
    }

    /**
     * @param contractId the contractId to set
     */
    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    /**
     * @return the custIdNo
     */
    public String getCustIdNo() {
        return custIdNo;
    }

    /**
     * @param custIdNo the custIdNo to set
     */
    public void setCustIdNo(String custIdNo) {
        this.custIdNo = custIdNo;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    /**
     * @return the telecomServiceAlias
     */
    public String getTelecomServiceAlias() {
        return telecomServiceAlias;
    }

    /**
     * @param telecomServiceAlias the telecomServiceAlias to set
     */
    public void setTelecomServiceAlias(String telecomServiceAlias) {
        this.telecomServiceAlias = telecomServiceAlias;
    }

    /**
     * @return the speed
     */
    public String getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(String speed) {
        this.speed = speed;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the subId
     */
    public String getSubId() {
        return subId;
    }

    /**
     * @param subId the subId to set
     */
    public void setSubId(String subId) {
        this.subId = subId;
    }

    /**
     * @return the contactMobile
     */
    public String getContactMobile() {
        return contactMobile;
    }

    /**
     * @param contactMobile the contactMobile to set
     */
    public void setContactMobile(String contactMobile) {
        this.contactMobile = contactMobile;
    }

    /**
     * @return the contactTelFax
     */
    public String getContactTelFax() {
        return contactTelFax;
    }

    /**
     * @param contactTelFax the contactTelFax to set
     */
    public void setContactTelFax(String contactTelFax) {
        this.contactTelFax = contactTelFax;
    }

    /**
     * @return the ipStatic
     */
    public String getIpStatic() {
        return ipStatic;
    }

    /**
     * @param ipStatic the ipStatic to set
     */
    public void setIpStatic(String ipStatic) {
        this.ipStatic = ipStatic;
    }

    /**
     * @return the ipBlock
     */
    public String getIpBlock() {
        return ipBlock;
    }

    /**
     * @param ipBlock the ipBlock to set
     */
    public void setIpBlock(String ipBlock) {
        this.ipBlock = ipBlock;
    }
}
