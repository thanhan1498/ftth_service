/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.api.supplier.nims.getInfrasGpon.GetPortBySplitterResultForm;
import com.viettel.bccs.api.supplier.nims.getInfrasGpon.GetSplitterBySubNodeResultForm;
import com.viettel.bccs.api.supplier.nims.getInfrasGpon.GetSubNodeByBranchNodeResultForm;
import com.viettel.bccs.cm.model.BoxCode;
import com.viettel.bccs.cm.model.ODFIndoorCode;
import com.viettel.bccs.cm.model.portOnBox;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author nhandv
 */
public class AccountInfrasOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement
    private String stationCode;
    @XmlElement
    private String deviceCode;
    @XmlElement
    private String devicePort;
    @XmlElement
    private String odfIndoorCode;
    @XmlElement
    private Long odfIndoorId;
    @XmlElement
    private String odfIndoorPort;
    @XmlElement
    private String odfOutdoorCode;
    @XmlElement
    private String odfOutdoorPort;
    @XmlElement
    private Long odfOutdoorId;
    @XmlElement
    private String portLogic;
    @XmlElement
    private String splitterPort;
     @XmlElement
    private String splitterPortID;
    @XmlElement
    private String snCode;
    @XmlElement
    private String splitterCode;
    @XmlElement
    private String boxCableCode;
    @XmlElement
    private String boxCablePort;
    @XmlElement
    private String lengthCable;
    @XmlElement
    private String latX;
    @XmlElement
    private String latY;
    @XmlElement
    private String latXSN;
    @XmlElement
    private String latYSN;
    @XmlElement
    private String infraType;
    @XmlElement
    private String subType;
    @XmlElement
    private String setupAddress;
    @XmlElement
    private String subAddress;
    @XmlElement
    private String portId;
    @XmlElement
    private String portCode;
    @XmlElement
    private Long stationId;
    @XmlElement
    private Long deviceId;
    @XmlElement
    private Long portNoMdfA;
    @XmlElement
    private Long portNoMdfB;
    @XmlElement
    private Long secondCouplerNo;
    
    @XmlElement
    private String isSplitter;
    // protected Long stationId;
    // protected Long deviceId;
    // lay danh sach nhanh con GPON
    @XmlElement
    private List<GetSubNodeByBranchNodeResultForm> lstSubBranchGpon;
    // lay danh sach spitter GPON
    @XmlElement
    private List<GetSplitterBySubNodeResultForm> lstSpitterGpon;
    // lay danh sach port GPON
    @XmlElement
    private List<GetPortBySplitterResultForm> lstPortGpon;
    @XmlElement
    private List<ODFIndoorCode> lstODFIndoorCode;

    @XmlElement
    private List<ODFIndoorCode> lstODFIndoorPort;

    @XmlElement
    private List<ODFIndoorCode> lstODFOutdoorCode;

    @XmlElement
    private List<ODFIndoorCode> lstODFOutdoorPort;

    @XmlElement
    private List<BoxCode> lstBoxCode;

    @XmlElement
    private List<portOnBox> lstBoxPort;

    public String getSplitterPortID() {
        return splitterPortID;
    }

    public void setSplitterPortID(String splitterPortID) {
        this.splitterPortID = splitterPortID;
    }
    
    public Long getOdfOutdoorId() {
        return odfOutdoorId;
    }

    public void setOdfOutdoorId(Long odfOutdoorId) {
        this.odfOutdoorId = odfOutdoorId;
    }

    public String getIsSplitter() {
        return isSplitter;
    }

    public void setIsSplitter(String isSplitter) {
        this.isSplitter = isSplitter;
    }

    public Long getSecondCouplerNo() {
        return secondCouplerNo;
    }

    public void setSecondCouplerNo(Long secondCouplerNo) {
        this.secondCouplerNo = secondCouplerNo;
    }

    public Long getPortNoMdfA() {
        return portNoMdfA;
    }

    public void setPortNoMdfA(Long portNoMdfA) {
        this.portNoMdfA = portNoMdfA;
    }

    public Long getPortNoMdfB() {
        return portNoMdfB;
    }

    public void setPortNoMdfB(Long portNoMdfB) {
        this.portNoMdfB = portNoMdfB;
    }

    public String getSplitterPort() {
        return splitterPort;
    }

    public void setSplitterPort(String splitterPort) {
        this.splitterPort = splitterPort;
    }

    public Long getOdfIndoorId() {
        return odfIndoorId;
    }

    public void setOdfIndoorId(Long odfIndoorId) {
        this.odfIndoorId = odfIndoorId;
    }

    public String getPortCode() {
        return portCode;
    }

    public void setPortCode(String portCode) {
        this.portCode = portCode;
    }

    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getSetupAddress() {
        return setupAddress;
    }

    public void setSetupAddress(String setupAddress) {
        this.setupAddress = setupAddress;
    }

    public String getSubAddress() {
        return subAddress;
    }

    public void setSubAddress(String subAddress) {
        this.subAddress = subAddress;
    }

    public String getPortId() {
        return portId;
    }

    public void setPortId(String portId) {
        this.portId = portId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public String getInfraType() {
        return infraType;
    }

    public void setInfraType(String infraType) {
        this.infraType = infraType;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getDevicePort() {
        return devicePort;
    }

    public void setDevicePort(String devicePort) {
        this.devicePort = devicePort;
    }

    public String getOdfIndoorCode() {
        return odfIndoorCode;
    }

    public void setOdfIndoorCode(String odfIndoorCode) {
        this.odfIndoorCode = odfIndoorCode;
    }

    public String getOdfIndoorPort() {
        return odfIndoorPort;
    }

    public void setOdfIndoorPort(String odfIndoorPort) {
        this.odfIndoorPort = odfIndoorPort;
    }

    public String getOdfOutdoorCode() {
        return odfOutdoorCode;
    }

    public void setOdfOutdoorCode(String odfOutdoorCode) {
        this.odfOutdoorCode = odfOutdoorCode;
    }

    public String getOdfOutdoorPort() {
        return odfOutdoorPort;
    }

    public void setOdfOutdoorPort(String odfOutdoorPort) {
        this.odfOutdoorPort = odfOutdoorPort;
    }

    public String getPortLogic() {
        return portLogic;
    }

    public void setPortLogic(String portLogic) {
        this.portLogic = portLogic;
    }

    public String getSnCode() {
        return snCode;
    }

    public void setSnCode(String snCode) {
        this.snCode = snCode;
    }

    public String getSplitterCode() {
        return splitterCode;
    }

    public void setSplitterCode(String splitterCode) {
        this.splitterCode = splitterCode;
    }

    public String getBoxCableCode() {
        return boxCableCode;
    }

    public void setBoxCableCode(String boxCableCode) {
        this.boxCableCode = boxCableCode;
    }

    public String getBoxCablePort() {
        return boxCablePort;
    }

    public void setBoxCablePort(String boxCablePort) {
        this.boxCablePort = boxCablePort;
    }

    public String getLengthCable() {
        return lengthCable;
    }

    public void setLengthCable(String lengthCable) {
        this.lengthCable = lengthCable;
    }

    public AccountInfrasOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public AccountInfrasOut() {

    }

    public List<GetSubNodeByBranchNodeResultForm> getLstSubBranchGpon() {
        return lstSubBranchGpon;
    }

    public void setLstSubBranchGpon(List<GetSubNodeByBranchNodeResultForm> lstSubBranchGpon) {
        this.lstSubBranchGpon = lstSubBranchGpon;
    }

    public List<GetSplitterBySubNodeResultForm> getLstSpitterGpon() {
        return lstSpitterGpon;
    }

    public void setLstSpitterGpon(List<GetSplitterBySubNodeResultForm> lstSpitterGpon) {
        this.lstSpitterGpon = lstSpitterGpon;
    }

    public List<GetPortBySplitterResultForm> getLstPortGpon() {
        return lstPortGpon;
    }

    public void setLstPortGpon(List<GetPortBySplitterResultForm> lstPortGpon) {
        this.lstPortGpon = lstPortGpon;
    }

    public String getLatX() {
        return latX;
    }

    public void setLatX(String latX) {
        this.latX = latX;
    }

    public String getLatY() {
        return latY;
    }

    public void setLatY(String latY) {
        this.latY = latY;
    }

    public String getLatXSN() {
        return latXSN;
    }

    public void setLatXSN(String latXSN) {
        this.latXSN = latXSN;
    }

    public String getLatYSN() {
        return latYSN;
    }

    public void setLatYSN(String latYSN) {
        this.latYSN = latYSN;
    }

    public List<ODFIndoorCode> getLstODFIndoorCode() {
        return lstODFIndoorCode;
    }

    public void setLstODFIndoorCode(List<ODFIndoorCode> lstODFIndoorCode) {
        this.lstODFIndoorCode = lstODFIndoorCode;
    }

    public List<ODFIndoorCode> getLstODFIndoorPort() {
        return lstODFIndoorPort;
    }

    public void setLstODFIndoorPort(List<ODFIndoorCode> lstODFIndoorPort) {
        this.lstODFIndoorPort = lstODFIndoorPort;
    }

    public List<ODFIndoorCode> getLstODFOutdoorCode() {
        return lstODFOutdoorCode;
    }

    public void setLstODFOutdoorCode(List<ODFIndoorCode> lstODFOutdoorCode) {
        this.lstODFOutdoorCode = lstODFOutdoorCode;
    }

    public List<ODFIndoorCode> getLstODFOutdoorPort() {
        return lstODFOutdoorPort;
    }

    public void setLstODFOutdoorPort(List<ODFIndoorCode> lstODFOutdoorPort) {
        this.lstODFOutdoorPort = lstODFOutdoorPort;
    }

    public List<BoxCode> getLstBoxCode() {
        return lstBoxCode;
    }

    public void setLstBoxCode(List<BoxCode> lstBoxCode) {
        this.lstBoxCode = lstBoxCode;
    }

    public List<portOnBox> getLstBoxPort() {
        return lstBoxPort;
    }

    public void setLstBoxPort(List<portOnBox> lstBoxPort) {
        this.lstBoxPort = lstBoxPort;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

}
