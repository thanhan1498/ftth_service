/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.ApDomain;
import java.util.List;

/**
 *
 * @author linhlh2
 */
public class ApDomainOut {

    private String errorCode;
    private String errorDecription;
    private List<ApDomain> apDomains;

    public ApDomainOut() {
    }

    public ApDomainOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ApDomainOut(String errorCode, String errorDecription, List<ApDomain> apDomains) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.apDomains = apDomains;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ApDomain> getApDomains() {
        return apDomains;
    }

    public void setApDomains(List<ApDomain> apDomains) {
        this.apDomains = apDomains;
    }
}
