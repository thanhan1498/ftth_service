/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.Bank;
import java.util.List;

/**
 *
 * @author linhlh2
 */
public class BankOut {

    private String errorCode;
    private String errorDecription;
    private List<Bank> banks;

    public BankOut() {
    }

    public BankOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public BankOut(String errorCode, String errorDecription, List<Bank> banks) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.banks = banks;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<Bank> getBanks() {
        return banks;
    }

    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }
}
