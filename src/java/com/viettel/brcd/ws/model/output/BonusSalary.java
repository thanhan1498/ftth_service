/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author cuongdm
 */
public class BonusSalary {

    private Double totalTime;
    private Double currentDeadline;
    private Double currentBonus;
    private Double nextDeadline;
    private Double nextBonus;

    /**
     * @return the totalTime
     */
    public Double getTotalTime() {
        return totalTime;
    }

    /**
     * @param totalTime the totalTime to set
     */
    public void setTotalTime(Double totalTime) {
        this.totalTime = totalTime;
    }

    /**
     * @return the currentDeadline
     */
    public Double getCurrentDeadline() {
        return currentDeadline;
    }

    /**
     * @param currentDeadline the currentDeadline to set
     */
    public void setCurrentDeadline(Double currentDeadline) {
        this.currentDeadline = currentDeadline;
    }

    /**
     * @return the currentBonus
     */
    public Double getCurrentBonus() {
        return currentBonus;
    }

    /**
     * @param currentBonus the currentBonus to set
     */
    public void setCurrentBonus(Double currentBonus) {
        this.currentBonus = currentBonus;
    }

    /**
     * @return the nextDeadline
     */
    public Double getNextDeadline() {
        return nextDeadline;
    }

    /**
     * @param nextDeadline the nextDeadline to set
     */
    public void setNextDeadline(Double nextDeadline) {
        this.nextDeadline = nextDeadline;
    }

    /**
     * @return the nextBonus
     */
    public Double getNextBonus() {
        return nextBonus;
    }

    /**
     * @param nextBonus the nextBonus to set
     */
    public void setNextBonus(Double nextBonus) {
        this.nextBonus = nextBonus;
    }
}
