/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author cuongdm
 */
public class BrasIpPool {

    private String brasIp;

    /**
     * @return the brasIp
     */
    public String getBrasIp() {
        return brasIp;
    }

    /**
     * @param brasIp the brasIp to set
     */
    public void setBrasIp(String brasIp) {
        this.brasIp = brasIp;
    }
}
