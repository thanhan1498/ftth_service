/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author cuongdm
 */
public class BroadbandTabInfo {

    private String title;
    private Long newTask;
    private Long complainTask;
    private Long total;

    public BroadbandTabInfo() {
    }

    public BroadbandTabInfo(String title, Long newTask, Long complainTask) {
        this.title = title;
        this.newTask = newTask;
        this.complainTask = complainTask;
        this.total = newTask + complainTask;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the newTask
     */
    public Long getNewTask() {
        return newTask;
    }

    /**
     * @param newTask the newTask to set
     */
    public void setNewTask(Long newTask) {
        this.newTask = newTask;
    }

    /**
     * @return the complainTask
     */
    public Long getComplainTask() {
        return complainTask;
    }

    /**
     * @param complainTask the complainTask to set
     */
    public void setComplainTask(Long complainTask) {
        this.complainTask = complainTask;
    }

    /**
     * @return the total
     */
    public Long getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(Long total) {
        this.total = total;
    }
}
