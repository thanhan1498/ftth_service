/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.SubBundleTv;
import java.util.List;

/**
 *
 * @author cuongdm
 */
public class BundleTVInfo extends SubBundleTv{

    /**
     * @return the lstPackage
     */
    public List<ListDataTv> getLstPackage() {
        return lstPackage;
    }

    /**
     * @param lstPackage the lstPackage to set
     */
    public void setLstPackage(List<ListDataTv> lstPackage) {
        this.lstPackage = lstPackage;
    }
    private List<ListDataTv> lstPackage;
}
