/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.im.database.BO.CableBox;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nguyen Ngoc Viet <vietnn6@viettel.com.vn>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "inforInfrastructureOut")
public class CableBoxOut {

    @XmlElement
    String errorCode;
    @XmlElement
    String errorDecription;
    //Lay danh sach cap nhanh
    @XmlElement(name = "cableBox")
    List<CableBox> lstCableBox;
    
    public CableBoxOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public CableBoxOut() {
    }
    
    

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<CableBox> getLstCableBox() {
        return lstCableBox;
    }

    public void setLstCableBox(List<CableBox> lstCableBox) {
        this.lstCableBox = lstCableBox;
    }


}
