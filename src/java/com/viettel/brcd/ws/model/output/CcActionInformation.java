/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author cuongdm
 */
public class CcActionInformation {

    //complain
    private String dates;
    private String time;
    private String content;
    private String compType;
    private String acceptUser;
    private String resultContent;
    private Integer reCompNumber;
    private Long complainId;
    private String status;
    private String statusCode;
    private String note;
    private String endUser;
    private String custLimitDate;  //Ngay hen tra loi khach hang
    private String resLimitDate;  //Ngay hen phan hoi khach hang
    private String compLevel;
    private String subNo;
    private String subId;
    private String serviceType;
    private String compTypeId;
    private String priorityId;
    private String limitCustDate;
    private String compCauseName;
    private String typeAgent; //1: nhom agent, 0: ko la nhom agent
    private String numberComplain; // so lan khach hang nhan No
    private String account; // so lan khach hang nhan No
    private String acceptDate; // so lan khach hang nhan No
    //Biiling collection
    private String appliedCycle;
    private String collectionGroupName;
    private String collectionStaffName;
    private String collectionStaffCode;
    private String collectionStaffTel;

    /**
     * @return the dates
     */
    public String getDates() {
        return dates;
    }

    /**
     * @param dates the dates to set
     */
    public void setDates(String dates) {
        this.dates = dates;
    }

    /**
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the compType
     */
    public String getCompType() {
        return compType;
    }

    /**
     * @param compType the compType to set
     */
    public void setCompType(String compType) {
        this.compType = compType;
    }

    /**
     * @return the acceptUser
     */
    public String getAcceptUser() {
        return acceptUser;
    }

    /**
     * @param acceptUser the acceptUser to set
     */
    public void setAcceptUser(String acceptUser) {
        this.acceptUser = acceptUser;
    }

    /**
     * @return the resultContent
     */
    public String getResultContent() {
        return resultContent;
    }

    /**
     * @param resultContent the resultContent to set
     */
    public void setResultContent(String resultContent) {
        this.resultContent = resultContent;
    }

    /**
     * @return the reCompNumber
     */
    public Integer getReCompNumber() {
        return reCompNumber;
    }

    /**
     * @param reCompNumber the reCompNumber to set
     */
    public void setReCompNumber(Integer reCompNumber) {
        this.reCompNumber = reCompNumber;
    }

    /**
     * @return the complainId
     */
    public Long getComplainId() {
        return complainId;
    }

    /**
     * @param complainId the complainId to set
     */
    public void setComplainId(Long complainId) {
        this.complainId = complainId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the endUser
     */
    public String getEndUser() {
        return endUser;
    }

    /**
     * @param endUser the endUser to set
     */
    public void setEndUser(String endUser) {
        this.endUser = endUser;
    }

    /**
     * @return the custLimitDate
     */
    public String getCustLimitDate() {
        return custLimitDate;
    }

    /**
     * @param custLimitDate the custLimitDate to set
     */
    public void setCustLimitDate(String custLimitDate) {
        this.custLimitDate = custLimitDate;
    }

    /**
     * @return the resLimitDate
     */
    public String getResLimitDate() {
        return resLimitDate;
    }

    /**
     * @param resLimitDate the resLimitDate to set
     */
    public void setResLimitDate(String resLimitDate) {
        this.resLimitDate = resLimitDate;
    }

    /**
     * @return the compLevel
     */
    public String getCompLevel() {
        return compLevel;
    }

    /**
     * @param compLevel the compLevel to set
     */
    public void setCompLevel(String compLevel) {
        this.compLevel = compLevel;
    }

    /**
     * @return the subNo
     */
    public String getSubNo() {
        return subNo;
    }

    /**
     * @param subNo the subNo to set
     */
    public void setSubNo(String subNo) {
        this.subNo = subNo;
    }

    /**
     * @return the subId
     */
    public String getSubId() {
        return subId;
    }

    /**
     * @param subId the subId to set
     */
    public void setSubId(String subId) {
        this.subId = subId;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    /**
     * @return the compTypeId
     */
    public String getCompTypeId() {
        return compTypeId;
    }

    /**
     * @param compTypeId the compTypeId to set
     */
    public void setCompTypeId(String compTypeId) {
        this.compTypeId = compTypeId;
    }

    /**
     * @return the priorityId
     */
    public String getPriorityId() {
        return priorityId;
    }

    /**
     * @param priorityId the priorityId to set
     */
    public void setPriorityId(String priorityId) {
        this.priorityId = priorityId;
    }

    /**
     * @return the limitCustDate
     */
    public String getLimitCustDate() {
        return limitCustDate;
    }

    /**
     * @param limitCustDate the limitCustDate to set
     */
    public void setLimitCustDate(String limitCustDate) {
        this.limitCustDate = limitCustDate;
    }

    /**
     * @return the compCauseName
     */
    public String getCompCauseName() {
        return compCauseName;
    }

    /**
     * @param compCauseName the compCauseName to set
     */
    public void setCompCauseName(String compCauseName) {
        this.compCauseName = compCauseName;
    }

    /**
     * @return the typeAgent
     */
    public String getTypeAgent() {
        return typeAgent;
    }

    /**
     * @param typeAgent the typeAgent to set
     */
    public void setTypeAgent(String typeAgent) {
        this.typeAgent = typeAgent;
    }

    /**
     * @return the numberComplain
     */
    public String getNumberComplain() {
        return numberComplain;
    }

    /**
     * @param numberComplain the numberComplain to set
     */
    public void setNumberComplain(String numberComplain) {
        this.numberComplain = numberComplain;
    }

    /**
     * @return the appliedCycle
     */
    public String getAppliedCycle() {
        return appliedCycle;
    }

    /**
     * @param appliedCycle the appliedCycle to set
     */
    public void setAppliedCycle(String appliedCycle) {
        this.appliedCycle = appliedCycle;
    }

    /**
     * @return the collectionGroupName
     */
    public String getCollectionGroupName() {
        return collectionGroupName;
    }

    /**
     * @param collectionGroupName the collectionGroupName to set
     */
    public void setCollectionGroupName(String collectionGroupName) {
        this.collectionGroupName = collectionGroupName;
    }

    /**
     * @return the collectionStaffName
     */
    public String getCollectionStaffName() {
        return collectionStaffName;
    }

    /**
     * @param collectionStaffName the collectionStaffName to set
     */
    public void setCollectionStaffName(String collectionStaffName) {
        this.collectionStaffName = collectionStaffName;
    }

    /**
     * @return the collectionStaffCode
     */
    public String getCollectionStaffCode() {
        return collectionStaffCode;
    }

    /**
     * @param collectionStaffCode the collectionStaffCode to set
     */
    public void setCollectionStaffCode(String collectionStaffCode) {
        this.collectionStaffCode = collectionStaffCode;
    }

    /**
     * @return the collectionStaffTel
     */
    public String getCollectionStaffTel() {
        return collectionStaffTel;
    }

    /**
     * @param collectionStaffTel the collectionStaffTel to set
     */
    public void setCollectionStaffTel(String collectionStaffTel) {
        this.collectionStaffTel = collectionStaffTel;
    }

    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * @return the acceptDate
     */
    public String getAcceptDate() {
        return acceptDate;
    }

    /**
     * @param acceptDate the acceptDate to set
     */
    public void setAcceptDate(String acceptDate) {
        this.acceptDate = acceptDate;
    }
}
