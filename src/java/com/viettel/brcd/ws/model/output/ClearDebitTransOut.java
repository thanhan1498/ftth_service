/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ClearDebitTransOut")
public class ClearDebitTransOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement
    private Long transactionId;
    @XmlElement
    private String txPaymentTokenId;
    @XmlElement
    private Long errDebitId;

    public ClearDebitTransOut() {
    }

    public ClearDebitTransOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ClearDebitTransOut(String errorCode, String errorDecription, Long transactionId, String txPaymentTokenId) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.transactionId = transactionId;
        this.txPaymentTokenId = txPaymentTokenId;
    }

    public ClearDebitTransOut(String errorCode, String errorDecription, Long errDebitId) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.errDebitId = errDebitId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getTxPaymentTokenId() {
        return txPaymentTokenId;
    }

    public void setTxPaymentTokenId(String txPaymentTokenId) {
        this.txPaymentTokenId = txPaymentTokenId;
    }

    public Long getErrDebitId() {
        return errDebitId;
    }

    public void setErrDebitId(Long errDebitId) {
        this.errDebitId = errDebitId;
    }
    
}
