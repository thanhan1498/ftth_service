/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.TechnicalConnector;
import com.viettel.bccs.cm.model.TechnicalStation;
import com.viettel.bccs.cm.model.ftth.CustomerBTSConnectorBean;
import com.viettel.bccs.cm.model.ftth.InterruptReasonDetail;
import com.viettel.bccs.cm.model.ftth.InterruptReasonType;
import com.viettel.bccs.cm.model.ftth.Interruption;
import java.util.List;

/**
 *
 * @author partner1
 */
public class CmFtthWSOut {
    private String errorCode;
    private String errorDecription;
    private Shop curBranchUser;
    private List<InterruptReasonType> lstReasonTypes;
    private List<InterruptReasonDetail> lstReasonDetails;
    private List<Shop> lstBranch;
    private List<Staff> lstStaffOfBranch;
    private List<TechnicalStation> lstTechnicalStation;
    private List<TechnicalConnector> lstTechnicalConnector;
    private List<Interruption> lstInterruption;
    private List<Interruption> lstInterruptionDetail;
    private List<Interruption> lstInterruptionHistory;
    private String token;
    private List<CustomerBTSConnectorBean> lstCustomerBTSConnectorBean;

    public CmFtthWSOut() {
    }

    public CmFtthWSOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public Shop getCurBranchUser() {
        return curBranchUser;
    }

    public void setCurBranchUser(Shop curBranchUser) {
        this.curBranchUser = curBranchUser;
    }

    public List<InterruptReasonDetail> getLstReasonDetails() {
        return lstReasonDetails;
    }

    public void setLstReasonDetails(List<InterruptReasonDetail> lstReasonDetails) {
        this.lstReasonDetails = lstReasonDetails;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<InterruptReasonType> getLstReasonTypes() {
        return lstReasonTypes;
    }

    public void setLstReasonTypes(List<InterruptReasonType> lstReasonTypes) {
        this.lstReasonTypes = lstReasonTypes;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<Shop> getLstBranch() {
        return lstBranch;
    }

    public void setLstBranch(List<Shop> lstBranch) {
        this.lstBranch = lstBranch;
    }

    public List<TechnicalConnector> getLstTechnicalConnector() {
        return lstTechnicalConnector;
    }

    public void setLstTechnicalConnector(List<TechnicalConnector> lstTechnicalConnector) {
        this.lstTechnicalConnector = lstTechnicalConnector;
    }

    public List<TechnicalStation> getLstTechnicalStation() {
        return lstTechnicalStation;
    }

    public void setLstTechnicalStation(List<TechnicalStation> lstTechnicalStation) {
        this.lstTechnicalStation = lstTechnicalStation;
    }

    public List<Interruption> getLstInterruption() {
        return lstInterruption;
    }

    public void setLstInterruption(List<Interruption> lstInterruption) {
        this.lstInterruption = lstInterruption;
    }

    public List<Interruption> getLstInterruptionDetail() {
        return lstInterruptionDetail;
    }

    public void setLstInterruptionDetail(List<Interruption> lstInterruptionDetail) {
        this.lstInterruptionDetail = lstInterruptionDetail;
    }

    public List<Interruption> getLstInterruptionHistory() {
        return lstInterruptionHistory;
    }

    public void setLstInterruptionHistory(List<Interruption> lstInterruptionHistory) {
        this.lstInterruptionHistory = lstInterruptionHistory;
    }

    public List<Staff> getLstStaffOfBranch() {
        return lstStaffOfBranch;
    }

    public void setLstStaffOfBranch(List<Staff> lstStaffOfBranch) {
        this.lstStaffOfBranch = lstStaffOfBranch;
    }

    public List<CustomerBTSConnectorBean> getLstCustomerBTSConnectorBean() {
        return lstCustomerBTSConnectorBean;
    }

    public void setLstCustomerBTSConnectorBean(List<CustomerBTSConnectorBean> lstCustomerBTSConnectorBean) {
        this.lstCustomerBTSConnectorBean = lstCustomerBTSConnectorBean;
    }
    
}
