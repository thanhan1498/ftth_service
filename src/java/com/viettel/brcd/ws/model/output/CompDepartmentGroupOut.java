/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cc.model.CompDepartmentGroup;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CompDepartmentGroupOut")
public class CompDepartmentGroupOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement
    private String limitDate;
    @XmlElement(name = "CompDepartmentGroup")
    private List<CompDepartmentGroup> compDepartmentGroup;

    public CompDepartmentGroupOut() {
    }

    public CompDepartmentGroupOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public CompDepartmentGroupOut(String errorCode, String errorDecription, List<CompDepartmentGroup> compDepartmentGroup) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.compDepartmentGroup = compDepartmentGroup;
    }

    public CompDepartmentGroupOut(String errorCode, String errorDecription, String limitDate, List<CompDepartmentGroup> compDepartmentGroup) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.limitDate = limitDate;
        this.compDepartmentGroup = compDepartmentGroup;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<CompDepartmentGroup> getCompDepartmentGroup() {
        return compDepartmentGroup;
    }

    public void setCompDepartmentGroup(List<CompDepartmentGroup> compDepartmentGroup) {
        this.compDepartmentGroup = compDepartmentGroup;
    }

    public String getLimitDate() {
        return limitDate;
    }

    public void setLimitDate(String limitDate) {
        this.limitDate = limitDate;
    }

}
