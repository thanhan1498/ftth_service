/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cc.model.ComplaintAcceptSource;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ComplaintAcceptSourceOut")
public class ComplaintAcceptSourceOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "ComplaintAcceptSource")
    private List<ComplaintAcceptSource> complaintAcceptSource;

    public ComplaintAcceptSourceOut() {
    }

    public ComplaintAcceptSourceOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ComplaintAcceptSourceOut(String errorCode, String errorDecription, List<ComplaintAcceptSource> complaintAcceptSource) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.complaintAcceptSource = complaintAcceptSource;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ComplaintAcceptSource> getComplaintAcceptSource() {
        return complaintAcceptSource;
    }

    public void setComplaintAcceptSource(List<ComplaintAcceptSource> complaintAcceptSource) {
        this.complaintAcceptSource = complaintAcceptSource;
    }

}
