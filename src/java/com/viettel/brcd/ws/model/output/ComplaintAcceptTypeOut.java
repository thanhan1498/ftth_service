/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cc.model.ComplaintAcceptType;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ComplaintAcceptTypeOut")
public class ComplaintAcceptTypeOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "ComplaintAcceptType")
    private List<ComplaintAcceptType> complaintAcceptType;

    public ComplaintAcceptTypeOut() {
    }

    public ComplaintAcceptTypeOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ComplaintAcceptTypeOut(String errorCode, String errorDecription, List<ComplaintAcceptType> complaintAcceptType) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.complaintAcceptType = complaintAcceptType;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ComplaintAcceptType> getComplaintAcceptType() {
        return complaintAcceptType;
    }

    public void setComplaintAcceptType(List<ComplaintAcceptType> complaintAcceptType) {
        this.complaintAcceptType = complaintAcceptType;
    }
    
}
