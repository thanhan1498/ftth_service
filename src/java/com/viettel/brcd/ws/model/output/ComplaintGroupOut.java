/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cc.model.ComplaintGroup;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ComplaintGroupOut")
public class ComplaintGroupOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "ComplaintGroup")
    private List<ComplaintGroup> complaintGroup;

    public ComplaintGroupOut() {
    }

    public ComplaintGroupOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ComplaintGroupOut(String errorCode, String errorDecription, List<ComplaintGroup> complaintGroup) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.complaintGroup = complaintGroup;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ComplaintGroup> getComplaintGroup() {
        return complaintGroup;
    }

    public void setComplaintGroup(List<ComplaintGroup> complaintGroup) {
        this.complaintGroup = complaintGroup;
    }
    
}
