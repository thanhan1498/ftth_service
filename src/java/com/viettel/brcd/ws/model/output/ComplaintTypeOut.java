/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cc.model.ComplaintType;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ComplaintTypeOut")
public class ComplaintTypeOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "ComplaintType")
    private List<ComplaintType> complaintType;

    public ComplaintTypeOut() {
    }

    public ComplaintTypeOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ComplaintTypeOut(String errorCode, String errorDecription, List<ComplaintType> complaintType) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.complaintType = complaintType;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ComplaintType> getComplaintType() {
        return complaintType;
    }

    public void setComplaintType(List<ComplaintType> complaintType) {
        this.complaintType = complaintType;
    }
    
}
