/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author cuongdm
 */
public class ConnectorInfo {

    private String connectorCode;
    private String infraType;
    private String stationCode;

    public ConnectorInfo() {
    }

    public ConnectorInfo(String connectorCode, String infraType, String stationCode) {
        this.connectorCode = connectorCode;
        this.infraType = infraType;
        this.stationCode = stationCode;
    }

    /**
     * @return the connectorCode
     */
    public String getConnectorCode() {
        return connectorCode;
    }

    /**
     * @param connectorCode the connectorCode to set
     */
    public void setConnectorCode(String connectorCode) {
        this.connectorCode = connectorCode;
    }

    /**
     * @return the infraType
     */
    public String getInfraType() {
        return infraType;
    }

    /**
     * @param infraType the infraType to set
     */
    public void setInfraType(String infraType) {
        this.infraType = infraType;
    }

    /**
     * @return the stationCode
     */
    public String getStationCode() {
        return stationCode;
    }

    /**
     * @param stationCode the stationCode to set
     */
    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }
}
