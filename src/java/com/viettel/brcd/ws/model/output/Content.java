/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author cuongdm
 */
public class Content {

    /*request*/
    private String first_name;
    private String last_name;
    private String birthday;
    private String country_code;
    private String phone_number;
    private String post_code;
    private String address;
    private String page_size;
    private String page_index;
    private String user_unique_id;
    private String device_unique_id;
    private String device_serial_number;
    private String package_unique_id;
    private String account;
    /*response*/
    private String unique_id;
    private String serial_number;
    public Content() {
    }

    public Content(String first_name, String last_name, String birthday, String country_code, String phone_number, String post_code, String address,
            String page_size, String page_index, String user_unique_id, String device_unique_id, String device_serial_number, String package_unique_id) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.birthday = birthday;
        this.country_code = country_code;
        this.phone_number = phone_number;
        this.post_code = post_code;
        this.address = address;
        this.page_size = page_size;
        this.page_index = page_index;
        this.user_unique_id = user_unique_id;
        this.device_unique_id = device_unique_id;
        this.device_serial_number = device_serial_number;
        this.package_unique_id = package_unique_id;
    }

    /**
     * @return the first_name
     */
    public String getFirst_name() {
        return first_name;
    }

    /**
     * @param first_name the first_name to set
     */
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    /**
     * @return the last_name
     */
    public String getLast_name() {
        return last_name;
    }

    /**
     * @param last_name the last_name to set
     */
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    /**
     * @return the birthday
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * @param birthday the birthday to set
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    /**
     * @return the country_code
     */
    public String getCountry_code() {
        return country_code;
    }

    /**
     * @param country_code the country_code to set
     */
    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    /**
     * @return the phone_number
     */
    public String getPhone_number() {
        return phone_number;
    }

    /**
     * @param phone_number the phone_number to set
     */
    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    /**
     * @return the post_code
     */
    public String getPost_code() {
        return post_code;
    }

    /**
     * @param post_code the post_code to set
     */
    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the page_size
     */
    public String getPage_size() {
        return page_size;
    }

    /**
     * @param page_size the page_size to set
     */
    public void setPage_size(String page_size) {
        this.page_size = page_size;
    }

    /**
     * @return the page_index
     */
    public String getPage_index() {
        return page_index;
    }

    /**
     * @param page_index the page_index to set
     */
    public void setPage_index(String page_index) {
        this.page_index = page_index;
    }

    /**
     * @return the user_unique_id
     */
    public String getUser_unique_id() {
        return user_unique_id;
    }

    /**
     * @param user_unique_id the user_unique_id to set
     */
    public void setUser_unique_id(String user_unique_id) {
        this.user_unique_id = user_unique_id;
    }

    /**
     * @return the device_unique_id
     */
    public String getDevice_unique_id() {
        return device_unique_id;
    }

    /**
     * @param device_unique_id the device_unique_id to set
     */
    public void setDevice_unique_id(String device_unique_id) {
        this.device_unique_id = device_unique_id;
    }

    /**
     * @return the device_serial_number
     */
    public String getDevice_serial_number() {
        return device_serial_number;
    }

    /**
     * @param device_serial_number the device_serial_number to set
     */
    public void setDevice_serial_number(String device_serial_number) {
        this.device_serial_number = device_serial_number;
    }

    /**
     * @return the package_unique_id
     */
    public String getPackage_unique_id() {
        return package_unique_id;
    }

    /**
     * @param package_unique_id the package_unique_id to set
     */
    public void setPackage_unique_id(String package_unique_id) {
        this.package_unique_id = package_unique_id;
    }

    /**
     * @return the unique_id
     */
    public String getUnique_id() {
        return unique_id;
    }

    /**
     * @param unique_id the unique_id to set
     */
    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * @return the serial_number
     */
    public String getSerial_number() {
        return serial_number;
    }

    /**
     * @param serial_number the serial_number to set
     */
    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }
}
