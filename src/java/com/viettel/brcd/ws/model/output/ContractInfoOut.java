/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.brcd.ws.model.input.CustomerIn;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author linhlh2
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "contract")
public class ContractInfoOut {

    @XmlElement(name = "customer")
    private CustomerIn customer;
    @XmlElement(name = "subscriber")
    private Subscriber subscriber;
    @XmlElement(name = "infrastruct")
    private Infrastruct infrastruct;
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement
    private String token;

    public ContractInfoOut() {
    }

    public ContractInfoOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public CustomerIn getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerIn customer) {
        this.customer = customer;
    }

    public Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Subscriber subscriber) {
        this.subscriber = subscriber;
    }

    public Infrastruct getInfrastruct() {
        return infrastruct;
    }

    public void setInfrastruct(Infrastruct infrastruct) {
        this.infrastruct = infrastruct;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
