/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author linhlh2
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "contract")
public class ContractOut {

    @XmlElement
    private String requestCode;
    @XmlElement(name = "updateResult")
    private UpdateResultOut updateResult;

    public ContractOut() {
    }

    public ContractOut(String requestCode, UpdateResultOut updateResult) {
        this.requestCode = requestCode;
        this.updateResult = updateResult;
    }

    public String getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(String requestCode) {
        this.requestCode = requestCode;
    }

    public UpdateResultOut getUpdateResult() {
        return updateResult;
    }

    public void setUpdateResult(UpdateResultOut updateResult) {
        this.updateResult = updateResult;
    }
}
