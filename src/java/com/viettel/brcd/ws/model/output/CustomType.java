/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.BusType;

/**
 *
 * @author linhlh2
 */
public class CustomType {

    private String typeCode;
    private String typeName;
    private String custType;

    public CustomType() {
    }

    public CustomType(BusType busType) {
        if (busType != null) {
            this.typeCode = busType.getBusType();
            this.typeName = busType.getName();
            this.custType = busType.getCustType();
        }
    }

    public CustomType(String typeCode, String typeName, String custType) {
        this.typeCode = typeCode;
        this.typeName = typeName;
        this.custType = custType;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }
}
