/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author linhlh2
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "customTypes")
public class CustomTypeOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "customType")
    private List<CustomType> customTypes;
    @XmlElement
    private String token;

    public CustomTypeOut() {
    }

    public CustomTypeOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public CustomTypeOut(String errorCode, String errorDecription, List<CustomType> customTypes) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.customTypes = customTypes;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<CustomType> getCustomTypes() {
        return customTypes;
    }

    public void setCustomTypes(List<CustomType> customTypes) {
        this.customTypes = customTypes;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
