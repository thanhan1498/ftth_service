/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import java.util.List;

/**
 *
 * @author nhandv
 */
public class CustomerInfraResponse {
    private String status;
    private String message;
    private List<customerInfrasDetail> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<customerInfrasDetail> getData() {
        return data;
    }

    public void setData(List<customerInfrasDetail> data) {
        this.data = data;
    }


    
}
