/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import java.util.List;
import javassist.runtime.Inner;

/**
 *
 * @author cuongdm
 */
public class DashboardBroadbanTab extends WSRespone {

    private List<BroadbandTabInfo> listTab;
    public Inner TabInfo;

    /**
     * @return the listTab
     */
    public List<BroadbandTabInfo> getListTab() {
        return listTab;
    }

    /**
     * @param listTab the listTab to set
     */
    public void setListTab(List<BroadbandTabInfo> listTab) {
        this.listTab = listTab;
    }
}
