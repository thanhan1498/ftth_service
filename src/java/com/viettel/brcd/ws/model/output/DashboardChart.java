/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import java.util.List;

/**
 *
 * @author cuongdm
 */
public class DashboardChart {

    private List<ProvinceChart> request;

    /**
     * @return the request
     */
    public List<ProvinceChart> getRequest() {
        return request;
    }

    /**
     * @param request the request to set
     */
    public void setRequest(List<ProvinceChart> request) {
        this.request = request;
    }
}
