/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author cuongdm
 */
public class DataNotification {

    private String tokenId;
    private String app;
    private String title;
    private String message;
    private String account;
    private Long taskMngtId;
    private Long taskStaffId;
    private String actionType;

    public DataNotification() {
    }

    public DataNotification(String deviceId, String app, String title, String message, String account, Long taskMngtId, Long taskStaffId, String actionType) {
        this.tokenId = deviceId;
        this.app = app;
        this.title = title;
        this.message = message;
        this.account = account;
        this.taskMngtId = taskMngtId;
        this.taskStaffId = taskStaffId;
        this.actionType = actionType;
    }

    /**
     * @return the deviceId
     */
    public String getTokenId() {
        return tokenId;
    }

    /**
     * @param deviceId the deviceId to set
     */
    public void setTokenId(String deviceId) {
        this.tokenId = deviceId;
    }

    /**
     * @return the app
     */
    public String getApp() {
        return app;
    }

    /**
     * @param app the app to set
     */
    public void setApp(String app) {
        this.app = app;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * @return the taskMngtId
     */
    public Long getTaskMngtId() {
        return taskMngtId;
    }

    /**
     * @param taskMngtId the taskMngtId to set
     */
    public void setTaskMngtId(Long taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    /**
     * @return the taskStaffId
     */
    public Long getTaskStaffId() {
        return taskStaffId;
    }

    /**
     * @param taskStaffId the taskStaffId to set
     */
    public void setTaskStaffId(Long taskStaffId) {
        this.taskStaffId = taskStaffId;
    }

    /**
     * @return the actionType
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * @param actionType the actionType to set
     */
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
}
