/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "data")
public class DataTopUpPinCode {

    private String isdn;
    private Double amount;
    private String time;
    private String merchantAccount;
    private String requestId;
    private String tid;
    private Long reasonId;
    private String staffCode;
    private String shopCode;
    /*--------------*/
    private int quantity;
    private String stockModelCode;
    private Long saleTransType;
    private String custName;
    private String ip;
    private String arpu;
    private String sms;
    private String voice;
    private String data;
    private String gender;
    private String age;
    private String location;
    private String device;
    private String subType;
    private String roaming;
    private String interCal;
    private String numRecord;
    /*--------------*/
    private String serviceType;
    private String packageCode;
    private String account;
    private int dateExpired;
    private String dealerCode;
    /*--------------*/
    private String reasonTopup;
    private Double amountBasic;
    private Double amountPromotion;
    
    
    /**
     * @return the arpu
     */
    public String getArpu() {
        return arpu;
    }

    /**
     * @param arpu the arpu to set
     */
    public void setArpu(String arpu) {
        this.arpu = arpu;
    }

    /**
     * @return the sms
     */
    public String getSms() {
        return sms;
    }

    /**
     * @param sms the sms to set
     */
    public void setSms(String sms) {
        this.sms = sms;
    }

    /**
     * @return the voice
     */
    public String getVoice() {
        return voice;
    }

    /**
     * @param voice the voice to set
     */
    public void setVoice(String voice) {
        this.voice = voice;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the age
     */
    public String getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(String age) {
        this.age = age;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the device
     */
    public String getDevice() {
        return device;
    }

    /**
     * @param device the device to set
     */
    public void setDevice(String device) {
        this.device = device;
    }

    /**
     * @return the subType
     */
    public String getSubType() {
        return subType;
    }

    /**
     * @param subType the subType to set
     */
    public void setSubType(String subType) {
        this.subType = subType;
    }

    /**
     * @return the roaming
     */
    public String getRoaming() {
        return roaming;
    }

    /**
     * @param roaming the roaming to set
     */
    public void setRoaming(String roaming) {
        this.roaming = roaming;
    }

    /**
     * @return the interCal
     */
    public String getInterCal() {
        return interCal;
    }

    /**
     * @param interCal the interCal to set
     */
    public void setInterCal(String interCal) {
        this.interCal = interCal;
    }

    /**
     * @return the numRecord
     */
    public String getNumRecord() {
        return numRecord;
    }

    /**
     * @param numRecord the numRecord to set
     */
    public void setNumRecord(String numRecord) {
        this.numRecord = numRecord;
    }

    /**
     * @return the staffCode
     */
    public String getStaffCode() {
        return staffCode;
    }

    /**
     * @param staffCode the staffCode to set
     */
    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the stockModelCode
     */
    public String getStockModelCode() {
        return stockModelCode;
    }

    /**
     * @param stockModelCode the stockModelCode to set
     */
    public void setStockModelCode(String stockModelCode) {
        this.stockModelCode = stockModelCode;
    }

    /**
     * @return the saleTransType
     */
    public Long getSaleTransType() {
        return saleTransType;
    }

    /**
     * @param saleTransType the saleTransType to set
     */
    public void setSaleTransType(Long saleTransType) {
        this.saleTransType = saleTransType;
    }

    /**
     * @return the isdn
     */
    public String getIsdn() {
        return isdn;
    }

    /**
     * @param isdn the isdn to set
     */
    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return the merchantAccount
     */
    public String getMerchantAccount() {
        return merchantAccount;
    }

    /**
     * @param merchantAccount the merchantAccount to set
     */
    public void setMerchantAccount(String merchantAccount) {
        this.merchantAccount = merchantAccount;
    }

    /**
     * @return the requestId
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * @param requestId the requestId to set
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * @return the tid
     */
    public String getTid() {
        return tid;
    }

    /**
     * @param tid the tid to set
     */
    public void setTid(String tid) {
        this.tid = tid;
    }

    /**
     * @return the reasonId
     */
    public Long getReasonId() {
        return reasonId;
    }

    /**
     * @param reasonId the reasonId to set
     */
    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    /**
     * @return the custName
     */
    public String getCustName() {
        return custName;
    }

    /**
     * @param custName the custName to set
     */
    public void setCustName(String custName) {
        this.custName = custName;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    /**
     * @return the packageCode
     */
    public String getPackageCode() {
        return packageCode;
    }

    /**
     * @param packageCode the packageCode to set
     */
    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * @return the dateExpired
     */
    public int getDateExpired() {
        return dateExpired;
    }

    /**
     * @param dateExpired the dateExpired to set
     */
    public void setDateExpired(int dateExpired) {
        this.dateExpired = dateExpired;
    }

    /**
     * @return the dealerCode
     */
    public String getDealerCode() {
        return dealerCode;
    }

    /**
     * @param dealerCode the dealerCode to set
     */
    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    /**
     * @return the shopCode
     */
    public String getShopCode() {
        return shopCode;
    }

    /**
     * @param shopCode the shopCode to set
     */
    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getReasonTopup() {
        return reasonTopup;
    }

    public void setReasonTopup(String reasonTopup) {
        this.reasonTopup = reasonTopup;
    }

    public Double getAmountBasic() {
        return amountBasic;
    }

    public void setAmountBasic(Double amountBasic) {
        this.amountBasic = amountBasic;
    }

    public Double getAmountPromotion() {
        return amountPromotion;
    }

    public void setAmountPromotion(Double amountPromotion) {
        this.amountPromotion = amountPromotion;
    }
}
