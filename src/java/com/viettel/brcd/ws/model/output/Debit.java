/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "debit")
public class Debit {
    
    @XmlElement
    private String debitDecripsion;
    @XmlElement
    private boolean result;

    public String getDebitDecripsion() {
        return debitDecripsion;
    }

    public void setDebitDecripsion(String debitDecripsion) {
        this.debitDecripsion = debitDecripsion;
    }

 

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
    
    
    
    
}
