/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "DebitStaffOut")
public class DebitStaffOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "DebitStaffDetail")
    private List<DebitStaffDetail> debitStaffDetail;

    public DebitStaffOut() {
    }

    public DebitStaffOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public DebitStaffOut(String errorCode, String errorDecription, List<DebitStaffDetail> DebitStaffDetail) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.debitStaffDetail = DebitStaffDetail;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<DebitStaffDetail> getDebitStaffDetail() {
        return debitStaffDetail;
    }

    public void setDebitStaffDetail(List<DebitStaffDetail> DebitStaffDetail) {
        this.debitStaffDetail = DebitStaffDetail;
    }
    
}
