/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "DebitTransHistoryOut")
public class DebitTransHistoryOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement
    private Long totalPage;
    @XmlElement
    private Long totalSize;
    @XmlElement(name = "DebitTransHistoryDetail")
    private List<DebitTransHistoryDetail> debitTransHistoryDetail;

    public DebitTransHistoryOut() {
    }

    public DebitTransHistoryOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public DebitTransHistoryOut(String errorCode, String errorDecription, Long totalPage, Long totalSize, List<DebitTransHistoryDetail> debitTransHistoryDetail) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.totalPage = totalPage;
        this.totalSize = totalSize;
        this.debitTransHistoryDetail = debitTransHistoryDetail;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public Long getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Long totalPage) {
        this.totalPage = totalPage;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    public List<DebitTransHistoryDetail> getDebitTransHistoryDetail() {
        return debitTransHistoryDetail;
    }

    public void setDebitTransHistoryDetail(List<DebitTransHistoryDetail> debitTransHistoryDetail) {
        this.debitTransHistoryDetail = debitTransHistoryDetail;
    }
    
}
