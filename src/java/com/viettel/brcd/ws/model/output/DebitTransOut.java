/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.api.debit.BO.DebitTrans;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "DebitTransactionOut")
public class DebitTransOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "DebitTransactionDetail")
    private List<DebitTrans> lstDebitTrans;

    public DebitTransOut() {
    }

    public DebitTransOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public DebitTransOut(String errorCode, String errorDecription, List<DebitTrans> lstDebitTrans) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.lstDebitTrans = lstDebitTrans;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<DebitTrans> getLstDebitTrans() {
        return lstDebitTrans;
    }

    public void setLstDebitTrans(List<DebitTrans> lstDebitTrans) {
        this.lstDebitTrans = lstDebitTrans;
    }
    
}
