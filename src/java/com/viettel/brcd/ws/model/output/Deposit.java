/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.ApDomain;

/**
 *
 * @author linhlh2
 */
public class Deposit {

    private String depositCode;
    private String depositName;

    public Deposit() {
    }

    public Deposit(ApDomain domain) {
        if (domain != null) {
            this.depositCode = domain.getCode();
            this.depositName = domain.getValue();
        }
    }

    public Deposit(String depositCode, String depositName) {
        this.depositCode = depositCode;
        this.depositName = depositName;
    }

    public String getDepositCode() {
        return depositCode;
    }

    public void setDepositCode(String depositCode) {
        this.depositCode = depositCode;
    }

    public String getDepositName() {
        return depositName;
    }

    public void setDepositName(String depositName) {
        this.depositName = depositName;
    }
}
