/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author nguyen ngoc viet
 */
public class DetailRevokeTaskOut {

    private String shopName;
    private String deployAddress;
    private Long shopId;
    private Long staffId;
    private String staffName;
    private String sourceType;
    private String startDate;
    private String endDate;
    private String taskName;
    private Long taskMngtId;
    private Long taskStaffMngtId;
    private Long subId;
    private Long revokeFor;
    private String error;
    private String resultMess;

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Long getRevokeFor() {
        return revokeFor;
    }

    public void setRevokeFor(Long revokeFor) {
        this.revokeFor = revokeFor;
    }

    public Long getTaskStaffMngtId() {
        return taskStaffMngtId;
    }

    public void setTaskStaffMngtId(Long taskStaffMngtId) {
        this.taskStaffMngtId = taskStaffMngtId;
    }

    public Long getTaskMngtId() {
        return taskMngtId;
    }

    public void setTaskMngtId(Long taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getDeployAddress() {
        return deployAddress;
    }

    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getResultMess() {
        return resultMess;
    }

    public void setResultMess(String resultMess) {
        this.resultMess = resultMess;
    }
}
