/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.Area;

/**
 *
 * @author linhlh2
 */
public class District {

    private String districtCode;
    private String districtName;

    public District() {
    }

    public District(Area area) {
        if (area != null) {
            this.districtCode = area.getDistrict();
            this.districtName = area.getName();
        }
    }

    public District(String districtCode, String districtName) {
        this.districtCode = districtCode;
        this.districtName = districtName;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }
}
