/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.Domain;
import java.util.List;

/**
 *
 * @author linhlh2
 */
public class DomainOut {

    private String errorCode;
    private String errorDecription;
    private List<Domain> Domains;

    public DomainOut() {
    }

    public DomainOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public DomainOut(String errorCode, String errorDecription, List<Domain> Domains) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.Domains = Domains;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<Domain> getDomains() {
        return Domains;
    }

    public void setDomains(List<Domain> Domains) {
        this.Domains = Domains;
    }
}
