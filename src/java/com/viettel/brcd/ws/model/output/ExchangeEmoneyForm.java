/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author cuongdm
 */
public class ExchangeEmoneyForm {
    private String errCode;
    private String errOcs;
    private String message;

    /**
     * @return the errCode
     */
    public String getErrCode() {
        return errCode;
    }

    /**
     * @param errCode the errCode to set
     */
    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    /**
     * @return the errOcs
     */
    public String getErrOcs() {
        return errOcs;
    }

    /**
     * @param errOcs the errOcs to set
     */
    public void setErrOcs(String errOcs) {
        this.errOcs = errOcs;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
