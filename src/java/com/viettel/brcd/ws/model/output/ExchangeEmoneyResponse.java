/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "processResponse", namespace = "ws")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "processResponse", propOrder = {
    "_return"
})
public class ExchangeEmoneyResponse {

    @XmlElement(name = "return", nillable = true)
    private ExchangeEmoneyForm _return;

    /**
     * @return the _return
     */
    public ExchangeEmoneyForm getReturn() {
        return _return;
    }

    /**
     * @param _return the _return to set
     */
    public void setReturn(ExchangeEmoneyForm _return) {
        this._return = _return;
    }
}
