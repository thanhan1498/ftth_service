/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cc.model.GetSubscriptionInfoForm;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author duyetdk
 */
@XmlRootElement(name = "getSubscriptionInfoResponse", namespace = "http://webservice.infra.nims.viettel.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getSubscriptionInfoResponse", propOrder = {"_return"})
public class GetSubscriptionInfoResponse {

    @XmlElement(name = "return")
    protected GetSubscriptionInfoForm _return;

    public GetSubscriptionInfoForm getReturn() {
        return _return;
    }

    public void setReturn(GetSubscriptionInfoForm _return) {
        this._return = _return;
    }
}
