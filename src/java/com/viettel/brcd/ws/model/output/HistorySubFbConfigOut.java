/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.SubFbConfigLog;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "HistorySubFbConfig")
public class HistorySubFbConfigOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "SubFbConfigLog")
    private List<SubFbConfigLog> subFbConfigLogs;

    public HistorySubFbConfigOut() {
    }

    public HistorySubFbConfigOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public HistorySubFbConfigOut(String errorCode, String errorDecription, List<SubFbConfigLog> subFbConfigLogs) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.subFbConfigLogs = subFbConfigLogs;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<SubFbConfigLog> getSubFbConfigLogs() {
        return subFbConfigLogs;
    }

    public void setSubFbConfigLogs(List<SubFbConfigLog> subFbConfigLogs) {
        this.subFbConfigLogs = subFbConfigLogs;
    }
    
}
