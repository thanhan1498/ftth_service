package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.ApDomain;

/**
 * @author linhlh2
 */
public class IdType {

    private String typeCode;
    private String typeName;

    public IdType() {
    }

    public IdType(ApDomain domain) {
        if (domain != null) {
            this.typeCode = domain.getCode();
            this.typeName = domain.getName();
        }
    }

    public IdType(String typeCode, String typeName) {
        this.typeCode = typeCode;
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }
}
