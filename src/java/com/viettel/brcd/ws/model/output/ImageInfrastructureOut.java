/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.ApParam;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ImageInfrastructure")
public class ImageInfrastructureOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "ImageType")
    private List<ApParam> apParam;

    public ImageInfrastructureOut() {
    }

    public ImageInfrastructureOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ImageInfrastructureOut(String errorCode, String errorDecription, List<ApParam> apParam) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.apParam = apParam;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ApParam> getApParam() {
        return apParam;
    }

    public void setApParam(List<ApParam> apParam) {
        this.apParam = apParam;
    }
    
}
