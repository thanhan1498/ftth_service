/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.api.Task.BO.APStockModelBean;
import com.viettel.bccs.api.Task.BO.EquipmentPort;
import com.viettel.bccs.api.Task.BO.InfastructureDetail;
import com.viettel.bccs.api.Task.BO.SubStockModelRel;
import com.viettel.bccs.api.supplier.nims.getInfrasGpon.GetBranchNodeByStationResutForm;
import com.viettel.bccs.api.supplier.nims.getInfrasGpon.GetPortBySplitterResultForm;
import com.viettel.bccs.api.supplier.nims.getInfrasGpon.GetSplitterBySubNodeResultForm;
import com.viettel.bccs.api.supplier.nims.getInfrasGpon.GetSubNodeByBranchNodeResultForm;
import com.viettel.brcd.ws.model.input.NewImageInput;
import com.viettel.im.database.BO.Boards;
import com.viettel.im.database.BO.CableBox;
import com.viettel.im.database.BO.Dslam;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 * @author Nguyen Ngoc Viet <vietnn6@viettel.com.vn>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "inforInfrastructureOut")
public class InforInfrastructureOut implements Cloneable {

    @XmlElement
    String errorCode;
    @XmlElement
    String errorDecription;
    //Lay danh sach Cap Tong
    @XmlElement(name = "boards")
    List<Boards> lstBoard;
    //Lay danh sach cap nhanh
    @XmlElement(name = "cableBox")
    List<CableBox> lstCableBox;
    // Lay danh sach hang hoa
    @XmlElement(name = "goods")
    List<SubStockModelRel> lstGood;
    // Lay danh sach vat tu
    @XmlElement(name = "item")
    List<APStockModelBean> lstItem;
    // Lay danh sach dslam
    @XmlElement(name = "dslam")
    List<Dslam> lstDslam;
    // Lay danh sach port
    @XmlElement(name = "port")
    List<EquipmentPort> lstPort;
    // Lay thong tin chung
    @XmlElement(name = "infastructureDetail")
    InfastructureDetail infastructureDetail;
    @XmlElement
    private Boolean isGpon;
    // lay danh sach nhanh GPON
    @XmlElement(name = "branchGpon")
    private List<GetBranchNodeByStationResutForm> lstBranchGpon;
    // lay danh sach nhanh con GPON
    @XmlElement(name = "subBranchGpon")
    private List<GetSubNodeByBranchNodeResultForm> lstSubBranchGpon;
    // lay danh sach spitter GPON
    @XmlElement(name = "spitterGpon")
    private List<GetSplitterBySubNodeResultForm> lstSpitterGpon;
    // lay danh sach port GPON
    @XmlElement(name = "portGpon")
    private List<GetPortBySplitterResultForm> lstPortGpon;
    // Lay thong tin toa do ha tang
    @XmlElement(name = "locationInfra")
    LocationInfo locationInfra;
    // Lay thong tin toa do KH
    @XmlElement(name = "locationCust")
    LocationInfo locationCust;
    @XmlElement(name = "imageInfra")
    private List<NewImageInput> imageInfra;
    @XmlElement(name = "imageGood")
    private List<NewImageInput> imageGood;
    @XmlElement(name = "maxDistanceConnector")
    private String maxDistanceConnector;
    @XmlElement(name = "maxDistanceImage")
    private String maxDistanceImage;
    @XmlElement(name = "isSplitter")
    private String isSplitter;
    @XmlElement(name = "serviceType")
    private String serviceType;
    private String couplerNo;

    public List<GetBranchNodeByStationResutForm> getLstBranchGpon() {
        return lstBranchGpon;
    }

    public void setLstBranchGpon(List<GetBranchNodeByStationResutForm> lstBranchGpon) {
        this.lstBranchGpon = lstBranchGpon;
    }

    public List<GetSubNodeByBranchNodeResultForm> getLstSubBranchGpon() {
        return lstSubBranchGpon;
    }

    public void setLstSubBranchGpon(List<GetSubNodeByBranchNodeResultForm> lstSubBranchGpon) {
        this.lstSubBranchGpon = lstSubBranchGpon;
    }

    public List<GetSplitterBySubNodeResultForm> getLstSpitterGpon() {
        return lstSpitterGpon;
    }

    public void setLstSpitterGpon(List<GetSplitterBySubNodeResultForm> lstSpitterGpon) {
        this.lstSpitterGpon = lstSpitterGpon;
    }

    public List<GetPortBySplitterResultForm> getLstPortGpon() {
        return lstPortGpon;
    }

    public void setLstPortGpon(List<GetPortBySplitterResultForm> lstPortGpon) {
        this.lstPortGpon = lstPortGpon;
    }

    public Boolean getIsGpon() {
        return isGpon;
    }

    public void setIsGpon(Boolean isGpon) {
        this.isGpon = isGpon;
    }

    public List<Dslam> getLstDslam() {
        return lstDslam;
    }

    public void setLstDslam(List<Dslam> lstDslam) {
        this.lstDslam = lstDslam;
    }

    public List<EquipmentPort> getLstPort() {
        return lstPort;
    }

    public void setLstPort(List<EquipmentPort> lstPort) {
        this.lstPort = lstPort;
    }

    public InforInfrastructureOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public InfastructureDetail getInfastructureDetail() {
        return infastructureDetail;
    }

    public void setInfastructureDetail(InfastructureDetail infastructureDetail) {
        this.infastructureDetail = infastructureDetail;
    }

    public InforInfrastructureOut() {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<Boards> getLstBoard() {
        return lstBoard;
    }

    public void setLstBoard(List<Boards> lstBoard) {
        this.lstBoard = lstBoard;
    }

    public List<CableBox> getLstCableBox() {
        return lstCableBox;
    }

    public void setLstCableBox(List<CableBox> lstCableBox) {
        this.lstCableBox = lstCableBox;
    }

    public List<SubStockModelRel> getLstGood() {
        return lstGood;
    }

    public void setLstGood(List<SubStockModelRel> lstGood) {
        this.lstGood = lstGood;
    }

    public List<APStockModelBean> getLstItem() {
        return lstItem;
    }

    public void setLstItem(List<APStockModelBean> lstItem) {
        this.lstItem = lstItem;
    }

    public LocationInfo getLocationInfra() {
        return locationInfra;
    }

    public void setLocationInfra(LocationInfo locationInfra) {
        this.locationInfra = locationInfra;
    }

    public LocationInfo getLocationCust() {
        return locationCust;
    }

    public void setLocationCust(LocationInfo locationCust) {
        this.locationCust = locationCust;
    }

    public List<NewImageInput> getImageInfra() {
        return imageInfra;
    }

    public void setImageInfra(List<NewImageInput> imageInfra) {
        this.imageInfra = imageInfra;
    }

    public List<NewImageInput> getImageGood() {
        return imageGood;
    }

    public void setImageGood(List<NewImageInput> imageGood) {
        this.imageGood = imageGood;
    }

    /**
     * @return the maxDistanceConnector
     */
    public String getMaxDistanceConnector() {
        return maxDistanceConnector;
    }

    /**
     * @param maxDistanceConnector the maxDistanceConnector to set
     */
    public void setMaxDistanceConnector(String maxDistanceConnector) {
        this.maxDistanceConnector = maxDistanceConnector;
    }

    /**
     * @return the maxDistanceImage
     */
    public String getMaxDistanceImage() {
        return maxDistanceImage;
    }

    /**
     * @param maxDistanceImage the maxDistanceImage to set
     */
    public void setMaxDistanceImage(String maxDistanceImage) {
        this.maxDistanceImage = maxDistanceImage;
    }

    /**
     * @return the isSplitter
     */
    public String getIsSplitter() {
        return isSplitter;
    }

    /**
     * @param isSplitter the isSplitter to set
     */
    public void setIsSplitter(String isSplitter) {
        this.isSplitter = isSplitter;
    }

    @Override
    public InforInfrastructureOut clone() throws CloneNotSupportedException {
        InforInfrastructureOut clone = (InforInfrastructureOut) super.clone();
        if (this.imageInfra != null) {
            clone.setImageInfra(new ArrayList<NewImageInput>());
            for (NewImageInput image : this.imageInfra) {
                clone.getImageInfra().add(new NewImageInput(image));
            }
        }
        if (this.imageGood != null) {
            clone.setImageInfra(new ArrayList<NewImageInput>());
            for (NewImageInput image : this.imageGood) {
                clone.getImageInfra().add(new NewImageInput(image));
            }
        }
        return clone;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    /**
     * @return the couplerNo
     */
    public String getCouplerNo() {
        return couplerNo;
    }

    /**
     * @param couplerNo the couplerNo to set
     */
    public void setCouplerNo(String couplerNo) {
        this.couplerNo = couplerNo;
    }
}
