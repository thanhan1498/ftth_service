/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.cc.client.bean.CompProcess;
import com.viettel.cc.client.bean.InfoComplain;
import com.viettel.cc.database.BO.CompCause;
import com.viettel.cc.database.BO.CompSatisfiedLevel;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nguyen Ngoc Viet <vietnn6@viettel.com.vn>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "inforResolveComplainOut")
public class InforResolveComplainOut {

    @XmlElement
    String errorCode;
    @XmlElement
    String errorDecription;
    //Muc do hai long cua khach hang
    @XmlElement(name = "satisfileLevel")
    List<CompSatisfiedLevel> lstSatisfileLevel;
    //Nguyen nhan loi
    @XmlElement(name = "compCause")
    List<CompCause> lstCompCause;
    //chi tiet xu ly
    @XmlElement(name = "detailComplain")
    InfoComplain detailComplain;
    // Danh sach tac dong
    @XmlElement(name = "compProcess")
    List<CompProcess> lstCompProcess;

    public InforResolveComplainOut(String errCode, String description) {
        this.errorCode = errCode;
        this.errorDecription = description;
    }

    public InforResolveComplainOut() {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<CompSatisfiedLevel> getLstSatisfileLevel() {
        return lstSatisfileLevel;
    }

    public void setLstSatisfileLevel(List<CompSatisfiedLevel> lstSatisfileLevel) {
        this.lstSatisfileLevel = lstSatisfileLevel;
    }

    public List<CompCause> getLstCompCause() {
        return lstCompCause;
    }

    public void setLstCompCause(List<CompCause> lstCompCause) {
        this.lstCompCause = lstCompCause;
    }

    public InfoComplain getDetailComplain() {
        return detailComplain;
    }

    public void setDetailComplain(InfoComplain detailComplain) {
        this.detailComplain = detailComplain;
    }

    public List<CompProcess> getLstCompProcess() {
        return lstCompProcess;
    }

    public void setLstCompProcess(List<CompProcess> lstCompProcess) {
        this.lstCompProcess = lstCompProcess;
    }
}
