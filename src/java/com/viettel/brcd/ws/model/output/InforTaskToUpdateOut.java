/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.GroupReason;
import com.viettel.bccs.cm.model.InforTaskToUpdate;
import com.viettel.bccs.cm.model.ReasonComplaint;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nguyen Tran Minh Nhut <tranminhnhutn@metfone.com.kh>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "inforTaskToUpdateOut")
public class InforTaskToUpdateOut {
    @XmlElement
    String errorCode;
    @XmlElement
    String errorDecription;
    @XmlElement(name= "inforTaskToUpdate")
    InforTaskToUpdate inforTaskToUpdate;
    @XmlElement(name= "reason")
    List<ReasonComplaint> lstReason;
    @XmlElement(name= "groupReason")
    List<GroupReason> lstGroupReason;
    //truongpv edit 20/07/2017
    @XmlElement(name= "reasonDeductTime")
    List<ReasonComplaint> lstReasonDeductTime;
    
    public InforTaskToUpdateOut(String errCode, String description) {
        this.errorCode = errCode;
        this.errorDecription = description;
    }

    public List<GroupReason> getLstGroupReason() {
        return lstGroupReason;
    }

    public void setLstGroupReason(List<GroupReason> lstGroupReason) {
        this.lstGroupReason = lstGroupReason;
    }

    public InforTaskToUpdateOut() {
    }

    public InforTaskToUpdate getInforTaskToUpdate() {
        return inforTaskToUpdate;
    }

    public void setInforTaskToUpdate(InforTaskToUpdate inforTaskToUpdate) {
        this.inforTaskToUpdate = inforTaskToUpdate;
    }

    public List<ReasonComplaint> getLstReason() {
        return lstReason;
    }

    public void setLstReason(List<ReasonComplaint> lstReason) {
        this.lstReason = lstReason;
    }

    

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ReasonComplaint> getLstReasonDeductTime() {
        return lstReasonDeductTime;
    }

    public void setLstReasonDeductTime(List<ReasonComplaint> lstReasonDeductTime) {
        this.lstReasonDeductTime = lstReasonDeductTime;
    }

}
