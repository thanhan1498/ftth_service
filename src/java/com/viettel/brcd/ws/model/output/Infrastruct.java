/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author linhlh2
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "infrastruct")
public class Infrastruct {

    @XmlElement
    private String device;
    @XmlElement
    private String port;
    @XmlElement(required = true)
    private String ipStatic;
    @XmlElement
    private String account;
    @XmlElement(required = true)
    private String isdn;
    @XmlElement(required = true)
    private Long cableBoxId;
    @XmlElement(required = true)
    private String cableBoxCode;
    @XmlElement(required = true)
    private Long teamId;
    @XmlElement(required = true)
    private String cableLength;
    @XmlElement(required = true)
    private String portCode;
    @XmlElement(required = true)
    private String technology;

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getCableBoxCode() {
        return cableBoxCode;
    }

    public void setCableBoxCode(String cableBoxCode) {
        this.cableBoxCode = cableBoxCode;
    }

    public String getPortCode() {
        return portCode;
    }

    public void setPortCode(String portCode) {
        this.portCode = portCode;
    }

    public String getCableLength() {
        return cableLength;
    }

    public void setCableLength(String cableLength) {
        this.cableLength = cableLength;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getIpStatic() {
        return ipStatic;
    }

    public void setIpStatic(String ipStatic) {
        this.ipStatic = ipStatic;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Long getCableBoxId() {
        return cableBoxId;
    }

    public void setCableBoxId(Long cableBoxId) {
        this.cableBoxId = cableBoxId;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }
}
