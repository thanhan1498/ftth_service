/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author vietnn6
 */
public class InvestOut {

    private Double amountCore;
    private Double amountBox;
    private Double amountProduct;
    private String errorCode;
    private String errorDecription;

    public Double getAmountBox() {
        return amountBox;
    }

    public void setAmountBox(Double amountBox) {
        this.amountBox = amountBox;
    }

    public Double getAmountCore() {
        return amountCore;
    }

    public void setAmountCore(Double amountCore) {
        this.amountCore = amountCore;
    }

    public Double getAmountProduct() {
        return amountProduct;
    }

    public void setAmountProduct(Double amountProduct) {
        this.amountProduct = amountProduct;
    }

    public InvestOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public InvestOut() {
    }
}
