package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.database.BO.InvoiceList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "InvoiceLstOut")
public class InvoiceListOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "invoiceOut")
    private List<InvoiceList> lstInvoice;

    public InvoiceListOut() {
    }

    public InvoiceListOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<InvoiceList> getLstInvoice() {
        return lstInvoice;
    }

    public void setLstInvoice(List<InvoiceList> lstInvoice) {
        this.lstInvoice = lstInvoice;
    }
}
