/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "LastUpdateConfig")
public class LastUpdateConfigOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement
    private String status;
    @XmlElement
    private String reasonCode;
    @XmlElement
    private String reasonName;
    @XmlElement
    private String description;
    @XmlElement
    private String createUser;
    @XmlElement
    private String createDate;

    public LastUpdateConfigOut() {
    }

    public LastUpdateConfigOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public LastUpdateConfigOut(String errorCode, String errorDecription, String status, String reasonCode, String reasonName, String description, String createUser, String createDate) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.status = status;
        this.reasonCode = reasonCode;
        this.reasonName = reasonName;
        this.description = description;
        this.createUser = createUser;
        this.createDate = createDate;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    
}
