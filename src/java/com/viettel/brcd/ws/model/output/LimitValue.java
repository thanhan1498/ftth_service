/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author linhlh2
 */
public class LimitValue {

    private Long limitValue;

    public LimitValue() {
    }

    public LimitValue(Long limitValue) {
        this.limitValue = limitValue;
    }

    public Long getLimitValue() {
        return limitValue;
    }

    public void setLimitValue(Long limitValue) {
        this.limitValue = limitValue;
    }
}
