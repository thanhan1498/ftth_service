/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author linhlh2
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "limitValues")
public class LimitValueOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "limitValue")
    private List<LimitValue> limitValues;
    @XmlElement
    private String token;

    public LimitValueOut() {
    }

    public LimitValueOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public LimitValueOut(String errorCode, String errorDecription, List<LimitValue> limitValues) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.limitValues = limitValues;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<LimitValue> getLimitValues() {
        return limitValues;
    }

    public void setLimitValues(List<LimitValue> limitValues) {
        this.limitValues = limitValues;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
