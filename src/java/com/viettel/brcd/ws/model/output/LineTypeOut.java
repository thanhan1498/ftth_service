/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.ApDomain;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "lineTypes")
public class LineTypeOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    private List<ApDomain> lineTypes;

    public LineTypeOut() {
    }

    public LineTypeOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public LineTypeOut(String errorCode, String errorDecription, List<ApDomain> lineTypes) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.lineTypes = lineTypes;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ApDomain> getLineTypes() {
        return lineTypes;
    }

    public void setLineTypes(List<ApDomain> lineTypes) {
        this.lineTypes = lineTypes;
    }
}
