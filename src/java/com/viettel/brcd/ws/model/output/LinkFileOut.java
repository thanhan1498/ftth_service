package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.PolicyFile;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "linkFileOut")
public class LinkFileOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "linkFile")
    private List<PolicyFile> data;

    public List<PolicyFile> getData() {
        return data;
    }

    public void setData(List<PolicyFile> data) {
        this.data = data;
    }

    public LinkFileOut() {
    }

    public LinkFileOut(String errorCode) {
        this.errorCode = errorCode;
    }

    public LinkFileOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String responeCode) {
        this.errorCode = responeCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }
}
