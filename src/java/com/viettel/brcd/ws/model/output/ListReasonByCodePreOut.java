/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.pre.ReasonPre;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ListReasonByCodePre")
public class ListReasonByCodePreOut {
    
    private String errorCode;
    private String errorDecription;
    
    @XmlElement(name = "reason")
    private List<ReasonPre> reason;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ReasonPre> getReason() {
        return reason;
    }

    public void setReason(List<ReasonPre> reason) {
        this.reason = reason;
    }

    public ListReasonByCodePreOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ListReasonByCodePreOut() {
    }

    public ListReasonByCodePreOut(String errorCode, String errorDecription, List<ReasonPre> reason) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.reason = reason;
    }
    
}
