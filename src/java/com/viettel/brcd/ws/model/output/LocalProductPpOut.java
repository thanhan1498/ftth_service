/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.LocalProductPp;
import java.util.List;

/**
 *
 * @author linhlh2
 */
public class LocalProductPpOut {

    private String errorCode;
    private String errorDecription;
    private List<LocalProductPp> localProduct;

    public LocalProductPpOut() {
    }

    public LocalProductPpOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public LocalProductPpOut(String errorCode, String errorDecription, List<LocalProductPp> localProduct) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.localProduct = localProduct;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<LocalProductPp> getLocalProduct() {
        return localProduct;
    }

    public void setLocalProduct(List<LocalProductPp> localProduct) {
        this.localProduct = localProduct;
    }
}
