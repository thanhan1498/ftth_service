/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author duyetdk
 */

public class LocationInfo {
    private String maxDistance;
    private String latitude;
    private String longitude;

    public LocationInfo() {
    }

    public LocationInfo(String maxDistance, String latitude, String longitude) {
        this.maxDistance = maxDistance;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getMaxDistance() {
        return maxDistance;
    }

    public void setMaxDistance(String maxDistance) {
        this.maxDistance = maxDistance;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    
}
