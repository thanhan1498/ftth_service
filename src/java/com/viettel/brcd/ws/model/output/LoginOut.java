/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.CmUserToken;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vietnn6
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "login")
public class LoginOut {

    private String token;
    private Staff staff;
    private Shop shop;
    private String versionIos;
    private String versionAndroid;
    private String messageIosEn;
    private String messageIosKh;
    private String messageAndroidEn;
    private String messageAndroidKh;
    private String forceUpgrade;
    private String upgradeDescription;
    private String errorCode;
    private String errorDecription;
    private CmUserToken userToken;
    private String version ;
    private String meesEn ;
    private String messKh ;

    public LoginOut() {
    }

    public LoginOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public LoginOut(Staff staff, Shop shop, String token, CmUserToken userToken, String errorCode, String errorDecription) {
        this.staff = staff;
        this.shop = shop;
        this.token = token;
        this.userToken = userToken;
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public LoginOut(Staff staff, Shop shop, String errorCode, String errorDecription) {
        this.staff = staff;
        this.shop = shop;
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public CmUserToken getUserToken() {
        return userToken;
    }

    public void setUserToken(CmUserToken userToken) {
        this.userToken = userToken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getForceUpgrade() {
        return forceUpgrade;
    }

    public void setForceUpgrade(String forceUpgrade) {
        this.forceUpgrade = forceUpgrade;
    }

    public String getUpgradeDescription() {
        return upgradeDescription;
    }

    public void setUpgradeDescription(String upgradeDescription) {
        this.upgradeDescription = upgradeDescription;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    /**
     * @return the versionIos
     */
    public String getVersionIos() {
        return versionIos;
    }

    /**
     * @param versionIos the versionIos to set
     */
    public void setVersionIos(String versionIos) {
        this.versionIos = versionIos;
    }

    /**
     * @return the versionAndroid
     */
    public String getVersionAndroid() {
        return versionAndroid;
    }

    /**
     * @param versionAndroid the versionAndroid to set
     */
    public void setVersionAndroid(String versionAndroid) {
        this.versionAndroid = versionAndroid;
    }

    /**
     * @return the messageIosEn
     */
    public String getMessageIosEn() {
        return messageIosEn;
    }

    /**
     * @param messageIosEn the messageIosEn to set
     */
    public void setMessageIosEn(String messageIosEn) {
        this.messageIosEn = messageIosEn;
    }

    /**
     * @return the messageIosKh
     */
    public String getMessageIosKh() {
        return messageIosKh;
    }

    /**
     * @param messageIosKh the messageIosKh to set
     */
    public void setMessageIosKh(String messageIosKh) {
        this.messageIosKh = messageIosKh;
    }

    /**
     * @return the messageAndroidEn
     */
    public String getMessageAndroidEn() {
        return messageAndroidEn;
    }

    /**
     * @param messageAndroidEn the messageAndroidEn to set
     */
    public void setMessageAndroidEn(String messageAndroidEn) {
        this.messageAndroidEn = messageAndroidEn;
    }

    /**
     * @return the messageAndroidKh
     */
    public String getMessageAndroidKh() {
        return messageAndroidKh;
    }

    /**
     * @param messageAndroidKh the messageAndroidKh to set
     */
    public void setMessageAndroidKh(String messageAndroidKh) {
        this.messageAndroidKh = messageAndroidKh;
    }

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return the meesEn
     */
    public String getMeesEn() {
        return meesEn;
    }

    /**
     * @param meesEn the meesEn to set
     */
    public void setMeesEn(String meesEn) {
        this.meesEn = meesEn;
    }

    /**
     * @return the messKh
     */
    public String getMessKh() {
        return messKh;
    }

    /**
     * @param messKh the messKh to set
     */
    public void setMessKh(String messKh) {
        this.messKh = messKh;
    }
}
