/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author linhlh2
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "maxRadius")
public class MaxRadiusOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement
    private Long maxRadius;
    @XmlElement
    private String token;

    public MaxRadiusOut() {
    }

    public MaxRadiusOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public MaxRadiusOut(String errorCode, String errorDecription, Long maxRadius) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.maxRadius = maxRadius;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public Long getMaxRadius() {
        return maxRadius;
    }

    public void setMaxRadius(Long maxRadius) {
        this.maxRadius = maxRadius;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
