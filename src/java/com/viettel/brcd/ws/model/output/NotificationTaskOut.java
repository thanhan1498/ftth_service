/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author linhlh2
 */
public class NotificationTaskOut {

    private String errorCode;
    private String errorDecription;
    private String totalTaskNew;

    public NotificationTaskOut() {
    }

    public NotificationTaskOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }    

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public String getTotalTaskNew() {
        return totalTaskNew;
    }

    public void setTotalTaskNew(String totalTaskNew) {
        this.totalTaskNew = totalTaskNew;
    }
    
}
