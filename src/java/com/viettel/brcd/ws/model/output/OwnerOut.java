package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.Member;
import com.viettel.bccs.cm.model.Owner;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author quangdm
 */
public class OwnerOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement
    private List<Owner> listOwner;
    @XmlElement
    private List<Member> listMember;

    public OwnerOut(String errorCode, String errorDecription, List<Owner> lstOwner,List<Member>listMember) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.listOwner = lstOwner;
        this.listMember=listMember;
    }

    public OwnerOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public OwnerOut() {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<Owner> getListOwner() {
        return listOwner;
    }

    public void setListOwner(List<Owner> listOwner) {
        this.listOwner = listOwner;
    }

    public List<Member> getListMember() {
        return listMember;
    }

    public void setListMember(List<Member> listMember) {
        this.listMember = listMember;
    }
}
