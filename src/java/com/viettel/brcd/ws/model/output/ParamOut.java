/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.api.ws.bccsgw.model.PortOdf;
import com.viettel.bccs.cc.model.ComplaintEx;
import com.viettel.bccs.cc.model.ComplaintGroup;
import com.viettel.bccs.cc.model.ComplaintType;
import com.viettel.bccs.cm.model.ApParam;
import com.viettel.bccs.cm.model.ApParamCustom;
import com.viettel.bccs.cm.model.ApParamExtend;
import com.viettel.bccs.cm.model.KpiBonusTask;
import com.viettel.bccs.cm.model.KpiDeadlineTask;
import com.viettel.bccs.cm.model.NotificationLog;
import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.Shop;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.SubAdslLeaseline;
import com.viettel.bccs.cm.model.TechnicalStation;
import com.viettel.bccs.cm.model.pre.ReasonPre;
import com.viettel.bccs.target.model.Target;
import com.viettel.bccs.target.model.TargetResultOnline;
import com.viettel.bccs.target.model.TargetResultOnlineReport;
import com.viettel.bccs.target.model.TargetWarning;
import com.viettel.brcd.ws.model.input.NewImageInput;
import java.util.List;

/**
 *
 * @author cuongdm
 */
public class ParamOut extends WSRespone {

    private List<NewImageInput> imgeList;
    private List<CcActionInformation> ccActionDetail;
    private List<String> accountCustomer;
    private List<ApParam> ccActionType;
    private List<TechnicalStation> listStation;
    private List<PortOdf> portOdf;
    private List<StaffManageConnectorInfo> staffMangeConnector;
    private StaffSalaryDetail salaryDetail;
    private List<ApParam> listKpiInfo;
    private List<KpiDeadlineTask> listKpiDeadline;
    private List<KpiBonusTask> listKpiBonus;
    private List<BrasIpPool> brasIpPool;
    private List<NotificationLog> listNotificationLog;
    private String transactionId;
    private String saleTransId;
    private String encrypt;
    private String signature;
    private String tranId;
    private List<String> listProductVas;
    private AccountInformation accountInfo;
    private boolean isRightInfor;
    private Long isBlockSim;
    private SubscriberRestoreInfo subscriberRestoreInfo;
    private List<SubInfoSms> listSub;
    private String dateTimeStr;
    private List<TargetResultOnlineReport> listTargetResult;
    private List<Target> listTarget;
    private List<Shop> listCenter;
    private List<Staff> listStaff;
    private List<TargetResultOnline> currentMonth;
    private List<TargetResultOnline> lastMonth;
    private List<TargetWarning> listTargetWarning;
    private List<ApParam> listActionType;
    private List<ApParam> listRecoverReason;
    private List<ComplaintGroup> listComplaintGroup;
    private List<ComplaintType> listComType;
    private List<ApParam> listProcess;
    private List<ComplaintEx> listComplaintHistory;
    private List<ApParamExtend> listExchangeEmoney;
    private List<ApParamExtend> listPackageData;
    private List<ApParamExtend> listPackageDataDetail;
    private Long totalPage;
    private Long totalRequest;
    private List<RegOnlineFtthInfo> regOnlineFtthInfo;
    private List<RegOnlineHisEx> regOnlHis;
    private List<RegOnlineProgress> listProgress;
    private Long total;
    private List<ProvinceChart> chart;
    private DashboardChart dashboard;
    private List<String> reason;
    private List<Shop> showroom;
    private List<Staff> staff;
    private Long mbData;
    private String expiredDate;
    //phuonghc - add response for payment report
    private List<PaymentReport> paymentReport;
    private List<UnpaidPaymentDetailExpand> customer;
    //phuonghc 15062020
    private List<ApParamCustom> listParamPotentialCustomer;
    private String account;
    //phuonghc 09092020
    private List<Reason> reasonChangeDeploymentAddress;
    private List<SubAdslLeaseline> accountOfCustomer;

    public ParamOut() {
    }
    private List<ReasonPre> listReasonChangeSim;

    public ParamOut(String errorCode, String errorDescription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDescription;
    }

    /**
     * @return the imgeList
     */
    public List<NewImageInput> getImgeList() {
        return imgeList;
    }

    /**
     * @param imgeList the imgeList to set
     */
    public void setImgeList(List<NewImageInput> imgeList) {
        this.imgeList = imgeList;
    }

    /**
     * @return the ccActionDetail
     */
    public List<CcActionInformation> getCcActionDetail() {
        return ccActionDetail;
    }

    /**
     * @param ccActionDetail the ccActionDetail to set
     */
    public void setCcActionDetail(List<CcActionInformation> ccActionDetail) {
        this.ccActionDetail = ccActionDetail;
    }

    /**
     * @return the accountCustomer
     */
    public List<String> getAccountCustomer() {
        return accountCustomer;
    }

    /**
     * @param accountCustomer the accountCustomer to set
     */
    public void setAccountCustomer(List<String> accountCustomer) {
        this.accountCustomer = accountCustomer;
    }

    /**
     * @return the ccActionType
     */
    public List<ApParam> getCcActionType() {
        return ccActionType;
    }

    /**
     * @param ccActionType the ccActionType to set
     */
    public void setCcActionType(List<ApParam> ccActionType) {
        this.ccActionType = ccActionType;
    }

    /**
     * @return the listStation
     */
    public List<TechnicalStation> getListStation() {
        return listStation;
    }

    /**
     * @param listStation the listStation to set
     */
    public void setListStation(List<TechnicalStation> listStation) {
        this.listStation = listStation;
    }

    /**
     * @return the portOdf
     */
    public List<PortOdf> getPortOdf() {
        return portOdf;
    }

    /**
     * @param portOdf the portOdf to set
     */
    public void setPortOdf(List<PortOdf> portOdf) {
        this.portOdf = portOdf;
    }

    /**
     * @return the staffMangeConnector
     */
    public List<StaffManageConnectorInfo> getStaffMangeConnector() {
        return staffMangeConnector;
    }

    /**
     * @param staffMangeConnector the staffMangeConnector to set
     */
    public void setStaffMangeConnector(List<StaffManageConnectorInfo> staffMangeConnector) {
        this.staffMangeConnector = staffMangeConnector;
    }

    /**
     * @return the salaryDetail
     */
    public StaffSalaryDetail getSalaryDetail() {
        return salaryDetail;
    }

    /**
     * @param salaryDetail the salaryDetail to set
     */
    public void setSalaryDetail(StaffSalaryDetail salaryDetail) {
        this.salaryDetail = salaryDetail;
    }

    /**
     * @return the listKpiInfo
     */
    public List<ApParam> getListKpiInfo() {
        return listKpiInfo;
    }

    /**
     * @param listKpiInfo the listKpiInfo to set
     */
    public void setListKpiInfo(List<ApParam> listKpiInfo) {
        this.listKpiInfo = listKpiInfo;
    }

    /**
     * @return the listKpiDeadline
     */
    public List<KpiDeadlineTask> getListKpiDeadline() {
        return listKpiDeadline;
    }

    /**
     * @param listKpiDeadline the listKpiDeadline to set
     */
    public void setListKpiDeadline(List<KpiDeadlineTask> listKpiDeadline) {
        this.listKpiDeadline = listKpiDeadline;
    }

    /**
     * @return the listKpiBonus
     */
    public List<KpiBonusTask> getListKpiBonus() {
        return listKpiBonus;
    }

    /**
     * @param listKpiBonus the listKpiBonus to set
     */
    public void setListKpiBonus(List<KpiBonusTask> listKpiBonus) {
        this.listKpiBonus = listKpiBonus;
    }

    /**
     * @return the brasIpPool
     */
    public List<BrasIpPool> getBrasIpPool() {
        return brasIpPool;
    }

    /**
     * @param brasIpPool the brasIpPool to set
     */
    public void setBrasIpPool(List<BrasIpPool> brasIpPool) {
        this.brasIpPool = brasIpPool;
    }

    /**
     * @return the listNotificationLog
     */
    public List<NotificationLog> getListNotificationLog() {
        return listNotificationLog;
    }

    /**
     * @param listNotificationLog the listNotificationLog to set
     */
    public void setListNotificationLog(List<NotificationLog> listNotificationLog) {
        this.listNotificationLog = listNotificationLog;
    }

    /**
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * @return the saleTransId
     */
    public String getSaleTransId() {
        return saleTransId;
    }

    /**
     * @param saleTransId the saleTransId to set
     */
    public void setSaleTransId(String saleTransId) {
        this.saleTransId = saleTransId;
    }

    /**
     * @return the encrypt
     */
    public String getEncrypt() {
        return encrypt;
    }

    /**
     * @param encrypt the encrypt to set
     */
    public void setEncrypt(String encrypt) {
        this.encrypt = encrypt;
    }

    /**
     * @return the signature
     */
    public String getSignature() {
        return signature;
    }

    /**
     * @param signature the signature to set
     */
    public void setSignature(String signature) {
        this.signature = signature;
    }

    /**
     * @return the tranId
     */
    public String getTranId() {
        return tranId;
    }

    /**
     * @param tranId the tranId to set
     */
    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    /**
     * @return the accountInfo
     */
    public AccountInformation getAccountInfo() {
        return accountInfo;
    }

    /**
     * @param accountInfo the accountInfo to set
     */
    public void setAccountInfo(AccountInformation accountInfo) {
        this.accountInfo = accountInfo;
    }

    /**
     * @return the isRightInfor
     */
    public boolean isIsRightInfor() {
        return isRightInfor;
    }

    /**
     * @param isRightInfor the isRightInfor to set
     */
    public void setIsRightInfor(boolean isRightInfor) {
        this.isRightInfor = isRightInfor;
    }

    /**
     * @return the listReasonChangeSim
     */
    public List<ReasonPre> getListReasonChangeSim() {
        return listReasonChangeSim;
    }

    /**
     * @param listReasonChangeSim the listReasonChangeSim to set
     */
    public void setListReasonChangeSim(List<ReasonPre> listReasonChangeSim) {
        this.listReasonChangeSim = listReasonChangeSim;
    }

    /**
     * @return the isBlockSim
     */
    public Long getIsBlockSim() {
        return isBlockSim;
    }

    /**
     * @param isBlockSim the isBlockSim to set
     */
    public void setIsBlockSim(Long isBlockSim) {
        this.isBlockSim = isBlockSim;
    }

    /**
     * @return the listProductVas
     */
    public List<String> getListProductVas() {
        return listProductVas;
    }

    /**
     * @param listProductVas the listProductVas to set
     */
    public void setListProductVas(List<String> listProductVas) {
        this.listProductVas = listProductVas;
    }

    public SubscriberRestoreInfo getSubscriberRestoreInfo() {
        return subscriberRestoreInfo;
    }

    public void setSubscriberRestoreInfo(SubscriberRestoreInfo subscriberRestoreInfo) {
        this.subscriberRestoreInfo = subscriberRestoreInfo;
    }

    /**
     * @return the listSub
     */
    public List<SubInfoSms> getListSub() {
        return listSub;
    }

    /**
     * @param listSub the listSub to set
     */
    public void setListSub(List<SubInfoSms> listSub) {
        this.listSub = listSub;
    }

    /**
     * @return the listTarget
     */
    public List<TargetResultOnlineReport> getListTargetResult() {
        return listTargetResult;
    }

    /**
     * @param listTargetResult the listTarget to set
     */
    public void setListTargetResult(List<TargetResultOnlineReport> listTargetResult) {
        this.listTargetResult = listTargetResult;
    }

    /**
     * @return the listTarget
     */
    public List<Target> getListTarget() {
        return listTarget;
    }

    /**
     * @param listTarget the listTarget to set
     */
    public void setListTarget(List<Target> listTarget) {
        this.listTarget = listTarget;
    }

    /**
     * @return the listCenter
     */
    public List<Shop> getListCenter() {
        return listCenter;
    }

    /**
     * @param listCenter the listCenter to set
     */
    public void setListCenter(List<Shop> listCenter) {
        this.listCenter = listCenter;
    }

    /**
     * @return the listStaff
     */
    public List<Staff> getListStaff() {
        return listStaff;
    }

    /**
     * @param listStaff the listStaff to set
     */
    public void setListStaff(List<Staff> listStaff) {
        this.listStaff = listStaff;
    }

    /**
     * @return the currentMonth
     */
    public List<TargetResultOnline> getCurrentMonth() {
        return currentMonth;
    }

    /**
     * @param currentMonth the currentMonth to set
     */
    public void setCurrentMonth(List<TargetResultOnline> currentMonth) {
        this.currentMonth = currentMonth;
    }

    /**
     * @return the lastMonth
     */
    public List<TargetResultOnline> getLastMonth() {
        return lastMonth;
    }

    /**
     * @param lastMonth the lastMonth to set
     */
    public void setLastMonth(List<TargetResultOnline> lastMonth) {
        this.lastMonth = lastMonth;
    }

    /**
     * @return the dateTimeStr
     */
    public String getDateTimeStr() {
        return dateTimeStr;
    }

    /**
     * @param dateTimeStr the dateTimeStr to set
     */
    public void setDateTimeStr(String dateTimeStr) {
        this.dateTimeStr = dateTimeStr;
    }

    /**
     * @return the listTargetWarning
     */
    public List<TargetWarning> getListTargetWarning() {
        return listTargetWarning;
    }

    /**
     * @param listTargetWarning the listTargetWarning to set
     */
    public void setListTargetWarning(List<TargetWarning> listTargetWarning) {
        this.listTargetWarning = listTargetWarning;
    }

    /**
     * @return the listActionType
     */
    public List<ApParam> getListActionType() {
        return listActionType;
    }

    /**
     * @param listActionType the listActionType to set
     */
    public void setListActionType(List<ApParam> listActionType) {
        this.listActionType = listActionType;
    }

    /**
     * @return the listRecoverReason
     */
    public List<ApParam> getListRecoverReason() {
        return listRecoverReason;
    }

    /**
     * @param listRecoverReason the listRecoverReason to set
     */
    public void setListRecoverReason(List<ApParam> listRecoverReason) {
        this.listRecoverReason = listRecoverReason;
    }

    /**
     * @return the listComplaintGroup
     */
    public List<ComplaintGroup> getListComplaintGroup() {
        return listComplaintGroup;
    }

    /**
     * @param listComplaintGroup the listComplaintGroup to set
     */
    public void setListComplaintGroup(List<ComplaintGroup> listComplaintGroup) {
        this.listComplaintGroup = listComplaintGroup;
    }

    /**
     * @return the listComType
     */
    public List<ComplaintType> getListComType() {
        return listComType;
    }

    /**
     * @param listComType the listComType to set
     */
    public void setListComType(List<ComplaintType> listComType) {
        this.listComType = listComType;
    }

    /**
     * @return the listProcess
     */
    public List<ApParam> getListProcess() {
        return listProcess;
    }

    /**
     * @param listProcess the listProcess to set
     */
    public void setListProcess(List<ApParam> listProcess) {
        this.listProcess = listProcess;
    }

    /**
     * @return the listComplaintHistory
     */
    public List<ComplaintEx> getListComplaintHistory() {
        return listComplaintHistory;
    }

    /**
     * @param listComplaintHistory the listComplaintHistory to set
     */
    public void setListComplaintHistory(List<ComplaintEx> listComplaintHistory) {
        this.listComplaintHistory = listComplaintHistory;
    }

    /**
     * @return the listExchangeEmoney
     */
    public List<ApParamExtend> getListExchangeEmoney() {
        return listExchangeEmoney;
    }

    /**
     * @param listExchangeEmoney the listExchangeEmoney to set
     */
    public void setListExchangeEmoney(List<ApParamExtend> listExchangeEmoney) {
        this.listExchangeEmoney = listExchangeEmoney;
    }

    /**
     * @return the listPackageData
     */
    public List<ApParamExtend> getListPackageData() {
        return listPackageData;
    }

    /**
     * @param listPackageData the listPackageData to set
     */
    public void setListPackageData(List<ApParamExtend> listPackageData) {
        this.listPackageData = listPackageData;
    }

    /**
     * @return the listPackageDataDetail
     */
    public List<ApParamExtend> getListPackageDataDetail() {
        return listPackageDataDetail;
    }

    /**
     * @param listPackageDataDetail the listPackageDataDetail to set
     */
    public void setListPackageDataDetail(List<ApParamExtend> listPackageDataDetail) {
        this.listPackageDataDetail = listPackageDataDetail;
    }

    /**
     * @return the totalPage
     */
    public Long getTotalPage() {
        return totalPage;
    }

    /**
     * @param totalPage the totalPage to set
     */
    public void setTotalPage(Long totalPage) {
        this.totalPage = totalPage;
    }

    /**
     * @return the totalRequest
     */
    public Long getTotalRequest() {
        return totalRequest;
    }

    /**
     * @param totalRequest the totalRequest to set
     */
    public void setTotalRequest(Long totalRequest) {
        this.totalRequest = totalRequest;
    }

    /**
     * @return the regOnlineFtthInfo
     */
    public List<RegOnlineFtthInfo> getRegOnlineFtthInfo() {
        return regOnlineFtthInfo;
    }

    /**
     * @param regOnlineFtthInfo the regOnlineFtthInfo to set
     */
    public void setRegOnlineFtthInfo(List<RegOnlineFtthInfo> regOnlineFtthInfo) {
        this.regOnlineFtthInfo = regOnlineFtthInfo;
    }

    /**
     * @return the regOnlHis
     */
    public List<RegOnlineHisEx> getRegOnlHis() {
        return regOnlHis;
    }

    /**
     * @param regOnlHis the regOnlHis to set
     */
    public void setRegOnlHis(List<RegOnlineHisEx> regOnlHis) {
        this.regOnlHis = regOnlHis;
    }

    /**
     * @return the listProgress
     */
    public List<RegOnlineProgress> getListProgress() {
        return listProgress;
    }

    /**
     * @param listProgress the listProgress to set
     */
    public void setListProgress(List<RegOnlineProgress> listProgress) {
        this.listProgress = listProgress;
    }

    /**
     * @return the total
     */
    public Long getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(Long total) {
        this.total = total;
    }

    /**
     * @return the chart
     */
    public List<ProvinceChart> getChart() {
        return chart;
    }

    /**
     * @param chart the chart to set
     */
    public void setChart(List<ProvinceChart> chart) {
        this.chart = chart;
    }

    /**
     * @return the dashboard
     */
    public DashboardChart getDashboard() {
        return dashboard;
    }

    /**
     * @param dashboard the dashboard to set
     */
    public void setDashboard(DashboardChart dashboard) {
        this.dashboard = dashboard;
    }

    /**
     * @return the reason
     */
    public List<String> getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(List<String> reason) {
        this.reason = reason;
    }

    /**
     * @return the showroom
     */
    public List<Shop> getShowroom() {
        return showroom;
    }

    /**
     * @param showroom the showroom to set
     */
    public void setShowroom(List<Shop> showroom) {
        this.showroom = showroom;
    }

    /**
     * @return the staff
     */
    public List<Staff> getStaff() {
        return staff;
    }

    /**
     * @param staff the staff to set
     */
    public void setStaff(List<Staff> staff) {
        this.staff = staff;
    }

    /**
     * @return the mbData
     */
    public Long getMbData() {
        return mbData;
    }

    /**
     * @param mbData the mbData to set
     */
    public void setMbData(Long mbData) {
        this.mbData = mbData;
    }

    /**
     * @return the expiredDate
     */
    public String getExpiredDate() {
        return expiredDate;
    }

    /**
     * @param expiredDate the expiredDate to set
     */
    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    public List<ApParamCustom> getListParamPotentialCustomer() {
        return listParamPotentialCustomer;
    }

    public void setListParamPotentialCustomer(List<ApParamCustom> listParamPotentialCustomer) {
        this.listParamPotentialCustomer = listParamPotentialCustomer;
    }

    public List<PaymentReport> getPaymentReport() {
        return paymentReport;
    }

    public void setPaymentReport(List<PaymentReport> paymentReport) {
        this.paymentReport = paymentReport;
    }

    public List<UnpaidPaymentDetailExpand> getCustomer() {
        return customer;
    }

    public void setCustomer(List<UnpaidPaymentDetailExpand> customer) {
        this.customer = customer;
    }

    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }

    public List<Reason> getReasonChangeDeploymentAddress() {
        return reasonChangeDeploymentAddress;
    }

    public void setReasonChangeDeploymentAddress(List<Reason> reasonChangeDeploymentAddress) {
        this.reasonChangeDeploymentAddress = reasonChangeDeploymentAddress;
    }

    public List<SubAdslLeaseline> getAccountOfCustomer() {
        return accountOfCustomer;
    }

    public void setAccountOfCustomer(List<SubAdslLeaseline> accountOfCustomer) {
        this.accountOfCustomer = accountOfCustomer;
    }
}
