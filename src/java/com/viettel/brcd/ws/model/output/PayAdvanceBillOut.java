/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.Customer;
import com.viettel.bccs.cm.model.Staff;
import com.viettel.bccs.cm.model.StaffInfo;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cuongdm
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "payAdvanceBillOut")
public class PayAdvanceBillOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "staffInfo")
    private StaffInfo lstStaffInfo;
    @XmlElement(name = "contract")
    private CustomerPayment customerPayment;
    private Staff staff;
    private Customer customer;
    private CustomerPayment invoice;

    public PayAdvanceBillOut() {
    }

    public PayAdvanceBillOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public PayAdvanceBillOut(String errorCode, String errorDecription, StaffInfo lstStaffInfo, CustomerPayment customerPayment) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.lstStaffInfo = lstStaffInfo;
        this.customerPayment = customerPayment;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * @return the errorDecription
     */
    public String getErrorDecription() {
        return errorDecription;
    }

    /**
     * @param errorDecription the errorDecription to set
     */
    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    /**
     * @return the lstStaffInfo
     */
    public StaffInfo getLstStaffInfo() {
        return lstStaffInfo;
    }

    /**
     * @param lstStaffInfo the lstStaffInfo to set
     */
    public void setLstStaffInfo(StaffInfo lstStaffInfo) {
        this.lstStaffInfo = lstStaffInfo;
    }

    /**
     * @return the customerPayment
     */
    public CustomerPayment getCustomerPayment() {
        return customerPayment;
    }

    /**
     * @param customerPayment the customerPayment to set
     */
    public void setCustomerPayment(CustomerPayment customerPayment) {
        this.customerPayment = customerPayment;
    }

    /**
     * @return the staff
     */
    public Staff getStaff() {
        return staff;
    }

    /**
     * @param staff the staff to set
     */
    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    /**
     * @return the customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * @return the invoice
     */
    public CustomerPayment getInvoice() {
        return invoice;
    }

    /**
     * @param invoice the invoice to set
     */
    public void setInvoice(CustomerPayment invoice) {
        this.invoice = invoice;
    }
}
