package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.database.BO.ContractPayment;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "contractOut")
public class PayContractOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "paymentCon")
    private List<ContractPayment> lstContract;

    public PayContractOut() {
    }
    
    

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ContractPayment> getLstContract() {
        return lstContract;
    }

    public void setLstContract(List<ContractPayment> lstContract) {
        this.lstContract = lstContract;
    }

    public PayContractOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }
}
