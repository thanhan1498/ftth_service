package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.ApDomain;

/**
 * @author linhlh2
 */
public class Payment {

    private String payCode;
    private String payName;

    public Payment() {
    }

    public Payment(ApDomain domain) {
        if (domain != null) {
            this.payCode = domain.getCode();
            this.payName = domain.getName();
        }
    }

    public Payment(String payCode, String payName) {
        this.payCode = payCode;
        this.payName = payName;
    }

    public String getPayCode() {
        return payCode;
    }

    public void setPayCode(String payCode) {
        this.payCode = payCode;
    }

    public String getPayName() {
        return payName;
    }

    public void setPayName(String payName) {
        this.payName = payName;
    }
}
