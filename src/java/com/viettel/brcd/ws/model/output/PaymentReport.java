/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import org.apache.commons.lang.StringUtils;

/**
 * PaymentReport
 *
 * @author phuonghc
 */
public class PaymentReport {

    private String unitName;
    private Integer totalSubscriber;
    private Integer totalSubscriberPaid;
    private Double totalRevenue;
    private Double totalRevenuePaid;

    public PaymentReport() {
        this.totalSubscriber = 0;
        this.totalSubscriberPaid = 0;
        this.totalRevenue = 0D;
        this.totalRevenuePaid = 0D;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        if (StringUtils.isEmpty(unitName)) {
            this.unitName = "N/A";
        } else {
            this.unitName = unitName;

        }
    }

    public Integer getTotalSubscriber() {
        return totalSubscriber;
    }

    public void setTotalSubscriber(Integer totalSubscriber) {
        if (totalSubscriber == null) {
            this.totalSubscriber = 0;
        } else {
            this.totalSubscriber = totalSubscriber;
        }

    }

    public Integer getTotalSubscriberPaid() {
        return totalSubscriberPaid;
    }

    public void setTotalSubscriberPaid(Integer totalSubscriberPaid) {
        if (totalSubscriberPaid == null) {
            this.totalSubscriberPaid = 0;
        } else {
            this.totalSubscriberPaid = totalSubscriberPaid;
        }
    }

    public Double getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(Double totalRevenue) {
        if (totalRevenue == null) {
            this.totalRevenue = 0D;
        } else {
            this.totalRevenue = totalRevenue;
        }
    }

    public Double getTotalRevenuePaid() {
        return totalRevenuePaid;
    }

    public void setTotalRevenuePaid(Double totalRevenuePaid) {
        if (totalRevenuePaid == null) {
            this.totalRevenuePaid = 0D;
        } else {
            this.totalRevenuePaid = totalRevenuePaid;
        }
    }
}
