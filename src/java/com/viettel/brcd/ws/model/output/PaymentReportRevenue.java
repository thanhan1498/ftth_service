/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 * PaymentReportRevenue
 *
 * @author phuonghc
 */
public class PaymentReportRevenue {

    private String unitName;
    private Double totalPerUnit;

    public PaymentReportRevenue() {
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Double getTotalPerUnit() {
        return totalPerUnit;
    }

    public void setTotalPerUnit(Double totalPerUnit) {
        this.totalPerUnit = totalPerUnit;
    }
}
