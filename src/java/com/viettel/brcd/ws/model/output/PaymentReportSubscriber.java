/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 * PaymentReportSubscriber
 *
 * @author phuonghc
 */
public class PaymentReportSubscriber {

    private String unitName;
    private int totalPerUnit;

    public PaymentReportSubscriber() {
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public int getTotalPerUnit() {
        return totalPerUnit;
    }

    public void setTotalPerUnit(int totalPerUnit) {
        this.totalPerUnit = totalPerUnit;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.unitName != null ? this.unitName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PaymentReportSubscriber other = (PaymentReportSubscriber) obj;
        if ((this.unitName == null) ? (other.unitName != null) : !this.unitName.equals(other.unitName)) {
            return false;
        }
        return true;
    }
}
