/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.PotentialCustomer;
import com.viettel.bccs.cm.model.PotentialCustomerHistory;
import com.viettel.bccs.cm.model.PotentialCustomerHistoryDto;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PotentialCustomerOut")
public class PotentialCustomerOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement
    private Long totalPage;
    @XmlElement
    private Long totalSize;
    @XmlElement(name = "PotentialCustomer")
    private List<PotentialCustomer> lst;
    //phuonghc 15062020
    @XmlElement(name = "PotentialCustomerHistory")
    private List<PotentialCustomerHistoryDto> listPotentialHistory;

    public PotentialCustomerOut() {
    }

    public PotentialCustomerOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public PotentialCustomerOut(String errorCode, String errorDecription, Long totalPage,
            Long totalSize, List<PotentialCustomer> lst) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.lst = lst;
        this.totalPage = totalPage;
        this.totalSize = totalSize;
    }

    public PotentialCustomerOut(String errorCode, String errorDecription, List<PotentialCustomerHistoryDto> list) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.listPotentialHistory = list;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public Long getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Long totalPage) {
        this.totalPage = totalPage;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    public List<PotentialCustomer> getLst() {
        return lst;
    }

    public void setLst(List<PotentialCustomer> lst) {
        this.lst = lst;
    }

    public List<PotentialCustomerHistoryDto> getListPotentialHistory() {
        return listPotentialHistory;
    }

    public void setListPotentialHistory(List<PotentialCustomerHistoryDto> listPotentialHistory) {
        this.listPotentialHistory = listPotentialHistory;
    }
}
