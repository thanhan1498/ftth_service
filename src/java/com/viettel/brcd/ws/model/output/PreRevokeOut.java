package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.Reason;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "preRevokeOut")
public class PreRevokeOut {

    @XmlElement(name = "detailTask")
    private DetailRevokeTaskOut detailTask;
    @XmlElement(name = "reason")
    private List<Reason> lstReason;
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;

    public PreRevokeOut() {
    }

    public PreRevokeOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public List<Reason> getLstReason() {
        return lstReason;
    }

    public void setLstReason(List<Reason> lstReason) {
        this.lstReason = lstReason;
    }

    public DetailRevokeTaskOut getDetailTask() {
        return detailTask;
    }

    public void setDetailTask(DetailRevokeTaskOut detailTask) {
        this.detailTask = detailTask;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }
}
