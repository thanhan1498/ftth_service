/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.Area;

/**
 *
 * @author linhlh2
 */
public class Precinct {

    private String precinctCode;
    private String precinctName;

    public Precinct() {
    }

    public Precinct(Area area) {
        if (area != null) {
            this.precinctCode = area.getPrecinct();
            this.precinctName = area.getName();
        }
    }

    public Precinct(String precinctCode, String precinctName) {
        this.precinctCode = precinctCode;
        this.precinctName = precinctName;
    }

    public String getPrecinctCode() {
        return precinctCode;
    }

    public void setPrecinctCode(String precinctCode) {
        this.precinctCode = precinctCode;
    }

    public String getPrecinctName() {
        return precinctName;
    }

    public void setPrecinctName(String precinctName) {
        this.precinctName = precinctName;
    }
}
