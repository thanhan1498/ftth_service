/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author linhlh2
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "groups")
public class ProductOut {

    private String errorCode;
    private String errorDecription;
    private String accountBundleTv;
    @XmlElement(name = "product")
    List<Product> products;
    private String token;

    public ProductOut() {
    }

    public ProductOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ProductOut(String errorCode, String errorDecription, List<Product> products) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.products = products;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the accountBundleTv
     */
    public String getAccountBundleTv() {
        return accountBundleTv;
    }

    /**
     * @param accountBundleTv the accountBundleTv to set
     */
    public void setAccountBundleTv(String accountBundleTv) {
        this.accountBundleTv = accountBundleTv;
    }
}
