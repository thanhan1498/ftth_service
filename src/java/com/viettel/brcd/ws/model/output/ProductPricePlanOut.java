/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.ProductPricePlan;
import java.util.List;

/**
 *
 * @author linhlh2
 */
public class ProductPricePlanOut {

    private String errorCode;
    private String errorDecription;
    private List<ProductPricePlan> priceProduct;

    public ProductPricePlanOut() {
    }

    public ProductPricePlanOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ProductPricePlanOut(String errorCode, String errorDecription, List<ProductPricePlan> priceProduct) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.priceProduct = priceProduct;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ProductPricePlan> getPriceProduct() {
        return priceProduct;
    }

    public void setPriceProduct(List<ProductPricePlan> priceProduct) {
        this.priceProduct = priceProduct;
    }
}
