package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.PromotionType;

/**
 * @author linhlh2
 */
public class Promotion {

    private String promCode;
    private String promName;

    public Promotion() {
    }

    public Promotion(PromotionType promotion) {
        if (promotion != null) {
            this.promCode = promotion.getPromProgramCode();
            this.promName = promotion.getName();
        }
    }

    public Promotion(String promCode, String promName) {
        this.promCode = promCode;
        this.promName = promName;
    }

    public String getPromCode() {
        return promCode;
    }

    public void setPromCode(String promCode) {
        this.promCode = promCode;
    }

    public String getPromName() {
        return promName;
    }

    public void setPromName(String promName) {
        this.promName = promName;
    }
}
