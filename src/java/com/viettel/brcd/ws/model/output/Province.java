/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.Area;

/**
 *
 * @author linhlh2
 */
public class Province {

    private String provinceCode;
    private String provinceName;

    public Province() {
    }

    public Province(Area area) {
        if (area != null) {
            this.provinceCode = area.getProvince();
            this.provinceName = area.getName();
        }
    }

    public Province(String provinceCode, String provinceName) {
        this.provinceCode = provinceCode;
        this.provinceName = provinceName;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }
}
