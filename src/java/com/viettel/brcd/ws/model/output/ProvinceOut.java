/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author linhlh2
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "provinces")
public class ProvinceOut {

    private String errorCode;
    private String errorDecription;
    @XmlElement(name = "province")
    private List<Province> provinces;
    private String token;

    public ProvinceOut() {
    }

    public ProvinceOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ProvinceOut(String errorCode, String errorDecription, List<Province> provinces) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.provinces = provinces;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<Province> getProvinces() {
        return provinces;
    }

    public void setProvinces(List<Province> provinces) {
        this.provinces = provinces;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
