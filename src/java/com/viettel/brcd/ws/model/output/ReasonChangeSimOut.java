/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.pre.ReasonPre;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vietnn6
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "reasonChangeSim")
public class ReasonChangeSimOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "reasonChangeSimPre")
    private List<ReasonPre> reasonChangePre;
    @XmlElement(name = "reasonChangeSimPos")
    private List<Reason> reasonChangePos;

    public ReasonChangeSimOut() {
    }

    public ReasonChangeSimOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ReasonPre> getReasonChangePre() {
        return reasonChangePre;
    }

    public void setReasonChangePre(List<ReasonPre> reasonChangePre) {
        this.reasonChangePre = reasonChangePre;
    }

    public List<Reason> getReasonChangePos() {
        return reasonChangePos;
    }

    public void setReasonChangePos(List<Reason> reasonChangePos) {
        this.reasonChangePos = reasonChangePos;
    }
}
