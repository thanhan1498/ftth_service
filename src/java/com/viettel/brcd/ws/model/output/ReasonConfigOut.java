/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.ReasonConfigDetail;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ReasonConfig")
public class ReasonConfigOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "ReasonConfigDetail")
    private List<ReasonConfigDetail> reasonConfigDetails;

    public ReasonConfigOut() {
    }

    public ReasonConfigOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ReasonConfigOut(String errorCode, String errorDecription, List<ReasonConfigDetail> reasonConfigDetails) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.reasonConfigDetails = reasonConfigDetails;
    }
    
    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ReasonConfigDetail> getReasonConfigDetails() {
        return reasonConfigDetails;
    }

    public void setReasonConfigDetails(List<ReasonConfigDetail> reasonConfigDetails) {
        this.reasonConfigDetails = reasonConfigDetails;
    }

}
