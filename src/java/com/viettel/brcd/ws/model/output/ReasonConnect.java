/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.brcd.ws.model.output;

/**
 *
 * @author linhlh2
 */
public class ReasonConnect {
    private String roamCode;
    private String roamName;

    public String getRoamCode() {
        return roamCode;
    }

    public void setRoamCode(String roamCode) {
        this.roamCode = roamCode;
    }

    public String getRoamName() {
        return roamName;
    }

    public void setRoamName(String roamName) {
        this.roamName = roamName;
    }
    
}
