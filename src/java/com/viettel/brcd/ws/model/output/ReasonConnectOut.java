/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.Reason;
import com.viettel.bccs.cm.model.ReasonDetail;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author linhlh2
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "reasonConnects")
public class ReasonConnectOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "reasonConnect")
    private List<ReasonDetail> reasonConnects;
    @XmlElement
    private String token;

    
    public ReasonConnectOut() {
    }

    public ReasonConnectOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ReasonConnectOut(String errorCode, String errorDecription, List<ReasonDetail> reasonConnects) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.reasonConnects = reasonConnects;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ReasonDetail> getReasonConnects() {
        return reasonConnects;
    }

    public void setReasonConnects(List<ReasonDetail> reasonConnects) {
        this.reasonConnects = reasonConnects;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
