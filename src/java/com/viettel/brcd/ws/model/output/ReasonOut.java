/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.Reason;
import java.util.List;

/**
 *
 * @author linhlh2
 */
public class ReasonOut {

    private String errorCode;
    private String errorDecription;
    private List<Reason> reasons;

    public ReasonOut() {
    }

    public ReasonOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ReasonOut(String errorCode, String errorDecription, List<Reason> reasons) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.reasons = reasons;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<Reason> getReasons() {
        return reasons;
    }

    public void setReasons(List<Reason> reasons) {
        this.reasons = reasons;
    }
}
