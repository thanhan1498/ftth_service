/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.pre.ReasonPre;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author linhlh2
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "reasonConnects")
public class ReasonPreConnectOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "reasonConnect")
    private List<ReasonPre> reasonConnects;
    @XmlElement
    private String token;

    public ReasonPreConnectOut() {
    }

    public ReasonPreConnectOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ReasonPreConnectOut(String errorCode, String errorDecription, List<ReasonPre> reasonConnects) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.reasonConnects = reasonConnects;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ReasonPre> getReasonConnects() {
        return reasonConnects;
    }

    public void setReasonConnects(List<ReasonPre> reasonConnects) {
        this.reasonConnects = reasonConnects;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
