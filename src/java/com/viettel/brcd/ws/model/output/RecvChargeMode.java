/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author linhlh2
 */
public class RecvChargeMode {

    private String recvCode;
    private String recvName;

    public RecvChargeMode() {
    }

    public RecvChargeMode(String recvCode, String recvName) {
        this.recvCode = recvCode;
        this.recvName = recvName;
    }

    public String getRecvCode() {
        return recvCode;
    }

    public void setRecvCode(String recvCode) {
        this.recvCode = recvCode;
    }

    public String getRecvName() {
        return recvName;
    }

    public void setRecvName(String recvName) {
        this.recvName = recvName;
    }
}
