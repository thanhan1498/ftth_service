/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author linhlh2
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "recvChargeModes")
public class RecvChargeModeOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "recvChargeMode")
    private List<RecvChargeMode> recvChargeModes;
    @XmlElement
    private String token;

    public RecvChargeModeOut() {
    }

    public RecvChargeModeOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public RecvChargeModeOut(String errorCode, String errorDecription, List<RecvChargeMode> recvChargeModes) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.recvChargeModes = recvChargeModes;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<RecvChargeMode> getRecvChargeModes() {
        return recvChargeModes;
    }

    public void setRecvChargeModes(List<RecvChargeMode> recvChargeModes) {
        this.recvChargeModes = recvChargeModes;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
