/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.RegOnline;

/**
 *
 * @author cuongdm
 */
public class RegOnlineFtthInfo extends RegOnline {

    protected String createDateStr;
    protected String statusDes;
    protected String overtime;
    protected String color;
    protected Long showAssignStaff;
    protected Long showAssignProvince;
    protected Long showCancel;
    protected Long showConnect;
    protected Long showReject;
    protected Long showApprove;
    private String requesterCancel;

    /**
     * @return the createDateStr
     */
    public String getCreateDateStr() {
        return createDateStr;
    }

    /**
     * @param createDateStr the createDateStr to set
     */
    public void setCreateDateStr(String createDateStr) {
        this.createDateStr = createDateStr;
    }

    /**
     * @return the statusDes
     */
    public String getStatusDes() {
        return statusDes;
    }

    /**
     * @param statusDes the statusDes to set
     */
    public void setStatusDes(String statusDes) {
        this.statusDes = statusDes;
    }

    /**
     * @return the overtime
     */
    public String getOvertime() {
        return overtime;
    }

    /**
     * @param overtime the overtime to set
     */
    public void setOvertime(String overtime) {
        this.overtime = overtime;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the showAssignStaff
     */
    public Long getShowAssignStaff() {
        return showAssignStaff;
    }

    /**
     * @param showAssignStaff the showAssignStaff to set
     */
    public void setShowAssignStaff(Long showAssignStaff) {
        this.showAssignStaff = showAssignStaff;
    }

    /**
     * @return the showAssignProvince
     */
    public Long getShowAssignProvince() {
        return showAssignProvince;
    }

    /**
     * @param showAssignProvince the showAssignProvince to set
     */
    public void setShowAssignProvince(Long showAssignProvince) {
        this.showAssignProvince = showAssignProvince;
    }

    /**
     * @return the showCancel
     */
    public Long getShowCancel() {
        return showCancel;
    }

    /**
     * @param showCancel the showCancel to set
     */
    public void setShowCancel(Long showCancel) {
        this.showCancel = showCancel;
    }

    /**
     * @return the showConnect
     */
    public Long getShowConnect() {
        return showConnect;
    }

    /**
     * @param showConnect the showConnect to set
     */
    public void setShowConnect(Long showConnect) {
        this.showConnect = showConnect;
    }

    /**
     * @return the showReject
     */
    public Long getShowReject() {
        return showReject;
    }

    /**
     * @param showReject the showReject to set
     */
    public void setShowReject(Long showReject) {
        this.showReject = showReject;
    }

    /**
     * @return the showApprove
     */
    public Long getShowApprove() {
        return showApprove;
    }

    /**
     * @param showApprove the showApprove to set
     */
    public void setShowApprove(Long showApprove) {
        this.showApprove = showApprove;
    }

    /**
     * @return the requesterCancel
     */
    public String getRequesterCancel() {
        return requesterCancel;
    }

    /**
     * @param requesterCancel the requesterCancel to set
     */
    public void setRequesterCancel(String requesterCancel) {
        this.requesterCancel = requesterCancel;
    }

}
