/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.Customer;
import java.text.SimpleDateFormat;

/**
 *
 * @author cuongdm
 */
public class Request {

    public static final String CATEGORY_USER = "user";
    public static final String CATEGORY_VIDEO = "video";
    public static final String ACTION_ADD_USER = "addUser";
    public static final String ACTION_GET_USER_LIST = "userList";
    public static final String ACTION_UPDATE_USER = "updateUser";
    public static final String ACTION_DELETE_USER = "deleteUser";
    public static final String ACTION_FIND_DEVICE_BY_ID = "findDeviceByUniqueIdentifier";
    public static final String ACTION_FIND_DEVICE_BY_SERIAL = "findDeviceBySerialNumber";
    public static final String ACTION_GET_USER_DEVICE_LIST = "userDeviceList";
    public static final String ACTION_ADD_DEVICE_TO_USER = "addDeviceToUser";
    public static final String ACTION_DELETE_DEVICE_TO_USER = "deleteDeviceFromUser";
    public static final String ACTION_GET_PACKAGE_LIST = "packageList";
    public static final String ACTION_GET_USER_DEVICE_PACKAGE_LIST = "userDevicePackageList";
    public static final String ACTION_ADD_USER_DEVICE_PACKAGE_LIST = "addPackageToUserDevice";
    public static final String ACTION_DEL_USER_DEVICE_PACKAGE_LIST = "deletePackageFromUserDevice";
    private RequestBundleTv request;

    /**
     * @return the request
     */
    public RequestBundleTv getRequest() {
        return request;
    }

    /**
     * @param request the request to set
     */
    public void setRequest(RequestBundleTv request) {
        this.request = request;
    }

    public void addUser(Customer customer, String accountTv) {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
        request = new RequestBundleTv(CATEGORY_USER, ACTION_ADD_USER);
        Content content = new Content();
        content.setFirst_name(customer.getName());
        content.setLast_name("");
        content.setBirthday(customer.getBirthDate() != null ? dt.format(customer.getBirthDate()) : "");
        content.setCountry_code("855");
        content.setPhone_number(customer.getTelFax() != null ? customer.getTelFax() : "");
        content.setAddress(customer.getAddress());
        content.setPost_code("");
        content.setAccount(accountTv);
        request.setContent(content);
    }

    public void getUserList(String pageSize, String pageIndex) {
        request = new RequestBundleTv(CATEGORY_USER, ACTION_GET_USER_LIST);
        Content content = new Content();
        content.setPage_size(pageSize);
        content.setPage_index(pageIndex);
        request.setContent(content);
    }

    public void updateUser(Customer customer) {
        request = new RequestBundleTv(CATEGORY_USER, ACTION_UPDATE_USER);
    }

    public void deleteUser(String userUniqueId) {
        request = new RequestBundleTv(CATEGORY_USER, ACTION_DELETE_USER);
        Content content = new Content();
        content.setUser_unique_id(userUniqueId);
        request.setContent(content);
    }

    public void findDeviceById(String deviceUniqueId) {
        request = new RequestBundleTv(CATEGORY_VIDEO, ACTION_FIND_DEVICE_BY_ID);
        Content content = new Content();
        content.setDevice_unique_id(deviceUniqueId);
        request.setContent(content);
    }

    public void findDeviceBySerial(String serial) {
        request = new RequestBundleTv(CATEGORY_VIDEO, ACTION_FIND_DEVICE_BY_SERIAL);
        Content content = new Content();
        content.setDevice_serial_number(serial);
        request.setContent(content);
    }

    public void getUserDeviceList(String userUniqueId) {
        request = new RequestBundleTv(CATEGORY_USER, ACTION_GET_USER_DEVICE_LIST);
        Content content = new Content();
        content.setUser_unique_id(userUniqueId);
        request.setContent(content);
    }

    public void addDeviceToUser(String userUniqueId, String deviceUniqueId) {
        request = new RequestBundleTv(CATEGORY_USER, ACTION_ADD_DEVICE_TO_USER);
        Content content = new Content();
        content.setUser_unique_id(userUniqueId);
        content.setDevice_unique_id(deviceUniqueId);
        request.setContent(content);
    }

    public void deleteDeviceToUser(String userUniqueId, String deviceUniqueId) {
        request = new RequestBundleTv(CATEGORY_USER, ACTION_DELETE_DEVICE_TO_USER);
        Content content = new Content();
        content.setUser_unique_id(userUniqueId);
        content.setDevice_unique_id(deviceUniqueId);
        request.setContent(content);
    }

    public void getPackageList() {
        request = new RequestBundleTv(CATEGORY_VIDEO, ACTION_GET_PACKAGE_LIST);
    }

    public void getUserDevicePackageList(String userUniqueId, String deviceSerialNumber) {
        request = new RequestBundleTv(CATEGORY_VIDEO, ACTION_GET_USER_DEVICE_PACKAGE_LIST);
        Content content = new Content();
        content.setUser_unique_id(userUniqueId);
        content.setDevice_serial_number(deviceSerialNumber);
        request.setContent(content);
    }

    public void addUserDevicePackageList(String userUniqueId, String deviceSerialNumber, String packageUniqueId) {
        request = new RequestBundleTv(CATEGORY_VIDEO, ACTION_ADD_USER_DEVICE_PACKAGE_LIST);
        Content content = new Content();
        content.setUser_unique_id(userUniqueId);
        content.setDevice_serial_number(deviceSerialNumber);
        content.setPackage_unique_id(packageUniqueId);
        request.setContent(content);
    }

    public void deleteUserDevicePackageList(String userUniqueId, String deviceUniqueId, String packageUniqueId) {
        request = new RequestBundleTv(CATEGORY_VIDEO, ACTION_DEL_USER_DEVICE_PACKAGE_LIST);
        Content content = new Content();
        content.setUser_unique_id(userUniqueId);
        content.setDevice_unique_id(deviceUniqueId);
        content.setPackage_unique_id(packageUniqueId);
        request.setContent(content);
    }
}
