/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.ApParam;
import java.util.List;

/**
 *
 * @author linhlh2
 */
public class RequestStatusOut {

    private String errorCode;
    private String errorDecription;
    private List<ApParam> lstReqStatus;

    public RequestStatusOut() {
    }

    public RequestStatusOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public RequestStatusOut(String errorCode, String errorDecription, List<ApParam> lstReqStatus) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.lstReqStatus = lstReqStatus;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ApParam> getLstReqStatus() {
        return lstReqStatus;
    }

    public void setLstReqStatus(List<ApParam> lstReqStatus) {
        this.lstReqStatus = lstReqStatus;
    }
}
