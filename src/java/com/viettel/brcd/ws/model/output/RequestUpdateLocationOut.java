/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.RequestUpdateCusLocationDetail;
import com.viettel.bccs.cm.model.RequestUpdateLocation;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RequestUpdateLocation")
public class RequestUpdateLocationOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement
    private Long totalPage;
    @XmlElement
    private Long totalSize;
    
    @XmlElement(name = "requestUpdateLocationDetail")
    private List<RequestUpdateCusLocationDetail> ruls;
    

    public RequestUpdateLocationOut() {
    }

    public RequestUpdateLocationOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public RequestUpdateLocationOut(String errorCode, String errorDecription,Long totalPage,Long totalSize,List<RequestUpdateCusLocationDetail> ruls) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.ruls = ruls;
        this.totalPage = totalPage;
        this.totalSize = totalSize;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public Long getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Long totalPage) {
        this.totalPage = totalPage;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    public List<RequestUpdateCusLocationDetail> getRuls() {
        return ruls;
    }

    public void setRuls(List<RequestUpdateCusLocationDetail> ruls) {
        this.ruls = ruls;
    }
    
}
