package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.database.BO.SalaryStaff;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "salaryOut")
public class SalaryOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "staff")
    private List<SalaryStaff> lstStaff;

    public List<SalaryStaff> getLstStaff() {
        return lstStaff;
    }

    public void setLstStaff(List<SalaryStaff> lstStaff) {
        this.lstStaff = lstStaff;
    }

    public SalaryOut() {
    }

    public SalaryOut(String errorCode) {
        this.errorCode = errorCode;
    }

    public SalaryOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String responeCode) {
        this.errorCode = responeCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }
}
