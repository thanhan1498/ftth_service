/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.VAdslRequest;
import java.util.List;

public class SearchSubRequestOut {

    private String errorCode;
    private String errorDecription;
    private List<VAdslRequest> subRequests;
    private Long rowCount;

    public SearchSubRequestOut() {
    }

    public SearchSubRequestOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public SearchSubRequestOut(String errorCode, String errorDecription, List<VAdslRequest> subRequests, Long rowCount) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.subRequests = subRequests;
        this.rowCount = rowCount;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<VAdslRequest> getSubRequests() {
        return subRequests;
    }

    public void setSubRequests(List<VAdslRequest> subRequests) {
        this.subRequests = subRequests;
    }

    public Long getRowCount() {
        return rowCount;
    }

    public void setRowCount(Long rowCount) {
        this.rowCount = rowCount;
    }
}
