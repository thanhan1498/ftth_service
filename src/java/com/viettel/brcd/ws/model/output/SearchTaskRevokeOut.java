package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.api.Task.BO.ViewTaskStaff;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SearchRevokeOut")
public class SearchTaskRevokeOut {

    @XmlElement(name = "taskRevoke")
    private List<ViewTaskStaff> lstStaff;
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    private String total;

    public List<ViewTaskStaff> getLstStaff() {
        return lstStaff;
    }

    public void setLstStaff(List<ViewTaskStaff> lstStaff) {
        this.lstStaff = lstStaff;
    }

    public SearchTaskRevokeOut() {
    }

    public SearchTaskRevokeOut(String errorCode) {
        this.errorCode = errorCode;
    }

    public SearchTaskRevokeOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String responeCode) {
        this.errorCode = responeCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    /**
     * @return the total
     */
    public String getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(String total) {
        this.total = total;
    }
}
