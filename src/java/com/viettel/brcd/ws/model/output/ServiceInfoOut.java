/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.ServiceInfo;
import com.viettel.bccs.cm.model.TaskProgress;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nguyen Tran Minh Nhut
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "serviceInfoOut")
public class ServiceInfoOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "serviceInfo")
    private List<ServiceInfo> lstService;

    public ServiceInfoOut(String errorCode, String errorDecription, List<ServiceInfo> lstService) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.lstService = lstService;
    }

    public ServiceInfoOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ServiceInfoOut() {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<ServiceInfo> getLstService() {
        return lstService;
    }

    public void setLstService(List<ServiceInfo> lstService) {
        this.lstService = lstService;
    }

   
}
