package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.database.BO.ServicePayment;
import com.viettel.bccs.cm.model.Reason;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ServicePaymentOut")
public class ServicePaymentOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "service")
    private ServicePayment service;
    private List<Reason> reasons;
    private RecoverInfo recoverInfo;

    public ServicePaymentOut() {
    }

    public ServicePaymentOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public ServicePaymentOut(String errorCode, String errorDecription, ServicePayment service) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.service = service;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public ServicePayment getService() {
        return service;
    }

    public void setService(ServicePayment service) {
        this.service = service;
    }

    /**
     * @return the reasons
     */
    public List<Reason> getReasons() {
        return reasons;
    }

    /**
     * @param reasons the reasons to set
     */
    public void setReasons(List<Reason> reasons) {
        this.reasons = reasons;
    }

    /**
     * @return the recoverInfo
     */
    public RecoverInfo getRecoverInfo() {
        return recoverInfo;
    }

    /**
     * @param recoverInfo the recoverInfo to set
     */
    public void setRecoverInfo(RecoverInfo recoverInfo) {
        this.recoverInfo = recoverInfo;
    }
}
