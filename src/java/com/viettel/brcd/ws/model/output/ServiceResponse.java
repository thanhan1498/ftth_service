/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author cuongdm
 */
public class ServiceResponse {

    private String status;
    private String result_code;
    private String detail;
    private String ne_name;
    private String card_olt;
    private String port_olt;
    private String port_logic;
    private String result;
    private String message;
    private Content content;
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the result_code
     */
    public String getResult_code() {
        return result_code;
    }

    /**
     * @param result_code the result_code to set
     */
    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

    /**
     * @return the detail
     */
    public String getDetail() {
        return detail;
    }

    /**
     * @param detail the detail to set
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * @return the ne_name
     */
    public String getNe_name() {
        return ne_name;
    }

    /**
     * @param ne_name the ne_name to set
     */
    public void setNe_name(String ne_name) {
        this.ne_name = ne_name;
    }

    /**
     * @return the card_olt
     */
    public String getCard_olt() {
        return card_olt;
    }

    /**
     * @param card_olt the card_olt to set
     */
    public void setCard_olt(String card_olt) {
        this.card_olt = card_olt;
    }

    /**
     * @return the port_olt
     */
    public String getPort_olt() {
        return port_olt;
    }

    /**
     * @param port_olt the port_olt to set
     */
    public void setPort_olt(String port_olt) {
        this.port_olt = port_olt;
    }

    /**
     * @return the port_logic
     */
    public String getPort_logic() {
        return port_logic;
    }

    /**
     * @param port_logic the port_logic to set
     */
    public void setPort_logic(String port_logic) {
        this.port_logic = port_logic;
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the content
     */
    public Content getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(Content content) {
        this.content = content;
    }
}
