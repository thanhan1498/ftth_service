/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import java.util.List;

/**
 *
 * @author cuongdm
 */
public class ServiceResponseList {

    private String result;
    private String message;
    private List<ListDataTv> content;

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the content
     */
    public List<ListDataTv> getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(List<ListDataTv> content) {
        this.content = content;
    }

}
