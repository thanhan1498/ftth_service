/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.SourceType;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/**
 *
 * @author Nguyen Tran Minh Nhut
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "sourceTypeOutput")
public class SourceTypeOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name ="sourceType")
    private List<SourceType> listSourceType;

    public SourceTypeOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public SourceTypeOut() {
    }

    public SourceTypeOut(String errorCode, String errorDecription, List<SourceType> listSourceType) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.listSourceType = listSourceType;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<SourceType> getListSourceType() {
        return listSourceType;
    }

    public void setListSourceType(List<SourceType> listSourceType) {
        this.listSourceType = listSourceType;
    }
    
}
