/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.StaffInfo;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nguyen Tran Minh Nhut
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "staffInfoOut")
public class StaffInfoOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "staffInfo")
    private List<StaffInfo> lstStaffInfo;

    public StaffInfoOut(String errorCode, String errorDecription, List<StaffInfo> lstStaffInfo) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.lstStaffInfo = lstStaffInfo;
    }

    public StaffInfoOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public StaffInfoOut() {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<StaffInfo> getLstStaffInfo() {
        return lstStaffInfo;
    }

    public void setLstStaffInfo(List<StaffInfo> lstStaffInfo) {
        this.lstStaffInfo = lstStaffInfo;
    }
}
