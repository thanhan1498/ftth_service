/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "StaffManageConnectorInfo")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StaffManageConnectorInfo", propOrder = {
    "name",
    "code",
    "branch",
    "teamName",
    "teamCode",
    "connectors"
})
public class StaffManageConnectorInfo {

    private String name;
    private String code;
    private String branch;
    private String teamName;
    private String teamCode;
    private List<ConnectorInfo> connectors;

    public StaffManageConnectorInfo() {
        connectors = new ArrayList<ConnectorInfo>();
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the branch
     */
    public String getBranch() {
        return branch;
    }

    /**
     * @param branch the branch to set
     */
    public void setBranch(String branch) {
        this.branch = branch;
    }

    /**
     * @return the teamName
     */
    public String getTeamName() {
        return teamName;
    }

    /**
     * @param teamName the teamName to set
     */
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    /**
     * @return the connectors
     */
    public List<ConnectorInfo> getConnectors() {
        return connectors;
    }

    /**
     * @param connectors the connectors to set
     */
    public void setConnectors(List<ConnectorInfo> connectors) {
        this.connectors = connectors;
    }

    /**
     * @return the teamCode
     */
    public String getTeamCode() {
        return teamCode;
    }

    /**
     * @param teamCode the teamCode to set
     */
    public void setTeamCode(String teamCode) {
        this.teamCode = teamCode;
    }
}
