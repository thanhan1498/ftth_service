/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "return")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class StaffSalaryDetail extends WSRespone {

    private int month;
    private int year;
    private String message;
    private String branch;
    private String district;
    private String teamName;
    private String teamCode;
    private String teamLeader;
    private String staffCode;
    private String staffName;
    private String total;
    private StaffSalaryDetailInfo salary;
    private StaffSalaryDetailInfo detail;

    public StaffSalaryDetail() {
    }

    public StaffSalaryDetail(String errorCode, String errDesc) {
        this.errorCode = errorCode;
        this.errorDecription = errDesc;
    }

    /**
     * @return the month
     */
    public int getMonth() {
        return month;
    }

    /**
     * @param month the month to set
     */
    public void setMonth(int month) {
        this.month = month;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return the branch
     */
    public String getBranch() {
        return branch;
    }

    /**
     * @param branch the branch to set
     */
    public void setBranch(String branch) {
        this.branch = branch;
    }

    /**
     * @return the district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * @param district the district to set
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     * @return the teamName
     */
    public String getTeamName() {
        return teamName;
    }

    /**
     * @param teamName the teamName to set
     */
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    /**
     * @return the teamCode
     */
    public String getTeamCode() {
        return teamCode;
    }

    /**
     * @param teamCode the teamCode to set
     */
    public void setTeamCode(String teamCode) {
        this.teamCode = teamCode;
    }

    /**
     * @return the teamLeader
     */
    public String getTeamLeader() {
        return teamLeader;
    }

    /**
     * @param teamLeader the teamLeader to set
     */
    public void setTeamLeader(String teamLeader) {
        this.teamLeader = teamLeader;
    }

    /**
     * @return the salary
     */
    public StaffSalaryDetailInfo getSalary() {
        return salary;
    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(StaffSalaryDetailInfo salary) {
        this.salary = salary;
    }

    /**
     * @return the staffCode
     */
    public String getStaffCode() {
        return staffCode;
    }

    /**
     * @param staffCode the staffCode to set
     */
    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    /**
     * @return the staffName
     */
    public String getStaffName() {
        return staffName;
    }

    /**
     * @param staffName the staffName to set
     */
    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the detail
     */
    public StaffSalaryDetailInfo getDetail() {
        return detail;
    }

    /**
     * @param detail the detail to set
     */
    public void setDetail(StaffSalaryDetailInfo detail) {
        this.detail = detail;
    }

    /**
     * @return the total
     */
    public String getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(String total) {
        this.total = total;
    }
}
