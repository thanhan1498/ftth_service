/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cuongdm
 */
public class StaffSalaryDetailInfo {

    private String title;
    private String total;
    private String money;
    private String value;
    private String multiplier;
    private String tbqd;
    private List<StaffSalaryDetailInfo> leaf;
    private List<StaffSalaryDetailInfo> node;
    private List<StaffSalaryTable> col;

    public StaffSalaryDetailInfo() {
        leaf = new ArrayList<StaffSalaryDetailInfo>();
        node = new ArrayList<StaffSalaryDetailInfo>();
    }

    public StaffSalaryDetailInfo(String title, String money, String value, String multiplier, String tbqd) {
        this.title = title;
        this.money = money;
        this.value = value;
        this.multiplier = multiplier;
        this.tbqd = tbqd;
        leaf = new ArrayList<StaffSalaryDetailInfo>();
        node = new ArrayList<StaffSalaryDetailInfo>();
    }

    /**
     * @return the total
     */
    public String getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(String total) {
        this.total = total;
    }

    /**
     * @return the money
     */
    public String getMoney() {
        return money;
    }

    /**
     * @param money the money to set
     */
    public void setMoney(String money) {
        this.money = money;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the multiplier
     */
    public String getMultiplier() {
        return multiplier;
    }

    /**
     * @param multiplier the multiplier to set
     */
    public void setMultiplier(String multiplier) {
        this.multiplier = multiplier;
    }

    /**
     * @return the tbqd
     */
    public String getTbqd() {
        return tbqd;
    }

    /**
     * @param tbqd the tbqd to set
     */
    public void setTbqd(String tbqd) {
        this.tbqd = tbqd;
    }

    /**
     * @return the leaf
     */
    public List<StaffSalaryDetailInfo> getLeaf() {
        return leaf;
    }

    /**
     * @param leaf the leaf to set
     */
    public void setLeaf(List<StaffSalaryDetailInfo> leaf) {
        this.leaf = leaf;
    }

    /**
     * @return the node
     */
    public List<StaffSalaryDetailInfo> getNode() {
        return node;
    }

    /**
     * @param node the node to set
     */
    public void setNode(List<StaffSalaryDetailInfo> node) {
        this.node = node;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the col
     */
    public List<StaffSalaryTable> getCol() {
        return col;
    }

    /**
     * @param col the col to set
     */
    public void setCol(List<StaffSalaryTable> col) {
        this.col = col;
    }
}
