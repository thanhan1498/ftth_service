/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author cuongdm
 */
public class StaffSalaryImtResponse {

    private String status;
    private StaffSalaryDetail data;

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the data
     */
    public StaffSalaryDetail getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(StaffSalaryDetail data) {
        this.data = data;
    }
}
