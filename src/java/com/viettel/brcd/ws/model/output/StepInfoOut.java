/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.StepInfo;

public class StepInfoOut {

    private String errorCode;
    private String errorDecription;
    private Boolean cancelSubReq;
    private Boolean signContract;
    private Boolean cancelContract;
    private Boolean activeSubscriber;
    private Long contractId;

    public StepInfoOut() {
    }

    public StepInfoOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public StepInfoOut(String errorCode, String errorDecription, Boolean cancelSubReq, Boolean signContract, Boolean cancelContract, Boolean activeSubscriber) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.cancelSubReq = cancelSubReq;
        this.signContract = signContract;
        this.cancelContract = cancelContract;
        this.activeSubscriber = activeSubscriber;
    }

    public StepInfoOut(String errorCode, String errorDecription, StepInfo stepInfo) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        if (stepInfo != null) {
            this.cancelSubReq = stepInfo.getCancelSubReq();
            this.signContract = stepInfo.getSignContract();
            this.cancelContract = stepInfo.getCancelContract();
            this.activeSubscriber = stepInfo.getActiveSubscriber();
            this.contractId = stepInfo.getContractId();
        }
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public Boolean getCancelSubReq() {
        return cancelSubReq;
    }

    public void setCancelSubReq(Boolean cancelSubReq) {
        this.cancelSubReq = cancelSubReq;
    }

    public Boolean getSignContract() {
        return signContract;
    }

    public void setSignContract(Boolean signContract) {
        this.signContract = signContract;
    }

    public Boolean getCancelContract() {
        return cancelContract;
    }

    public void setCancelContract(Boolean cancelContract) {
        this.cancelContract = cancelContract;
    }

    public Boolean getActiveSubscriber() {
        return activeSubscriber;
    }

    public void setActiveSubscriber(Boolean activeSubscriber) {
        this.activeSubscriber = activeSubscriber;
    }
}
