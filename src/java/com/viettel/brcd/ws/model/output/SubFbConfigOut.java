/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.SubFbConfigDetail;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duyetdk
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SubFbConfig")
public class SubFbConfigOut {
    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement
    private Long totalPage;
    @XmlElement
    private Long totalSize;
    @XmlElement(name = "subFbConfigDetail")
    private List<SubFbConfigDetail> subFbConfigDetails;

    public SubFbConfigOut() {
    }

    public SubFbConfigOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public SubFbConfigOut(String errorCode, String errorDecription, Long totalPage, Long totalSize, List<SubFbConfigDetail> subFbConfigDetails) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.totalPage = totalPage;
        this.totalSize = totalSize;
        this.subFbConfigDetails = subFbConfigDetails;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public Long getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Long totalPage) {
        this.totalPage = totalPage;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    public List<SubFbConfigDetail> getSubFbConfigDetails() {
        return subFbConfigDetails;
    }

    public void setSubFbConfigDetails(List<SubFbConfigDetail> subFbConfigDetails) {
        this.subFbConfigDetails = subFbConfigDetails;
    }
    
}
