/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.SubMb;
import com.viettel.bccs.cm.model.SubMbOut;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author partner1
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SubInfoByISDNPre")
public class SubInfoByISDNPreOut {
    
    private String errorCode;
    private String errorDecription;
    
    @XmlElement(name = "subMb")
    private SubMbOut subMb;

    public SubInfoByISDNPreOut() {
    }

    public SubInfoByISDNPreOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public SubInfoByISDNPreOut(String errorCode, String errorDecription, SubMbOut subMb) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.subMb = subMb;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public SubMbOut getSubMb() {
        return subMb;
    }

    public void setSubMb(SubMbOut subMb) {
        this.subMb = subMb;
    }
    
}
