/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author cuongdm
 */
public class SubInfoSms {

    /*input*/
    private String arpu;
    private String sms;
    private String voice;
    private String data;
    private String gender;
    private String age;
    private String location;
    private String device;
    private String subType;
    private String roaming;
    private String interCal;
    private String numRecord;
    /*output*/
    private String subId;
    private String isdn;
    private String status;
    private String usageType;
    private String province;
    private String district;
    private String commune;
    private String latitude;
    private String longtitude;
    private String createDate;

    /**
     * @return the arpu
     */
    public String getArpu() {
        return arpu;
    }

    /**
     * @param arpu the arpu to set
     */
    public void setArpu(String arpu) {
        this.arpu = arpu;
    }

    /**
     * @return the sms
     */
    public String getSms() {
        return sms;
    }

    /**
     * @param sms the sms to set
     */
    public void setSms(String sms) {
        this.sms = sms;
    }

    /**
     * @return the voice
     */
    public String getVoice() {
        return voice;
    }

    /**
     * @param voice the voice to set
     */
    public void setVoice(String voice) {
        this.voice = voice;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the age
     */
    public String getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(String age) {
        this.age = age;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the device
     */
    public String getDevice() {
        return device;
    }

    /**
     * @param device the device to set
     */
    public void setDevice(String device) {
        this.device = device;
    }

    /**
     * @return the subType
     */
    public String getSubType() {
        return subType;
    }

    /**
     * @param subType the subType to set
     */
    public void setSubType(String subType) {
        this.subType = subType;
    }

    /**
     * @return the roaming
     */
    public String getRoaming() {
        return roaming;
    }

    /**
     * @param roaming the roaming to set
     */
    public void setRoaming(String roaming) {
        this.roaming = roaming;
    }

    /**
     * @return the interCal
     */
    public String getInterCal() {
        return interCal;
    }

    /**
     * @param interCal the interCal to set
     */
    public void setInterCal(String interCal) {
        this.interCal = interCal;
    }

    /**
     * @return the subId
     */
    public String getSubId() {
        return subId;
    }

    /**
     * @param subId the subId to set
     */
    public void setSubId(String subId) {
        this.subId = subId;
    }

    /**
     * @return the isdn
     */
    public String getIsdn() {
        return isdn;
    }

    /**
     * @param isdn the isdn to set
     */
    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the usageType
     */
    public String getUsageType() {
        return usageType;
    }

    /**
     * @param usageType the usageType to set
     */
    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    /**
     * @return the province
     */
    public String getProvince() {
        return province;
    }

    /**
     * @param province the province to set
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     * @return the district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * @param district the district to set
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     * @return the commune
     */
    public String getCommune() {
        return commune;
    }

    /**
     * @param commune the commune to set
     */
    public void setCommune(String commune) {
        this.commune = commune;
    }

    /**
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longtitude
     */
    public String getLongtitude() {
        return longtitude;
    }

    /**
     * @param longtitude the longtitude to set
     */
    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    /**
     * @return the createDate
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the numRecord
     */
    public String getNumRecord() {
        return numRecord;
    }

    /**
     * @param numRecord the numRecord to set
     */
    public void setNumRecord(String numRecord) {
        this.numRecord = numRecord;
    }

}
