/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.SubBundleTv;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author linhlh2
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "subscriber")
public class Subscriber {

    @XmlElement(required = true)
    private Long productId;
    @XmlElement(required = true)
    private String productCode;
    @XmlElement
    private String productName;
    @XmlElement(required = true)
    private Long serviceId;
    @XmlElement
    private String roamMode;
    @XmlElement(required = true)
    private String promotion;
    @XmlElement
    private String deposit;
    @XmlElement
    private String limit;
    @XmlElement
    private String reasonRoam;
    @XmlElement
    private String customPhone;
    @XmlElement
    private String payMode;
    @XmlElement
    private String recvChargeMode;
    @XmlElement
    private String lineType;
    @XmlElement
    private String subType;
    @XmlElement
    private Long pricePlan;
    @XmlElement
    private Long localPricePlan;
    @XmlElement
    private String oldAccount;
    private SubBundleTv bundleTv;

    public Long getPricePlan() {
        return pricePlan;
    }

    public void setPricePlan(Long pricePlan) {
        this.pricePlan = pricePlan;
    }

    public Long getLocalPricePlan() {
        return localPricePlan;
    }

    public void setLocalPricePlan(Long localPricePlan) {
        this.localPricePlan = localPricePlan;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getRoamMode() {
        return roamMode;
    }

    public void setRoamMode(String roamMode) {
        this.roamMode = roamMode;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getReasonRoam() {
        return reasonRoam;
    }

    public void setReasonRoam(String reasonRoam) {
        this.reasonRoam = reasonRoam;
    }

    public String getCustomPhone() {
        return customPhone;
    }

    public void setCustomPhone(String customPhone) {
        this.customPhone = customPhone;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getRecvChargeMode() {
        return recvChargeMode;
    }

    public void setRecvChargeMode(String recvChargeMode) {
        this.recvChargeMode = recvChargeMode;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    /**
     * @return the oldAccount
     */
    public String getOldAccount() {
        return oldAccount;
    }

    /**
     * @param oldAccount the oldAccount to set
     */
    public void setOldAccount(String oldAccount) {
        this.oldAccount = oldAccount;
    }

    /**
     * @return the bundleTv
     */
    public SubBundleTv getBundleTv() {
        return bundleTv;
    }

    /**
     * @param bundleTv the bundleTv to set
     */
    public void setBundleTv(SubBundleTv bundleTv) {
        this.bundleTv = bundleTv;
    }
}
