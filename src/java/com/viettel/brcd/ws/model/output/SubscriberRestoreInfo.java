/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.pre.CustomerPre;
import com.viettel.bccs.cm.model.pre.SubMbPre;

/**
 *
 * @author cuongdm
 */
public class SubscriberRestoreInfo {

    private CustomerPre customer;
    private SubMbPre subscriber;
    private String productName;
    private String offerName;
    private String hasCustomer;
    private Long custImgId;
    /**
     * @return the customer
     */
    public CustomerPre getCustomer() {
        return customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(CustomerPre customer) {
        this.customer = customer;
    }

    /**
     * @return the subscriber
     */
    public SubMbPre getSubscriber() {
        return subscriber;
    }

    /**
     * @param subscriber the subscriber to set
     */
    public void setSubscriber(SubMbPre subscriber) {
        this.subscriber = subscriber;
    }

    /**
     * @return the hasCustomer
     */
    public String getHasCustomer() {
        return hasCustomer;
    }

    /**
     * @param hasCustomer the hasCustomer to set
     */
    public void setHasCustomer(String hasCustomer) {
        this.hasCustomer = hasCustomer;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the offerName
     */
    public String getOfferName() {
        return offerName;
    }

    /**
     * @param offerName the offerName to set
     */
    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    /**
     * @return the custImgId
     */
    public Long getCustImgId() {
        return custImgId;
    }

    /**
     * @param custImgId the custImgId to set
     */
    public void setCustImgId(Long custImgId) {
        this.custImgId = custImgId;
    }
}
