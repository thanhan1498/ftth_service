/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import java.util.List;

/**
 *
 * @author Nguyen Tran Minh Nhut
 */
public class TaskAssign {

    String taskName;
    String account;
    String contractNo;
    String deployAddress;
    String limitDate;
    String taskCreateDate;
    Long taskMngtId;
    Long taskShopProgress;
    String staffName;
    Long isSystemError;
    String shopName;
    String progress;
    String custName;
    Long custReqId;
    String custTelFax;
    Long telServiceId;
    Long subId;
    Long shopId;
    Long staffId;
    String endDateValue;
    String dsLamName;
    String productName;
    String sourceType;
    Long taskShopMngtId;
    String startDateValue;
    String reqType;
    String serviceType;
    String pricePlanName;
    String localPricePlanName;
    String pricePlanSpeed;
    String compContent;
    String startDate;
    String endDate;
    String textColor;
    String backgroundColor;
    String assignTo;
    String distanceToInfra;
    List<CoordinateSubDrawCable> coordinates;
    private int isVip;

    public String getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(String assignTo) {
        this.assignTo = assignTo;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public Long getCustReqId() {
        return custReqId;
    }

    public void setCustReqId(Long custReqId) {
        this.custReqId = custReqId;
    }

    public String getCustTelFax() {
        return custTelFax;
    }

    public void setCustTelFax(String custTelFax) {
        this.custTelFax = custTelFax;
    }

    public Long getTelServiceId() {
        return telServiceId;
    }

    public void setTelServiceId(Long telServiceId) {
        this.telServiceId = telServiceId;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getEndDateValue() {
        return endDateValue;
    }

    public void setEndDateValue(String endDateValue) {
        this.endDateValue = endDateValue;
    }

    public String getDsLamName() {
        return dsLamName;
    }

    public void setDsLamName(String dsLamName) {
        this.dsLamName = dsLamName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public Long getTaskShopMngtId() {
        return taskShopMngtId;
    }

    public void setTaskShopMngtId(Long taskShopMngtId) {
        this.taskShopMngtId = taskShopMngtId;
    }

    public String getStartDateValue() {
        return startDateValue;
    }

    public void setStartDateValue(String startDateValue) {
        this.startDateValue = startDateValue;
    }

    public String getReqType() {
        return reqType;
    }

    public void setReqType(String reqType) {
        this.reqType = reqType;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getPricePlanName() {
        return pricePlanName;
    }

    public void setPricePlanName(String pricePlanName) {
        this.pricePlanName = pricePlanName;
    }

    public String getLocalPricePlanName() {
        return localPricePlanName;
    }

    public void setLocalPricePlanName(String localPricePlanName) {
        this.localPricePlanName = localPricePlanName;
    }

    public String getPricePlanSpeed() {
        return pricePlanSpeed;
    }

    public void setPricePlanSpeed(String pricePlanSpeed) {
        this.pricePlanSpeed = pricePlanSpeed;
    }

    public String getCompContent() {
        return compContent;
    }

    public void setCompContent(String compContent) {
        this.compContent = compContent;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getDeployAddress() {
        return deployAddress;
    }

    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    public String getLimitDate() {
        return limitDate;
    }

    public void setLimitDate(String limitDate) {
        this.limitDate = limitDate;
    }

    public String getTaskCreateDate() {
        return taskCreateDate;
    }

    public void setTaskCreateDate(String taskCreateDate) {
        this.taskCreateDate = taskCreateDate;
    }

    public Long getTaskMngtId() {
        return taskMngtId;
    }

    public void setTaskMngtId(Long taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    public Long getTaskShopProgress() {
        return taskShopProgress;
    }

    public void setTaskShopProgress(Long taskShopProgress) {
        this.taskShopProgress = taskShopProgress;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public Long getIsSystemError() {
        return isSystemError;
    }

    public void setIsSystemError(Long isSystemError) {
        this.isSystemError = isSystemError;
    }

    public String getDistanceToInfra() {
        return distanceToInfra;
    }

    public void setDistanceToInfra(String distanceToInfra) {
        this.distanceToInfra = distanceToInfra;
    }

    public List<CoordinateSubDrawCable> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<CoordinateSubDrawCable> coordinates) {
        this.coordinates = coordinates;
    }

    /**
     * @return the isVip
     */
    public int getIsVip() {
        return isVip;
    }

    /**
     * @param isVip the isVip to set
     */
    public void setIsVip(int isVip) {
        this.isVip = isVip;
    }
}
