/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.AssignToObject;
import com.viettel.bccs.cm.model.TaskInfoDetail;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User-PC
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskDetailToAssignOut")
public class TaskDetailToAssignOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "assignToObject")
    private List<AssignToObject> listObject;
    @XmlElement(name = "taskInfoDetail")
    private TaskInfoDetail taskInfo;

    public TaskDetailToAssignOut() {
    }

    public TaskDetailToAssignOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

   
    public TaskInfoDetail getTaskInfo() {
        return taskInfo;
    }

    public void setTaskInfo(TaskInfoDetail taskInfo) {
        this.taskInfo = taskInfo;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<AssignToObject> getListObject() {
        return listObject;
    }

    public void setListObject(List<AssignToObject> listObject) {
        this.listObject = listObject;
    }
}
