/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.api.Task.BO.DetailInfoTask;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nguyen Ngoc Viet
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskInfoDetailOut")
public class TaskInfoDetailOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "taskInfoDetail")
    private DetailInfoTask infoDetailTask;

    public TaskInfoDetailOut() {
    }

    public TaskInfoDetailOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public DetailInfoTask getInfoDetailTask() {
        return infoDetailTask;
    }

    public void setInfoDetailTask(DetailInfoTask infoDetailTask) {
        this.infoDetailTask = infoDetailTask;
    }

   
}
