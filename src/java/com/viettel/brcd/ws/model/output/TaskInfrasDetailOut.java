/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.api.Task.BO.SubDeployment;
import com.viettel.bccs.api.Task.BO.SubStockModelRel;
import com.viettel.bccs.api.Task.BO.TaskStageItem;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nguyen Ngoc Viet
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskInfrasDetailOut")
public class TaskInfrasDetailOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "good")
    List<SubStockModelRel> lstGood = null;
    @XmlElement(name = "item")
    List<TaskStageItem> lstItem = null;
    @XmlElement(name = "subInfo")
    SubDeployment subDep = null;

    public TaskInfrasDetailOut() {
    }

    public TaskInfrasDetailOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<SubStockModelRel> getLstGood() {
        return lstGood;
    }

    public void setLstGood(List<SubStockModelRel> lstGood) {
        this.lstGood = lstGood;
    }

    public List<TaskStageItem> getLstItem() {
        return lstItem;
    }

    public void setLstItem(List<TaskStageItem> lstItem) {
        this.lstItem = lstItem;
    }

    public SubDeployment getSubDep() {
        return subDep;
    }

    public void setSubDep(SubDeployment subDep) {
        this.subDep = subDep;
    }
}
