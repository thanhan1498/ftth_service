/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.TaskProgress;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nguyen Tran Minh Nhut
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskProgressOut")
public class TaskProgressOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "taskProgress")
    private List<TaskProgress> lstProgress;

    public TaskProgressOut(String errorCode, String errorDecription, List<TaskProgress> lstProgress) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.lstProgress = lstProgress;
    }

    public TaskProgressOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public TaskProgressOut() {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<TaskProgress> getLstProgress() {
        return lstProgress;
    }

    public void setLstProgress(List<TaskProgress> lstProgress) {
        this.lstProgress = lstProgress;
    }

}
