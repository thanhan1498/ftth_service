/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Vietnn6
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskWarningOut")
public class TaskWarningOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement(name = "warningTask")
    private List<WarningTask> lstWarningTask;

    public TaskWarningOut() {
    }

    public TaskWarningOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public List<WarningTask> getLstWarningTask() {
        return lstWarningTask;
    }

    public void setLstWarningTask(List<WarningTask> lstWarningTask) {
        this.lstWarningTask = lstWarningTask;
    }
}
