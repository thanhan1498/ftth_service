/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 * UnpaidPaymentDetail
 *
 * @author phuonghc
 */
public class UnpaidPaymentDetail {

    private String contractId;

    public UnpaidPaymentDetail() {
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }
}
