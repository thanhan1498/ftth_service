/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author linhlh2
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "updateResult")
public class UpdateResultOut {

    @XmlElement
    private String errorCode;
    @XmlElement
    private String errorDecription;
    @XmlElement
    private boolean result;
    @XmlElement
    private String token;
    private Long reqId;
    private String account;
    private String accBundleTv;

    public UpdateResultOut() {
    }

    public UpdateResultOut(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public UpdateResultOut(String errorCode, String errorDecription, boolean result) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.result = result;
    }

    public UpdateResultOut(String errorCode, String errorDecription, boolean result, Long reqId) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
        this.result = result;
        this.reqId = reqId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getReqId() {
        return reqId;
    }

    public void setReqId(Long reqId) {
        this.reqId = reqId;
    }

    /**
     * @return the accBundleTv
     */
    public String getAccBundleTv() {
        return accBundleTv;
    }

    /**
     * @param accBundleTv the accBundleTv to set
     */
    public void setAccBundleTv(String accBundleTv) {
        this.accBundleTv = accBundleTv;
    }
}
