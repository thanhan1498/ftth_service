/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author duyetdk
 */
public class VBtsAccount {

    private Long custId;
    private Long contractId;
    private String account;
    private String latth;
    private String longth;

    public VBtsAccount() {
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }
    
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getLatth() {
        return latth;
    }

    public void setLatth(String latth) {
        this.latth = latth;
    }

    public String getLongth() {
        return longth;
    }

    public void setLongth(String longth) {
        this.longth = longth;
    }
}
