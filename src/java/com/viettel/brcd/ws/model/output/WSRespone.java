package com.viettel.brcd.ws.model.output;

public class WSRespone {

    private String messageCode;
    protected String errorCode;
    protected String errorDecription;
    private String merchantAccount;

    public WSRespone() {
    }

    public WSRespone(String errorCode) {
        this.errorCode = errorCode;
    }

    public WSRespone(String errorCode, String errorDecription) {
        this.errorCode = errorCode;
        this.errorDecription = errorDecription;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String responeCode) {
        this.errorCode = responeCode;
    }

    public String getErrorDecription() {
        return errorDecription;
    }

    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    /**
     * @return the merchantAccount
     */
    public String getMerchantAccount() {
        return merchantAccount;
    }

    /**
     * @param merchantAccount the merchantAccount to set
     */
    public void setMerchantAccount(String merchantAccount) {
        this.merchantAccount = merchantAccount;
    }

    /**
     * @return the messageCode
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * @param messageCode the messageCode to set
     */
    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }
}
