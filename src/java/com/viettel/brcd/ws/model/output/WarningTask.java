/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

/**
 *
 * @author vietnn6
 */
public class WarningTask {

    Long totalTaskExpire;
    Long totalTask;
    Long todoTask;
    Long totalTaskFinishInMonth;
    Long totalTaskInMonth;
    Long sourceType;
    String totalTaskExpireName;
    String totalTaskFinishName;
    String totalTaskTodoName;

    public Long getSourceType() {
        return sourceType;
    }

    public void setSourceType(Long sourceType) {
        this.sourceType = sourceType;
    }

    public String getTotalTaskTodoName() {
        return totalTaskTodoName;
    }

    public void setTotalTaskTodoName(String totalTaskTodoName) {
        this.totalTaskTodoName = totalTaskTodoName;
    }

    public Long getTodoTask() {
        return todoTask;
    }

    public void setTodoTask(Long todoTask) {
        this.todoTask = todoTask;
    }

    public Long getTotalTaskFinishInMonth() {
        return totalTaskFinishInMonth;
    }

    public void setTotalTaskFinishInMonth(Long totalTaskFinishInMonth) {
        this.totalTaskFinishInMonth = totalTaskFinishInMonth;
    }

    public Long getTotalTaskInMonth() {
        return totalTaskInMonth;
    }

    public void setTotalTaskInMonth(Long totalTaskInMonth) {
        this.totalTaskInMonth = totalTaskInMonth;
    }

    public Long getTotalTaskExpire() {
        return totalTaskExpire;
    }

    public void setTotalTaskExpire(Long totalTaskExpire) {
        this.totalTaskExpire = totalTaskExpire;
    }

    public Long getTotalTask() {
        return totalTask;
    }

    public void setTotalTask(Long totalTask) {
        this.totalTask = totalTask;
    }

    public String getTotalTaskExpireName() {
        return totalTaskExpireName;
    }

    public void setTotalTaskExpireName(String totalTaskExpireName) {
        this.totalTaskExpireName = totalTaskExpireName;
    }

    public String getTotalTaskFinishName() {
        return totalTaskFinishName;
    }

    public void setTotalTaskFinishName(String totalTaskFinishName) {
        this.totalTaskFinishName = totalTaskFinishName;
    }
}
