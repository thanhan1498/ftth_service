/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import com.viettel.bccs.cm.model.ODFIndoorCode;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author nhandv
 */
@XmlRootElement(name = "return")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "return", propOrder = {
    "lst"
})
public class getListODFIndoorByStationCodeOut {

    protected List<ODFIndoorCode> lst;

    public List<ODFIndoorCode> getLst() {
        return lst;
    }

    public void setLst(List<ODFIndoorCode> lst) {
        this.lst = lst;
    }

}
