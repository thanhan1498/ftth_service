/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author nhandv
 */
@XmlRootElement(name = "return")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "return", propOrder = {
    "stationCode",
    "deviceCode",
    "portCode",
    "odfIndoorCode",
    "couplerNo",
    "odfOutdoorCode",
    "odfCode",
    "infraType",
    "subType",
    "setupAddress",
    "subAddress",
    "portId",
    "stationId",
    "deviceId",
    "odfIndoorId",
    "secondCouplerNo",
    "odfId"
})
public class getSubscriptionInfoAONOutPut {

    protected String stationCode;
    protected String deviceCode;
    protected String portCode;
    protected String odfIndoorCode;
    protected String couplerNo;
    protected String odfOutdoorCode;
    protected String odfCode;
    protected String infraType;
    protected String subType;
    protected String setupAddress;
    protected String subAddress;
    protected String portId;
    protected Long stationId;
    protected Long deviceId;
    protected Long odfIndoorId;
    protected Long secondCouplerNo;
    protected Long odfId;

    public Long getOdfId() {
        return odfId;
    }

    public void setOdfId(Long odfId) {
        this.odfId = odfId;
    }
    
    public Long getSecondCouplerNo() {
        return secondCouplerNo;
    }

    public void setSecondCouplerNo(Long secondCouplerNo) {
        this.secondCouplerNo = secondCouplerNo;
    }
    
    public Long getOdfIndoorId() {
        return odfIndoorId;
    }

    public void setOdfIndoorId(Long odfIndoorId) {
        this.odfIndoorId = odfIndoorId;
    }
    
    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getSetupAddress() {
        return setupAddress;
    }

    public void setSetupAddress(String setupAddress) {
        this.setupAddress = setupAddress;
    }

    public String getSubAddress() {
        return subAddress;
    }

    public void setSubAddress(String subAddress) {
        this.subAddress = subAddress;
    }

    public String getPortId() {
        return portId;
    }

    public void setPortId(String portId) {
        this.portId = portId;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getOdfCode() {
        return odfCode;
    }

    public void setOdfCode(String odfCode) {
        this.odfCode = odfCode;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getPortCode() {
        return portCode;
    }

    public void setPortCode(String portCode) {
        this.portCode = portCode;
    }

    public String getOdfIndoorCode() {
        return odfIndoorCode;
    }

    public void setOdfIndoorCode(String odfIndoorCode) {
        this.odfIndoorCode = odfIndoorCode;
    }

    public String getCouplerNo() {
        return couplerNo;
    }

    public void setCouplerNo(String couplerNo) {
        this.couplerNo = couplerNo;
    }

    public String getOdfOutdoorCode() {
        return odfOutdoorCode;
    }

    public void setOdfOutdoorCode(String odfOutdoorCode) {
        this.odfOutdoorCode = odfOutdoorCode;
    }

    public String getInfraType() {
        return infraType;
    }

    public void setInfraType(String infraType) {
        this.infraType = infraType;
    }

}
