/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.model.output;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author nhandv
 */
@XmlRootElement(name = "return")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "return", propOrder = {
    "stationCode",
    "deviceCode",
    "actualPort",
    "portLogic",
    "nodeTbCode",
    "spliterCode",
    "infraType",
    "subType",
    "setupAddress",
    "subAddress",
    "portId",
    "stationId",
    "deviceId",
    "portCode"
})
public class getSubscriptionInfoGponOutPut {

    protected String stationCode;
    protected String deviceCode;
    protected String actualPort;
    protected String portLogic;
    protected String nodeTbCode;
    protected String spliterCode;
    protected String infraType;
    protected String subType;
    protected String setupAddress;
    protected String subAddress;
    protected String portId;
    protected Long stationId;
    protected Long deviceId;
    protected String portCode;

    public String getPortCode() {
        return portCode;
    }

    public void setPortCode(String portCode) {
        this.portCode = portCode;
    }
    
    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }
    
    public String getSetupAddress() {
        return setupAddress;
    }

    public void setSetupAddress(String setupAddress) {
        this.setupAddress = setupAddress;
    }

    public String getSubAddress() {
        return subAddress;
    }

    public void setSubAddress(String subAddress) {
        this.subAddress = subAddress;
    }

    public String getPortId() {
        return portId;
    }

    public void setPortId(String portId) {
        this.portId = portId;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getActualPort() {
        return actualPort;
    }

    public void setActualPort(String actualPort) {
        this.actualPort = actualPort;
    }

    public String getPortLogic() {
        return portLogic;
    }

    public void setPortLogic(String portLogic) {
        this.portLogic = portLogic;
    }

    public String getNodeTbCode() {
        return nodeTbCode;
    }

    public void setNodeTbCode(String nodeTbCode) {
        this.nodeTbCode = nodeTbCode;
    }

    public String getSpliterCode() {
        return spliterCode;
    }

    public void setSpliterCode(String spliterCode) {
        this.spliterCode = spliterCode;
    }

    public String getInfraType() {
        return infraType;
    }

    public void setInfraType(String infraType) {
        this.infraType = infraType;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

}
