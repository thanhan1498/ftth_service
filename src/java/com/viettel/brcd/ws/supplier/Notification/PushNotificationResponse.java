/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.Notification;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "sendMulticastMessageResponse", namespace = "http://service.metfone.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendMulticastMessageResponse", propOrder = {
    "_return"
})
public class PushNotificationResponse {

    @XmlElement(name = "return", nillable = true)
    private String _return;

    /**
     * @return the _return
     */
    public String getReturn() {
        return _return;
    }

    /**
     * @param _return the _return to set
     */
    public void setReturn(String _return) {
        this._return = _return;
    }
}
