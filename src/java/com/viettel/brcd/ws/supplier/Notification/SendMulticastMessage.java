/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.Notification;

import com.viettel.brcd.ws.model.output.DataNotification;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "sendMulticastMessage")
@XmlAccessorType(XmlAccessType.FIELD)
public class SendMulticastMessage {

    private DataNotification data;

    /**
     * @return the data
     */
    public DataNotification getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(DataNotification data) {
        this.data = data;
    }
}
