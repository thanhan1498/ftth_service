/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.accountinfras;

import com.viettel.brcd.ws.bccsgw.model.Param;
import com.viettel.brcd.ws.common.WebServiceClient;
import com.viettel.brcd.ws.model.output.AccountInfrasLocationOut;
import com.viettel.brcd.ws.model.output.getListODFIndoorByStationCodeOut;
import com.viettel.brcd.ws.model.output.getSubscriptionInfoADSLOutPut;
import com.viettel.brcd.ws.model.output.getSubscriptionInfoAONOutPut;
import com.viettel.brcd.ws.model.output.getSubscriptionInfoGponOutPut;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nhandv
 */
public class NimWsGetInfoBusines extends WebServiceClient {

    public static getSubscriptionInfoGponOutPut getSubscriptionInfoGpon(String accountIsdn) throws Exception {
        List<Param> lstParam = new ArrayList<Param>();
        Param prName = new Param();
        prName.setName("accountIsdn");
        prName.setValue(accountIsdn);
        lstParam.add(prName);
        return new NimWsGetInfoBusines().sendReqgetSubscriptionInfoGpon(lstParam);
    }

    public getSubscriptionInfoGponOutPut sendReqgetSubscriptionInfoGpon(List<Param> lstParam) throws Exception {
        return (getSubscriptionInfoGponOutPut) sendRequestViaBccsGW_Param("getSubscriptionInfo", getSubscriptionInfoGponOutPut.class, lstParam);
    }

    public static getSubscriptionInfoAONOutPut getSubscriptionInfoAON(String accountIsdn) throws Exception {
        List<Param> lstParam = new ArrayList<Param>();
        Param prName = new Param();
        prName.setName("accountIsdn");
        prName.setValue(accountIsdn);
        lstParam.add(prName);
        return new NimWsGetInfoBusines().sendReqgetSubscriptionInfoAON(lstParam);
    }

    public getSubscriptionInfoAONOutPut sendReqgetSubscriptionInfoAON(List<Param> lstParam) throws Exception {
        return (getSubscriptionInfoAONOutPut) sendRequestViaBccsGW_Param("getSubscriptionInfo", getSubscriptionInfoAONOutPut.class, lstParam);
    }

    public static getSubscriptionInfoADSLOutPut getSubscriptionInfoADSL(String accountIsdn) throws Exception {
        List<Param> lstParam = new ArrayList<Param>();
        Param prName = new Param();
        prName.setName("accountIsdn");
        prName.setValue(accountIsdn);
        lstParam.add(prName);
        return new NimWsGetInfoBusines().sendReqgetSubscriptionInfoADSL(lstParam);
    }

    public getSubscriptionInfoADSLOutPut sendReqgetSubscriptionInfoADSL(List<Param> lstParam) throws Exception {
        return (getSubscriptionInfoADSLOutPut) sendRequestViaBccsGW_Param("getSubscriptionInfo", getSubscriptionInfoADSLOutPut.class, lstParam);
    }

    public static AccountInfrasLocationOut getStationAccountInfras(String stationCode) throws Exception {
        List<Param> lstParam = new ArrayList<Param>();
        Param prName = new Param();
        prName.setName("stationCode");
        prName.setValue(stationCode);
        lstParam.add(prName);
        return new NimWsGetInfoBusines().sendReqgetStation(lstParam);
    }

    public AccountInfrasLocationOut sendReqgetStation(List<Param> lstParam) throws Exception {
        return (AccountInfrasLocationOut) sendRequestViaBccsGW_Param("getStation", AccountInfrasLocationOut.class, lstParam);
    }

    public static getListODFIndoorByStationCodeOut getListODFIndoorByStationCode(String stationCode) throws Exception {
        List<Param> lstParam = new ArrayList<Param>();
        Param prName = new Param();
        prName.setName("stationCode");
        prName.setValue(stationCode);
        lstParam.add(prName);
        return new NimWsGetInfoBusines().sendReqgetListODFIndoorByStationCode(lstParam);
    }

    public getListODFIndoorByStationCodeOut sendReqgetListODFIndoorByStationCode(List<Param> lstParam) throws Exception {
        return (getListODFIndoorByStationCodeOut) sendRequestViaBccsGW_Param("getListODFIndoorByStationCode", getListODFIndoorByStationCodeOut.class, lstParam);
    }

    public static void main(String[] args) {
        try {
            getListODFIndoorByStationCodeOut oDFIndoorByStationCodeOut = getListODFIndoorByStationCode("KAN040");
            if (oDFIndoorByStationCodeOut != null) {
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
