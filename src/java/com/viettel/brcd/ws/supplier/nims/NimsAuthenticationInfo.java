/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims;

import java.util.ResourceBundle;

/**
 *
 * @author TuLA4
 */
public class NimsAuthenticationInfo {

    private static ResourceBundle resource = ResourceBundle.getBundle("provisioning");

    public static String getUserName() {
        return resource.getString("nims_ws_username");
    }

    public static String getPassword() {
        return resource.getString("nims_ws_password");
    }
}
