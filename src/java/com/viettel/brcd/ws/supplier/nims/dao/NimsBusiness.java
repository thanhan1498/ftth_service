package com.viettel.brcd.ws.supplier.nims.dao;

import com.viettel.brcd.util.Validator;
import com.viettel.bccs.cm.util.Constants;

/**
 *
 * @author TuLA4
 */
public class NimsBusiness {

    public static String NOK_ACCOUNT_NOT_EXIST = "NOK_ACCOUNT_NOT_EXIST";
    public static String OK = "OK";

    /**
     * ham lay thong tin ha tang khi khao sat online dau noi thue bao moi
     *
     * @param lat
     * @param lng
     * @param serviceAlias
     * @param radius
     * @return
     * @throws Exception
     */
    public static String getServiceTypeFromServiceAliasAndLineType(String hostServiceAlias,
            String serviceAlias, String lineType) {
        return getServiceTypeFromServiceAliasAndLineType(hostServiceAlias, serviceAlias, lineType, false);
    }

    public static String getServiceTypeFromServiceAliasAndLineType(String hostServiceAlias,
            String serviceAlias, String lineType, boolean isGpon) {

        if (Constants.SERVICE_ALIAS_ADSL.equals(serviceAlias)) {
            if (Validator.isNull(lineType)) {
                return Constants.SERVICE_ALIAS_ADSL;
            }
            return lineType;
        }

        if (Constants.SERVICE_ALIAS_PSTN.equals(serviceAlias)) {
            if (Validator.isNull(lineType)) {
                return Constants.SERVICE_ALIAS_PSTN;
            }
            return lineType;
        }

//        if (Constants.LINE_TYPE_A_AND_P.equals(serviceAlias)) {
//            return serviceAlias;
//        }
//
//        if (Constants.SERVICE_ALIAS_CABLE_TV.equals(serviceAlias)) {
//            if (isGpon) {
//                return Constants.INFRA_CABLE_TV;
//            } else {
//                if (hostServiceAlias != null) {
//                    return null;
//                }
//                return Constants.INFRA_CABLE_TV;
//            }
//        }

        if (Constants.SERVICE_ALIAS_FTTH.equals(serviceAlias)) {
            return Constants.SERVICE_ALIAS_FTTH;
        }

        if (Constants.SERVICE_ALIAS_LEASEDLINE.equals(serviceAlias)) {
            return Constants.SERVICE_ALIAS_LEASEDLINE;
        }

//        if (Constants.SERVICE_ALIAS_IPTV.equals(serviceAlias) || Constants.SERVICE_ALIAS_NGN.equals(serviceAlias)
//                || Constants.SERVICE_ALIAS_INTERNET_EOC.equals(serviceAlias)) {
//            if (isGpon) {
//                return serviceAlias;
//            }
//            return null;
//        }
//        if (Constants.SERVICE_ALIAS_MULTI_SCREEN.equals(serviceAlias)) {
//            return serviceAlias;
//        }

        return null;
    }

    /**
     * @Techno @param value
     * @return
     */
    public static String convertInfraType(String value) {
        if (Validator.isNull(value)) {
            return null;
        }
        if (Constants.IN_FRATYPE_CCN_CM.equals(value)) {
//            return Constants.IN_FRATYPE_CCN;
            return null;
        }
        if (Constants.IN_FRATYPE_CATV_CM.equals(value)) {
//            return Constants.IN_FRATYPE_CATV;
            return null;
        }
        if (Constants.IN_FRATYPE_FCN_CM.equals(value)) {
//            return Constants.IN_FRATYPE_FCN;
            return null;
        }
        if (Constants.IN_FRATYPE_GPON_CM.equals(value)) {
            return Constants.IN_FRATYPE_GPON;
        }
        return "";
    }
}
