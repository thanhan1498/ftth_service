
package com.viettel.brcd.ws.supplier.nims.getInfoInfras;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getListBox complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getListBox">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hasCabinet" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="cabinetId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="stationId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement(name = "getListBox")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getListBox", propOrder = {
    "hasCabinet",
    "cabinetId",
    "stationId"
})
public class GetListBox {

    protected Long hasCabinet;
    protected Long cabinetId;
    protected Long stationId;

    /**
     * Gets the value of the hasCabinet property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getHasCabinet() {
        return hasCabinet;
    }

    /**
     * Sets the value of the hasCabinet property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHasCabinet(Long value) {
        this.hasCabinet = value;
    }

    /**
     * Gets the value of the cabinetId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCabinetId() {
        return cabinetId;
    }

    /**
     * Sets the value of the cabinetId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCabinetId(Long value) {
        this.cabinetId = value;
    }

    /**
     * Gets the value of the stationId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getStationId() {
        return stationId;
    }

    /**
     * Sets the value of the stationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setStationId(Long value) {
        this.stationId = value;
    }

}
