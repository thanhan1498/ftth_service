
package com.viettel.brcd.ws.supplier.nims.getInfoInfras;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getListDeviceByStation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getListDeviceByStation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="stationId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="networkClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement(name = "getListDeviceByStation")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getListDeviceByStation", propOrder = {
    "stationId",
    "networkClass"
})
public class GetListDeviceByStation {

    protected Long stationId;
    protected String networkClass;

    /**
     * Gets the value of the stationId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getStationId() {
        return stationId;
    }

    /**
     * Sets the value of the stationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setStationId(Long value) {
        this.stationId = value;
    }

    /**
     * Gets the value of the networkClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClass() {
        return networkClass;
    }

    /**
     * Sets the value of the networkClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClass(String value) {
        this.networkClass = value;
    }

}
