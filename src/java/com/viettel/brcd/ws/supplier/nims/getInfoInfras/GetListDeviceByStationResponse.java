
package com.viettel.brcd.ws.supplier.nims.getInfoInfras;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getListDeviceByStationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getListDeviceByStationResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://webservice.infra.nims.viettel.com/}resultBasicForm" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement(name = "getListDeviceByStationResponse", namespace="http://webservice.infra.nims.viettel.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getListDeviceByStationResponse", propOrder = {
    "_return"
})
public class GetListDeviceByStationResponse {

    @XmlElement(name = "return", nillable = true)
    protected List<ResultBasicForm> _return;

    /**
     * Gets the value of the return property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the return property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReturn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResultBasicForm }
     * 
     * 
     */
    public List<ResultBasicForm> getReturn() {
        if (_return == null) {
            _return = new ArrayList<ResultBasicForm>();
        }
        return this._return;
    }

}
