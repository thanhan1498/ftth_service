/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.getInfoInfras;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "getNodeTBBySplitterCodeResponse", namespace = "http://webservice.infra.nims.viettel.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getNodeTBBySplitterCodeResponse", propOrder = {
    "_return"
})
public class GetSnFromSplReponse {

    @XmlElement(name = "return", nillable = true)
    private Long _return;

    /**
     * @return the _return
     */
    public Long getReturn() {
        return _return;
    }

    /**
     * @param _return the _return to set
     */
    public void setReturn(Long _return) {
        this._return = _return;
    }
}
