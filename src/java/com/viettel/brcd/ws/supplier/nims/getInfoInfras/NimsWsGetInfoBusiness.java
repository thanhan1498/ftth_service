/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.getInfoInfras;

import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.brcd.ws.common.WebServiceClient;

/**
 *
 * @author Nguyen Van Dung
 */
public class NimsWsGetInfoBusiness extends WebServiceClient {

    /**
     * Gui tham so Tu cap toi BCCS GW
     */
    public GetListCabinetByStationIdResponse sendReqCabinetToBCCSGW(GetListCabinetByStationId getListCabinetByStationId) throws Exception {

        String xmlText = CommonWebservice.marshal(getListCabinetByStationId).replaceAll("getListCabinetByStationId", "web:getListCabinetByStationId");
        return (GetListCabinetByStationIdResponse) sendRequestViaBccsGW(xmlText, "getListCabinetByStationId", GetListCabinetByStationIdResponse.class);
    }

    /**
     * Lay danh sach Tu cap
     */
    public static GetListCabinetByStationIdResponse getListCabinetByStationIdResponse(Long stationId) throws Exception {

        GetListCabinetByStationId getListCabinet = new GetListCabinetByStationId();
        getListCabinet.setStationId(stationId);
        return new NimsWsGetInfoBusiness().sendReqCabinetToBCCSGW(getListCabinet);
    }
}
