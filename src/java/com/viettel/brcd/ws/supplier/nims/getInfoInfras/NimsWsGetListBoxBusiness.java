/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.getInfoInfras;

import java.util.List;

/**
 *
 * @author Nguyen Van Dung
 */
public class NimsWsGetListBoxBusiness {
     public static GetListBoxResponse getListBoxResponse(Long hasCabinet, 
             Long cabinetId,Long stationId) throws Exception 
     {

        GetListBox getListBox = new GetListBox();
        getListBox.setHasCabinet(hasCabinet);
        getListBox.setCabinetId(cabinetId);
        getListBox.setStationId(stationId);        
        return new NimsWsGetListBoxDataSupplier().getListBoxResponse(getListBox);
    }
    
    public static void main(String[] args) {
        try {

            System.out.println("Hello World!"); // Display the string.              
            GetListBoxResponse gldbsr = getListBoxResponse(1L, 364759L,62824L);
            List<ResultBasicForm> resultForm = gldbsr.getReturn();
            if(resultForm!=null && resultForm.size()>0){
                for(ResultBasicForm basicForm : resultForm){
                
                    System.out.println("Id: "+ basicForm.getId());                    
                    System.out.println("code: "+ basicForm.getCode());
                }
            
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
}
