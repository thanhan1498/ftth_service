/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.getInfoInfras;

import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.brcd.ws.common.WebServiceClient;

/**
 *
 * @author Nguyen Van Dung
 */
public class NimsWsGetListBoxDataSupplier extends WebServiceClient {

    public GetListBoxResponse getListBoxResponse(GetListBox getListBox) throws Exception {

        String xmlText = CommonWebservice.marshal(getListBox).replaceAll("getListBox", "web:getListBox");
        return (GetListBoxResponse) sendRequestViaBccsGW(xmlText, "getListBox", GetListBoxResponse.class);
    }
}
