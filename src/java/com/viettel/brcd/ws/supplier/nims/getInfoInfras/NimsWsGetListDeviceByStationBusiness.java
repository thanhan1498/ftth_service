/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.getInfoInfras;

import java.util.List;

/**
 *
 * @author Nguyen Van Dung
 */
public class NimsWsGetListDeviceByStationBusiness {
    
     /**
      * 
      * @param stationId
      * @param networkClass
      * @return
      * @throws Exception 
      */
    
    public static GetListDeviceByStationResponse getListDeviceByStationId(Long stationId, String networkClass
         ) throws Exception {

        GetListDeviceByStation deviceByStation = new GetListDeviceByStation();
        deviceByStation.setStationId(stationId);
        deviceByStation.setNetworkClass(networkClass);      
        return new NimsWsGetListDeviceByStationDataSupplier().getListDeviceByStation(deviceByStation);
    }
    
    public static void main(String[] args) {
        try {

            System.out.println("Hello World!"); // Display the string.
            GetListDeviceByStationResponse gldbsr = new GetListDeviceByStationResponse();
            gldbsr = getListDeviceByStationId(62824L, "DSLAM");
            List<ResultBasicForm> resultForm = gldbsr.getReturn();
            if(resultForm!=null && resultForm.size()>0){
                for(ResultBasicForm basicForm : resultForm){
                
                    System.out.println("Message: "+ basicForm.getId());                    
                    System.out.println("code: "+ basicForm.getCode());
                }
            
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
}
