/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.getInfoInfras;

import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.brcd.ws.common.WebServiceClient;

/**
 *
 * @author Nguyen Van Dung
 */
public class NimsWsGetListDeviceByStationDataSupplier extends WebServiceClient {

    public GetListDeviceByStationResponse getListDeviceByStation(GetListDeviceByStation getListDeviceByStation) throws Exception {

        String xmlText = CommonWebservice.marshal(getListDeviceByStation).replaceAll("getListDeviceByStation", "web:getListDeviceByStation");
        return (GetListDeviceByStationResponse) sendRequestViaBccsGW(xmlText, "getListDeviceByStation", GetListDeviceByStationResponse.class);
    }
}
