/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.getInfoInfras;

import java.util.List;

/**
 *
 * @author Nguyen Van Dung
 */
public class NimsWsGetListPortByDeviceIdBusiness {
    
     public static GetListPortByDeviceIdResponse getListPortByDeviceIdResponse (Long deviceId, String account,String serviceType
         ) throws Exception {

        GetListPortByDeviceId byDeviceId = new GetListPortByDeviceId();
        byDeviceId.setDeviceId(deviceId);
        byDeviceId.setAccount(account);
        byDeviceId.setServiceType(serviceType);
        return new NimsWsGetListPortByDeviceIdDataSupplier().getListPortByDeviceId(byDeviceId);
    }
    
    public static void main(String[] args) {
        try {

            System.out.println("Hello World!"); // Display the string.            
            GetListPortByDeviceIdResponse gldbsr = getListPortByDeviceIdResponse(299833L, null, "A");
            List<ResultBasicForm> resultForm = gldbsr.getReturn();
            if(resultForm!=null && resultForm.size()>0){
                for(ResultBasicForm basicForm : resultForm){
                
                    System.out.println("ID: "+ basicForm.getId());                    
                    System.out.println("code: "+ basicForm.getCode());
                    System.out.println("Message: "+ basicForm.getMessage());
                    System.out.println("NetworkClass: "+ basicForm.getNetworkClass());
                }
            
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
}
