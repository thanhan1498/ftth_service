/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.getInfoInfras;

import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.brcd.ws.common.WebServiceClient;

/**
 *
 * @author Nguyen Van Dung
 */
public class NimsWsGetListPortByDeviceIdDataSupplier extends WebServiceClient {

    public GetListPortByDeviceIdResponse getListPortByDeviceId(GetListPortByDeviceId getListPortByDeviceId) throws Exception {

        String xmlText = CommonWebservice.marshal(getListPortByDeviceId).replaceAll("getListPortByDeviceId", "web:getListPortByDeviceId");
        return (GetListPortByDeviceIdResponse) sendRequestViaBccsGW(xmlText, "getListPortByDeviceId", GetListPortByDeviceIdResponse.class);
    }
}
