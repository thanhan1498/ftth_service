/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.getInfoInfras;

import com.viettel.brcd.ws.bccsgw.model.Input;
import com.viettel.brcd.ws.bccsgw.model.Param;
import com.viettel.brcd.ws.common.WebServiceClient;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cuongdm
 */
public class NimsWsGetOdfCouplerList extends WebServiceClient {

    public OdfCouplerListResponse getOdfCouplerList(Long odfId) throws Exception {
        Input input = new Input();
        List<Param> lstParam = new ArrayList<Param>();
        lstParam.add(new Param("odfId", odfId.toString()));
        input.setParam(lstParam);
        return (OdfCouplerListResponse) sendRequestViaBccsGW(input, "getOdfCouplerList", OdfCouplerListResponse.class);
    }
}
