/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.getInfoInfras;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "getOdfCouplerListResponse", namespace = "http://webservice.infra.nims.viettel.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getOdfCouplerListResponse", propOrder = {
    "_return"
})
public class OdfCouplerListResponse {

    @XmlElement(name = "return", nillable = true)
    private List<String> _return;

    /**
     * @return the _return
     */
    public List<String> getReturn() {
        return _return;
    }

    /**
     * @param _return the _return to set
     */
    public void setReturn(List<String> _return) {
        this._return = _return;
    }
}
