
package com.viettel.brcd.ws.supplier.nims.lockinfras;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for lockAndUnlockInfras complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="lockAndUnlockInfras">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="form" type="{http://webservice.infra.nims.viettel.com/}lockAndUnlockInfrasForm" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement(name = "lockAndUnlockInfras")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "lockAndUnlockInfras", propOrder = {
    "user",
    "pass",
    "local",
    "form"
})
public class LockAndUnlockInfras {

    protected String user;
    protected String pass;
    protected String local;
    protected LockAndUnlockInfrasForm form;

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }
    /**
     * Gets the value of the user property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUser() {
        return user;
    }

    /**
     * Sets the value of the user property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUser(String value) {
        this.user = value;
    }

    /**
     * Gets the value of the pass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPass() {
        return pass;
    }

    /**
     * Sets the value of the pass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPass(String value) {
        this.pass = value;
    }

    /**
     * Gets the value of the form property.
     * 
     * @return
     *     possible object is
     *     {@link LockAndUnlockInfrasForm }
     *     
     */
    public LockAndUnlockInfrasForm getForm() {
        return form;
    }

    /**
     * Sets the value of the form property.
     * 
     * @param value
     *     allowed object is
     *     {@link LockAndUnlockInfrasForm }
     *     
     */
    public void setForm(LockAndUnlockInfrasForm value) {
        this.form = value;
    }

}
