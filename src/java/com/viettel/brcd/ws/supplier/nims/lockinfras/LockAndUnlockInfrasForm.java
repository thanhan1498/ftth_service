package com.viettel.brcd.ws.supplier.nims.lockinfras;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for lockAndUnlockInfrasForm complex type.
 *
 * <p>The following schema fragment specifies the expected content contained
 * within this class.
 *
 * <pre>
 * &lt;complexType name="lockAndUnlockInfrasForm">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="account" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="action" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="connectorCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="connectorId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="infraType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isChangeAddress" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="isdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="portCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="portId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="requestId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "lockAndUnlockInfrasForm", propOrder = {
    "account",
    "action",
    "connectorCode",
    "connectorId",
    "infraType",
    "isChangeAddress",
    "isdn",
    "portCode",
    "portId",
    "requestId",
    "serviceType",
    "type",
    "msType",
    "numberCoupler"
})
public class LockAndUnlockInfrasForm {

    protected String account;
    protected Long action;
    protected String connectorCode;
    protected Long connectorId;
    protected String infraType;
    protected Long isChangeAddress;
    protected String isdn;
    protected String portCode;
    protected Long portId;
    protected String requestId;
    protected String serviceType;
    protected String type;
    protected String msType;
    protected String numberCoupler;

    public String getNumberCoupler() {
        return numberCoupler;
    }

    public void setNumberCoupler(String numberCoupler) {
        this.numberCoupler = numberCoupler;
    }

    public String getMsType() {
        return msType;
    }

    public void setMsType(String msType) {
        this.msType = msType;
    }

    /**
     * Gets the value of the account property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getAccount() {
        return account;
    }

    /**
     * Sets the value of the account property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setAccount(String value) {
        this.account = value;
    }

    /**
     * Gets the value of the action property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setAction(Long value) {
        this.action = value;
    }

    /**
     * Gets the value of the connectorCode property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getConnectorCode() {
        return connectorCode;
    }

    /**
     * Sets the value of the connectorCode property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setConnectorCode(String value) {
        this.connectorCode = value;
    }

    /**
     * Gets the value of the connectorId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getConnectorId() {
        return connectorId;
    }

    /**
     * Sets the value of the connectorId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setConnectorId(Long value) {
        this.connectorId = value;
    }

    /**
     * Gets the value of the infraType property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getInfraType() {
        return infraType;
    }

    /**
     * Sets the value of the infraType property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setInfraType(String value) {
        this.infraType = value;
    }

    /**
     * Gets the value of the isChangeAddress property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getIsChangeAddress() {
        return isChangeAddress;
    }

    /**
     * Sets the value of the isChangeAddress property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setIsChangeAddress(Long value) {
        this.isChangeAddress = value;
    }

    /**
     * Gets the value of the isdn property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIsdn() {
        return isdn;
    }

    /**
     * Sets the value of the isdn property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setIsdn(String value) {
        this.isdn = value;
    }

    /**
     * Gets the value of the portCode property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getPortCode() {
        return portCode;
    }

    /**
     * Sets the value of the portCode property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setPortCode(String value) {
        this.portCode = value;
    }

    /**
     * Gets the value of the portId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getPortId() {
        return portId;
    }

    /**
     * Sets the value of the portId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setPortId(Long value) {
        this.portId = value;
    }

    /**
     * Gets the value of the requestId property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets the value of the requestId property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }

    /**
     * Gets the value of the serviceType property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * Sets the value of the serviceType property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setServiceType(String value) {
        this.serviceType = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setType(String value) {
        this.type = value;
    }
}
