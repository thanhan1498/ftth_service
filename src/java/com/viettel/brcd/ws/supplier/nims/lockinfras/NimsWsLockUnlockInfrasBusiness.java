/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.lockinfras;

import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.bccs.cm.model.SubReqDeployment;
import com.viettel.bccs.cm.model.SubRequest;
import com.viettel.brcd.ws.supplier.nims.dao.NimsBusiness;
import com.viettel.brcd.util.Validator;
import com.viettel.bccs.cm.util.Constants;
import com.viettel.brcd.ws.supplier.nims.NimsAuthenticationInfo;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author tula4
 */
public class NimsWsLockUnlockInfrasBusiness {

    public static Long INFRAS_ACTION_LOCK = 1L;
    public static Long INFRAS_ACTION_UNLOCK = 0L;
    public static String INFRAS_LOCL_UNLOCK_OK = "OK";
    public static String INFRAS_LOCL_UNLOCK_NOK = "NOK";
    public static String NOK_ACCOUNT_NOT_LOCK = "NOK_ACCOUNT_NOT_LOCK";
    private static final Logger log = Logger.getLogger(NimsWsLockUnlockInfrasBusiness.class);

    /**
     * @param subRequest
     * @param subReqDeployment
     * @param action
     * @return
     * @throws Exception
     */
    public static LockAndUnlockInfrasResponse lockUnlockInfras(SubRequest subRequest, Long action,
            Session cmPosSession) throws Exception {
        new BaseDAO().info(log, " begin lockUnlockInfras : 3sub_id = " + subRequest.getSubReqId());
        SubReqDeployment subReqDeployment = new SubReqDeployment();
//        if (subReqDeployment == null) {
//            subReqDeployment = SubReqDeploymentSupplier.findBySubReqId(subRequest.getSubReqId(), cmPosSession);
//            log.info(" lockUnlockInfras : 1sub_id = " + subRequest.getSubReqId());
//            subRequest.setSubReqDeployment(subReqDeployment);
//        }
        String lineType = subRequest.getLineType();
        boolean isGpon = true;
        String serviceType = NimsBusiness.getServiceTypeFromServiceAliasAndLineType(null, subRequest.getServiceType(), lineType, isGpon);
        new BaseDAO().info(log, " lockUnlockInfras : 2sub_id = " + subRequest.getSubReqId() + ": " + subRequest.getServiceType() + ": " + lineType);
        if (Validator.isNull(serviceType)) {
            return null;
        }
        String pstn = null;

        if (Constants.SERVICE_ALIAS_ADSL.equals(subRequest.getServiceType())) {
//            SubRequest parentSubRequest = SubRequestSupplier.getBySubId(subRequest.getParentSubId());
            SubRequest parentSubRequest = new SubRequest();
            new BaseDAO().info(log, " lockUnlockInfras : 3sub_id = " + subRequest.getSubReqId());
            if (parentSubRequest != null) {
                pstn = parentSubRequest.getAccount();
            }
        }
//        String mstType = CommonWebServicePM.getAttributeMstProduct(subRequest.getProductCode(), serviceType);
        String mstType = new String();
        new BaseDAO().info(log, " begin11 lockUnlockInfras : 3sub_id = " + subRequest.getSubReqId());
        return lockUnlockInfras(action, subRequest.getAccount(), pstn, subReqDeployment.getCableBoxId(),
                subReqDeployment.getCableBoxType(), serviceType, subRequest.getTechnology(), mstType, subReqDeployment.getCableBoxCode(), null);
    }

    /**
     *
     * @param action
     * @param account
     * @param cableBoxId
     * @param serviceType
     * @return
     * @throws Exception
     */
    public static LockAndUnlockInfrasResponse lockUnlockInfras(Long action, String account,
            String isdn, Long cableBoxId, String cableBoxType,
            String serviceType, String technology, String cableBoxCode, String portCode) throws Exception {
        return lockUnlockInfras(action, account, isdn, cableBoxId, cableBoxType, serviceType, technology, null, cableBoxCode, portCode);
    }

    public static LockAndUnlockInfrasResponse lockUnlockInfras(Long action, String account,
            String isdn, Long cableBoxId, String cableBoxType,
            String serviceType, String technology, String mstType, String cableBoxCode, String portCode) throws Exception {

        LockAndUnlockInfras lockAndUnlockInfras = new LockAndUnlockInfras();
        LockAndUnlockInfrasForm lockAndUnlockInfrasForm = new LockAndUnlockInfrasForm();
        lockAndUnlockInfrasForm.setConnectorCode(cableBoxCode);
        lockAndUnlockInfrasForm.setPortCode(portCode);
        lockAndUnlockInfrasForm.setAccount(account);
        lockAndUnlockInfrasForm.setAction(action);
        lockAndUnlockInfrasForm.setIsdn(isdn);
        lockAndUnlockInfrasForm.setConnectorId(cableBoxId);
        lockAndUnlockInfrasForm.setType(cableBoxType);
        lockAndUnlockInfrasForm.setServiceType(serviceType);
        String infraType = NimsBusiness.convertInfraType(technology);
        lockAndUnlockInfrasForm.setInfraType(infraType);
        lockAndUnlockInfrasForm.setMsType(mstType);
        lockAndUnlockInfras.setForm(lockAndUnlockInfrasForm);

        lockAndUnlockInfras.setUser(NimsAuthenticationInfo.getUserName());
        lockAndUnlockInfras.setPass(NimsAuthenticationInfo.getPassword());
        lockAndUnlockInfras.setLocal("en_US");

        return new NimsWsLockUnlockInfrasDataSupplier().lockAndUnlockInfras(lockAndUnlockInfras);
    }

    /**
     * huynq36
     *
     * @param serviceTypeCM
     * @return
     * @throws Exception
     */
//    public static String getServiceTypeNims(String serviceTypeCM) throws Exception {
//        if (Constant.SERVICE_ALIAS_CABLE_TV.equals(serviceTypeCM)) {
//            return Constant.INFRA_CABLE_TV;
//        } else {
//            return serviceTypeCM;
//        }
//    }
    public static void main(String[] args) {
        try {

            System.out.println("Hello World!"); // Display the string.
            LockAndUnlockInfrasResponse infrasResponse = new LockAndUnlockInfrasResponse();
//            infrasResponse = lockUnlockInfras(1L, "986302577", "986302577", 364763L, "A", "A", "1", "1");
            ResultForm resultForm = infrasResponse.getReturn();
            System.out.println("Message: " + resultForm.getMessage());
            System.out.println("Result: " + resultForm.getResult());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
