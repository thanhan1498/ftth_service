/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.lockinfras;

import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.brcd.ws.common.WebServiceClient;

/**
 *
 * @author tula4
 */
public class NimsWsLockUnlockInfrasDataSupplier extends WebServiceClient {

    /**
     * webservice lock ha tang NIMS
     *
     * @param account
     * @param taskType
     * @return
     * @throws Exception
     */
    public LockAndUnlockInfrasResponse lockAndUnlockInfras(LockAndUnlockInfras lockAndUnlockInfras) throws Exception {

        String xmlText = CommonWebservice.marshal(lockAndUnlockInfras).replaceAll("lockAndUnlockInfras", "web:lockAndUnlockInfras");
        return (LockAndUnlockInfrasResponse) sendRequestViaBccsGW(xmlText, "lockAndUnlockInfras", LockAndUnlockInfrasResponse.class);
    }
}
