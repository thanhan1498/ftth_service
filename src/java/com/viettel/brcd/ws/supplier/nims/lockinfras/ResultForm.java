
package com.viettel.brcd.ws.supplier.nims.lockinfras;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for resultForm complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="resultForm">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pstn" type="{http://webservice.infra.nims.viettel.com/}pstnForm" minOccurs="0"/>
 *         &lt;element name="result" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceSetup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resultForm", propOrder = {
    "message",
    "pstn",
    "result",
    "serviceSetup"
})
public class ResultForm {

    protected String message;
    protected PstnForm pstn;
    protected String result;
    protected String serviceSetup;

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the pstn property.
     * 
     * @return
     *     possible object is
     *     {@link PstnForm }
     *     
     */
    public PstnForm getPstn() {
        return pstn;
    }

    /**
     * Sets the value of the pstn property.
     * 
     * @param value
     *     allowed object is
     *     {@link PstnForm }
     *     
     */
    public void setPstn(PstnForm value) {
        this.pstn = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResult(String value) {
        this.result = value;
    }

    /**
     * Gets the value of the serviceSetup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceSetup() {
        return serviceSetup;
    }

    /**
     * Sets the value of the serviceSetup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceSetup(String value) {
        this.serviceSetup = value;
    }

}
