/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.pre;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "changeSim4G")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChangeSim4G {

    private String pin_code;
    private String trans_id;

    public ChangeSim4G() {
    }

    public ChangeSim4G(String pinCode, String transId) {
        pin_code = pinCode;
        trans_id = transId;
    }

    /**
     * @return the pin_code
     */
    public String getPin_code() {
        return pin_code;
    }

    /**
     * @param pin_code the pin_code to set
     */
    public void setPin_code(String pin_code) {
        this.pin_code = pin_code;
    }

    /**
     * @return the trans_id
     */
    public String getTrans_id() {
        return trans_id;
    }

    /**
     * @param trans_id the trans_id to set
     */
    public void setTrans_id(String trans_id) {
        this.trans_id = trans_id;
    }
}
