/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.pre;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "checkSubIsdn")
@XmlAccessorType(XmlAccessType.FIELD)
public class CheckSubIsdn {

    private String serial;
    private String isdn;
    private String dealerIsdn;
    private String token;
    private String puk;
    private String isScan;

    public CheckSubIsdn() {
    }

    public CheckSubIsdn(String serial, String isdn, String dealerIsdn, String token, String puk) {
        this.serial = serial;
        this.isdn = isdn;
        this.dealerIsdn = dealerIsdn;
        this.token = token;
        this.puk = puk;
        if (isdn != null) {
            isScan = "0";
        } else {
            isScan = "1";
        }
    }

    /**
     * @return the serial
     */
    public String getSerial() {
        return serial;
    }

    /**
     * @param serial the serial to set
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     * @return the isdn
     */
    public String getIsdn() {
        return isdn;
    }

    /**
     * @param isdn the isdn to set
     */
    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    /**
     * @return the dealerIsdn
     */
    public String getDealerIsdn() {
        return dealerIsdn;
    }

    /**
     * @param dealerIsdn the dealerIsdn to set
     */
    public void setDealerIsdn(String dealerIsdn) {
        this.dealerIsdn = dealerIsdn;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the puk
     */
    public String getPuk() {
        return puk;
    }

    /**
     * @param puk the puk to set
     */
    public void setPuk(String puk) {
        this.puk = puk;
    }

    /**
     * @return the isScan
     */
    public String getIsScan() {
        return isScan;
    }

    /**
     * @param isScan the isScan to set
     */
    public void setIsScan(String isScan) {
        this.isScan = isScan;
    }
}
