/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.pre;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "checkSubIsdnResponse", namespace = "http://ws.bccs.viettel.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "checkSubIsdnResponse", propOrder = {
    "_return"
})
public class CheckSubIsdnResponse {

    @XmlElement(name = "return", nillable = true)
    private verifyChangeKitForm _return;

    /**
     * @return the _return
     */
    public verifyChangeKitForm getReturn() {
        return _return;
    }

    /**
     * @param _return the _return to set
     */
    public void setReturn(verifyChangeKitForm _return) {
        this._return = _return;
    }
}
