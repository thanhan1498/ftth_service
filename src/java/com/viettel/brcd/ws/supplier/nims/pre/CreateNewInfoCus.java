/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.pre;

import com.viettel.brcd.ws.model.input.ImageInput;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "createNewInfoCus")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateNewInfoCus {

    private String serial;
    private String isdn;
    private String dealerIsdn;
    private String idType;
    private String fullName;
    private String idNumber;
    private String dob;
    private String token;
    private List<ImageInput> lstImage;

    public CreateNewInfoCus() {
    }

    public CreateNewInfoCus(String token, String serial, String isdn, String dealerIsdn, String idType, String fullName, String idNumber, String dob, List<ImageInput> lstImage) {
        this.token = token;
        this.serial = serial;
        this.isdn = isdn;
        this.dealerIsdn = dealerIsdn;
        this.idType = idType;
        this.fullName = fullName;
        this.idNumber = idNumber;
        this.dob = dob;
        this.lstImage = lstImage;
    }

    /**
     * @return the serial
     */
    public String getSerial() {
        return serial;
    }

    /**
     * @param serial the serial to set
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     * @return the isdn
     */
    public String getIsdn() {
        return isdn;
    }

    /**
     * @param isdn the isdn to set
     */
    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    /**
     * @return the dealerIsdn
     */
    public String getDealerIsdn() {
        return dealerIsdn;
    }

    /**
     * @param dealerIsdn the dealerIsdn to set
     */
    public void setDealerIsdn(String dealerIsdn) {
        this.dealerIsdn = dealerIsdn;
    }

    /**
     * @return the idType
     */
    public String getIdType() {
        return idType;
    }

    /**
     * @param idType the idType to set
     */
    public void setIdType(String idType) {
        this.idType = idType;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the idNumber
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * @param idNumber the idNumber to set
     */
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the lstImage
     */
    public List<ImageInput> getLstImage() {
        return lstImage;
    }

    /**
     * @param lstImage the lstImage to set
     */
    public void setLstImage(List<ImageInput> lstImage) {
        this.lstImage = lstImage;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }
}
