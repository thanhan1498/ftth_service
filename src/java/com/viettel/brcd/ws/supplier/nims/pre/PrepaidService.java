/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.pre;

import com.viettel.bccs.cm.dao.BaseDAO;
import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.brcd.ws.common.WebServiceClient;
import com.viettel.brcd.ws.model.input.ImageInput;
import java.util.List;

/**
 *
 * @author cuongdm
 */
public class PrepaidService extends BaseDAO {

    private static WebServiceClient wsClient = new WebServiceClient();

    public static verifyChangeKitResponse verifyChangeKit(String msisdnAgent, String msisdnCust, String newMsisdnCust, String lastSerial) throws Exception {
        VerifyChangeKit verifyChangeKit = new VerifyChangeKit(msisdnAgent, msisdnCust, newMsisdnCust, lastSerial);
        String xmlText = CommonWebservice.marshal(verifyChangeKit).replaceAll("verifyChangeKit", "com:verifyChangeKit");
        return (verifyChangeKitResponse) wsClient.sendRequestViaBccsGW(xmlText, "verifyChangeKit", verifyChangeKitResponse.class);
    }

    public static VerifyChangeSimResponse verifyChangeSim(String msisdnAgent, String msisdnCust, String lastSerial) throws Exception {
        VerifyChangeSim verifyChangeSim = new VerifyChangeSim(msisdnAgent, msisdnCust, lastSerial);
        String xmlText = CommonWebservice.marshal(verifyChangeSim).replaceAll("verifyChangeSim", "com:verifyChangeSim");
        return (VerifyChangeSimResponse) wsClient.sendRequestViaBccsGW(xmlText, "verifyChangeSim", VerifyChangeSimResponse.class);
    }

    public static ChangeSim4GResponse changeSim4G(String pinCode, String trans_id) throws Exception {
        ChangeSim4G changeSim4G = new ChangeSim4G(pinCode, trans_id);
        String xmlText = CommonWebservice.marshal(changeSim4G).replaceAll("changeSim4G", "com:changeSim4G");
        return (ChangeSim4GResponse) wsClient.sendRequestViaBccsGW(xmlText, "changeSim4G", ChangeSim4GResponse.class);
    }

    public static CreateNewInfoCusResponse createNewInfoCus(String token, String serial, String isdn, String dealerIsdn, String idType, String fullName, String idNumber, String dob, List<ImageInput> lstImage, String puk) throws Exception {
        CheckSubIsdn checkSubIsdn = new CheckSubIsdn(serial, isdn, dealerIsdn, token, puk);
        String xmlText = CommonWebservice.marshal(checkSubIsdn).replaceAll("checkSubIsdn", "ws:checkSubIsdn");
        CheckSubIsdnResponse response = (CheckSubIsdnResponse) wsClient.sendRequestViaBccsGW(xmlText, "checkSubIsdn", CheckSubIsdnResponse.class);

        if (response != null && response.getReturn() != null && "0".equals(response.getReturn().getErrorCode())) {
            CreateNewInfoCus createNewInfoCus = new CreateNewInfoCus(token, serial, isdn, dealerIsdn, idType, fullName, idNumber, dob, lstImage);
            xmlText = CommonWebservice.marshal(createNewInfoCus).replaceAll("createNewInfoCus", "ws:createNewInfoCus");
            return (CreateNewInfoCusResponse) wsClient.sendRequestViaBccsGW(xmlText, "createNewInfoCus", CreateNewInfoCusResponse.class);
        } else {
            CreateNewInfoCusResponse responseError = new CreateNewInfoCusResponse();
            responseError.setReturn(response != null ? response.getReturn() : new verifyChangeKitForm());
            responseError.getReturn().setErrorDecription(responseError.getReturn().getErrorDecription() == null ? responseError.getReturn().getMessageCode() : responseError.getReturn().getErrorDecription());
            return responseError;
        }
    }
}
