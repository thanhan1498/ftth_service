/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.pre;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "verifyChangeKit")
@XmlAccessorType(XmlAccessType.FIELD)
public class VerifyChangeKit {

    private String msisdn_agent;
    private String msisdn_customer;
    private String new_msisdn_customer;
    private String last_serial;

    public VerifyChangeKit() {
    }

    public VerifyChangeKit(String msisdnAgent, String msisdnCust, String newMsisdnCust, String lastSerial) {
        this.msisdn_agent = msisdnAgent;
        this.msisdn_customer = msisdnCust;
        this.new_msisdn_customer = newMsisdnCust;
        this.last_serial = lastSerial;
    }

    /**
     * @return the msisdn_agent
     */
    public String getMsisdn_agent() {
        return msisdn_agent;
    }

    /**
     * @param msisdn_agent the msisdn_agent to set
     */
    public void setMsisdn_agent(String msisdn_agent) {
        this.msisdn_agent = msisdn_agent;
    }

    /**
     * @return the msisdn_customer
     */
    public String getMsisdn_customer() {
        return msisdn_customer;
    }

    /**
     * @param msisdn_customer the msisdn_customer to set
     */
    public void setMsisdn_customer(String msisdn_customer) {
        this.msisdn_customer = msisdn_customer;
    }

    /**
     * @return the new_msisdn_customer
     */
    public String getNew_msisdn_customer() {
        return new_msisdn_customer;
    }

    /**
     * @param new_msisdn_customer the new_msisdn_customer to set
     */
    public void setNew_msisdn_customer(String new_msisdn_customer) {
        this.new_msisdn_customer = new_msisdn_customer;
    }

    /**
     * @return the last_serial
     */
    public String getLast_serial() {
        return last_serial;
    }

    /**
     * @param last_serial the last_serial to set
     */
    public void setLast_serial(String last_serial) {
        this.last_serial = last_serial;
    }
}
