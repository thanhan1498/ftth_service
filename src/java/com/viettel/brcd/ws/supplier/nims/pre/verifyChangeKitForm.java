/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.pre;

/**
 *
 * @author cuongdm
 */
public class verifyChangeKitForm {
    private String errorCode;
    private String errorDecription;
    private String description;
    private String tranId;
    private String messageCode;

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the tranId
     */
    public String getTranId() {
        return tranId;
    }

    /**
     * @param tranId the tranId to set
     */
    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    /**
     * @return the errorDecription
     */
    public String getErrorDecription() {
        return errorDecription;
    }

    /**
     * @param errorDecription the errorDecription to set
     */
    public void setErrorDecription(String errorDecription) {
        this.errorDecription = errorDecription;
    }

    /**
     * @return the messageCode
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * @param messageCode the messageCode to set
     */
    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }
}
