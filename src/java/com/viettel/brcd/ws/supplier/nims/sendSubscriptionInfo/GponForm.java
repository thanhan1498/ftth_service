
package com.viettel.brcd.ws.supplier.nims.sendSubscriptionInfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gponForm complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gponForm">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="actualPort" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ampCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ampPort" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deviceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="portLogic" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gponForm", propOrder = {
    "actualPort",
    "ampCode",
    "ampPort",
    "deviceCode",
    "portLogic"
})
public class GponForm {

    protected String actualPort;
    protected String ampCode;
    protected String ampPort;
    protected String deviceCode;
    protected Long portLogic;

    /**
     * Gets the value of the actualPort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActualPort() {
        return actualPort;
    }

    /**
     * Sets the value of the actualPort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActualPort(String value) {
        this.actualPort = value;
    }

    /**
     * Gets the value of the ampCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmpCode() {
        return ampCode;
    }

    /**
     * Sets the value of the ampCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmpCode(String value) {
        this.ampCode = value;
    }

    /**
     * Gets the value of the ampPort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmpPort() {
        return ampPort;
    }

    /**
     * Sets the value of the ampPort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmpPort(String value) {
        this.ampPort = value;
    }

    /**
     * Gets the value of the deviceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceCode() {
        return deviceCode;
    }

    /**
     * Sets the value of the deviceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceCode(String value) {
        this.deviceCode = value;
    }

    /**
     * Gets the value of the portLogic property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPortLogic() {
        return portLogic;
    }

    /**
     * Sets the value of the portLogic property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPortLogic(Long value) {
        this.portLogic = value;
    }

}
