
package com.viettel.brcd.ws.supplier.nims.sendSubscriptionInfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ngnForm complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ngnForm">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountPPPoE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ipPrivate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rmId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ngnForm", propOrder = {
    "accountPPPoE",
    "ipPrivate",
    "rmId"
})
public class NgnForm {

    protected String accountPPPoE;
    protected String ipPrivate;
    protected Long rmId;

    /**
     * Gets the value of the accountPPPoE property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountPPPoE() {
        return accountPPPoE;
    }

    /**
     * Sets the value of the accountPPPoE property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountPPPoE(String value) {
        this.accountPPPoE = value;
    }

    /**
     * Gets the value of the ipPrivate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIpPrivate() {
        return ipPrivate;
    }

    /**
     * Sets the value of the ipPrivate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIpPrivate(String value) {
        this.ipPrivate = value;
    }

    /**
     * Gets the value of the rmId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRmId() {
        return rmId;
    }

    /**
     * Sets the value of the rmId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRmId(Long value) {
        this.rmId = value;
    }

}
