/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.nims.sendSubscriptionInfo;

import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.brcd.ws.common.WebServiceClient;
import com.viettel.brcd.ws.supplier.nims.NimsAuthenticationInfo;

/**
 *
 * @author nhandv
 */
public class NimsWsUpdateSubscriptionInfoBusiness extends WebServiceClient {

    /**
     *
     * @param updateSubscriptionInfo
     * @return
     * @throws Exception
     */
    public UpdateSubscriptionInfoResponse updateSubscriptionInfoResponse(UpdateSubscriptionInfo updateSubscriptionInfo) throws Exception {

        String xmlText = CommonWebservice.marshal(updateSubscriptionInfo).replaceAll("updateSubscriptionInfo", "web:updateSubscriptionInfo");
        return (UpdateSubscriptionInfoResponse) sendRequestViaBccsGW(xmlText, "updateSubscriptionInfo", UpdateSubscriptionInfoResponse.class);
    }

    /**
     *
     * @param acceptanceDate
     * @param account
     * @param accountGline
     * @param accountGroup
     * @param accountPPPoE
     * @param boxCode
     * @param boxId
     * @param cableId
     * @param cardIdentity
     * @param connectorDeptCode
     * @param contactMobile
     * @param contractNo
     * @param couplerNo
     * @param departmentId
     * @param deviceCode
     * @param deviceId
     * @param fax
     * @param fullName
     * @param gponSerial
     * @param infraType
     * @param isAccountUplink
     * @param isChangeAddGline
     * @param isTemp
     * @param isTwoStation
     * @param isdn
     * @param lengthCableSub
     * @param lineId
     * @param lineNo
     * @param lineNoA
     * @param lineNoB
     * @param llInterface
     * @param llProtectLevel
     * @param llSpeed
     * @param locationCode
     * @param mdfACode
     * @param mdfAId
     * @param mdfBCode
     * @param mdfBId
     * @param mwMacNum
     * @param mwVlan
     * @param networkClass
     * @param networkType
     * @param noConnector
     * @param nodeOpticalCode
     * @param nodeTbCode
     * @param nodeTbId
     * @param numberOfLine
     * @param odfCode
     * @param odfId
     * @param odfIndoorCode
     * @param odfIndoorId
     * @param originCabinetId
     * @param owIpGateway
     * @param owIpSub
     * @param owLan
     * @param owNetwork
     * @param pointNumber
     * @param portCode
     * @param portId
     * @param portLogic
     * @param portNoMdfA
     * @param portNoMdfB
     * @param portSpliterId
     * @param portTap
     * @param reasonUpdate
     * @param requestId
     * @param rootCabinetCode
     * @param secondCouplerNo
     * @param secondDeviceId
     * @param secondPortId
     * @param secondStationId
     * @param serviceLevel
     * @param setupAddress
     * @param spliterId
     * @param stationAddress
     * @param stationCode
     * @param stationDeptCode
     * @param stationId
     * @param streamId
     * @param subAddress
     * @param subType
     * @param tapCode
     * @param tapId
     * @param type
     * @return
     * @throws Exception
     */
    public static UpdateSubscriptionInfoResponse updateSubscriptionInfoResponse(String acceptanceDate, String account,
            String boxCode, Long boxId, Long cableId, String cardIdentity, String connectorDeptCode, String contactMobile,
            String contractNo, Long couplerNo, Long departmentId, String deviceCode, Long deviceId, String fax, String fullName,
            String infraType, Long isTwoStation, String isdn,
            Long lengthCableSub, Long lineId, Long lineNo, Long lineNoA, Long lineNoB, String llInterface, String llProtectLevel,
            String llSpeed, String locationCode, String mdfACode, Long mdfAId, String mdfBCode, Long mdfBId, String mwMacNum,
            String mwVlan, String networkClass, String networkType, Long noConnector, String nodeOpticalCode,
            String odfCode, Long odfId, String odfIndoorCode, Long odfIndoorId, Long originCabinetId, String owIpGateway,
            String owIpSub, String owLan, String owNetwork, Long pointNumber, String portCode, Long portId, Long portNoMdfA,
            Long portNoMdfB, String portTap, String requestId, String rootCabinetCode, Long secondCouplerNo,
            Long secondDeviceId, Long secondPortId, Long secondStationId, String serviceLevel, String setupAddress, String stationAddress,
            String stationCode, Long stationId, Long streamId, String subAddress, String subType, String tapCode, Long tapId, String type, String location) throws Exception {

        UpdateSubscriptionInfo sendSubscriptionInfo = new UpdateSubscriptionInfo();
        SubscriptionInfoForm infoForm = new SubscriptionInfoForm();
        infoForm.setAcceptanceDate(acceptanceDate);
        infoForm.setAccount(account);

        infoForm.setBoxCode(boxCode);
        infoForm.setBoxId(boxId);
        infoForm.setCableId(cableId);
        infoForm.setCardIdentity(cardIdentity);
        infoForm.setConnectorDeptCode(connectorDeptCode);
        infoForm.setContactMobile(contactMobile);
        infoForm.setContractNo(contractNo);
        infoForm.setCouplerNo(couplerNo);
        infoForm.setDepartmentId(departmentId);
        infoForm.setDeviceCode(deviceCode);
        infoForm.setDeviceId(deviceId);
        infoForm.setFax(fax);
        infoForm.setFullName(fullName);
        infoForm.setInfraType(infraType);

        infoForm.setIsTwoStation(isTwoStation);
        infoForm.setIsdn(isdn);
        infoForm.setLengthCableSub(lengthCableSub);
        infoForm.setLineId(lineId);
        infoForm.setLineNo(lineNo);
        infoForm.setLineNoA(lineNoA);
        infoForm.setLineNoB(lineNoB);
        infoForm.setLlInterface(llInterface);
        infoForm.setLlProtectLevel(llProtectLevel);
        infoForm.setLlSpeed(llSpeed);
        infoForm.setLocationCode(locationCode);
        infoForm.setMdfACode(mdfACode);
        infoForm.setMdfAId(mdfAId);
        infoForm.setMdfBCode(mdfBCode);
        infoForm.setMdfBId(mdfBId);
        infoForm.setMwMacNum(mwMacNum);
        infoForm.setMwVlan(mwVlan);
        infoForm.setNetworkClass(networkClass);
        infoForm.setNetworkType(networkType);
        infoForm.setNoConnector(noConnector);
        infoForm.setNodeOpticalCode(nodeOpticalCode);

        infoForm.setOdfCode(odfCode);
        infoForm.setOdfId(odfId);
        infoForm.setOdfIndoorCode(odfIndoorCode);
        infoForm.setOdfIndoorId(odfIndoorId);
        infoForm.setOriginCabinetId(originCabinetId);
        infoForm.setOwIpGateway(owIpGateway);
        infoForm.setOwIpSub(owIpSub);
        infoForm.setOwLan(owLan);
        infoForm.setPointNumber(pointNumber);
        infoForm.setPortCode(portCode);
        infoForm.setPortId(portId);

        infoForm.setPortNoMdfA(portNoMdfA);
        infoForm.setPortNoMdfB(portNoMdfB);

        infoForm.setPortTap(portTap);

        infoForm.setRequestId(requestId);
        infoForm.setRootCabinetCode(rootCabinetCode);
        infoForm.setSecondCouplerNo(secondCouplerNo);
        infoForm.setSecondDeviceId(secondDeviceId);
        infoForm.setSecondPortId(secondPortId);
        infoForm.setSecondStationId(secondStationId);
        infoForm.setServiceLevel(serviceLevel);
        infoForm.setSetupAddress(setupAddress);

        infoForm.setStationAddress(stationAddress);
        infoForm.setStationCode(stationCode);

        infoForm.setStationId(stationId);
        infoForm.setStreamId(streamId);
        infoForm.setSubAddress(subAddress);
        infoForm.setSubType(subType);
        infoForm.setTapCode(tapCode);
        infoForm.setTapId(tapId);
        infoForm.setType(type);

        sendSubscriptionInfo.setUser(NimsAuthenticationInfo.getUserName());
        sendSubscriptionInfo.setPass(NimsAuthenticationInfo.getUserName());
        sendSubscriptionInfo.setForm(infoForm);

        return new NimsWsUpdateSubscriptionInfoBusiness().updateSubscriptionInfoResponse(sendSubscriptionInfo);
    }

}