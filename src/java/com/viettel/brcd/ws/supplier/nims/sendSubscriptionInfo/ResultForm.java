
package com.viettel.brcd.ws.supplier.nims.sendSubscriptionInfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for resultForm complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="resultForm">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ftth" type="{http://webservice.infra.nims.viettel.com/}ftthForm" minOccurs="0"/>
 *         &lt;element name="gponForm" type="{http://webservice.infra.nims.viettel.com/}gponForm" minOccurs="0"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ngn" type="{http://webservice.infra.nims.viettel.com/}ngnForm" minOccurs="0"/>
 *         &lt;element name="portFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pstn" type="{http://webservice.infra.nims.viettel.com/}pstnForm" minOccurs="0"/>
 *         &lt;element name="requestId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="result" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceSetup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resultForm", propOrder = {
    "ftth",
    "gponForm",
    "message",
    "ngn",
    "portFormat",
    "pstn",
    "requestId",
    "result",
    "serviceSetup"
})
public class ResultForm {

    protected FtthForm ftth;
    protected GponForm gponForm;
    protected String message;
    protected NgnForm ngn;
    protected String portFormat;
    protected PstnForm pstn;
    protected String requestId;
    protected String result;
    protected String serviceSetup;

    /**
     * Gets the value of the ftth property.
     * 
     * @return
     *     possible object is
     *     {@link FtthForm }
     *     
     */
    public FtthForm getFtth() {
        return ftth;
    }

    /**
     * Sets the value of the ftth property.
     * 
     * @param value
     *     allowed object is
     *     {@link FtthForm }
     *     
     */
    public void setFtth(FtthForm value) {
        this.ftth = value;
    }

    /**
     * Gets the value of the gponForm property.
     * 
     * @return
     *     possible object is
     *     {@link GponForm }
     *     
     */
    public GponForm getGponForm() {
        return gponForm;
    }

    /**
     * Sets the value of the gponForm property.
     * 
     * @param value
     *     allowed object is
     *     {@link GponForm }
     *     
     */
    public void setGponForm(GponForm value) {
        this.gponForm = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the ngn property.
     * 
     * @return
     *     possible object is
     *     {@link NgnForm }
     *     
     */
    public NgnForm getNgn() {
        return ngn;
    }

    /**
     * Sets the value of the ngn property.
     * 
     * @param value
     *     allowed object is
     *     {@link NgnForm }
     *     
     */
    public void setNgn(NgnForm value) {
        this.ngn = value;
    }

    /**
     * Gets the value of the portFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortFormat() {
        return portFormat;
    }

    /**
     * Sets the value of the portFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortFormat(String value) {
        this.portFormat = value;
    }

    /**
     * Gets the value of the pstn property.
     * 
     * @return
     *     possible object is
     *     {@link PstnForm }
     *     
     */
    public PstnForm getPstn() {
        return pstn;
    }

    /**
     * Sets the value of the pstn property.
     * 
     * @param value
     *     allowed object is
     *     {@link PstnForm }
     *     
     */
    public void setPstn(PstnForm value) {
        this.pstn = value;
    }

    /**
     * Gets the value of the requestId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets the value of the requestId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResult(String value) {
        this.result = value;
    }

    /**
     * Gets the value of the serviceSetup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceSetup() {
        return serviceSetup;
    }

    /**
     * Sets the value of the serviceSetup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceSetup(String value) {
        this.serviceSetup = value;
    }

}
