package com.viettel.brcd.ws.supplier.nims.sendSubscriptionInfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for subscriptionInfoForm complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="subscriptionInfoForm">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="acceptanceDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="account" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accountGline" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accountGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accountPPPoE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accountUplink" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="boxCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="boxId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="brasAgg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cableId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="cardIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="connectorDeptCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contactMobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contractNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="couplerNo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="departmentId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="deviceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deviceId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="gponSerial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="infraType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="internationalSpeed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isAccountUplink" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="isChangeAddGline" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="isChangePortLogic" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="isTemp" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="isTwoStation" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="isUpLink" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="isdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lengthCableSub" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="lineId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="lineNo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="lineNoA" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="lineNoB" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="llInterface" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="llProtectLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="llSpeed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mdfACode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mdfAId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="mdfBCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mdfBId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="msType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mwMacNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mwVlan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="networkClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="networkType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="noConnector" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="nodeOpticalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="odfCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="odfId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="odfIndoorCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="odfIndoorId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="originCabinetId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="owIpGateway" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="owIpSub" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="owLan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="owNetwork" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pointNumber" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="portCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="portId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="portLogic" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="portNoMdfA" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="portNoMdfB" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="portSpliterId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="portTap" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requestId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rootCabinetCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rtRd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secondCouplerNo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="secondDeviceId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="secondPortId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="secondStationId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="serviceLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="setupAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="stationAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="stationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="stationId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="stbSerial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="streamId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="subAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subscriptionStatus" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="tapCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tapId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="tranmissionDeviceId1" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="tranmissionDeviceId2" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="tranmissionPortId1" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="tranmissionPortId2" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "subscriptionInfoForm", propOrder = {
    "acceptanceDate",
    "account",
    "accountGline",
    "accountGroup",
    "accountPPPoE",
    "accountUplink",
    "boxCode",
    "boxId",
    "brasAgg",
    "cableId",
    "cardIdentity",
    "connectorDeptCode",
    "contactMobile",
    "contractNo",
    "couplerNo",
    "departmentId",
    "deviceCode",
    "deviceId",
    "fax",
    "fullName",
    "gponSerial",
    "infraType",
    "internationalSpeed",
    "isAccountUplink",
    "isChangeAddGline",
    "isChangePortLogic",
    "isTemp",
    "isTwoStation",
    "isUpLink",
    "isdn",
    "lengthCableSub",
    "lineId",
    "lineNo",
    "lineNoA",
    "lineNoB",
    "llInterface",
    "llProtectLevel",
    "llSpeed",
    "locationCode",
    "mdfACode",
    "mdfAId",
    "mdfBCode",
    "mdfBId",
    "msType",
    "mwMacNum",
    "mwVlan",
    "networkClass",
    "networkType",
    "noConnector",
    "nodeOpticalCode",
    "nodeTbCode",
    "odfCode",
    "odfId",
    "odfIndoorCode",
    "odfIndoorId",
    "originCabinetId",
    "owIpGateway",
    "owIpSub",
    "owLan",
    "owNetwork",
    "pointNumber",
    "portCode",
    "portId",
    "portLogic",
    "portNoMdfA",
    "portNoMdfB",
    "portSpliterId",
    "portTap",
    "requestId",
    "rootCabinetCode",
    "rtRd",
    "secondCouplerNo",
    "secondDeviceId",
    "secondPortId",
    "secondStationId",
    "serviceLevel",
    "setupAddress",
    "stationAddress",
    "stationCode",
    "stationId",
    "stbSerial",
    "streamId",
    "subAddress",
    "subType",
    "spliterCode",
    "subscriptionStatus",
    "tapCode",
    "tapId",
    "tranmissionDeviceId1",
    "tranmissionDeviceId2",
    "tranmissionPortId1",
    "tranmissionPortId2",
    "type"
})
public class SubscriptionInfoForm {

    protected String acceptanceDate;
    protected String account;
    protected String accountGline;
    protected String accountGroup;
    protected String accountPPPoE;
    protected String accountUplink;
    protected String boxCode;
    protected Long boxId;
    protected String brasAgg;
    protected Long cableId;
    protected String cardIdentity;
    protected String connectorDeptCode;
    protected String contactMobile;
    protected String contractNo;
    protected Long couplerNo;
    protected Long departmentId;
    protected String deviceCode;
    protected Long deviceId;
    protected String fax;
    protected String fullName;
    protected String gponSerial;
    protected String infraType;
    protected String internationalSpeed;
    protected Long isAccountUplink;
    protected Long isChangeAddGline;
    protected Integer isChangePortLogic;
    protected Long isTemp;
    protected Long isTwoStation;
    protected Long isUpLink;
    protected String isdn;
    protected Long lengthCableSub;
    protected Long lineId;
    protected Long lineNo;
    protected Long lineNoA;
    protected Long lineNoB;
    protected String llInterface;
    protected String llProtectLevel;
    protected String llSpeed;
    protected String locationCode;
    protected String mdfACode;
    protected Long mdfAId;
    protected String mdfBCode;
    protected Long mdfBId;
    protected String msType;
    protected String mwMacNum;
    protected String mwVlan;
    protected String networkClass;
    protected String networkType;
    protected Long noConnector;
    protected String nodeOpticalCode;
    protected String nodeTbCode;
    protected String odfCode;
    protected Long odfId;
    protected String odfIndoorCode;
    protected Long odfIndoorId;
    protected Long originCabinetId;
    protected String owIpGateway;
    protected String owIpSub;
    protected String owLan;
    protected String owNetwork;
    protected Long pointNumber;
    protected String portCode;
    protected Long portId;
    protected Long portLogic;
    protected Long portNoMdfA;
    protected Long portNoMdfB;
    protected Long portSpliterId;
    protected String portTap;
    protected String requestId;
    protected String rootCabinetCode;
    protected String rtRd;
    protected Long secondCouplerNo;
    protected Long secondDeviceId;
    protected Long secondPortId;
    protected Long secondStationId;
    protected String serviceLevel;
    protected String setupAddress;
    protected String stationAddress;
    protected String stationCode;
    protected Long stationId;
    protected String stbSerial;
    protected Long streamId;
    protected String subAddress;
    protected String subType;
    protected Long subscriptionStatus;
    protected String spliterCode;
    protected String tapCode;
    protected Long tapId;
    protected Long tranmissionDeviceId1;
    protected Long tranmissionDeviceId2;
    protected Long tranmissionPortId1;
    protected Long tranmissionPortId2;
    protected String type;

    /**
     * Gets the value of the acceptanceDate property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getAcceptanceDate() {
        return acceptanceDate;
    }

    /**
     * Sets the value of the acceptanceDate property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setAcceptanceDate(String value) {
        this.acceptanceDate = value;
    }

    /**
     * Gets the value of the account property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getAccount() {
        return account;
    }

    /**
     * Sets the value of the account property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setAccount(String value) {
        this.account = value;
    }

    /**
     * Gets the value of the accountGline property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getAccountGline() {
        return accountGline;
    }

    /**
     * Sets the value of the accountGline property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setAccountGline(String value) {
        this.accountGline = value;
    }

    /**
     * Gets the value of the accountGroup property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getAccountGroup() {
        return accountGroup;
    }

    /**
     * Sets the value of the accountGroup property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setAccountGroup(String value) {
        this.accountGroup = value;
    }

    /**
     * Gets the value of the accountPPPoE property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getAccountPPPoE() {
        return accountPPPoE;
    }

    /**
     * Sets the value of the accountPPPoE property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setAccountPPPoE(String value) {
        this.accountPPPoE = value;
    }

    /**
     * Gets the value of the accountUplink property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getAccountUplink() {
        return accountUplink;
    }

    /**
     * Sets the value of the accountUplink property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setAccountUplink(String value) {
        this.accountUplink = value;
    }

    /**
     * Gets the value of the boxCode property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getBoxCode() {
        return boxCode;
    }

    /**
     * Sets the value of the boxCode property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setBoxCode(String value) {
        this.boxCode = value;
    }

    /**
     * Gets the value of the boxId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getBoxId() {
        return boxId;
    }

    /**
     * Sets the value of the boxId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setBoxId(Long value) {
        this.boxId = value;
    }

    /**
     * Gets the value of the brasAgg property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getBrasAgg() {
        return brasAgg;
    }

    /**
     * Sets the value of the brasAgg property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setBrasAgg(String value) {
        this.brasAgg = value;
    }

    /**
     * Gets the value of the cableId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getCableId() {
        return cableId;
    }

    /**
     * Sets the value of the cableId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setCableId(Long value) {
        this.cableId = value;
    }

    /**
     * Gets the value of the cardIdentity property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCardIdentity() {
        return cardIdentity;
    }

    /**
     * Sets the value of the cardIdentity property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setCardIdentity(String value) {
        this.cardIdentity = value;
    }

    /**
     * Gets the value of the connectorDeptCode property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getConnectorDeptCode() {
        return connectorDeptCode;
    }

    /**
     * Sets the value of the connectorDeptCode property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setConnectorDeptCode(String value) {
        this.connectorDeptCode = value;
    }

    /**
     * Gets the value of the contactMobile property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getContactMobile() {
        return contactMobile;
    }

    /**
     * Sets the value of the contactMobile property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setContactMobile(String value) {
        this.contactMobile = value;
    }

    /**
     * Gets the value of the contractNo property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getContractNo() {
        return contractNo;
    }

    /**
     * Sets the value of the contractNo property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setContractNo(String value) {
        this.contractNo = value;
    }

    /**
     * Gets the value of the couplerNo property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getCouplerNo() {
        return couplerNo;
    }

    /**
     * Sets the value of the couplerNo property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setCouplerNo(Long value) {
        this.couplerNo = value;
    }

    /**
     * Gets the value of the departmentId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getDepartmentId() {
        return departmentId;
    }

    /**
     * Sets the value of the departmentId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setDepartmentId(Long value) {
        this.departmentId = value;
    }

    /**
     * Gets the value of the deviceCode property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getDeviceCode() {
        return deviceCode;
    }

    /**
     * Sets the value of the deviceCode property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setDeviceCode(String value) {
        this.deviceCode = value;
    }

    /**
     * Gets the value of the deviceId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getDeviceId() {
        return deviceId;
    }

    /**
     * Sets the value of the deviceId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setDeviceId(Long value) {
        this.deviceId = value;
    }

    /**
     * Gets the value of the fax property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getFax() {
        return fax;
    }

    /**
     * Sets the value of the fax property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setFax(String value) {
        this.fax = value;
    }

    /**
     * Gets the value of the fullName property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setFullName(String value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the gponSerial property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getGponSerial() {
        return gponSerial;
    }

    /**
     * Sets the value of the gponSerial property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setGponSerial(String value) {
        this.gponSerial = value;
    }

    /**
     * Gets the value of the infraType property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getInfraType() {
        return infraType;
    }

    /**
     * Sets the value of the infraType property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setInfraType(String value) {
        this.infraType = value;
    }

    /**
     * Gets the value of the internationalSpeed property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getInternationalSpeed() {
        return internationalSpeed;
    }

    /**
     * Sets the value of the internationalSpeed property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setInternationalSpeed(String value) {
        this.internationalSpeed = value;
    }

    /**
     * Gets the value of the isAccountUplink property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getIsAccountUplink() {
        return isAccountUplink;
    }

    /**
     * Sets the value of the isAccountUplink property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setIsAccountUplink(Long value) {
        this.isAccountUplink = value;
    }

    /**
     * Gets the value of the isChangeAddGline property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getIsChangeAddGline() {
        return isChangeAddGline;
    }

    /**
     * Sets the value of the isChangeAddGline property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setIsChangeAddGline(Long value) {
        this.isChangeAddGline = value;
    }

    /**
     * Gets the value of the isChangePortLogic property.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getIsChangePortLogic() {
        return isChangePortLogic;
    }

    /**
     * Sets the value of the isChangePortLogic property.
     *
     * @param value allowed object is {@link Integer }
     *
     */
    public void setIsChangePortLogic(Integer value) {
        this.isChangePortLogic = value;
    }

    /**
     * Gets the value of the isTemp property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getIsTemp() {
        return isTemp;
    }

    /**
     * Sets the value of the isTemp property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setIsTemp(Long value) {
        this.isTemp = value;
    }

    /**
     * Gets the value of the isTwoStation property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getIsTwoStation() {
        return isTwoStation;
    }

    /**
     * Sets the value of the isTwoStation property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setIsTwoStation(Long value) {
        this.isTwoStation = value;
    }

    /**
     * Gets the value of the isUpLink property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getIsUpLink() {
        return isUpLink;
    }

    /**
     * Sets the value of the isUpLink property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setIsUpLink(Long value) {
        this.isUpLink = value;
    }

    /**
     * Gets the value of the isdn property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIsdn() {
        return isdn;
    }

    /**
     * Sets the value of the isdn property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setIsdn(String value) {
        this.isdn = value;
    }

    /**
     * Gets the value of the lengthCableSub property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getLengthCableSub() {
        return lengthCableSub;
    }

    /**
     * Sets the value of the lengthCableSub property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setLengthCableSub(Long value) {
        this.lengthCableSub = value;
    }

    /**
     * Gets the value of the lineId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getLineId() {
        return lineId;
    }

    /**
     * Sets the value of the lineId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setLineId(Long value) {
        this.lineId = value;
    }

    /**
     * Gets the value of the lineNo property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getLineNo() {
        return lineNo;
    }

    /**
     * Sets the value of the lineNo property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setLineNo(Long value) {
        this.lineNo = value;
    }

    /**
     * Gets the value of the lineNoA property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getLineNoA() {
        return lineNoA;
    }

    /**
     * Sets the value of the lineNoA property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setLineNoA(Long value) {
        this.lineNoA = value;
    }

    /**
     * Gets the value of the lineNoB property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getLineNoB() {
        return lineNoB;
    }

    /**
     * Sets the value of the lineNoB property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setLineNoB(Long value) {
        this.lineNoB = value;
    }

    /**
     * Gets the value of the llInterface property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getLlInterface() {
        return llInterface;
    }

    /**
     * Sets the value of the llInterface property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setLlInterface(String value) {
        this.llInterface = value;
    }

    /**
     * Gets the value of the llProtectLevel property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getLlProtectLevel() {
        return llProtectLevel;
    }

    /**
     * Sets the value of the llProtectLevel property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setLlProtectLevel(String value) {
        this.llProtectLevel = value;
    }

    /**
     * Gets the value of the llSpeed property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getLlSpeed() {
        return llSpeed;
    }

    /**
     * Sets the value of the llSpeed property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setLlSpeed(String value) {
        this.llSpeed = value;
    }

    /**
     * Gets the value of the locationCode property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getLocationCode() {
        return locationCode;
    }

    /**
     * Sets the value of the locationCode property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setLocationCode(String value) {
        this.locationCode = value;
    }

    /**
     * Gets the value of the mdfACode property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getMdfACode() {
        return mdfACode;
    }

    /**
     * Sets the value of the mdfACode property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setMdfACode(String value) {
        this.mdfACode = value;
    }

    /**
     * Gets the value of the mdfAId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getMdfAId() {
        return mdfAId;
    }

    /**
     * Sets the value of the mdfAId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setMdfAId(Long value) {
        this.mdfAId = value;
    }

    /**
     * Gets the value of the mdfBCode property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getMdfBCode() {
        return mdfBCode;
    }

    /**
     * Sets the value of the mdfBCode property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setMdfBCode(String value) {
        this.mdfBCode = value;
    }

    /**
     * Gets the value of the mdfBId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getMdfBId() {
        return mdfBId;
    }

    /**
     * Sets the value of the mdfBId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setMdfBId(Long value) {
        this.mdfBId = value;
    }

    /**
     * Gets the value of the msType property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getMsType() {
        return msType;
    }

    /**
     * Sets the value of the msType property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setMsType(String value) {
        this.msType = value;
    }

    /**
     * Gets the value of the mwMacNum property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getMwMacNum() {
        return mwMacNum;
    }

    /**
     * Sets the value of the mwMacNum property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setMwMacNum(String value) {
        this.mwMacNum = value;
    }

    /**
     * Gets the value of the mwVlan property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getMwVlan() {
        return mwVlan;
    }

    /**
     * Sets the value of the mwVlan property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setMwVlan(String value) {
        this.mwVlan = value;
    }

    /**
     * Gets the value of the networkClass property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNetworkClass() {
        return networkClass;
    }

    /**
     * Sets the value of the networkClass property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setNetworkClass(String value) {
        this.networkClass = value;
    }

    /**
     * Gets the value of the networkType property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNetworkType() {
        return networkType;
    }

    /**
     * Sets the value of the networkType property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setNetworkType(String value) {
        this.networkType = value;
    }

    /**
     * Gets the value of the noConnector property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getNoConnector() {
        return noConnector;
    }

    /**
     * Sets the value of the noConnector property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setNoConnector(Long value) {
        this.noConnector = value;
    }

    /**
     * Gets the value of the nodeOpticalCode property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNodeOpticalCode() {
        return nodeOpticalCode;
    }

    /**
     * Sets the value of the nodeOpticalCode property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setNodeOpticalCode(String value) {
        this.nodeOpticalCode = value;
    }

    /**
     * Gets the value of the odfCode property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getOdfCode() {
        return odfCode;
    }

    /**
     * Sets the value of the odfCode property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setOdfCode(String value) {
        this.odfCode = value;
    }

    /**
     * Gets the value of the odfId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getOdfId() {
        return odfId;
    }

    /**
     * Sets the value of the odfId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setOdfId(Long value) {
        this.odfId = value;
    }

    /**
     * Gets the value of the odfIndoorCode property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getOdfIndoorCode() {
        return odfIndoorCode;
    }

    /**
     * Sets the value of the odfIndoorCode property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setOdfIndoorCode(String value) {
        this.odfIndoorCode = value;
    }

    /**
     * Gets the value of the odfIndoorId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getOdfIndoorId() {
        return odfIndoorId;
    }

    /**
     * Sets the value of the odfIndoorId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setOdfIndoorId(Long value) {
        this.odfIndoorId = value;
    }

    /**
     * Gets the value of the originCabinetId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getOriginCabinetId() {
        return originCabinetId;
    }

    /**
     * Sets the value of the originCabinetId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setOriginCabinetId(Long value) {
        this.originCabinetId = value;
    }

    /**
     * Gets the value of the owIpGateway property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getOwIpGateway() {
        return owIpGateway;
    }

    /**
     * Sets the value of the owIpGateway property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setOwIpGateway(String value) {
        this.owIpGateway = value;
    }

    /**
     * Gets the value of the owIpSub property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getOwIpSub() {
        return owIpSub;
    }

    /**
     * Sets the value of the owIpSub property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setOwIpSub(String value) {
        this.owIpSub = value;
    }

    /**
     * Gets the value of the owLan property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getOwLan() {
        return owLan;
    }

    /**
     * Sets the value of the owLan property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setOwLan(String value) {
        this.owLan = value;
    }

    /**
     * Gets the value of the owNetwork property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getOwNetwork() {
        return owNetwork;
    }

    /**
     * Sets the value of the owNetwork property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setOwNetwork(String value) {
        this.owNetwork = value;
    }

    /**
     * Gets the value of the pointNumber property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getPointNumber() {
        return pointNumber;
    }

    /**
     * Sets the value of the pointNumber property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setPointNumber(Long value) {
        this.pointNumber = value;
    }

    /**
     * Gets the value of the portCode property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getPortCode() {
        return portCode;
    }

    /**
     * Sets the value of the portCode property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setPortCode(String value) {
        this.portCode = value;
    }

    /**
     * Gets the value of the portId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getPortId() {
        return portId;
    }

    /**
     * Sets the value of the portId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setPortId(Long value) {
        this.portId = value;
    }

    /**
     * Gets the value of the portLogic property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getPortLogic() {
        return portLogic;
    }

    /**
     * Sets the value of the portLogic property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setPortLogic(Long value) {
        this.portLogic = value;
    }

    /**
     * Gets the value of the portNoMdfA property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getPortNoMdfA() {
        return portNoMdfA;
    }

    /**
     * Sets the value of the portNoMdfA property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setPortNoMdfA(Long value) {
        this.portNoMdfA = value;
    }

    /**
     * Gets the value of the portNoMdfB property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getPortNoMdfB() {
        return portNoMdfB;
    }

    /**
     * Sets the value of the portNoMdfB property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setPortNoMdfB(Long value) {
        this.portNoMdfB = value;
    }

    /**
     * Gets the value of the portSpliterId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getPortSpliterId() {
        return portSpliterId;
    }

    /**
     * Sets the value of the portSpliterId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setPortSpliterId(Long value) {
        this.portSpliterId = value;
    }

    /**
     * Gets the value of the portTap property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getPortTap() {
        return portTap;
    }

    /**
     * Sets the value of the portTap property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setPortTap(String value) {
        this.portTap = value;
    }

    /**
     * Gets the value of the requestId property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets the value of the requestId property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }

    /**
     * Gets the value of the rootCabinetCode property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getRootCabinetCode() {
        return rootCabinetCode;
    }

    /**
     * Sets the value of the rootCabinetCode property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setRootCabinetCode(String value) {
        this.rootCabinetCode = value;
    }

    /**
     * Gets the value of the rtRd property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getRtRd() {
        return rtRd;
    }

    /**
     * Sets the value of the rtRd property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setRtRd(String value) {
        this.rtRd = value;
    }

    /**
     * Gets the value of the secondCouplerNo property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getSecondCouplerNo() {
        return secondCouplerNo;
    }

    /**
     * Sets the value of the secondCouplerNo property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setSecondCouplerNo(Long value) {
        this.secondCouplerNo = value;
    }

    /**
     * Gets the value of the secondDeviceId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getSecondDeviceId() {
        return secondDeviceId;
    }

    /**
     * Sets the value of the secondDeviceId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setSecondDeviceId(Long value) {
        this.secondDeviceId = value;
    }

    /**
     * Gets the value of the secondPortId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getSecondPortId() {
        return secondPortId;
    }

    /**
     * Sets the value of the secondPortId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setSecondPortId(Long value) {
        this.secondPortId = value;
    }

    /**
     * Gets the value of the secondStationId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getSecondStationId() {
        return secondStationId;
    }

    /**
     * Sets the value of the secondStationId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setSecondStationId(Long value) {
        this.secondStationId = value;
    }

    /**
     * Gets the value of the serviceLevel property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getServiceLevel() {
        return serviceLevel;
    }

    /**
     * Sets the value of the serviceLevel property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setServiceLevel(String value) {
        this.serviceLevel = value;
    }

    /**
     * Gets the value of the setupAddress property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getSetupAddress() {
        return setupAddress;
    }

    /**
     * Sets the value of the setupAddress property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setSetupAddress(String value) {
        this.setupAddress = value;
    }

    /**
     * Gets the value of the stationAddress property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getStationAddress() {
        return stationAddress;
    }

    /**
     * Sets the value of the stationAddress property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setStationAddress(String value) {
        this.stationAddress = value;
    }

    /**
     * Gets the value of the stationCode property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getStationCode() {
        return stationCode;
    }

    /**
     * Sets the value of the stationCode property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setStationCode(String value) {
        this.stationCode = value;
    }

    /**
     * Gets the value of the stationId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getStationId() {
        return stationId;
    }

    /**
     * Sets the value of the stationId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setStationId(Long value) {
        this.stationId = value;
    }

    /**
     * Gets the value of the stbSerial property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getStbSerial() {
        return stbSerial;
    }

    /**
     * Sets the value of the stbSerial property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setStbSerial(String value) {
        this.stbSerial = value;
    }

    /**
     * Gets the value of the streamId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getStreamId() {
        return streamId;
    }

    /**
     * Sets the value of the streamId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setStreamId(Long value) {
        this.streamId = value;
    }

    /**
     * Gets the value of the subAddress property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getSubAddress() {
        return subAddress;
    }

    /**
     * Sets the value of the subAddress property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setSubAddress(String value) {
        this.subAddress = value;
    }

    /**
     * Gets the value of the subType property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getSubType() {
        return subType;
    }

    /**
     * Sets the value of the subType property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setSubType(String value) {
        this.subType = value;
    }

    /**
     * Gets the value of the subscriptionStatus property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getSubscriptionStatus() {
        return subscriptionStatus;
    }

    /**
     * Sets the value of the subscriptionStatus property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setSubscriptionStatus(Long value) {
        this.subscriptionStatus = value;
    }

    /**
     * Gets the value of the tapCode property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getTapCode() {
        return tapCode;
    }

    /**
     * Sets the value of the tapCode property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setTapCode(String value) {
        this.tapCode = value;
    }

    /**
     * Gets the value of the tapId property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getTapId() {
        return tapId;
    }

    /**
     * Sets the value of the tapId property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setTapId(Long value) {
        this.tapId = value;
    }

    /**
     * Gets the value of the tranmissionDeviceId1 property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getTranmissionDeviceId1() {
        return tranmissionDeviceId1;
    }

    /**
     * Sets the value of the tranmissionDeviceId1 property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setTranmissionDeviceId1(Long value) {
        this.tranmissionDeviceId1 = value;
    }

    /**
     * Gets the value of the tranmissionDeviceId2 property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getTranmissionDeviceId2() {
        return tranmissionDeviceId2;
    }

    /**
     * Sets the value of the tranmissionDeviceId2 property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setTranmissionDeviceId2(Long value) {
        this.tranmissionDeviceId2 = value;
    }

    /**
     * Gets the value of the tranmissionPortId1 property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getTranmissionPortId1() {
        return tranmissionPortId1;
    }

    /**
     * Sets the value of the tranmissionPortId1 property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setTranmissionPortId1(Long value) {
        this.tranmissionPortId1 = value;
    }

    /**
     * Gets the value of the tranmissionPortId2 property.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getTranmissionPortId2() {
        return tranmissionPortId2;
    }

    /**
     * Sets the value of the tranmissionPortId2 property.
     *
     * @param value allowed object is {@link Long }
     *
     */
    public void setTranmissionPortId2(Long value) {
        this.tranmissionPortId2 = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setType(String value) {
        this.type = value;
    }

    public String getNodeTbCode() {
        return nodeTbCode;
    }

    public void setNodeTbCode(String nodeTbCode) {
        this.nodeTbCode = nodeTbCode;
    }

    public String getSpliterCode() {
        return spliterCode;
    }

    public void setSpliterCode(String spliterCode) {
        this.spliterCode = spliterCode;
    }
}
