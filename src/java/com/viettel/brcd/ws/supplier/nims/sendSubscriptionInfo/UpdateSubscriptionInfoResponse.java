
package com.viettel.brcd.ws.supplier.nims.sendSubscriptionInfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <p>Java class for updateSubscriptionInfoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateSubscriptionInfoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://webservice.infra.nims.viettel.com/}resultForm" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement(name = "updateSubscriptionInfoResponse", namespace="http://webservice.infra.nims.viettel.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateSubscriptionInfoResponse", propOrder = {
    "_return"
})
public class UpdateSubscriptionInfoResponse {

    @XmlElement(name = "return")
    protected ResultForm _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link ResultForm }
     *     
     */
    public ResultForm getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultForm }
     *     
     */
    public void setReturn(ResultForm value) {
        this._return = value;
    }

}
