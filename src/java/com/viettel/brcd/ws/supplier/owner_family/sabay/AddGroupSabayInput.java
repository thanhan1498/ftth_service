/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.owner_family.sabay;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author quangdm
 */
@XmlRootElement(name = "AddGroupSabayInput")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddGroupSabayInput", propOrder = {
    "username",
    "password",
    "requestId",
    "msisdn",
    "command",
    "agent",
    "param",
    "action",
    "send_sms"
})
public class AddGroupSabayInput {

    protected String username;
    protected String password;
    protected String requestId;
    protected String msisdn;
    protected String command;
    protected String agent;
    protected String param;
    protected String action;
    protected String send_sms;
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getSend_sms() {
        return send_sms;
    }

    public void setSend_sms(String send_sms) {
        this.send_sms = send_sms;
    }

}
