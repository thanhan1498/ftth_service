/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.owner_family.sabay;

import com.viettel.bccs.cm.model.Owner;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author quangdm
 */
@XmlRootElement(name = "return")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "return", propOrder = {
    "errCode",
    "errOcs",
    "message",
    "listOwner"
})
public class AddGroupSabayOutput {
    protected String errCode;
    protected String errOcs;
    protected String message;
    protected List<Owner>listOwner;

  
    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrOcs() {
        return errOcs;
    }

    public void setErrOcs(String errOcs) {
        this.errOcs = errOcs;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
      public List<Owner> getListOwner() {
        return listOwner;
    }

    public void setListOwner(List<Owner> listOwner) {
        this.listOwner = listOwner;
    }
}
