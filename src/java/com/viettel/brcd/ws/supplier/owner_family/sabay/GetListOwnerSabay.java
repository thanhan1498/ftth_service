/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.owner_family.sabay;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author quangdm
 */
@XmlRootElement(name = "GetListOwnerSabay")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetListOwnerSabay", propOrder = {
    "hasCabinet",
    "cabinetId",
    "stationId"
})
public class GetListOwnerSabay {
    
}
