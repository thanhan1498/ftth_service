/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.owner_family.sabay;

import java.sql.Timestamp;
import java.util.HashMap;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author quangdm
 */
@XmlRootElement(name = "GetListOwnerSabayMsisdn")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetListOwnerSabayMsisdn", propOrder = {
    "MO_ID",
    "MSISDN",
    "COMMAND",
    "PARAM",
    "RECEIVE_TIME",
    "STATUS",
    "CHANNEL",
    "ACTION_TYPE",
    "APP_NAME",
    "PREPAID",
    "POSTPAID",
    "id",
    "msisdn",
    "subId",
    "productCode",
    "subType",
    "command",
    "param",
    "receiveTime",
    "channel",
    "actionType",
    "errCode",
    "errOcs",
    "fee",
    "nodeName",
    "clusterName",
    "message",
    "object",
    "feeAction",
    "appName",
    "hashMap"
})
public class GetListOwnerSabayMsisdn {
    protected  String MO_ID;
    protected String MSISDN;
    protected String COMMAND;
    protected String PARAM;
    protected String RECEIVE_TIME;
    protected String STATUS;
    protected String CHANNEL;
    protected String ACTION_TYPE;
    protected String APP_NAME;
    protected int PREPAID;
    protected int POSTPAID;
    protected Long id;
    protected String msisdn;
    protected Long subId;
    protected String productCode;
    protected Integer subType;
    protected String command;
    protected String param;
    protected Timestamp receiveTime;
    protected String channel;
    protected Integer actionType;
    protected String errCode;
    protected String errOcs;
    protected Long fee;
    protected String nodeName;
    protected String clusterName;
    protected String message;
    protected Object object;
    protected long feeAction;
    protected String appName;
    protected HashMap hashMap;

    public String getMO_ID() {
        return MO_ID;
    }

    public void setMO_ID(String MO_ID) {
        this.MO_ID = MO_ID;
    }

    public String getMSISDN() {
        return MSISDN;
    }

    public void setMSISDN(String MSISDN) {
        this.MSISDN = MSISDN;
    }

    public String getCOMMAND() {
        return COMMAND;
    }

    public void setCOMMAND(String COMMAND) {
        this.COMMAND = COMMAND;
    }

    public String getPARAM() {
        return PARAM;
    }

    public void setPARAM(String PARAM) {
        this.PARAM = PARAM;
    }

    public String getRECEIVE_TIME() {
        return RECEIVE_TIME;
    }

    public void setRECEIVE_TIME(String RECEIVE_TIME) {
        this.RECEIVE_TIME = RECEIVE_TIME;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getCHANNEL() {
        return CHANNEL;
    }

    public void setCHANNEL(String CHANNEL) {
        this.CHANNEL = CHANNEL;
    }

    public String getACTION_TYPE() {
        return ACTION_TYPE;
    }

    public void setACTION_TYPE(String ACTION_TYPE) {
        this.ACTION_TYPE = ACTION_TYPE;
    }

    public String getAPP_NAME() {
        return APP_NAME;
    }

    public void setAPP_NAME(String APP_NAME) {
        this.APP_NAME = APP_NAME;
    }

    public int getPREPAID() {
        return PREPAID;
    }

    public void setPREPAID(int PREPAID) {
        this.PREPAID = PREPAID;
    }

    public int getPOSTPAID() {
        return POSTPAID;
    }

    public void setPOSTPAID(int POSTPAID) {
        this.POSTPAID = POSTPAID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Integer getSubType() {
        return subType;
    }

    public void setSubType(Integer subType) {
        this.subType = subType;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public Timestamp getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(Timestamp receiveTime) {
        this.receiveTime = receiveTime;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getActionType() {
        return actionType;
    }

    public void setActionType(Integer actionType) {
        this.actionType = actionType;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrOcs() {
        return errOcs;
    }

    public void setErrOcs(String errOcs) {
        this.errOcs = errOcs;
    }

    public Long getFee() {
        return fee;
    }

    public void setFee(Long fee) {
        this.fee = fee;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public long getFeeAction() {
        return feeAction;
    }

    public void setFeeAction(long feeAction) {
        this.feeAction = feeAction;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public HashMap getHashMap() {
        return hashMap;
    }

    public void setHashMap(HashMap hashMap) {
        this.hashMap = hashMap;
    }
}
