/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.owner_family.sabay;

import com.viettel.bccs.cm.controller.SmsClient;
import com.viettel.bccs.cm.util.ResourceUtils;
import com.viettel.brcd.ws.bccsgw.model.Param;
import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.brcd.ws.common.WebServiceClient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 *
 * @author quangdm
 */
public class SabayWsGetInfoBusiness extends WebServiceClient {

    public AddGroupSabayOutput sendReqGetListOwnerToBCCSGW(List<Param> lstParam) throws Exception {
        return (AddGroupSabayOutput) sendRequestViaBccsGW_Param("GetListOwner", AddGroupSabayOutput.class, lstParam);
    }

    public static AddGroupSabayOutput GetListOwnerSabayResponse(String username, String password,
            String requestID, String msisdn, String command, String agent,
            String param, String action, String send_sms) throws Exception {
        return new SabayWsGetInfoBusiness().sendReqGetListOwnerToBCCSGW(
                getLstParam(username, password, requestID, msisdn, command,
                        agent, param, action, send_sms));
    }

    public GetListMemberOwnerSabayOutput sendReqGetListMemnerOwnerToBCCSGW(List<Param> lstParam) throws Exception {
        return (GetListMemberOwnerSabayOutput) sendRequestViaBccsGW_Param("GetListMemberSabay", GetListMemberOwnerSabayOutput.class, lstParam);
    }

    public static GetListMemberOwnerSabayOutput getListMemberSabay(String username,
            String password, String requestID, String msisdn, String command, String agent,
            String param, String action, String send_sms) throws Exception {
        return new SabayWsGetInfoBusiness().sendReqGetListMemnerOwnerToBCCSGW(getLstParam(username,
                password, requestID, msisdn, command, agent, param, action, send_sms));
    }

    public GetListMemberOwnerSabayOutput sendReqcheckAddGroupN3_P3ToBCCSGW(List<Param> lstParam) throws Exception {
        return (GetListMemberOwnerSabayOutput) sendRequestViaBccsGW_Param("CheckAddMemberSabay", GetListMemberOwnerSabayOutput.class, lstParam);
    }

    public static GetListMemberOwnerSabayOutput checkAddGroupN3_P3(String username,
            String password, String requestID, String msisdn, String command, String agent,
            String param, String action, String send_sms) throws Exception {
        return new SabayWsGetInfoBusiness().sendReqcheckAddGroupN3_P3ToBCCSGW(getLstParam(username,
                password, requestID, msisdn, command, agent, param, action, send_sms));
    }

    public GetListMemberOwnerSabayOutput sendReqAddGroupN3_P3ToBCCSGW(List<Param> lstParam) throws Exception {
        return (GetListMemberOwnerSabayOutput) sendRequestViaBccsGW_Param("addMemberSabay", GetListMemberOwnerSabayOutput.class, lstParam);
    }

    public static GetListMemberOwnerSabayOutput AddGroupN3_P3(String username,
            String password, String requestID, String msisdn, String command, String agent,
            String param, String action, String send_sms) throws Exception {
        return new SabayWsGetInfoBusiness().sendReqAddGroupN3_P3ToBCCSGW(getLstParam(username,
                password, requestID, msisdn, command, agent, param, action, send_sms));
    }

    public GetListMemberOwnerSabayOutput sendReqcheckOwnerN3_P3ToBCCSGW(List<Param> lstParam) throws Exception {
        return (GetListMemberOwnerSabayOutput) sendRequestViaBccsGW_Param("CheckAddMemberSabay", GetListMemberOwnerSabayOutput.class, lstParam);
    }

    public static GetListMemberOwnerSabayOutput checkOwnerN3_P3(String username,
            String password, String requestID, String msisdn, String command, String agent,
            String param, String action, String send_sms) throws Exception {
        return new SabayWsGetInfoBusiness().sendReqcheckOwnerN3_P3ToBCCSGW(getLstParam(username,
                password, requestID, msisdn, command, agent, param, action, send_sms));
    }

    private static List<Param> getLstParam(String username, String password,
            String requestID, String msisdn, String command, String agent,
            String param, String action, String send_sms) {
        List<Param> lstParam = new ArrayList<Param>();
        Param prName = new Param();
        prName.setName("username");
        prName.setValue(username);
        lstParam.add(prName);
        //
        Param prPassword = new Param();
        prPassword.setName("password");
        prPassword.setValue(password);
        lstParam.add(prPassword);
        //
        Param prRequest = new Param();
        prRequest.setName("requestId");
        prRequest.setValue(requestID);
        lstParam.add(prRequest);
        //
        Param prMsisdn = new Param();
        prMsisdn.setName("msisdn");
        prMsisdn.setValue(msisdn);
        lstParam.add(prMsisdn);
        //
        Param prCommand = new Param();
        prCommand.setName("command");
        prCommand.setValue(command);
        lstParam.add(prCommand);
        //
        Param prAgent = new Param();
        prAgent.setName("param");
        prAgent.setValue(param);
        lstParam.add(prAgent);
        //
        Param prParam = new Param();
        prParam.setName("agent");
        prParam.setValue(agent);
        lstParam.add(prParam);
        //
        Param prAction = new Param();
        prAction.setName("action");
        prAction.setValue(action);
        lstParam.add(prAction);
        //
        Param prSend_sms = new Param();
        prSend_sms.setName("send_sms");
        prSend_sms.setValue(send_sms);
        lstParam.add(prSend_sms);
        return lstParam;
    }

    public static void main(String[] args) {
        try {
         //   String OTP =String.valueOf(OTP(4));
         //  SmsClient smsClient=new SmsClient();
//            smsClient.sendSMS("0716028228", "tesst");
            AddGroupSabayOutput addGroupSabayOutput = GetListOwnerSabayResponse(ResourceUtils.getResource("userName_API_SaBay", "provisioning"),
                    ResourceUtils.getResource("password_API_SaBay", "provisioning"), "43242", "977231127", "ADD",
                    "977231127", "09", "3", "0");
            if (addGroupSabayOutput.getErrCode().equals("11")) {

            } else if (addGroupSabayOutput.getErrCode().equals("0")) {

            } else if (addGroupSabayOutput.getErrCode().equals("-99")) {

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
     static char[] OTP(int len) {
        System.out.println("Generating OTP using random() : ");
        System.out.print("You OTP is : ");

        // Using numeric values
        String numbers = "0123456789";

        // Using random method
        Random rndm_method = new Random();

        char[] otp = new char[len];

        for (int i = 0; i < len; i++) {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            otp[i]
                    = numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return otp;
    }
}
