/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.soc;

/**
 *
 * @author cuongdm
 */
public class ErrorKnowledgeCataForm {
    private String authorPersonId;
    private String authorPersonName;
    private String authorUnitId;
    private String classId;
    private String className;
    private String createDate;
    private String description;
    private String deviceId;
    private String deviceName;
    private String ekcaId;
    private String errorCode;
    private String errorObjTopoId;
    private String errorObjTopoName;
    private String guidelineCustomer;
    private String guidelineId;
    private String guidelineName;
    private String guidelineValue;
    private String isActive;
    private String locationId;
    private String locationName;
    private String notice;
    private String rootReason;
    private String scope;
    private String serviceId;
    private String serviceName;
    private String signal;
    private String solution;
    private String solutionTemparory;
    private String testCode;
    private String testType;
    private String toolCode;
    private String toolId;
    private String toolName;
    private String version;
    private String woTime;

    /**
     * @return the authorPersonId
     */
    public String getAuthorPersonId() {
        return authorPersonId;
    }

    /**
     * @param authorPersonId the authorPersonId to set
     */
    public void setAuthorPersonId(String authorPersonId) {
        this.authorPersonId = authorPersonId;
    }

    /**
     * @return the authorPersonName
     */
    public String getAuthorPersonName() {
        return authorPersonName;
    }

    /**
     * @param authorPersonName the authorPersonName to set
     */
    public void setAuthorPersonName(String authorPersonName) {
        this.authorPersonName = authorPersonName;
    }

    /**
     * @return the authorUnitId
     */
    public String getAuthorUnitId() {
        return authorUnitId;
    }

    /**
     * @param authorUnitId the authorUnitId to set
     */
    public void setAuthorUnitId(String authorUnitId) {
        this.authorUnitId = authorUnitId;
    }

    /**
     * @return the classId
     */
    public String getClassId() {
        return classId;
    }

    /**
     * @param classId the classId to set
     */
    public void setClassId(String classId) {
        this.classId = classId;
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * @return the createDate
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId the deviceId to set
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return the deviceName
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * @param deviceName the deviceName to set
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    /**
     * @return the ekcaId
     */
    public String getEkcaId() {
        return ekcaId;
    }

    /**
     * @param ekcaId the ekcaId to set
     */
    public void setEkcaId(String ekcaId) {
        this.ekcaId = ekcaId;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * @return the errorObjTopoId
     */
    public String getErrorObjTopoId() {
        return errorObjTopoId;
    }

    /**
     * @param errorObjTopoId the errorObjTopoId to set
     */
    public void setErrorObjTopoId(String errorObjTopoId) {
        this.errorObjTopoId = errorObjTopoId;
    }

    /**
     * @return the errorObjTopoName
     */
    public String getErrorObjTopoName() {
        return errorObjTopoName;
    }

    /**
     * @param errorObjTopoName the errorObjTopoName to set
     */
    public void setErrorObjTopoName(String errorObjTopoName) {
        this.errorObjTopoName = errorObjTopoName;
    }

    /**
     * @return the guidelineCustomer
     */
    public String getGuidelineCustomer() {
        return guidelineCustomer;
    }

    /**
     * @param guidelineCustomer the guidelineCustomer to set
     */
    public void setGuidelineCustomer(String guidelineCustomer) {
        this.guidelineCustomer = guidelineCustomer;
    }

    /**
     * @return the guidelineId
     */
    public String getGuidelineId() {
        return guidelineId;
    }

    /**
     * @param guidelineId the guidelineId to set
     */
    public void setGuidelineId(String guidelineId) {
        this.guidelineId = guidelineId;
    }

    /**
     * @return the guidelineName
     */
    public String getGuidelineName() {
        return guidelineName;
    }

    /**
     * @param guidelineName the guidelineName to set
     */
    public void setGuidelineName(String guidelineName) {
        this.guidelineName = guidelineName;
    }

    /**
     * @return the guidelineValue
     */
    public String getGuidelineValue() {
        return guidelineValue;
    }

    /**
     * @param guidelineValue the guidelineValue to set
     */
    public void setGuidelineValue(String guidelineValue) {
        this.guidelineValue = guidelineValue;
    }

    /**
     * @return the isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * @param isActive the isActive to set
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * @return the locationId
     */
    public String getLocationId() {
        return locationId;
    }

    /**
     * @param locationId the locationId to set
     */
    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    /**
     * @return the locationName
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * @param locationName the locationName to set
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    /**
     * @return the notice
     */
    public String getNotice() {
        return notice;
    }

    /**
     * @param notice the notice to set
     */
    public void setNotice(String notice) {
        this.notice = notice;
    }

    /**
     * @return the rootReason
     */
    public String getRootReason() {
        return rootReason;
    }

    /**
     * @param rootReason the rootReason to set
     */
    public void setRootReason(String rootReason) {
        this.rootReason = rootReason;
    }

    /**
     * @return the scope
     */
    public String getScope() {
        return scope;
    }

    /**
     * @param scope the scope to set
     */
    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     * @return the serviceId
     */
    public String getServiceId() {
        return serviceId;
    }

    /**
     * @param serviceId the serviceId to set
     */
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    /**
     * @return the serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @param serviceName the serviceName to set
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * @return the signal
     */
    public String getSignal() {
        return signal;
    }

    /**
     * @param signal the signal to set
     */
    public void setSignal(String signal) {
        this.signal = signal;
    }

    /**
     * @return the solution
     */
    public String getSolution() {
        return solution;
    }

    /**
     * @param solution the solution to set
     */
    public void setSolution(String solution) {
        this.solution = solution;
    }

    /**
     * @return the solutionTemparory
     */
    public String getSolutionTemparory() {
        return solutionTemparory;
    }

    /**
     * @param solutionTemparory the solutionTemparory to set
     */
    public void setSolutionTemparory(String solutionTemparory) {
        this.solutionTemparory = solutionTemparory;
    }

    /**
     * @return the testCode
     */
    public String getTestCode() {
        return testCode;
    }

    /**
     * @param testCode the testCode to set
     */
    public void setTestCode(String testCode) {
        this.testCode = testCode;
    }

    /**
     * @return the testType
     */
    public String getTestType() {
        return testType;
    }

    /**
     * @param testType the testType to set
     */
    public void setTestType(String testType) {
        this.testType = testType;
    }

    /**
     * @return the toolCode
     */
    public String getToolCode() {
        return toolCode;
    }

    /**
     * @param toolCode the toolCode to set
     */
    public void setToolCode(String toolCode) {
        this.toolCode = toolCode;
    }

    /**
     * @return the toolId
     */
    public String getToolId() {
        return toolId;
    }

    /**
     * @param toolId the toolId to set
     */
    public void setToolId(String toolId) {
        this.toolId = toolId;
    }

    /**
     * @return the toolName
     */
    public String getToolName() {
        return toolName;
    }

    /**
     * @param toolName the toolName to set
     */
    public void setToolName(String toolName) {
        this.toolName = toolName;
    }

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return the woTime
     */
    public String getWoTime() {
        return woTime;
    }

    /**
     * @param woTime the woTime to set
     */
    public void setWoTime(String woTime) {
        this.woTime = woTime;
    }
}
