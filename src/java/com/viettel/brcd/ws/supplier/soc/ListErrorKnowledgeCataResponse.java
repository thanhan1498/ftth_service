/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.soc;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "getListErrorKnowledgeCataResponse", namespace = "http://service.spm.soc.viettel.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getListErrorKnowledgeCataResponse", propOrder = {
    "_return"
})
public class ListErrorKnowledgeCataResponse {

    @XmlElement(name = "return", nillable = true)
    private List<ErrorKnowledgeCataForm> _return;

    /**
     * @return the _return
     */
    public List<ErrorKnowledgeCataForm> getReturn() {
        return _return;
    }

    /**
     * @param _return the _return to set
     */
    public void setReturn(List<ErrorKnowledgeCataForm> _return) {
        this._return = _return;
    }
}
