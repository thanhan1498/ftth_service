/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.soc;

/**
 *
 * @author cuongdm
 */
public class ParamReqestSOC {

    private String paramKey;
    private String paramValue;

    public ParamReqestSOC() {
    }

    public ParamReqestSOC(String paramKey, String paramValue) {
        this.paramKey = paramKey;
        this.paramValue = paramValue;
    }

    /**
     * @return the paramKey
     */
    public String getParamKey() {
        return paramKey;
    }

    /**
     * @param paramKey the paramKey to set
     */
    public void setParamKey(String paramKey) {
        this.paramKey = paramKey;
    }

    /**
     * @return the paramValue
     */
    public String getParamValue() {
        return paramValue;
    }

    /**
     * @param paramValue the paramValue to set
     */
    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }
}
