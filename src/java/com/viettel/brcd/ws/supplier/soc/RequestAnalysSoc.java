/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.soc;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "doRequestAnalys")
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestAnalysSoc {

    private RequestDTO requestDTO;
    private List<ParamReqestSOC> params;

    /**
     * @return the requestDTO
     */
    public RequestDTO getRequestDTO() {
        return requestDTO;
    }

    /**
     * @param requestDTO the requestDTO to set
     */
    public void setRequestDTO(RequestDTO requestDTO) {
        this.requestDTO = requestDTO;
    }

    /**
     * @return the params
     */
    public List<ParamReqestSOC> getParams() {
        return params;
    }

    /**
     * @param params the params to set
     */
    public void setParams(List<ParamReqestSOC> params) {
        this.params = params;
    }
}
