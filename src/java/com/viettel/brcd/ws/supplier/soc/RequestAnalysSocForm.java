/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.soc;

/**
 *
 * @author cuongdm
 */
public class RequestAnalysSocForm {
    private String id;
    private String key;
    private String message;
    private String quantitySucc;
    private String quantityFail;
    private String requestTime;
    private String finishTime;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the quantitySucc
     */
    public String getQuantitySucc() {
        return quantitySucc;
    }

    /**
     * @param quantitySucc the quantitySucc to set
     */
    public void setQuantitySucc(String quantitySucc) {
        this.quantitySucc = quantitySucc;
    }

    /**
     * @return the quantityFail
     */
    public String getQuantityFail() {
        return quantityFail;
    }

    /**
     * @param quantityFail the quantityFail to set
     */
    public void setQuantityFail(String quantityFail) {
        this.quantityFail = quantityFail;
    }

    /**
     * @return the requestTime
     */
    public String getRequestTime() {
        return requestTime;
    }

    /**
     * @param requestTime the requestTime to set
     */
    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    /**
     * @return the finishTime
     */
    public String getFinishTime() {
        return finishTime;
    }

    /**
     * @param finishTime the finishTime to set
     */
    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }
}
