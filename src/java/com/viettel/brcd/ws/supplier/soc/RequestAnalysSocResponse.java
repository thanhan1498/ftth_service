/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.soc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author cuongdm
 */
@XmlRootElement(name = "doRequestAnalysResponse", namespace = "http://service.spm.soc.viettel.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "doRequestAnalysResponse", propOrder = {
    "_return"
})
public class RequestAnalysSocResponse {

    @XmlElement(name = "return", nillable = true)
    private RequestAnalysSocForm _return;

    /**
     * @return the _return
     */
    public RequestAnalysSocForm getReturn() {
        return _return;
    }

    /**
     * @param _return the _return to set
     */
    public void setReturn(RequestAnalysSocForm _return) {
        this._return = _return;
    }
}
