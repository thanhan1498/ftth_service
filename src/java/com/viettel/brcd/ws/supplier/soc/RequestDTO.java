/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.soc;

/**
 *
 * @author cuongdm
 */
public class RequestDTO {

    private String partyCode;
    private String password;
    private String requestId;
    private String username;

    public RequestDTO() {
    }

    public RequestDTO(String partyCode, String password, String requestId, String username) {
        this.partyCode = partyCode;
        this.password = password;
        this.requestId = requestId;
        this.username = username;
    }

    /**
     * @return the partyCode
     */
    public String getPartyCode() {
        return partyCode;
    }

    /**
     * @param partyCode the partyCode to set
     */
    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the requestId
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * @param requestId the requestId to set
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }
}
