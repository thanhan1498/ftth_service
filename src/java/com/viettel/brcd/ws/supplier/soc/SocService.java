/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.supplier.soc;

import com.google.gson.Gson;
import com.viettel.brcd.ws.common.CommonWebservice;
import com.viettel.brcd.ws.common.WebServiceClient;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 *
 * @author cuongdm
 */
public class SocService {

    private static WebServiceClient wsClient = new WebServiceClient();
    private static ResourceBundle resource = ResourceBundle.getBundle("provisioning");

    public static RequestAnalysSocResponse requestAnalysSoc(String account, Long taskMngtId, StringBuilder requestStr) throws Exception {
        RequestAnalysSoc request = new RequestAnalysSoc();
        RequestDTO requestDtp = new RequestDTO(resource.getString("soc_partyCode"), resource.getString("soc_password"), resource.getString("soc_requestId"), resource.getString("soc_username"));
        List<ParamReqestSOC> list = new ArrayList<ParamReqestSOC>();
        list.add(new ParamReqestSOC("account", account));
        list.add(new ParamReqestSOC("actor", "5"));
        list.add(new ParamReqestSOC("complaintId", taskMngtId.toString()));

        request.setParams(list);
        request.setRequestDTO(requestDtp);
        requestStr.append(new Gson().toJson(request));
        String xmlText = CommonWebservice.marshal(request).replaceAll("doRequestAnalys", "ser:doRequestAnalys");
        return (RequestAnalysSocResponse) wsClient.sendRequestViaBccsGW(xmlText, "doRequestAnalys", RequestAnalysSocResponse.class);
    }

    public static ListErrorKnowledgeCataResponse getSOCReasonFail() throws Exception{
        return (ListErrorKnowledgeCataResponse) wsClient.sendRequestViaBccsGW("", "getListErrorKnowledgeCata", ListErrorKnowledgeCataResponse.class);
    }
}
