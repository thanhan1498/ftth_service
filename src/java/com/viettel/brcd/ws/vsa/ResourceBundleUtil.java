/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.brcd.ws.vsa;

import java.util.ResourceBundle;

/**
 *
 * @author Administrator
 */
public class ResourceBundleUtil {
    static private ResourceBundle rb = null;

    public static String getResource(String key) {
        rb = ResourceBundle.getBundle("cas_en_US");
        try {
            if (rb.containsKey(key)) {
                return rb.getString(key);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return key;
    }
}
