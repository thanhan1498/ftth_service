package com.viettel.eafs.dataaccess.connector;

import com.viettel.eafs.dataaccess.config.HibernateSessionFactoryConfig;
import com.viettel.eafs.dataaccess.interpreter.HibernateFactoryXMLConfigurationInterpreter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;

public class HibernateConnector extends AbstractDataSourceConnector {

    private HashMap<String, HibernateSessionFactoryConfig> factoryConfigMap = new HashMap();
    private HashMap<String, SessionFactory> factoryMap = new HashMap();
    private String defSesName;
    private String factoryConfigFilePath;
    private final String TRANS_EX_MSG_TRANSNOTSTARTED = "Transaction not successfully started";
    private static Logger logger = Logger.getLogger(HibernateConnector.class);

    protected void readConfiguration() throws Throwable {
        logger.info("Reading configuration");
        HibernateFactoryXMLConfigurationInterpreter xmlInterpreter = new HibernateFactoryXMLConfigurationInterpreter(this);
        xmlInterpreter.interprete(this.factoryConfigFilePath);
    }

    protected void initDataSource() throws Throwable {
        logger.info("Initiating datasources/sessionfactories");

        for (String sesName : this.factoryConfigMap.keySet()) {
            try {
                HibernateSessionFactoryConfig factCfg = (HibernateSessionFactoryConfig) this.factoryConfigMap.get(sesName);
                String cfgPath = factCfg.getConfigFilePath();

                AnnotationConfiguration cfgObj = new AnnotationConfiguration().configure(cfgPath);
                if (factCfg.getIsEncrypted().booleanValue()) {
                    injectEncryptedConfigProperties(factCfg.getEncryptedProperties(), cfgObj.getProperties());
                }

                SessionFactory sesFact = cfgObj.buildSessionFactory();
                this.factoryMap.put(sesName, sesFact);
            } catch (Throwable tr) {
                logger.error("Loi khi tao factory co session name = " + sesName, tr);
                throw tr;
            }
        }
    }

    public void shutdown()
            throws Throwable {
        logger.info("Shutdowning connector");
        for (String sesName : this.factoryMap.keySet()) {
            ((SessionFactory) this.factoryMap.get(sesName)).close();
        }
    }

    protected void injectEncryptedConfigProperties(String[] properties, Properties op)
            throws Throwable {
        for (String property : properties) {
            String[] temp = property.split("=", 2);
            if (temp.length == 2) {
                String key = temp[0];
                String value = temp[1];

                op.setProperty(key, value);
                logger.info(String.format("Set property '%s' to Configuration", new Object[]{key}));
            }
        }
    }

    public void applyConfiguration(HashMap datasourceConfigMap, String defaultDataSourceName)
            throws Throwable {
        logger.info("Applying the result of configuration interpretering process");
        this.factoryConfigMap = datasourceConfigMap;
        setDefSesName(defaultDataSourceName);
    }

    public HashMap<String, SessionFactory> getSessionFactories() {
        return this.factoryMap;
    }

    public SessionFactory getSessionFactory(String sessionName) {
        return (SessionFactory) getSessionFactories().get(sessionName);
    }

    public SessionFactory getSessionFactory() {
        return (SessionFactory) getSessionFactories().get(this.defSesName);
    }

    public HashMap<String, Session> getCurrentSessions() {
        HashMap sessions = new HashMap();
        for (String key : getSessionFactories().keySet()) {
            sessions.put(key, ((SessionFactory) getSessionFactories().get(key)).getCurrentSession());
        }
        return sessions;
    }

    public Session getSessionAndBeginTransaction() {
        return getSessionAndBeginTransaction(this.defSesName);
    }

    public Session getSessionAndBeginTransaction(int transTimeout) {
        return getSessionAndBeginTransaction(this.defSesName, transTimeout);
    }

    public Session getSessionAndBeginTransaction(String sessionName) {
        if (getSessionFactory(sessionName) == null) {
            return null;
        }
        Session session = getSessionFactory(sessionName).openSession();
        session.beginTransaction();
        return session;
    }

    public Session getSessionAndBeginTransaction(String sessionName, int transTimeout) {
        if (getSessionFactory(sessionName) == null) {
            return null;
        }
        Session session = getSessionFactory(sessionName).openSession();
        session.getTransaction().setTimeout(transTimeout);
        session.getTransaction().begin();
        return session;
    }

    public HashMap<String, Session> commitCurrentSessions()
            throws Exception {
        HashMap<String, Session> sessions = getCurrentSessions();

        HashMap sessionsToRollBack = new HashMap();
        boolean hasExceptionDuringCommit = false;

        for (String sessionName : sessions.keySet()) {
            Session session = (Session) sessions.get(sessionName);
            if (session.isOpen()) {
                Transaction t = session.getTransaction();

                if ((t.isActive()) && (!hasExceptionDuringCommit)) {
                    try {
                        t.commit();
                    } catch (Throwable ex) {
                        hasExceptionDuringCommit = true;
                        sessionsToRollBack.put(sessionName, session);
                        logger.error("Co loi xay ra khi commit transaction cua session " + sessionName, ex);
                    }
                } else if (hasExceptionDuringCommit) {
                    sessionsToRollBack.put(sessionName, session);
                }
            }
        }
        return sessionsToRollBack;
    }

    public List commitCurrentSessionsWithExceptionHandling()
            throws Throwable {
        HashMap<String, Session> sessions = getCurrentSessions();

        HashMap sessionsToRollBack = new HashMap();
        boolean hasExceptionDuringCommit = false;

        Throwable caughtException = null;

        for (String sessionName : sessions.keySet()) {
            Session session = (Session) sessions.get(sessionName);
            if (session.isOpen()) {
                Transaction t = session.getTransaction();

                if ((t.isActive()) && (!hasExceptionDuringCommit)) {
                    try {
                        t.commit();
                    } catch (Throwable ex) {
                        caughtException = ex;

                        hasExceptionDuringCommit = true;
                        sessionsToRollBack.put(sessionName, session);
                        logger.error("Co loi xay ra khi commit transaction cua session " + sessionName, ex);
                    }
                } else if (hasExceptionDuringCommit) {
                    sessionsToRollBack.put(sessionName, session);
                }
            }
        }

        ArrayList ret = new ArrayList();
        ret.add(sessionsToRollBack);
        ret.add(caughtException);

        return ret;
    }

    public void rollBackSessions(HashMap<String, Session> sessionsToRollBack)
            throws Throwable {
        if (sessionsToRollBack != null) {
            for (String sessionName : sessionsToRollBack.keySet()) {
                Session session = (Session) sessionsToRollBack.get(sessionName);
                if (session.isOpen()) {
                    Transaction t = session.getTransaction();
                    try {
                        t.rollback();
                    } catch (Exception ex) {
                        if (!ex.getMessage().equals("Transaction not successfully started")) {
                            logger.error("CÃ³ lá»—i xáº£y ra khi rollback session " + sessionName, ex);
                            throw ex;
                        }
                    }
                }
            }
        }
    }

    public void closeCurrentSessions() {
        HashMap<String, Session> sessionMaps = getCurrentSessions();
        if (sessionMaps != null) {
            for (String key : sessionMaps.keySet()) {
                Session session = (Session) sessionMaps.get(key);
                try {
                    if (session.isOpen()) {
                        session.close();
                    }
                } catch (Exception ex) {
                    logger.error("Lá»—i khi Ä‘Ã³ng session \"" + key + "\"", ex);
                }
            }
        }
    }

    public String getFactoryConfigFilePath() {
        return this.factoryConfigFilePath;
    }

    public void setFactoryConfigFilePath(String factoryConfigFilePath) {
        this.factoryConfigFilePath = factoryConfigFilePath;
    }

    public String getDefSesName() {
        return this.defSesName;
    }

    public void setDefSesName(String defSesName) {
        this.defSesName = defSesName;
    }
}