/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package viettel.passport.client;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author cuongdm
 */
public class NewMain {

    private static final String ALGORITHM = "AES";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            String text = "Hello World";
            String key = "Bar12345Bar12345"; // 128 bit key

            // Create key and cipher
            Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
            Key aesKey1 = new SecretKeySpec("sdfdfdfdf".getBytes(), "AES");

            String de = encrypt(aesKey, text);
            String en = decrypt(aesKey1, de);
            int a = 1;
        } catch (Exception ex) {
            int a = 1;
        }
    }

    public static String encrypt(Key key, String valueToEnc) throws Exception {

        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.ENCRYPT_MODE, key);

        System.out.println("valueToEnc.getBytes().length " + valueToEnc.getBytes().length);
        byte[] encValue = c.doFinal(valueToEnc.getBytes());
        System.out.println("encValue length" + encValue.length);
        byte[] encryptedByteValue = new Base64().encode(encValue);
        String encryptedValue = new String(encryptedByteValue);
        System.out.println("encryptedValue " + encryptedValue);

        return encryptedValue;
    }

    public static String decrypt(Key key, String encryptedValue) throws Exception {
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.DECRYPT_MODE, key);

        byte[] enctVal = new Base64().decode(encryptedValue.getBytes());
        System.out.println("enctVal length " + enctVal.length);

        byte[] decordedValue = c.doFinal(enctVal);

        return new String(decordedValue);
    }
}
