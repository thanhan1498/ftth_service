package viettel.passport.client;

import java.util.ArrayList;
import org.w3c.dom.Element;
import viettel.passport.util.XMLUtil;

public class ObjectToken
        implements Comparable<ObjectToken> {

    private ArrayList<ObjectToken> childObjects;
    private Long ord;
    private long parentId;
    private long status;
    private long objectId;
    private long objectType;
    private String description;
    private String objectName;
    private String objectUrl;
    public static final String MODULE_TYPE = "M";
    public static final String COMPONENT_TYPE = "C";
    private String objectCode;

    public ObjectToken() {
        this.childObjects = new ArrayList();
    }

    public String getObjectCode() {
        return this.objectCode;
    }

    public void setObjectCode(String objectCode) {
        this.objectCode = objectCode;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getObjectId() {
        return this.objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    public String getObjectName() {
        return this.objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectType() {
        if (this.objectType == 0L) {
            return "M";
        }
        return "C";
    }

    public void setObjectType(long objectType) {
        this.objectType = objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = Long.parseLong(objectType);
    }

    public String getObjectUrl() {
        return this.objectUrl;
    }

    public void setObjectUrl(String objectUrl) {
        this.objectUrl = objectUrl;
    }

    public Long getOrd() {
        return this.ord;
    }

    public void setOrd(Long ord) {
        this.ord = ord;
    }

    public long getParentId() {
        return this.parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public long getStatus() {
        return this.status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public ArrayList<ObjectToken> getChildObjects() {
        return this.childObjects;
    }

    public void setChildObjects(ArrayList<ObjectToken> childObjects) {
        this.childObjects = childObjects;
    }

    public int compareTo(ObjectToken o) {
        return this.ord.compareTo(o.ord);
    }

    public static ArrayList<ObjectToken> findFirstLevelMenus(ArrayList<ObjectToken> listObjects) {
        ArrayList list = new ArrayList();
        for (Object item : listObjects) {
            ObjectToken mt = (ObjectToken) item;
            if (mt.getParentId() <= 0L) {
                list.add(mt);
            }
        }
        return list;
    }

    public static ObjectToken getMenuToken(Element menuEle) {
        ObjectToken mt = new ObjectToken();

        mt.setObjectId(XMLUtil.getLongValue(menuEle, "OBJECT_ID"));
        mt.setParentId(XMLUtil.getLongValue(menuEle, "PARENT_ID"));
        mt.setStatus(XMLUtil.getLongValue(menuEle, "STATUS"));
        mt.setOrd(Long.valueOf(XMLUtil.getLongValue(menuEle, "ORD")));
        mt.setObjectUrl(XMLUtil.getTextValue(menuEle, "OBJECT_URL"));
        mt.setObjectName(XMLUtil.getTextValue(menuEle, "OBJECT_NAME"));
        mt.setObjectCode(XMLUtil.getTextValue(menuEle, "OBJECT_CODE"));
        mt.setDescription(XMLUtil.getTextValue(menuEle, "DESCRIPTION"));
        mt.setObjectType(XMLUtil.getLongValue(menuEle, "OBJECT_TYPE_ID"));
        return mt;
    }
}